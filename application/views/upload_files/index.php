<!DOCTYPE html><head>

<?php $this->load->view('headerandsidebar'); ?>


<style>
ul.gallery {
    clear: both;
    float: left;
    width: 100%;
    margin-bottom: -10px;
    padding-left: 3px;
}
ul.gallery li.item {
    width: 25%;
    height: 215px;
    display: block;
    float: left;
    margin: 0px 15px 15px 0px;
    font-size: 12px;
    font-weight: normal;
    background-color: d3d3d3;
    padding: 10px;
    box-shadow: 10px 10px 5px #888888;
}

.item img{width: 100%; height: 184px;}

.item p {
    color: #6c6c6c;
    letter-spacing: 1px;
    text-align: center;
    position: relative;
    margin: 5px 0px 0px 0px;
}
</style>

    <p><?php echo $this->session->flashdata('statusMsg'); ?></p>
            
            <form enctype="multipart/form-data" action="" method="post">
                    <div class="col-lg-6">
                    <label><h3>Add Pictures to Provider Gallery</h3>
                    <br />
                    <div class="form-group">
                    <label>Select Provider Name</label>
                    <select  name="providerid" class="form-control" required>
                      <option value="">--SELECT PROVIDER NAME--</option>
                      <?php foreach ($view_providerdetails as $nameprovider) { ?>
                      <option value="<?php echo $nameprovider['id']; ?>">
                      <?php echo $nameprovider['provider_name']; ?></option>
                      <?php } ?>
                    </select>
                    </div>
                    <div class="form-group">
                        <label>Choose Files</label>
                        <input type="file"  name="userFiles[]" multiple />
                    </div>
                    <p>(You can select multiple images at a time)</p>
                    <div class="form-group">
                        <input class="form-control" type="submit" name="fileSubmit" value="UPLOAD FILES"/>
                    </div>
                   </div>
                 </form>  

        <div class="col-lg-12">
            <div class="row">
                <ul class="gallery">
                    <?php if(!empty($files)): foreach($files as $file): ?>
                    <li class="item">
                        <img src="<?php echo base_url('uploads/files/'.$file['file_name']); ?>" alt="" >
                        <p>Uploaded On <?php echo date("j M Y",strtotime($file['created'])); ?></p>
                    </li>
                    <?php endforeach; else: ?>
                    <p>File(s) not found.....</p>
                    <?php endif; ?>
                </ul>
            </div>
        </div>
        <?php $this->load->view('footer'); ?>
  