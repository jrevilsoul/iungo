<!DOCTYPE html><head>

  <meta charset="utf-8">

  <title>Admin Panel</title> 

  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <meta name="description" content="">

  <meta name="author" content="">

  <meta name="author" content="">

  <link rel="shortcut icon" href="">



  <link href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" rel="stylesheet">

  

  <link href="<?php echo base_url(); ?>css/loading.css" rel="stylesheet">

  <link href="<?php echo base_url(); ?>css/bootstrap.min" rel="stylesheet">

  <link href="<?php echo base_url(); ?>css/style.css" rel="stylesheet">

  <link href="<?php echo base_url(); ?>css/admin.css" rel="stylesheet">

  <link href="<?php echo base_url(); ?>css/font-awesome.min.css" rel="stylesheet">

  <link href="<?php echo base_url(); ?>css/themes/facebook.css" rel="stylesheet">

  <link href="<?php echo base_url(); ?>css/animate.min.css" rel="stylesheet">

  <link href="<?php echo base_url(); ?>css/dataurl.css" rel="stylesheet">

  <script src="<?php echo base_url(); ?>js/pace.min.js"></script>

  <script src="<?php echo base_url(); ?>js/jquery-1.11.2.js"></script>

  <link href="<?php echo base_url(); ?>css/alert.css" rel="stylesheet" />

  <link href="<?php echo base_url(); ?>css/default/theme.css" rel="stylesheet" />

  <script src="<?php echo base_url(); ?>js/alert.js"></script>

  <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>

  <!-- ckeditor -->

  <script src="http://phpexpertfoodordering.in/grill9/ckeditor/ckeditor.js"></script>

  <script type="text/javascript">

  CKEDITOR.replace( 'content_description',

  {

  width:"700",

  toolbar :

  [

  ['Source'],

  ['Cut','Copy','Paste','PasteText','PasteFromWord','-','Scayt'],

  ['Undo','Redo','-','Find','Replace','-','SelectAll','RemoveFormat'],

  ['Image','Flash','Table','HorizontalRule','Smiley','SpecialChar','PageBreak'],

  ['Styles','Format'],['FontSize'],

  ['Bold','Italic','Strike'],

  ['NumberedList','BulletedList','/','Outdent','Indent','Blockquote'],

  ['Link','Unlink','Anchor'],

  ['Maximize','-','About']

  ],

  filebrowserBrowseUrl : 'ckeditor/ckfinder/ckfinder.html',

  filebrowserImageBrowseUrl : 'ckeditor/ckfinder/ckfinder.html?type=Images',

  filebrowserFlashBrowseUrl : 'ckeditor/ckfinder/ckfinder.html?type=Flash',

  filebrowserUploadUrl : 'ckeditor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',

  filebrowserImageUploadUrl : 'ckeditor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',

  filebrowserFlashUploadUrl : 'ckeditor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'

  }

  );

</script>
    <aside class="social-sidebar">

      <div class="social-sidebar-content">

        <div class="search-sidebar">

          <form class="search-sidebar-form has-icon filterform" onsubmit="return false;">

            <label for="sidebar-query" class="fa fa-search"></label>

            <input id="sidebar-query" type="text" placeholder="Search" class="search-query filtertxt">

          </form>

        </div>

        <div class="clearfix"></div>

        <div class="user"> <i class="fa-1x glyphicon glyphicon-user"></i> <span>Dashboard</span> </div>

        <div class="menu">

          <div class="menu-content">

          <ul id="social-sidebar-menu">



          <li><a href="#sitesetting" data-toggle="collapse" data-parent="#social-sidebar-menu">

            <i class="fa fa-gear"></i><span><small>Website Settings</span></small><i class="fa arrow"></i></a>

            <ul id="sitesetting" class="collapse  wow fadeIn animated">

              <li><small><a href="<?php echo site_url('dashboard/add_general_settings'); ?>">Site Configuration</a></small></li>

              <!--<li><small><a href="<?php //echo site_url('dashboard/email_settings'); ?>">Site Email Settings</a></small></li>-->

              <li></small><a href="<?php echo site_url('dashboard/add_social_settings'); ?>">Site Social Media</a></small></li>

              <!--<li><small><a href="<?php echo site_url('dashboard/website_add_banner'); ?>">Site Add Banner</a></small></li>-->

            <li><small><a href="<?php echo site_url('dashboard/website_flash_banner'); ?>">Site Website Flash Banner</a></small></li>              

            </ul>

          </li>



          <li><a href="#extras" data-toggle="collapse" data-parent="#social-sidebar-menu">

            <i class="fa fa-user"></i><span><small>Extra Settings</span></small><i class="fa arrow"></i></a>

            <ul id="extras" class="collapse  wow fadeIn animated">



              <li><a href="<?php echo site_url('dashboard/view_comments'); ?>">Comments and Reviews</a></li>

              <li><a href="<?php echo site_url('dashboard/view_users'); ?>">User management</a></li>

            </ul>

          </li>



          <!-- <li>

				<a href="#Location" data-toggle="collapse" data-parent="#social-sidebar-menu"> <i class="fa fa-map"></i><span><small>Location</span></small><i class="fa arrow"></i> </a>

                <ul id="Location" class="collapse  wow fadeIn animated">

                  <li><small><a href="<?php //echo site_url('dashboard/location'); ?>">Add Location</a></small></li>

                </ul>

              </li> -->



              <li><a href="#Category" data-toggle="collapse" data-parent="#social-sidebar-menu">

                <i class="fa fa-suitcase"></i><span><small>Service Management</small></span> <i class="fa arrow"></i></a>

               <ul id="Category" class="collapse  wow fadeIn animated">

               

            <li><a href="<?php echo site_url('dashboard/view_catone'); ?>">Manage Services</a></li>

            <!--  <li><a href="<?php //echo site_url('dashboard/view_plan'); ?>">Manage Categories</a></li>

            <li><a href="<?php //echo site_url('dashboard/view_catthree'); ?>">Manage Sub Categories</a></li>                    -->

            </ul>

          </li>

             

        <li><a href="#cms" data-toggle="collapse" data-parent="#social-sidebar-menu"><i class="fa fa-clock-o"></i>

        <span><small>CMS Management</small></span><i class="fa arrow"></i></a>

            <ul id="cms" class="collapse  wow fadeIn animated">

				  <li><small><a href="<?php echo site_url('dashboard/cms_aboutus'); ?>">About us</a></small></li>

				  <li><small><a href="<?php echo site_url('dashboard/cms_services'); ?>">Services</a></small></li>

				  <li><small><a href="<?php echo site_url('dashboard/cms_contactus'); ?>">Contact Us</a></small></li>    

          <li><small><a href="<?php echo site_url('dashboard/view_testimonial'); ?>">Testimonials</a></small></li>

				  <li><small><a href="<?php echo site_url('dashboard/view_clients'); ?>">Our Clients</a></small></li>

            </ul>

        </li>


        <li><a href="#event" data-toggle="collapse" data-parent="#social-sidebar-menu"><i class="fa fa-gift"></i>

        <span><small>Event</small></span><i class="fa arrow"></i></a>

            <ul id="event" class="collapse  wow fadeIn animated">

          <li><small><a href="<?php echo site_url('dashboard/up_event'); ?>">Upcoming Event</a></small></li>

          <li><small><a href="<?php echo site_url('dashboard/past_event'); ?>">Past Event</a></small></li>

            </ul>

        </li>

        <li><a href="<?php echo site_url('dashboard/view_city'); ?>" ><i class="fa fa-building"></i>

        <span><small>City</small></span></a></li>


        <li><a href="<?php echo site_url('dashboard/view_plan'); ?>" ><i class="fa fa-gift"></i>

        <span><small>Plan</small></span></a></li>


       <!--  <li><a href="<?php //echo site_url('dashboard/view_subscriber'); ?>" ><i class="fa fa-rocket"></i>
       
       <span><small>Subscriber List</small></span></a></li> -->




  </div>

</aside>

<header>

  <!-- BEGIN NAVBAR-->

  <input type="hidden" id="sidebarclass" class="">

  <nav role="navigation" class="navbar navbar-fixed-top navbar-super social-navbar">

    <div class="navbar-header"> <a href="http://demo/supplier" class="navbar-brand"> <i class="fa fa-inbox light"></i><span>&nbsp;&nbsp;IUNGO</span> </a> </div>

    <div class="navbar-toggle navtogglebtn"><i class="fa fa-align-justify"></i> </div>

    <div>

      <ul class="nav navbar-nav">

        <li class="dropdown navbar-super-fw hidden-xs">

          <a><span>

          <script> function startTime() { var today=new Date(); var h=today.getHours(); var m=today.getMinutes(); var s=today.getSeconds(); m=checkTime(m); s=checkTime(s); document.getElementById('txt').innerHTML=h+":"+m+":"+s; t=setTimeout(function(){startTime()},500); } function checkTime(i) { if (i<10) { i="0" + i; } return i; } </script>

          <strong> <body onload="startTime()" class="">

          </body><div class="pull-left wow fadeInLeft animated" id="txt"></div>

          </strong>&nbsp;|&nbsp;<small class="pull-right wow fadeInRight animated">

          <script> var tD = new Date(); var datestr =  tD.getDate(); document.write(""+datestr+""); </script>

          <script type="text/javascript"> var d=new Date(); var weekday=new Array("","","","","","", ""); var monthname=new Array("January","February","March","April","May","June","July","August","September","Octobar","November","December"); document.write(monthname[d.getMonth()] + " "); </script>

          <script> var tD = new Date(); var datestr = tD.getFullYear(); document.write(""+datestr+""); </script>

          </small></span>

          </a>

        </li>

      </ul>

    <ul class="nav navbar-nav navbar-right">

      <!-- END DROPDOWN MESSAGES-->

      <li class="divider-vertical"></li>

      <!-- BEGIN EXTRA DROPDOWN-->

      <li class="dropdown"><a href="#" data-toggle="dropdown" data-hover="dropdown" data-delay="0" class="dropdown-toggle"><i class="fa fa-gear fa-lg"></i><span>&nbsp;&nbsp;Setting</span></a>

        <ul class="dropdown-menu">     

        <li><a href="<?php echo site_url('dashboard/user_profile');?>"><i class="glyphicon glyphicon-user">          

        </i>&nbsp;<?php echo $this->session->userdata('username'); ?></a></li>



        <li><a href="<?php echo site_url('dashboard/logout'); ?>"><i class="fa fa-sign-out"></i>&nbsp;Log Out</a></li>

        <li class="divider"></li>

        <li><a href="supplier_help.html" target="_blank"><i class="fa fa-info"></i>&nbsp;Help</a></li>

        </ul>

      </li>

    <!-- END EXTRA DROPDOWN-->

  </ul>

</div>

<!-- /.navbar-collapse-->

</nav>

<!-- END NAVBAR-->

</header>

<script type="text/javascript">

$(function(){

var sideClass = $("#sidebarclass").prop('class');

$('body').addClass(sideClass);

$(".navtogglebtn").on('click',function(){

var sidebar = '';

if($('body').hasClass('reduced-sidebar')){

sidebar = "";

}else{

sidebar = "reduced-sidebar";

}

$.post("http://demo/admin/ajaxcalls/reduceSidebar", {sidebar: sidebar}, function(resp){

});

});

});

function getNotifications(){

$.post("http://demo/admin/ajaxcalls/notifications",{},function(response){

var resp = $.parseJSON(response);

if(resp.totalReviews > 0){

$(".notifyRevCount").html(resp.totalReviews);

$(".revnotifyHeader").show();

/*if($("li").hasClass("notificationReviews")){

}else{

}*/

$(".notificationReviews").remove();

$(".revnotifyHeader").after(resp.revhtml);

}else{

$(".revnotifyHeader").hide();

$(".notifyRevCount").html("");

}

//Supplier notifications

if(resp.totalAccounts > 0){

$(".notifyAccountsCount").html(resp.totalAccounts);

$(".accountsnotifyHeader").show();

if($("li").hasClass("notificationAccounts")){

}else{

$(".accountsnotifyHeader").after(resp.accountshtml);

}

}else{

$(".accountsnotifyHeader").hide();

$(".notifyAccountsCount").html("");

}

//Booking notifications

if(resp.totalBookings > 0){

$(".notifyBookingsCount").html(resp.totalBookings);

$(".bookingsnotifyHeader").show();

if($("li").hasClass("notificationBookings")){

}else{

$(".bookingsnotifyHeader").after(resp.bookingshtml);

}

}else{

$(".bookingsnotifyHeader").hide();

$(".notifyBookingsCount").html("");

}

});

}

</script>

<div class="main">

<div class="container" id="content">

<!-- <script type="text/javascript">

$(function(){

var room = $("#roomid").val();

$(".submitfrm").click(function(){

var submitType = $(this).prop('id');

for ( instance in CKEDITOR.instances )

{

CKEDITOR.instances[instance].updateElement();

}

$(".output").html("");

$('html, body').animate({

scrollTop: $('body').offset().top

}, 'slow');

if(submitType == "add"){

url = "#" ;

}else{

url = "#"+room;

}

$.post(url,$(".room-form").serialize() , function(response){

if($.trim(response) != "done"){

$(".output").html(response);

}else{

window.location.href = "#";

}

});

})

})

</script> -->