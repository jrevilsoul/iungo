<!DOCTYPE html><head>

<?php include('userheadersidebar.php'); ?>
<div class="row">
  <div class="container" id="content">
    <div class="panel panel-default">
      <div class="panel-heading">Edit Product</div>

      <form class="add_button" action="#" method="post">
      <!-- <button type="submit" class="btn btn-success">
      <i class="glyphicon glyphicon-plus-sign"></i>Booking Edit Delete </button> -->
      </form>
      
      <div class="panel-body">   
            <div class="xcrud">
                <div class="xcrud-container">
                <div class="xcrud-ajax">
                <div class=" pull-right" style="float:right;">
                <input type="hidden" class="xcrud-data" name="key" 
                value="017bfe2df5d71e29f34946466c178b21e4491ace" />
                <input type="hidden" class="xcrud-data" name="orderby" value="pt_cars.car_id" />
                <input type="hidden" class="xcrud-data" name="order" value="desc" />
                <input type="hidden" class="xcrud-data" name="start" value="0" />
                <input type="hidden" class="xcrud-data" name="limit" value="50" />
                <input type="hidden" class="xcrud-data" name="instance" 
                value="786870d5a374d329252a31dbbc49e5d866817eb0" />
                <input type="hidden" class="xcrud-data" name="task" value="list" />
                </div> 
                <div class="xcrud-list-container">
                 <div id="container" class="container">
                 <div class="row">
                 <div class="col-md-6 col-md-offset-2">
                 <h2 class="text-center">Add Product</h2>
                <br />
                <form method="post" action="<?php echo site_url('user/update_product/'.$edit_product[0]['p_id'].''); ?>" name="data_register" enctype="multipart/form-data">
                <label for="Country">Status</label>
                <select class="form-control" name="status" required />
                <option value="1">Enable</option>
                <option value="0">Disable</option>
                </select>                       
                <br />
                <br />
                <label for="Country">Select Category</label>
				
                <select class="form-control" name="sub_id" required />
                           
                <?php  foreach ($view_catthree as $value){
				?>
                <option value="<?php echo $value['sub_cate_id'];  ?>"
				<?php if($value['sub_cate_id']==$edit_product[0]['mini_sub_cat_id']){ ?> selected="selected" <?php }?>
				"><?php echo $value['name']; ?></option>
                <?php } ?>
                </select>
                <br />
                <br />
				<label for="product">Product ID</label>
                <input type="text" align="center" class="form-control" name="p_id" value="<?php echo $edit_product[0]['product_id'] ?>" required  />          
                <br />
                <label for="product">Product Name</label>
                <input type="text" align="center" class="form-control" name="pname" value="<?php echo $edit_product[0]['p_name'] ?>" required />          
                <br />
				<label for="product">product image</label>
                
		      <input type="file" name="image" id="image" /><img src="<?php echo base_url('uploads/').$edit_product[0]['image'];?>" id="profile-img-tag" width="100px" />
                <br />
				 <br />
				<label for="product">Product Amount</label>
			    <input type="text" align="center" class="form-control" name="amount" value="<?php echo $edit_product[0]['amount'] ?>" required />  
                <br />
				<label for="product">Product Description</label>
			    <textarea name="description" class="form-control"  id="one" rows="8" cols="65"><?php echo $edit_product[0]['description'] ?></textarea>
                <br />				
                <br />
                <button type="submit" class="btn btn-primary pull-right">Submit</button>
                </form>
                </div>
                </div>
                </div>
                </div>
               </div>
               
      <?php include('footer.php'); ?>
</body>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
<script type="text/javascript">
     function readURL(input) {
        if (input.files && input.files[0]) {
        // alert("hello");
            var reader = new FileReader();
            console.log(reader);
            reader.onload = function (e) {
                $('#profile-img-tag').attr('src', e.target.result);
				//$('#profile-img-tag').show();
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#image").change(function(){
        readURL(this);
    });

</script>
</html>