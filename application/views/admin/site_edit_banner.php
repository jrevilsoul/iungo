<!DOCTYPE html>

<head>

<?php include('headerandsidebar.php'); ?>

<div class="row">

  <div class="container" id="content">

    <div class="panel panel-default">

      <div class="panel-heading">Site Edit banner</div>

      <div class="panel-body">

        <div class="xcrud">

          <div class="xcrud-container">

            <div class="xcrud-ajax">

              <div class="xcrud-list-container">

              <div id="container" class="container">

              <div class="col-md-11">                                 

          

          <form method="post" action="<?php echo site_url('dashboard/update_banner/').$view_banner[0]['id']; ?>" name="data_register" enctype="multipart/form-data">

               <br />

			   

			   <div class="row form-group">

                <label class="col-md-2 control-label text-left">Banner Name:</label>

                <div class="col-md-10">

                 <input type="text" name="banner_name" class="form-control" value="<?php echo $view_banner[0]['banner_name'];?>" />

                </div>

                </div>

			   

			   <div class="row form-group">

                <label class="col-md-2 control-label text-left">Banner Title:</label>

                <div class="col-md-10">

                 <input type="text" name="banner_title" class="form-control" value="<?php echo $view_banner[0]['banner_title'];?>" />

                </div>

                </div>

				

                      

          <div class="row form-group">

                <label class="col-md-2 control-label text-left">Banner Image:</label>

                <div class="col-md-10">

                <input type="file" name="image" id="image" multiple />  

                  <img src="<?php echo base_url('banners/').$view_banner[0]['image'];?>" id="profile-img-tag" width="100px" style=""/>  

                </div>

                </div>

                



                <button type="submit" class="btn btn-primary pull-right">Submit</button>

                </form>

                </div>

              </div>

            </div>

           </div>



    <?php include('footer.php'); ?>

</body>



<script type="text/javascript">

     function readURL(input) {

        if (input.files && input.files[0]) {

        // alert("hello");

            var reader = new FileReader();

            console.log(reader);

            reader.onload = function (e) {

                $('#profile-img-tag').attr('src', e.target.result);

        $('#profile-img-tag').show();

            }

            reader.readAsDataURL(input.files[0]);

        }

    }

    $("#image").change(function(){

        readURL(this);

    });



</script>



</html>