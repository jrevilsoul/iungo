<!DOCTYPE html>

<head>

<?php include('headerandsidebar.php'); ?>

<div class="row">

  <div class="container" id="content">

    <div class="panel panel-default">

      <div class="panel-heading">Site Setting</div>

    

      <div class="panel-body">

       <div>

          <?php 

       if($this->session->flashdata('updatemessage')){

          ?>

       <div class="alert alert-success" style="center"> 

         <?php  echo $this->session->flashdata('updatemessage'); }



         elseif($this->session->flashdata('insertmessage')){?>

          <div class="alert alert-success"> 

         <?php  echo $this->session->flashdata('insertmessage');}

         ?>

      </div>

        <div class="xcrud">

          <div class="xcrud-container">

            <div class="xcrud-ajax">

              <div class="xcrud-list-container">

              <div id="container" class="container">

              <div class="col-md-8">                                 

              <form method="post" action="<?php if(!empty($view_site)){ echo site_url('dashboard/update_setting/'.$view_site[0]['id'].'');} else{echo site_url('dashboard/submit_setting');} ?>" name="data_register" enctype ="Multipart/form-data">

               <br />

			   

			   <div class="row form-group">

                <label class="col-md-2 control-label text-left">Site Name:</label>

                <div class="col-md-10">

                 <input type="text" name="name" value="<?php foreach($view_site as $data) { echo $data['name']; }?>" />

                </div>

                </div>

			   

			   <div class="row form-group">

                <label class="col-md-2 control-label text-left">currency symbol:</label>

                <div class="col-md-10">

                 <input type="text" name="symbol" value="<?php foreach($view_site as $data) { echo $data['currency_symbol']; }?>" />

                </div>

                </div>

				

                      

                <div class="row form-group">

                <label class="col-md-2 control-label text-left">Site Email:</label>

                <div class="col-md-10">

                 <input type="text" name="email" value="<?php foreach($view_site as $data) { echo $data['email']; }?>" />

                </div>

                </div>

                <div class="row form-group">

                <label class="col-md-2 control-label text-left">Site phone:</label>

                <div class="col-md-10">

                <input type="text" name="phone" value="<?php foreach($view_site as $data) { echo $data['phone']; }?>" />

                

                </div>

                </div>



                <div class="row form-group">

                <label class="col-md-2 control-label text-left">Site Address: </label>

                <div class="col-md-10">

                <input type="text" name="address" value="<?php foreach($view_site as $data) { echo $data['address']; }?>" />       

                </div>

                </div>



                <div class="row form-group">

                <label class="col-md-2 control-label text-left">Site Logo: </label>

                <div class="col-md-10">

                        



                <input type="file" name="image" id="image" />



                </div>

                </div>

                <button type="submit" class="btn btn-primary pull-right">Submit</button>

                </form>

                </div>

                <div class="col-md-4" >

                <img src="<?php echo base_url('uploads/').$view_site[0]['image'];?>" id="profile-img-tag" width="200px" style="    margin-top: 24px;width: 219px;margin-left: -64px;" />  

                </div>

              </div>

            </div>

           </div>



    <?php include('footer.php'); ?>

</body>

<script type="text/javascript">

     function readURL(input) {

        if (input.files && input.files[0]) {

        // alert("hello");

            var reader = new FileReader();

            console.log(reader);

            reader.onload = function (e) {

                $('#profile-img-tag').attr('src', e.target.result);

        $('#profile-img-tag').show();

            }

            reader.readAsDataURL(input.files[0]);

        }

    }

    $("#image").change(function(){

        readURL(this);

    });

</script>

</html>