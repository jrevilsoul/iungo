<!DOCTYPE html>

<head>

<?php include('headerandsidebar.php'); ?>

<div class="row">

  <div class="container" id="content">

    <div class="panel panel-default">

      <div class="panel-heading">Users Profile</div>

      <div class="panel-body">

        <div>

          <?php 

       if($this->session->flashdata('updatemessage')){

          ?>

       <div class="alert alert-success" style="center"> 

         <?php  echo $this->session->flashdata('updatemessage'); }



         elseif($this->session->flashdata('insertmessage')){?>

          <div class="alert alert-success"> 

         <?php  echo $this->session->flashdata('insertmessage');}

         ?>

      </div>

     

        <div class="xcrud">

          <div class="xcrud-container">

            <div class="xcrud-ajax">

              <div class="xcrud-list-container">

              <div id="container" class="container">

              <div class="col-md-11">                                 

              <form method="post" action="<?php echo site_url('dashboard/update_profile/').$view_team[0]['id']; ?>" name="data_register" enctype="multipart/form-data">

               <br />

			   

			   <div class="row form-group">

                <label class="col-md-2 control-label text-left">User Name:</label>

                <div class="col-md-10">

                 <input type="text" name="username" class="form-control" value="<?php echo $view_team[0]['username'];?>"/>

                </div>

                </div>

			   

                <div class="row form-group">

                <label class="col-md-2 control-label text-left">Email:</label>

                <div class="col-md-10">

                 <input type="text" name="email" class="form-control" value="<?php echo $view_team[0]['email'];?>" />

                </div>

                </div>

                <div class="row form-group">

                <label class="col-md-2 control-label text-left">Designation:</label>

                <div class="col-md-10">

                 <input type="text" name="designation" class="form-control" value="<?php echo $view_team[0]['designation'];?>"/>

                </div>

                </div>



                <div class="row form-group">

                <label class="col-md-2 control-label text-left">Description:</label>

                <div class="col-md-10">

                 <input type="text" name="description" class="form-control" value="<?php echo $view_team[0]['description'];?>"/>

                </div>

                </div>

               

                <div class="row form-group">

                <label class="col-md-2 control-label text-left">Facebook Link:</label>

                <div class="col-md-10">

                 <input type="text" name="f_link" class="form-control" value="<?php echo $view_team[0]['f_link'];?>" />

                </div>

                </div>

                <div class="row form-group">

                <label class="col-md-2 control-label text-left">Google Link:</label>

                <div class="col-md-10">

                 <input type="text" name="g_link" class="form-control" value="<?php echo $view_team[0]['g_link'];?>" />

                </div>

                </div>

                <div class="row form-group">

                <label class="col-md-2 control-label text-left">Instagram:</label>

                <div class="col-md-10">

                 <input type="text" name="i_link" class="form-control" value="<?php echo $view_team[0]['i_link'];?>" />

                </div>

                </div>

                <div class="row form-group">

                <label class="col-md-2 control-label text-left">Image:</label>

                <div class="col-md-10">

                 <input type="file" name="image" id="image"  value="" />

                 <img src="<?php echo base_url('uploads/').$view_team[0]['image'];?>" id="profile-img-tag" width="100px"/>

                </div>

                </div>

                <button type="submit" class="btn btn-primary pull-right">Submit</button>

                </form>

                </div>

              </div>

            </div>

           </div>



    <?php include('footer.php'); ?>

</body>

<script type="text/javascript">

     function readURL(input) {

        if (input.files && input.files[0]) {

        // alert("hello");

            var reader = new FileReader();

            console.log(reader);

            reader.onload = function (e) {

                $('#profile-img-tag').attr('src', e.target.result);

        $('#profile-img-tag').show();

            }

            reader.readAsDataURL(input.files[0]);

        }

    }

    $("#image").change(function(){

        readURL(this);

    });



</script>

</html>