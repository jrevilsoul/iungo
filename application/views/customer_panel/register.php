 <div role="tabpanel" class="tab-pane active" id="tab1">
                            <form>
                              <div class="col-md-6">
                                
                                <div class="form-group ">
                                  <label for="first_name">First Name</label>
                                  <input type="name" class="pr_edit form-control" id="first_name" disabled value="Chandan">
                                </div>
                                <div class="form-group ">
                                  <label for="last_name">Last Name</label>
                                  <input type="name" class="pr_edit form-control" id="last_name" disabled value="Pandey">
                                </div>
                              <div class="form-group"> 
                                <div class="">
                                  <div>
                                    <label for="gender">Gender</label>
                                  </div>                               
                                  <label class="radio-inline">
                                    <input type="radio" class="pr_edit" name="gender" id="male" value="male" checked disabled> Male
                                  </label>
                                  <label class="radio-inline">
                                    <input type="radio" class="pr_edit" name="gender" id="female" value="female" disabled> Female
                                  </label>
                                </div>
                              </div>
                                <div class="form-group ">
                                  <label for="email">Email Address</label>
                                  <input type="email" class="pr_edit form-control" id="email" disabled value="Chandan@gmail.com">
                                </div>
                                <div class="form-group ">
                                  <label for="mobile">Mobile No</label>
                                  <input type="number" class="pr_edit form-control" id="mobile" disabled value="9555341287">
                                </div>
                                <div id="sub_bt" style="display: none;">
                                  <button type="submit" class="btn btn-success">Submit</button>
                                </div>
                              </div>
                            </form>
                          <button class="btn btn-primary pull-right" id="edit">Edit</button>
                    </div>