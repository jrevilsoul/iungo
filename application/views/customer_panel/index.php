<!DOCTYPE html>

<html>

<head>

	<meta charset="utf-8">

	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<title></title>

	<link rel="stylesheet" href="<?php echo base_url('customer/');?>css/style.css"></link>

	<link rel="stylesheet" href="<?php echo base_url('customer/');?>css/bootstrap.css"></link>

	<link rel="stylesheet" href="<?php echo base_url('customer/');?>css/font-awesome.css"></link>

	<script src="js/jquery.js"></script>

	<!-- <script  src="https://code.jquery.com/jquery-2.2.4.min.js"

  integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="

  crossorigin="anonymous"></script> -->

	<script src="js/bootstrap.js"></script>

</head>

<body>

	<section class="user_dashboard" style="margin-top: 150px;">

  

<div class="dash_container">

  <div class="row">

    <div class="user_dash_head">

      <h1>Dashboard</h1>

      <hr>

    </div>

        <div role="tabpanel">

            <div class="col-sm-3">

                <ul class="nav nav-pills brand-pills nav-stacked" role="tablist">

                    <li role="presentation" class="brand-nav active"><a href="#tab1" aria-controls="tab1" role="tab" data-toggle="tab"><i class="fa fa-user"></i>&nbsp;&nbsp;Your Profile</a></li>

                    <li role="presentation" class="brand-nav" ><a href="#tab2" aria-controls="tab2" role="tab" data-toggle="tab"><i class="fa fa-list"></i>&nbsp;&nbsp;Your Products</a></li>

                    <li role="presentation" class="brand-nav "><a href="#tab3" aria-controls="tab3" role="tab" data-toggle="tab"><i class="fa fa-cog"></i>&nbsp;&nbsp;Buyer List</a></li>

                    <li role="presentation" class="brand-nav"><a href="#tab4" aria-controls="tab4" role="tab" data-toggle="tab"><i class="fa fa-google-wallet"></i>&nbsp;&nbsp;Wallet</a></li>

                    <li role="presentation" class="brand-nav"><a href="#tab5" aria-controls="tab5" role="tab" data-toggle="tab"><i class="fa fa-cog"></i>&nbsp;&nbsp;Setting</a></li>

                </ul>

            </div>

            <div class="col-sm-9">

                <div class="tab-content">

                    <div role="tabpanel" class="tab-pane active" id="tab1">

                            <form>

                              <div class="col-md-6">

                                

                                <div class="form-group ">

                                  <label for="first_name">First Name</label>

                                  <input type="name" class="pr_edit form-control" id="first_name" disabled value="Chandan">

                                </div>

                                <div class="form-group ">

                                  <label for="last_name">Last Name</label>

                                  <input type="name" class="pr_edit form-control" id="last_name" disabled value="Pandey">

                                </div>

                              <div class="form-group"> 

                                <div class="">

                                  <div>

                                    <label for="gender">Gender</label>

                                  </div>                               

                                  <label class="radio-inline">

                                    <input type="radio" class="pr_edit" name="gender" id="male" value="male" checked disabled> Male

                                  </label>

                                  <label class="radio-inline">

                                    <input type="radio" class="pr_edit" name="gender" id="female" value="female" disabled> Female

                                  </label>

                                </div>

                              </div>

                                <div class="form-group ">

                                  <label for="email">Email Address</label>

                                  <input type="email" class="pr_edit form-control" id="email" disabled value="Chandan@gmail.com">

                                </div>

                                <div class="form-group ">

                                  <label for="mobile">Mobile No</label>

                                  <input type="number" class="pr_edit form-control" id="mobile" disabled value="9555341287">

                                </div>

                                <div id="sub_bt" style="display: none;">

                                  <button type="submit" class="btn btn-success">Submit</button>

                                </div>

                              </div>

                            </form>

                          <button class="btn btn-primary pull-right" id="edit">Edit</button>

                    </div>

                    <div role="tabpanel" class="tab-pane" id="tab2">

                        <div class="tot_pro">

                          <span>Total Product : 3</span>

                          <a href="#" class="btn btn-danger btn-sm prod_bt" id="">Add New</a>

                        </div>

                          <div id="prod_list">

                              <div class="my_prod">

                                <div class="prod_img col-md-3">

                                  <img src="{{asset('images/2.jpg')}}"  alt="">

                                </div>

                                <div class="prod_detail col-md-6">

                                  <h3>Product Title</h3>

                                  <ul>

                                    <li>blah blah</li>

                                    <li>blah blah</li>

                                    <li>blah blah</li>

                                    <li>blah blah</li>

                                  </ul>



                                </div>

                                <div class="prod_action col-md-3">

                                  <p>

                                      <a href=""><i class="fa fa-pencil-square-o" style="color: green;"></i></a>

                                      <a href=""><i class="fa fa-trash" style="color: red;"></i></a>

                                  </p>

                                </div>

                              </div>

                              <div class="my_prod">

                                <div class="prod_img col-md-3">

                                  <img src="{{asset('images/2.jpg')}}"  alt="">

                                </div>

                                <div class="prod_detail col-md-6">

                                  <h3>Product Title</h3>

                                  <ul>

                                    <li>blah blah</li>

                                    <li>blah blah</li>

                                    <li>blah blah</li>

                                    <li>blah blah</li>

                                  </ul>



                                </div>

                                <div class="prod_action col-md-3">

                                  <p>

                                      <a href=""><i class="fa fa-pencil-square-o" style="color: green;"></i></a>

                                      <a href=""><i class="fa fa-trash" style="color: red;"></i></a>

                                  </p>

                                </div>

                              </div>

                              <div class="my_prod">

                                <div class="prod_img col-md-3">

                                  <img src="{{asset('images/2.jpg')}}"  alt="">

                                </div>

                                <div class="prod_detail col-md-6">

                                  <h3>Product Title</h3>

                                  <ul>

                                    <li>blah blah</li>

                                    <li>blah blah</li>

                                    <li>blah blah</li>

                                    <li>blah blah</li>

                                  </ul>



                                </div>

                                <div class="prod_action col-md-3">

                                  <p>

                                      <a href=""><i class="fa fa-pencil-square-o" style="color: green;"></i></a>

                                      <a href=""><i class="fa fa-trash" style="color: red;"></i></a>

                                  </p>

                                </div>

                              </div>

                          </div>

                          <div id="add_prod" style="display: none;">

                            <div class="col-md-6">

                              <form action="">

                                <div class="form-group ">

                                  <div>

                                    <label for="mobile">Choose a category *</label>

                                  </div>

                                  <select class="form-control" name="" id="">

                                    <option value="">category</option>

                                    <option value="">category</option>

                                    <option value="">category</option>

                                    <option value="">category</option>

                                  </select>

                                </div>

                                <div class="form-group">

                                  <input type="file" id="choose" multiple="multiple" />

                                </div>

                                <div class="form-group ">

                                  <div>

                                    <label for="mobile">Product Title *</label>

                                  </div>

                                  <input type="text" class="form-control" value="">

                                </div>

                                <div class="form-group ">

                                  <div>

                                    <label for="mobile">Your Price *</label>

                                  </div>

                                  <input type="text" class="form-control" value="">

                                </div>

                                <div class="form-group ">

                                  <div>

                                    <label for="mobile">Product Description *</label>

                                  </div>

                                  <textarea name="" id="" class="form-control"></textarea>  

                                </div>

                                <div class="form-group ">

                                  <div>

                                    <label for="mobile">Choose Your City *</label>

                                  </div>

                                  <select class="form-control" name="" id="">

                                    <option value="">--SELECT--</option>

                                    <option value="">Delhi</option>

                                    <option value="">Calcutta</option>

                                    <option value="">Banglore</option>

                                    <option value="">Mumbai</option>

                                  </select>

                                </div>

                                <div class="form-group ">

                                  <div>

                                    <label for="mobile">Pin Code *</label>

                                  </div>

                                  <input type="text" class="form-control" value="">

                                </div>

                                <button type="submit" class="btn btn-primary">Submit</button>

                              </form>

                            </div>

                            <div class="col-md-6">

                              <div id="uploadPreview"></div>

                            </div>

                          </div>

                    </div>

                    <div role="tabpanel" class="tab-pane" id="tab3">

                        <div class="buyer_table">

                          <table id="" class="table table-striped table-bordered">

                              <tbody>

                                <tr>

                                  <th>Name Of Buyer</th>

                                  <th>Produt Name</th>

                                  <th>Product Image</th>

                                  <th>Total item</th>

                                  <th>Product Description</th>

                                  <th>Total Payment</th>

                                </tr>

                                <tr>

                                  <td>Ajay</td>

                                  <td>Fastrack Watch</td>

                                  <td>Not available</td>

                                  <td>1</td>

                                  <td>Denim shade fastrack watch</td>

                                  <td>1599</td>

                                </tr>

                              </tbody> 

                          </table>

                        </div>

                    </div>

                    <div role="tabpanel" class="tab-pane" id="tab4">

                        <p>Coming Soon ...</p>

                    </div>

                    <div role="tabpanel" class="tab-pane" id="tab5">

                        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

                          <div class="panel panel-default">

                            <div class="panel-heading" role="tab" id="headingOne">

                              <h4 class="panel-title">

                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">Change Password</a>

                              </h4>

                            </div>

                            <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">

                              <div class="panel-body">

                                <form action="">

                                  <div class="col-md-6">

                                    <div class="form-group ">

                                      <div>

                                        <label for="mobile">Old Password *</label>

                                      </div>

                                      <input type="text" class="form-control" value="">

                                    </div>

                                    <div class="form-group ">

                                      <div>

                                        <label for="mobile">New Password *</label>

                                      </div>

                                      <input type="text" class="form-control" value="">

                                    </div>

                                    <div class="form-group ">

                                      <div>

                                        <label for="mobile">Confirm New Password *</label>

                                      </div>

                                      <input type="text" class="form-control" value="">

                                    </div>

                                    <button type="submit" class="btn btn-primary">Submit</button>

                                  </div>

                                </form>

                              </div>

                            </div>

                          </div>

                          <!-- <div class="panel panel-default">

                            <div class="panel-heading" role="tab" id="headingTwo">

                              <h4 class="panel-title">

                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">

                                  Email Notification

                                </a>

                              </h4>

                            </div>

                            <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">

                              <div class="panel-body">

                                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.

                              </div>

                            </div>

                          </div> -->

<!--                           <div class="panel panel-default">

                            <div class="panel-heading" role="tab" id="headingThree">

                              <h4 class="panel-title">

                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                  Delete Account

                                </a>

                              </h4>

                            </div>

                            <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">

                              <div class="panel-body text-center">

                                <a href="" class="btn btn-danger">Delete Account</a>

                              </div>

                            </div>

                          </div> -->

                        </div>

                    </div>

                </div>

            </div>

        </div>

  </div>

</div>

</section>

</body>

</html>