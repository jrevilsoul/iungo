
<!DOCTYPE html>
<html>
<head>
  <?php include('header.php');?>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title></title>
</head>
<style>
  .maps{
    box-shadow: 1px 0px 2px 1px #00000040;
    border: 1px solid #ccc;
    border-radius: 5px;
    padding: 0;
    margin-bottom: 25px;
  }
</style>
<body>
    <section class="user_dashboard" style="margin-top: 30px; margin-bottom: 100px;">
<div class="dash_container container-fluid">
  <div class="row">
    <div class="jumbotron" style="background: url('../front/images/bg.jpg');position: relative;">
      <div id="black_filter"></div>
      <h1 style="text-align: center;color: white;position: relative;z-index: 3;">We Are Here</h1>
      <!-- <p>...</p> -->
      <!-- <p><a class="btn btn-primary btn-lg" href="#" role="button">Learn more</a></p> -->
    </div>
    <div class="user_dash_head container" style="">
      <hr>
        <div class="panel-body">
        <?php //echo $aboutus[0]['content_description']; ?>
      </div>
      <div class="col-md-6">
        <h3 style="text-align: center;text-decoration: underline;">SEND MESSAGE</h3>
        <form  method="post" action="<?php echo base_url('Welcome/send_mail');?>">
          <div class="form-group">
            <label for="name">Name</label>
            <input type="text" class="form-control" id="name" name="username">
          </div>
          <div class="form-group">
            <label for="email">E-mail</label>
            <input type="email" class="form-control" id="email" name="email">
          </div>
          <div class="form-group">
            <label for="mobile">Mobile</label>
            <input type="text" class="form-control" id="mobile" name="mobile">
          </div>
          <div class="form-group">
            <label for="message">Message</label>
            <textarea name="message" id="message" class="form-control" cols="30" rows="10" style="resize: none;"></textarea>
          </div>
          <input type="submit" class="btn btn-success" value="Submit">
        </form>
      </div>
      <div class="col-md-6">
        <div class="col-md-12 maps">
          <div class="col-md-4">
            <h4>Delhi Office(HO):</h4>
            <address>
              A-55, FIEE Complex, <br>
              Okhla Phase II, <br>
              Delhi - 110020
            </address>
          </div>
          <div class="col-md-8">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3505.1646392699136!2d77.26726631434933!3d28.534770682456486!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390ce158cbee6c25%3A0x454b0d16e6d232b1!2sFIEE+Complex%2C+Ma+Anandmayee+Marg%2C+Block+A%2C+Okhla+Phase+II%2C+Okhla+Industrial+Area%2C+New+Delhi%2C+Delhi+110020!5e0!3m2!1sen!2sin!4v1515759225671" width="100%" height="200" frameborder="0" style="border:0" allowfullscreen></iframe>
          </div>
        </div>
        <div class="col-md-12 maps">
          <div class="col-md-4">
            <h4>Noida Office:</h4>
            <address>
              H 15, Office No.- 410<br>
              Sector 63, Noida<br>
              UP - 201301
            </address>
          </div>
          <div class="col-md-8">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d7004.115283245474!2d77.3732880725199!3d28.628034479677936!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390ce557e55ef211%3A0x814faac123187cb7!2sH+Block%2C+Sector+63%2C+Noida%2C+Uttar+Pradesh+201301!5e0!3m2!1sen!2sin!4v1515759685480" width="100%" height="200" frameborder="0" style="border:0" allowfullscreen></iframe>
          </div>
        </div>
        
        <div class="col-md-12 maps">
          <div class="col-md-4">
            <h4>Sikkim Office(HO):</h4>
            <address>
              Lama Building, <br>
              Arithang Near Arithang School , <br>
              East Sikkim , <br>
              Sikkim - 737101
            </address>
          </div>
          <div class="col-md-8">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2759.024420977827!2d88.61977141432676!3d27.364389982935187!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39e6bab0575ce7b3%3A0xd6811257e1eaecdf!2sMingma+Lama+Building!5e1!3m2!1sen!2sin!4v1516020019172" width="100%" height="200" frameborder="0" style="border:0" allowfullscreen></iframe>
          </div>
        </div>
        <div class="col-md-12 maps">
          <div class="col-md-4">
            <h4>Harayana Office(HO):</h4>
            <address>
              689, <br>
              Sector-6 , <br>
              PAnchkula - 134109 <br>
            </address>
          </div>
          <div class="col-md-8">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3430.4807125264974!2d76.85902921456513!3d30.704883594026466!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390f9372aff420b1%3A0xbe3cde4b14b91b6a!2s689%2C+Sector+6%2C+Panchkula%2C+Haryana+134109!5e0!3m2!1sen!2sin!4v1516621910258" width="100%" height="200" frameborder="0" style="border:0" allowfullscreen></iframe>
          </div>
        </div>
        <div class="col-md-12 maps">
          <div class="col-md-4">
            <h4>Dehradun  Office(HO):</h4>
            <address>
              75A, <br>
              Raj Plaza, Rajpur Road <br>
              Dehradun - 248009 <br>
            </address>
          </div>
          <div class="col-md-8">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3443.5309688958055!2d78.05183321455566!3d30.33585636170331!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390929d00b0c0831%3A0xa20968e20b7ba016!2sRaj+Plaza%2C+Rajpur+Rd%2C+Ravindrapuri%2C+Hathibarkala+Salwala%2C+Dehradun%2C+Uttarakhand+248001!5e0!3m2!1sen!2sin!4v1516621968946" width="100%" height="200" frameborder="0" style="border:0" allowfullscreen></iframe>
          </div>
        </div>
      </div>
   </div>   
  </div> 
</div>
</div>
</section>
</body>
</html>
<?php include('footer.php');?>