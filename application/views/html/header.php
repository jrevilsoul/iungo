<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>iungo</title>
<link rel="icon" href="<?php echo base_url('uploads/').$view_site[0]['image'];?>" type="image/gif" sizes="16x16">
<link href="<?php echo base_url('front/'); ?>css/bootstrap.css" rel="stylesheet" media="all" type="text/css"/>
<link href="<?php echo base_url('front/'); ?>css/style.css" rel="stylesheet" media="all" type="text/css"/>
<link href="<?php echo base_url('front/'); ?>css/font-awesome.css" rel="stylesheet" media="all" type="text/css"/>

<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.css"/>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick-theme.min.css"/>
<link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light" rel="stylesheet" type="text/css">
 <style>
   @media screen and (min-width: 768px) {
     nav.small {
      display: none;
     }
     nav.large {
       display: block;
     }
   }
   @media screen and (max-width: 768px) {
     nav.large {
      display: none;
     }
     nav.small {
       display: block;
     }
   }

   a:hover{
    text-decoration: none;
   }

 </style>
</head>
<script type="text/javascript">function add_chatinline(){var hccid=12059016;var nt=document.createElement("script");nt.async=true;nt.src="https://mylivechat.com/chatinline.aspx?hccid="+hccid;var ct=document.getElementsByTagName("script")[0];ct.parentNode.insertBefore(nt,ct);}
add_chatinline(); </script>
<body>
<header>
      
        <div class="header_top-bar" id="menu">
            <div class="top_bar" style="text-align: left;">
              <div class="container">
                <div class="col-md-4">
                  
                <span class="social">
                  <a href="">
                    <i class="fa fa-facebook"></i>
                  </a>
                </span>
                <span class="social">
                  <a href="">
                    <i class="fa fa-instagram"></i>
                  </a>
                </span>
                <span class="social">
                  <a href="">
                    <i class="fa fa-twitter"></i>
                  </a>
                </span>
                <span class="social">
                  <a href="">
                    <i class="fa fa-google-plus"></i>
                  </a>
                </span>
                </div>
                <div class="login">
                  <?php
                if($this->session->userdata('email')){
                  ?>
                <a class="" href="<?php echo base_url('welcome/dashboard_profile/#tab1');?>"><i class="fa fa-user"></i>&nbsp;Users Profile</a>
                <a class="" href="<?php echo base_url('welcome/logout_users');?>"> <i class="fa fa-sign-out"></i> &nbsp;Logout</a>
                <?php 
                }
                else
                {
                ?>
                <a class="" href="<?php echo base_url('menu/login');?>"><i class="fa fa-user"></i>&nbsp;Customer Login</a>
                <?php 
                }
                ?> 
                </div>
              </div>
              
            </div>
          <div class="container" style="height: 130px;">
            <div class="brand col-md-2">
              <h1 class="brand_name">
                <a href="./"><img src="<?php echo base_url('uploads/').$view_site[0]['image'];?>" width="150" height="100"></a>
              </h1>
              <p class="brand_slogan">
              </p>
            </div>
            <div id="nav_link">
              <a href="<?php echo base_url('welcome');?>" class="menuitem">Home</a>
              <a href="<?php echo base_url('menu/aboutus');?>" class="menuitem">About</a>
              <a href="<?php echo base_url('menu/Services');?>" class="menuitem">Services</a>
              <a href="<?php echo base_url('menu/plans');?>" class="menuitem">Plans</a>
              <a href="<?php echo base_url('menu/ourcustomer');?>" class="menuitem">Our Customers</a>
              <a href="<?php echo base_url('menu/tenders');?>" class="menuitem">Tenders</a>
              <!-- <a href="#mission_content" class="menuitem">Clients</a> -->
              <a href="<?php echo base_url('menu/contactus');?>" class="menuitem">Contact</a>
              <a href="<?php echo base_url('menu/direct_pay_detail');?>" class="menuitem">Pay Online</a>
            </div>
          <div class="call-us">
              <p>Call - Us<br>78-40-084-710</p>
            </div>
          </div>
        </div>
 <div id="mybutton">
  <a href="<?php echo base_url('menu/Buy_plan') ?>" class="feedback">Buy Plan</a>
</div>

  </header>
<style>
.feedback {
    background-color: #ee864a;
    color: white;
    padding: 14px 23px;
    border-radius: 4px;
    border-color: #ee864a;
}
#mybutton {
    position: fixed;
    bottom: 50%;
    right: -33px;
    transform: rotate(90deg);
    z-index: 999;
}
  
 .call-us {
      text-align: center;
    display: inline-block;
    float: right;
    font-family: 'Tangerine', cursive;
    font-weight: 700;
    font-size: 12px;
    line-height: 18px;
    background: #009dd5;
    margin-top: 0;
    margin-bottom: -20px;
    margin-left: 0;
    -moz-transform: rotate(340deg);
    -ms-transform: rotate(340deg);
    -o-transform: rotate(340deg);
    -webkit-transform: rotate(340deg);
    transform: rotate(324deg);
    writing-mode: lr-tb;
    color: white;
    z-index: 999;
    position: relative;
    box-shadow: 3px 4px 6px 0px #c7bdbd;
}
 .call-us p {
  background: #009dd5;
  padding: 30px 16px;
  padding-bottom: 20px;
}
 .call-us p:after {
  -moz-transform: rotate(-5deg);
  -ms-transform: rotate(-5deg);
  -o-transform: rotate(-5deg);
  -webkit-transform: rotate(-5deg);
  transform: rotate(-5deg);
  top: 100%;
  margin-top: -13px;
  height: 10px;
  width: 90%;
  z-index: -1;
}
@media (max-width: 767px) {
   {
    text-align: center;
  }
   .call-us {
    float: none;
    margin: 20px 0;
  }
}



.top_bar{
  height: 35px;
    background: #47b053;
}
.social{
  padding: 4px;
}
.social .fa {
  width: 32px;
  height: 30px;
  padding: 8px;
  text-align: center;
  border-radius: 50%;
  margin-top: 2px;
  color: white;
}
.social .fa-facebook {
    background: #009dd5;
    
}
.social .fa-instagram {
    background: #fb3958;
}
.social .fa-twitter {
    background: #55acee;
}
.social .fa-google-plus {
    background: #dd4b39;
}
.login{
  float: right;
  margin-top: 7px;
    
}
.login a {
  font-size: 15px;
    padding:4px;
    padding-right: 15px;
    padding-left: 15px;
    background: #009cd3;
    margin-top: 2px;
    border-radius: 3px;
    color: white;
}

  #menu{
  text-align: center;
  color: black;
  border-bottom: 1px solid #ccc;
}
.menuitem{
  padding: 14px;
    cursor: pointer;
    text-decoration: none;
    font-family: initial;
    font-size: 19px;
    color: black
}
.menuitem:hover{
    background-color: #3aa24c;
    color: white !important;
    border-radius: 3px;
}
#nav_link{
  margin-top: 50px;
}
</style>