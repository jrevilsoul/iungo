<!DOCTYPE html>
<html>
<head>
  <?php include('header.php');?>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title></title>
</head>
<body>
   <div class="customized-template" id="customers" style="padding-bottom: 50px;">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                
                <div class="center">
                    <div class="leyered-section">
                        <div class="leyered-heading">
                        <h2 style="text-align: center; color: black;">Customer Login</h2>
                        <div class="circle-icon">
                            <div class="circle">
                                
                            </div>
                            
                        </div>



<div id="" class="" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
    <?php 
       if($this->session->flashdata('msg')){
        echo $this->session->flashdata('msg'); 
       
       }?>
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Customer Login</h4>
      </div>
      <div class="modal-body">
        <form method="post" action="<?php echo base_url('welcome/login_users');?>">
      <div class="form-group">
        <label for="email">Email address:</label>
        <input type="email"  name="email" class="form-control" required>
      </div>
      <div class="form-group">
        <label for="pwd">Password:</label>
        <input type="password" name="password" class="form-control" id="pwd">
      </div>
      <div class="checkbox">
        <label><input type="checkbox">Remember me ? Not A Member <a href="<?php echo base_url('menu/register');?>">Sign Up Here</a></label>
      </div>
      <button type="submit" class="btn btn-default">Submit</button>
    </form>
          </div>
    </div>

  </div>
</div>


                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
<?php include('footer.php');?>