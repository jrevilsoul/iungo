
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Home</title>
  <meta charset="utf-8">
  <meta name="format-detection" content="telephone=no"/>
  <link rel="icon" href="images/favicon.ico" type="image/x-icon">
  <link rel="stylesheet" href="<?php echo base_url('front/'); ?>newweb/css/grid.css">
  <link rel="stylesheet" href="<?php echo base_url('front/'); ?>newweb/css/style.css">
  <link rel="stylesheet" href="<?php echo base_url('front/'); ?>newweb/css/jquery.fancybox.css">
  <link rel="stylesheet" href="<?php echo base_url('front/'); ?>newweb/css/contact-form.css">
</head>

<body>
  <style>
    #menu{
  text-align: center;
  color: #fff;
}
.menuitem{
  padding: 14px;
    cursor: pointer;
    text-decoration: none;
    font-family: monospace;
    font-size: 19px;
}
.menuitem:hover{
    background-color: #3aa24c;
    color: white !important;
    border-radius: 3px;
}
#nav_link{
  margin-top: 30px;
}
  
  #slider{
  width:100%;
  height:500px;
  position:relative;
  overflow:hidden;
  /*padding-top: 85px;*/
}
@keyframes load{
  from{left:-100%;}
  to{left:0;}
}
.slides{
  width:400%;
  height:100%;
  position:relative;
  -webkit-animation:slide 15s infinite;
  -moz-animation:slide 15s infinite;
  animation:slide 15s infinite;
}
.slider{
  width:25%;
  height:100%;
  float:left;
  position:relative;
  z-index:1;
  overflow:hidden;
}
.slide img{
  width:100%;
  height:100%;
}
.slide img{
  width:100%;
  height:100%;
}
.image{
  width:100%;
  height:100%;
}
.image img{
  width:100%;
  height:100%;
}

/* Legend */
.legend{
  border:500px solid transparent;
  border-left:800px solid rgba(52, 73, 94, .7);
  border-bottom:0;
  position:absolute;
  bottom:0;
}

/* Contents */
.content{
  width:100%;
  height:100%;
  position:absolute;
  overflow:hidden;
}
.content-txt{
      width: 500px;
    height: 150px;
    float: left;
    position: relative;
    top: 170px;
    margin-left: 70px;
    -webkit-animation: content-s 7.5s infinite;
    -moz-animation: content-s 7.5s infinite;
    animation: content-s 7.5s infinite;
}
.content-txt h1{
  font-family: Arial;
    text-transform: uppercase;
    font-size: 30px;
    color: #fff;
    text-align: left;
    margin-left: 10px;
    /* padding-bottom: 10px; */
    font-weight: 600;
}
.content-txt h2{
      font-family: monospace;
    font-weight: normal;
    font-size: 50px;
    font-style: italic;
    color: #fff;
    text-align: left;
    margin-left: 30px;
}

/* Switch */
.switch{
  width:120px;
  height:10px;
  position:absolute;
  bottom:50px;
  z-index:99;
  left:30px;
}
.switch > ul{
  list-style:none;
}
.switch > ul > li{
  width:10px;
  height:10px;
  border-radius:50%;
  background:#333;
  float:left;
  margin-right:5px;
  cursor:pointer;
}
.switch ul{
  overflow:hidden;
}
.on{
  width:100%;
  height:100%;
  border-radius:50%;
  background:#f39c12;
  position:relative;
  -webkit-animation:on 15s infinite;
  -moz-animation:on 15s infinite;
  animation:on 15s infinite;
}

/* Animation */
@-webkit-keyframes slide{
  0%,100%{
    margin-left:0%;
  }
  21%{
    margin-left:0%;
  }
  25%{
    margin-left:-100%;
  }
  46%{
    margin-left:-100%;
  }
  50%{
    margin-left:-200%;
  }
  71%{
    margin-left:-200%;
  }
  75%{
    margin-left:-300%;
  }
  96%{
    margin-left:-300%;
  }
}
@-moz-keyframes slide{
  0%,100%{
    margin-left:0%;
  }
  21%{
    margin-left:0%;
  }
  25%{
    margin-left:-100%;
  }
  46%{
    margin-left:-100%;
  }
  50%{
    margin-left:-200%;
  }
  71%{
    margin-left:-200%;
  }
  75%{
    margin-left:-300%;
  }
  96%{
    margin-left:-300%;
  }
}
@keyframes slide{
  0%,100%{
    margin-left:0%;
  }
  21%{
    margin-left:0%;
  }
  25%{
    margin-left:-100%;
  }
  46%{
    margin-left:-100%;
  }
  50%{
    margin-left:-200%;
  }
  71%{
    margin-left:-200%;
  }
  75%{
    margin-left:-300%;
  }
  96%{
    margin-left:-300%;
  }
}

@-webkit-keyframes content-s{
  0%{left:-620px;}
  10%{left:0px;}
  30%{left:0px;}
  40%{left:0px;}
  50%{left:0px;}
  60%{left:0px;}
  70%{left:0;}
  80%{left:-620px;}
  90%{left:-620px;}
  100%{left:-620px;}
}
@-moz-keyframes content-s{
  0%{left:-620px;}
  10%{left:0px;}
  30%{left:0px;}
  40%{left:0px;}
  50%{left:0px;}
  60%{left:0px;}
  70%{left:0;}
  80%{left:-620px;}
  90%{left:-620px;}
  100%{left:-620px;}
}
@keyframes content-s{
  0%{left:-620px;}
  10%{left:20px;}
  15%{left:0px;}
  30%{left:0px;}
  40%{left:0px;}
  50%{left:0px;}
  60%{left:0px;}
  70%{left:0;}
  80%{left:-620px;}
  90%{left:-620px;}
  100%{left:-620px;}
}

@-webkit-keyframes on{
  0%,100%{
    margin-left:0%;
  }
  21%{
    margin-left:0%;
  }
  25%{
    margin-left:15px;
  }
  46%{
    margin-left:15px;
  }
  50%{
    margin-left:30px;
  }
  71%{
    margin-left:30px;
  }
  75%{
    margin-left:45px;
  }
  96%{
    margin-left:45px;
  }
}

@-moz-keyframes on{
  0%,100%{
    margin-left:0%;
  }
  21%{
    margin-left:0%;
  }
  25%{
    margin-left:15px;
  }
  46%{
    margin-left:15px;
  }
  50%{
    margin-left:30px;
  }
  71%{
    margin-left:30px;
  }
  75%{
    margin-left:45px;
  }
  96%{
    margin-left:45px;
  }
}

@keyframes on{
  0%,100%{
    margin-left:0%;
  }
  21%{
    margin-left:0%;
  }
  25%{
    margin-left:15px;
  }
  46%{
    margin-left:15px;
  }
  50%{
    margin-left:30px;
  }
  71%{
    margin-left:30px;
  }
  75%{
    margin-left:45px;
  }
  96%{
    margin-left:45px;
  }
}

  </style>
<div class="page">
  <!--========================================================
                            HEADER
  =========================================================-->
  <header>
      
        <div class="header_top-bar" id="menu">
          <div class="container">
            <div class="brand">
              <h1 class="brand_name">
                <a href="./"><img src="<?php echo base_url('uploads/').$view_site[0]['image'];?>" width="100" height="75"></a>
              </h1>
              <p class="brand_slogan">
              </p>
            </div>
            <div id="nav_link">
              <a href="#home_content" class="menuitem">Home</a>
              <a href="#about_content" class="menuitem">About</a>
              <a href="#mission_content" class="menuitem">Services</a>
              <a href="#mission_content" class="menuitem">Plans</a>
              <a href="#mission_content" class="menuitem">Tenders</a>
              <!-- <a href="#mission_content" class="menuitem">Clients</a> -->
              <a href="#contact_content" class="menuitem">Contact</a>
            </div>
          <div class="call-us">
              <p>Call<br>123-456-789</p>
            </div>
          </div>
        </div>
        <div id="slider">
          <div class="slides">
            <div class="slider">
              <div class="content">
                <div class="content-txt">
                  <h1>We Connect You</h1>
                  <h2>EveryWhere!!!</h2>
                </div>
              </div>
              <div class="image">
                <img src="<?php echo base_url('front/'); ?>newweb/images/main_slide5.jpg">
              </div>
            </div>
            <div class="slider">
              <div class="content">
                <div class="content-txt">
                  <h1><a href="" class="btn btn-primary">Contact Us</a></h1>
                  <h2>… and You’re Done!!!</h2>
                </div>
              </div>
              <div class="image">
                <img src="<?php echo base_url('front/'); ?>newweb/images/main_slide.jpg">
              </div>
            </div>
            <div class="slider">
             <!--  <div class="content">
                <div class="content-txt">
                  <h1>Lorem ipsum dolor</h1>
                  <h2>Nam ultrices pellentesque facilisis. In semper tellus mollis nisl pulvinar vitae vulputate lorem consequat. Fusce odio tortor, pretium sit amet auctor ut, ultrices vel nibh.</h2>
                </div>
              </div>
 -->              <div class="image">
                <img src="<?php echo base_url('front/'); ?>newweb/images/main_slide4.jpg">
              </div>
            </div>
            <div class="slider">
              <div class="content">
                <div class="content-txt">
                  <h1>Cut The Cable , And Go</h1>
                  <h2>WireLess!!!</h2>
                </div>
              </div>
              <div class="image">
                <img src="<?php echo base_url('front/'); ?>newweb/images/main_slide3.jpg">
              </div>
            </div>
          </div>
          <div class="switch">
            <ul>
              <li>
                <div class="on"></div>
              </li>
              <li></li>
              <li></li>
              <li></li>
            </ul>
          </div>
        </div>
  </header>

  <!--========================================================
                            CONTENT
  =========================================================-->
  <main>
    <section class="well bg2 cnt">
      <div class="container">
        <h2>How Easy It Is!!</h2>
        <ul class="list">
            <li>
                <span class="icon icon-internet-7"></span><a href="#">Choose Your Plan</a>
            </li>
            <li>
                <span class="icon icon-internet-5"></span><a href="#">Book Your Plan</a>
            </li>    
            <li>
                <span class="icon icon-internet-3"></span><a href="#">Buy Your Plan</a>
            </li>    
            <li>
                <span class="icon icon-internet-8"></span><a href="#">Enjoy Our Service</a>
            </li>
        </ul>
      </div>
    </section>

    <section class="well-2 bg-4 cnt2">
      <div class="container">
        <div class="section-header">
          <span class="icon2 icon-internet-2"></span>
          <h2>benefits</h2>
        </div>
        <div class="row">
          <div class="grid_6">            
            <div class="grid_1">
              <span><i class="fa fa-bolt" style="font-size: 70px; margin-top: 30px;"></i></span>
            </div>
            <div class="grid_4">
              <h2 style="font-size: 35px;">High Speed</h2>
              <p>We Provide You High Speed</p>
            </div>
            
          </div>
          <div class="grid_6">            
            <div class="grid_1">
              <img src="<?php echo base_url('front/'); ?>/newweb/images/handshake.png" alt="">
            </div>
            <div class="grid_4">
              <h2 style="font-size: 35px;">Commitement</h2>
              <p>We Provide You High Speed</p>
            </div>
            
          </div>
          <div class="grid_12" style="height: 100px;"></div>
          <div class="grid_6">            
            <div class="grid_1">
              <span><i class="fa fa-reply" style="font-size: 50px; margin-top: 30px;"></i></span>
            </div>
            <div class="grid_4">
              <h2 style="font-size: 35px;">Quick Response</h2>
              <p>We Provide You High Speed</p>
            </div>
            
          </div>
          <div class="grid_6">            
            <div class="grid_1">
             <img src="<?php echo base_url('front/'); ?>/newweb/images/customer-service.png" alt="">
            </div>
            <div class="grid_4">
              <h2 style="font-size: 35px;">24/7 Care</h2>
              <p>We Provide You High Speed</p>
            </div>
            
          </div>
        </div>
      </div>
    </section>

    <section class="well-3 bg3">
      <div class="container">
        <div class="section-header __mod">

          <span class="icon2 icon-internet-1"><img style="width: 80px; margin-top: 30px;" src="<?php echo base_url('front/'); ?>/newweb/images/shield.png" alt=""></span>
          <h2>a few words about us</h2>
        </div>
        <div class="row">
          <div class="grid_6">
            <div class="box">
              <div class="box_aside">
                <div class="icon3 icon-internet-4"></div>
              </div>
              <div class="box_cnt">
                <p class="who">Sam Adams</p>
                <p>Fusce euismod consequat ante. Lorem ipsum dolor sit amet, con sectetuer adipiscing elit. Pellentesque sed dolor. Aliquam congue fermentum nisl. Mauris accumsan nulla.</p>
              </div>
            </div>
          </div>
          <div class="grid_6">
            <div class="box">
              <div class="box_aside">
                <div class="icon3 icon-internet-4"></div>
              </div>
              <div class="box_cnt">
                <p class="who">Ivana Wong</p>
                <p>Fusce euismod consequat ante. Lorem ipsum dolor sit amet, con sectetuer adipiscing elit. Pellentesque sed dolor. Aliquam congue fermentum nisl. Mauris accumsan nulla.</p>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="grid_6">
            <div class="box">
              <div class="box_aside">
                <div class="icon3 icon-internet-4"></div>
              </div>
              <div class="box_cnt">
                <p class="who">Laura Stegner</p>
                <p>Fusce euismod consequat ante. Lorem ipsum dolor sit amet, con sectetuer adipiscing elit. Pellentesque sed dolor. Aliquam congue fermentum nisl. Mauris accumsan nulla.</p>
              </div>
            </div>
          </div>
          <div class="grid_6">
            <div class="box">
              <div class="box_aside">
                <div class="icon3 icon-internet-4"></div>
              </div>
              <div class="box_cnt">
                <p class="who">Mark Johnson</p>
                <p>Fusce euismod consequat ante. Lorem ipsum dolor sit amet, con sectetuer adipiscing elit. Pellentesque sed dolor. Aliquam congue fermentum nisl. Mauris accumsan nulla.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section class="well-4">
      <div class="container">
        <div class="section-header __mod2">
          <span class="icon2 icon-internet-6"><img style="width: 80px; margin-top: 30px;" src="<?php echo base_url('front/'); ?>/newweb/images/work-briefcase.png" alt=""></span>
          <h2>our clients</h2>
        </div>
        <ul class="clients-list">
          <li><a href="#"><img src="<?php echo base_url('front/'); ?>newweb/images/Untitled-1.png" alt=""></a></li>
          <li><a href="#"><img src="<?php echo base_url('front/'); ?>newweb/images/Untitled-2.png" alt=""></a></li>
          <li><a href="#"><img src="<?php echo base_url('front/'); ?>newweb/images/Untitled-3.png" alt=""></a></li>
          <li><a href="#"><img src="<?php echo base_url('front/'); ?>newweb/images/Untitled-4.png" alt=""></a></li>
        </ul>
      </div>
    </section>

    <section class="well-5 bg-secondary">
      <div class="container">
        <div class="section-header __mod3">
          <span class="icon2 icon-internet-9"><img style="width: 80px; margin-top: 30px;" src="<?php echo base_url('front/'); ?>/newweb/images/contract.png" alt=""></span>
          <h2>contact us</h2>
        </div>

        <form id="contact-form" class='contact-form'>
          <div class="contact-form-loader"></div>
          <fieldset>
            <div class="row">
              <div class="grid_4">
                <label class="name">
                  <input type="text" name="name" placeholder="Name:" value="" data-constraints="@Required @JustLetters"/>
                  <span class="empty-message">*This field is required.</span>
                  <span class="error-message">*This is not a valid name.</span>
                </label>
            
                <label class="email">
                  <input type="text" name="email" placeholder="Email:" value="" data-constraints="@Required @Email"/>
                  
                  <span class="empty-message">*This field is required.</span>
                  <span class="error-message">*This is not a valid email.</span>
                </label>
            
                <label class="phone">
                  <input type="text" name="phone" placeholder="Phone:" value="" data-constraints="@JustNumbers"/>
            
                  <span class="empty-message">*This field is required.</span>
                  <span class="error-message">*This is not a valid phone.</span>
                </label>
              </div>
              <div class="grid_8">
                <label class="message">
                  <textarea name="message" placeholder="Message:" data-constraints='@Required @Length(min=20,max=999999)'></textarea>
            
                  <span class="empty-message">*This field is required.</span>
                  <span class="error-message">*The message is too short.</span>
                </label>
              </div>
              
            </div>
            <div class="btn-wr">
              <a class="btn" href="#" data-type="submit">send message</a>
            </div>
          </fieldset>
          <div class="modal fade response-message">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                  </button>
                  <h4 class="modal-title">Modal title</h4>
                </div>
                <div class="modal-body">
                  You message has been sent! We will be in touch soon.
                </div>
              </div>
            </div>
          </div>
        </form>
        
      </div>
    </section>
  </main>

  <!--========================================================
                            FOOTER
  =========================================================-->
 <footer style="background: #29353e; padding-bottom: 0;">
  <div class="container">
    <div class="grid_3" style="margin-top: 30px; margin-bottom: 30px;">
      <h4 style="color: white; margin-bottom: 15px; font-size: 28px;">Quick Links</h4>
      <p><a href="" style="text-decoration: none; font-size: 16px; color: white;">About Us</a></p>
      <p><a href="" style="text-decoration: none; font-size: 16px; color: white;">Plan</a></p>
      <p><a href="" style="text-decoration: none; font-size: 16px; color: white;">Services</a></p>
      <p><a href="" style="text-decoration: none; font-size: 16px; color: white;">Tender</a></p>
      <p><a href="" style="text-decoration: none; font-size: 16px; color: white;">Contact Us</a></p>
    </div>
    <div class="grid_3" style="margin-top: 30px; margin-bottom: 30px; color: white;">
      <h4 style="color: white; margin-bottom: 15px; font-size: 28px;">Contact Us</h4>
      <p>Delhi office:A-55, FIEE Complex, Okhla Phase II, Delhi - 110020</p>
      <hr>
      <p>Noida Office: H 15, Office No.- 410 Sector 63, Noida UP - 201301</p>
    </div>
    <div class="grid_3" style="color: white; margin-top: 30px; margin-bottom: 30px;">
      <h4>Reach Us</h4>
      <p>Call us on Delhi: 011-66241100 , 7840084710</p>
      <p>Email:info@iungo.in</p>
      <hr>
    </div>
    <div class="grid_2" style="margin-top: 30px; margin-bottom: 30px; text-align: center;">
        <img src="<?php echo base_url('uploads/').$view_site[0]['image'];?>" alt="" style="width: 200px; height: 150px;">
      <div class="social_link" style="color: white; padding-left: 10px;">
        <i style="background: #2b87c7;" class="fa fa-facebook"></i>
        <i style="background:#38c0dd; " class="fa fa-twitter"></i>
        <i style="background: #2862d8;" class="fa fa-linkedin"></i>
        <i style="background: #dc6218; " class="fa fa-google-plus" ></i>
      </div>
      <style>
        .social_link i{
          font-size: 20px;
          padding: 0px;
          /* background: white; */
          color: white;
          border-radius: 50%;
          margin: 3px;
          width: 30px;
          height: 30px;
        }
      </style>
    </div>
  </div>
<div class="footer_end" style="height: 100px; background: #1c2329;">
  <div class="container">
    <div class="grid_5">
      <p style="margin-top: 40px; color: #8e9194;"> © Copyright 2017 iungo.in Designed By Backstage Supporters Pvt Ltd.</p>
    </div>
    <div class="grid_6">
      <p style="margin-top: 40px; color: #8e9194; float: right;"><a style="text-decoration: none; color: #8e9194;" href="">Terms of Service |</a><a style="text-decoration: none; color: #8e9194;" href=""> Privacy Policy  </a><a style="text-decoration: none; color: #8e9194;" href="">| Site Map</a></p>
    </div>
  </div>
</div>
</footer>  
</div>

</html>