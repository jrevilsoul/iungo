
<!DOCTYPE html>
<html>
<head>
  <?php include('header.php');?>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title></title>
</head>
<body>
    <section class="user_dashboard" style="margin-top: 30px; margin-bottom: 100px;">
<div class="dash_container container-fluid">
  <div class="row">
    <div class="jumbotron" style="background: url('../front/images/bg.jpg');position: relative;">
      <div id="black_filter"></div>
      <h1 style="text-align: center;color: white;position: relative;z-index: 3;">Who We Are</h1>
      <!-- <p>...</p> -->
      <!-- <p><a class="btn btn-primary btn-lg" href="#" role="button">Learn more</a></p> -->
    </div>
    <div class="user_dash_head container" style="">
      
        <div class="panel-body">
          <!-- <h1 style="text-align: center;">Who We Are</h1> -->
          <hr>
          <p>IUNGO Entertainment House Private Limited is a category A Internet Service Provider licensed by the Department of Telecommunication catering high-technology driven and an extensive range of Internet and other Value Added services at affordable prices while maintaining the highest Quality of Service.

Set up in the year 2006, the Company initiated its operations in the field of International data and satellite solutions (SCPC, MCPC and VSAT) Broadcast Signal Distribution and Public Data Network Services.

The Company started its internet operations in the Country with the brand name of “IUNGO” in the year 2014 and has been catering high speed Internet and networking requirements through various state-of-the-art delivery platforms to residential, SME and corporate customers since then. The Company is solely focused on delivering High Speed Internet & Internet Solutions to its clients.

Besides providing Internet/Intranet and Data Center Solutions, IUNGO is specialized in giving optimized custom Wi-Fi solutions and Managed Services to Corporate, Hospitality Industry, Educational Institutions, Residential Societies, Shopping Malls etc. In this era of global Internet revolution, IUNGO has ambitiously diversified into IT and intends to carve a strong presence in the country as a premier Internet Service Provider.

IUNGO provides highly efficient 24/7 technical support 365 days a year through online chat, telephone and email. Care is taken to deliver within a quick turnaround time to your queries. Our services ensure that our customers shall only “Pay for what they use”.

IUNGO has a core “Professional Services Group” to provide system integration, network support, security, application management and facility management services to its Customers.

Currently IUNGO is in process of signing business associates who could even be end customer and simple broadband users with its unique IUNGO REFERRAL PROGRAM</p>
        <?php //echo $aboutus[0]['content_description']; ?>
      </div>
      <div class="vision_mission" style="">
        <div class="vision_wrap col-md-6">
          <h3 style="text-align: center;">Our Mission</h3>
          <p>
            <ul>
              <li>To revolutionize broadband services in India by providing simple-to-understand yet state-of-the-art services that will enable people of our country to do so much more in their daily lives.</li>
              <li>To offer a working environment with strong focus on performance and deliver a standard of service that goes beyond costumer expectations.</li>
              <li>To empower the rural community and to bring about social change through modern technologies.</li>
              <li>To create Rings of Smart Villages around 100 metro cities</li>
              <li>To provide upto 100 Mbps of Broadband connectivity upto every user.</li>
              <li>To provide B2B services in a non-discriminatory manner.</li>
              <li>To become India’s No one fiber and Satellite internet service provider.</li>
            </ul>
          </p>
        </div>
        <div class="vision_wrap col-md-6">
          <h3 style="text-align: center;">Our Vision</h3>
          <p>IUNGO pledges to ensure that our clients always receive the best Service Solutions to meet their specific requirements at the best possible prices</p>

          <p>We develop innovative products &amp; solutions that would help customers with enhanced user experience and hence becoming the most preferred technology integrator and serve institutional and corporate with multi-platform, vendor independent information and communication technology solution.</p>

          <p>Hence Our mission is therefore to provide for More Speed, More Entertainment, More Productivity, More Time, More Fun, More Flexibility and Overall More value.</p>
        </div>
      </div>
   </div>   
  </div> 
</div>
</div>
</section>
</body>
</html>
<?php include('footer.php');?>