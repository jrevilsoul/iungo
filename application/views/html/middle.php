
<section class="slidder">
    <ul class="bxslider slidder-ul">
  <li><img src="<?php echo base_url('front/'); ?>images/s3.jpg" class="img-responsive"/>
    <div class="bx-captionslidder">
        <p>Image gallery with captions</p>
    </div>
  </li>
  <li><img src="<?php echo base_url('front/'); ?>images/foodservice.jpg" class="img-responsive"/>
  <div class="bx-captionslidder">
        <p>Image gallery with captions</p>
    </div>
  </li>
   <li><img src="<?php echo base_url('front/'); ?>images/s4.jpg" class="img-responsive" />
   <div class="bx-captionslidder">
        <p>Image gallery with captions</p>
    </div>
   </li>
  

</ul>
</section>
<div class="clearfix"></div>
<div class="customized-template">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                
                <div class="center">
                    <div class="leyered-section">
                        <div class="leyered-heading">
                        <h3> We are fully committed to integrating customer driven quality standards</h3>
                        <h6> Our Services</h6>
                        <div class="circle-icon">
                            <div class="circle">
                                
                            </div>
                            
                        </div>
                        <article class="service-content">
                            <p>We are fully committed to integrating customer driven quality standards with superior innovation. O
                            ur abiding Endeavour is to provide customized software solutions to suit all your business requirements and with the best
                             returns on investment. Our comprehensive portfolio of products and services includes, but is not limited to, website development, 
                             software solutions. 
                            All our processes are geared towards producing the best products and services in terms of quality, functionality, and affordability.</p>
                            </article>
                        </div>
                        <div class="four-boxServiceDescription">
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="box-1">
                                <div class="heading-center">
                                    <div class="circle-heading">
                                    <i class="fa fa-bell"></i>
                                    </div>
                                    </div>
                                    <div class="service-heading">
                                    <h5>We are fully committed </h5>
                                    <p>We are fully committed to integrating</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="box-1">
                                  <div class="heading-center">
                                    <div class="circle-heading">
                                    <i class="fa  fa-cab "></i>
                                    </div>
                                    </div>
                                    <div class="service-heading">
                                    <h5>Software Solutions </h5>
                                    <p>We are fully committed to integrating</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="box-1">
                                  
                                <div class="heading-center">
                                    <div class="circle-heading">
                                    <i class="fa  fa-asterisk"></i>
                                    </div>
                                    </div>
                                    <div class="service-heading">
                                    <h5>quality standards <h5>
                                    <p>We are fully committed to integrating</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="box-1">
                                  <div class="heading-center">
                                    <div class="circle-heading">
                                    <i class="fa fa-bolt"></i>
                                    </div>
                                    </div>
                                    <div class="service-heading">
                                    <h5>Team of expert </h5>
                                    <p>We are fully committed to integrating</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
<div class="design">
    <div class="overlay"></div>
    <div class="content">
        <p>
Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when        </p>
<p> Lorem Ipsum</p>
    </div>
    
</div>
<div class="clearfix"></div>
<div class="modern-class">
    <div class="container">
        <div class="row">
            <div class="center">
                <div class="modern-heading">
                <h4> Lorem Ipsum is simply dummy text</h4>
                <h6>Our Service</h6>
                <div class="circle-icon">
                            <div class="circle">
                                
                            </div>
                            
                        </div>
                        <p>
                       Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 
                        </p>
                </div>
               <div class="graphic-tab">
                                <ul class="nav nav-pills">
                    <li class="active"><a data-toggle="pill" href="#home">Home</a></li>
                    <li><a data-toggle="pill" href="#menu1">Menu 1</a></li>
                    <li><a data-toggle="pill" href="#menu2">Menu 2</a></li>
                    <li><a data-toggle="pill" href="#menu3">Menu 3</a></li>
                  </ul>
                  
                  <div class="tab-content">
                    <div id="home" class="tab-pane fade in active">
                        <div class="image-section">
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="tab-image">
                                <img src="<?php echo base_url('front/'); ?>images/t1.jpg" class="img-responsive"/>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="tab-image">
                                <img src="<?php echo base_url('front/'); ?>images/t2.jpg" class="img-responsive"/>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="tab-image">
                                <img src="<?php echo base_url('front/'); ?>images/t3.jpg" class="img-responsive"/>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="tab-image">
                                <img src="<?php echo base_url('front/'); ?>images/t4.jpg" class="img-responsive"/>
                            </div>
                        </div>
                        </div>
                    </div>
                    <div id="menu1" class="tab-pane fade">
                     <div class="image-section">
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="tab-image">
                                <img src="<?php echo base_url('front/'); ?>images/t1.jpg" class="img-responsive"/>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="tab-image">
                                <img src="<?php echo base_url('front/'); ?>images/t2.jpg" class="img-responsive"/>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="tab-image">
                                <img src="<?php echo base_url('front/'); ?>images/t3.jpg" class="img-responsive"/>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="tab-image">
                                <img src="<?php echo base_url('front/'); ?>images/t4.jpg" class="img-responsive"/>
                            </div>
                        </div>
                        </div>
                    </div>
                    <div id="menu2" class="tab-pane fade">
                     <div class="image-section">
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="tab-image">
                                <img src="<?php echo base_url('front/'); ?>images/t1.jpg" class="img-responsive"/>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="tab-image">
                                <img src="<?php echo base_url('front/'); ?>images/t2.jpg" class="img-responsive"/>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="tab-image">
                                <img src="<?php echo base_url('front/'); ?>images/t3.jpg" class="img-responsive"/>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="tab-image">
                                <img src="<?php echo base_url('front/'); ?>images/t4.jpg" class="img-responsive"/>
                            </div>
                        </div>
                        </div>
                    </div>
                    <div id="menu3" class="tab-pane fade">
                     <div class="image-section">
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="tab-image">
                                <img src="<?php echo base_url('front/'); ?>images/t1.jpg" class="img-responsive"/>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="tab-image">
                                <img src="<?php echo base_url('front/'); ?>images/t2.jpg" class="img-responsive"/>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="tab-image">
                                <img src="<?php echo base_url('front/'); ?>images/t3.jpg" class="img-responsive"/>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="tab-image">
                                <img src="<?php echo base_url('front/'); ?>images/t4.jpg" class="img-responsive"/>
                            </div>
                        </div>
                        </div>
                    </div>
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
<div class="blue-background">
    <div class="container">
        <div class="row">
            <div class="center">
                <div class="col-md-6">
                <div class="blue_image">
                    <img src="<?php echo base_url('front/'); ?>images/q.jpg" class="img-responsive"/>
                </div>
                </div>
                <div class="col-md-6">
                <article class="blue-content">
                <h5>What is Lorem Ipsum?</h5>
                <p>
                Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                 Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scra
                </p>
                </article>
                <div class="line">
                </div>
                <ul class="client_description">
                <li><span>
                <i class="fa fa-dribbble"></i></span>&nbsp;Lorem Ipsum is
                </li>
                <li><span>
                <i class="fa fa-adjust "></i></span>&nbsp;Lorem Ipsum is
                </li>

                 <li><span>
                <i class="fa  fa-asterisk "></i></span>&nbsp;Lorem Ipsum is
                </li>

                </ul>
                <a href="#" class="blue_button"> View In Action</a>
                </div>
            </div>
        </div>
    </div>
    <div class="cross" title="Close the Div">
    <i class="fa fa-close"></i>
    </div>
</div>
<div class="work-section">
    <div class="container">
        <div class="row">
            <div class="center">
                <div class="col-md-3 col-sm-3 col-xs-12">
                <div class="image-work">
                <img src="<?php echo base_url('front/'); ?>images/i1.jpg" class="img-responsive"/>
                </div>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-12">
                <div class="image-work">
                <img src="<?php echo base_url('front/'); ?>images/i2.jpg" class="img-responsive"/>
                </div>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-12 clearfix">
                <div class="image-work">
                <img src="<?php echo base_url('front/'); ?>images/i3.jpg" class="img-responsive"/>
                </div>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-12 ">
                <div class="image-work">
                <img src="<?php echo base_url('front/'); ?>images/i4.jpg" class="img-responsive"/>
                </div>
                </div>
               <div class="arrows">
               <i class="fa fa-arrow-down"></i>
               </div>
               <div class="work_heading">
               <h4> Lorem Ipsum is simply dummy text</h4>
               <p>
               Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been 
               </p>
               <a href="#" class="work_section"> Read More</a>
               </div>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
<div class="our_team">
    <div class="container">
        <div class="row">
            <div class="center">
            <div class="our_team-box">
                <div class="team_heading">
                <h3> Lorem Ipsum is simply dummy text of the printing </h3>
                <p>
               Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been 
               </p>
               <div class="circle-icon">
                            <div class="circle">
                                
                            </div>
                            
                        </div>
                </div>
                <div class="team_description">
                    <div class="row">
                        <div class="col-md-3 col-sm-3 col-xs-6">
                        <a href="javascript:void(0)" class="button" id="showdiv1">
                            <div class="employee_description">
                                <img src="<?php echo base_url('front/'); ?>images/e1.jpg" class="img-responsive img-circle" alt=""/>
                                <h5>Lorem Ipsum</h5>
                                <p>Lorem Ipsum</p>
                            </div>
                            </a>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-6">
                        <a href="javascript:void(0)"  class="button" id="showdiv2">
                            <div class="employee_description">
                                <img src="<?php echo base_url('front/'); ?>images/e2.jpg" class="img-responsive img-circle" alt=""/>
                                <h5>Lorem Ipsum</h5>
                                <p>Lorem Ipsum</p>
                            </div>
                            </a>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-6">
                        <a href="javascript:void(0)" class="button" id="showdiv3">

                            <div class="employee_description">
                                <img src="<?php echo base_url('front/'); ?>images/e3.jpg" class="img-responsive img-circle" alt=""/>
                                <h5>Lorem Ipsum</h5>
                                <p>Lorem Ipsum</p>
                            </div>
                            </a>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-6">
                        <a href="javascript:void(0)" class="button" id="showdiv4">

                            <div class="employee_description">
                                <img src="<?php echo base_url('front/'); ?>images/e4.jpg" class="img-responsive img-circle" alt=""/>
                                <h5>Lorem Ipsum</h5>
                                <p>Lorem Ipsum</p>
                            </div>
                            </a>
                        </div>
                    </div>
                    
                </div>
            </div>
            </div>
        </div>
    </div>
</div>
<div class="employee_history">
    <div class="container">
        <div class="center">
            <div id="div1">
            <div class="employee_story">
             <h2>My Story1</h2>
        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since</p>
    </div>
    </div>
<div id="div2" style="display:none;">
<div class="employee_story">
             <h2>My Story2</h2>
        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since</p>
    </div>
</div>
<div id="div3" style="display:none;">
<div class="employee_story">
             <h2>My Story3</h2>
        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since</p>
    </div>
</div>
<div id="div4" style="display:none;">
<div class="employee_story">
             <h2>My Story4</h2>
        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since</p>
    </div>
</div>
        </div>
    </div>
</div>
<div class="employee_skills">
    <div class="container">
        <div class="row">
            <div class="center">
                <div class="col-md-12">
                    <div class="employee_heading">
                        <h2> We Have Skills</h2>
                        <p>
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since
                        </p>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <article class="about_us">
                    <h2>Lorem Ipsum</h2>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, 
                    when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the 
                    leap into
                     electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset
                     sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                     <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, 
                    when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the 
                    leap into
                     .</p>
                     <a href="#" class="work_section1"> Read More</a>
                    </article>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12 marg-20">
                <h2 class="skills_heading">Lorem Ipsum</h2><hr />
                 <p>Sed ut perspiciatis</p>
                    <div class="progress">
                   
                        <div class="progress-bar" role="progressbar" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100" style="width:90%">
                          <span class="sr-only">70% Complete</span>
                        </div>
                      </div>
                      <p>Sed ut perspiciatis</p>
                    <div class="progress">
                   
                        <div class="progress-bar" role="progressbar" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100" style="width:90%">
                          <span class="sr-only">70% Complete</span>
                        </div>
                      </div>
                      <p>Sed ut perspiciatis</p>
                    <div class="progress">
                   
                        <div class="progress-bar" role="progressbar" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100" style="width:90%">
                          <span class="sr-only">70% Complete</span>
                        </div>
                      </div>
                      <p>Sed ut perspiciatis</p>
                    <div class="progress">
                   
                        <div class="progress-bar" role="progressbar" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100" style="width:90%">
                          <span class="sr-only">70% Complete</span>
                        </div>
                      </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="project_completed">
    <div class="container">
        <div class="row">
            <div class="center">
                <div class="row">
                    <div class="col-md-3 col-sm-6 col-xs-6">
                        <div class="icon1">
                        <i class="fa fa-heart"></i>Sed ut perspiciatis
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-6">
                        <div class="icon1">
                        <i class="fa fa-hand-o-down"></i>Sed ut perspiciatis
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-6">
                        <div class="icon1">
                        <i class="fa fa-hourglass-3 "></i>Sed ut perspiciatis
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-6">
                        <div class="icon1">
                        <i class="fa fa-graduation-cap"></i>Sed ut perspiciatis
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="grid_syestem">
    <div class="container">
        <div class="row">
            <div class="center">
                <div class="row">
                    <div class="grid_heading">
                     <h2>Lorem ipsum dolor</h2>
                     <h6> Our Blog</h6>
                     <div class="circle-icon">
                            <div class="circle">
                                
                            </div>
                            
                        </div>
        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since</p>
                    </div>
                    <div class="tab_grid">
                        <ul class="nav nav-pills">
                        <li class="active"><a data-toggle="pill" href="#home1">Home</a></li>
                        <li><a data-toggle="pill" href="#menu11">Menu 1</a></li>
                        <li><a data-toggle="pill" href="#menu21">Menu 2</a></li>
                        <li><a data-toggle="pill" href="#menu31">Menu 3</a></li>
                      </ul>
                      
                      <div class="tab-content">
                        <div id="home1" class="tab-pane fade in active">
                          <div class="col-md-6">
                          <div class="img-tab">
                          <img src="<?php echo base_url('front/'); ?>images/w1.jpg" class="img-responsive" alt=""/>
                          </div>
                          </div>
                          <div class="col-md-6">
                          <div class="img-tab">
                          <img src="<?php echo base_url('front/'); ?>images/w2.jpg" class="img-responsive" alt=""/>
                          </div>
                          </div>
                          
                        </div>
                        <div id="menu11" class="tab-pane fade">
                          <div class="col-md-6">
                          <div class="img-tab">
                          <img src="<?php echo base_url('front/'); ?>images/w3.jpg" class="img-responsive" alt=""/>
                          </div>
                          </div>
                          <div class="col-md-6">
                          <div class="img-tab">
                          <img src="<?php echo base_url('front/'); ?>images/w4.jpg" class="img-responsive" alt=""/>
                          </div>
                          </div></div>
                        <div id="menu21" class="tab-pane fade">
                          <div class="col-md-6">
                          <div class="img-tab">
                          <img src="<?php echo base_url('front/'); ?>images/w1.jpg" class="img-responsive" alt=""/>
                          </div>
                          </div>
                          <div class="col-md-6">
                          <div class="img-tab">
                          <img src="<?php echo base_url('front/'); ?>images/w2.jpg" class="img-responsive" alt=""/>
                          </div>
                          </div>
                        </div>
                        <div id="menu31" class="tab-pane fade">
                          <div class="col-md-6">
                          <div class="img-tab">
                          <img src="<?php echo base_url('front/'); ?>images/w3.jpg" class="img-responsive" alt=""/>
                          </div>
                          </div>
                          <div class="col-md-6">
                          <div class="img-tab">
                          <img src="<?php echo base_url('front/'); ?>images/w4.jpg" class="img-responsive" alt=""/>
                          </div>
                          </div>
                        </div>
                      
                    </div>
                </div>
            </div>
            <div class="arrows1">
               <i class="fa fa-arrow-down"></i>
               </div>
               
        </div>
    </div>
</div>
</div>
<div class="newsLetter-section">
    <div class="container">
        <div class="row">
            <div class="center">
            <div class="newsLetter">
                <div class="news_heading">
                <h2> News Letter</h2>
                <p>
                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy 
                </p>
                </div>
                <form id="newsletter_form" method="post" action="/">
                    <label></label>
                    <div class="col-md-12">
                    <input type="text"  class="form_input" placeholder=" Enter Your Email" />
                    <button type="submit" class="bt btn-info button_subscribe"> Subscribe</button>
                    </div>
                </form>
                </div>
              </div>
             </div>
            </div>
       </div>
       
  <div class="clearfix"></div>
      <div class="our_customers">
        <div class="container">
            <div class="row">
                <div class="center">
                <div class="customers_heading">
                <h2> Our Customer</h2>
                <p>
                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever
                </p>
                </div>
                <div class="client_logo">
                    <div class="col-md-2 col-sm-2 col-xs-4">
                    <div class="logo">
                    <img src="<?php echo base_url('front/'); ?>images/l1.png" class="img-responsive"/>
                    </div>
                    </div>
                    <div class="col-md-2 col-sm-2 col-xs-4">
                    <div class="logo">
                    <img src="<?php echo base_url('front/'); ?>images/l2.png" class="img-responsive"/>
                    </div>
                    </div>
                    <div class="col-md-2 col-sm-2 col-xs-4">
                    <div class="logo">
                    <img src="<?php echo base_url('front/'); ?>images/l3.png" class="img-responsive"/>
                    </div>
                    </div>
                    <div class="col-md-2 col-sm-2 col-xs-4">
                    <div class="logo">
                    <img src="<?php echo base_url('front/'); ?>images/l4.png" class="img-responsive"/>
                    </div>
                    </div>
                    <div class="col-md-2 col-sm-2 col-xs-4">
                    <div class="logo">
                    <img src="<?php echo base_url('front/'); ?>images/l1.png" class="img-responsive"/>
                    </div>
                    </div>
                    <div class="col-md-2 col-sm-2 col-xs-4">
                    <div class="logo">
                    <img src="<?php echo base_url('front/'); ?>images/l2.png" class="img-responsive"/>
                    </div>
                    </div>
                </div>
                    
                </div>
            </div>
        </div>
      </div>
<div class="clearfix"></div>
<div class="testimonial">
    <div class="container">
        <div class="row">
            <div class="center">
                <div class="testimonial_heading">
                    <h2> Testimonial</h2>
                </div>
             <div class="testimonial_section">
                <div id="myCarousel" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
                 
                
                  <!-- Wrapper for slides -->
                  <div class="carousel-inner">
                    <div class="item active">
                        <div class="testimonial_feedback">
                      <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever</p>
                      <div class="circle-icon">
                            <div class="circle">
                                
                            </div>
                            
                        </div>
                      <div class="client-name">Lorem Ipsum</div>
                      </div>
                    </div>
                
                    <div class="item">
                     <div class="testimonial_feedback">
                      <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever</p>
                      <div class="circle-icon">
                            <div class="circle">
                                
                            </div>
                            
                        </div>
                      <div class="client-name">Lorem Ipsum</div>
                      </div>
                    </div>
                
                    <div class="item">
                      <div class="testimonial_feedback">
                      <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever</p>
                      <div class="circle-icon">
                            <div class="circle">
                                
                            </div>
                            
                        </div>
                      <div class="client-name">Lorem Ipsum</div>
                      </div>
                    </div>
                  </div>
                
                  <!-- Left and right controls -->
                  <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                    <span class="fa fa-chevron-left"></span>
                    <span class="sr-only">Previous</span>
                  </a>
                  <a class="right carousel-control" href="#myCarousel" data-slide="next">
                    <span class="fa fa-chevron-right"></span>
                    <span class="sr-only">Next</span>
                  </a>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>
   <footer class="footer-form">
    <div class="container">
        <div class="row">
            <div class="center">
    <div class="footer-heading">
    <h2>
    Feel Free To Ask 
    </h2>
    <h4> Contact Us</h4>
    <div class="circle-icon1">
         <div class="circle1">
               </div>
      </div>
    </div>
    <div class="footer-address">
        <div class="col-md-4 col-sm-4 col-xs-12">
            <div class="circle-middle">
            <div class="circle-heading">
                <i class="fa fa-anchor "></i>
                     </div>
                     </div>
                     <div class="address">
        <p>Lorem Ipsum is simply dummy text </p>
        </div>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-12">
            <div class="circle-middle">
            <div class="circle-heading">
                <i class="fa fa-bell"></i>
                     </div>
                     </div>
                     <div class="address">
        <p>Lorem Ipsum is simply dummy text </p>
        </div>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-12">
            <div class="circle-middle">
            <div class="circle-heading">
                <i class="fa fa-mobile-phone"></i>
                     </div>
                     </div>
                     <div class="address">
        <p>Lorem Ipsum is simply dummy text </p>
        </div>
        </div>
        
    </div>
    <div class="footer_fullform">
        <form id="form_last">
        <div class="row">
            <div class="col-md-4 col-sm-4 col-xs-12">
                <input type="text" class="form-control form-input" placeholder="Enter Your Name"/>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12">
                <input type="text" class="form-control form-input" placeholder="Enter Your Mail"/>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12">
                <input type="text" class="form-control form-input" placeholder="Enter Your Subject"/>
            </div>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12">
            <textarea class="form-control text-comment" rows="6" id="comment" placeholder="Enter Your Messsage"></textarea>
            </div>
            <div class="middle-div">
            <button type="submit" class=" submit_button"> Submit</button>
            </div>
        </form>
    </div>
    </div>
    </div>
    </div>