
<!DOCTYPE html>
<html>
<head>
  <?php include('header.php');?>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Services</title>
</head>
<style>
  .service_head{
    margin-top: 50px;
    font-size: 40px;
    text-transform: uppercase;
    letter-spacing: 1px;
    word-spacing: 5px;
    font-family: serif;
  }
  #black_filter {
        position: absolute;
        width: 100%;
        height: 100%;
        background-color: #2282a68c;
        z-index: 0;
        top: 0;
        left: 0;
      }

      .tender_cntnt h1{
      color: #fff;
    text-align: center;
    font-size: 50px;
    letter-spacing: 5px;
    margin-bottom: 50px;
}
.tender_cntnt #del-countdown{
      width: 850px;
    /* margin: 15% auto; */
    display: block;
    margin: auto;
    height: auto;
    padding-bottom: 100px;
    padding-top: 50px;
}
.tender_cntnt #clock span{
  float: left;
  text-align: center;
  font-size: 84px;
  margin: 0 2.5%;
  color: #fff;
  padding: 20px;
  width: 20%;
  border-radius: 20px;
  box-sizing: border-box;
}
.tender_cntnt #clock span:nth-child(1){

  background: #fa5559;
}
.tender_cntnt #clock span:nth-child(2){

  background: #26c2b9;
}
.tender_cntnt #clock span:nth-child(3){

  background: #f6bc58;
}
.tender_cntnt #clock span:nth-child(4){

  background: #2dcb74;
}
.tender_cntnt #clock:after{
  content: "";
  display: block;
  clear: both;
}
.tender_cntnt #units span {
  float: left;
  width: 25%;
  text-align: center;
  margin-top: 30px;
  color: #ddd;
  text-transform: uppercase;
  font-size: 13px;
  letter-spacing: 2px;
  text-shadow: 1px 1px 1px rgba(10,18,10,0.7);
}

.tender_cntnt span.turn{
  animation: turn 0.8s ease forwards;
}

@keyframes .tender_cntnt turn{
  0%{transform: rotateX(0deg);}
  100%{transform: rotateX(360deg);}
}

</style>
<body>

<?php 
    
    if ($view_services[0]['mcate_name'] == "Events" ) {
      ?>
      <section class="user_dashboard" style="margin-top: 30px;">
      <div class="tender">
        <div class="tender_head">
            <div class="jumbotron" style="background: url('<?php echo base_url('front/images/event_img.jpg'); ?>') no-repeat;background-size: 100% 100%;position: relative;padding: 150px;">
              <div id="black_filter"></div>
              <div class="container">
                <h1 style="text-align: center;color: white;position: relative;z-index: 3; text-transform: uppercase; letter-spacing: 10px;"><?php echo $view_services[0]['mcate_name']; ?></h1>
              </div>
            </div>
        </div>
            <div class="tender_cntnt " style=" background: #282e3a; font-family: tahoma; margin-top: -30px;">
                 <div class="upcoming_event">
                   <div id="del-countdown">
                      <h1>UPCOMING EVENT</h1>
                      <div id="clock"></div>
                      <div id="units">
                        <span>Days</span>
                        <span>Hours</span>
                        <span>Minutes</span>
                        <span>Seconds</span>
                      </div>
                    </div>
                 </div> 
        </div>
        <div class="past_events" style="background: #fa5559; color: white; padding-bottom: 100px;">
          <div class="tender_cntnt container">
               <h1>PAST EVENT</h1>
               <hr style="width: 20%; border: 1px solid #fff; margin-top: -25px;">
                <?php 
                  foreach ($view_pevent as $value) {
                   ?>
               <div class="col-md-3">
                 <img src="<?php echo base_url('uploads/').$value['event_logo']; ?>" style="width: 100%; height: 200px;" alt=""><br>
                  
                  <h4 style="text-align: center; padding-top: 20px; border-top: 1px solid;"><?php echo $value['event_name']; ?></h4>
               </div>
                 <?php }         ?>
          </div>
         </div> 


      </div>
   <script>
     function updateTimer(deadline){
  var time = deadline - new Date();
  return {
    'days': Math.floor( time/(1000*60*60*24) ),
    'hours': Math.floor( time/(1000*60*60) % 24 ),
    'minutes': Math.floor( time/(1000*60) % 60 ),
    'seconds': Math.floor( time/(1000) % 60 ),
    'total' : time
  };
}

function animateClock(span){
  span.className = "turn";
  setTimeout(function(){
    span.className = "";
  },700);
}


function startTimer(id , deadline){

  var timeInterval = setInterval(function(){
    var clock = document.getElementById(id);
    var timer = updateTimer(deadline);

    clock.innerHTML = '<span>' + timer.days + '</span>'
            + '<span>' + timer.hours + '</span>'  
            + '<span>' + timer.minutes + '</span>'  
            + '<span>' + timer.seconds + '</span>';

    var spans = clock.getElementsByTagName("span");
    animateClock(spans[3]);
    if (timer.seconds == 59) animateClock(spans[2]);
    if (timer.minutes == 59 && timer.seconds == 59) animateClock(spans[1]);
    if (timer.hours == 23 && timer.minutes == 59 && timer.seconds == 59) animateClock(spans[0]);


    if (timer.total < 1) {
      clearInterval(timeInterval);
      clock.innerHTML = '<span>0</span><span>0</span><span>0</span><span>0</span>';

    } 

  },1000);
}


window.onload = function() {
  // var date = 
  var deadline = new Date("<?php echo $view_uevent[0]['end_date']; ?>");
  startTimer("clock",deadline);
}
   </script>
</section>
 <?php      
    }else{

 ?>

    <section class="user_dashboard" style="margin-top: 30px; background: #cccccc38; padding-bottom: 100px;">
      <div class="container">
        <div class="service_head" >
          <h2 style="text-align: center;"><?php echo $view_services[0]['mcate_name']; ?></h2>
          <hr style="width: 15%; margin-top: 20px; margin-bottom: 50px; border: 1px solid black;">
        </div>
        <div class="col-md-6">
          <img src="<?php echo base_url('uploads/').$view_services[0]['image'];?>" alt="" style="width: 100%; height: 400px; border-radius: 5px; box-shadow: 0 0 2px 4px #c1bdbd94;">
        </div>
        <div class="col-md-6">
          
            <h2></h2>
            <p style="font-size: 18px; font-family: serif;"><?php echo $view_services[0]['description'];?></p>
        </div>
      </div>
</section>
<?php } ?>
</body>
</html>
<?php include('footer.php');?>