<!DOCTYPE html>
<html>
<head>
  <?php include('header.php');?>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title></title>
    <style>
    	#gov::after {
    		content: "";
		    display: block;
		    background: #4392b0;
		    height: 2px;
		    width: 220px;
		    position: absolute;
		    bottom: -5px;
    	}
    	#pvt::after {
    		content: "";
		    display: block;
		    background: #4392b0;
		    height: 2px;
		    width: 220px;
		    position: absolute;
		    bottom: -5px;
    	}
    	#black_filter {
    		position: absolute;
    		width: 100%;
    		height: 100%;
    		background-color: #2282a68c;
    		z-index: 0;
    		top: 0;
    		left: 0;
    	}
    </style>
</head>
<body>
    <section class="user_dashboard" style="margin-top: 30px; margin-bottom: 100px;">
      <div class="tender">
        <div class="tender_head">
          	<div class="jumbotron" style="background: url('<?php echo base_url('front/images/ten_slide.jpg'); ?>') no-repeat;background-size: 100% 100%;position: relative;padding: 110px;">
          		<div id="black_filter"></div>
          	  <div class="container">
          	    <h1 style="text-align: center;color: white;position: relative;z-index: 3;">Tenders</h1>
          	  </div>
          	</div>
          	<hr>
          	<div class="tender_cntnt container">
          		<!-- <h1 style="text-align: center;font-weight: bold;"></h1>/ -->
				<h3 id="gov" style="position: relative;">Government Tenter</h3>
				<p>e-Procurement Technologies Ltd. is an ISO 27001 Certified eProcurement Company that provides an end to end eProcurement Solution like Tender Alert Service (Market Making), eAuction, Reverse Auction, eTendering, eSourcing, etc.</p>

				<p>We annually Track more than 1 Million Tenders/Annum & we successfully enable Rs.10,000 Cr worth of eProcurement every Year. Out of thousands of our costumers some of the reputed one includes ICICI Bank, SBI Bank, BHEL, Bank of Baroda, ESSAR Group, etc. for whom we are the exclusive eProcurement service providers & their procurement worth 1000's of crores are processed through our platform www.abcProcure.com</p>

				<p>We are a professionally managed, 3 Year old profit making company, and partners spread across India. We are Procurement Leaders & have managed to become #1 eProcurement Player because of our Domain Focus & core competence in Procurement space.</p>
				<h3 id="pvt" style="position: relative;">PVT/LTD/SME</h3>
				<p></p>
          	</div>
        </div>

      </div>
   
</section>
</body>
</html>
<?php include('footer.php');?>