<!DOCTYPE html>
<html>
<head>
  <?php include('header.php');?>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title></title>
</head>
<body>
   <div class="customized-template" id="customers" style="padding-bottom: 50px;">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                
                <div class="center">
                    <div class="leyered-section">
                        <div class="leyered-heading">
                        <h2 style="text-align: center; color: black;">Create Your Account</h2>
                        <div class="circle-icon">
                            <div class="circle">
                                
                            </div>
                            
                        </div>


<div id="" class=" " role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
        <?php 
       if($this->session->flashdata('insertmessage')){
        echo $this->session->flashdata('insertmessage'); 
       }elseif($this->session->flashdata('email_id')){

        echo $this->session->flashdata('email_id'); 
       }


       ?>
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Create Your Account</h4>
      </div>
      <div class="modal-body">
        <form method="post"  action="<?php echo base_url('welcome/users_register');?>">
      <div class="form-group">
        <label for="email">User Name:</label>
        <input type="text" class="form-control" name="username" required>
      </div>
      <div class="form-group">
        <label for="pwd">Password:</label>
        <input type="password" class="form-control" name="password" required>
      </div>
      <div class="form-group">
        <label for="pwd">Email:</label>
        <input type="email" class="form-control" name="email" required>
      </div>
      <div class="form-group">
        <label for="pwd">Phone:</label>
        <input type="phone" class="form-control" name="phone" required>
      </div>

      <button type="submit" class="btn btn-default">Submit</button>
    </form>
          </div>
    </div>

  </div>
</div>


                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
<?php include('footer.php');?>