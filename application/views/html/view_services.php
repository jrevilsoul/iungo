<!DOCTYPE html>
<html>
<head>
  <?php include('header.php');?>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title></title>
</head>
<body>
   <div class="customized-template" id="services" style="padding-bottom: 50px;">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                
                <div class="center">
                    <div class="leyered-section">
                        <div class="leyered-heading">
                        <h2 style="text-align: center; color: black;">Our Services</h2>
                        <div class="circle-icon">
                            <div class="circle">
                                
                            </div>
                            
                        </div>
                        <article class="service-content">
                            <p>We are fully committed to integrating customer driven quality standards with superior innovation. Our abiding Endeavour is to provide customized software solutions to suit all your business requirements and with the best returns on investment. Our comprehensive portfolio of products and services includes, but is not limited to, website development,software solutions. All our processes are geared towards producing the best products and services in terms of quality, functionality, and affordability.</p>
                            </article>
                        </div>
                        <main class="main-area our_packages">

                          <div class="centered">

                            <section class="cards">
                                <div class="services">            
                                <?php foreach ($view_catone as $value) {?>  
                                <div class="col-md-4" style="height: 500px;">
                                  <article class="card" style="height: 450px;">
                                    <a href="<?php echo base_url('menu/Service_detail/').$value['mcat_id'];?>">
                                      <figure class="thumbnail" style="margin-bottom: 0;">
                                      <img src="<?php echo base_url('uploads/').$value['image'];?>" alt="meow">
                                      </figure>
                                      <div class="card-content">
                                        <h2><?php echo $value['mcate_name'];?></h2>
                                        <p class="service_desc"><?php echo $value['description'];?></p>
                                      </div>
                                      <!-- .card-content -->
                                    </a>
                                  </article>
                                </div>
                              <?php }?>
                                </div>
                              <!-- .card -->

                            </section>
                            <!-- .cards -->

                          </div>
                          <!-- .centered -->

                        </main>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
<?php include('footer.php');?>