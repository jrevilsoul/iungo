<?php include('header.php');
error_reporting(0);
?>

<style>
    
.slick-next:before, .slick-prev:before {
    color: black;
}

</style>
<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
    <li data-target="#carousel-example-generic" data-slide-to="3"></li>
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">
    <?php 
    $i = 1;
    foreach ($view_banner as  $value) {         ?>
    <div class="item <?php if($i == 1){ echo "active" ;}  ?> ">
      <img src="<?php echo base_url('banners/').$value['image']; ?>" style="height: 400px; width: 100%;" class="img-responsive"/>
    </div>

  <?php $i++; } ?>
  </div>

  <!-- Controls -->
  <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
    <span class="fa fa-chevron-left" style="margin-top: 115%; font-size: 30px;" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
    <span class="fa fa-chevron-right" style="margin-top: 115%; font-size: 30px;" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
<div class="clearfix"></div>
<div class="customized-template">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                
                <div class="center">
                    <div class="leyered-section">
                        <div class="leyered-heading">
                        <h3> We are fully committed to integrating customer driven quality standards</h3>
                        <h6> Our Services</h6>
                        <div class="circle-icon">
                            <div class="circle">
                                
                            </div>
                            
                        </div>
                        <article class="service-content">
                            <p>We are fully committed to integrating customer driven quality standards with superior innovation. O
                            ur abiding Endeavour is to provide customized software solutions to suit all your business requirements and with the best
                             returns on investment. Our comprehensive portfolio of products and services includes, but is not limited to, website development, 
                             software solutions. 
                            All our processes are geared towards producing the best products and services in terms of quality, functionality, and affordability.</p>
                            </article>
                        </div>
                        <main class="main-area our_packages">

  <div class="centered">

    <section class="cards">
        <div class="services">            
        <?php foreach ($view_catone as $value) {?>  
        <div>
          <article class="card">
            <a href="<?php echo base_url('menu/services/').$value['mcat_id'];?>">
              <figure class="thumbnail" style="margin-bottom: 0;">
              <img src="<?php echo base_url('uploads/').$value['image'];?>" alt="meow">
              </figure>
              <div class="card-content">
                <h2><?php echo $value['mcate_name'];?></h2>
                <p><?php echo $value['description'];?></p>
              </div>
              <!-- .card-content -->
            </a>
          </article>
        </div>
      <?php }?>
        </div>
      <!-- .card -->

    </section>
    <!-- .cards -->

  </div>
  <!-- .centered -->

</main>
                        <!-- <div class="four-boxServiceDescription">
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="box-1">
                                <div class="heading-center">
                                    <div class="circle-heading">
                                    <i class="fa fa-bell"></i>
                                    </div>
                                    </div>
                                    <div class="service-heading">
                                    <h5>We are fully committed </h5>
                                    <p>We are fully committed to integrating</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="box-1">
                                  <div class="heading-center">
                                    <div class="circle-heading">
                                    <i class="fa  fa-cab "></i>
                                    </div>
                                    </div>
                                    <div class="service-heading">
                                    <h5>Software Solutions </h5>
                                    <p>We are fully committed to integrating</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="box-1">
                                  
                                <div class="heading-center">
                                    <div class="circle-heading">
                                    <i class="fa  fa-asterisk"></i>
                                    </div>
                                    </div>
                                    <div class="service-heading">
                                    <h5>quality standards <h5>
                                    <p>We are fully committed to integrating</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="box-1">
                                  <div class="heading-center">
                                    <div class="circle-heading">
                                    <i class="fa fa-bolt"></i>
                                    </div>
                                    </div>
                                    <div class="service-heading">
                                    <h5>Team of expert </h5>
                                    <p>We are fully committed to integrating</p>
                                    </div>
                                </div>
                            </div>
                        </div> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
<div class="design">
    <div class="overlay"></div>
    <div class="content">
        <p>
Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when        </p>
<p> Lorem Ipsum</p>
    </div>
    
</div>
<div class="clearfix"></div>
<div class="modern-class">
    <div class="container">
        <div class="row">
            <div class="center">
                <div class="modern-heading">
                <!-- <h4> Lorem Ipsum is simply dummy text</h4> -->
                <h2 style="font-size: 45px; font-family: serif;">Our Plan</h2>
                <div class="circle-icon">
                            <div class="circle">
                                
                            </div>
                            
                        </div>
                        <p>
                       Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 
                        </p>
                </div>
<div class="plan container" style="margin-top: 20px;">
    
<div class="col-md-3">
    
<div class="circle_contnn">
        <div class="center12 ">
        <span><i class="fa fa-headphones" style="font-size: 60px;  margin-bottom: 5px;"></i></span><br>
            <p>10 Mbps</p>
            <p><i class="fa fa-rupee">&nbsp;1499</i></p>
                <div class="hiddenmm">
                    <p>Data Limit 100GB</p>
                    <p>Unlimited Data</p>
            </div>
        </div>
    </div>
</div>
    <div class="col-md-3">
        
    <div class="circle_contnn">
        <div class="center12 ">
            <span><i class="fa fa-picture-o" style="font-size: 60px;  margin-bottom: 5px;"></i></span><br>
            <p>20 Mbps</p>
            <p><i class="fa fa-rupee">&nbsp;1999</i></p>
                <div class="hiddenmm">
                    <p>Data Limit 150GB</p>
                    <p>Unlimited Data</p>
            </div>
        </div>
    </div>
    </div>
     <div class="col-md-3">
         
     <div class="circle_contnn">
        <div class="center12 ">
            <span><i class="fa fa-youtube-play" style="font-size: 60px;  margin-bottom: 5px;"></i></span><br>
            <p>30 Mbps</p>
            <p><i class="fa fa-rupee">&nbsp;2499</i></p>
                <div class="hiddenmm">
                    <p>Data Limit 250GB</p>
                    <p>Unlimited Data</p>
            </div>
        </div>
    </div>
     </div>
     <div class="col-md-3">
         
     <div class="circle_contnn">
        <div class="center12 ">
            <span><i class="fa fa-gamepad" style="font-size: 60px;  margin-bottom: 5px;"></i></span><br>
            <p>50 Mbps</p>
            <p><i class="fa fa-rupee">&nbsp;3599</i></p>
                <div class="hiddenmm">
                    <p>Data Limit 300GB</p>
                    <p>Unlimited Data</p>
            </div>
        </div>
    </div>
     </div>
</div>    
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
<!-- <div class="blue-background">
    <div class="container">
        <div class="row">
            <div class="center">
                <div class="col-md-6">
                <div class="blue_image">
                    <img src="<?php echo base_url('front/'); ?>images/q.jpg" class="img-responsive"/>
                </div>
                </div>
                <div class="col-md-6">
                <article class="blue-content">
                <h5>What is Lorem Ipsum?</h5>
                <p>
                Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                 Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scra
                </p>
                </article>
                <div class="line">
                </div>
                <ul class="client_description">
                <li><span>
                <i class="fa fa-dribbble"></i></span>&nbsp;Lorem Ipsum is
                </li>
                <li><span>
                <i class="fa fa-adjust "></i></span>&nbsp;Lorem Ipsum is
                </li>

                 <li><span>
                <i class="fa  fa-asterisk "></i></span>&nbsp;Lorem Ipsum is
                </li>

                </ul>
                <a href="#" class="blue_button"> View In Action</a>
                </div>
            </div>
        </div>
    </div>
    <div class="cross" title="Close the Div">
    <i class="fa fa-close"></i>
    </div>
</div>
<div class="work-section">
    <div class="container">
        <div class="row">
            <div class="center">
                <div class="col-md-3 col-sm-3 col-xs-12">
                <div class="image-work">
                <img src="<?php echo base_url('front/'); ?>images/i1.jpg" class="img-responsive"/>
                </div>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-12">
                <div class="image-work">
                <img src="<?php echo base_url('front/'); ?>images/i2.jpg" class="img-responsive"/>
                </div>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-12 clearfix">
                <div class="image-work">
                <img src="<?php echo base_url('front/'); ?>images/i3.jpg" class="img-responsive"/>
                </div>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-12 ">
                <div class="image-work">
                <img src="<?php echo base_url('front/'); ?>images/i4.jpg" class="img-responsive"/>
                </div>
                </div>
               <div class="arrows">
               <i class="fa fa-arrow-down"></i>
               </div>
               <div class="work_heading">
               <h4> Lorem Ipsum is simply dummy text</h4>
               <p>
               Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been 
               </p>
               <a href="#" class="work_section"> Read More</a>
               </div>
            </div>
        </div>
    </div>
</div> -->
<div class="clearfix"></div>
<div class="our_team">
    <div class="container">
        <div class="row">
            <div class="center">
            <div class="our_team-box">
                <div class="team_heading">
                <h3> Lorem Ipsum is simply dummy text of the printing </h3>
                <p>
               Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been 
               </p>
               <div class="circle-icon">
                            <div class="circle">
                                
                            </div>
                            
                        </div>
                </div>
                <div class="team_description">
                    <div class="row">
                        <div class="col-md-3 col-sm-3 col-xs-6">
                        <a href="javascript:void(0)" class="button" id="showdiv1">
                            <div class="employee_description">
                                <img src="<?php echo base_url('uploads/').$view_team[0]['image']; ?>" class="img-responsive img-circle" alt=""/>
                                <h5><?php echo $view_team[0]['username'];?></h5>
                                <p><?php echo $view_team[0]['designation'];?></p>
                            </div>
                            </a>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-6">
                        <a href="javascript:void(0)" class="button" id="showdiv2">
                            <div class="employee_description">
                                <img src="<?php echo base_url('uploads/').$view_team[1]['image']; ?>" class="img-responsive img-circle" alt=""/>
                                <h5><?php echo $view_team[1]['username'];?></h5>
                                <p><?php echo $view_team[1]['designation'];?></p>
                            </div>
                            </a>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-6">
                        <a href="javascript:void(0)" class="button" id="showdiv3">
                            <div class="employee_description">
                                <img src="<?php echo base_url('uploads/').$view_team[2]['image']; ?>" class="img-responsive img-circle" alt=""/>
                                <h5><?php echo $view_team[2]['username'];?></h5>
                                <p><?php echo $view_team[2]['designation'];?></p>
                            </div>
                            </a>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-6">
                        <a href="javascript:void(0)" class="button" id="showdiv4">
                            <div class="employee_description">
                                <img src="<?php echo base_url('uploads/').$view_team[3]['image']; ?>" class="img-responsive img-circle" alt=""/>
                                <h5><?php echo $view_team[3]['username'];?></h5>
                                <p><?php echo $view_team[3]['designation'];?></p>
                            </div>
                            </a>
                        </div>
                    </div>
                    
                </div>
            </div>
            </div>
        </div>
    </div>
</div>
<div class="employee_history">
    <div class="container">
        <div class="center">
            <div id="div1">
            <div class="employee_story">
             <h2><?php echo $view_team[0]['username']; ?></h2>
        <p><?php echo $view_team[0]['description'];?></p>
    </div>
    </div>
<div id="div2" style="display:none;">
<div class="employee_story">
             <h2><?php echo $view_team[1]['username']; ?></h2>
        <p><?php echo $view_team[1]['description'];?></p>
    </div>
</div>
<div id="div3" style="display:none;">
<div class="employee_story">
             <h2><?php echo $view_team[2]['username']; ?></h2>
        <p><?php echo $view_team[2]['description'];?></p>
    </div>
</div>
<div id="div4" style="display:none;">
<div class="employee_story">
             <h2><?php echo $view_team[3]['username']; ?></h2>
        <p><?php echo $view_team[3]['description'];?></p>
    </div>
</div>
        </div>
    </div>
</div>
<!-- <div class="employee_skills">
    <div class="container">
        <div class="row">
            <div class="center">
                <div class="col-md-12">
                    <div class="employee_heading">
                        <h2> We Have Skills</h2>
                        <p>
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since
                        </p>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <article class="about_us">
                    <h2>Lorem Ipsum</h2>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, 
                    when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the 
                    leap into
                     electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset
                     sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                     <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, 
                    when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the 
                    leap into
                     .</p>
                     <a href="#" class="work_section1"> Read More</a>
                    </article>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12 marg-20">
                <h2 class="skills_heading">Lorem Ipsum</h2><hr />
                 <p>Sed ut perspiciatis</p>
                    <div class="progress">
                   
                        <div class="progress-bar" role="progressbar" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100" style="width:90%">
                          <span class="sr-only">70% Complete</span>
                        </div>
                      </div>
                      <p>Sed ut perspiciatis</p>
                    <div class="progress">
                   
                        <div class="progress-bar" role="progressbar" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100" style="width:90%">
                          <span class="sr-only">70% Complete</span>
                        </div>
                      </div>
                      <p>Sed ut perspiciatis</p>
                    <div class="progress">
                   
                        <div class="progress-bar" role="progressbar" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100" style="width:90%">
                          <span class="sr-only">70% Complete</span>
                        </div>
                      </div>
                      <p>Sed ut perspiciatis</p>
                    <div class="progress">
                   
                        <div class="progress-bar" role="progressbar" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100" style="width:90%">
                          <span class="sr-only">70% Complete</span>
                        </div>
                      </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="project_completed">
    <div class="container">
        <div class="row">
            <div class="center">
                <div class="row">
                    <div class="col-md-3 col-sm-6 col-xs-6">
                        <div class="icon1">
                        <i class="fa fa-heart"></i>Sed ut perspiciatis
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-6">
                        <div class="icon1">
                        <i class="fa fa-hand-o-down"></i>Sed ut perspiciatis
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-6">
                        <div class="icon1">
                        <i class="fa fa-hourglass-3 "></i>Sed ut perspiciatis
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-6">
                        <div class="icon1">
                        <i class="fa fa-graduation-cap"></i>Sed ut perspiciatis
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> -->
<div class="grid_syestem">
    <div class="container">
        <div class="row">
            <div class="center">
                <div class="row">
                    <div class="grid_heading">
                     <h2>Lorem ipsum dolor</h2>
                     <h6> Our Blog</h6>
                     <div class="circle-icon">
                            <div class="circle">
                                
                            </div>
                            
                        </div>
        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since</p>
                    </div>
                    <div class="tab_grid">
                        <ul class="nav nav-pills">
                        <li class="active"><a data-toggle="pill" href="#home1">Home</a></li>
                        <li><a data-toggle="pill" href="#menu11">Menu 1</a></li>
                        <li><a data-toggle="pill" href="#menu21">Menu 2</a></li>
                        <li><a data-toggle="pill" href="#menu31">Menu 3</a></li>
                      </ul>
                      
                      <div class="tab-content">
                        <div id="home1" class="tab-pane fade in active">
                          <div class="col-md-6">
                          <div class="img-tab">
                          <img src="<?php echo base_url('front/'); ?>images/w1.jpg" class="img-responsive" alt=""/>
                          </div>
                          </div>
                          <div class="col-md-6">
                          <div class="img-tab">
                          <img src="<?php echo base_url('front/'); ?>images/w2.jpg" class="img-responsive" alt=""/>
                          </div>
                          </div>
                          
                        </div>
                        <div id="menu11" class="tab-pane fade">
                          <div class="col-md-6">
                          <div class="img-tab">
                          <img src="<?php echo base_url('front/'); ?>images/w3.jpg" class="img-responsive" alt=""/>
                          </div>
                          </div>
                          <div class="col-md-6">
                          <div class="img-tab">
                          <img src="<?php echo base_url('front/'); ?>images/w4.jpg" class="img-responsive" alt=""/>
                          </div>
                          </div></div>
                        <div id="menu21" class="tab-pane fade">
                          <div class="col-md-6">
                          <div class="img-tab">
                          <img src="<?php echo base_url('front/'); ?>images/w1.jpg" class="img-responsive" alt=""/>
                          </div>
                          </div>
                          <div class="col-md-6">
                          <div class="img-tab">
                          <img src="<?php echo base_url('front/'); ?>images/w2.jpg" class="img-responsive" alt=""/>
                          </div>
                          </div>
                        </div>
                        <div id="menu31" class="tab-pane fade">
                          <div class="col-md-6">
                          <div class="img-tab">
                          <img src="<?php echo base_url('front/'); ?>images/w3.jpg" class="img-responsive" alt=""/>
                          </div>
                          </div>
                          <div class="col-md-6">
                          <div class="img-tab">
                          <img src="<?php echo base_url('front/'); ?>images/w4.jpg" class="img-responsive" alt=""/>
                          </div>
                          </div>
                        </div>
                      
                    </div>
                </div>
            </div>
            <div class="arrows1">
               <i class="fa fa-arrow-down"></i>
               </div>
               
        </div>
    </div>
</div>
</div>
<div class="newsLetter-section">
    <div class="container">
        <div class="row">
            <div class="center">
            <div class="newsLetter">
                <div class="news_heading">
                <h2> News Letter</h2>
                <p>
                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy 
                </p>
                </div>
                <form id="newsletter_form" method="post" action="/">
                    <label></label>
                    <div class="col-md-12">
                    <input type="text"  class="form_input" placeholder=" Enter Your Email" />
                    <button type="submit" class="bt btn-info button_subscribe"> Subscribe</button>
                    </div>
                </form>
                </div>
              </div>
             </div>
            </div>
       </div>
       
  <div class="clearfix"></div>
      <div class="our_customers">
        <div class="container">
            <div class="row">
                <div class="center">
                <div class="customers_heading">
                <h2 style="margin-bottom: 50px;"> Our Customer</h2>
                <!-- <hr style="width: 10%;"> -->
                </div>
                <div class="client_logo">
                    <div class="col-md-2 col-sm-2 col-xs-4">
                    <div class="logo">
                    <img src="<?php echo base_url('front/'); ?>images/l1.png" class="img-responsive"/>
                    </div>
                    </div>
                    <div class="col-md-2 col-sm-2 col-xs-4">
                    <div class="logo">
                    <img src="<?php echo base_url('front/'); ?>images/l2.png" class="img-responsive"/>
                    </div>
                    </div>
                    <div class="col-md-2 col-sm-2 col-xs-4">
                    <div class="logo">
                    <img src="<?php echo base_url('front/'); ?>images/l3.png" class="img-responsive"/>
                    </div>
                    </div>
                    <div class="col-md-2 col-sm-2 col-xs-4">
                    <div class="logo">
                    <img src="<?php echo base_url('front/'); ?>images/l4.png" class="img-responsive"/>
                    </div>
                    </div>
                    <div class="col-md-2 col-sm-2 col-xs-4">
                    <div class="logo">
                    <img src="<?php echo base_url('front/'); ?>images/l1.png" class="img-responsive"/>
                    </div>
                    </div>
                    <div class="col-md-2 col-sm-2 col-xs-4">
                    <div class="logo">
                    <img src="<?php echo base_url('front/'); ?>images/l2.png" class="img-responsive"/>
                    </div>
                    </div>
                </div>
                    
                </div>
            </div>
        </div>
      </div>
<div class="clearfix"></div>
<div class="testimonial">
    <div class="container">
        <div class="row">
            <div class="center">
                <div class="testimonial_heading">
                    <h2> Testimonial</h2>
                </div>
             <div class="testimonial_section">
                <div id="myCarousel" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
                 
                
                  <!-- Wrapper for slides -->
                  <div class="carousel-inner">
                    <div class="item active">
                        <div class="testimonial_feedback">
                      <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever</p>
                      <div class="circle-icon">
                            <div class="circle">
                                
                            </div>
                            
                        </div>
                      <div class="client-name">Lorem Ipsum</div>
                      </div>
                    </div>
                
                    <div class="item">
                     <div class="testimonial_feedback">
                      <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever</p>
                      <div class="circle-icon">
                            <div class="circle">
                                
                            </div>
                            
                        </div>
                      <div class="client-name">Lorem Ipsum</div>
                      </div>
                    </div>
                
                    <div class="item">
                      <div class="testimonial_feedback">
                      <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever</p>
                      <div class="circle-icon">
                            <div class="circle">
                                
                            </div>
                            
                        </div>
                      <div class="client-name">Lorem Ipsum</div>
                      </div>
                    </div>
                  </div>
                
                  <!-- Left and right controls -->
                  <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                    <span class="fa fa-chevron-left"></span>
                    <span class="sr-only">Previous</span>
                  </a>
                  <a class="right carousel-control" href="#myCarousel" data-slide="next">
                    <span class="fa fa-chevron-right"></span>
                    <span class="sr-only">Next</span>
                  </a>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>
   <footer class="footer-form">
    <div class="container">
        <div class="row">
            <div class="center">
    <div class="footer-heading">
    <h2>
    Feel Free To Ask 
    </h2>
    <h4> Contact Us</h4>
    <div class="circle-icon1">
         <div class="circle1">
               </div>
      </div>
    </div>
 <!--    <div class="footer-address">
        <div class="col-md-4 col-sm-4 col-xs-12">
            <div class="circle-middle">
            <div class="circle-heading">
                <i class="fa fa-anchor "></i>
                     </div>
                     </div>
                     <div class="address">
        <p>Lorem Ipsum is simply dummy text </p>
        </div>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-12">
            <div class="circle-middle">
            <div class="circle-heading">
                <i class="fa fa-bell"></i>
                     </div>
                     </div>
                     <div class="address">
        <p>Lorem Ipsum is simply dummy text </p>
        </div>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-12">
            <div class="circle-middle">
            <div class="circle-heading">
                <i class="fa fa-mobile-phone"></i>
                     </div>
                     </div>
                     <div class="address">
        <p>Lorem Ipsum is simply dummy text </p>
        </div>
        </div>
        
    </div> -->
    <div class="footer_fullform">
        <form id="form_last">
        <div class="row">
            <div class="col-md-4 col-sm-4 col-xs-12" style="margin-top: 16px;">
                <input type="text" class="form-control form-input" placeholder="Enter Your Name"/>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12" style="margin-top: 16px;">
                <input type="text" class="form-control form-input" placeholder="Enter Your Mail"/>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12" style="margin-top: 16px;">
                <input type="text" class="form-control form-input" placeholder="Enter Your Subject"/>
            </div>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12">
            <textarea style="display:table;" class="form-control text-comment" rows="6" id="comment" placeholder="Enter Your Messsage"></textarea>
            </div>
            <div class="middle-div">
            <button type="submit" class=" submit_button"> Submit</button>
            </div>
        </form>
    </div>
    </div>
    </div>
    </div>
    
   </footer>  
   <div class="last-footer">
        <p> IU&Go</p>
    </div>    
    
   

<script src="<?php echo base_url('front/'); ?>js/jquery-1.11.1.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.js"></script>
<script src="<?php echo base_url('front/'); ?>js/bootstrap.js"></script>
<script src="<?php echo base_url('front/'); ?>js/owl.carousel.js"></script>
<script src="<?php echo base_url('front/'); ?>js/jquery.bxslider.js"></script>
<script>

$(function() {
    $('#showdiv1').click(function() {
        $('div[id^=div]').hide();
        $('#div1').fadeIn();
    });
    $('#showdiv2').click(function() {
        $('div[id^=div]').fadeOut();
        $('#div2').fadeIn();
    });
 
    $('#showdiv3').click(function() {
        $('div[id^=div]').fadeOut();
        $('#div3').fadeIn();
    });
 
    $('#showdiv4').click(function() {
        $('div[id^=div]').fadeOut();
        $('#div4').fadeIn();
    });
 
})
</script>

<script>
function openNav() {
    document.getElementById("mySidenav").style.width = "250px";
}

function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
}
</script>
<script>
$(function(){
  $('.bxslider').bxSlider({
   
    captions: true,
   
  });
});</script>
<script>
$('.cross').click(function(){
    $('.blue-background').toggle(1000);
    })
</script>



<!-- ############################################### User login ####################################### -->

<div id="LoginModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
    <?php 
       if($this->session->flashdata('msg')){
        echo $this->session->flashdata('msg'); 
       
       }?>
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">User Login</h4>
      </div>
      <div class="modal-body">
        <form method="post" action="<?php echo base_url('welcome/login_users');?>">
      <div class="form-group">
        <label for="email">Email address:</label>
        <input type="email"  name="email" class="form-control" required>
      </div>
      <div class="form-group">
        <label for="pwd">Password:</label>
        <input type="password" name="password" class="form-control" id="pwd">
      </div>
      <div class="checkbox">
        <label><input type="checkbox">Remember me</label>
      </div>
      <button type="submit" class="btn btn-default">Submit</button>
    </form>
          </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
<!-- ############################################## User login ####################################### -->


<div id="myModa2" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
        <?php 
       if($this->session->flashdata('insertmessage')){
        echo $this->session->flashdata('insertmessage'); 
       }elseif($this->session->flashdata('email_id')){

        echo $this->session->flashdata('email_id'); 
       }


       ?>
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">User Register</h4>
      </div>
      <div class="modal-body">
        <form method="post"  action="<?php echo base_url('welcome/users_register');?>">
      <div class="form-group">
        <label for="email">User Name:</label>
        <input type="text" class="form-control" name="username" required>
      </div>
      <div class="form-group">
        <label for="pwd">Password:</label>
        <input type="password" class="form-control" name="password" required>
      </div>
      <div class="form-group">
        <label for="pwd">Email:</label>
        <input type="email" class="form-control" name="email" required>
      </div>
      <div class="form-group">
        <label for="pwd">Phone:</label>
        <input type="phone" class="form-control" name="phone" required>
      </div>

      <button type="submit" class="btn btn-default">Submit</button>
    </form>
          </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

      </div>
    </div>

  </div>
</div>
<?php 
       if($this->session->flashdata('msg')){
                
        echo "<script>
                 $(window).on('load',function(){
                        $('#LoginModal').modal('show');
                       
                    });   
            </script>";       
       }elseif($this->session->flashdata('insertmessage')){
    
        echo "<script>
                 $(window).on('load',function(){
                        $('#myModa2').modal('show');
                    });   
            </script>";

       }elseif($this->session->flashdata('email_id')){

            echo "<script>
                 $(window).on('load',function(){
                        $('#myModa2').modal('show');
                    });   
            </script>";

       }

        ?>
</body>
</html>
<script>
    $('.services').slick({
  slidesToShow: 3,
  slidesToScroll: 1,
  autoplay: true,
  autoplaySpeed: 2000,
});
</script>