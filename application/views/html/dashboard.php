



<!DOCTYPE html>

<html>

<head>

  <?php include('header.php');?>

	<meta charset="utf-8">

	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<title></title>

	<!-- <link rel="stylesheet" href="css/style.css"></link>

	<link rel="stylesheet" href="css/bootstrap.css"></link>

  <link rel="stylesheet" href="css/font-awesome.css"></link> -->

	<!-- <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css"></link> -->

	<!-- <script src="js/jquery.js"></script> -->

	<!-- <script  src="https://code.jquery.com/jquery-2.2.4.min.js"

  integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="

  crossorigin="anonymous"></script> -->

  <!-- <script src="js/bootstrap.js"></script> -->

	<!-- <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script> -->



</head>

<body>

	<section class="user_dashboard" style="margin-top: 30px; margin-bottom: 100px;">
  

<div class="dash_container container">

  <div class="row">

    <div class="user_dash_head" style="text-align: center;">

      <h1>Dashboard</h1>

      <hr>

    </div>

        <div role="tabpanel">

            <div class="col-sm-3">

                <ul class="nav nav-pills brand-pills nav-stacked" role="tablist">

                    <li role="presentation" class="brand-nav active"><a href="#tab1" aria-controls="tab1" role="tab" data-toggle="tab"><i class="fa fa-user"></i>&nbsp;&nbsp;Your Profile</a></li>

                    <li role="presentation" class="brand-nav" ><a href="#tab2" aria-controls="tab2" role="tab" data-toggle="tab"><i class="fa fa-list"></i>&nbsp;&nbsp;Your Packages</a></li>

                    <!-- <li role="presentation" class="brand-nav "><a href="#tab3" aria-controls="tab3" role="tab" data-toggle="tab"><i class="fa fa-cog"></i>&nbsp;&nbsp;Buyer List</a></li> -->

                    <li role="presentation" class="brand-nav"><a href="#tab4" aria-controls="tab4" role="tab" data-toggle="tab"><i class="fa fa-google-wallet"></i>&nbsp;&nbsp;Wallet</a></li>

                    <li role="presentation" class="brand-nav"><a href="#tab5" aria-controls="tab5" role="tab" data-toggle="tab"><i class="fa fa-cog"></i>&nbsp;&nbsp;Setting</a></li>

                </ul>

            </div>

            <div>

             <?php 

             if($this->session->flashdata('updatemessage')){

            ?>

             <div class="alert alert-success" id="display-success" style="center" width=50px; > 

            <?php  echo $this->session->flashdata('updatemessage'); 

             

             }?></div></div>

            <div class="col-sm-9">

          

         

             

                <div class="tab-content">

                 <div role="tabpanel" class="tab-pane active" id="tab1">

                  <form method="post" action="<?php echo base_url('welcome/submit_profile/').$profile_data[0]['id'];?>">



                    <?php $profile_data[0]['id'];?>

                              <div class="col-md-6">

                                

                                <div class="form-group ">

                                  <label for="first_name">User Name</label>

                        <input type="name" class="pr_edit form-control" name="username"  disabled value="<?php echo $profile_data[0]['name'];?>">

                                </div>



                                <div class="form-group ">

                                  <label for="email">Email Address</label>

                                  <input type="email" class="pr_edit form-control" name="email" disabled value="<?php echo $profile_data[0]['email'];?>">

                                </div>

                                <div class="form-group ">

                                  <label for="mobile">Mobile No</label>

                                  <input type="number" class="pr_edit form-control" name="phone" disabled value="<?php echo $profile_data[0]['phone'];?>">

                                </div>

                                <div id="sub_bt" style="display: none;">

                                  <button type="submit" class="btn btn-success">Submit</button>

                                </div>

                              </div>

                            </form>

                          <button class="btn btn-primary pull-right" id="edit">Edit</button>

                    </div>

                    <div role="tabpanel" class="tab-pane" id="tab2" style="border: 1px solid #ccc; padding: 10px; border-radius: 5px;">

                       <table class="table" id="product_table">

                        <thead>

                         <tr>

                           <th>Plan Name</th>

                           <th>Amount</th>

                           <th>City</th>

                         </tr>

                        </thead>

                            <tbody>

                           <?php foreach ($customerbooking as  $value) {   ?>



                            

                          <tr>

                            <td><?php echo $value['plan_name']; ?></td>

                            <td><?php echo $value['amount'];?></td>

                            <td><?php echo $value['city_name']; ?></td>



                          </tr>



                        <?php } ?>

                        </tbody>

                       </table>

                         

                    </div>

                    <div role="tabpanel" class="tab-pane" id="tab3">

                        <div class="buyer_table">

                          <table id="" class="table table-striped table-bordered">

                              <tbody>

                                <tr>

                                  <th>Name Of Buyer</th>

                                  <th>Produt Name</th>

                                  <th>Product Image</th>

                                  <th>Total item</th>

                                  <th>Product Description</th>

                                  <th>Total Payment</th>

                                </tr>

                                <tr>

                                  <td>Ajay</td>

                                  <td>Fastrack Watch</td>

                                  <td>Not available</td>

                                  <td>1</td>

                                  <td>Denim shade fastrack watch</td>

                                  <td>1599</td>

                                </tr>

                              </tbody> 

                          </table>

                        </div>

                    </div>

                    <div role="tabpanel" class="tab-pane" id="tab4">

                      <?php 
                          $credit_amount = 0;
                          $debit_amount = 0;
                          foreach ($wallet as $value) {
                              if ($value['transaction_type'] =="credit") {
                                    $credit_amount = $credit_amount + $value['amount'];    
                                  
                              }else{                
                                   $debit_amount = $debit_amount + $value['amount'];    
                              }
                          }

                          $amount = $credit_amount - $debit_amount;

                       ?>
                        <div class="panel-group">
                          <div class="panel panel-default">
                            <div class="wallet_amount" style="padding: 10px;">
                              <h2>Your Amount : <?php echo $amount; ?> <i class="fa fa-rupee"></i></h2>
                            </div>
                            <hr>                          
                              <h4 style="padding-left: 10px;" class="head">Add Money To Wallet</h4>
                         <form method= "post" action="<?php echo base_url('welcome/add_money');?>" style="padding: 20px; height: 200px;">
                            <div class="col-md-6">
                              <div class="form-group ">
                                <div>
                                  <label for="mobile">Amount *</label>
                                </div>
                                <input type="text" name="amount" class="form-control" value="" required>
                              </div>
                              <button type="submit" class="btn btn-primary">Add Amount</button>
                            </div>
                          </form>
                        </div>    

                          </div>
                    </div>

                    <div role="tabpanel" class="tab-pane" id="tab5">

                        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

                          <div class="panel panel-default">

                            <div class="panel-heading" role="tab" id="headingOne">

                              <h4 class="panel-title">

                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">Change Password</a>

                              </h4>

                              <div>

             <?php 

             if($this->session->flashdata('updatepass')){

            ?>

             <div class="alert alert-success" id="display-success" style="center" width=50px; > 

            <?php  echo $this->session->flashdata('updatepass'); 

             

             }?></div>

                            </div>

                            <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">

                              <div class="panel-body">

                      <form method= "post" action="<?php echo base_url('welcome/changed');?>">

                                  <div class="col-md-6">

                                    <div class="form-group ">

                                      <div>

                                        <label for="mobile">Old Password *</label>

                                      </div>

                                      <input type="text" name="old_password" class="form-control" value="" required>

                                    </div>

                                    <div class="form-group ">

                                      <div>

                                        <label for="mobile">New Password *</label>

                                      </div>

                                      <input type="text" name="new_password" class="form-control" value="">

                                    </div>

                                    <div class="form-group ">

                                      <div>

                                        <label for="mobile">Confirm New Password *</label>

                                      </div>

                                    <input type="text" name="confirm_password" class="form-control" value="">

                                    </div>

                                    <button type="submit" class="btn btn-primary">Submit</button>

                                  </div>

                                </form>

                              </div>

                            </div>

                          </div>

                          <div class="panel panel-default">

                            <div class="panel-heading" role="tab" id="headingTwo">

                              <h4 class="panel-title">

                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">

                                  Refer Your Friend

                                </a>

                              </h4>

                            </div>

                            <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">

                              <div class="panel-body">

                      <form method= "post" action="<?php echo base_url('welcome/refer');?>">

                                  <div class="col-md-6">

                                    <div class="form-group ">

                                      <div>

                                        <label for="mobile">Name*</label>

                                      </div>

                                      <input type="text" name="name" class="form-control" value="" required>

                                    </div>

                                    <div class="form-group ">

                                      <div>

                                        <label for="mobile">Email *</label>

                                      </div>

                                      <input type="email" name="email" class="form-control" value="" required>

                                    </div>

                                    <div class="form-group ">

                                      <div>

                                        <label for="mobile">Phone*</label>

                                      </div>

                                    <input type="text" name="phone" class="form-control" value="" required>

                                    </div>

                                    <button type="submit" class="btn btn-primary">Submit</button>

                                  </div>

                                </form>

                              </div>

                            </div>

                          </div>

                         <!--  <div class="panel panel-default">

                            <div class="panel-heading" role="tab" id="headingThree">

                              <h4 class="panel-title">

                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">

                                  Delete Account

                                </a>

                              </h4>

                            </div>

                            <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">

                              <div class="panel-body text-center">

                                <a href="" class="btn btn-danger">Delete Account</a>

                              </div>

                            </div>

                          </div> -->

                        </div>

                    </div>

                </div>

            </div>

        </div>

  </div>

</div>

</section>

</body>



</html>

<?php include('footer.php');?>