<?php 

if (!$_POST["txnid"]) {
   header('location:'.base_url().'welcome/index');
 } ?>
<!DOCTYPE html>
<html>
<head>
  <?php include('header.php');?>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Our Plan</title>
</head>
<style>
   /* #black_filter {
        position: absolute;
        width: 100%;
        height: 100%;
        background-color: #2282a6c7;
        z-index: 0;
        top: 0;
        left: 0;
      }
      .terms{
        padding: 10%;

      }
      .terms p {
        font: 14px 'Open Sans', sans-serif;
        font-weight: normal;
        line-height: 23px;
      }*/
      .buy_plan{
            border-radius: 4px;
            margin: 0 auto;
            width: 610px;
            text-align: left;
            z-index: 6;
            position: relative;
            background-color: #fefefe;
            border: 1px solid #e2e2e2;
            box-shadow: 0 32px 56px -22px #ccc;
      }
      .buy_plan p {
            margin: 0;
            text-align: center;
            font-weight: 500;
            border-top-left-radius: 4px;
            border-top-right-radius: 4px;
            min-height: 80px;
            background-color: #fff;
            padding: 28px 80px;
            font-size: 24px;
            line-height: 1.42;
            color: #000;
      }
      .plan_form{
        height: 250px;
        overflow: auto;
      }
      .plan_form li {
        padding: 7px 24px 7px 16px;
        box-shadow: 0 1px 0 0 #f0f0f0, 0 -1px 0 0 #f0f0f0;
        margin-bottom: 1px!important;
        margin-top: 1px!important;
        transition: all .15s ease-out;
      }
      .plan_form label {
        width: 25%;
      }
      .plan_form .form-control{
            display: initial;
            width: 70%;
      }
      .sub_pay{
        padding: 12px 16px;
        border-bottom-left-radius: 4px;
        border-bottom-right-radius: 4px;
        height: 72px;
        background-color: #f7f8fa;
        box-shadow: inset 0 1px 0 0 #e2e2e2;
      }
      .button{
            float: right;
            width: 160px;
            height: 48px;
            border-radius: 4px;
            background-color: #304ffe;
            font-size: 16px;
            text-align: center;
            color: #fff;
            position: relative;
            font-weight: 500;
      }
</style>

<body>
    <section class="user_dashboard" style="margin-top: 30px; margin-bottom: 100px;">
        <!-- <div class="sec_head">
            
            <h2 style="text-align: center;">Buy Your Plan</h2>
        </div> -->
        <div class="container">
               <?php
                  $status=$_POST["status"];
                  $firstname=$_POST["firstname"];
                  $amount=$_POST["amount"];
                  $txnid=$_POST["txnid"];

                  $posted_hash=$_POST["hash"];
                  $key=$_POST["key"];
                  $productinfo=$_POST["productinfo"];
                  $email=$_POST["email"];
                  $salt="";

                  // Salt should be same Post Request 

                  If (isset($_POST["additionalCharges"])) {
                         $additionalCharges=$_POST["additionalCharges"];
                          $retHashSeq = $additionalCharges.'|'.$salt.'|'.$status.'|||||||||||'.$email.'|'.$firstname.'|'.$productinfo.'|'.$amount.'|'.$txnid.'|'.$key;
                    } else {
                          $retHashSeq = $salt.'|'.$status.'|||||||||||'.$email.'|'.$firstname.'|'.$productinfo.'|'.$amount.'|'.$txnid.'|'.$key;
                           }
                       $hash = hash("sha512", $retHashSeq);
                    
                         if ($hash != $posted_hash) {
                           echo "Invalid Transaction. Please try again";
                         } else {
                           echo "<h3>Your order status is ". $status .".</h3>";
                           echo "<h4>Your transaction id for this transaction is ".$txnid.". Something Went Wrong Please try after some time.</h4>";
                       } 
                  ?>
        </div>
            
</section>
</body>
</html>
<?php include('footer.php');?>