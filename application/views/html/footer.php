<footer style="background: #29353e;">
  <div class="container">
    <div class="col-md-3 col-sm-3 col-xs-3" style="margin-top: 30px; margin-bottom: 30px;">
      <h4 style="color: white; margin-bottom: 15px; font-size: 28px;">Quick Links</h4>
      <p><a href="<?php echo base_url('menu/aboutus');?>" style="text-decoration: none; font-size: 16px; color: white;">About Us</a></p>
      <p><a href="<?php echo base_url('menu/plans');?>" style="text-decoration: none; font-size: 16px; color: white;">Plan</a></p>
      <p><a href="<?php echo base_url('menu/Services');?>" style="text-decoration: none; font-size: 16px; color: white;">Services</a></p>
      <p><a href="<?php echo base_url('menu/tender');?>" style="text-decoration: none; font-size: 16px; color: white;">Tender</a></p>
      <p><a href="<?php echo base_url('menu/contactus');?>" style="text-decoration: none; font-size: 16px; color: white;">Contact Us</a></p>
    </div>
    <div class="col-md-3 col-sm-3 col-xs-3" style="margin-top: 30px; margin-bottom: 30px; color: white;">
      <h4 style="color: white; margin-bottom: 15px; font-size: 28px;">Contact Us</h4>
      <p>Delhi office:A-55, FIEE Complex, Okhla Phase II, Delhi - 110020</p>
      <hr>
      <p>Noida Office: H 15, Office No.- 410 Sector 63, Noida UP - 201301</p>
    </div>
    <div class="col-md-3 col-sm-3 col-xs-3" style="color: white; margin-top: 30px; margin-bottom: 30px;">
      <h4>Reach Us</h4>
      <p>Call us on Delhi: 011-66241100 , 7840084710</p>
      <p>Email:info@iungo.in</p>
      <hr>
    </div>
    <div class="col-md-3 col-sm-3 col-xs-3" style="margin-top: 30px; margin-bottom: 30px; text-align: center;">
        <img src="<?php echo base_url('uploads/').$view_site[0]['image'];?>" alt="" style="width: 200px; height: 150px;">
      <div class="social_link" style="color: white; padding-left: 10px;">
        <div class="social_icon col-sm-3" style="background: #2b87c7;"><i class="fa fa-facebook"></i></div>
        <div class="social_icon col-sm-3" style="background:#38c0dd; "><i class="fa fa-twitter"></i></div>
        <div class="social_icon col-sm-3" style="background: #2862d8;"><i class="fa fa-linkedin"></i></div>
        <div class="social_icon col-sm-3" style="background: #dc6218; "><i class="fa fa-google-plus" ></i></div>
      </div>
      <style>
        .social_icon{
          font-size: 20px;
          padding: 10px;
          /*background: white;*/
          color: white;
          border-radius: 50%;
          margin: 10px;
          width: 40px;
          height: 40px;
        }
      </style>
    </div>
  </div>
<div class="footer_end" style="height: 100px; background: #1c2329;">
  <div class="container">
    <div class="col-md-6">
      <p style="margin-top: 40px; color: #8e9194;"> © Copyright 2017 iungo.in Designed By Backstage Supporters Pvt Ltd.</p>
    </div>
    <div class="col-md-6">
      <p style="margin-top: 40px; color: #8e9194; float: right;"><a style="text-decoration: none; color: #8e9194;" href="<?php echo base_url('menu/privacy_pol');?>"> Privacy Policy  </a></p>
    </div>
  </div>
</div>
</footer>    
    
   
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.1/jquery.min.js"></script>
<!-- <script src="<?php echo base_url('front/'); ?>js/jquery-1.11.1.js"></script> -->
<!-- <script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script> -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.js"></script>
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url('front/'); ?>js/bootstrap.js"></script>
<script src="<?php echo base_url('front/'); ?>js/owl.carousel.js"></script>
<script src="<?php echo base_url('front/'); ?>js/jquery.bxslider.js"></script>
   <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css"></link>


<script>

$(function() {
    $('#showdiv1').click(function() {
        $('div[id^=div]').hide();
        $('#div1').fadeIn();
    });
    $('#showdiv2').click(function() {
        $('div[id^=div]').fadeOut();
        $('#div2').fadeIn();
    });
 
    $('#showdiv3').click(function() {
        $('div[id^=div]').fadeOut();
        $('#div3').fadeIn();
    });
 
    $('#showdiv4').click(function() {
        $('div[id^=div]').fadeOut();
        $('#div4').fadeIn();
    });
 
})
</script>

<script>
function openNav() {
    document.getElementById("mySidenav").style.width = "250px";
}

function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
}
</script>
<script>
$(function(){
  $('.bxslider').bxSlider({
   
    captions: true,
   
  });
});</script>
<script>
$('.cross').click(function(){
    $('.blue-background').toggle(1000);
    })
</script>





<!-- ############################################### User login ####################################### -->

<div id="LoginModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
    <?php 
       if($this->session->flashdata('msg')){
        echo $this->session->flashdata('msg'); 
       
       }?>
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">User Login</h4>
      </div>
      <div class="modal-body">
        <form method="post" action="<?php echo base_url('welcome/login_users');?>">
      <div class="form-group">
        <label for="email">Email address:</label>
        <input type="email"  name="email" class="form-control" required>
      </div>
      <div class="form-group">
        <label for="pwd">Password:</label>
        <input type="password" name="password" class="form-control" id="pwd">
      </div>
      <div class="checkbox">
        <label><input type="checkbox">Remember me</label>
      </div>
      <button type="submit" class="btn btn-default">Submit</button>
    </form>
          </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
<!-- ############################################## User login ####################################### -->


<div id="myModa2" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
        <?php 
       if($this->session->flashdata('insertmessage')){
        echo $this->session->flashdata('insertmessage'); 
       }elseif($this->session->flashdata('email_id')){

        echo $this->session->flashdata('email_id'); 
       }


       ?>
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">User Register</h4>
      </div>
      <div class="modal-body">
        <form method="post"  action="<?php echo base_url('welcome/users_register');?>">
      <div class="form-group">
        <label for="email">User Name:</label>
        <input type="text" class="form-control" name="username" required>
      </div>
      <div class="form-group">
        <label for="pwd">Password:</label>
        <input type="password" class="form-control" name="password" required>
      </div>
      <div class="form-group">
        <label for="pwd">Email:</label>
        <input type="email" class="form-control" name="email" required>
      </div>
      <div class="form-group">
        <label for="pwd">Phone:</label>
        <input type="phone" class="form-control" name="phone" required>
      </div>

      <button type="submit" class="btn btn-default">Submit</button>
    </form>
          </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

      </div>
    </div>

  </div>
</div>
<?php 
       if($this->session->flashdata('msg')){
                
        echo "<script>
                 $(window).on('load',function(){
                        $('#LoginModal').modal('show');
                       
                    });   
            </script>";       
       }elseif($this->session->flashdata('insertmessage')){
    
        echo "<script>
                 $(window).on('load',function(){
                        $('#myModa2').modal('show');
                    });   
            </script>";

       }elseif($this->session->flashdata('email_id')){

            echo "<script>
                 $(window).on('load',function(){
                        $('#myModa2').modal('show');
                    });   
            </script>";

       }

        ?>

        <script>
  $(document).ready(function() {
    $('#product_table').DataTable();
} );

  $(document).ready(function(){
    $("#edit").click(function(){
      $("#sub_bt").show();
        var inputs = document.getElementsByClassName('pr_edit');
          for(var i = 0; i < inputs.length; i++) {
              inputs[i].disabled = false;
          }
    });
});

$('#display-success').fadeIn().delay(1000).fadeOut();


$(document).ready(function(){

var url = window.location.href;
var activeTab = url.substring(url.indexOf("#") + 1);
$(".tab-pane").removeClass("active in");
$("#" + activeTab).addClass("active in");
$('a[href="#'+ activeTab +'"]').tab('show')

});

    // $(document).ready(function (){

    //     var plans = $('#plan option[value='0']').text();
    //     if (plans=='1199_Speedo') {
    //         $('#amount').html('<i class="fa fa-rupee"></i>200');
    //     }
    // });


</script>
