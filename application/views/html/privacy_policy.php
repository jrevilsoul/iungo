<!DOCTYPE html>
<html>
<head>
  <?php include('header.php');?>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title></title>
</head>
<body>
   <div class="customized-template" id="customers" style="padding-bottom: 50px;">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                
                <div class="center">
                    <div class="leyered-section">
                        <div class="leyered-heading">
                        <h2 style="text-align: center; color: black;">Our Policy</h2>
                        <div class="circle-icon">
                            <div class="circle">
                                
                            </div>
                            
                        </div>
                       <div id='main'>
<section ><div class="text"><p style="text-align: justify">“Personal Information” is any data that identifies you. The personal information given to us is presumed to be true, complete and accurate in all respects and you agree to notify us immediately of any changes to that. Personal information held by IUNGO may include your name, date of birth, current and previous addresses, telephone/mobile phone number, email address, bank account or credit card details, occupation, driver’s licence number, Social Security Number and your IUNGO PIN, username or password. IUNGO also holds details of your IUNGO account.</p>
<p style="text-align: justify">We are committed to protecting your privacy. This privacy policy applies to all the web pages related to this website.</p>
<p style="text-align: justify">All the information gathered in the online forms on the website is used to personally identify users that subscribe to this service. The information will not be used for anything other that which is stated in the Terms &#038; Conditions of use for this service. None of the information will be sold or made available to anyone.</p>
<p style="text-align: justify">The Site may collect certain information about your visit, such as the Internet Protocol (IP) address through which you access the Internet; the date and time you access the Site; the pages that you access while at the Site and the Internet address of the Web site from which you linked directly to our site. This information is used to help improve the Site, analyze trends, and administer the Site.</p>
<p style="text-align: justify">We may need to change this policy from time to time in order to address new issues and reflect changes on our site. We will post those changes here so that you will always know what information we gather, how we might use that information, and whether we will disclose that information to anyone. Please refer back to this policy regularly. If you have any questions or concerns about our privacy policy, please send us an E-mail.</p>
<p style="text-align: justify">In the course of using this website and/ or availing the Services, IUNGO and its Affiliates may become privy to information of its customers, including Personally Identifiable Information (PII) such as name, date of birth, birthplace, genetic information, address, telephone number, email address, bank accounts, card numbers, identity number, Vehicle Registration Number, Driving License Number, Face/ Photograph, Digital identity. IUNGO is strongly committed to protecting the privacy of its customers and has taken all necessary and reasonable measures to protect the confidentiality of the customer information and its transmission through the web. While no data transmission over the Internet is 100% secure from intrusion, we have used and will continue to use commercially reasonable efforts to ensure the protection of your information. IUNGO will limit the collection and use of Customer Personally Identifiable Information only on a need-to-know basis to deliver better service to the customers.</p>
<p style="text-align: justify">IUNGO may use and share the information provided by Customers with its Affiliates and third parties for providing services and any service-related activities such as billing, collecting bills, notifying or contacting Customers regarding any service related matters. In this regard, it may be necessary to disclose customer information to one or more agents and contractors and their sub-contractors, but such agents, contractors, and sub-contractors will be required to agree to use the information obtained from IUNGO only for these purposes. The Customer authorizes IUNGO to exchange, share, part with all Personally Identifiable Information and transaction history of the Customers to its Affiliates/ banks/ financial institutions/ credit bureaus/ agencies/participation in any telecommunication or electronic clearing network as may be required by law, customary practice, credit reporting, statistical analysis and credit scoring, verification or risk management and shall not hold IUNGO liable for use or disclosure of this information. IUNGO will not sell or rent Customer Personally Identifiable Information to third parties.</p>
<p style="text-align: justify">IUNGO will not otherwise disclose the Personally Identifiable Information provided by Customers to third parties, unless such action is necessary to:</p>
<ul>
<li>Conform to legal requirements or comply with legal process</li>
<li>Comply with investigations of purported unlawful activities, as provided for under the Information Technology Act, 2000.</li>
<li>Protect and defend the rights, interests or property of IUNGO, and/ or its Affiliates, other customers.</li>
<li>Enforce the terms and conditions of the services provided to Customers.</li>
</ul>
<p style="text-align: justify">Your use of our services is at your own risk. The services are provided by the Company on an “as is” and “as available” basis. To the full extent permissible by applicable law, cyber site disclaims all warranties, express or implied, including, but not limited to, implied warranties of merchant-ability, fitness for a particular purpose and non-infringement. You acknowledge that any warranty that is provided in connection with any of the products or services made available on or through our websites and applications is provided solely by the owner, advertiser or manufacturer of that product and/or service and not by the Company.</p>
<p style="text-align: justify">The Company makes no representations or warranties of any kind, express or implied, as to the operation of the website or the information, content, materials, or products on the website, including that</p>
<ul>
<li>Our websites and their services will meet your requirements.</li>
<li>Our websites will be uninterrupted, timely, secure or error free.</li>
<li>The quality of any products, services, information or other material purchased or obtained by you though our websites will meet your expectations, be reliable or accurate, and errors in the software will be corrected.</li>
</ul>
<p style="text-align: justify">Your sole remedy for dissatisfaction with our websites, site-related services and/or content or information contained within the site is to stop using the site and/or its services. Any material downloaded or otherwise obtained through the use of our websites is done at your own discretion and risk and you will be solely responsible for any damage to your computer system or loss of data that results from the download of any such material.</p>
<p style="text-align: justify">It is notified to all concerned, that they agree that the jurisdiction and venue for any legal proceedings relating to this policy shall be in appropriate courts at Mumbai, India and in accordance with the applicable laws of India.</p>
<p style="text-align: justify">By using this website, you signify your acceptance of our Privacy Policy. If you do not agree to this policy, please do not use our site. Your continued use of the website following the posting of changes to these terms will mean that you accept those changes.</p>
</div></section></main>
                       
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>

<div class="clearfix"></div>
<?php include('footer.php');?>