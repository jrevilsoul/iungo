<!DOCTYPE html>
<html>
<head>
  <?php include('header.php');?>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Our Plan</title>
    <link href="<?php echo base_url('front/'); ?>plan_css/tabs.css" rel="stylesheet" media="all" type="text/css"/>
    <link href="<?php echo base_url('front/'); ?>plan_css/tabstyles.css" rel="stylesheet" media="all" type="text/css"/>
    <link href="<?php echo base_url('front/'); ?>plan_css/demo.css" rel="stylesheet" media="all" type="text/css"/>
</head>
<style>
    #black_filter {
        position: absolute;
        width: 100%;
        height: 100%;
        background-color: #2282a6c7;
        z-index: 0;
        top: 0;
        left: 0;
      }
      .terms p {
        font: 14px 'Open Sans', sans-serif;
        font-weight: normal;
        line-height: 23px;
      }
    .tab_name{
        text-decoration: none;
    }
    th ,td{
        font-size: 15px;
        text-align: center;
    }
    th{
        background:#4ab356; 
        color:white;
    }
</style>
<body>
    <section class="user_dashboard" style="padding-bottom: 100px; background: #e7ecea;">
    <div class="plan">
    <div class="plan_head">
        <div class="jumbotron" style="background: url('<?php echo base_url('front/images/plan_slide.jpg'); ?>') no-repeat;background-size: 100% 100%;position: relative;padding: 150px;">
          <div id="black_filter"></div>
          <!-- <div class="container">
            <h1 style="text-align: center;color: white;position: relative;z-index: 3; text-transform: uppercase; letter-spacing: 5px;">Our Plans</h1>
          </div> -->
        </div>
    </div>
    <div class="container">
        <div class="modern-heading">
            <h2 style="font-size: 45px; font-family: serif;">Our Plan</h2>
            <div class="circle-icon">
                <div class="circle">
                    
                </div>
                        
            </div>
        </div>
    <section>
       <div class="tabs tabs-style-iconbox">
           <nav>
               <ul>
                   <li class="tab-current"><a class="tab_name" href="#section-iconbox-1" ><span><i class="fa fa-wifi"></i><br>Noida</span></a></li>
                   <li><a class="tab_name" href="#section-iconbox-2" ><span><i class="fa fa-wifi"></i><br>Greater Noida</span></a></li>
                   <li><a class="tab_name" href="#section-iconbox-3" ><span><i class="fa fa-wifi"></i><br>Panchkula</span></a></li>
                   <li><a class="tab_name" href="#section-iconbox-4" ><span><i class="fa fa-wifi"></i><br>Dehradun</span></a></li>
                   <li><a class="tab_name" href="#section-iconbox-5" ><span><i class="fa fa-wifi"></i><br>Sikkim</span></a></li>
               </ul>
           </nav>
           <div class="content-wrap">
               <section id="section-iconbox-1">
                   <table class="table table-hover table-bordered" style="margin-top: 50px;">
                     <tbody>
                         
                         <tr>
                             <th>S.no</th>
                             <th>Plan Name</th>
                             <th>Primary Speed</th>
                             <th>Data Limit</th>
                             <th>Monthly Charges</th>
                         </tr>
                         <tr>
                             <td>1</td>
                             <td>1199_Speedo</td>
                             <td>10 Mbps</td>
                             <td>100 GB</td>
                             <td><i class="fa fa-rupee"></i>&nbsp; 1499</td>
                         </tr>
                         <tr>
                             <td>2</td>
                             <td>1999_Super</td>
                             <td>20 Mbps</td>
                             <td>150 GB</td>
                             <td><i class="fa fa-rupee"></i>&nbsp; 1999</td>
                         </tr>
                         <tr>
                             <td>3</td>
                             <td>2499_Excellent</td>
                             <td>30 Mbps</td>
                             <td>250 GB</td>
                             <td><i class="fa fa-rupee"></i>&nbsp; 2499</td>
                         </tr>
                         <tr>
                             <td>4</td>
                             <td>3599_Speedo</td>
                             <td>50 Mbps</td>
                             <td>300 GB</td>
                             <td><i class="fa fa-rupee"></i>&nbsp; 3599</td>
                         </tr>
                     </tbody>
                 </table>
               </section>
               <section id="section-iconbox-2">
                    <h2 style="text-align: center; margin-top: 50px;">Domestic Plans - Unlimited</h2>
                   <table class="table table-hover table-bordered" style="margin-top: 50px;">
                     <tbody>
                         
                         <tr>
                             <th>S.no</th>
                             <th>Plan Name</th>
                             <th>Primary Speed</th>
                             <th>Data Limit</th>
                             <th>Price</th>
                         </tr>
                         <tr>
                             <td>1</td>
                             <td>Plan 1</td>
                             <td>1 Mbps</td>
                             <td>Unlimited</td>
                             <td><i class="fa fa-rupee"></i>&nbsp;399</td>
                         </tr>
                         <tr>
                             <td>2</td>
                             <td>Plan 2</td>
                             <td>2 Mbps</td>
                             <td>Unlimited</td>
                             <td><i class="fa fa-rupee"></i>&nbsp;499</td>
                         </tr>
                         <tr>
                             <td>3</td>
                             <td>Plan 3</td>
                             <td>4 Mbps</td>
                             <td>Unlimited</td>
                             <td><i class="fa fa-rupee"></i>&nbsp;599</td>
                         </tr>
                         <tr>
                             <td>4</td>
                             <td>Plan 4</td>
                             <td>5 Mbps</td>
                             <td>Unlimited</td>
                             <td><i class="fa fa-rupee"></i>&nbsp;699</td>
                         </tr>
                         <tr>
                             <td>5</td>
                             <td>Plan 5</td>
                             <td>6 Mbps</td>
                             <td>Unlimited</td>
                             <td><i class="fa fa-rupee"></i>&nbsp;799</td>
                         </tr>
                         <tr>
                             <td>6</td>
                             <td>Plan 6</td>
                             <td>8 Mbps</td>
                             <td>Unlimited</td>
                             <td><i class="fa fa-rupee"></i>&nbsp;899</td>
                         </tr>
                         <tr>
                             <td>7</td>
                             <td>Plan 7</td>
                             <td>10 Mbps</td>
                             <td>Unlimited</td>
                             <td><i class="fa fa-rupee"></i>&nbsp;999</td>
                         </tr>
                         <tr>
                             <td>8</td>
                             <td>Plan 8</td>
                             <td>12 Mbps</td>
                             <td>Unlimited</td>
                             <td><i class="fa fa-rupee"></i>&nbsp;1299</td>
                         </tr>
                         <tr>
                             <td>9</td>
                             <td>Plan 9</td>
                             <td>15 Mbps</td>
                             <td>Unlimited</td>
                             <td><i class="fa fa-rupee"></i>&nbsp;1499</td>
                         </tr>
                     </tbody>
                 </table>

                 <h2 style="text-align: center; margin-top: 50px;">Corporate Plans -  Unlimited</h2>
                   <table class="table table-hover table-bordered" style="margin-top: 50px;">
                     <tbody>                         
                         <tr>
                             <th>S.no</th>
                             <th>Plan Name</th>
                             <th>Primary Speed</th>
                             <th>Data Limit</th>
                             <th>Price</th>
                         </tr>
                         <tr>
                             <td>1</td>
                             <td>Plan 1</td>
                             <td>1 Mbps</td>
                             <td>Unlimited</td>
                             <td><i class="fa fa-rupee"></i>&nbsp;1199</td>
                         </tr>
                         <tr>
                             <td>2</td>
                             <td>Plan 2</td>
                             <td>2 Mbps</td>
                             <td>Unlimited</td>
                             <td><i class="fa fa-rupee"></i>&nbsp;2099</td>
                         </tr>
                         <tr>
                             <td>3</td>
                             <td>Plan 3</td>
                             <td>4 Mbps</td>
                             <td>Unlimited</td>
                             <td><i class="fa fa-rupee"></i>&nbsp;4099</td>
                         </tr>
                         <tr>
                             <td>4</td>
                             <td>Plan 4</td>
                             <td>5 Mbps</td>
                             <td>Unlimited</td>
                             <td><i class="fa fa-rupee"></i>&nbsp;5099</td>
                         </tr>
                         <tr>
                             <td>5</td>
                             <td>Plan 5</td>
                             <td>6 Mbps</td>
                             <td>Unlimited</td>
                             <td><i class="fa fa-rupee"></i>&nbsp;6099</td>
                         </tr>
                         <tr>
                             <td>6</td>
                             <td>Plan 6</td>
                             <td>8 Mbps</td>
                             <td>Unlimited</td>
                             <td><i class="fa fa-rupee"></i>&nbsp;8099</td>
                         </tr>
                         <tr>
                             <td>7</td>
                             <td>Plan 7</td>
                             <td>10 Mbps</td>
                             <td>Unlimited</td>
                             <td><i class="fa fa-rupee"></i>&nbsp;10099</td>
                         </tr>
                        
                     </tbody>
                 </table>

               </section>
               <section id="section-iconbox-3">
                   <h2 style="text-align: center; margin-top: 50px;">Domestic Plans - Unlimited</h2>
                   <table class="table table-hover table-bordered" style="margin-top: 50px;">
                     <tbody>
                         
                         <tr>
                             <th>S.no</th>
                             <th>Plan Name</th>
                             <th>Primary Speed</th>
                             <th>Secondary Speed</th>
                             <th>Data Limit</th>
                             <th>Price 1 Month</th>
                             <th>Price 3 Month</th>
                             <th>Price 6 Month</th>
                             <th>Price 12 Month</th>
                         </tr>
                         <tr>
                             <td>1</td>
                             <td>512KB_UL_1M_PKL</td>
                             <td>512 KB</td>
                             <td>N/A</td>
                             <td>Unlimited</td>
                             <td><i class="fa fa-rupee"></i>&nbsp; 555</td>
                             <td><i class="fa fa-rupee"></i>&nbsp; 1664</td>
                             <td><i class="fa fa-rupee"></i>&nbsp; 3328</td>
                             <td><i class="fa fa-rupee"></i>&nbsp; 6655</td>
                         </tr>
                         <tr>
                             <td>2</td>
                             <td>1MB_UL_1M_PKL</td>
                             <td>1 MB</td>
                             <td>N/A</td>
                             <td>Unlimited</td>
                             <td><i class="fa fa-rupee"></i>&nbsp; 708</td>
                             <td><i class="fa fa-rupee"></i>&nbsp; 2124</td>
                             <td><i class="fa fa-rupee"></i>&nbsp; 4248</td>
                             <td><i class="fa fa-rupee"></i>&nbsp; 8496</td>
                         </tr>
                         <tr>
                             <td>3</td>
                             <td>2MB_UL_1M_PKL</td>
                             <td>2 MB</td>
                             <td>N/A</td>
                             <td>Unlimited</td>
                             <td><i class="fa fa-rupee"></i>&nbsp; 826</td>
                             <td><i class="fa fa-rupee"></i>&nbsp; 2478</td>
                             <td><i class="fa fa-rupee"></i>&nbsp; 4956</td>
                             <td><i class="fa fa-rupee"></i>&nbsp; 9912</td>
                         </tr>
                         <tr>
                             <td>4</td>
                             <td>3MB_UL_1M_PKL</td>
                             <td>3 MB</td>
                             <td>N/A</td>
                             <td>Unlimited</td>
                             <td><i class="fa fa-rupee"></i>&nbsp; 1003</td>
                             <td><i class="fa fa-rupee"></i>&nbsp; 3009</td>
                             <td><i class="fa fa-rupee"></i>&nbsp; 6018</td>
                             <td><i class="fa fa-rupee"></i>&nbsp; 12036</td>
                         </tr>
                         <tr>
                             <td>5</td>
                             <td>4MB_UL_1M_PKL</td>
                             <td>4 MB</td>
                             <td>N/A</td>
                             <td>Unlimited</td>
                             <td><i class="fa fa-rupee"></i>&nbsp; 1180</td>
                             <td><i class="fa fa-rupee"></i>&nbsp; 3540</td>
                             <td><i class="fa fa-rupee"></i>&nbsp; 7080</td>
                             <td><i class="fa fa-rupee"></i>&nbsp; 14160</td>
                         </tr>
                         <tr>
                             <td>6</td>
                             <td>5MB_UL_1M_PKL</td>
                             <td>5 MB</td>
                             <td>N/A</td>
                             <td>Unlimited</td>
                             <td><i class="fa fa-rupee"></i>&nbsp; 1328</td>
                             <td><i class="fa fa-rupee"></i>&nbsp; 3983</td>
                             <td><i class="fa fa-rupee"></i>&nbsp; 7965</td>
                             <td><i class="fa fa-rupee"></i>&nbsp; 15930</td>
                         </tr>
                         <tr>
                             <td>7</td>
                             <td>6MB_UL_1M_PKL</td>
                             <td>6 MB</td>
                             <td>N/A</td>
                             <td>Unlimited</td>
                             <td><i class="fa fa-rupee"></i>&nbsp; 1475</td>
                             <td><i class="fa fa-rupee"></i>&nbsp; 4425</td>
                             <td><i class="fa fa-rupee"></i>&nbsp; 8850</td>
                             <td><i class="fa fa-rupee"></i>&nbsp; 17700</td>
                         </tr>
                         <tr>
                             <td>8</td>
                             <td>7MB_UL_1M_PKL</td>
                             <td>7 MB</td>
                             <td>N/A</td>
                             <td>Unlimited</td>
                             <td><i class="fa fa-rupee"></i>&nbsp; 1623</td>
                             <td><i class="fa fa-rupee"></i>&nbsp; 4868</td>
                             <td><i class="fa fa-rupee"></i>&nbsp; 9735</td>
                             <td><i class="fa fa-rupee"></i>&nbsp; 19470</td>
                         </tr>
                     </tbody>
                 </table>

                 <h2 style="text-align: center; margin-top: 50px;">Domestic Plans - FUP Unlimited</h2>
                   <table class="table table-hover table-bordered" style="margin-top: 50px;">
                     <tbody>
                         
                         <tr>
                             <th>S.no</th>
                             <th>Plan Name</th>
                             <th>Primary Speed</th>
                             <th>Secondary Speed</th>
                             <th>Data 1 Month</th>
                             <th>Price 1 Month</th>
                             <th>Data 3 Month</th>
                             <th>Price 3 Month</th>
                             <th>Data 6 Month</th>
                             <th>Price 6 Month</th>
                             <th>Data 12 Month</th>
                             <th>Price 12 Month</th>
                         </tr>
                         <tr>
                             <td>1</td>
                             <td>2MB_512KB_40GB_1M_PKL</td>
                             <td>2MB</td>
                             <td>512KB</td>
                             <td>40GB</td>
                             <td><i class="fa fa-rupee"></i>&nbsp; 732</td>
                             <td>120GB</td>
                             <td><i class="fa fa-rupee"></i>&nbsp; 2195</td>
                             <td>240GB</td>
                             <td><i class="fa fa-rupee"></i>&nbsp; 4390</td>
                             <td>480GB</td>
                             <td><i class="fa fa-rupee"></i>&nbsp; 8779</td>
                         </tr>
                         <tr>
                             <td>2</td>
                             <td>4MB_1MB_50GB_1M_PKL</td>
                             <td>4MB</td>
                             <td>1MB</td>
                             <td>50GB</td>
                             <td><i class="fa fa-rupee"></i>&nbsp; 838</td>
                             <td>150GB</td>
                             <td><i class="fa fa-rupee"></i>&nbsp; 2513</td>
                             <td>300GB</td>
                             <td><i class="fa fa-rupee"></i>&nbsp; 5027</td>
                             <td>600GB</td>
                             <td><i class="fa fa-rupee"></i>&nbsp; 10054</td>
                         </tr>
                         <tr>
                             <td>3</td>
                             <td>6MB_1MB_60GB_1M_PKL</td>
                             <td>6MB</td>
                             <td>1MB</td>
                             <td>60GB</td>
                             <td><i class="fa fa-rupee"></i>&nbsp; 915</td>
                             <td>180GB</td>
                             <td><i class="fa fa-rupee"></i>&nbsp; 2744</td>
                             <td>360GB</td>
                             <td><i class="fa fa-rupee"></i>&nbsp; 5487</td>
                             <td>720GB</td>
                             <td><i class="fa fa-rupee"></i>&nbsp; 10974</td>
                         </tr>
                         <tr>
                             <td>4</td>
                             <td>8MB_2MB_70GB_1M_PKL</td>
                             <td>8MB</td>
                             <td>2MB</td>
                             <td>70GB</td>
                             <td><i class="fa fa-rupee"></i>&nbsp; 1239</td>
                             <td>210GB</td>
                             <td><i class="fa fa-rupee"></i>&nbsp; 3717</td>
                             <td>420GB</td>
                             <td><i class="fa fa-rupee"></i>&nbsp; 7434</td>
                             <td>960GB</td>
                             <td><i class="fa fa-rupee"></i>&nbsp; 14868</td>
                         </tr>
                         <tr>
                             <td>5</td>
                             <td>10MB_3MB_80GB_1M_PKL</td>
                             <td>10MB</td>
                             <td>3MB</td>
                             <td>80GB</td>
                             <td><i class="fa fa-rupee"></i>&nbsp; 1357</td>
                             <td>240GB</td>
                             <td><i class="fa fa-rupee"></i>&nbsp; 4071</td>
                             <td>480GB</td>
                             <td><i class="fa fa-rupee"></i>&nbsp; 8142</td>
                             <td>960GB</td>
                             <td><i class="fa fa-rupee"></i>&nbsp; 16284</td>
                         </tr>
                         <tr>
                             <td>6</td>
                             <td>12MB_4MB_100GB_1M_PKL</td>
                             <td>12MB</td>
                             <td>4MB</td>
                             <td>100GB</td>
                             <td><i class="fa fa-rupee"></i>&nbsp; 1475</td>
                             <td>300GB</td>
                             <td><i class="fa fa-rupee"></i>&nbsp; 4425</td>
                             <td>600GB</td>
                             <td><i class="fa fa-rupee"></i>&nbsp; 8850</td>
                             <td>1200GB</td>
                             <td><i class="fa fa-rupee"></i>&nbsp; 17700</td>
                         </tr>
                     </tbody>
                 </table>

                 <h2 style="text-align: center; margin-top: 50px;">Corporate Office Plans</h2>
                   <table class="table table-hover table-bordered" style="margin-top: 50px;">
                     <tbody>
                         
                         <tr>
                             <th>S.no</th>
                             <th>Plan Name</th>
                             <th>Primary Speed</th>
                             <th>Secondary Speed</th>
                             <th>Data Limit</th>
                             <th>Price 1 Month</th>
                             <th>Price 3 Month</th>
                             <th>Price 6 Month</th>
                             <th>Price 12 Month</th>
                        </tr>
                         <tr>
                             <td>1</td>
                             <td>3MB_512KB_40GB_1M_CORP_ZRK</td>
                             <td>3MB</td>
                             <td>512KB</td>
                             <td>40GB</td>
                             <td><i class="fa fa-rupee"></i> 802</td>
                             <td>N/A</td>
                             <td>N/A</td>
                             <td>N/A</td>
                         </tr>
                         <tr>
                             <td>2</td>
                             <td>5MB_1MB_50GB_1M_CORP_ZRK</td>
                             <td>5MB</td>
                             <td>1MB</td>
                             <td>50GB</td>
                             <td><i class="fa fa-rupee"></i> 1263</td>
                             <td>N/A</td>
                             <td>N/A</td>
                             <td>N/A</td>
                         </tr>
                         <tr>
                             <td>1</td>
                             <td>7MB_2MB_60GB_1M_CORP_ZRK</td>
                             <td>7MB</td>
                             <td>2MB</td>
                             <td>60GB</td>
                             <td><i class="fa fa-rupee"></i> 1735</td>
                             <td>N/A</td>
                             <td>N/A</td>
                             <td>N/A</td>
                         </tr>
                     </tbody>
                 </table>
               </section>
               <section id="section-iconbox-4">
                   <h2 style="text-align: center; margin-top: 50px;">Home Plans</h2>
                   <table class="table table-hover table-bordered" style="margin-top: 50px;">
                     <tbody>
                         <tr>
                                <th>S.No.</th>
                            <th>Primary Speed</th>
                            <th>Secondry Speed</th>
                            <th>Data Limit</th>
                            <th>Download</th>
                            <th>Price</th>
                       </tr>
                       <tr>
                            <td>1</td>
                           <td>4 Mbps</td>
                           <td>1 Mbps</td>
                           <td>20 GB</td>
                           <td>Unlimited</td>
                           <td><i class="fa fa-rupee"></i> 564</td>
                       </tr>
                       <tr>
                            <td>2</td>
                           <td>7 Mbps</td>
                           <td>2 Mbps</td>
                           <td>30 GB</td>
                           <td>Unlimited</td>
                           <td><i class="fa fa-rupee"></i> 720</td>
                       </tr>
                       <tr>
                            <td>3</td>
                           <td>8 Mbps</td>
                           <td>3 Mbps</td>
                           <td>50 GB</td>
                           <td>Unlimited</td>
                           <td><i class="fa fa-rupee"></i> 1130</td>
                       </tr>
                       <tr>
                            <td>4</td>
                           <td>10 Mbps</td>
                           <td>4 Mbps</td>
                           <td>70 GB</td>
                           <td>Unlimited</td>
                           <td><i class="fa fa-rupee"></i> 1690</td>
                       </tr>
                       <tr>
                            <td>5</td>
                           <td>12 Mbps</td>
                           <td>5 Mbps</td>
                           <td>100 GB</td>
                           <td>Unlimited</td>
                           <td><i class="fa fa-rupee"></i> 2820</td>
                       </tr>
                     </tbody>
                 </table>
                 <h2 style="text-align: center; margin-top: 50px;">Office Plans</h2>
                   <table class="table table-hover table-bordered" style="margin-top: 50px;">
                     <tbody>
                         <tr>
                                <td>S.No.</td>
                            <th>Primary Speed</th>
                            <th>Secondry Speed</th>
                            <th>Data Limit</th>
                            <th>Download</th>
                            <th>Price</th>
                       </tr>
                       <tr>
                            <td>1</td>
                           <td>4 Mbps</td>
                           <td>1 Mbps</td>
                           <td>35 GB</td>
                           <td>Unlimited</td>
                           <td><i class="fa fa-rupee"></i> 845</td>
                       </tr>
                       <tr>
                            <td>2</td>
                           <td>7 Mbps</td>
                           <td>2 Mbps</td>
                           <td>55 GB</td>
                           <td>Unlimited</td>
                           <td><i class="fa fa-rupee"></i> 1299</td>
                       </tr>
                       <tr>
                            <td>3</td>
                           <td>8 Mbps</td>
                           <td>3 Mbps</td>
                           <td>85 GB</td>
                           <td>Unlimited</td>
                           <td><i class="fa fa-rupee"></i> 2120</td>
                       </tr>
                       <tr>
                            <td>4</td>
                           <td>10 Mbps</td>
                           <td>4 Mbps</td>
                           <td>110 GB</td>
                           <td>Unlimited</td>
                           <td><i class="fa fa-rupee"></i> 3170</td>
                       </tr>
                       <tr>
                            <td>5</td>
                           <td>12 Mbps</td>
                           <td>5 Mbps</td>
                           <td>130 GB</td>
                           <td>Unlimited</td>
                           <td><i class="fa fa-rupee"></i> 5285</td>
                       </tr>

                     </tbody>
                 </table>
               </section>
               <section id="section-iconbox-5">
                   <!-- <h2 style="text-align: center; margin-top: 50px;">Domestic Plans - Unlimited</h2> -->
                   <table class="table table-hover table-bordered" style="margin-top: 50px;">
                     <tbody>
                         
                         <tr>
                             <th>S.no</th>
                             <th>Plan Name</th>
                             <th>Primary Speed</th>
                             <th>FUP</th>
                             <th>Price Including GST 18%</th>
                         </tr>
                         <tr>
                             <td>1</td>
                             <td>512KB_5GB_1M_SK</td>
                             <td>512Kbps</td>
                             <td>512Kbps</td>
                             <td><i class="fa fa-rupee"></i>&nbsp;589</td>
                         </tr>
                         <tr>
                             <td>2</td>
                             <td>512KB_UL_1M_SK</td>
                             <td>512Kbps</td>
                             <td>512Kbps</td>
                             <td><i class="fa fa-rupee"></i>&nbsp;1061</td>
                         </tr>
                         <tr>
                             <td>3</td>
                             <td>1Mbps</td>
                             <td>1MB_5GB_1M_SK</td>
                             <td>1MPs</td>
                             <td><i class="fa fa-rupee"></i>&nbsp;1002</td>
                         </tr>
                         <tr>
                             <td>4</td>
                             <td>1MB_UL_1M_SK</td>
                             <td>1Mbps</td>
                             <td>Unlimited</td>
                             <td><i class="fa fa-rupee"></i>&nbsp;1769</td>
                         </tr>
                         <tr>
                             <td>5</td>
                             <td>2MB_20GB_512KB_1M_SK</td>
                             <td>2Mbps</td>
                             <td>512Kbps</td>
                             <td><i class="fa fa-rupee"></i>&nbsp;2123</td>
                         </tr>
                         <tr>
                             <td>6</td>
                             <td>2MB_40GB_512KB_1M_SK</td>
                             <td>2Mbps</td>
                             <td>512Kbps</td>
                             <td><i class="fa fa-rupee"></i>&nbsp;2595</td>
                         </tr>
                         <tr>
                             <td>7</td>
                             <td>2MB_UL_1M_SK</td>
                             <td>2Mbps</td>
                             <td>Unlimited</td>
                             <td><i class="fa fa-rupee"></i>&nbsp;4719</td>
                         </tr>
                         <tr>
                             <td>8</td>
                             <td>4MB_45GB_512KB_1M_SK</td>
                             <td>4Mbps</td>
                             <td>Unlimited</td>
                             <td><i class="fa fa-rupee"></i>&nbsp;3185</td>
                         </tr>
                         <tr>
                             <td>9</td>
                             <td>4MB_UL_1M_SK</td>
                             <td>4Mbps</td>
                             <td>Unlimited</td>
                             <td><i class="fa fa-rupee"></i>&nbsp;7079</td>
                         </tr>
                         <tr>
                             <td>10</td>
                             <td>1MB_UL _SK_NEW</td>
                             <td>1Mbps</td>
                             <td>Unlimited</td>
                             <td><i class="fa fa-rupee"></i>&nbsp;1231</td>
                         </tr>
                         <tr>
                             <td>11</td>
                             <td>2MB_UL_SK_1M</td>
                             <td>2Mbps</td>
                             <td>Unlimited</td>
                             <td><i class="fa fa-rupee"></i>&nbsp;1769</td>
                         </tr>
                         <tr>
                             <td>12</td>
                             <td>2MB_UL_1M_SK_NEW</td>
                             <td>2Mbps</td>
                             <td>Unlimited</td>
                             <td><i class="fa fa-rupee"></i>&nbsp;3079</td>
                         </tr>
                         <tr>
                             <td>13</td>
                             <td>3MB_UL_SK_1M</td>
                             <td>3Mbps</td>
                             <td>Unlimited</td>
                             <td><i class="fa fa-rupee"></i>&nbsp;3079</td>
                         </tr>
                         <tr>
                             <td>14</td>
                             <td>2MB_UL_1M_SK_NEW</td>
                             <td>2Mbps</td>
                             <td>Unlimited</td>
                             <td><i class="fa fa-rupee"></i>&nbsp;2441</td>
                         </tr>
                         <tr>
                             <td>15</td>
                             <td>4MB_UL_1M_SK_NEW</td>
                             <td>4Mbps</td>
                             <td>Unlimited</td>
                             <td><i class="fa fa-rupee"></i>&nbsp;4617</td>
                         </tr>
                         <tr>
                             <td>16</td>
                             <td>10_MB_BB_SK</td>
                             <td>10Mbps</td>
                             <td>Unlimited</td>
                             <td><i class="fa fa-rupee"></i>&nbsp;11800</td>
                         </tr>
                         <tr>
                             <td>17</td>
                             <td>4MB_1M_SK_GOLCON</td>
                             <td>4Mbps</td>
                             <td>Unlimited</td>
                             <td><i class="fa fa-rupee"></i>&nbsp;5747</td>
                         </tr>
                         <tr>
                             <td>18</td>
                             <td>2MB_UL_1M_LL_SK</td>
                             <td>2Mbps</td>
                             <td>Unlimited</td>
                             <td><i class="fa fa-rupee"></i>&nbsp;14160</td>
                         </tr>
                         <tr>
                             <td>19</td>
                             <td>4MB_LL_1M_SK</td>
                             <td>4Mbps</td>
                             <td>Unlimited</td>
                             <td><i class="fa fa-rupee"></i>&nbsp;14160</td>
                         </tr>
                         <tr>
                             <td>20</td>
                             <td>4MB_DEMO_1M_SK</td>
                             <td>4Mbps</td>
                             <td>Unlimited</td>
                             <td><i class="fa fa-rupee"></i>&nbsp;0</td>
                         </tr>
                         <tr>
                             <td>21</td>
                             <td>DEMO_2MB_UL_12M_SK</td>
                             <td>2Mbps</td>
                             <td>Unlimited</td>
                             <td><i class="fa fa-rupee"></i>&nbsp;0</td>
                         </tr>
                         <tr>
                             <td>22</td>
                             <td>1MB_DEMO_12M_SK</td>
                             <td>1Mbps</td>
                             <td>Unlimited</td>
                             <td><i class="fa fa-rupee"></i>&nbsp;0</td>
                         </tr>
                        
                     </tbody>
                 </table>
               </section>
           </div>
       </div>
   </section>
   </div>

   <!-- <div class="buy_plan container-fluid" style="text-align: center;">
       <a href="" class="btn btn-lg btn-success">Buy Plan</a>
   </div> -->
<div class="container">
    <div class="terms">
        
    <p><strong> Terms &amp; Conditions:</strong></p>
    <p>1.) All prices are inclusive of all taxes.</p>
    <p>2.) Above Plans are on Pre-Pay basis</p>
    <p>3.) Installation charges extra as applicable.</p>
   <!--  <p>4.) Internet speed indicated only upto ISP node.</p>
    <p>5.) Check download &amp; upload speed on ynetworks.speedtest.net. </p>
    <p>6.) Unlimited movie, video &amp; data in realtime.</p> -->
    <p></p>
    </div>
    </div>
</section>
</body>
</html>
<?php include('footer.php');?>
<script src="<?php echo base_url('front/'); ?>plan_css/plan_custom.js"></script>
<script src="<?php echo base_url('front/'); ?>plan_css/cbpFWTabs.js"></script>
<script>
    (function() {

        [].slice.call( document.querySelectorAll( '.tabs' ) ).forEach( function( el ) {
            new CBPFWTabs( el );
        });

    })();
</script>