<!DOCTYPE html>
<html>
<head>
  <?php include('header.php');?>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Our Plan</title>
</head>
<style>
   /* #black_filter {
        position: absolute;
        width: 100%;
        height: 100%;
        background-color: #2282a6c7;
        z-index: 0;
        top: 0;
        left: 0;
      }
      .terms{
        padding: 10%;

      }
      .terms p {
        font: 14px 'Open Sans', sans-serif;
        font-weight: normal;
        line-height: 23px;
      }*/
      .buy_plan{
            border-radius: 4px;
            margin: 0 auto;
            width: 610px;
            text-align: left;
            z-index: 6;
            position: relative;
            background-color: #fefefe;
            border: 1px solid #e2e2e2;
            box-shadow: 0 32px 56px -22px #ccc;
      }
      .buy_plan p {
            margin: 0;
            text-align: center;
            font-weight: 500;
            border-top-left-radius: 4px;
            border-top-right-radius: 4px;
            min-height: 80px;
            background-color: #fff;
            padding: 28px 80px;
            font-size: 24px;
            line-height: 1.42;
            color: #000;
      }
      .plan_form{
        height: 250px;
        overflow: auto;
      }
      .plan_form li {
        padding: 7px 24px 7px 16px;
        box-shadow: 0 1px 0 0 #f0f0f0, 0 -1px 0 0 #f0f0f0;
        margin-bottom: 1px!important;
        margin-top: 1px!important;
        transition: all .15s ease-out;
      }
      .plan_form label {
        width: 25%;
      }
      .plan_form .form-control{
            display: initial;
            width: 70%;
      }
      .sub_pay{
        padding: 12px 16px;
        border-bottom-left-radius: 4px;
        border-bottom-right-radius: 4px;
        height: 72px;
        background-color: #f7f8fa;
        box-shadow: inset 0 1px 0 0 #e2e2e2;
      }
      .button{
            float: right;
            width: 160px;
            height: 48px;
            border-radius: 4px;
            background-color: #304ffe;
            font-size: 16px;
            text-align: center;
            color: #fff;
            position: relative;
            font-weight: 500;
      }
</style>

<body>
    <section class="user_dashboard" style="margin-top: 30px; margin-bottom: 100px;">
        <!-- <div class="sec_head">
            
            <h2 style="text-align: center;">Buy Your Plan</h2>
        </div> -->
        <div class="container">
            <div class="buy_plan">
                <p>Buy Your Plan</p>
                <div class="plan_form">
                    <form method="post" action="<?php echo base_url('Menu/user_detail');?>">
                        <ul>
                            <li>
                                 <label for="Country">Select Your City</label>
                                        <select class="form-control" id="city_id" name="city_id" required onChange="getcitydetails(this.value)" />
                                        <option value="">-- SELECT --</option>

                                <?php if($view_city >0){

                                  foreach($view_city as $data){?>

                                        <option value="<?php echo $data['id'];?>"><?php echo $data['city_name'];?></option>

                                <?php }

                                }?>

                                        </select>  

                            </li>
                            <div id="plan_data"></div>
                            <div id="amount_data"></div>
                            <!-- <li>
                                <label for="amount">Amount</label>
                                <span  id="amount"></span>
                            </li>
                            <li>
                                <label for="data">Data Limit</label>
                                <span id="data"></span>
                            </li>
                            <li>
                                <label for="type">Plan Type</label>
                                <span id="type">Unlimited</span>
                            </li> -->

                        </ul>
                        <!-- <div class="sub_pay">
                           <input type="submit" class="button" value="Pay Now"> 
                        </div> -->
                    </form>
                </div>   
            </div> 
        </div>
            
</section>
</body>
</html>
<?php include('footer.php');?>

<script>
  function getcitydetails(id){
       $.ajax({
            type: "POST",
            url: '<?php echo site_url('dashboard/plan_data').'/';?>'+id,
            data: 'city_id ='+id,
            success: function(data){
                $('#plan_data').html(data);

        },

        });
}

  function getplandetail(id){
       $.ajax({
            type: "POST",
            url: '<?php echo site_url('dashboard/plan_data2').'/';?>'+id,
            data: 'plan_id ='+id,
            success: function(data){
                $('#amount_data').html(data);

        },

        });
}
</script>