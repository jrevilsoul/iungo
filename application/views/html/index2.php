<?php include('header.php');
error_reporting(0);
?>

<style>
    
.slick-next:before, .slick-prev:before {
    color: black;

}
.i4ewOd-pzNkMb-haAclf{
    display: none !important;
}
.service:hover{
    background: #f1f1f1;
    box-shadow: 1px 0px 6px 2px #00000057;
    padding: 6px;
    transition: all 0.5s;
}
table{
  border-collapse: unset;
}
</style>
<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
    <li data-target="#carousel-example-generic" data-slide-to="3"></li>
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">
    <?php 
    $i = 1;
    foreach ($view_banner as  $value) {         ?>
    <div class="item <?php if($i == 1){ echo "active" ;}  ?> ">
      <img src="<?php echo base_url('banners/').$value['image']; ?>" style="height: 400px; width: 100%;" class="img-responsive"/>
    </div>

  <?php $i++; } ?>
  </div>

  <!-- Controls -->
  <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
    <span class="fa fa-chevron-left" style="margin-top: 100%; font-size: 30px;" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
    <span class="fa fa-chevron-right" style="margin-top: 100%; font-size: 30px;" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
<div class="clearfix"></div>
<div class="customized-template" id="services" style="padding-bottom: 50px; background: #F9F9FF;">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                
                <div class="center">
                    <div class="leyered-section">
                        <div class="leyered-heading">
                        <h2 style="text-align: center; color: black; font-family: Raleway , 'times new roman';"> Our Services</h2>
                        <div class="circle-icon">
                            <div class="circle">
                                
                            </div>
                            
                        </div>
                        <article class="service-content" style="font-family: Raleway , 'times new roman';">
                            <p>We are fully committed to integrating customer driven quality standards with superior innovation. Our abiding Endeavour is to provide customized software solutions to suit all your business requirements and with the best returns on investment. Our comprehensive portfolio of products and services includes, but is not limited to, website development,software solutions. 
                            All our processes are geared towards producing the best products and services in terms of quality, functionality, and affordability.</p>
                            </article>
                        </div>
                        <main class="main-area our_packages">

                          <div class="centered">

                            <section class="cards">
                                <div class="services">            
                                <?php foreach ($view_catone as $value) {?>  
                                <div>
                                  <article class="card">
                                    <a href="<?php echo base_url('menu/Service_detail/').$value['mcat_id'];?>">
                                      <figure class="thumbnail" style="margin-bottom: 0;">
                                      <img src="<?php echo base_url('uploads/').$value['image'];?>" alt="meow">
                                      </figure>
                                      <div class="card-content">
                                        <h2><?php echo $value['mcate_name'];?></h2>
                                        <p class="service_desc"><?php echo $value['description'];?></p>
                                      </div>
                                      <!-- .card-content -->
                                    </a>
                                  </article>
                                </div>
                              <?php }?>
                                </div>
                              <!-- .card -->

                            </section>
                            <!-- .cards -->

                          </div>
                          <!-- .centered -->

                        </main>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>

  <section class="services1">



        <!-- <div id="black_filter"></div> -->


        <table style="border-collaps:collaps; " align="center" bgcolor="ffffff" border="0" cellpadding="0" cellspacing="0" width="100%" class="">


    <!-- Start-->
    <tbody>
      <tr>
        <td valign="top">
          <table class="full" style="margin: 0 auto; border-collaps:collaps; " align="center" border="0" cellpadding="0" cellspacing="0" width="600">


            <tbody>
              <tr>
                <td>

                  <table style=" margin: 0 auto; border-collaps:collaps;" class="mtitle" align="left" border="0" cellpadding="0" cellspacing="0" width="100%">


                    <tbody>
                      <tr>
                        <td style="letter-spacing: 1px; text-align: center; font-size:25px;  font-family: Raleway , 'times new roman'; font-weight:400 ; color: #1c1c1c;"
                        height="40">WHY CHOOSE US
                        </td>
                      </tr>

                    </tbody>
                  </table>

                </td>
              </tr>

              <tr>
                <td height="65"></td>
              </tr>

              <tr>
                <td>

                  <table class="service" style=" margin: 0 auto; border-collaps:collaps;" align="left" border="0" cellpadding="0" cellspacing="0" width="255">

                    <tbody>
                      <tr>

                        <td>

                          <table style=" margin: 0 auto; border-collaps:collaps;" align="left" border="0" cellpadding="0" cellspacing="0" width="80">

                            <tbody>
                              <tr>
                                <td data-bgcolor="Service Icon" style="background: #f1f1f1; border-radius: 4px;" height="80">
                                  <img mc:edit="service1-img" class="serv" alt="img" src="front/images/startup.png" style="display:block; margin: 0 auto;" width="35">
                                </td>
                              </tr>

                            </tbody>
                          </table>

                          <table class="service-text" style=" margin: 0 auto; border-collaps:collaps;" align="right" border="0" cellpadding="0" cellspacing="0" width="149">

                            <tbody>
                              <tr>

                                <td data-color="Service Title"  mc:edit="service1-title" style="text-align:left; font-size:17px; font-family:'Raleway' , tahoma; font-weight:600 ; color: #5b5b5b; " height="17">
                                  <a data-color="Service Title"  href="#" style="text-decoration: none; text-align:left; font-size:17px; font-family:'Raleway' , tahoma; font-weight:600 ; color: #5b5b5b; ">Bulk Bandwidth</a>
                                </td>

                              </tr>

                              <tr>
                                <td height="17"></td>
                              </tr>

                              <tr>
                                <td data-color="Service Text" data-size="Service Text" data-min="14" data-max="16" mc:edit="service1-text" style="text-align:left; font-size:15px; font-family:'Raleway' , tahoma; font-weight:400 ; color: #656565; line-height: 25px; ">With Bandwidth-on-Demand, you commit to a minimal amount of bandwidth needed on an average monthly basis                                 
                                </td>
                              </tr>

                              <tr>
                                <td height="10"></td>
                              </tr>


                            </tbody>
                          </table>

                        </td>

                      </tr>

                    </tbody>
                  </table>

                  <table class="service" style=" margin: 0 auto; border-collaps:collaps;" align="right" width="255">

                    <tbody>
                      <tr>

                        <td>

                          <table style=" margin: 0 auto; border-collaps:collaps;" align="left" border="0" cellpadding="0" cellspacing="0" width="80">

                            <tbody>
                              <tr>
                                <td data-bgcolor="Service Icon" style="background: #f1f1f1; border-radius: 4px;" height="80">
                                  <img mc:edit="service2-img" class="serv" alt="img" src="front/images/dribble-logo.png" style="display:block; margin: 0 auto;" width="35">
                                </td>
                              </tr>

                            </tbody>
                          </table>

                          <table class="service-text" style=" margin: 0 auto;" align="right" border="0" cellpadding="0" cellspacing="0" width="149">

                            <tbody>
                              <tr>

                                <td data-color="Service Title"  mc:edit="service2-title" style="text-align:left; font-size:17px; font-family:'Raleway' , tahoma; font-weight:600 ; color: #5b5b5b; " height="17">
                                  <a data-color="Service Title"  href="#" style="text-decoration: none; text-align:left; font-size:17px; font-family:'Raleway' , tahoma; font-weight:600 ; color: #5b5b5b; "> Internet Lease Line</a>
                                </td>

                              </tr>

                              <tr>
                                <td height="17"></td>
                              </tr>

                              <tr>
                                <td data-color="Service Text" data-size="Service Text" style="text-align:left; font-size:15px; font-family:'Raleway' , tahoma; font-weight:400 ; color: #656565; line-height: 25px; ">Get the bandwidth your business needs to Increase communication and collaboration.
                                </td>
                              </tr>

                              <tr>
                                <td height="10"></td>
                              </tr>

                            </tbody>
                          </table>

                        </td>

                      </tr>

                    </tbody>
                  </table>





                </td>
              </tr>

              <tr>
                <td class="none" height="62"></td>
              </tr>

              <tr>
                <td>

                  <table class="service" style=" margin: 0 auto; border-collaps:collaps;" align="left" border="0" cellpadding="0" cellspacing="0" width="255">

                    <tbody>
                      <tr>

                        <td>

                          <table style=" margin: 0 auto; border-collaps:collaps;" align="left" border="0" cellpadding="0" cellspacing="0" width="80">

                            <tbody>
                              <tr>
                                <td data-bgcolor="Service Icon" style="background: #f1f1f1; border-radius: 4px;" height="80">
                                  <img mc:edit="service3-img" class="serv" alt="img" src="front/images/icon (1).png" style="display:block; margin: 0 auto;" width="35">
                                </td>
                              </tr>

                            </tbody>
                          </table>

                          <table class="service-text" style=" margin: 0 auto; border-collaps:collaps;" align="right" border="0" cellpadding="0" cellspacing="0" width="149">

                            <tbody>
                              <tr>

                                <td data-color="Service Title"  mc:edit="service3-title" style="text-align:left; font-size:17px; font-family:'Raleway' , tahoma; font-weight:600 ; color: #5b5b5b; " height="17">
                                  <a data-color="Service Title"  href="#" style="text-decoration: none; text-align:left; font-size:17px; font-family:'Raleway' , tahoma; font-weight:600 ; color: #5b5b5b; " class="">Corporate Broadband</a>
                                </td>

                              </tr>

                              <tr>
                                <td height="17"></td>
                              </tr>

                              <tr>
                                <td data-color="Service Text" data-size="Service Text" data-min="14" data-max="16" mc:edit="service3-text" style="text-align:left; font-size:15px; font-family:'Raleway' , tahoma; font-weight:400 ; color: #656565; line-height: 25px; " class="">Ultra-Fast broadband plans up to 100 Mbps with unlimited usage and symmetrical speed.
                                </td>
                              </tr>

                              <tr>
                                <td height="10"></td>
                              </tr>
                            </tbody>
                          </table>

                        </td>

                      </tr>

                    </tbody>
                  </table>

                  <table class="service" style=" margin: 0 auto; border-collaps:collaps;" align="right" width="255">

                    <tbody>
                      <tr>

                        <td>

                          <table style=" margin: 0 auto; border-collaps:collaps;" align="left" border="0" cellpadding="0" cellspacing="0" width="80">

                            <tbody>
                              <tr>
                                <td data-bgcolor="Service Icon" style="background: #f1f1f1; border-radius: 4px;" height="80">
                                  <img mc:edit="service4-img" class="serv" alt="img" src="front/images/open-padlock-silhouette.png" style="display:block; margin: 0 auto;" width="35">
                                </td>
                              </tr>

                            </tbody>
                          </table>

                          <table class="service-text" style=" margin: 0 auto; border-collaps:collaps;" align="right" border="0" cellpadding="0" cellspacing="0" width="149">

                            <tbody>
                              <tr>

                                <td data-color="Service Title"  mc:edit="service4-title" style="text-align:left; font-size:17px; font-family:'Raleway' , tahoma; font-weight:600 ; color: #5b5b5b; " height="17">
                                  <a data-color="Service Title"  href="#" style="text-decoration: none; text-align:left; font-size:17px; font-family:'Raleway' , tahoma; font-weight:600 ; color: #5b5b5b; "> Network Security</a>
                                </td>

                              </tr>

                              <tr>
                                <td height="17"></td>
                              </tr>

                              <tr>
                                <td data-color="Service Text" data-size="Service Text" data-min="14" data-max="16" mc:edit="service4-text" style="text-align:left; font-size:15px; font-family:'Raleway' , tahoma; font-weight:400 ; color: #656565; line-height: 25px; ">Security is an essential element of any data network and thus has to be inherent
                                </td>
                              </tr>

                              <tr>
                                <td height="10"></td>
                              </tr>

                            </tbody>
                          </table>

                        </td>

                      </tr>

                    </tbody>
                  </table>

                </td>
              </tr>


              <tr>
                <td height="80"></td>
              </tr>


            </tbody>
          </table>
        </td>
      </tr>
      <!--End-->


    </tbody>
  </table>
        <!-- <div class="container">
          <div class="head_choose">
            <h2 style="text-align: center; color: white; position: relative; z-index: 999;">Why Choose Us</h2>
            <hr style="width: 10%; border:1px solid white; position: relative; z-index: 999;">
          </div>
            <div class="row">
                <div class="col-md-4 col-sm-4 col-xs-4" style="height: 200px;">
                    <div class="service-icon1">
                        <img src="front/images/startup.png" alt="">
                    </div>
                    <div class="service-contnt1">
                        <h3>Bulk BandWith</h3>
                        <p>With Bandwidth-on-Demand, you commit to a minimal amount of bandwidth needed on an average monthly basis</p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-4" style="height: 200px;">
                    <div class="service-icon1">
                        <img src="front/images/dribble-logo.png" alt="">
                    </div>
                    <div class="service-contnt1">
                        <h3>Internet Lease Line</h3>
                        <p>Get the bandwidth your business needs to Increase communication and collaboration</p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-4" style="height: 200px;">
                    <div class="service-icon1">
                        <img src="front/images/icon.png" alt="">
                    </div>
                    <div class="service-contnt1">
                        <h3>Corporate Broadband</h3>
                        <p>Ultra-Fast broadband plans up to 100 Mbps with unlimited usage and symmetrical speed</p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-4" style="height: 200px;">
                    <div class="service-icon1">
                        <img src="front/images/padlock-unlocked.png" alt="">
                    </div>
                    <div class="service-contnt1">
                        <h3>Network Security</h3>
                        <p>Security is an essential element of any data network and thus has to be inherent</p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-4" style="height: 200px;">
                    <div class="service-icon1">
                        <img src="front/images/hardware-hotspot.png" alt="">
                    </div>
                    <div class="service-contnt1">
                        <h3>Wifi Hotspot</h3>
                        <p>Smartphone’s, tablets, and other mobile devices free you from the tethers</p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-4" style="height: 200px;">
                    <div class="service-icon1">
                        <img src="front/images/link-symbol.png" alt="">
                    </div>
                    <div class="service-contnt1">
                        <h3>Value Added Services</h3>
                        <p>High speed internet access opens up a range of entertainment 
                        opportunities</p>
                    </div>
                </div>
            </div>
        </div> -->
    </section>
<div class="design">
    <div class="overlay"></div>
    <div class="content">
        <p>
Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when        </p>
<p> Lorem Ipsum</p>
    </div>
    
</div>
<div class="clearfix"></div>

<!--  -->
<div class="grid_syestem">
    <div class="container">
        <div class="row">
            <div class="center">
                
            <div class="arrows1">
               <i class="fa fa-arrow-down"></i>
               </div>
               
        </div>
    </div>
</div>
</div>
<div class="newsLetter-section">
    <div class="container">
        <div class="row">
            <div class="center">
            <div class="newsLetter">
                <div class="news_heading">
                <h2> News Letter</h2>
                <p>
                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy 
                </p>
                </div>
                <form id="newsletter_form" method="post" action="/">
                    <label></label>
                    <div class="col-md-12">
                    <input type="text"  class="form_input" placeholder=" Enter Your Email" />
                    <button type="submit" class="bt btn-info button_subscribe"> Subscribe</button>
                    </div>
                </form>
                </div>
              </div>
             </div>
            </div>
       </div>
       
  <div class="clearfix"></div>
   <div class="footer-form">
    <div class="container">
        <div class="row">
            <div class="center">
    <div class="footer-heading">
    <h2>
    Feel Free To Ask 
    </h2>
    <h4> Contact Us</h4>
    <div class="circle-icon1">
         <div class="circle1">
               </div>
      </div>
    </div>
    <div class="footer_fullform">
        <form id="form_last" method="post" action="<?php echo base_url('welcome/send_mail');?>">
        <div class="row">
            <div class="col-md-4 col-sm-4 col-xs-12" style="margin-top: 16px;">
                <input type="text" name="username" class="form-control form-input" placeholder="Enter Your Name"/>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12" style="margin-top: 16px;">
                <input type="text" name="email" class="form-control form-input" placeholder="Enter Your Mail"/>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12" style="margin-top: 16px;">
                <input type="text" name="mobile" class="form-control form-input" placeholder="Enter Your mobile"/>
            </div>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12">
            <textarea style="display:table;" name="message" class="form-control text-comment" rows="6" id="comment" placeholder="Enter Your Messsage"></textarea>
            </div>
            <div class="middle-div">
            <button type="submit" class=" submit_button"> Submit</button>
            </div>
        </form>
    </div>
    </div>
    </div>
    </div>
    
   <div class="clear-fix"></div>
   <div class="clear-fix"></div>
   </div>  

   <div class="container our_services_area" style="margin-bottom: 50px; padding-top: 50px;">

      <h2 style="text-align: center;">Our Services Area</h2>
      <hr style="width: 10%;">
     <iframe src="https://www.google.com/maps/d/u/0/embed?mid=15mF2Qwfb4gfxYA7wWrx6x3crQ7AzMrbE" width="100%" height="480"></iframe>
   </div>
   <?php include('footer.php'); ?>   
    
   

<!-- <script src="<?php echo base_url('front/'); ?>js/jquery-1.11.1.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.js"></script>
<script src="<?php echo base_url('front/'); ?>js/bootstrap.js"></script>
<script src="<?php echo base_url('front/'); ?>js/owl.carousel.js"></script>
<script src="<?php echo base_url('front/'); ?>js/jquery.bxslider.js"></script> -->
<script>

$(function() {
    $('#showdiv1').click(function() {
        $('div[id^=div]').hide();
        $('#div1').fadeIn();
    });
    $('#showdiv2').click(function() {
        $('div[id^=div]').fadeOut();
        $('#div2').fadeIn();
    });
 
    $('#showdiv3').click(function() {
        $('div[id^=div]').fadeOut();
        $('#div3').fadeIn();
    });
 
    $('#showdiv4').click(function() {
        $('div[id^=div]').fadeOut();
        $('#div4').fadeIn();
    });
 
})
</script>

<script>
function openNav() {
    document.getElementById("mySidenav").style.width = "250px";
}

function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
}
</script>
<script>
$(function(){
  $('.bxslider').bxSlider({
   
    captions: true,
   
  });
});</script>
<script>
$('.cross').click(function(){
    $('.blue-background').toggle(1000);
    })
</script>



<!-- ############################################### User login ####################################### -->

<div id="LoginModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
    <?php 
       if($this->session->flashdata('msg')){
        echo $this->session->flashdata('msg'); 
       
       }?>
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">User Login</h4>
      </div>
      <div class="modal-body">
        <form method="post" action="<?php echo base_url('welcome/login_users');?>">
      <div class="form-group">
        <label for="email">Email address:</label>
        <input type="email"  name="email" class="form-control" required>
      </div>
      <div class="form-group">
        <label for="pwd">Password:</label>
        <input type="password" name="password" class="form-control" id="pwd">
      </div>
      <div class="checkbox">
        <label><input type="checkbox">Remember me</label>
      </div>
      <button type="submit" class="btn btn-default">Submit</button>
    </form>
          </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
<!-- ############################################## User login ####################################### -->


<div id="myModa2" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
        <?php 
       if($this->session->flashdata('insertmessage')){
        echo $this->session->flashdata('insertmessage'); 
       }elseif($this->session->flashdata('email_id')){

        echo $this->session->flashdata('email_id'); 
       }


       ?>
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">User Register</h4>
      </div>
      <div class="modal-body">
        <form method="post"  action="<?php echo base_url('welcome/users_register');?>">
      <div class="form-group">
        <label for="email">User Name:</label>
        <input type="text" class="form-control" name="username" required>
      </div>
      <div class="form-group">
        <label for="pwd">Password:</label>
        <input type="password" class="form-control" name="password" required>
      </div>
      <div class="form-group">
        <label for="pwd">Email:</label>
        <input type="email" class="form-control" name="email" required>
      </div>
      <div class="form-group">
        <label for="pwd">Phone:</label>
        <input type="phone" class="form-control" name="phone" required>
      </div>

      <button type="submit" class="btn btn-default">Submit</button>
    </form>
          </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

      </div>
    </div>

  </div>
</div>
<?php 
       if($this->session->flashdata('msg')){
                
        echo "<script>
                 $(window).on('load',function(){
                        $('#LoginModal').modal('show');
                       
                    });   
            </script>";       
       }elseif($this->session->flashdata('insertmessage')){
    
        echo "<script>
                 $(window).on('load',function(){
                        $('#myModa2').modal('show');
                    });   
            </script>";

       }elseif($this->session->flashdata('email_id')){

            echo "<script>
                 $(window).on('load',function(){
                        $('#myModa2').modal('show');
                    });   
            </script>";

       }

        ?>
</body>
</html>
<script>
    $('.services').slick({
  slidesToShow: 3,
  slidesToScroll: 1,
  autoplay: true,
  autoplaySpeed: 2000,
});


$(function(){
  $(".service_desc").each(function(){
    len=$(this).text().length;
    if(len>80)
    {
      $(this).text($(this).text().substr(0,200)+'...');
    }
  });
});
</script>