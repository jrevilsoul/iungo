<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class dashboard extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper(array('form', 'url','html'));
        $this->load->model('admin_model', 'welcome'); 
        $this->load->library('session');
        if($this->session->userdata('username')){
            redirect('admin/index');
        }
 
    }
    public function dashboard() {
        $this->load->view('admin/dashboard');
    }
    public function logout() {
        //unset($_SESSION['username']);
        $this->session->unset_userdata("username");
        $this->session->sess_destroy();
        redirect('admin/index');
    }
    
     public function add_catone() {
        //$this->data['view_catone'] = $this->welcome->view_catone();
        $this->load->view('admin/add_catone');
    }

    public function view_catone(){
        $this->data['view_catone'] = $this->welcome->view_catone();
        $this->load->view('admin/view_catone', $this->data);
    }

    public function submit_catone() {
        
        $config['upload_path'] = './uploads';
        $config['allowed_types'] = 'jpg|png';
        $config['max_size'] = '10240';
        
        $this->load->library('upload', $config);
        $this->upload->do_upload('image');
        $data_upload_files = $this->upload->data();
        
        $data = array('mcate_name'   => $this->input->post('name'),
                      'description'  => $this->input->post('description'),
                      'image'        => $data_upload_files['file_name'],
                      'created_date' => date("m/d/y"),
                      'update_date'  => date("y/m/d"));
            
           // print_r($data);exit;        
              
        $insert = $this->welcome->insert_catone($data);
        if($insert ){
        $this->session->set_flashdata('insertmessage', 'Your data inserted Successfully..');
        redirect('dashboard/view_catone');
        }else{
            redirect('dashboard/view_catone');
        }
    }

    public function edit_catone($id){
        $this->data['edit_catone'] = $this->welcome->edit_catone($id);
        $this->load->view('admin/edit_catone', $this->data, FALSE);
    }
    
    public function update_catone($id){
        
        $config['upload_path'] = './uploads';
        $config['allowed_types'] = 'jpg|png';
        $config['max_size'] = '10240';
        
        $this->load->library('upload', $config);
        $this->upload->do_upload('image');
        $data_upload_files = $this->upload->data();
        
    if($this->upload->do_upload('image')){

        $data = array('mcate_name'   => $this->input->post('name'),
                      'description'  => $this->input->post('description'),
                      'image'        => $data_upload_files['file_name'],
                      'created_date' => date("m/d/y"),
                      'update_date'  => date("y/m/d"));
                      
                    
        $this->db->where('mcat_id',$id);
        $this->db->update('main_category',$data);
    }else{

         $data = array('mcate_name'   => $this->input->post('name'),
                      'description'  => $this->input->post('description'),
                      'created_date' => date("m/d/y"),
                      'update_date'  => date("y/m/d"));
                      
                    
        $this->db->where('mcat_id',$id);
        $this->db->update('main_category',$data);

    }
        $this->session->set_flashdata('updatemessage', 'Your data update Successfully..');
        redirect('dashboard/view_catone');
}

    

    public function delete_catone($id) {
        $this->db->where('mcat_id', $id);
        $this->db->delete('main_category');
        $this->session->set_flashdata('deletemessage', 'Your data delete Successfully..');
        redirect('dashboard/view_catone');
    }
     public function delete_bulkcatone() {
        $this->input->post['catone'];
        $data = $this->input->post('catone');
        foreach ($data as $dtd) {
            $this->db->where('mcat_id', $dtd);
            $this->db->delete('main_category');
        }
        $this->session->set_flashdata('deletemessage', 'Your data delete Successfully..');
        redirect('dashboard/view_catone');
    }

    /*     * **************************  Catone Module End ************************** */
    
    

    /*     * **************************  Cattwo Module Starts *************************** */
// ======================== Plan Start =====================
    public function add_cattwo(){
        
        $this->data['view_city'] = $this->welcome->view_city();
        //$this->data['view_addplan'] = $this->welcome->view_addplan();
        $this->load->view('admin/add_cattwo',$this->data);
    }

    public function view_plan() {
       $this->data['view_cate'] = $this->welcome->view_cate();
       
        $this->load->view('admin/view_category',$this->data);
    }

    public function submit_cattwo() {
        
        $count = $this->input->post('count');
        $description =array();

        for ($i = 1;$i<=$count;$i++){
            $description[$this->input->post('description'.$i)] = $this->input->post('value'.$i);
        }

        $description = json_encode($description);
        
        $data = array('city_id'      => $this->input->post('city_id'),
                      'plan_name'   => $this->input->post('plan_name'),
                      'description'  => $description,
                      'amount'        => $this->input->post('amount'));
     // print_r($data);exit;
        $insert = $this->welcome->insert_cattwo($data);
        if($insert >0){
         $this->session->set_flashdata('insertmessage', 'Your data inserted Successfully..');
        redirect('dashboard/view_plan');
        }else{
            redirect('dashboard/view_plan');
        }
    }

    public function edit_cattwo($id) {
        $this->data['view_catone'] = $this->welcome->view_catone();
        $this->data['edit_cattwo'] = $this->welcome->edit_cattwo($id);
        
        $this->load->view('admin/edit_cattwo', $this->data, FALSE);
    }

    public function update_cattwo($id) {
        
        $data = array('city_id'      => $this->input->post('mcate_id'),
                      'plan_name'         => $this->input->post('plan_name'),
                      'amount'         => $this->input->post('amount'),
                      'description'  => $this->input->post('description'),
                      'updated_at'  => date("y/m/d")); 
                $this->db->where('catt_id', $id);
                $this->db->update('category', $data);
                $this->session->set_flashdata('insertmessage', 'Your data inserted Successfully..');
                redirect('dashboard/view_plan');            
       
    }

    public function delete_cattwo($id) {
        $this->db->where('id', $id);
        $this->db->delete('category');
        $this->session->set_flashdata('deletemessage', 'Your data delete Successfully..');
        redirect('dashboard/view_plan');
    }
    
    public function delete_bulkcattwo(){
    $this->input->post('boxes');
    $data = $this->input->post('boxes');
    foreach($data as $dtd){
           $this->db->where('id',$dtd);
           $this->db->delete('category');
       }
     $this->session->set_flashdata('deletemessage', 'Your data delete Successfully..');
     redirect('dashboard/view_plan');
    }
    



// ======================= plan end ===================    
    
    public function add_catthree() {
        $this->data['view_catone'] = $this->welcome->view_catone();
        $this->data['view_cate'] = $this->welcome->view_cate();
        
        $this->load->view('admin/catthree',$this->data);
    }
    
    
    public function submit_catthree() {
        
        $config['upload_path'] = './uploads';
        $config['allowed_types'] = 'jpg|png';
        $config['max_size'] = '10240';
        
        $this->load->library('upload', $config);
        echo $this->upload->do_upload('image');
        $data_upload_files = $this->upload->data();
        
        $data = array('mcat_id'      => $this->input->post('mcate_id'),
                      'catt_id'      => $this->input->post('catt_id'),
                      'name'   => $this->input->post('name'),
                      'description'  => $this->input->post('description'),
                      'image'        => $data_upload_files['file_name'],
                      'created_date' => date("m/d/y"),
                      'update_date'  => date("y/m/d"));
     
        $insert = $this->welcome->insert_catthree($data);
        if($insert >0){
         $this->session->set_flashdata('insertmessage', 'Your data inserted Successfully..');
        redirect('dashboard/view_catthree');
        }else{
            redirect('dashboard/view_catthree');
        }
    }
    
    public function view_catthree() {
        $this->data['view_catthree'] = $this->welcome->view_catthree();
        //$this->data['view_cate'] = $this->welcome->view_cate();
        
        $this->load->view('admin/view_catthree',$this->data);
    }
    
    
    public function edit_catthree($id){
        
        $this->data['view_catone'] = $this->welcome->view_catone();
        $this->data['view_cate'] = $this->welcome->view_cate();
        $this->data['edit_catthree'] = $this->welcome->edit_catthree($id);
        $this->load->view('admin/edit_catthree', $this->data, FALSE);
    }
    
    public function update_catthree($id) {
        
        $config['upload_path'] = './uploads';
        $config['allowed_types'] = 'jpg|png';
        $config['max_size'] = '10240';
        
        $this->load->library('upload', $config);
        $this->upload->do_upload('image');
        $data_upload_files = $this->upload->data();
        
        if($this->upload->do_upload('image')){
        
            
        $data = array('mcat_id'      => $this->input->post('mcate_id'),
                      'catt_id'      => $this->input->post('catt_id'),
                      'name'         => $this->input->post('name'),
                      'description'  => $this->input->post('description'),
                      'image'        => $data_upload_files['file_name'],
                      'created_date' => date("m/d/y"),
                      'update_date'  => date("y/m/d"));
                      
                      
        }else{
        
        $data = array('mcat_id'      => $this->input->post('mcate_id'),
                      'catt_id'      => $this->input->post('catt_id'),
                      'name'         => $this->input->post('name'),
                      'description'  => $this->input->post('description'),
                      'created_date' => date("m/d/y"),
                      'update_date'  => date("y/m/d")); 
                  
                      
        }
                $this->db->where('sub_cate_id', $id);
                $this->db->update('sub_category', $data);
                $this->session->set_flashdata('updatemessage', 'Your data inserted Successfully..');
                redirect('dashboard/view_catthree');
                      
       
    }
    
    public function delete_catthree($id) {
        $this->db->where('sub_cate_id', $id);
        $this->db->delete('sub_category');
        $this->session->set_flashdata('deletemessage', 'Your data delete Successfully..');
        redirect('dashboard/view_catthree');
    }
    
    public function delete_bulkcatthree(){
    $this->input->post('boxes');
    $data = $this->input->post('boxes');
    foreach($data as $dtd){
           $this->db->where('sub_cate_id',$dtd);
           $this->db->delete('sub_category');
       }
     $this->session->set_flashdata('deletemessage', 'Your data delete Successfully..');
     redirect('dashboard/view_catthree');
    }


    
    /***********************************End Cattwo Module Starts******************************* */
    
    
    /***************************** Cms about Module starts ******************************/
    
     public function cms_aboutus() {
        $this->data['aboutusview'] = $this->welcome->view_aboutus();
        $this->load->view('admin/cms_aboutus', $this->data);
    }

    public function insert_cms_aboutus() {
        $data = array(
            'content_description' => $this->input->post('content_description'),
            'content_TitleMeta' => $this->input->post('content_TitleMeta'),
            'content_KeywordMeta' => $this->input->post('content_KeywordMeta'),
            'content_DescriptionMeta' => $this->input->post('content_DescriptionMeta'));

        $insertandupdate = $this->welcome->insert_aboutus($data);
        if($insertandupdate > 0){
              $this->session->set_flashdata('insertmessage', 'Your data insert Successfully..');
         redirect('dashboard/cms_aboutus');
        }else{
             redirect('dashboard/cms_aboutus');
        }
            
    }

    public function update_cms_aboutus($id) {
        $data = array('content_description' => $this->input->post('content_description'),
            'content_TitleMeta' => $this->input->post('content_TitleMeta'),
            'content_KeywordMeta' => $this->input->post('content_KeywordMeta'),
            'content_DescriptionMeta' => $this->input->post('content_DescriptionMeta'));
        $this->db->where('id!=', $id);
        $this->db->update('cms_aboutus', $data);
        $this->session->set_flashdata('updatemessage', 'Your data update Successfully..');
        redirect('dashboard/cms_aboutus');
    }
    
    /***************************** Cms about Module starts ******************************/
    
    /***************************** Cms contact Module starts ******************************/
    
    
    public function cms_contactus() {
        $this->data['aboutusview'] = $this->welcome->view_contactus();
        $this->load->view('admin/cms_contactus', $this->data);
    }

    public function insert_cms_contactus() {
        $data = array(
            'content_description' => $this->input->post('content_description'),
            'content_TitleMeta' => $this->input->post('content_TitleMeta'),
            'content_KeywordMeta' => $this->input->post('content_KeywordMeta'),
            'content_DescriptionMeta' => $this->input->post('content_DescriptionMeta'));

        $insertandupdate = $this->welcome->insert_contactus($data);
        if($insertandupdate > 0){
        $this->session->set_flashdata('insertmessage', 'Your data update Successfully..');
        redirect('dashboard/cms_contactus');
        }else{
          redirect('dashboard/cms_contactus');
        }
    }

    public function update_cms_contactus($id) {
        $data = array('content_description' => $this->input->post('content_description'),
            'content_TitleMeta' => $this->input->post('content_TitleMeta'),
            'content_KeywordMeta' => $this->input->post('content_KeywordMeta'),
            'content_DescriptionMeta' => $this->input->post('content_DescriptionMeta'));
        $this->db->where('id!=', $id);
        $this->db->update('cms_contactus', $data);
        $this->session->set_flashdata('updatemessage', 'Your data update Successfully..');
        redirect('dashboard/cms_contactus');
    }
    
    /***************************** End Cms about Module starts ******************************/
    
    /***************************************** CMS_career start ************************** */

    public function cms_career() {
        $this->data['dashboardeer'] = $this->welcome->view_dashboardeer();
        $this->load->view('admin/cms_career', $this->data);
    }

    public function insert_cms_career() {
        $data = array(
            'content_description' => $this->input->post('content_description'),
            'content_TitleMeta' => $this->input->post('content_TitleMeta'),
            'content_KeywordMeta' => $this->input->post('content_KeywordMeta'),
            'content_DescriptionMeta' => $this->input->post('content_DescriptionMeta'));
        $insertandupdate = $this->welcome->insert_dashboardeer($data);
        if($insertandupdate > 0){
            $this->session->set_flashdata('insertmessage','Data insert successfully');
        redirect('dashboard/cms_career');
        }else{
            redirect('welcome/cms_career');
        }
    }

    public function update_cms_career($id) {
        $data = array('content_description' => $this->input->post('content_description'),
            'content_TitleMeta' => $this->input->post('content_TitleMeta'),
            'content_KeywordMeta' => $this->input->post('content_KeywordMeta'),
            'content_DescriptionMeta' => $this->input->post('content_DescriptionMeta'));
        $this->db->where('id!=', $id);
        $this->db->update('cms_career', $data);
        $this->session->set_flashdata('updatemessage','Data update successfully');
        redirect('dashboard/cms_career');
    }

    /*     * ************************** CMS_career End ************************** */
    
    
    /*     * ************************** CMS_services start ************************** */

    public function cms_services() {
        $this->data['aboutusview'] = $this->welcome->view_services();
        $this->load->view('admin/cms_services', $this->data);
    }

    public function insert_cms_services() {
        $data = array(
            'content_description' => $this->input->post('content_description'),
            'content_TitleMeta' => $this->input->post('content_TitleMeta'),
            'content_KeywordMeta' => $this->input->post('content_KeywordMeta'),
            'content_DescriptionMeta' => $this->input->post('content_DescriptionMeta'));

        $insertandupdate = $this->welcome->insert_services($data);
        if($insertandupdate > 0){
              $this->session->set_flashdata('insertmessage', 'Your data insert Successfully..');
              redirect('dashboard/cms_services');
        }else{
            redirect('dashboard/cms_services');
        }
    }

    public function update_cms_services($id) {
        $data = array('content_description' => $this->input->post('content_description'),
            'content_TitleMeta' => $this->input->post('content_TitleMeta'),
            'content_KeywordMeta' => $this->input->post('content_KeywordMeta'),
            'content_DescriptionMeta' => $this->input->post('content_DescriptionMeta'));
        $this->db->where('id!=', $id);
        $this->db->update('cms_services', $data);
        $this->session->set_flashdata('updatemessage', 'Your data update Successfully..');
        redirect('dashboard/cms_services');
    }




    public function view_testimonial(){
    $data['view_testimonial'] = $this->welcome->view_testimonial();
    $this->load->view('admin/view_testimonial',$data);
    
    }
    
    
     public function cms_testimonial(){
        $this->data['testimonial'] = $this->welcome->view_testimonial();
        $this->load->view('admin/testimonial', $this->data);
    }
    
     public function insert_testimonial() {
        $data = array(
            'user_name' => $this->input->post('user_name'),
            'user_email' => $this->input->post('user_email'),
            'testimonial' => $this->input->post('content_TitleMeta'));
           

        $insertandupdate = $this->welcome->insert_testimonial($data);
        if($insertandupdate > 0){
              $this->session->set_flashdata('insertmessage', 'Your data insert Successfully..');
              redirect('dashboard/view_testimonial');
        }else{
            redirect('dashboard/view_testimonial');
        }
     }


     public function edit_testimonial($id) {
       $this->data['view_testimonial'] = $this->welcome->edit_testimonial($id);
        
        $this->load->view('admin/edit_testimonial', $this->data);
    }
        
     public function update_testimonial($id) {
        $data = array(
            'user_name' => $this->input->post('user_name'),
            'user_email' => $this->input->post('user_email'),
            'testimonial' => $this->input->post('content_TitleMeta'));
        $this->db->where('id =', $id);
        $this->db->update('testimonial', $data);
        $this->session->set_flashdata('updatemessage', 'Your data update Successfully..');
        redirect('dashboard/view_testimonial');
    }
    



// ================ Client ==================== 




    public function view_clients(){
    $data['view_clients'] = $this->welcome->view_client();
    $this->load->view('admin/view_clients',$data);
    
    }
    
    
     public function cms_client(){
        $this->data['clients'] = $this->welcome->view_client();
        $this->load->view('admin/add_client', $this->data);
    }
    
     public function insert_client() {


      $config['upload_path'] = './uploads';
        $config['allowed_types'] = 'jpg|png';
        $config['max_size'] = '10240';
        
        $this->load->library('upload', $config);
        $this->upload->do_upload('image');
        $data_upload_files = $this->upload->data();
        
     $data = array(
            
            'company_name'   => $this->input->post('company_name'),
            'company_logo'         => $data_upload_files['file_name']);
           

        $insertandupdate = $this->welcome->insert_client($data);
        if($insertandupdate > 0){
              $this->session->set_flashdata('insertmessage', 'Your data insert Successfully..');
              redirect('dashboard/view_clients');
        }else{
            redirect('dashboard/view_clients');
        }
     }


     public function edit_client($id) {
       $this->data['view_clients'] = $this->welcome->edit_client($id);
        
        $this->load->view('admin/edit_client', $this->data);
    }
        
     public function update_client($id) {
        $config['upload_path'] = './uploads';
        $config['allowed_types'] = 'jpg|png';
        $config['max_size'] = '10240';
        
        $this->load->library('upload', $config);
        $this->upload->do_upload('image');
        $data_upload_files = $this->upload->data();
        
     $data = array(
            
            'company_name'   => $this->input->post('company_name'),
            'company_logo'         => $data_upload_files['file_name']);
        $this->db->where('id =', $id);
        $this->db->update('testimonial', $data);
        $this->session->set_flashdata('updatemessage', 'Your data update Successfully..');
        redirect('dashboard/view_clients');
    }
    


    // ======================= events =====================


public function up_event(){
        $this->data['view_uevent'] = $this->welcome->view_uevent();
        $this->load->view('admin/view_uevent', $this->data);

    }


    public function add_uevent(){
        $this->data['uevent'] = $this->welcome->view_uevent();
        $this->load->view('admin/add_uevent', $this->data);
    }



  public function insert_uevent() {


      $config['upload_path'] = './uploads';
        $config['allowed_types'] = 'jpg|png';
        $config['max_size'] = '10240';
        
        $this->load->library('upload', $config);
        $this->upload->do_upload('image');
        $data_upload_files = $this->upload->data();
        
     $data = array(
            
            'event_name'   => $this->input->post('event_name'),
            'start_date'   => $this->input->post('start_date'),
            'end_date'   => $this->input->post('end_date'),
            'event_logo'         => $data_upload_files['file_name'],
            'event_description'   => $this->input->post('event_description'));
           

        $insertandupdate = $this->welcome->insert_uevent($data);
        if($insertandupdate > 0){
              $this->session->set_flashdata('insertmessage', 'Your data insert Successfully..');
              redirect('dashboard/past_event');
        }else{
            redirect('dashboard/past_event');
        }
     }

public function past_event(){
        $this->data['view_pevent'] = $this->welcome->view_pevent();
        $this->load->view('admin/view_pevent', $this->data);
    }


    public function add_pevent(){
        $this->data['pevent'] = $this->welcome->view_pevent();
        $this->load->view('admin/add_pevent', $this->data);
    }
    

  public function insert_pevent() {


      $config['upload_path'] = './uploads';
        $config['allowed_types'] = 'jpg|png';
        $config['max_size'] = '10240';
        
        $this->load->library('upload', $config);
        $this->upload->do_upload('image');
        $data_upload_files = $this->upload->data();
        
     $data = array(
            
            'event_name'   => $this->input->post('event_name'),
            'event_year'   => $this->input->post('event_year'),
            'event_logo'         => $data_upload_files['file_name'],
            'event_description'   => $this->input->post('event_description'));
           

        $insertandupdate = $this->welcome->insert_pevent($data);
        if($insertandupdate > 0){
              $this->session->set_flashdata('insertmessage', 'Your data insert Successfully..');
              redirect('dashboard/past_event');
        }else{
            redirect('dashboard/past_event');
        }
     }





     public function edit_pevent($id) {
       $this->data['view_pevent'] = $this->welcome->edit_pevent($id);
        
        $this->load->view('admin/edit_pevent', $this->data);
    }


    public function update_pevent($id) {
        $config['upload_path'] = './uploads';
        $config['allowed_types'] = 'jpg|png';
        $config['max_size'] = '10240';
        
        $this->load->library('upload', $config);
        $this->upload->do_upload('image');
        $data_upload_files = $this->upload->data();
        
     $data = array(
            
            'event_name'   => $this->input->post('event_name'),
            'event_year'   => $this->input->post('event_year'),
            'event_logo'         => $data_upload_files['file_name'],
            'event_description'   => $this->input->post('event_description'));
        $this->db->where('id =', $id);
        $this->db->update('past_event', $data);
        $this->session->set_flashdata('updatemessage', 'Your data update Successfully..');
        redirect('dashboard/past_event');
    }

    

    /**************************** CMS_services End ************************** */
    
    /****************************  USER PROFILE   ***********************************/
    
    public function user_profile(){
       $this->data['view_profile'] = $this->welcome->view_profile();
       $this->load->view('admin/user_profile',$this->data);
    
     }

     public function add_userprofile(){

        $this->load->view('admin/add_userprofile');
     }

     public function submit_userprofile(){

        $config['upload_path'] = './uploads';
        $config['allowed_types'] = 'jpg|png';
        $config['max_size'] = '10240';
        
        $this->load->library('upload', $config);
        $this->upload->do_upload('image');
        $data_upload_files = $this->upload->data();

         $data    = array('username'=>$this->input->post('username'),
                          'email'=>$this->input->post('email'),
                          'designation'=>$this->input->post('designation'),
                          'description'=>$this->input->post('description'),
                          'image'=>$data_upload_files['file_name'],
                          'f_link'=>$this->input->post('f_link'),
                          'g_link'=>$this->input->post('g_link'),
                          'i_link'=>$this->input->post('i_link'));
            // print_r($data);exit;              
        $insert = $this->welcome->insert_userprofile($data);
        if($insert > 0){
        $this->session->set_flashdata('insertmessage', 'Your data Insert Successfully..');
        redirect('dashboard/user_profile');
        }else{
            redirect('dashboard/user_profile');
        }

     }
    
     public function edit_profile($id) {
        $this->data['view_profile'] = $this->welcome->edit_profile($id);
        
        $this->load->view('admin/edit_profile',$this->data);
    }

     public function edit_team($id) {
        $this->data['view_team'] = $this->welcome->edit_team($id);
        $this->load->view('admin/edit_team',$this->data);
    }
    
    public function update_profile($id){
        $config['upload_path'] = './uploads';
        $config['allowed_types'] = 'jpg|png';
        $config['max_size'] = '10240';
        
        $this->load->library('upload', $config);
        $this->upload->do_upload('image');
        $data_upload_files = $this->upload->data();
       if($this->upload->do_upload('image')){
         $data    = array('username'=>$this->input->post('username'),
                          'email'=>$this->input->post('email'),
                          'designation'=>$this->input->post('designation'),
                          'description'=>$this->input->post('description'),
                          'image'=>$data_upload_files['file_name'],
                          'f_link'=>$this->input->post('f_link'),
                          'g_link'=>$this->input->post('g_link'),
                          'i_link'=>$this->input->post('i_link'));

        $this->db->where('id',$id);
        $this->db->update('tbl_team',$data);
        $this->session->set_flashdata('updatemessage', 'Your data update Successfully..');
        redirect('dashboard/user_profile');
       }else{

          $data    = array('username'=>$this->input->post('username'),
                          'email'=>$this->input->post('email'),
                          'designation'=>$this->input->post('designation'),
                          'description'=>$this->input->post('description'),
                          'f_link'=>$this->input->post('f_link'),
                          'g_link'=>$this->input->post('g_link'),
                          'i_link'=>$this->input->post('i_link'));

        $this->db->where('id',$id);
        $this->db->update('tbl_team',$data);
        $this->session->set_flashdata('updatemessage', 'Your data update Successfully..');
        redirect('dashboard/user_profile');


       }
                    
                    
        
    }
    
    public function delete_profile($id){
        
        $this->db->where('id',$id);
        $this->db->delete('tbl_team');
        $this->session->set_flashdata('deletemessage', 'Your data delete Successfully..');
        redirect('dashboard/user_profile');
        
    }

    public function delete_bulkteam(){
    $this->input->post('catone');
    $data = $this->input->post('catone');
    foreach($data as $dtd){
           $this->db->where('d',$dtd);
           $this->db->delete('tbl_team');
       }
     $this->session->set_flashdata('deletemessage', 'Your data delete Successfully..');
     redirect('dashboard/user_profile');
    }
        
    
    /************************** End user PROFILE*********************************/
    
    
    /************************** Drop down city and location *********************************/
    
    public function location(){
        $this->data['country'] = $this->welcome->getcountry();
        $this->load->view('admin/location',$this->data); 
        
    }
    
    public function ajax_state_list($country_id)
    {
        //$this->load->helper('url');
        $data['state'] = $this->welcome->getstate($country_id);
        $this->load->view('admin/ajax_get_state',$data);
    }
    
    public function ajax_city_list($state_id)
    {
        $data['city'] = $this->welcome->getcity($state_id);
        $this->load->view('admin/ajax_get_city',$data);
    }
    
    /************************** Drop down city and location *********************************/
    
    
    /************************** services *********************************/
    
    
    public function add_product(){
        $this->data['view_catthree'] = $this->welcome->view_catthree();
        $this->load->view('admin/add_product',$this->data);
    
    }
    
    
    public function submit_product(){
        
        $config['upload_path'] = './uploads';
        $config['allowed_types'] = 'jpg|png';
        $config['max_size'] = '10240';
        
        $this->load->library('upload', $config);
        $this->upload->do_upload('image');
        $data_upload_files = $this->upload->data();
        
     $data = array(
            'mini_sub_cat_id'  => $this->input->post('sub_id'),
            'product_id'       => $this->input->post('p_id'),
            'p_name'           => $this->input->post('pname'),
            'image'            => $data_upload_files['file_name'],
            'amount'           => $this->input->post('amount'),
            'description'      => $this->input->post('description'),
            'status'           => $this->input->post('status'),
            'created_date'     => date("m/d/y"),
            'update_date'      => date("m/d/y"));
      // print_r($data);exit;
        $insert = $this->welcome->insert_addproduct($data);
        if($insert >0){
         $this->session->set_flashdata('insertmessage', 'Your data inserted Successfully..');
        redirect('dashboard/view_product');
        }else{
            redirect('dashboard/view_product');
        }
    }
    
    public function view_product(){
    $data['view_product'] = $this->welcome->view_product();
    $this->load->view('admin/view_product',$data);
    
    }
    
    public function edit_prodcut($id){
        //echo '===';exit;
        $this->data['view_catthree'] = $this->welcome->view_catthree();
        $this->data['edit_product'] = $this->welcome->edit_product($id);
        $this->load->view('admin/edit_product', $this->data, FALSE);
        
    }
    
    public function update_product($id){
        
        $config['upload_path'] = './uploads';
        $config['allowed_types'] = 'jpg|png';
        $config['max_size'] = '10240';
        
        $this->load->library('upload', $config);
        $this->upload->do_upload('image');
        $data_upload_files = $this->upload->data();
        
        if($this->upload->do_upload('image') == 1){
        $data = array('product_id' => $this->input->post('sub_id'),
            'mini_sub_cat_id' => $this->input->post('p_id'),
            'p_name' => $this->input->post('pname'),
            'amount' => $this->input->post('amount'),
            'image' => $data_upload_files['file_name'],
            'description'=>$this->input->post('description'),
            'status' => $this->input->post('status'),
            'created_date' => date("m/d/y h:i:s"));
        //print_r($data);exit;
        $this->db->where('p_id =', $id);
        $this->db->update('product_table', $data);
        }else{
        $data = array('product_id'    => $this->input->post('sub_id'),
               'mini_sub_cat_id'      => $this->input->post('p_id'),
               'p_name'               => $this->input->post('pname'),
               'amount'               => $this->input->post('amount'),
               'description'          => $this->input->post('description'),
               'status'               => $this->input->post('status'),
               'created_date'         => date("m/d/y h:i:s"));
        //print_r($data);exit;
        $this->db->where('p_id =', $id);
        $this->db->update('product_table', $data);
        }
        $this->session->set_flashdata('updatemessage', 'Your data update Successfully..');
        redirect('dashboard/view_product');
        
    }
    
    public function delete_product($id){
        
         $this->db->where('p_id =', $id);
         $this->db->delete('product_table');
         $this->session->set_flashdata('deletemessage', 'Your data delete Successfully..');
         redirect('dashboard/view_product');
    }
    
    public function delete_bulkproduct(){
    $this->input->post('catone');
    $data = $this->input->post('catone');
    foreach($data as $dtd){
           $this->db->where('p_id',$dtd);
           $this->db->delete('product_table');
       }
     $this->session->set_flashdata('deletemessage', 'Your data delete Successfully..');
     redirect('dashboard/view_product');
    }
    
    
    public function add_offers(){
        $this->data['view_product'] = $this->welcome->view_product();
        
        $this->load->view('admin/offerce',$this->data);
        
    }public function view_offer(){
        $this->data['view_offer'] = $this->welcome->view_offer();
        
        $this->load->view('admin/view_offer',$this->data);
        
    }
    
    
    public function edit_offer($id){
    $this->data['view_product'] = $this->welcome->view_product();   
    $this->data['edit_offer'] = $this->welcome->edit_offer($id);
    $this->load->view('admin/edit_offer',$this->data);
    
    
    }
    
    public function delete_offer($id){
        
         $this->db->where('id =', $id);
         $this->db->delete('tbl_offer');
         $this->session->set_flashdata('deletemessage', 'Your data delete Successfully..');
         redirect('dashboard/view_offer');
    }
    
    public function delete_bulkoffer(){
    $this->input->post('catone');
    $data = $this->input->post('catone');
    foreach($data as $dtd){
           $this->db->where('id',$dtd);
           $this->db->delete('tbl_offer');
       }
     $this->session->set_flashdata('deletemessage', 'Your data delete Successfully..');
     redirect('dashboard/view_offer');
    }
    
    
    public function update_offer($id){
        
        $config['upload_path'] = './uploads';
        $config['allowed_types'] = 'jpg|png';
        $config['max_size'] = '10240';
        
        $this->load->library('upload', $config);
        $this->upload->do_upload('image');
        $data_upload_files = $this->upload->data();
        
   if($this->upload->do_upload('image') > 0){
     $data = array(
            
            'product_id'   => $this->input->post('pro_id'),
            'image'        => $data_upload_files['file_name'],
            'title'        => $this->input->post('title'),
            'type'         => $this->input->post('type'),
            'amount'       => $this->input->post('amount'),
            'description'  => $this->input->post('description'),
            'created_date' => date("m/d/y"),
            'update_date' => date("m/d/y"),
            'delete_date' => date("m/d/y"));
        // print_r($data);exit;
        $this->db->where('id =', $id);
        $this->db->update('tbl_offer', $data);
        }else{
            
            $data = array(
            
            'product_id'   => $this->input->post('pro_id'),
            'title'        => $this->input->post('title'),
            'type'         => $this->input->post('type'),
            'amount'       => $this->input->post('amount'),
            'description'  => $this->input->post('description'),
            'created_date' => date("m/d/y"),
            'update_date' => date("m/d/y"),
            'delete_date' => date("m/d/y"));
         //print_r($data);exit;
        $this->db->where('id =', $id);
        $this->db->update('tbl_offer', $data);
            
        }   
        $this->session->set_flashdata('updatemessage', 'Your data update Successfully..');
        redirect('dashboard/view_Offer');
    }
    
    
        public function submit_offer(){
        
        $config['upload_path'] = './uploads';
        $config['allowed_types'] = 'jpg|png';
        $config['max_size'] = '10240';
        
        $this->load->library('upload', $config);
        $this->upload->do_upload('image');
        $data_upload_files = $this->upload->data();
        
     $data = array(
            
            'product_id'   => $this->input->post('pro_id'),
            'image'        => $data_upload_files['file_name'],
            'title'        => $this->input->post('title'),
            'type'         => $this->input->post('type'),
            'amount'       => $this->input->post('amount'),
            'description'  => $this->input->post('description'),
            'created_date' => date("m/d/y"),
            'update_date' => date("m/d/y"),
            'delete_date' => date("m/d/y"));
       
        $insert = $this->welcome->insert_offer($data);
        if($insert >0){
         $this->session->set_flashdata('insertmessage', 'Your data inserted Successfully..');
        redirect('dashboard/view_Offer');
        }else{
            redirect('dashboard/view_Offer');
        }
    }
    public function view_users(){

        $data['view_users'] = $this->welcome->view_users();
        
        $this->load->view('admin/view_users',$data);
    }
    

    public function view_comments(){

        $data['view_comments'] = $this->welcome->view_comments();
        
        $this->load->view('admin/user_comments',$data);
    }
    
    public function delete_comments($id){
        
         $this->db->where('id =', $id);
         $this->db->delete('tbl_comments');
         $this->session->set_flashdata('deletemessage', 'Your data delete Successfully..');
         redirect('dashboard/view_comments');
    }
    
    public function delete_bulkcomments(){
        $this->input->post('catone');
        $data = $this->input->post('catone');
        foreach($data as $dtd){
               $this->db->where('id',$dtd);
               $this->db->delete('tbl_comments');
           }
         $this->session->set_flashdata('deletemessage', 'Your data delete Successfully..');
         redirect('dashboard/view_comments');
    }
    
    public function website_flash_banner(){ 
       $data['view_banner'] = $this->welcome->view_banner();
       $this->load->view('admin/banner',$data);
    }


     public function website_add_banner(){


       $this->load->view('admin/site_add_banner');

     }

     public function website_edit_banner($id){

        $this->data['view_banner'] = $this->welcome->edit_banner($id);
        $this->load->view('admin/site_edit_banner',$this->data);

    }

    public function submit_banner(){


        $config['upload_path'] = './banners';
        $config['allowed_types'] = 'jpg|png';
        $config['max_size'] = '10240';
        
        $this->load->library('upload', $config);
        $this->upload->do_upload('image');
        $data_upload_files = $this->upload->data();
        
     $data = array(
            
            'banner_name'   => $this->input->post('banner_name'),
            'banner_title'  => $this->input->post('banner_title'),
            'image'         => $data_upload_files['file_name']);
       
       //print_r($data);exit;    

        $insert = $this->welcome->insert_addbanner($data);
    if($insert >0){
         $this->session->set_flashdata('insertmessage', 'Your data inserted Successfully..');
         redirect('dashboard/website_flash_banner');
         }else{
            redirect('dashboard/website_flash_banner');
        }
    }

    public function update_banner($id){

        $config['upload_path'] = './banners';
        $config['allowed_types'] = 'jpg|png';
        $config['max_size'] = '10240';
        
        $this->load->library('upload', $config);
        $this->upload->do_upload('image');
        
        $data_upload_files = $this->upload->data();

if($this->upload->do_upload('image') > 0){  

     $data = array(
            
            'banner_name'   => $this->input->post('banner_name'),
            'banner_title'  => $this->input->post('banner_title'),
            'image'         => $data_upload_files['file_name']);

       //print_r($data);exit;

        $this->db->where('id =', $id);
        $this->db->update('site_banner', $data);
    }else{

         $data = array(
            
            'banner_name'   => $this->input->post('banner_name'),
            'banner_title'  => $this->input->post('banner_title'));
        

        $this->db->where('id =', $id);
        $this->db->update('site_banner', $data);   

    }
        $this->session->set_flashdata('updatemessage', 'Your data update Successfully..');
        redirect('dashboard/website_flash_banner');

}


    public function delete_banner($id){
        $this->db->where('id=',$id);
        $this->db->delete('site_banner');
        $this->session->set_flashdata('deletemessage', 'Your data delete Successfully..');
        redirect('dashboard/website_flash_banner');

    }

    public function delete_bulkbanner(){

    $this->input->post('catone');
    $data = $this->input->post('catone');
    foreach($data as $dtd){
        $this->db->where('id=',$dtd);
        $this->db->delete('site_banner');
    }
        $this->session->set_flashdata('deletemessage', 'Your data delete Successfully..');
        redirect('dashboard/website_flash_banner');
}

   
    
    
    public function view_order(){
        
        $this->data['view_order'] = $this->welcome->view_order();
        
        $this->load->view('admin/view_order',$this->data);
    }
    
    public function add_general_settings(){
        
        $this->data['view_site'] = $this->welcome->view_site();
        
        $this->load->view('admin/site',$this->data);
    
    }

    public function admin(){

        $this->data['admin_profile'] = $this->welcome->admin_profile();
        $this->load->view('admin/admin_profile',$this->data);
    
    }


    public function submit_adminprofile(){

     $data = array(
            'username'      => $this->input->post('username'),
            'password'      => $this->input->post('password'),
            'email'         => $this->input->post('email'),
            'email'         => $this->input->post('email'));
           
        $insert = $this->welcome->insert_addprofile($data);
        if($insert >0){
         $this->session->set_flashdata('insertmessage', 'Your data inserted Successfully..');
        redirect('dashboard/admin');
        }else{
            redirect('dashboard/admin');
        }

    }


    public function update_adminprofile($id){
        $data = array(
            'username'       => $this->input->post('username'),
            'password'       => $this->input->post('password'),
            'email'          => $this->input->post('email'),
            'email'          => $this->input->post('email'));
        //print_r($data);exit;
        $this->db->where('id =', $id);
        $this->db->update('tbl_usrs', $data);
        $this->session->set_flashdata('updatemessage', 'Your data update Successfully..');
        redirect('dashboard/admin');
    
            
        }   
    
    public function submit_setting(){

        $config['upload_path'] = './uploads';
        $config['allowed_types'] = 'jpg|png';
        $config['max_size'] = '10240';
        
        $this->load->library('upload', $config);
        $this->upload->do_upload('image');
        $data_upload_files[] = $this->upload->data();

     $data = array(
            'name'              => $this->input->post('name'),
            'currency_symbol'   => $this->input->post('symbol'),
            'address'           => $this->input->post('address'),
            'email'             => $this->input->post('email'),
            'phone'             => $this->input->post('phone'),
            'image'             => $data_upload_files['file_name']);
            
        $insert = $this->welcome->insert_addsetting($data);
        if($insert >0){
         $this->session->set_flashdata('insertmessage', 'Your data inserted Successfully..');
        redirect('dashboard/add_general_settings');
        }else{
            redirect('dashboard/add_general_settings');
        }

    }
    
    public function update_setting($id){


        $config['upload_path'] = './uploads';
        $config['allowed_types'] = 'jpg|png';
        $config['max_size'] = '10240';
        
        $this->load->library('upload', $config);
        $this->upload->do_upload('image');
        $data_upload_files = $this->upload->data();

     $data = array(
            'name'              => $this->input->post('name'),
            'currency_symbol'   => $this->input->post('symbol'),
            'address'      => $this->input->post('address'),
            'email'             => $this->input->post('email'),
            'phone'             => $this->input->post('phone'),
            'image'             => $data_upload_files['file_name']);
            //print_r($data);exit;
        $this->db->where('id =', $id);
        $this->db->update('tbl_setting', $data);
        $this->session->set_flashdata('updatemessage', 'Your data update Successfully..');
        redirect('dashboard/add_general_settings');
    
            
        }   
        
        public function add_social_settings(){
        
        $this->data['social_site'] = $this->welcome->view_social();
        
        $this->load->view('admin/social_media_setting',$this->data);
    
    }
        
        
        
        
    public function submit_social(){

     $data = array(
                 'facebook'              => $this->input->post('facebook'),
                 'google'                => $this->input->post('google'),
                 'twitter'               => $this->input->post('twitter'),
                 'youtube'               => $this->input->post('youtube'));
            
        $insert = $this->welcome->insert_social($data);
        if($insert >0){
         $this->session->set_flashdata('insertmessage', 'Your data inserted Successfully..');
        redirect('dashboard/add_social_settings');
        }else{
            redirect('dashboard/add_social_settings');
        }

    }
    
     public function update_social($id){

     $data = array(
                 'facebook'              => $this->input->post('facebook'),
                 'google'                => $this->input->post('google'),
                 'twitter'               => $this->input->post('twitter'),
                 'youtube'               => $this->input->post('youtube'));

        $this->db->where('id =', $id);
        $this->db->update('tbl_social', $data);
        $this->session->set_flashdata('updatemessage', 'Your data update Successfully..');
        redirect('dashboard/add_social_settings');
        
     }








// ===================== City ================







      public function add_city() {
        //$this->data['view_city'] = $this->welcome->view_city();
        $this->load->view('admin/add_city');
    }

    public function view_city(){
        $this->data['view_city'] = $this->welcome->view_city();
        $this->load->view('admin/view_city', $this->data);
    }

    public function submit_city() {
        
        $data = array('city_name'   => $this->input->post('city_name'));
            
           // print_r($data);exit;        
              
        $insert = $this->welcome->insert_city($data);
        if($insert ){
        $this->session->set_flashdata('insertmessage', 'Your data inserted Successfully..');
        redirect('dashboard/view_city');
        }else{
            redirect('dashboard/view_city');
        }
    }

    public function edit_city($id){
        $this->data['edit_city'] = $this->welcome->edit_city($id);
        $this->load->view('admin/edit_city', $this->data, FALSE);
    }
    
    public function update_city($id){

        $data = array('city_name'   => $this->input->post('city_name'));
                      
                    
        $this->db->where('id',$id);
        $this->db->update('city',$data);
        $this->session->set_flashdata('updatemessage', 'Your data update Successfully..');
        redirect('dashboard/view_city');
}

    

    public function delete_city($id) {
        $this->db->where('id', $id);
        $this->db->delete('city');
        $this->session->set_flashdata('deletemessage', 'Your data delete Successfully..');
        redirect('dashboard/view_city');
    }
     public function delete_bulkcity() {
        $this->input->post['city'];
        $data = $this->input->post('city');
        foreach ($data as $dtd) {
            $this->db->where('id', $dtd);
            $this->db->delete('city');
        }
        $this->session->set_flashdata('deletemessage', 'Your data delete Successfully..');
        redirect('dashboard/view_city');
    }




// ================================ iungo_plan==============

    public function plan_data($city_id)
    {
        $data['plan'] = $this->welcome->getcityforplan($city_id);
        // print_r($data);
        $this->load->view('html/plandata',$data);
    }


     public function plan_data2($city_id)
    {
        $data['plan'] = $this->welcome->getplan($city_id);
        // print_r($data);
        $this->load->view('html/plandata2',$data);
    }



    // ==================== subscriber list==============


 
      public function do_recharge($id) {
        $data['user_id']= $id; 
        $data['view_user'] = $this->welcome->view_user($id);
        $data['view_city'] = $this->welcome->view_plancity();
        // print_r($data);
        // exit;
        // $this->data['userdata'] = $this->welcome->userdata($id);
        $this->load->view('admin/recharge',$data);
    }


 public function planrecharge($city_id)
    {
        $data['plan'] = $this->welcome->getcityforplan($city_id);
        // print_r($data);
        $this->load->view('admin/recdata',$data);
    }


     public function plan_recharge2($city_id)
    {
        $data['plan'] = $this->welcome->getplan($city_id);
        // print_r($data);
        $this->load->view('admin/recdata2',$data);
    }


public function submit_recharge() {  


        $user_id = $this->input->post('user_id');
        $recharge_amount = $this->input->post('amount');
        $data['wallet_data'] = $this->welcome->walletdata($user_id);
        $credit_amount = 0;
        $debit_amount = 0;
        foreach ($data as $value) {
            if ($value[0]['transaction_type'] =="credit") {
                 echo $credit_amount = $credit_amount + $value[0]['amount'];    
                
            }else{                
                 $debit_amount = $debit_amount + $value[0]['amount'];    
            }
        }

         $amount = $credit_amount - $debit_amount;

        if ($amount>$recharge_amount) {
            $data = array(
                'user_id'             => $this->input->post('user_id'),
                'plan_id'             => $this->input->post('plan_id'),
                'start_date'          => $this->input->post('start_date'),
                'end_date'            => $this->input->post('end_date'),
                'amount'              => $this->input->post('amount'),
                'subscription_type'   => $this->input->post('subscription_type')
            );    

        $insert = $this->welcome->insert_recharge($data);
            if($insert ){
                $transaction_type = "debit";
                $status = "success";
            $insert_data = array(
                'user_id'           => $this->input->post('user_id'),
                'amount'            => $this->input->post('amount'),
                'transaction_type'  => $transaction_type,
                'status'            => $status,
                'date'              => date(y-m-d)
            );
            $insert = $this->welcome->update_wallet($insert_data);
            if (insert) {
                $this->session->set_flashdata('insertmessage', 'Recharge Successful !!!');
                redirect('dashboard/view_users');
                }else{
                    redirect('dashboard/view_users');
                }
            }
        }else{
            exit;
            $this->session->set_flashdata('insertmessage', 'Insufficent Balance in user wallet!!!' );
                redirect('dashboard/view_users');
        }
    }


}