<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Menu extends CI_Controller {


    public function __construct() {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->model('Login_model', 'welcome');
        $this->load->library('session');
    }
	

    public function aboutus(){
        $this->data['aboutus']   = $this->welcome->view_about();
        $this->data['view_site'] = $this->welcome->view_site();
        $this->load->view('html/about_us',$this->data);
    }


    public function Services(){

         $this->data['view_site'] = $this->welcome->view_site();
         $this->data['view_catone'] = $this->welcome->view_catone();
         $this->load->view('html/view_services',$this->data);
    }


    public function Service_detail($id){
       
         if($id){
         $this->data['view_services'] = $this->welcome->service($id);
         $this->data['view_catone']   = $this->welcome->view_catone();
         $this->data['view_site']     = $this->welcome->view_site();
         $this->data['view_pevent'] = $this->welcome->view_pevent();
         $this->data['view_uevent'] = $this->welcome->view_uevent();
         $this->load->view('html/services',$this->data);
        }
        else{
          redirect('welcome');
        }
    }

     public function clients(){
        //$this->data['aboutus']   = $this->welcome->view_about();
        $this->data['view_site'] = $this->welcome->view_site();
        $this->load->view('html/clients',$this->data);
    }

    public function contactus(){
        $this->data['view_site'] = $this->welcome->view_site();
        $this->load->view('html/contact',$this->data);
    }

    public function ourcustomer(){
        $this->data['view_site'] = $this->welcome->view_site();
        $this->load->view('html/our_customer',$this->data);
    }

     public function tenders(){
        $this->data['view_site'] = $this->welcome->view_site();
        $this->load->view('html/tenders',$this->data);
    }

    public function Plans(){
        $this->data['view_site'] = $this->welcome->view_site();
        $this->load->view('html/plans',$this->data);
    }

    public function privacy_policy(){
        $this->data['view_site'] = $this->welcome->view_site();
        $this->load->view('html/privacy_policy',$this->data);
    }

    public function Buy_plan(){
        $this->data['view_site'] = $this->welcome->view_site();
        $this->data['view_city'] = $this->welcome->view_city();
        $this->load->view('html/buy_plan',$this->data);
    }

public function demo(){
        $this->data['view_site'] = $this->welcome->view_site();
        $this->load->view('html/demo',$this->data);
    }

    public function login(){
        $this->data['view_site'] = $this->welcome->view_site();
        $this->load->view('html/login',$this->data);
    }


    public function register(){
        $this->data['view_site'] = $this->welcome->view_site();
        $this->load->view('html/register',$this->data);
    }
public function pay_success(){
        $this->data['view_site'] = $this->welcome->view_site();
        $this->load->view('html/success',$this->data);
    }

    public function pay_failure(){
        $this->load->view('html/failure');
    }

    public function payment_sub(){
           $plan_id = $this->input->post('plan_id');
           $id = $this->input->post('insert_id');

           $data = array('name'     => $this->input->post('firstname'),
                          'email'    => $this->input->post('email'),
                          'phone'    => $this->input->post('phone'));
           $this->db->where('id',$id);
           $this->db->where('plan_id',$plan_id);
           $this->db->update('booking',$data);
           $booking_data = $this->welcome->userbooking($id);
           $data['request'] = array( 'firstname' =>  $booking_data[0]['name'],
                              'amount'           =>  $booking_data[0]['amount'],
                              'email'            =>  $booking_data[0]['email'],
                              'phone'            =>  $booking_data[0]['phone'],
                              'productinfo'      =>  $booking_data[0]['plan_id']

                  );
                  $this->load->view('html/payment',$data);
    }


    public function user_detail(){
       $id = $this->session->userdata('id');
         $data = $this->welcome->onuserdata($id); 
      if ($id) {
      $insert_data = array(   'user_id'        =>  $data[0]['id'],
                              'plan_id'          =>  $this->input->post('plan_id'),
                              'amount'          =>  $this->input->post('amount')); 
      $insert = $this->welcome->insert_booking($insert_data);
              if ($insert) {
                $id = $this->db->insert_id();
                $booking_data = $this->welcome->userbooking($id);
                $data['request'] = array(
                                  'firstname'        =>  $data[0]['name'],
                                  'amount'           =>  $booking_data[0]['amount'],
                                  'email'            =>  $data[0]['email'],
                                  'phone'            =>  $data[0]['phone'],
                                  'productinfo'      =>  $booking_data[0]['plan_id'],

                  );
                  $this->load->view('html/payment',$data);
              }
       

      }else{

         $insert_data = array( 'plan_id'          =>  $this->input->post('plan_id'),
                                'amount'          =>  $this->input->post('amount')); 
          $insert = $this->welcome->insert_booking($insert_data);
          $id = $this->db->insert_id();
          $this->data['booking'] = $this->welcome->userbooking($id);
          $this->data['view_site'] = $this->welcome->view_site();
          $this->load->view('html/fill_detail',$this->data);
      }
    }


    public function direct_pay_detail(){
        $this->data['view_site'] = $this->welcome->view_site();
        $this->load->view('html/online_pay_detail',$this->data);
    }


     public function direct_pay(){

           $data = array('name'     => $this->input->post('firstname'),
                          'email'    => $this->input->post('email'),
                          'phone'    => $this->input->post('phone'),
                          'amount'    => $this->input->post('amount'),
                          'purpose'    => $this->input->post('purpose')
                        );
          $insert = $this->welcome->insert_direct_pay($data);
          if ($insert) {
            $id = $this->db->insert_id();
           $direct_pay = $this->welcome->site_pay($id);
           $data['request'] = array( 'firstname' =>  $direct_pay[0]['name'],
                              'amount'           =>  $direct_pay[0]['amount'],
                              'email'            =>  $direct_pay[0]['email'],
                              'phone'            =>  $direct_pay[0]['phone'],
                              'productinfo'      =>  $direct_pay[0]['purpose']

                  );
        $data['view_site'] = $this->welcome->view_site();
                  $this->load->view('html/direct_payment',$data);
          }
    }

    public function paytmdetail(){
            $order_id = rand(99999,999999);
            $industry_id = "Retail";
            $channel_id = "WEB";
           $data = array(
                          'order_id'    => $order_id,
                          'industry_id'    => $industry_id,
                          'channel_id'    => $channel_id,
                          'cust_id'    => $this->input->post('cust_id'),
                          'amount'    => $this->input->post('amount'),
                          'purpose'    => $this->input->post('purpose')
                        );
          $insert = $this->welcome->insert_direct_paytm($data);
          if ($insert) {
            $id = $this->db->insert_id();
           $direct_pay = $this->welcome->paytm_pay_data($id);
           $data['request'] = array( 'order_id'    => $direct_pay[0]['order_id'],
                                    'industry_id'    => $direct_pay[0]['industry_id'],
                                    'channel_id'    => $direct_pay[0]['channel_id'],
                                    'cust_id'    => $direct_pay[0]['cust_id'],
                                    'amount'    => $direct_pay[0]['amount']


                  );
        $data['view_site'] = $this->welcome->view_site();
                  $this->load->view('html/paytm_success',$data);
          }
    }

}
?>