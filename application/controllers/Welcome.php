<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {


    public function __construct() {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->model('Login_model', 'welcome');
        //$this->load->library('encrypt');
        $this->load->library('session');

    }
	

    public function index(){
        $this->data['view_banner'] = $this->welcome->view_banner();
        $this->data['view_testimonial'] = $this->welcome->view_testimonial();
        $this->data['view_team'] = $this->welcome->view_team();
        $this->data['view_site'] = $this->welcome->view_site();
        $this->data['view_client'] = $this->welcome->view_client();
        $this->data['view_catone'] = $this->welcome->view_catone();
        $this->load->view('html/index',$this->data);
    }

      public function dashboard_profile(){


          $email = $this->session->userdata('email');
          if($email){
          $id = $this->session->userdata('id');
          $data['profile_data'] = $this->welcome->view_profile($email);
          $data['view_profile'] = $this->welcome->userprofile($id); 
          $data['view_site'] = $this->welcome->view_site();
          $data['customerbooking'] = $this->welcome->customerbooking($id);
          $data['wallet'] = $this->welcome->user_wallet($id);
          $this->load->view('html/dashboard',$data);
          }
          else
          {
            redirect('welcome','refresh');
          }

    }

  public function userlogout() {
     $this->session->unset_userdata("email");
        $this->session->sess_destroy();
        redirect('login/index');
    }


    public function users_register(){

       $data = array(
                     'name'           => $this->input->post('username'),
                     'password'       => $this->input->post('password'),
                     'email'          => $this->input->post('email'),
                     'phone'          => $this->input->post('phone'),
                     'status'         => '1',
                     'created_date'   => date('Y-M-d'),
                     'updated_date'   => date('Y-m-d'));

             
         $insert = $this->welcome->insert_register($data);
        if($insert){
            $this->session->set_flashdata('insertmessage', '<div class="alert alert-danger text-center">your register succsessfully</div>');
            redirect('welcome');
        }elseif(empty($insert)){
          $this->session->set_flashdata('email_id', '<div class="alert alert-danger text-center">Your email already exit..</div>');
           redirect('welcome');
        }else{
          redirect('welcome');
        }
    }
  

  public function contact() {

     $this->load->view('customer_panel/index.php');
    }


    public function login_users(){

       
      $email = $this->input->post('email');
      $password = $this->input->post('password');


      $sql  = $this->db->query("select * from tbl_users where email = '$email'");
      $data = $sql->result_array();
      $id   = $data[0]['id'];
      

       $usr_result = $this->welcome->get_users($email, $password);
           //print_r($usr_result);
           
                  if ($usr_result > 0) 
                  {
        
                      
                       $sessiondata = array(
                            'id'    => $id,
                            'email' => $email,
                            'loginuser' => TRUE
                       );
                       $this->session->set_userdata($sessiondata);
                       redirect('welcome/dashboard_profile/#tab1');
                  }
                   else
                  {
                    
                     $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">Invalid Email and password!</div>');
                     //exit;
                     redirect('welcome');
                  }

    }

  
  
  public function logout_users() {
        //unset($_SESSION['username']);
        $this->session->sess_destroy();
        redirect('welcome');
    }

    public function submit_profile($id){

        $data = array('name'     => $this->input->post('username'),
                      'email'    => $this->input->post('email'),
                      'phone'    => $this->input->post('phone'));


    $this->db->where('id',$id);
    $this->db->update('tbl_users',$data);
    $this->session->set_flashdata('updatemessage', 'Your data update Successfully..');
    redirect('welcome/dashboard_profile');

    }

    public function view_pakage(){
      $id = $this->session->userdata('id');
      $data['view_profile'] = $this->welcome->userprofile($id); 
      $this->load->view('html/package_detail',$data);
    }


     public function delete_profile($id){
      $uid = $this->session->userdata('id');
      $this->db->where('p_id',$id);
      $this->db->where('user_id',$uid);
      $this->db->delete('product_table');
      redirect('welcome/dashboard_profile/#tab2');
    }

      public function changed(){
        $uid = $this->session->userdata('id');
        $old_password     = $this->input->post('old_password');
        $new_password     = $this->input->post('new_password');
        $confirm_password = $this->input->post('confirm_password');

        if($new_password == $confirm_password){
          
         $data = array('password' => $new_password);
          print_r($data);
          //exit;

          $this->db->where('id',$uid);
          $this->db->update('tbl_users',$data);
          $this->session->set_flashdata('updatepass', 'Your password update Successfully..');
          redirect('welcome/dashboard_profile/#tab5');

        }

    }

     public function send_mail(){
            
      
       $sender_email='info@iungo.in';
       $receiver_email = $this->input->post('email');
      
        
       
      $insert_data = array('name'=>$this->input->post('username'),
                        'email'=>$this->input->post('email'),
                        'mobile'=>$this->input->post('mobile'),
                        'enquiry_description'=>$this->input->post('message'),
          ); 
 
           $message = '
                      <html>
                      <head>
                          <title>Contact Us Query</title>
                      </head>
                      <body>
                          <table cellspacing="0" style="border: 1px dashed #FB4314; width: 300px; height: 200px;">
                              <tr>
                                  <th>Name:</th><td>'.$insert_data ['name'].'</td>
                              </tr>
                              <tr style="background-color: #e0e0e0;">
                                  <th>Email:</th><td>'.$insert_data ['email'].'</td>
                              </tr>
                              <tr>
                                  <th>Subject:</th><td>'.$insert_data ['mobile'].'</td>
                              </tr>
                              <tr>
                                  <th>Subject:</th><td>'.$insert_data ['enquiry_description'].'</td>
                              </tr>
                          </table>
                      </body>
                      </html>';

          // $message =  "Name = ".$name.'<br>'."Email = ".$inesrt_data[1]['email'];
         
            $config['protocol'] = 'send_email';
            $config['smtp_host'] = 'ssl://smtp.googlemail.com';
            $config['smtp_port'] = 465;
            $config['smtp_user'] = $sender_email;
            $config['mailtype'] = 'html';

            $this->load->library('email', $config);
            $this->email->set_newline("\r\n");
            $this->email->from($sender_email);
            $this->email->to($receiver_email);
            $this->email->subject($subject);
            $this->email->message($message);
            if ($this->email->send()){
 
       $this->session->set_flashdata("message","Email Successfully Send !");
        redirect('welcome');
                 
            }
           
        }


      public function refer(){

         $email = $this->session->userdata('email');
         $data = $this->welcome->refer($email); 
         $sender_email='namdev.ajay2@gmail.com';
         $receiver_email = $this->input->post('email');
      
        print_r($data);
       
         $insert_data = array('name'           =>  $this->input->post('name'),
                              'user_id'        =>  $data[0]['id'],
                              'email'          =>  $this->input->post('email'),
                              'phone'          =>  $this->input->post('phone')); 

          $insert = $this->welcome->insert_refer($insert_data);
        
        if($insert){

          $subject = "Refer by Friend";

           $message = '
                <html>
                <head>
                    <title>Contact Us Query</title>
                </head>
                <body>
                   <p>Hello Sir</p>
                   <p>You are refered by '.$data[0]['name'].' . Please use this url sign up and get 100rs cash back... </p>
                   <p><a href="http://iungo.in/demo/menu/register"></a></p>
                </body>
                </html>';

          // $message =  "Name = ".$name.'<br>'."Email = ".$inesrt_data[1]['email'];
         
            $config['protocol'] = 'send_email';
            $config['smtp_host'] = 'ssl://smtp.googlemail.com';
            $config['smtp_port'] = 465;
            $config['smtp_user'] = $sender_email;
            $config['mailtype'] = 'html';

            $this->load->library('email', $config);
            $this->email->set_newline("\r\n");
            $this->email->from($sender_email);
            $this->email->to($receiver_email);
            $this->email->subject($subject);
            $this->email->message($message);
            if ($this->email->send()){

               $subject = "Refer by Friend";
              $receiver_email="namdev.ajay2@gmail.com";
           $message = '
                <html>
                <head>
                    <title>Contact Us Query</title>
                </head>
                <body>
                   <p>Hello Sir</p>
                   <p>You are refered by '.$data[0]['name'].' . Please use this url sign up and get 100rs cash back... </p>
                   <p><a href="http://iungo.in/demo/menu/register"></a></p>
                </body>
                </html>';

          // $message =  "Name = ".$name.'<br>'."Email = ".$inesrt_data[1]['email'];
         
            $config['protocol'] = 'send_email';
            $config['smtp_host'] = 'ssl://smtp.googlemail.com';
            $config['smtp_port'] = 465;
            $config['smtp_user'] = $sender_email;
            $config['mailtype'] = 'html';

            $this->load->library('email', $config);
            $this->email->set_newline("\r\n");
            $this->email->from($sender_email);
            $this->email->to($receiver_email);
            $this->email->subject($subject);
            $this->email->message($message);
              if ($this->email->send()){
            $this->session->set_flashdata("message","Email Successfully Send !");
            redirect('welcome/dashboard_profile/#tab1');
                 
            }

          }
           

      }

    }

     public function add_money(){

         $email          = $this->session->userdata('email');
         $data           = $this->welcome->get_user_info($email); 
         $amount         = $this->input->post('amount');
         $this->load->helper('string');
         $transaction_id = random_string('alnum',20);

      
         $insert_data = array(
                              'user_id'          =>  $data[0]['id'],
                              'gateway'          =>  "payumoney",
                              'status'           =>  "panding",
                              'remark'           =>  "null",
                              'transaction_id'   =>  $transaction_id,
                              'amount'           =>  $amount,
                              'date'             =>  date('Y-m-d'),
                              'created_date'     =>  date('y-m-d h:i:s'));
         //print_r($insert_data);
        //exit();

         $insert = $this->welcome->insert_money($insert_data);

         if ($insert){
                //$id = $this->db->insert_id();
                //$booking_data = $this->welcome->userbooking($id);
                $data['request']  = array(
                                'firstname'        =>  $data[0]['name'],
                                'amount'           =>  $amount,
                                'email'            =>  $data[0]['email'],
                                'phone'            =>  $data[0]['phone'],
                                'productinfo'      =>  "Productinfo");
             
            // $insert = $this->welcome->insert_transaction($insert_data);

                $this->load->view('html/wallet_money',$data);
              }
                             
       }

      public function wallet_success(){
        
          $data['view_site'] = $this->welcome->view_site();
         $email          = $this->session->userdata('email');
         $data           = $this->welcome->get_user_info($email); 
         $amount         = $this->input->post('amount');
         // $this->load->helper('string');
         // $transaction_id = random_string('alnum',20);

      
         $insert_data = array(
                              'user_id'            =>  $data[0]['id'],
                              'transaction_type'   =>  123,
                              'amount'             =>  $amount,
                              'date'               =>  date('Y-m-d'),
                              'status'             =>  'success');

         //print_r($insert_data); exit();

        $insert = $this->welcome->insert_transaction($insert_data);
        if($insert){

        $this->load->view('html/wallet_success',$data);
      }
        
      }

      public function wallet_failure(){

          $data['view_site'] = $this->welcome->view_site();
         $email          = $this->session->userdata('email');
         $data           = $this->welcome->get_user_info($email); 
         $amount         = $this->input->post('amount');
         // $this->load->helper('string');
         // $transaction_id = random_string('alnum',20);

      
         $insert_data = array(
                              'user_id'            =>  $data[0]['id'],
                              'transaction_type'   =>  123,
                              'amount'             =>  $amount,
                              'date'               =>  date('Y-m-d'),
                              'status'             =>  'failure');

          $insert = $this->welcome->insert_transaction($insert_data);
          if($insert){
              $this->load->view('html/wallet_failure',$data);
            }
        }
      

      public function direct_payment_success(){
          $data['view_site'] = $this->welcome->view_site();
        
        //  $txnid         = $this->input->post('txnid');
        //  $status         = $this->input->post('status');
      
        //  $insert_data = array(
                              
        //                       'transaction_type'   =>  $txnid,
        //                       'status'             =>  $status);
        //  //print_r($insert_data); exit();

        // $insert = $this->welcome->insert_transaction($insert_data);
        // if($insert){

        $this->load->view('html/direct_payment_success',$data);
      // }
        
      }

      public function direct_payment_failure(){
          $data['view_site'] = $this->welcome->view_site();

         // $txnid         = $this->input->post('txnid');
         // $status         = $this->input->post('status');
      
         // $insert_data = array(
                              
         //                      'transaction_type'   =>  $txnid,
         //                      'status'             =>  $status);

         //  $this->db->where('id',$id);
         //  $this->db->update('tbl_users',$data);
         //  if($insert){
              $this->load->view('html/direct_payment_failure',$data);
            // }
        }
      

        public function paytm_success(){
          $data['view_site'] = $this->welcome->view_site();
              $this->load->view('html/paytm_success',$data);
        }
      


}
?>