<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {


    public function __construct() {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->model('Login_model', 'welcome'); 
        $this->load->library('session');
	
		 // if(!$this->session->userdata('email')){
   //          redirect('welcome'); 
   //      }
         
    }
	


      public function dashboard_profile(){

          $email = $this->session->userdata('email');
          $id = $this->session->userdata('id');
          $data['profile_data'] = $this->welcome->view_profile($email);
          $data['view_profile'] = $this->welcome->userprofile($id);
          $data['wallet'] = $this->welcome->user_wallet($id);
          print_r($data['wallet']);
          exit;
          $this->load->view('html/dashboard',$data);

    }

  public function userlogout() {
     $this->session->unset_userdata("email");
        $this->session->sess_destroy();
        redirect('login/index');
    }


    public function users_register(){

       $data = array(
                     'name'           => $this->input->post('username'),
                     'password'       => $this->input->post('password'),
                     'email'          => $this->input->post('email'),
                     'phone'          => $this->input->post('phone'),
                     'status'         => '1',
                     'created_date'   => date('Y-M-d'),
                     'updated_date'   => date('Y-m-d'));

             
         $insert = $this->welcome->insert_register($data);
        if($insert){
            $this->session->set_flashdata('insertmessage', '<div class="alert alert-danger text-center">your register succsessfully</div>');
            redirect('welcome');
        }elseif(empty($insert)){
          $this->session->set_flashdata('email_id', '<div class="alert alert-danger text-center">Your email already exit..</div>');
           redirect('welcome');
        }else{
          redirect('welcome');
        }
    }
  

  public function contact() {

     $this->load->view('customer_panel/index.php');
    }


    public function login_users(){

       
      $email = $this->input->post('email');
      $password = $this->input->post('password');


      $sql  = $this->db->query("select * from tbl_users where email = '$email'");
      $data = $sql->result_array();
      $id   = $data[0]['id'];
      
      echo '===';exit;

       $usr_result = $this->welcome->get_users($email, $password);
           print_r($usr_result);
           
                    if ($usr_result > 0) 
                    {
          
                        
                         $sessiondata = array(
                              'id'    => $id,
                              'email' => $email,
                              'loginuser' => TRUE
                         );
                         $this->session->set_userdata($sessiondata);
                         redirect('website/dashboard_profile');
                    }
                     else
                    {
                      
                         $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">Invalid Email and password!</div>');
                         //exit;
                         redirect('welcome');
                    }

    }

  
	
	public function logout_users() {
        //unset($_SESSION['username']);
        $this->session->sess_destroy();
        redirect('welcome');
    }


    public function submit_profile($id){

        $data = array('name'     => $this->input->post('username'),
                      'email'    => $this->input->post('email'),
                      'phone'    => $this->input->post('phone'));


    $this->db->where('id',$id);
    $this->db->update('tbl_users',$data);
    $this->session->set_flashdata('updatemessage', 'Your data update Successfully..');
    redirect('welcome/dashboard_profile');

    }

    public function view_pakage(){
      $id = $this->session->userdata('id');
      $data['view_profile'] = $this->welcome->userprofile($id); 
      $this->load->view('html/package_detail',$data);
    }


    public function delete_profile($id){
      $uid = $this->session->userdata('id');
      $this->db->where('p_id',$id);
      $this->db->where('user_id',$uid);
      $this->db->delete('product_table');
      redirect('welcome/dashboard_profile/#tab2');
    }

    // public function changed(){

    //      echo '==='.$old_password = $this->input->post('old_password');exit;



    // }

    
	
}
?>