<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class User extends CI_Controller {


    public function __construct() {
        parent::__construct();
        $this->load->helper(array('form', 'url','html'));
        $this->load->model('admin_model', 'welcome'); 
        $this->load->library('session');
 
    }
	
	
	public function add_product(){
		$this->data['view_catthree'] = $this->welcome->view_catthree();
	    $this->load->view('admin/add_view_product',$this->data);
	
	}
	
	public function view_product(){
	$data['view_product'] = $this->welcome->view_product();
	$this->load->view('admin/view_user_prodcut',$data);
	
	}
	
	public function submit_product(){
		
		$config['upload_path'] = './uploads';
        $config['allowed_types'] = 'jpg|png';
        $config['max_size'] = '10240';
        
        $this->load->library('upload', $config);
        $this->upload->do_upload('image');
        $data_upload_files = $this->upload->data();
		
	 $data = array(
            'mini_sub_cat_id'  => $this->input->post('sub_id'),
			'product_id'       => $this->input->post('p_id'),
            'p_name'             => $this->input->post('pname'),
			'image'            => $data_upload_files['file_name'],
			'amount'           => $this->input->post('amount'),
			'description'      => $this->input->post('description'),
            'status'           => $this->input->post('status'),
            'created_date'     => date("m/d/y"),
			'update_date' => date("m/d/y"));
      // print_r($data);exit;
        $insert = $this->welcome->insert_addproduct($data);
		if($insert >0){
         $this->session->set_flashdata('insertmessage', 'Your data inserted Successfully..');
        redirect('user/view_product');
		}else{
			redirect('user/view_product');
		}
    }
	
	public function update_product($id){
		
		$config['upload_path'] = './uploads';
        $config['allowed_types'] = 'jpg|png';
        $config['max_size'] = '10240';
        
        $this->load->library('upload', $config);
        $this->upload->do_upload('image');
        $data_upload_files = $this->upload->data();
		
		if($this->upload->do_upload('image') == 1){
		$data = array('product_id' => $this->input->post('sub_id'),
            'mini_sub_cat_id' => $this->input->post('p_id'),
			'p_name' => $this->input->post('pname'),
			'amount' => $this->input->post('amount'),
			'image' => $data_upload_files['file_name'],
			'description'=>$this->input->post('description'),
            'status' => $this->input->post('status'),
            'created_date' => date("m/d/y h:i:s"));

        $this->db->where('p_id =', $id);
        $this->db->update('product_table', $data);
		}else{
		$data = array('product_id' => $this->input->post('sub_id'),
            'mini_sub_cat_id' => $this->input->post('p_id'),
			'p_name' => $this->input->post('pname'),
			'amount' => $this->input->post('amount'),
			'description'=>$this->input->post('description'),
            'status' => $this->input->post('status'),
            'created_date' => date("m/d/y h:i:s"));

        $this->db->where('p_id =', $id);
        $this->db->update('product_table', $data);
		}
		$this->session->set_flashdata('updatemessage', 'Your data update Successfully..');
        redirect('user/view_product');
		
	}
	
	
	public function edit_prodcut($id){
		
		$this->data['view_catthree'] = $this->welcome->view_catthree();
		$this->data['edit_product'] = $this->welcome->edit_product($id);
        $this->load->view('admin/edit_user_product', $this->data);
		
	}
	
    public function delete_product($id){
		
		 $this->db->where('p_id =', $id);
		 $this->db->delete('product_table');
		 $this->session->set_flashdata('deletemessage', 'Your data update Successfully..');
         redirect('user/view_product');
	}
	
	public function delete_bulkproduct(){
		$this->input->post('catone');
		$data = $this->input->post('catone');
		foreach($data as $dtd){
			   $this->db->where('p_id',$dtd);
			   $this->db->delete('product_table');
		   }
		 redirect('user/view_product');
	}
	

	public function contact_us(){

		     $this->load->view('html/header');
			 $this->load->view('customer_panel/index');
			  $this->load->view('html/footer');

	}

	
	
	
}
?>