<?php
class admin_model extends CI_Model
{
public function __construct()
{
parent::__construct();
}

     /* ----------- Start Catone Model--------------- */


	public function insert_catone($data){
	$this->db->insert('main_category', $data);
	return TRUE;
	}

	public function view_catone(){
	$query=$this->db->query("SELECT * FROM main_category");
	return $query->result_array();
	}
	
	public function edit_catone($id){
	$query=$this->db->query("SELECT * FROM main_category WHERE mcat_id = $id");
	return $query->result_array();
	}
	
	public function view_addplan(){
		$query = $this->db->query("SELECT * FROM add_plan");
		return $query->result_array();
	}


      /* ------------ End Catone Model -------------------- */
	  
	  
	  /* ---------------Start Cattwo Model------------------------ */
		public function insert_cattwo($data){
		$this->db->insert('category', $data);
		return TRUE;
		}

		public function view_cate(){
		 $query = $this->db->query("SELECT  * FROM category");
		 return $query->result_array();
		 }

		public function edit_cattwo($id){
		 $query=$this->db->query("SELECT * from category where catt_id = $id");
		return $query->result_array();
		}
		
		
		public function insert_catthree($data){
		$this->db->insert('sub_category', $data);
		return TRUE;
		}
		
		public function view_catthree(){
		$query = $this->db->query("SELECT  * FROM sub_category");
		return $query->result_array();
		}
		 
		public function edit_catthree($id){
		$query=$this->db->query("SELECT * from sub_category where sub_cate_id = $id");
		return $query->result_array();
		}

/*----------------- End Cattwo Model -----------------***************************************/

      /* Start CMS_ABOUTUS Model */
		public function view_aboutus(){
		$query=$this->db->query("SELECT * FROM cms_aboutus where id!=''");
		return $query->result_array();
		}


		public function insert_aboutus($data)
		{
		$this->db->insert('cms_aboutus', $data);
		return TRUE;
		}
		/* End CMS_ABOUTUS Model */

		/* Start CMS_Services Model */

		public function view_services(){
		$query=$this->db->query("SELECT * FROM cms_services where id!=''");
		return $query->result_array();
		}
		public function insert_services($data)
		{
		$this->db->insert('cms_services', $data);
		return TRUE;
		}
		
		/*****************************End CMS_Services Model ************************************/
		
		/* ***********************Start CMS_CONTACTUS Model *************************************/
		public function view_contactus(){
		$query=$this->db->query("SELECT * FROM cms_contactus where id!=''");
		return $query->result_array();
		}

		public function insert_contactus($data)
		{
		$this->db->insert('cms_contactus',$data);
		return TRUE;
		}
		/********************************* End CMS_CONTACTUS Model ********************************/
		
		/********************************* Start CMS_Career Model *********************************/
 
        public function view_career(){
	    $query=$this->db->query("SELECT * FROM cms_career where id!=''");
	    return $query->result_array();
		}

	    public function insert_career($data)
		{
		$this->db->insert('cms_career', $data);
		return TRUE;
		}
		/************************************* end of cms career model ***************/
		
		/************************************* end of cms testimonial model ***************/
		
		public function view_testimonial(){
	    $query=$this->db->query("SELECT * FROM testimonial where id!=''");
	    return $query->result_array();
		}


		public function edit_testimonial($id){
	    $query=$this->db->query("SELECT * FROM testimonial where id = $id");
	    return $query->result_array();
		}

	    public function insert_testimonial($data)
		{
		$this->db->insert('testimonial', $data);
		return TRUE;
		}
		
		
		
        /********************************* End testimonial Model**************************************/


        /************************************* end of cms Client model ***************/
		
		public function view_client(){
	    $query=$this->db->query("SELECT * FROM clients where id!=''");
	    return $query->result_array();
		}


		public function edit_client($id){
	    $query=$this->db->query("SELECT * FROM clients where id = $id");
	    return $query->result_array();
		}

	    public function insert_client($data)
		{
		$this->db->insert('clients', $data);
		return TRUE;
		}



		// =========================== past event ================


		public function view_pevent(){
	    $query=$this->db->query("SELECT * FROM past_event where id!=''");
	    return $query->result_array();
		}


		public function edit_pevent($id){
	    $query=$this->db->query("SELECT * FROM past_event where id = $id");
	    return $query->result_array();
		}

	    public function insert_pevent($data)
		{
		$this->db->insert('past_event', $data);
		return TRUE;
		}



		public function view_uevent(){
	    $query=$this->db->query("SELECT * FROM event where id!=''");
	    return $query->result_array();
		}


		public function edit_uevent($id){
	    $query=$this->db->query("SELECT * FROM event where id = $id");
	    return $query->result_array();
		}

	    public function insert_uevent($data)
		{
		$this->db->insert('event', $data);
		return TRUE;
		}
		
		
		
        /********************************* End Client Model**************************************/
		
		/**************************User profile ***************************/
		
		
	   public function view_profile(){
	     $query=$this->db->query("SELECT * FROM tbl_team");
	     return $query->result_array();
	     }

	   public function edit_profile($id){
	   $query=$this->db->query("SELECT * FROM tbl_usrs WHERE id = $id");
	   return $query->result_array();
	   }

	   public function edit_team($id){
	   $query=$this->db->query("SELECT * FROM tbl_team WHERE id = $id");
	   return $query->result_array();
	   }
	  
       public function insert_userprofile($data){
       	$this->db->insert('tbl_team', $data);
       }

	  /**************************************city dropdown****************************/
	 
	   
		public function getcountry()
		{
        $query=$this->db->query("SELECT * FROM tbl_master_country");
	    return $query->result_array();
		}

        function getstate($country_id)
        {
		$query=$this->db->query("SELECT * FROM tbl_master_country_state WHERE country_id = $country_id");
	    return $query->result_array();
        }
    
        function getcity($state_id)
        {
	    $query=$this->db->query("SELECT * FROM tbl_master_country_city WHERE state_id = $state_id");
	    return $query->result_array();
        }
		
     /***********************************city dropdown****************************/	
	 
	    public function insert_addproduct($data){
	   
	    $this->db->insert('product_table', $data);
		return TRUE;
		}
		
	    public function view_product(){
	    $query=$this->db->query("SELECT * FROM product_table");
	    return $query->result_array();
        }
		
		public function insert_offer($data){
			
		$this->db->insert('tbl_offer', $data);
		return TRUE;
		}
		
		public function view_offer(){
			$query = $this->db->query("select * from tbl_offer");
			return $query->result_array();	
		}
		
		public function edit_offer($id){
		
		$query=$this->db->query("SELECT * FROM tbl_offer WHERE id = $id");
	    return $query->result_array();
        }
		
		
		public function edit_product($id){
		
		$query=$this->db->query("SELECT * FROM product_table WHERE p_id = $id");
	    return $query->result_array();
        }
		
		public function view_users(){
			$query = $this->db->query("select * from tbl_users");
			return $query->result_array();	
		}

		public function view_user($id){
			$query = $this->db->query("select * from tbl_users where id = $id");
			return $query->result_array();	
		}
		public function view_comments(){
			$query = $this->db->query("select * from  tbl_comments");
			return $query->result_array();	
		}
		
		public function view_banner(){
			$query = $this->db->query("select * from site_banner");
			return $query->result_array();	
		}

		public function insert_addbanner($data){

			$this->db->insert('site_banner',$data);
		}

		public function edit_banner($id){

			$query = $this->db->query("SELECT * from site_banner where id = $id");
			return $query->result_array();
		}
		
		public function view_order(){
			$query = $this->db->query("select * from tbl_order inner join product_table on tbl_order.product_id = product_table.p_id  inner join tbl_users on tbl_users.id = tbl_order.user_id");
			return $query->result_array();
			
		}
		
		public function view_site(){
			$query = $this->db->query("select * from tbl_setting");
			return $query->result_array();
			
		}

		public function admin_profile(){
			$query = $this->db->query("select * from tbl_usrs");
			return $query->result_array();
			
		}

		public function insert_addprofile($data){
			$this->db->insert('tbl_usrs', $data);
		return TRUE;
		}


		public function insert_addsetting($data){
			
		$this->db->insert('tbl_setting', $data);
		return TRUE;
		}
		
		public function insert_social($data){
			
		$this->db->insert('tbl_social', $data);
		return TRUE;
		}

		public function view_social(){
			$query = $this->db->query("select * from tbl_social");
			return $query->result_array();
			
		}
		
		
		
// ================= City ===================

		public function insert_city($data){
	$this->db->insert('city', $data);
	return TRUE;
	}

	public function view_city(){
	$query=$this->db->query("SELECT * FROM city");
	return $query->result_array();
	}
	
	public function edit_city($id){
	$query=$this->db->query("SELECT * FROM city WHERE id = $id");
	return $query->result_array();
	}
		
		
	  function getcityforplan($city_id)
        {
	    $query=$this->db->query("SELECT * FROM category WHERE city_id = $city_id");
	    return $query->result_array();
        } 
		   function getplan($city_id)
        {
	    $query=$this->db->query("SELECT * FROM category WHERE id = $city_id");
	    return $query->result_array();
        } 



        // ============================ subscriber =============


        	public function insert_subscriber($data){
	$this->db->insert('subscriber', $data);
	return TRUE;
	}

	public function view_subscriber(){
	$query=$this->db->query("SELECT * FROM subscriber");
	return $query->result_array();
	}
	
	public function edit_subscriber($id){
	$query=$this->db->query("SELECT * FROM subscriber WHERE id = $id");
	return $query->result_array();
	}


public function view_plancity(){
  $query=$this->db->query("SELECT * FROM city");
  return $query->result_array();
  }


  public function walletdata($id){
  $query=$this->db->query("SELECT * FROM wallet_transaction where user_id = $id");
  return $query->result_array();
  }

public function insert_recharge($data){
  $this->db->insert('subscriber', $data);
	return TRUE;
  }

  public function update_wallet($data){
	  $this->db->insert('wallet_transaction', $data);
		return TRUE;
  }

  
		   
}