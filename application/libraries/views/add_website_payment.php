<?php include('headerandsidebar.php'); ?>
      <div class="row">
      <div class="container" id="content">
          <style type="text/css">
    .modal .modal-body {
    max-height: 450px;
    overflow-y: auto;
    } 
</style>
          <form method="post" action="<?php if(!empty($view_payment)){ echo site_url('Welcome/update_website_payment_setting'); } else{ echo site_url('Welcome/insert_website_payment_setting'); } ?>" name="data_register">
          <div class="panel panel-default">
              <div class="panel-heading">Payment Settings</div>
              <ul class="nav nav-tabs nav-justified" role="tablist">
              <li class="active"><a href="#GENERAL" data-toggle="tab">Standard Paypal</a></li>
              <li class=""><a href="#HOTELS_TYPES" data-toggle="tab">Stripe Payment</a></li>
            </ul>
              <div class="panel-body">
			  <?php if($this->session->flashdata('insertmessage')){?>
				  <div class="alert alert-success" align="center">
				  <?php echo $this->session->flashdata('insertmessage'); ?>
					</div>
				  <?php
				  }elseif($this->session->flashdata('updatemessage')){
				  ?>
				  <div class="alert alert-success" align="center">
				  <?php echo $this->session->flashdata('updatemessage'); ?>
					</div>
				  <?php
				  }
				  ?><br>
              <div class="tab-content form-horizontal">
                  <div class="tab-pane wow fadeIn animated active in" id="GENERAL">
                  <div class="clearfix"></div>
                  <hr>
                  <div class="col-md-12 col-sm-12 col-xs-12 form_admin_page">
                      <div class="row form-group">
                      <label  class="col-md-2 control-label text-left">Paypal Show:<sup>*</sup></label>
                      <div class="col-md-10">
                          <?php foreach($view_payment as $data){ ?>
                          <select name="paypal_show" class="form-control" required>
                          <option value="1" <?php if($data['paypal_show'] == 1){ echo 'selected="selected"'; } ?> >Yes</option>
                          <option value="0" <?php if($data['paypal_show'] == 0){ echo 'selected="selected"'; }?> >No</option>
                        </select>
                          <?php } ?>
                        </div>
                    </div>
                      <hr>
                      <div class="row form-group">
                      <label class="col-md-2 control-label text-left">Paypal Url:<sup>*</sup></label>
                      <div class="col-md-10">
                          <input class="form-control" type="text" name="paypal_url" value="<?php foreach($view_payment as $data){ echo $data['paypal_url']; } ?>" required>
                        </div>
                    </div>
                      <hr>
                      <div class="row form-group">
                      <label  class="col-md-2 control-label text-left">Business Account:<sup>*</sup></label>
                      <div class="col-md-10">
                          <input class="form-control" type="text" placeholder="" name="paypal_bussiness"  value="<?php foreach($view_payment as $data){ echo $data['paypal_bussiness']; } ?>" required>
                        </div>
                    </div>
                      <div class="clearfix"></div>
                      <hr>
                    </div>
                </div>
                  <div class="tab-pane wow fadeIn animated in" id="HOTELS_TYPES">
                  <div class="xcrud">
                      <div class="xcrud-container">
                      <div class="xcrud-ajax">
                          <div class="xcrud-top-actions">
                          <div class="clearfix"></div>
                        </div>
                          <br />
                          <div class="xcrud-list-container">
                          <div class="col-md-12 col-sm-12 col-xs-12 form_admin_page">
                              <div class="row form-group">
                              <label class="col-md-2 control-label text-left">Credit/Debit Show:</label>
                              <div class="col-md-2">
                                  <?php foreach($view_payment as $data){ ?>
                                  <select name="stripe_show" class="form-control" name="target">
                                  <option value="1" <?php if($data['stripe_show'] == 1){ echo 'selected="selected"'; } ?>>Yes</option>
                                  <option value="0" <?php if($data['stripe_show'] == 0){ echo 'selected="selected"'; } ?>>No</option>
                                  </select>
                                  <?php } ?>
                                </div>
                            </div>
                              <div class="row form-group">
                              <label class="col-md-2 control-label text-left">Publish Key:</label>
                              <div class="col-md-10">
                                  <input type="text" class="form-control" name="stripe_publish_key" value="<?php foreach($view_payment as $data){ echo $data['stripe_publish_key']; } ?>">
                                </div>
                            </div>
                              <div class="row form-group">
                              <label class="col-md-2 control-label text-left">API Key:</label>
                              <div class="col-md-10">
                                  <input type="text" class="form-control" name="stripe_api_key" value="<?php foreach($view_payment as $data){ echo $data['stripe_api_key']; } ?>">
                                </div>
                            </div>
                            </div>
                          
                        </div>
                        </div>
                      <div class="xcrud-overlay"></div>
                    </div>
                    </div>
                </div>
                </div>
            </div>
              <div class="panel-footer">
              <input type="hidden" name="updatesettings" value="1" />
              <input type="hidden" name="updatefor" value="hotels" />
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>
            </div>
        </form>
        </div>
    </div>
    </div>
  <!----edit modal--->
  <script>
$(document).ready(function(){
if(window.location.hash != "") {
$('a[href="' + window.location.hash + '"]').click() } });
</script>
</div>
  </div>
  </div>
  <?php include('footer.php'); ?>