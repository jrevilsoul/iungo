<!DOCTYPE html><head>
  <meta charset="utf-8">
  <title>Admin Panel</title> 
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">
  <meta name="author" content="">
  <link rel="shortcut icon" href="">

  <link href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" rel="stylesheet">
  
  <link href="<?php echo base_url(); ?>css/loading.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>css/bootstrap.min" rel="stylesheet">
  <link href="<?php echo base_url(); ?>css/style.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>css/admin.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>css/font-awesome.min.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>css/themes/facebook.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>css/animate.min.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>css/dataurl.css" rel="stylesheet">
  <script src="<?php echo base_url(); ?>js/pace.min.js"></script>
  <script src="<?php echo base_url(); ?>js/jquery-1.11.2.js"></script>
  <link href="<?php echo base_url(); ?>css/alert.css" rel="stylesheet" />
  <link href="<?php echo base_url(); ?>css/default/theme.css" rel="stylesheet" />
  <script src="<?php echo base_url(); ?>js/alert.js"></script>
  <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
  <!-- ckeditor -->
  <script src="http://phpexpertfoodordering.in/grill9/ckeditor/ckeditor.js"></script>
  <script type="text/javascript">
  CKEDITOR.replace( 'content_description',
  {
  width:"700",
  toolbar :
  [
  ['Source'],
  ['Cut','Copy','Paste','PasteText','PasteFromWord','-','Scayt'],
  ['Undo','Redo','-','Find','Replace','-','SelectAll','RemoveFormat'],
  ['Image','Flash','Table','HorizontalRule','Smiley','SpecialChar','PageBreak'],
  ['Styles','Format'],['FontSize'],
  ['Bold','Italic','Strike'],
  ['NumberedList','BulletedList','/','Outdent','Indent','Blockquote'],
  ['Link','Unlink','Anchor'],
  ['Maximize','-','About']
  ],
  filebrowserBrowseUrl : 'ckeditor/ckfinder/ckfinder.html',
  filebrowserImageBrowseUrl : 'ckeditor/ckfinder/ckfinder.html?type=Images',
  filebrowserFlashBrowseUrl : 'ckeditor/ckfinder/ckfinder.html?type=Flash',
  filebrowserUploadUrl : 'ckeditor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
  filebrowserImageUploadUrl : 'ckeditor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
  filebrowserFlashUploadUrl : 'ckeditor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
  }
  );
</script>

  <!---Jquery Form --->
  <script src="assets/js/jquery.form.min.js"></script>
  <style>
  .wrapper .main {
  margin-top: 55px;
  }
  @media screen and (max-width: 480px) {
  .wrapper .main {
  margin-top: 100px;
  }
  }
  </style>
</head>
<body>
  <div class="wrapper">
    <script>
    (function ($) {
    // custom css expression for a case-insensitive contains()
    jQuery.expr[':'].Contains = function(a,i,m){
    return (a.textContent || a.innerText || "").toUpperCase().indexOf(m[3].toUpperCase())>=0;
    };
    function FilterMenu(list) {
    var input = $(".filtertxt");
    $(input).change( function () {
    var filter = $(this).val();
    //console.log(filter);
    //If search text box contains some text then apply filter list
    if(filter){
    //Add open class to parent li item
    $(list).parent().addClass('open');
    //Add class in and expand the ul item which is nested li of main ul
    $(list).addClass('in').prop('aria-expanded', 'true').slideDown();
    //Check if child list items contain the query text. Them make them active
    $(list).find('li:Contains('+filter+')').addClass('active');
    //Check if any child list items doesn't contain query text. Remove the active class
    //So that they are not more highlighted
    $(list).find('li:not(:Contains('+filter+'))').removeClass('active');
    //Show any ul inside main ul which contains the text.
    $(list).find('li:Contains('+filter+')').show();
    //Hide any ul inside main ul which doesn't contains the text.
    $(list).find('li:not(:Contains('+filter+'))').hide();
    //Filter top level list items to show and hide them.
    $('#social-sidebar-menu').find('li:Contains('+filter+')').show();
    $('#social-sidebar-menu').find('li:not(:Contains('+filter+'))').hide();
    }else{
    //On query text = empty reset all classes and styles to default.
    $(list).parent().removeClass('open');
    $(list).removeClass('in').prop('aria-expanded', 'false').slideUp();
    $(list).find('li').removeClass('active');
    $('#social-sidebar-menu').find('li').show();
    }
    })
    .keyup( function () {
    $(this).change();
    });
    }
    //ondomready
    $(function () {
    FilterMenu($("#social-sidebar-menu ul"));
    });
    }(jQuery));
    </script>
    <!-- BEGIN SIDEBAR -->
    <aside class="social-sidebar">
      <div class="social-sidebar-content">
        <div class="search-sidebar">
          <form class="search-sidebar-form has-icon filterform" onSubmit="return false;">
            <label for="sidebar-query" class="fa fa-search"></label>
            <input id="sidebar-query" type="text" placeholder="Search" class="search-query filtertxt">
          </form>
        </div>
        <div class="clearfix"></div>
        <div class="user"> <i class="fa-1x glyphicon glyphicon-user"></i> <span>Admin Dashboard</span> </div>
        <div class="menu">
     	      <div class="menu-content">
          <ul id="social-sidebar-menu">

          <li><a href="#sitesetting" data-toggle="collapse" data-parent="#social-sidebar-menu">
            <i class="fa fa-gear"></i><span>Website Settings</span><i class="fa arrow"></i></a>
            <ul id="sitesetting" class="collapse  wow fadeIn animated">
              <li><a href="<?php echo site_url('Welcome/add_general_settings'); ?>">Site Configuration</a></li>
              <li><a href="<?php echo site_url('Welcome/email_settings'); ?>">Site Email Settings</a></li>
              <li><a href="<?php echo site_url('Welcome/website_social_media'); ?>">Site Social Media</a></li>
            <li><a href="<?php echo site_url('Welcome/website_flash_banner'); ?>">Site Website Flash Banner</a></li>  
			<li><a href="<?php echo site_url('Welcome/add_testimonial'); ?>">Add testimonial</a></li>    
            <li><a href="<?php echo site_url('Welcome/testimonial'); ?>">Manage Testimonial</a></li> 
                      
            </ul>
          </li>

          <li><a href="#extras" data-toggle="collapse" data-parent="#social-sidebar-menu">
            <i class="fa fa-user"></i><span>Extra Settings</span><i class="fa arrow"></i></a>
            <ul id="extras" class="collapse  wow fadeIn animated">

              <li><a href="<?php echo site_url('Welcome/view_sms'); ?>">SMS API Settings</a></li>
              <li><a href="<?php echo site_url('Welcome/website_payment_setting'); ?>">Payment Settings</a></li>
              <li><a href="<?php echo site_url('Welcome/website_comments_reviews'); ?>">Comments and Reviews</a></li>
              <li><a href="<?php echo site_url('Welcome/view_website_user_management'); ?>">User management</a></li>
              <li><a href="<?php echo site_url('Welcome/view_voucher'); ?>">Vouchers/ Coupon codes</a></li>
              
            </ul>
          </li>

            <li><a href="#Service" data-toggle="collapse" data-parent="#social-sidebar-menu">
               <i class="fa fa-list"></i><span>Add Provider Type</span> <i class="fa arrow"></i> </a>
               <ul id="Service" class="collapse  wow fadeIn animated">
               <li><a href="<?php echo site_url('Welcome/add_sprovider'); ?>">Add Service Provider</a></li>
            <li>
              <a href="<?php echo site_url('Welcome/view_sprovider'); ?>">List Of Service Providers</a></li>
              </ul>
              </li>
              <li>
                <a href="#Location" data-toggle="collapse" data-parent="#social-sidebar-menu"> <i class="fa fa-map"></i> <span>Location</span> <i class="fa arrow"></i> </a>
                <ul id="Location" class="collapse  wow fadeIn animated">
                  <li><a href="<?php echo site_url('Welcome/add_country'); ?>">Add New Country</a></li>
                  <li><a href="<?php echo site_url('Welcome/view_country'); ?>">List/Manage Countries</a></li>
              
                  <li><a href="<?php echo site_url('Welcome/add_state'); ?>">Add New State</a></li>
                  <li><a href="<?php echo site_url('Welcome/view_state'); ?>">List/Manage States</a></li>
               
                  <li><a href="<?php echo site_url('Welcome/add_city'); ?>">Add New City</a></li>
                  <li><a href="<?php echo site_url('Welcome/view_city'); ?>">List/Manage Cities</a></li>
                </ul>
              </li>

              <li><a href="#Category" data-toggle="collapse" data-parent="#social-sidebar-menu">
                <i class="fa fa-suitcase"></i> <span>All Categories</span> <i class="fa arrow"></i> </a>
                <ul id="Category" class="collapse  wow fadeIn animated">
                <li><a href="<?php echo site_url('Welcome/add_catone'); ?>">Add Main Category</a></li>
                <li><a href="<?php echo site_url('Welcome/view_catone'); ?>">List/Manage Main Category</a></li>
              
              <li><a href="<?php echo site_url('Welcome/add_cattwo'); ?>">Add Sub Category</a></li>
              <li><a href="<?php echo site_url('Welcome/view_cattwo'); ?>">List/Manage Sub Category</a></li>
                
               <!-- <li><a href="<?php //echo site_url('Welcome/add_catthree'); ?>">
                      Add Third Level Category</a></li>

               <li><a href="<?php // echo site_url('Welcome/view_catthree'); ?>">List/Manage Third Level Categories</a></li> -->                  
                </ul>
              </li>
             
        <li><a href="#cms" data-toggle="collapse" data-parent="#social-sidebar-menu"><i class="fa fa-clock-o"></i>
        <span>CMS Management</span><i class="fa arrow"></i></a>
            <ul id="cms" class="collapse  wow fadeIn animated">
              <li><a href="<?php echo site_url('Welcome/cms_aboutus'); ?>">About us</a></li>
              <li><a href="<?php echo site_url('Welcome/cms_services'); ?>">Services</a></li>
              <li><a href="<?php echo site_url('Welcome/cms_privacypolicy'); ?>">Privacy Policy</a></li>
              <li><a href="<?php echo site_url('Welcome/cms_contactus'); ?>">Contact Us</a></li>
              <li><a href="<?php echo site_url('Welcome/cms_tnc'); ?>">Terms and Conditions</a></li>
              <li><a href="<?php echo site_url('Welcome/cms_faq'); ?>">FAQs</a></li>
              <li><a href="<?php echo site_url('Welcome/view_faq'); ?>">Manage FAQs</a></li>
              <li><a href="<?php echo site_url('Welcome/cms_disclaimer'); ?>">Disclaimer</a></li>
              <li><a href="<?php echo site_url('Welcome/cms_cancellation'); ?>">Cancellation Policy</a></li>
              <li><a href="<?php echo site_url('Welcome/cms_career'); ?>">Career</a></li>
              <li><a href="<?php echo site_url('Welcome/cms_dataprotection'); ?>">Dataprotection</a></li>
              <!--<li><a href="<?php //echo site_url('Welcome/add_testimonial'); ?>">Add testimonial</a></li>    
              <li><a href="<?php //echo site_url('Welcome/testimonial'); ?>">Manage Testimonial</a></li> --> 
              <!--<li><a href="<?php //echo site_url('Welcome/add_advertisement'); ?>">add Advertisement </a></li>
              <li><a href="<?php //echo site_url('Welcome/mangage_advertisement'); ?>">Manage Advertisement </a></li>
              <li><a href="<?php //echo site_url('Welcome/philosophy'); ?>">add philosophy </a></li>
              <li><a href="<?php //echo site_url('Welcome/mangage_philos'); ?>">Manage philosophy </a></li>
              <li><a href="<?php //echo site_url('Welcome/add_package'); ?>">Add Package</a></li>
              <li><a href="<?php //echo site_url('Welcome/manage_package'); ?>">Manage Package</a></li>-->
            </ul>
        </li>

        <li><a href="#details" data-toggle="collapse" data-parent="#social-sidebar-menu">
          <i class="fa fa-image"></i><span>Service Provider</span> <i class="fa arrow"></i> </a>
          <ul id="details" class="collapse  wow fadeIn animated">
          <li><a href="<?php echo site_url('Welcome/add_providerdetails'); ?>">Add New Provider</a></li>
          <li><a href="<?php echo site_url('Welcome/view_providerdetails'); ?>">View/Manage Service Provider Details</a></li>
          <li><a href="<?php echo site_url('Upload_files/index'); ?>">Add Gallery Images</a></li>
          <li><a href="<?php echo site_url('Welcome/view_gallery'); ?>">View/Manage Gallery Images</a></li>
		  <li><a href="<?php echo site_url('Upload_files/audio'); ?>">Add Audio</a></li>
          <li><a href="<?php echo site_url('Welcome/add_roomtype'); ?>">Room Settings</a></li>
          <li><a href="<?php echo site_url('Welcome/view_roomtype'); ?>">View/Manage Rooms</a></li>
          <li><a href="<?php echo site_url('Welcome/manage_review'); ?>">Manage Review</a></li>
           <!--<li><a href="<?php //echo site_url('Welcome/add_enquiry'); ?>">Add Provider</a></li>-->
          <li><a href="<?php echo site_url('Welcome/manage_enquiry'); ?>">Manage Enquiry Provider</a></li>
          
          </ul>
          </li>  
          
          <li><a href="#users" data-toggle="collapse" data-parent="#social-sidebar-menu">
          <i class="fa fa-image"></i><span>Mange Users</span> <i class="fa arrow"></i> </a>
          <ul id="users" class="collapse  wow fadeIn animated">
          <li><a href="<?php echo site_url('Welcome/manage_users'); ?>">Mange Users</a></li>
          <li><a href="<?php echo site_url('Welcome/manage_vender'); ?>">Manage Vendors</a></li>
		
          </ul>
          </li> 
          
          <li><a href="#membership" data-toggle="collapse" data-parent="#social-sidebar-menu">
          <i class="fa fa-image"></i><span>Membership</span> <i class="fa arrow"></i> </a>
          <ul id="membership" class="collapse  wow fadeIn animated">
          <li><a href="<?php echo site_url('Welcome/add_membership'); ?>">Add Membership</a></li>
          <li><a href="<?php echo site_url('Welcome/view_membership'); ?>">Manage Membership</a></li>
          </ul>
          </li> 
          <li><a href="#booking" data-toggle="collapse" data-parent="#social-sidebar-menu">
          <i class="fa fa-image"></i><span>Booking Section</span> <i class="fa arrow"></i> </a>
          <ul id="booking" class="collapse  wow fadeIn animated">
          <li><a href="<?php echo site_url('Welcome/manage_booking'); ?>">Manage Booking</a></li>
          
          </ul>
          </li>
		   <li><a href="#Language" data-toggle="collapse" data-parent="#social-sidebar-menu"> <i class="fa fa-image"></i><span>Language</span> <i class="fa arrow"></i> </a>
            <ul id="Language" class="collapse  wow fadeIn animated">
              <li><a href="<?php echo site_url('Welcome/header_navigation'); ?>">Header Navigation</a></li>
              <li><a href="<?php echo site_url('Welcome/footer_navigation'); ?>">Front Footer</a></li>
              <li><a href="<?php echo site_url('Welcome/front_navigation'); ?>">Front Navigation</a></li>
			  <li><a href="<?php echo site_url('Welcome/frontdetail_navigation'); ?>">Front detail Navigation</a></li>
            </ul>
          </li>
        </div>
      </div>
    <!-- END MENU SECTION-->
  </div>
</aside>
<header>
  <!-- BEGIN NAVBAR-->
  <input type="hidden" id="sidebarclass" class="">
  <nav role="navigation" class="navbar navbar-fixed-top navbar-super social-navbar">
    <div class="navbar-header"> <a href="http://demo/supplier" class="navbar-brand"> <i class="fa fa-inbox light"></i> <span>Dashboard</span> </a> </div>
    <div class="navbar-toggle navtogglebtn"><i class="fa fa-align-justify"></i> </div>
    <div>
      <ul class="nav navbar-nav">
        <li class="dropdown navbar-super-fw hidden-xs">
          <a><span>
          <script> function startTime() { var today=new Date(); var h=today.getHours(); var m=today.getMinutes(); var s=today.getSeconds(); m=checkTime(m); s=checkTime(s); document.getElementById('txt').innerHTML=h+":"+m+":"+s; t=setTimeout(function(){startTime()},500); } function checkTime(i) { if (i<10) { i="0" + i; } return i; } </script>
          <strong> <body onload="startTime()" class="">
          </body><div class="pull-left wow fadeInLeft animated" id="txt"></div>
          </strong>&nbsp;|&nbsp;<small class="pull-right wow fadeInRight animated">
          <script> var tD = new Date(); var datestr =  tD.getDate(); document.write(""+datestr+""); </script>
          <script type="text/javascript"> var d=new Date(); var weekday=new Array("","","","","","", ""); var monthname=new Array("January","February","March","April","May","June","July","August","September","Octobar","November","December"); document.write(monthname[d.getMonth()] + " "); </script>
          <script> var tD = new Date(); var datestr = tD.getFullYear(); document.write(""+datestr+""); </script>
          </small></span>
          </a>
        </li>
      </ul>
    <ul class="nav navbar-nav navbar-right">
      <!-- END DROPDOWN MESSAGES-->
      <li class="divider-vertical"></li>
      <!-- BEGIN EXTRA DROPDOWN-->
      <li class="dropdown"><a href="#" data-toggle="dropdown" data-hover="dropdown" data-delay="0" class="dropdown-toggle"><i class="fa fa-caret-down fa-lg"></i></a>
        <ul class="dropdown-menu">     
        <li><a href="supplier_profile.html"><i class="glyphicon glyphicon-user">          
        </i>&nbsp;<?php echo $this->session->userdata('username'); ?></a></li>

        <li><a href="<?php echo site_url('welcome/logout'); ?>"><i class="fa fa-sign-out"></i>&nbsp;Log Out</a></li>
        <li class="divider"></li>
        <li><a href="supplier_help.html" target="_blank"><i class="fa fa-info"></i>&nbsp;Help</a></li>
        </ul>
      </li>
    <!-- END EXTRA DROPDOWN-->
  </ul>
</div>
<!-- /.navbar-collapse-->
</nav>
<!-- END NAVBAR-->
</header>
<script type="text/javascript">
$(function(){
var sideClass = $("#sidebarclass").prop('class');
$('body').addClass(sideClass);
$(".navtogglebtn").on('click',function(){
var sidebar = '';
if($('body').hasClass('reduced-sidebar')){
sidebar = "";
}else{
sidebar = "reduced-sidebar";
}
$.post("http://demo/admin/ajaxcalls/reduceSidebar", {sidebar: sidebar}, function(resp){
});
});
});
function getNotifications(){
$.post("http://demo/admin/ajaxcalls/notifications",{},function(response){
var resp = $.parseJSON(response);
if(resp.totalReviews > 0){
$(".notifyRevCount").html(resp.totalReviews);
$(".revnotifyHeader").show();
/*if($("li").hasClass("notificationReviews")){
}else{
}*/
$(".notificationReviews").remove();
$(".revnotifyHeader").after(resp.revhtml);
}else{
$(".revnotifyHeader").hide();
$(".notifyRevCount").html("");
}
//Supplier notifications
if(resp.totalAccounts > 0){
$(".notifyAccountsCount").html(resp.totalAccounts);
$(".accountsnotifyHeader").show();
if($("li").hasClass("notificationAccounts")){
}else{
$(".accountsnotifyHeader").after(resp.accountshtml);
}
}else{
$(".accountsnotifyHeader").hide();
$(".notifyAccountsCount").html("");
}
//Booking notifications
if(resp.totalBookings > 0){
$(".notifyBookingsCount").html(resp.totalBookings);
$(".bookingsnotifyHeader").show();
if($("li").hasClass("notificationBookings")){
}else{
$(".bookingsnotifyHeader").after(resp.bookingshtml);
}
}else{
$(".bookingsnotifyHeader").hide();
$(".notifyBookingsCount").html("");
}
});
}
</script>
<div class="main">
<div class="container" id="content">
<!-- <script type="text/javascript">
$(function(){
var room = $("#roomid").val();
$(".submitfrm").click(function(){
var submitType = $(this).prop('id');
for ( instance in CKEDITOR.instances )
{
CKEDITOR.instances[instance].updateElement();
}
$(".output").html("");
$('html, body').animate({
scrollTop: $('body').offset().top
}, 'slow');
if(submitType == "add"){
url = "#" ;
}else{
url = "#"+room;
}
$.post(url,$(".room-form").serialize() , function(response){
if($.trim(response) != "done"){
$(".output").html(response);
}else{
window.location.href = "#";
}
});
})
})
</script> -->