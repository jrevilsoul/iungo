<?php include('headerandsidebar.php'); ?>

  <div class="output"></div>
  <div class="panel panel-default">
  <div class="panel-heading">Add Provider Type</div>
  <ul class="nav nav-tabs nav-justified" role="tablist">
      <li class="active"><a href="#GENERAL" data-toggle="tab">General</a></li>
    
      <li class=""><a href="#TRANSLATE" data-toggle="tab">Translate</a></li>
    </ul>
  <div class="panel-body">
<?php if($this->session->flashdata('insertmessage')){?>
				  <div class="alert alert-success" align="center">
				  <?php echo $this->session->flashdata('insertmessage'); ?>
					</div>
				  <?php
				  }elseif($this->session->flashdata('updatemessage')){
				  ?>
				  <div class="alert alert-success" align="center">
				  <?php echo $this->session->flashdata('updatemessage'); ?>
					</div>
				  <?php
				  }
				  ?>
  <br>
  <div class="tab-content form-horizontal">
  <div class="tab-pane wow fadeIn animated active in" id="GENERAL" >
  <form method="POST" action="<?php echo site_url('Welcome/submit_sprovider'); ?>" enctype="multipart/form-data">
  
      <div class="clearfix"></div>
      <div class="row form-group">
	  
      <label class="col-md-2 control-label text-left">Status</label>
      <div class="col-md-2">
          <select class="form-control" name="status" id="status">
          <option value="1">Enabled</option>
          <option value="0">Disabled</option>
        </select>
        </div>
    </div>
      <div class="row form-group">
      <label class="col-md-2 control-label text-left">Provider Type</label>
      <div class="col-md-4">
          <input type="text" value="" class="form-control" name="en_serviceTypeName" required>
        </div>
    </div>
	
      <div class="row form-group">
      <label class="col-md-2 control-label text-left">Provider Image</label>
      <div class="col-md-4">
          <input type="file" name="en_serviceTypeIcon" />
        </div>
    </div>
	
      <div class="row form-group">
      <label class="col-md-2 control-label text-left">Provider Type Description</label>
      <div class="col-md-10">
          <textarea name="en_serviceTypeDesc" id="roomdesc" rows="8" cols="60"></textarea>
          <script type="text/javascript">//<![CDATA[
                window.CKEDITOR_BASEPATH='l/application/libraries/ckeditor.php';
                //]]></script>
          <script type="text/javascript" src="l/application/libraries/ckeditor.phpckeditor.js?t=B5GJ5GG"></script>
          <script type="text/javascript">//<![CDATA[
                CKEDITOR.replace('roomdesc', {"toolbar":[["Source","-","Bold","Italic","Underline","Strike","Format","Styles"],["NumberedList","BulletedList","Outdent","Indent","Blockquote"],["Image","Link","Unlink","Anchor","Table","HorizontalRule","SpecialChar","Maximize"],["Cut","Copy","Paste","PasteText","PasteFromWord","-","Undo","Redo","Find","Replace","-","SelectAll","-","SpellChecker","Scayt"]],"language":"en"});
                //]]>
                </script>
        </div>
    </div>
      <div class="row form-group">
      <label class="col-md-2 control-label text-left">Meta Title</label>
      <div class="col-md-10">
          <textarea name="en_mtitle" id=translated[fr][desc] rows="8" cols="60"></textarea>
          <script type="text/javascript">//<![CDATA[
                CKEDITOR.replace('translated[fr][desc]', {"toolbar":[["Source","-","Bold","Italic","Underline","Strike","Format","Styles"],["NumberedList","BulletedList","Outdent","Indent","Blockquote"],["Image","Link","Unlink","Anchor","Table","HorizontalRule","SpecialChar","Maximize"],["Cut","Copy","Paste","PasteText","PasteFromWord","-","Undo","Redo","Find","Replace","-","SelectAll","-","SpellChecker","Scayt"]],"language":"en"});
                //]]></script>
        </div>
    </div>
      <div class="row form-group">
      <label class="col-md-2 control-label text-left">Meta Key</label>
      <div class="col-md-10">
          <textarea name="en_mkey" id=translated[ar][desc] rows="8" cols="60"></textarea>
          <script type="text/javascript">//<![CDATA[
                CKEDITOR.replace('translated[ar][desc]', {"toolbar":[["Source","-","Bold","Italic","Underline","Strike","Format","Styles"],["NumberedList","BulletedList","Outdent","Indent","Blockquote"],["Image","Link","Unlink","Anchor","Table","HorizontalRule","SpecialChar","Maximize"],["Cut","Copy","Paste","PasteText","PasteFromWord","-","Undo","Redo","Find","Replace","-","SelectAll","-","SpellChecker","Scayt"]],"language":"en"});
                //]]>       
                </script>
        </div>
    </div>
      <div class="row form-group">
      <label class="col-md-2 control-label text-left">Meta Description</label>
      <div class="col-md-10">
          <textarea name="en_mdesc" id=translated[tl][desc] rows="8" cols="60"></textarea>
          <script type="text/javascript">//<![CDATA[
                CKEDITOR.replace('translated[tl][desc]', {"toolbar":[["Source","-","Bold","Italic","Underline","Strike","Format","Styles"],["NumberedList","BulletedList","Outdent","Indent","Blockquote"],["Image","Link","Unlink","Anchor","Table","HorizontalRule","SpecialChar","Maximize"],["Cut","Copy","Paste","PasteText","PasteFromWord","-","Undo","Redo","Find","Replace","-","SelectAll","-","SpellChecker","Scayt"]],"language":"en"});
                //]]></script>
        </div>
    </div>
      </div>
      
      <div class="tab-pane wow fadeIn animated in" id="TRANSLATE">
      <div class="clearfix"></div>
      <p>&nbsp;&nbsp;&nbsp;&nbsp; German:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="<?php echo base_url('img/') . 'flag.png'; ?>" height="75px;" width="150px;"></p>
      <div class="row form-group">
          <label class="col-md-2 control-label text-left">Provider Type</label>
          <div class="col-md-4">
          <input type="text" value="" class="form-control" name="gr_serviceTypeName" >
        </div>
        </div>
      <div class="row form-group">
          <label class="col-md-2 control-label text-left">Provider Type Description</label>
          <div class="col-md-10">
          <textarea name="gr_serviceTypeDesc" id=translated[gr_pdesc][desc] rows="8" cols="60"></textarea>
          <script type="text/javascript">//<![CDATA[
                CKEDITOR.replace('translated[gr_pdesc][desc]', {"toolbar":[["Source","-","Bold","Italic","Underline","Strike","Format","Styles"],["NumberedList","BulletedList","Outdent","Indent","Blockquote"],["Image","Link","Unlink","Anchor","Table","HorizontalRule","SpecialChar","Maximize"],["Cut","Copy","Paste","PasteText","PasteFromWord","-","Undo","Redo","Find","Replace","-","SelectAll","-","SpellChecker","Scayt"]],"language":"en"});
                //]]></script>
        </div>
        </div>
      <div class="row form-group">
          <label class="col-md-2 control-label text-left">Meta Title</label>
          <div class="col-md-10">
          <textarea name="gr_mtitle" id=translated[gr_mtitle][desc] rows="8" cols="60"></textarea>
          <script type="text/javascript">//<![CDATA[
                CKEDITOR.replace('translated[gr_mtitle][desc]', {"toolbar":[["Source","-","Bold","Italic","Underline","Strike","Format","Styles"],["NumberedList","BulletedList","Outdent","Indent","Blockquote"],["Image","Link","Unlink","Anchor","Table","HorizontalRule","SpecialChar","Maximize"],["Cut","Copy","Paste","PasteText","PasteFromWord","-","Undo","Redo","Find","Replace","-","SelectAll","-","SpellChecker","Scayt"]],"language":"en"});
                //]]></script>
        </div>
        </div>
      <div class="row form-group">
          <label class="col-md-2 control-label text-left">Meta Key</label>
          <div class="col-md-10">
          <textarea name="gr_mkey" id=translated[gr_mkey][desc] rows="8" cols="60"></textarea>
          <script type="text/javascript">//<![CDATA[
                CKEDITOR.replace('translated[gr_mkey][desc]', {"toolbar":[["Source","-","Bold","Italic","Underline","Strike","Format","Styles"],["NumberedList","BulletedList","Outdent","Indent","Blockquote"],["Image","Link","Unlink","Anchor","Table","HorizontalRule","SpecialChar","Maximize"],["Cut","Copy","Paste","PasteText","PasteFromWord","-","Undo","Redo","Find","Replace","-","SelectAll","-","SpellChecker","Scayt"]],"language":"en"});
                //]]></script>
        </div>
        </div>
      <div class="row form-group">
          <label class="col-md-2 control-label text-left">Meta Description</label>
          <div class="col-md-10">
          <textarea name="gr_mdesc" id=translated[grmetadesc][desc] rows="8" cols="60"></textarea>
          <script type="text/javascript">//<![CDATA[
                CKEDITOR.replace('translated[grmetadesc][desc]', {"toolbar":[["Source","-","Bold","Italic","Underline","Strike","Format","Styles"],["NumberedList","BulletedList","Outdent","Indent","Blockquote"],["Image","Link","Unlink","Anchor","Table","HorizontalRule","SpecialChar","Maximize"],["Cut","Copy","Paste","PasteText","PasteFromWord","-","Undo","Redo","Find","Replace","-","SelectAll","-","SpellChecker","Scayt"]],"language":"en"});
                //]]></script>
        </div>
        </div>
    </div>
      </div>
      <div class="panel-footer">
      <!--<input type="hidden" id="roomid" name="roomid" value="" />
          <input type="hidden" name="oldquantity" value="" />
          <input type="hidden" name="submittype" value="add" /> -->
      <input class="btn btn-primary" type="submit" value="Submit">
    </div>
      </div>
    </form>
</div>
  <?php include('footer.php'); ?>
  </body>
  </html>
