<!-- Bootstrap JS-->
<script src="<?php echo base_url(); ?>js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>js/bootstrap-hover-dropdown.min.js"></script>
<script src="<?php echo base_url(); ?>js/sidebar.js"></script>
<script src="<?php echo base_url(); ?>js/panels.js"></script>

<!-- icheck -->
<script src="<?php echo base_url(); ?>js/icheck.min.js"></script>

<link href="<?php echo base_url(); ?>css/grey.css" rel="stylesheet">

            <script>
                $("#checkAll").change(function () {
                        $("input:checkbox").prop('checked', $(this).prop("checked"));
                    });
              </script>

              <script>
                  $('#checkAll').change(function(){
                  if($(this).is(":checked"))
                  $('#autoUpdate').show();
                  else
                  $('#autoUpdate').hide();
                  }); 
              </script>

                <script>
                    var cb, optionSet1;
                    $(function () {
                        var checkAll = $('input.all');
                        var checkboxes = $('input.minimal');
                        $('input').iCheck({
                          checkboxClass: "icheckbox_square-grey",
                        });
                        checkAll.on('ifChecked ifUnchecked', function(event) {
                            if (event.type == 'ifChecked') {
                                checkboxes.iCheck('check');
                            } else {
                                checkboxes.iCheck('uncheck');
                            }
                        });
                        checkboxes.on('ifChanged', function(event){
                            if(checkboxes.filter(':checked').length == checkboxes.length) {
                                checkAll.prop('checked', 'checked');
                            } else {
                                checkAll.removeProp('checked');
                            }
                            checkAll.iCheck('update');
                        });
                    });

                    $(".radio").iCheck({
                    checkboxClass: "icheckbox_square-grey",
                    radioClass: "iradio_square-grey"
                    });
                </script>

<!-- datepicker -->
<script src="<?php echo base_url(); ?>js/datepicker.js"></script>
<link rel="stylesheet" href="<?php echo base_url(); ?>css/datepicker.css" />
<script>
var fmt = "dd/mm/yyyy";
if (top.location != location) { top.location.href = document.location.href ; }
$(function(){ window.prettyPrint && prettyPrint(); $('.dob').datepicker({format: fmt,autoclose: true}).on('changeDate', function (ev) {
$(this).datepicker('hide'); });
$('#dp1').datepicker();
$('#dp2').datepicker();

// disabling dates
var nowTemp = new Date();
var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
var checkin = $('.dpd1').datepicker({format: fmt, onRender: function(date) { return date.valueOf() < now.valueOf() ? 'disabled' : ''; } }).on('changeDate', function(ev) {
if (ev.date.valueOf() > checkout.date.valueOf()) {
var newDate = new Date(ev.date)
newDate.setDate(newDate.getDate() + 1); checkout.setValue(newDate); } checkin.hide();
$('.dpd2')[0].focus(); }).data('datepicker'); var checkout = $('.dpd2').datepicker({format: fmt,
onRender: function(date) { return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : ''; }
}).on('changeDate', function(ev) { checkout.hide(); }).data('datepicker'); });
</script>

<!-- timepicker -->
<script src="<?php echo base_url(); ?>js/timepicker.js"></script>
<link rel="stylesheet" href="<?php echo base_url(); ?>css/timepicker.css" />
<script>
$(function(){
$('.timepicker').clockface(); });
</script>

<!-- dronzone -->
<link href="<?php echo base_url(); ?>css/dropzone.css" type="text/css" rel="stylesheet" />
<script src="<?php echo base_url(); ?>js/dropzone.min.js"></script>

<!--Custom functions file -->
<script src="<?php echo base_url(); ?>js/funcs.js"></script>
<!--Custom functions file-->

<!-- pnotify -->
<script src="<?php echo base_url(); ?>js/pnotify.custom.min.js"></script>
<script src="<?php echo base_url(); ?>js/common.js"></script>
<link href="<?php echo base_url(); ?>css/pnotify.custom.css" rel="stylesheet">

<!-- select2 -->
<link href="<?php echo base_url(); ?>css/select2.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>css/select2-default.css" rel="stylesheet">
<script src="<?php echo base_url(); ?>js/select2.min.js"></script>
<script>
$(function() {
$('.chosen-select').select2( { width:'100%', maximumSelectionSize: 1 } );
$(document).ready(function() {
$(".chosen-multi-select").select2( { width:'100%', } ); }); }); function slideout(){ setTimeout(function(){
$(".alert-success").fadeOut("slow", function () { });
$(".alert-danger").fadeOut("slow", function () { }); }, 4000);}
</script>

<!-- smothwhell starts-->
<script src="<?php echo base_url(); ?>js/smoothwheel.js"></script>
<!-- smothwhell ends-->

<!-- jQuery slimScroll-->
<script src="<?php echo base_url(); ?>js/jquery.slimscroll.min.js"></script>
<script>
window.jQuery.ui || document.write('<script src="<?php echo base_url(); ?>js/jquery.slimscroll.min.js"><\/script>')
</script>
<script src="<?php echo base_url(); ?>js/wow.min.js"></script>
<script>
/*<![CDATA[*/
$(function() {
$(".social-sidebar").socialSidebar();
$('.main').panels();
});
/*]]>*/
</script>

