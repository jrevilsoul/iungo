<?php include('headerandsidebar.php'); ?>

  <h3 class="margin-top-0">Add Enquiry</h3>
  
  <div class="output"></div>
  <div class="panel panel-default">
    <ul class="nav nav-tabs nav-justified" role="tablist">
      <li class="active"><a href="#GENERAL" data-toggle="tab">General</a></li>
      <!--  <li class=""><a href="#AMENITIES" data-toggle="tab">Amenities</a></li> -->
      <li class=""><a href="#TRANSLATE" data-toggle="tab">Translate</a></li>
    </ul>
    <div class="panel-body">
    <?php echo $this->session->flashdata('msg'); ?>
     <br>
      <div class="tab-content form-horizontal">
        <div class="tab-pane wow fadeIn animated active in" id="GENERAL" >
         <form method="POST" action="<?php echo site_url('Welcome/insert_enquiry'); ?>" enctype="multipart/form-data">
            <div class="clearfix"></div>
            <div class="row form-group">
              <label class="col-md-2 control-label text-left">Status</label>
              <div class="col-md-2">
                <select class="form-control" name="status" id="status">
                  <option value="1">Enabled</option>
                  <option value="0">Disabled</option>
                </select>
              </div>
            </div>
            <div class="row form-group">
              <label class="col-md-2 control-label text-left">Name</label>
              <div class="col-md-4">
                <input type="text"  class="form-control" placeholder="Name" name="name" required="">
              </div>
            </div>
            <div class="row form-group">
              <label class="col-md-2 control-label text-left">Phone</label>
              <div class="col-md-4">
                <input type="text"  class="form-control" placeholder="Phone" name="phone" required="">
              </div>
            </div>
            <div class="row form-group">
              <label class="col-md-2 control-label text-left">Enquiry Description</label>
              <div class="col-md-4">
                <input type="text" name="des" class="form-control" placeholder="Enquiry Description" />
              </div>
            </div>
          </div>
        </div>
        <div class="panel-footer">
          <!--<input type="hidden" id="roomid" name="roomid" value="" />
          <input type="hidden" name="oldquantity" value="" />
          <input type="hidden" name="submittype" value="add" /> -->
          <input class="btn btn-primary" type="submit" value="Submit">
        </div>
      </div>
    </div>
  </div>
</form>
</div>
<?php include('footer.php'); ?>
</body>
</html>