<?php include('headerandsidebar.php'); ?>
<div class="row">
  <div class="container" id="content">
    <style type="text/css">
    .modal .modal-body {
    max-height: 450px;
    overflow-y: auto;
    }
    </style>
    
	
    <form action="<?php echo site_url('welcome/submit_providerdetails'); ?>" method="POST" enctype="multipart/form-data">
      <div class="panel panel-default">
      <div class="panel-heading">Service Provider Details</div>
	    <?php if($this->session->flashdata('insertmessage')){?>
				  <div class="alert alert-success" align="center">
				  <?php echo $this->session->flashdata('insertmessage'); ?>
					</div>
				  <?php
				  }elseif($this->session->flashdata('updatemessage')){
				  ?>
				  <div class="alert alert-success" align="center">
				  <?php echo $this->session->flashdata('updatemessage'); ?>
					</div>
				  <?php
				  }
				  ?>
        <ul class="nav nav-tabs nav-justified" role="tablist">
          <li class="active"><a href="#GENERAL" data-toggle="tab">General</a></li>
          <li class=""><a href="#HOTELS_TYPES" data-toggle="tab">Social Media Links</a></li>
      
        </ul>
        <div class="panel-body"><br>
          <div class="tab-content form-horizontal">
            <div class="tab-pane wow fadeIn animated active in" id="GENERAL">
              <div class="clearfix"></div>
             
               <div class="col-md-12 col-sm-12 col-xs-12 form_admin_page">
              <div class="row form-group">
                <label  class="col-md-2 control-label text-left">Select Provider Type:</label>
                <div class="col-md-4"> <div>                 
                  </div>
                 
                    <select  name="providertype" class="form-control" name="target" required>
                      <option value="">--SELECT PROVIDER TYPE--</option>
                      <?php foreach($view_provider as $nameprovider) { ?>
                      <option value="<?php echo $nameprovider['id']; ?>">
                      <?php echo $nameprovider['en_serviceTypeName']; ?></option>
                      <?php } ?>
                    </select>
                  </div>
                <label  class="col-md-2 control-label text-left">Select Category:</label>
                <div class="col-md-3">
                    <select  name="catname" class="form-control"required>
                      <option value="">--SELECT CATGORY--</option>
                      <?php foreach ($view_catone as $nameprovider) { ?>
                      <option value="<?php echo $nameprovider['id']; ?>">
                      <?php echo $nameprovider['catonename']; ?></option>
                      <?php } ?>
                    </select>
                </div>
              </div>

              <div class="row form-group">
                <label  class="col-md-2 control-label text-left">Status:</label>
                <div class="col-md-4">
                    <select  name="status" class="form-control" name="target" required>
                      <option value="1">Enable</option>
                      <option value="0">Disable</option>
                    </select>
                </div>
              </div>
              <hr>
              <div class="row form-group">
                <label  class="col-md-2 control-label text-left">Provider Title (Name):</label>
                <div class="col-md-4">
                  <input class="form-control" type="text" placeholder="" name="provider_name" value="">
                </div>             
              </div>

              <div class="row form-group">
                <label  class="col-md-2 control-label text-left">Provider Image:</label>
                <div class="col-md-4">
                  <input class="form-control" type="file" name="provider_image" />
                </div>
                 <label  class="col-md-2 control-label text-left">provider video:</label>
                <div class="col-md-3">
                  <input class="form-control" type="file" placeholder="" name="video_name"  value="">
                </div> 				
              </div>

              <hr>
              <div class="row form-group">
                <label  class="col-md-2 control-label text-left">Provider Description:</label>
                <div class="col-md-4">
                  <input class="form-control" type="text" placeholder="" name="desc"  value="" required>
                </div>
                
              </div>
              <div class="clearfix"></div>
              <div class="row form-group">

              <label  class="col-md-2 control-label text-left">City:</label>
              <?php $sql = "SELECT * FROM `tbl_city`";
                $query = $this->db->query($sql); ?>

                <div class="col-md-4">
                  <select class="form-control" name="provider_city" required>
                    <option value="-1" label="" >--SELECT CITY--</option>
               <?php foreach ($query->result() as $row) { ?>
                    <option value="<?php echo $row->cityid;  ?>"><?php echo $row->cityName; ?></option>
                <?php } ?>
                  </select>
                </div>
                <label  class="col-md-2 control-label text-left">Zip:</label>
                <div class="col-md-3">
                  <input class="form-control" type="text" placeholder="" name="zipcode"  value="">
                </div>                
              </div>

              <div class="clearfix"></div>
              <div class="row form-group">
                <label  class="col-md-2 control-label text-left">Address:</label>
                <div class="col-md-4">
                  <input class="form-control" type="text" placeholder="" name="address"  value="">
                </div> 
                <label  class="col-md-2 control-label text-left">outside:</label>
                <div class="col-md-3">
                  <input class="form-control" type="text" placeholder="" name="outside"  value="">
                </div> 				
              </div>
			    <div class="clearfix"></div>
              <div class="row form-group">
                <label  class="col-md-2 control-label text-left">person of detail:</label>
                <div class="col-md-4">
                  <input class="form-control" type="text" placeholder="" name="persondetail"  value="">
                </div> 
                <label  class="col-md-2 control-label text-left">square of area:</label>
                <div class="col-md-3">
                  <input class="form-control" type="text" placeholder="" name="square"  value="">
                </div> 				
              </div>
			    <div class="clearfix"></div>
              <div class="row form-group">
                <label  class="col-md-2 control-label text-left">Location:</label>
                <div class="col-md-4">
                  <input class="form-control" type="text" placeholder="" name="location"  value="">
                </div> 
                <label  class="col-md-2 control-label text-left">catering:</label>
                <div class="col-md-3">
                  <input class="form-control" type="text" placeholder="" name="catering"  value="">
                </div> 				
              </div>
			    <div class="clearfix"></div>
              <div class="row form-group">
                <label  class="col-md-2 control-label text-left">Times booking:</label>
                <div class="col-md-4">
                  <input class="form-control" type="text" placeholder="" name="booking"  value="">
                </div> 
                <label  class="col-md-2 control-label text-left">Our opinion:</label>
                <div class="col-md-3">
                  <input class="form-control" type="text" placeholder="" name="opinion"  value="">
                </div> 				
              </div>
			    <div class="clearfix"></div>
              <div class="row form-group">
                <label  class="col-md-2 control-label text-left">Detail of Events:</label>
                <div class="col-md-4">
                 <textarea rows="3" cols="46" name="detailevent" value=""></textarea>
                </div> 			
              </div>

              <div class="clearfix"></div>
              <div class="row form-group">
                <label class="col-md-2 control-label text-left">Contact Name:</label>
                <div class="col-md-4">
                 <input class="form-control" type="text" name="contact_name" value="" required>
                </div>
                <label class="col-md-2 control-label text-left">Email:<sup>*</sup></label>
                <div class="col-md-3"><input class="form-control" type="text" placeholder="" name="email"  value=""></div>
              </div>
              
              <div class="row form-group">
                <label  class="col-md-2 control-label text-left">Phone:<sup>*</sup></label>
                <div class="col-md-4">
                  <input class="form-control" type="text" placeholder="" name="phone"  value="" required>
                </div>
                <label  class="col-md-2 control-label text-left">Mobile:</label>
                <div class="col-md-3">
                  <input class="form-control" type="text" placeholder="" name="mobile"  value="">
                </div>
              </div>
              <hr>
              <div class="row" >
            <label  class="col-md-2 control-label text-left">Policy:</label>
              <div class="col-md-9">
              <textarea name="policy" id="one" rows="8" cols="20">                
              </textarea>
                <script type="text/javascript">//<![CDATA[
                window.CKEDITOR_BASEPATH='l/application/libraries/ckeditor.php';
                //]]></script>
                <script type="text/javascript" src="l/application/libraries/ckeditor.phpckeditor.js?t=B5GJ5GG"></script>
                <script type="text/javascript">//<![CDATA[
                CKEDITOR.replace('one', {"toolbar":[["Source","-","Bold","Italic","Underline","Strike","Format","Styles"],["NumberedList","BulletedList","Outdent","Indent","Blockquote"],["Image","Link","Unlink","Anchor","Table","HorizontalRule","SpecialChar","Maximize"],["Cut","Copy","Paste","PasteText","PasteFromWord","-","Undo","Redo","Find","Replace","-","SelectAll","-","SpellChecker","Scayt"]],"language":"en"});
                //]]>
                </script>
              </div>
              </div>
              <hr>
               <div class="row" >
            <label  class="col-md-2 control-label text-left">Terms & Conditions:</label>
              <div class="col-md-9">
              <textarea name="tnc" id="two" rows="8" cols="20">                
              </textarea>
                <script type="text/javascript">//<![CDATA[
                window.CKEDITOR_BASEPATH='l/application/libraries/ckeditor.php';
                //]]></script>
                <script type="text/javascript" src="l/application/libraries/ckeditor.phpckeditor.js?t=B5GJ5GG"></script>
                <script type="text/javascript">//<![CDATA[
                CKEDITOR.replace('two', {"toolbar":[["Source","-","Bold","Italic","Underline","Strike","Format","Styles"],["NumberedList","BulletedList","Outdent","Indent","Blockquote"],["Image","Link","Unlink","Anchor","Table","HorizontalRule","SpecialChar","Maximize"],["Cut","Copy","Paste","PasteText","PasteFromWord","-","Undo","Redo","Find","Replace","-","SelectAll","-","SpellChecker","Scayt"]],"language":"en"});
                //]]>
                </script>
              </div>
              </div>
              <hr>
     
              </div>
            </div>
           
            <div class="tab-pane wow fadeIn animated in" id="HOTELS_TYPES">
             <div class="xcrud">
                <div class="xcrud-container">
                  <div class="xcrud-ajax">
                    <input type="hidden" class="xcrud-data" name="key" value="" />
                    <input type="hidden" class="xcrud-data" name="orderby" value="" />
                    <input type="hidden" class="xcrud-data" name="order" value="desc" />
                    <input type="hidden" class="xcrud-data" name="start" value="0" />
                    <input type="hidden" class="xcrud-data" name="limit" value="10" />
                    <input type="hidden" class="xcrud-data" name="instance" value="" />
                    <input type="hidden" class="xcrud-data" name="task" value="list" />
                    
                   
                    <div class="xcrud-list-container">
                    
                    <div class="col-md-12 col-sm-12 col-xs-12 form_admin_page">
                    <div class="row form-group">
          <label class="col-md-2 control-label text-left">Facebook Link</label>
          <div class="col-md-10">
          <input type="hidden" name="providerid" value="<?php // echo $a; ?>">
         <input type="text" class="form-control"  name="fb" placeholder="Please enter Facebook id" value="">
        </div>
        </div>
        <div class="row form-group">
          <label class="col-md-2 control-label text-left">Twitter Link</label>
          <div class="col-md-10">
       <input type="text" class="form-control" placeholder="Please enter Twitter id" name="twitter" value="">
        </div>
        </div>
        <div class="row form-group">
          <label class="col-md-2 control-label text-left">Google Plus Link</label>
          <div class="col-md-10">
        <input type="text" class="form-control"  placeholder="Please enter Googleplus id" name="googleplus" value="">
        </div>
        </div>
        <div class="row form-group">
          <label class="col-md-2 control-label text-left">Linkedin Link</label>
          <div class="col-md-10">
        <input type="text" class="form-control"  placeholder="Please enter Linkedin id" name="linkedin" value="">
        </div>
        </div>
        <div class="row form-group">
          <label class="col-md-2 control-label text-left">Instagram Link:</label>
          <div class="col-md-10">
         <input type="text" class="form-control" placeholder="Please enter Instagram id" name="instagram" value="">
        </div>
        </div>
        <div class="row form-group">
          <label class="col-md-2 control-label text-left">Tags:</label>
          <div class="col-md-10">
        <input type="text" class="form-control" placeholder="Please enter Tags" name="tags" value="">
        </div>
        </div>
                    
                    </div>
                  </div>
                </div>
                <div class="xcrud-overlay"></div>
              </div>
            </div>
          </div>
          
        </div>
      </div>
      <div class="panel-footer">
        <input type="hidden" name="updatesettings" value="1" />
        <input type="hidden" name="updatefor" value="hotels" />
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
    </div>
  </form>
          
       
     
    </div>
  </div>
</div>
<!----edit modal--->
<script>
$(document).ready(function(){
if(window.location.hash != "") {
$('a[href="' + window.location.hash + '"]').click() } });
</script>
</div>
</div>
</div>
<?php include('footer.php'); ?>