<?php

class user extends CI_Controller

{
	public function __construct()

	{
		parent::__construct();

		$this->load->helper(array('form','url'));

		$this->load->library(array('session', 'form_validation', 'email'));

		$this->load->database();

		$this->load->model('user_model');

	}
	function index()

	{
		$this->register();
	}
    
	function register()

    {
		$this->form_validation->set_rules('fname', 'First Name', 'trim|required|alpha|min_length[3]|max_length[30]');
		$this->form_validation->set_rules('lname', 'Last Name', 'trim|required|alpha|min_length[3]|max_length[30]');
		$this->form_validation->set_rules('email', 'Email ID', 'trim|required|valid_email[vendors.email]');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|md5');
		$this->form_validation->set_rules('cpassword', 'Confirm Password', 'trim|required|matches[password]|md5');

		if ($this->form_validation->run() == FALSE)
        {   
		  $this->load->view('html/user_registration_view');
        }
		else
		{
		$data = array(
			'fname' => $this->input->post('fname'),
			'lname' => $this->input->post('lname'),
			'email' => $this->input->post('email'),
			'password' => $this->input->post('password')
		   ); 
	     
		   $email = $this->input->post('email');
		
		    $redtwo = "SELECT * FROM `user` where email = '$email'";        
            $querytwo = $this->db->query($redtwo);
		    $var = $querytwo->num_rows();
			if($var >0){
			$this->session->set_flashdata('msg','<div class="alert alert-success text-center">Email already exit!!! </div>');
	        
			}else{
			  $this->session->set_flashdata('msg','<div class="alert alert-success text-center">You are Successfully Registered!!! </div>');
			  $this->user_model->insertUser($data);
			} 
			redirect('user/register');    
		}	
	      
}
  public function vendor_change_password(){
    $uid = $this->session->userdata('uid');
    $pass=$this->input->post('oldpassword');
	$pass1 = md5($pass);
    $npass=$this->input->post('newpassword');
    $rpass=$this->input->post('cpassword');
	$this->load->view('html/vendor_changepassword');
	if($pass != '')
	{
       $redtwo = "SELECT * FROM `vendors` where password = '$pass1' and id = $uid";    
	   $querytwo = $this->db->query($redtwo);
	   $var = $querytwo->num_rows(); 
	   if($var == 0){
	
	$this->session->set_flashdata('msg','<div class="alert alert-success text-center">Current password does not match!!!</div>');
	redirect('user/vendor_change_password');
	
	}
	
    elseif($npass!=$rpass){
	
	    $this->session->set_flashdata('msg','<div class="alert alert-success text-center">Password does not match!!!</div>');
	    redirect('user/vendor_change_password');

    }else{
        $this->db->select('*');
        $this->db->from('vendors');
        $this->db->where('id',$uid);
        $this->db->where('password',$pass1);
        $query = $this->db->get();
		
        if($query->num_rows()==1){
	   
            $data = array(
                           'password' => md5($npass)
                        );

		
            $this->db->where('id', $uid);
            $this->db->update('vendors', $data); 
		    if ($data)
			{
			 $this->session->set_flashdata('msg','<div class="alert alert-success text-center">Password Update Successfully!!!</div>');
			 redirect('website/vendor_dashboard');
			}
			else
			{
			$this->session->set_flashdata('msg','<div class="alert alert-danger text-center">Oops! Error. Please try again later!!!</div>');
			redirect('user/vendor_change_password');
			}
            return "true";
        }else{
            return "false";
        }
     } 
  } 
}
	

	
  public function change_password(){
    $uid = $this->session->userdata('uid');
    $pass=$this->input->post('oldpassword');
	$pass1 = MD5($pass);
    $npass=$this->input->post('newpassword');
    $rpass=$this->input->post('cpassword');
	$this->load->view('html/user_changepassword');
	if($pass != '')
	{
       $redtwo = "SELECT * FROM `user` where password = '$pass1' and id = $uid";    
	   $querytwo = $this->db->query($redtwo);
	   $var = $querytwo->num_rows(); 
	   if($var == 0){
	
	$this->session->set_flashdata('msg','<div class="alert alert-success text-center">Old password does not match!!!</div>');
	redirect('user/change_password');
	
	}
	
    elseif($npass!=$rpass){
	
	    $this->session->set_flashdata('msg','<div class="alert alert-success text-center">Password does not match!!!</div>');
	    redirect('user/change_password');

    }else{
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where('id',$uid);
        $this->db->where('password',md5($pass));
        $query = $this->db->get();
		
        if($query->num_rows()==1){
		echo '=='. $query->password;
            $data = array(
                           'password' => md5($npass)
                        );
		
            $this->db->where('id', $uid);
            $this->db->update('user', $data); 
		    if ($data)
			{
			 $this->session->set_flashdata('msg','<div class="alert alert-success text-center">Password Update Successfully!!!</div>');
			 redirect('website/user_account');
			}
			else
			{
			$this->session->set_flashdata('msg','<div class="alert alert-danger text-center">Oops! Error. Please try again later!!!</div>');
			redirect('user/change_password');
			}
            return "true";
        }else{
            return "false";
        }
     } 
  } 
}
	

    
   public function membership()
   {
   $uid  = $this->session->userdata('uid');
   $user = $this->user_model->getUserById($uid);
   $this->load->view('html/profile_page',['user' => $user]);
   }
   

   
 public function editprofile($id){
  
   $data = array('fname'=>$this->input->post('fname'),
                'lname'=>$this->input->post('lname'),
			    'email'=>$this->input->post('email')
				 );
  $email = $this->input->post('email');
  
   $redtwo = "SELECT * FROM `user` where email = '$email' and id != $id";      
            $querytwo = $this->db->query($redtwo);
		    echo $var = $querytwo->num_rows();
			if($var >0){
			$this->session->set_flashdata('msg','<div class="alert alert-success text-center">Email already exit!!! </div>');
	        redirect('user/membership'); 
			}else{
			$this->session->set_flashdata('msg1','<div class="alert alert-success text-center">Email Id Update Successfully !!! </div>'); 
			 $this->db->where('id', $id);           
             $this->db->update('user', $data); 
			 redirect('website/user_account'); 
			
			} 
           
	     redirect('website/user_account'); 
} 

	public function view_vendor()
	   {
	   $uid  = $this->session->userdata('uid');
	   $vendor = $this->user_model->getVendorById($uid);
	   $this->load->view('html/vendor_profile',['vendor' => $vendor]);
	   }
	   
   public function editvendor($id){
  
   $data = array('fname'=>$this->input->post('fname'),
                'lname'=>$this->input->post('lname'),
			    'email'=>$this->input->post('email')
				 );
  $email = $this->input->post('email');
  
   $redtwo = "SELECT * FROM `vendors` where email = '$email' and id != $id";      
            $querytwo = $this->db->query($redtwo);
		    echo $var = $querytwo->num_rows();
			if($var >0){
			$this->session->set_flashdata('msg','<div class="alert alert-success text-center">Email already exit!!! </div>');
	        redirect('user/view_vendor'); 
			}else{
			$this->session->set_flashdata('msg1','<div class="alert alert-success text-center">Email Id Update Successfully !!! </div>'); 
			 $this->db->where('id', $id);           
             $this->db->update('user', $data); 
			 redirect('website/vendor_dashboard'); 
			
			} 
           
	     redirect('website/vendor_dashboard'); 
}    
	   
	   
	/*function verify($hash=NULL)

	{

		if ($this->user_model->verifyEmailID($hash))

		{

			$this->session->set_flashdata('verify_msg','<div class="alert alert-success text-center">Your Email Address is successfully verified! Please login to access your account!</div>');

			redirect('user/register');

		}

		else

		{

			$this->session->set_flashdata('verify_msg','<div class="alert alert-danger text-center">Sorry! There is error verifying your Email Address!</div>');

			redirect('user/register');

		}

	}*/

}