
<!DOCTYPE html>
<html lang="en">
    <head>
    
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Event Management</title>
        <link rel="stylesheet" type="text/css" href="https://fonts.google.com/specimen/Open+Sans" />
        <link  href="<?php echo base_url('frontend_assets/'); ?>css/comment.css" type="text/css" rel="stylesheet" />     
        <link href="<?php echo base_url('frontend_assets/'); ?>css/style.css" type="text/css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url('frontend_assets/'); ?>css/share-style.css">
        <link href="<?php echo base_url('frontend_assets/'); ?>css/animate.css" type="text/css" rel="stylesheet">
        <link href="<?php echo base_url('frontend_assets/'); ?>css/jquery.littlelightbox.css" rel="stylesheet" type="text/css">
        <!-- Zabuto Calendar -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('frontend_assets/'); ?>css/zabuto_calendar.min.css">
        <link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_url('frontend_assets/'); ?>favicons/apple-touch-icon.png">
        <link rel="icon" type="image/png" href="<?php echo base_url('frontend_assets/'); ?>favicons/favicon-32x32.png" sizes="32x32">
        <link rel="icon" type="image/png" href="<?php echo base_url('frontend_assets/'); ?>favicons/favicon-16x16.png" sizes="16x16">
        <link rel="manifest" href="<?php echo base_url('frontend_assets/'); ?>favicons/manifest.json">
        <link rel="mask-icon" href="<?php echo base_url('frontend_assets/'); ?>favicons/safari-pinned-tab.svg" color="#5bbad5">
        <meta name="theme-color" content="#ffffff">

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]--> 

    </head>
    <style type="text/css">
        #boxes-list2::-webkit-scrollbar-track {
            -webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
            border-radius: 3px;
            background-color: #fff;
        }
        #boxes-list2::-webkit-scrollbar {
            width: 10px;
            background-color: #fff !important;
        }
        #boxes-list2::-webkit-scrollbar-thumb {
            border-radius: 3px;
            -webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, .3)!important;
            background-color: #2aa09c !important;
        }
    </style>
    <body id="boxes-list2" style="max-height:450px; overflow-x:hidden;">

        <header>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 header-top-inn">
                <div class="container">
                    <div class="row">

                        <div class="col-lg-7 col-sm-7 col-xs-12">
                            <ul class="header-menu-inn"> 
                                <?php
                                $sitesetting = "SELECT * FROM `site_settings`";
                                $queryfour = $this->db->query($sitesetting);
                                $qfour = $queryfour->result();
                                ?>
                                <li><a href=""><i class="fa fa-envelope" aria-hidden="true" style=""></i>&nbsp;<?php foreach ($qfour as $hello) { ?>
                                            <?php echo $hello->email; ?></a></li>
                                    <li><a href="" class="select_1"><i class="fa fa-phone" aria-hidden="true" style=""></i><?php if($language2){ echo $language2[0]->phone;}else{?>&nbsp;phone<?php } ?><?php echo $hello->mobile; ?></a></li>
                                <?php } ?>
                            </ul>

                        </div>
						
                        <div class="col-lg-5 col-sm-5 col-xs-12 text-right for-mobile" style="padding-top: 4px;"> <a href="" style="color:#fff;"><?php if($language2){ echo $language2[0]->did_you_need;}else{?>Did you need help or want to work with us?<?php } ?></a> <div class="dropdown pull-right"> <div class="dropbtn"><?php if($lang_vallue == 'en'){
							?>English<?php }elseif($lang_vallue == 'ger'){?>
							Germany <?php }else{?>Language<?php }?><span><img src="http://clonescriptsmart.com/Emirates/img/united.png" align="left"></span></div>

  <div class="dropdown-content">
    <a href="<?php echo site_url('welcome/index/').'en';?>">English</a>
    <a href="<?php echo site_url('welcome/index/').'ger';?>">Germany</a>
   
  </div>

          
          </div></div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 header-bottom-inn">
                <div class="container">
                    <div class="row">
                         <div class="col-lg-3 col-sm-3 col-xs-10 logo"> <a href="<?php echo base_url('welcome/index');?>"><img src="<?php echo base_url('frontend_assets/'); ?>images/logo.png"></a>
                        </div>
                        <div class="col-md-9 col-sm-8 col-xs-6"> <a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a>
                        </div>
                        <div class="col-lg-9 col-sm-9 col-xs-12 text-center menu-none">
                            <div class="navbar navbar-default pull-right" style="background: none; box-shadow: none; border: none;">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar"> <i class="fa fa-bars" aria-hidden="true"></i>
            
          </button>
                                <div class="navbar-collapse collapse" id="navbar" style="padding: 0;">
                                    <ul class="nav navbar-nav  logo-m-inn">
                                        <li><a href="<?php echo site_url('website/searchby_city');?>"><?php if($language){ echo $language[0]->weddingfinder;}else{?>WEDDING FINDER<?php }?></a></li>
                                        <li><a href="<?php echo site_url('website/searchby_city');?>" target="_blank"><?php if($language){ echo $language[0]->marketplace;}else{?>MARKETPLACE<?php }?></a></li>
                                        
                                  <?php
                                     
									 
								  if (!empty($_SESSION['vendor_email']) && !empty($_SESSION['vendor_uid'])) { ?>
                                              <?php $username = $_SESSION['vendor_fname'];?>
											 <li><a href="<?php echo site_url('website/vendor_service_provider'); ?>" data-target="">Service provider</a></li>
											 <li><a href="<?php echo site_url('website/vendor_view'); ?>" data-target="">Manage provider</a></li>
                                             <li><a href="<?php echo site_url('website/vendor_review'); ?>" data-target="">vendor Reviews</a></li>
                                             <li><a href="<?php  echo site_url('Website/vendor_favourite'); ?>" data-target="">favourite</a></li>
											 <li><a href="<?php echo site_url('website/vendor_pakage'); ?>" data-target="">Package</a></li>
											 <li><a href="<?php echo site_url('website/vendor_manage_product'); ?>" data-target="">View vendor product</a></li>
                                             <li><a href="<?php echo site_url('website/vendor_dashboard'); ?>" data-target=""><?php  echo $username;?></a></li>
                                             <li><a href="<?php echo site_url('website/user_logout'); ?>">LOGOUT</a></li>
                                          <?php
										  }elseif(!empty($_SESSION['email']) && !empty($_SESSION['uid'])){?>
										  <?php $username = $_SESSION['fname'];?>
                                             <li><a href="<?php echo site_url('website/review'); ?>" data-target="">Users Reviews</a></li>
                                             <li><a href="<?php echo site_url('website/favourite'); ?>" data-target="">favourite</a></li>
                                             <li><a href="<?php echo site_url('website/user_account'); ?>" data-target=""><?php  echo $username;?></a></li>
                                             <li><a href="<?php echo site_url('website/user_logout'); ?>">LOGOUT</a></li>
										  <?php
										  }
										  elseif(!empty($_SESSION['emaill'])){
										  $vusername = $_SESSION['fname']; 
										  ?>
                                           <li><a href="<?php echo site_url('website/user_account'); ?>" data-target="">My Accounts</a></li>
                                          <li><a href="#" data-target=""><?php  echo $vusername;?></a></li>
                                           <li><a href="<?php echo site_url('website/user_logout'); ?>"><?php if($language){ echo $language[0]->logout;}else{?>LOGOUT<?php }?></a></li>
                                            
                                        <?php } else { ?>
                                            <li><a href="" data-toggle="modal" data-target="#login_form"><?php if($language){ echo $language[0]->login;}else{?>LOGIN<?php }?></a></li>
                                            <li><a href="" data-toggle="modal" data-target="#vendor_login"><?php if($language){ echo $language[0]->vendor;}else{?>VENDOR LOGIN<?php }?></a></li>
                                        <?php } ?>         
                                    </ul>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </header>
