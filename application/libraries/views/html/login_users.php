<?php

class login_users extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper(array('form', 'url', 'html'));
        $this->load->library(array('session', 'form_validation'));
        $this->load->database();
        $this->load->model('user_model');
		$this->load->model('vendor_model');
    }

    public function index() {
        $email = $this->input->post("email");
        $password = $this->input->post("password");
        $this->form_validation->set_rules("email", "email", "trim|required");
        $this->form_validation->set_rules("password", "Password", "trim|required");
        if ($this->form_validation->run() == FALSE) { // validation fail
            echo '<script type="text/javascript">alert("Please fill all fields");
        </script>';
            redirect('website/view_website');
            } else {     
            $uresult = $this->user_model->userlogin($email, $password);
            print_r($uresult);
			if (count($uresult) > 0) {   
		    $sess_data = array('login' => TRUE, 'email' => $uresult[0]->email,'fname' => $uresult[0]->fname, 'uid' => $uresult[0]->id);
			$this->session->set_userdata($sess_data);
		    redirect('website/user_account');
            } else {
                $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">Wrong Email-ID or Password!</div>');
            }
        }
    }

    public function send_email() {
        if (isset($_POST['send_email'])) {
            $name = $this->input->post('name');
            $email = $this->input->post('email');
            echo $name;
            exit;
        }
    }
	   public function vendor() {
			 $email = $this->input->post("email");
			 $password = $this->input->post("password");
			 $this->form_validation->set_rules("email", "email", "trim|required");
             $this->form_validation->set_rules("password", "Password", "trim|required");
		     if($this->form_validation->run() == FALSE) { // validation fail
            echo '<script type="text/javascript">alert("Please fill all fields");
        </script>';
            redirect('website/view_website');
            }else{      
		    $uresult = $this->vendor_model->vendorlogin($email, $password);
            //print_r($uresult);die;
		    if (count($uresult) > 0){  
		    $sess_data = array('login' => TRUE, 'email' => $uresult[0]->email,'fname' => $uresult[0]->fname, 'uid' => $uresult[0]->id);
		    //print_r($sess_data);
		    $this->session->set_userdata($sess_data);
		    redirect('website/vendor_dashboard');
		    }else{
		    $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">Wrong Email-ID or Password!</div>');
         }
      }
   }
}	
	
?>

