<?php include('header.php'); ?>
<div class="col-md-12 col-sm-12 col-xs-12 search-section">
    <div class="container">
        <div class="row">
            <div class="bc">
                <ul class="breadcrumb">
                    <li><a href="<?php echo base_url(); ?>website/view_website" property="" rel="">Home</a></li>
                    <li class="active"><span class="pinkcolor-new">Faq</span></li>
                </ul>
            </div>
        </div>
        <div class="row contact_wrap">
            <div class="col-sm-12">
                <h3 class="contact-h3">Frequently asked questions</h3>
                <div class="clearfix"></div>
                <div class="line-same"></div>
            </div>
            <div class="col-md-6">

                <div class="accord">
                    <div class="panel-group" id="accordion">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <div class="panel-title ">
                                    <span class="pull-left"><a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                        </a></span>
                                    <span style="padding-left: 15px;  vertical-align: middle;">Why use a Wedding Specialist?</span>
                                </div>
                            </div>
                            <div id="collapseOne" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <div class="panel-title ">
                                    <span class="pull-left"><a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                                        </a></span>
                                    <span style="padding-left: 15px;  vertical-align: middle;">Will you do weddings elsewhere?</span>
                                </div>
                            </div>
                            <div id="collapseTwo" class="panel-collapse collapse">
                                <div class="panel-body">
                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <div class="panel-title ">
                                    <span class="pull-left"><a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                                        </a></span>
                                    <span style="padding-left: 15px;  vertical-align: middle;">What does a specialist/coordinator do?</span>
                                </div>
                            </div>
                            <div id="collapseThree" class="panel-collapse collapse">
                                <div class="panel-body">
                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="accord">
                    <div class="panel-group" id="accordion">

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <div class="panel-title ">
                                    <span class="pull-left"><a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
                                        </a></span>
                                    <span style="padding-left: 15px;  vertical-align: middle;">Will you do weddings elsewhere?</span>
                                </div>
                            </div>
                            <div id="collapseFour" class="panel-collapse collapse">
                                <div class="panel-body">
                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <div class="panel-title ">
                                    <span class="pull-left"><a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseFive">
                                        </a></span>
                                    <span style="padding-left: 15px;  vertical-align: middle;">When do I get to meet with my wedding specialist?</span>
                                </div>
                            </div>
                            <div id="collapseFive" class="panel-collapse collapse">
                                <div class="panel-body">
                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <div class="panel-title ">
                                    <span class="pull-left"><a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseSix">
                                        </a></span>
                                    <span style="padding-left: 15px;  vertical-align: middle;">Do you do other events besides weddings?</span>
                                </div>
                            </div>
                            <div id="collapseSix" class="panel-collapse collapse">
                                <div class="panel-body">
                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
</div> 
<div class="modal fade login_form" id="free_quote" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content login_div">
            <div class="modal-header boder_none" >
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="z-index: 9999;
                        position: relative;
                        top: -29px;
                        left: 20px;
                        background: #000;
                        opacity: 1;
                        border-radius: 50%"><span class="glyphicon glyphicon-remove removed"aria-hidden="true" style="line-height:21px; font-size:16px !important; color: #f393bd;"></span></button>
            </div>
            <div class="modal-body">
                <div class=" karara">
                    <!-- Logo -->
                    <!-- Form Base -->
                    <div class="row">
                        <div class="form-base">
                            <header>
                                <h1 class="pop-login">FREE QUOTE</h1>
                            </header>
                            <!-- Form -->
                            <form action="#" method="post" id="free_quote">
                                <div class="form-group">
                                    <div class="col-md-6" style="padding-left:0px !important;">
                                        <div class="required_field">
                                            <label class="label_field required" for="contact_request_name">Name</label>
                                            <input class="text_field" id="contact_request_name" name="contact_request[name]" placeholder="Max Mustermann" type="text">
                                        </div>
                                    </div>
                                    <div class="col-md-6" style="padding-right:0px !important;">
                                        <div class="right">
                                            <label class="label_field" for="contact_request_company">
                                                Firm</label>
                                            <input class="text_field" id="contact_request_company" name="contact_request[company]" placeholder="Mustermann GmbH" type="text">
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-6" style="padding-left:0px !important;">
                                        <div class="required_field">
                                            <label class="label_field required" for="contact_request_name">E-Mail</label>
                                            <input class="text_field" id="contact_request_name" name="contact_request[name]" placeholder="info@aroos.com" type="text">
                                        </div>
                                    </div>
                                    <div class="col-md-6" style="padding-right:0px !important;">
                                        <div class="right">
                                            <label class="label_field required" for="contact_request_company">
                                                Telephone</label>
                                            <input class="text_field" id="contact_request_company" name="contact_request[company]" placeholder="0120234567" type="text">
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-6" style="padding-left:0px !important;">
                                        <div class="required_field">
                                            <label class="label_field required" for="contact_request_name">Occasion</label>
                                            <select class="form-control">
                                                <option>Meeting & Congress</option>
                                                <option>wedding</option>
                                                <option>Birthday</option>
                                                <option>Meeting & Congress</option><option>wedding</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6" style="padding-right:0px !important;">
                                        <div class="right">
                                            <label class="label_field" for="contact_request_company">Date / time</label>
                                            <input class="text_field" id="contact_request_company" name="contact_request[company]" placeholder="0120234567" type="text">
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-4 no-left-margin">
                                        <div class="required_field">
                                            <label class="label_field required" for="contact_request_number_of_persons">Guests</label>
                                            <input class="text_field" id="contact_request_number_of_persons" name="contact_request[number_of_persons]" placeholder="250" type="text">
                                        </div>
                                    </div>
                                    <div class="col-md-4 " style="padding:10px !important;">
                                        <select class="form-control text_field" style="    margin-top: 15px;">
                                            <option>Sitting</option>
                                            <option>Standing</option>
                                        </select>
                                    </div>
                                    <div class="col-md-4 budget_field no-left-margin">
                                        <div class="right">
                                            <label class="label_field" for="contact_request_event_budget">Budget</label>
                                            <input class="text_field" id="contact_request_event_budget" name="contact_request[event_budget]" placeholder="z.B. 5.000 €" type="text">
                                        </div>
                                    </div>
                                </div>  
                                <div class="form-group">
                                    <div class="col-md-6  budget_col" style="padding: 0 !important;">
                                        <input checked="checked" class="radio" id="contact_request_budget_type_event" name="contact_request[budget_type]" type="radio" value="event" style="float: left;
                                               margin-right: 10px;"><label class="budget_type_event" for="contact_request_budget_type_event"><span></span>Event Gesamt</label>
                                    </div>
                                    <div class="col-md-6">

                                        <input class="radio" id="contact_request_budget_type_location" name="contact_request[budget_type]" type="radio" value="location" style="float: left;
                                               margin-right: 10px;"><label class="budget_type_location" for="contact_request_budget_type_location"><span></span>Locationmiete</label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <label class="label_field" for="contact_request_message">Details</label>
                                        <textarea class="textarea_field" id="contact_request_message" name="contact_request[message]" placeholder="z.B. Ablauf, Anforderungen, besondere Wünsche"></textarea>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="">
                                    <div class="col-md-9">
                                        <div class="more_suggestions">
                                            <input class="checkbox" id="contact_request_more_suggestions" name="contact_request[more_suggestions]" type="checkbox" value="1" style="float: left;
                                                   margin-right: 5px;">
                                            <label for="contact_request_more_suggestions" id="c1"><span></span>Beat me more appropriate provider before
                                            </label></div>
                                        <div class="newsletter">
                                            <input checked="checked" class="checkbox" id="contact_request_newsletter" name="contact_request[newsletter]" type="checkbox" value="1" style="float: left;
                                                   margin-right: 5px;">
                                            <label for="contact_request_newsletter" id="c2"><span></span>
                                                I want to receive exciting event news
                                            </label></div>
                                    </div>
                                    <div class="col-md-3">
                                        <button class="btn primary pull-right custom-color-back">Send</button>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="form-group">
                                    <div class="trust_footer"><div class="inner_trust">
                                            <div class="col-md-4"><span class="icon icon-ok"></span><span class="trust_text">
                                                    Known from the manufacturer
                                                </span>
                                            </div>
                                            <div class="col-md-4"><span class="icon icon-ok"></span><span class="trust_text">
                                                    100% free &amp; not binding
                                                </span>
                                            </div>
                                            <div class="col-md-4"><span class="icon icon-ok"></span><span class="trust_text">

                                                    Personal attention from Event Experts
                                                </span>
                                            </div>
                                        </div></div>
                                    <div class="clearfix"></div>
                                </div>    
                            </form>
                        </div>
                    </div>
                    <!-- Sign Up / Login Switch -->
                    <!--<div class="row">
                      <div class="account-switch text-center">
                        <p>Don't have an account? <a href="" class="custom-color"  data-toggle="modal" data-target="#Register_form">Sign up</a></p>
                      </div>
                    </div>-->
                </div>
            </div>
        </div>
    </div>
</div>
<?php include('footer.php'); ?>

<script src="http://codeorigin.jquery.com/jquery-1.10.2.min.js"></script>
<script src="<?php echo base_url('frontend_assets/'); ?>js/bootstrap.min.js"></script>
<script>
    $('#myCarousel').carousel({
        interval: 4000
    });

    var clickEvent = false;
    $('#myCarousel').on('click', '.nav a', function () {
        clickEvent = true;
        $('.nav li').removeClass('active');
        $(this).parent().addClass('active');
    }).on('slid.bs.carousel', function (e) {
        if (!clickEvent) {
            var count = $('.nav').children().length - 1;
            var current = $('.nav li.active');
            current.removeClass('active').next().addClass('active');
            var id = parseInt(current.data('slide-to'));
            if (count == id) {
                $('.nav li').first().addClass('active');
            }
        }
        clickEvent = false;
    });
</script>


<script src="<?php echo base_url('frontend_assets/'); ?>js/zabuto_calendar.min.js"></script>
<script type="application/javascript">
    $(document).ready(function () {
    $("#date-popover").popover({html: true, trigger: "manual"});
    $("#date-popover").hide();
    $("#date-popover").click(function (e) {
    $(this).hide();
    });

    $("#my-calendar").zabuto_calendar({
    action: function () {
    return myDateFunction(this.id, false);
    },
    action_nav: function () {
    return myNavFunction(this.id);
    },
    ajax: {
    url: "show_data.php?action=1",
    modal: true
    },
    legend: [
    {type: "text", label: "Special event", badge: "00"},
    {type: "block", label: "Regular event"}
    ]
    });
    });

    function myDateFunction(id, fromModal) {
    $("#date-popover").hide();
    if (fromModal) {
    $("#" + id + "_modal").modal("hide");
    }
    var date = $("#" + id).data("date");
    var hasEvent = $("#" + id).data("hasEvent");
    if (hasEvent && !fromModal) {
    return false;
    }
    $("#date-popover-content").html('You clicked on date ' + date);
    $("#date-popover").show();
    return true;
    }

    function myNavFunction(id) {
    $("#date-popover").hide();
    var nav = $("#" + id).data("navigation");
    var to = $("#" + id).data("to");
    console.log('nav ' + nav + ' to: ' + to.month + '/' + to.year);
    }
</script>


<script src="<?php echo base_url('frontend_assets/'); ?>js/sharemelody.dev.js" type="text/javascript"></script>
<script src="<?php echo base_url('frontend_assets/'); ?>js/share2melody.dev.js" type="text/javascript"></script>

<style type="text/css">
    .panel-heading .accordion-toggle:after {
        font-family: 'FontAwesome';
        content: "\f067";
        float: right;
        color: #f388a3;
        background: #db5677;
        padding:3px 8px;
    }
    .panel-heading .accordion-toggle.collapsed:after {
        /* symbol for "collapsed" panels */
        content: "\f068";    /* adjust as needed, taken from bootstrap.css */
    }

</style>