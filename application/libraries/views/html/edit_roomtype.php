<?php include('headerandsidebar.php'); ?>
<div class="row">
  <div class="container" id="content">
    <style type="text/css">
    .modal .modal-body {
    max-height: 450px;
    overflow-y: auto;
    }
    </style>

    <?php foreach($edit_data as $data);  { ?>
    <h3 class="margin-top-0">Edit Room Type</h3>
  <form action="<?php echo site_url('welcome/update_roomtype/'. $data['id'].''); ?>" method="POST" enctype="multipart/form-data">
      <div class="panel panel-default">
        <ul class="nav nav-tabs nav-justified" role="tablist">
           <li class="active"><a href="#GENERAL" data-toggle="tab">Room Type</a></li>
          <li class=""><a href="#HOTELS_TYPES" data-toggle="tab">Facilities</a></li>   
        </ul>

        <div class="panel-body"><br>
          <div class="tab-content form-horizontal">
            <div class="tab-pane wow fadeIn animated active in" id="GENERAL">
              <div class="clearfix"></div>

            <!-- <div class="row form-group">
                <label  class="col-md-2 control-label text-left">Icon Class</label>
                <div class="col-md-4">
                  <input type="text" name="page_icon" class="form-control" placeholder="Select icon" value="fa fa-building" >
                </div>
              </div> -->
              <div class="row form-group">
                <label  class="col-md-2 control-label text-left">Select Provider Name:</label>
                 <div class="col-md-4">
                  <div>                   
                  </div>                 
                    <select name="providerid" class="form-control" required>
                      <option value="">--SELECT PROVIDER NAME--</option>
                      <?php foreach ($view_providerdetails as $nameprovider) { ?>
                      <option value="<?php echo $nameprovider['id']; ?>" <?php if($data['provider_id'] == $nameprovider['id']) {echo 'selected="selected"';} ?>>
                      <?php echo $nameprovider['provider_name']; ?></option>
                      <?php } ?>
                    </select>
                  </div>

                  <label class="col-md-2 control-label text-left">Select Category:</label>
                    <div class="col-md-3">
                        <select name="catname" class="form-control"required>
                          <option value="">--SELECT CATGORY--</option>
                          <?php foreach ($view_catone as $nameprovider) { ?>
                          <option value="<?php echo $nameprovider['id']; ?>" <?php if($data['provider_category'] == $nameprovider['id']) {echo 'selected="selected"';} ?>>
                          <?php echo $nameprovider['catonename']; ?></option>
                          <?php } ?>
                        </select>
                    </div>
                  </div>

              <div class="row form-group">
                <label  class="col-md-2 control-label text-left">Status:</label>
                <div class="col-md-4">
                    <select  name="status" class="form-control" required>
                      <option value="1" <?php if($data['status'] == 1) {echo 'selected="selected"';} ?>>Enable</option>
                      <option value="0" <?php if($data['status'] == 0) {echo 'selected="selected"';} ?>>Disable</option>
                    </select>
                </div>
              </div>
              <hr>

              <div class="row form-group">
                <label  class="col-md-2 control-label text-left">Room Type (Name):</label>
                <div class="col-md-4">
                  <input class="form-control" type="text" placeholder="" name="roomtype"  value="<?php echo $data['room_type']; ?>">
                </div>             
              </div>
             
              <div class="row form-group">
                <label  class="col-md-2 control-label text-left">Room Description:</label>
                <div class="col-md-4">
                  <input class="form-control" type="text" placeholder="" name="desc"  value="<?php echo $data['room_desc']; ?>" required>
                </div>
              </div>
              
              <div class="clearfix"></div>
              <div class="row form-group">
              <label  class="col-md-2 control-label text-left">Wakeup Calls:</label>
                <div class="col-md-4">
                  <select class="form-control" name="wakeupcalls" required>
                    <option value="" label="">--SELECT--</option>
                    <option value="1" <?php if($data['wakeup_calls'] == '1') {echo 'selected="selected"';}  ?>>Yes</option>
                    <option value="0" <?php if($data['wakeup_calls'] == '0') {echo 'selected="selected"';}  ?>>No</option>
                  </select>
                </div>
              </div>

              <div class="clearfix"></div>

              <div class="row form-group">
              <label  class="col-md-2 control-label text-left">Bathrobes:</label>
                <div class="col-md-4">
                  <select class="form-control" name="bathrobes" required>
                    <option value="" label="" >--SELECT--</option>
                    <option value="1" <?php if($data['bathrobes'] == '1') {echo 'selected="selected"';}  ?>>Yes</option>
                    <option value="0" <?php if($data['bathrobes'] == '0') {echo 'selected="selected"';}  ?>>No</option>
                  </select>
                </div>
              </div>

              <div class="clearfix"></div>

              <div class="row form-group">
              <label  class="col-md-2 control-label text-left">LCD TVs:</label>
                <div class="col-md-4">
                  <select class="form-control" name="lcd" required>
                    <option value="" label="" >--SELECT--</option>
                    <option value="1" <?php if($data['lcd_tv'] == '1') {echo 'selected="selected"';}  ?>>Yes</option>
                    <option value="0" <?php if($data['lcd_tv'] == '0') {echo 'selected="selected"';}  ?>>No</option>
                  </select>
                </div>
              </div>
              <div class="clearfix"></div>

              <div class="row form-group">
              <label  class="col-md-2 control-label text-left">Inroom Childcare:</label>
                <div class="col-md-4">
                  <select class="form-control" name="inroomchildcare" required>
                    <option value="" label="" >--SELECT--</option>
                    <option value="1" <?php if($data['inroom_childcare'] == '1') {echo 'selected="selected"';}  ?>>Yes</option>
                    <option value="0" <?php if($data['inroom_childcare'] == '0') {echo 'selected="selected"';}  ?>>No</option>
                  </select>
                </div>
              </div>

              <div class="clearfix"></div>

              <div class="row form-group">
              <label  class="col-md-2 control-label text-left">Daily housekeeping:</label>
                <div class="col-md-4">
                  <select class="form-control" name="housekeeping" required>
                    <option value="" label="" >--SELECT--</option>
                    <option value="1" <?php if($data['housekeeping'] == '1') {echo 'selected="selected"';}  ?> >Yes</option>
                    <option value="0" <?php if($data['housekeeping'] == '0') {echo 'selected="selected"';}  ?> >>No</option>
                  </select>
                </div>
              </div>

              <hr>
              <hr>

        <div class="col-md-11">
        <h4 class="text-danger">Default Check-Time</h4>
        <hr>
  <table width="100%" border="0">
  <tr>
    <td ><strong style="padding:5px;">Days</strong></td>
    <td><strong style="padding:5px;">Opening Hours Start Timing</strong></td>
    <td><strong style="padding:5px;">Opening Hours End Timing</strong></td>
    <td ><strong style="padding:5px;">Closing Hours Start Timing</strong></td>
    <td><strong style="padding:5px;">Closing Hours End Timing</strong></td>
  </tr>
  <tr><td colspan="5"><hr></td></tr>
  <tr>
  <td><input type="checkbox" name="delivery_mon_selected" id="delivery_mon_selected" value="Monday" />
  &nbsp;&nbsp;Monday</td>
  <td><input type="text" name="delivery_mon_openFirst" id="delivery_mon_openFirst" value="" style="width:50px; "/>
  <input type="text" name="delivery_mon_openFirstMin" id="delivery_mon_openFirstMin" value="" style="width:50px;"/>

  <select name="delivery_mon_openFirstMinAM" placeholder='AM OR PM' id="delivery_mon_openFirstMinAM" style="width:70px;">
    <option value="AM" >AM</option>
    <option value="PM" >PM</option>    
  </select>  
</td>
<td><input type="text" name="delivery_mon_closeFirst" id="delivery_mon_closeFirst" value="" style="width:50px; " />
<input type="text" name="delivery_mon_closeFirstdMin" id="delivery_mon_closeFirstdMin" value="" style="width:50px;" />

<select name="delivery_mon_closeFirstMinPM" placeholder='AM OR PM' id="delivery_mon_closeFirstMinPM" style="width:70px;">
  <option value="PM" >PM</option>
  <option value="AM" >AM</option>
</select>

</td>
<td> <input type="text" name="delivery_mon_openSecond" id="delivery_mon_openSecond" value="" style="width:50px; " />
<input type="text" name="delivery_mon_openSecondMin" id="delivery_mon_openSecondMin" value="" style="width:50px;" />

<select name="delivery_mon_openSecondMinAM" placeholder='AM OR PM' id="delivery_mon_openSecondMinAM" style="width:70px;">
<option value="AM" >AM</option>
<option value="PM" selected="selected" >PM</option>
</select>
</td>

<td><input type="text" name="delivery_mon_closeSecond" id="delivery_mon_closeSecond" value="" style="width:50px; " />
<input type="text" name="delivery_mon_closeSecondMin" id="delivery_mon_closeSecondMin" value="" style="width:50px;" />

<select name="delivery_mon_closeSecondMinPM" placeholder='AM OR PM' id="delivery_mon_closeSecondMinPM" style="width:70px;">
<option value="PM" >PM</option>
<option value="AM" >AM</option>
</select>
</td>
</tr>

<tr>
<td><input type="checkbox" name="delivery_tue_selected" id="delivery_tue_selected" checked="checked"   value="Tuesday" />
&nbsp;&nbsp;Tuesday</td>
<td > <input type="text" name="delivery_tue_openFirst" id="delivery_tue_openFirst" value="11" style="width:50px; "/>
<input type="text" name="delivery_tue_openFirstMin" id="delivery_tue_openFirstMin" value="00" style="width:50px;"/>

<select name="delivery_tue_openFirstMinAM" placeholder='AM OR PM' id="delivery_tue_openFirstMinAM" style="width:70px;">
<option value="AM" selected="selected">AM</option>
<option value="PM" >PM</option>
</select>

</td>
<td><input type="text" name="delivery_tue_closeFirst" id="delivery_tue_closeFirst" value="02" style="width:50px; " />
<input type="text" name="delivery_tue_closeFirstdMin" id="delivery_tue_closeFirstdMin" value="00" style="width:50px;" />

<select name="delivery_tue_closeFirstMinPM" placeholder='AM OR PM' id="delivery_tue_closeFirstMinPM" style="width:70px;">
<option value="PM" selected="selected" >PM</option>
<option value="AM">AM</option>
</select>
</td>
<td> <input type="text" name="delivery_tue_openSecond" id="delivery_tue_openSecond" value="04" style="width:50px; " />
<input type="text" name="delivery_tue_openSecondMin" id="delivery_tue_openSecondMin" value="00" style="width:50px;" />
<select name="delivery_tue_openSecondMinAM" placeholder='AM OR PM' id="delivery_tue_openSecondMinAM" style="width:70px;">
<option value="AM">AM</option>
<option value="PM" selected="selected">PM</option>
</select>
</td>

<td>
<input type="text" name="delivery_tue_closeSecond" id="delivery_tue_closeSecond" 
value="11" style="width:50px; " />
<input type="text" name="delivery_tue_closeSecondMin" id="delivery_tue_closeSecondMin" value="30" style="width:50px;" />

<select name="delivery_tue_closeSecondMinPM" placeholder='AM OR PM' id="delivery_tue_closeSecondMinPM" style="width:70px;">
<option value="PM" selected="selected" >PM</option>
<option value="AM" >AM</option>
</select>
</td>

</tr>
<tr>
<td><input type="checkbox" name="delivery_wed_selected" id="delivery_wed_selected"   checked="checked"   value="Wednesday" />
&nbsp;&nbsp;Wednesday</td>

<td> <input type="text" name="delivery_wed_openFirst" id="delivery_wed_openFirst" value="11" style="width:50px; " />
<input type="text" name="delivery_wed_openFirstMin" id="delivery_wed_openFirstMin" value="00" style="width:50px;" />

<select name="delivery_wed_openFirstMinAM" placeholder='AM OR PM' id="delivery_wed_openFirstMinAM" style="width:70px;">
<option value="AM" selected="selected" >AM</option>
<option value="PM" >PM</option>

</select>

</td>
<td><input type="text" name="delivery_wed_closeFirst" id="delivery_wed_closeFirst" value="02" style="width:50px; "/>
<input type="text" name="delivery_wed_closeFirstdMin" id="delivery_wed_closeFirstdMin" value="00" style="width:50px;" />

<select name="delivery_wed_closeFirstMinPM" placeholder='AM OR PM' id="delivery_wed_closeFirstMinPM" style="width:70px;">
<option value="PM"  selected="selected" >PM</option>
<option value="AM" >AM</option>
</select>
</td>

<td > <input type="text" name="delivery_wed_openSecond" id="delivery_wed_openSecond" value="04" style="width:50px; " />
<input type="text" name="delivery_wed_openSecondMin" id="delivery_wed_openSecondMin" value="00" style="width:50px;" />


<select name="delivery_wed_openSecondMinAM" placeholder='AM OR PM' id="delivery_wed_openSecondMinAM" style="width:70px;">
<option value="AM" >AM</option>
<option value="PM" selected="selected" >PM</option>

</select>

</td>
<td><input type="text" name="delivery_wed_closeSecond" id="delivery_wed_closeSecond" value="11" style="width:50px; " />
<input type="text" name="delivery_wed_closeSecondMin" id="delivery_wed_closeSecondMin" value="30" style="width:50px;" />

<select name="delivery_wed_closeSecondMinPM" placeholder='AM OR PM' id="delivery_wed_closeSecondMinPM" style="width:70px;">
<option value="PM"  selected="selected" >PM</option>
<option value="AM" >AM</option>
</select>
</td>


</tr>

<tr >
<td><input type="checkbox" name="delivery_thu_selected" id="delivery_thu_selected"   checked="checked"   value="Thursday" />
&nbsp;&nbsp;Thursday</td>
<td > <input type="text" name="delivery_thu_openFirst" id="delivery_thu_openFirst" value="11" style="width:50px; " />
<input type="text" name="delivery_thu_openFirstMin" id="delivery_thu_openFirstMin" value="00" style="width:50px;" />


<select name="delivery_thu_openFirstMinAM" placeholder='AM OR PM' id="delivery_thu_openFirstMinAM" style="width:70px;">
<option value="AM"  selected="selected" >AM</option>
<option value="PM" >PM</option>

</select>

</td>
<td><input type="text" name="delivery_thu_closeFirst" id="delivery_thu_closeFirst" value="02" style="width:50px; " />
<input type="text" name="delivery_thu_closeFirstdMin" id="delivery_thu_closeFirstdMin" value="00" style="width:50px;" />

<select name="delivery_thu_closeFirstMinPM" placeholder='AM OR PM' id="delivery_thu_closeFirstMinPM" style="width:70px;">
<option value="PM"  selected="selected" >PM</option>
<option value="AM" >AM</option>
</select>
</td>

<td><input type="text" name="delivery_thu_openSecond" id="delivery_thu_openSecond" value="04" style="width:50px; " />
<input type="text" name="delivery_thu_openSecondMin" id="delivery_thu_openSecondMin" value="00" style="width:50px;" />

<select name="delivery_thu_openSecondMinAM" placeholder='AM OR PM' id="delivery_thu_openSecondMinAM" style="width:70px;">
<option value="AM" >AM</option>
<option value="PM" selected="selected" >PM</option>
</select>
</td>

<td><input type="text" name="delivery_thu_closeSecond" id="delivery_thu_closeSecond" value="11" style="width:50px; " />
<input type="text" name="delivery_thu_closeSecondMin" id="delivery_thu_closeSecondMin" value="30" style="width:50px;" />

<select name="delivery_thu_closeSecondMinPM" placeholder='AM OR PM' id="delivery_thu_closeSecondMinPM" style="width:70px;">
<option value="PM"  selected="selected" >PM</option>
<option value="AM" >AM</option>
</select>
</td>
</tr>
<tr >
<td><input type="checkbox" name="delivery_fri_selected" id="delivery_fri_selected"   checked="checked"   value="Friday" />
&nbsp;&nbsp;Friday</td>
<td > <input type="text" name="delivery_fri_openFirst" id="delivery_fri_openFirst" value="11" style="width:50px; " />
<input type="text" name="delivery_fri_openFirstMin" id="delivery_fri_openFirstMin" value="00" style="width:50px;" />


<select name="delivery_fri_openFirstMinAM" placeholder='AM OR PM' id="delivery_fri_openFirstMinAM" style="width:70px;">
<option value="AM"  selected="selected" >AM</option>
<option value="PM" >PM</option>

</select>

</td>
<td><input type="text" name="delivery_fri_closeFirst" id="delivery_fri_closeFirst" value="02" style="width:50px; " />
<input type="text" name="delivery_fri_closeFirstdMin" id="delivery_fri_closeFirstdMin" value="00" style="width:50px;" />

<select name="delivery_fri_closeFirstMinPM" placeholder='AM OR PM' id="delivery_fri_closeFirstMinPM" style="width:70px;">
<option value="PM"  selected="selected" >PM</option>
<option value="AM" >AM</option>
</select>
</td>


<td><input type="text" name="delivery_fri_openSecond" id="delivery_fri_openSecond" value="04" style="width:50px; "/>
<input type="text" name="delivery_fri_openSecondMin" id="delivery_fri_openSecondMin" value="00" style="width:50px;"/>


<select name="delivery_fri_openSecondMinAM" placeholder='AM OR PM' id="delivery_fri_openSecondMinAM" 
style="width:70px;">
<option value="AM" >AM</option>
<option value="PM" selected="selected">PM</option>
</select>

</td>
<td><input type="text" name="delivery_fri_closeSecond" id="delivery_fri_closeSecond" value="11" style="width:50px; "/>
<input type="text" name="delivery_fri_closeSecondMin" id="delivery_fri_closeSecondMin" value="30" style="width:50px;" />

<select name="delivery_fri_closeSecondMinPM" placeholder='AM OR PM' id="delivery_fri_closeSecondMinPM" style="width:70px;">
<option value="PM"  selected="selected" >PM</option>
<option value="AM" >AM</option>
</select>
</td>


</tr><tr><td><input type="checkbox" name="delivery_sat_selected" id="delivery_sat_selected"   checked="checked"   value="Saturday" />
&nbsp;&nbsp;Saturday</td>
<td > <input type="text" name="delivery_sat_openFirst" id="delivery_sat_openFirst" value="11" style="width:50px; "/>
<input type="text" name="delivery_sat_openFirstMin" id="delivery_sat_openFirstMin" value="00" style="width:50px;"/>

<select name="delivery_sat_openFirstMinAM" placeholder='AM OR PM' id="delivery_sat_openFirstMinAM" style="width:70px;">
<option value="AM"  selected="selected" >AM</option>
<option value="PM" >PM</option>

</select>

</td>
<td><input type="text" name="delivery_sat_closeFirst" id="delivery_sat_closeFirst" value="02" style="width:50px; " />
<input type="text" name="delivery_sat_closeFirstdMin" id="delivery_sat_closeFirstdMin" value="00" style="width:50px;" />

<select name="delivery_sat_closeFirstMinPM" placeholder='AM OR PM' id="delivery_sat_closeFirstMinPM" style="width:70px;">
<option value="PM"  selected="selected" >PM</option>
<option value="AM" >AM</option>
</select>
</td>

<td> <input type="text" name="delivery_sat_openSecond" id="delivery_sat_openSecond" value="04" style="width:50px; " />
<input type="text" name="delivery_sat_openSecondMin" id="delivery_sat_openSecondMin" value="00" style="width:50px;" />


<select name="delivery_sat_openSecondMinAM" placeholder='AM OR PM' id="delivery_sat_openSecondMinAM" 
style="width:70px;">
<option value="AM">AM</option>
<option value="PM" selected="selected" >PM</option>
</select>

</td>
<td><input type="text" name="delivery_sat_closeSecond" id="delivery_sat_closeSecond" value="11" style="width:50px; " />
<input type="text" name="delivery_sat_closeSecondMin" id="delivery_sat_closeSecondMin" value="30" style="width:50px;" />

<select name="delivery_sat_closeSecondMinPM" placeholder='AM OR PM' id="delivery_sat_closeSecondMinPM" style="width:70px;">
<option value="PM" selected="selected" >PM</option>
<option value="AM" >AM</option>
</select>
</td>
</tr>

<tr>
<td><input type="checkbox" name="delivery_sun_selected" id="delivery_sun_selected"   checked="checked"   value="Sunday" />
&nbsp;&nbsp;Sunday</td>
<td > <input type="text" name="delivery_sun_openFirst" id="delivery_sun_openFirst" value="11" style="width:50px; " />
<input type="text" name="delivery_sun_openFirstMin" id="delivery_sun_openFirstMin" value="00" style="width:50px;" />

<select name="delivery_sun_openFirstMinAM" placeholder='AM OR PM' id="delivery_sun_openFirstMinAM" style="width:70px;">
<option value="AM"  selected="selected" >AM</option>
<option value="PM" >PM</option>
</select>

</td>
<td><input type="text" name="delivery_sun_closeFirst" id="delivery_sun_closeFirst" value="02" style="width:50px; " />
<input type="text" name="delivery_sun_closeFirstdMin" id="delivery_sun_closeFirstdMin" value="00" style="width:50px;" />

<select name="delivery_sun_closeFirstMinPM" placeholder='AM OR PM' id="delivery_sun_closeFirstMinPM" style="width:70px;">
<option value="PM"  selected="selected" >PM</option>
<option value="AM" >AM</option>
</select>
</td>

<td > <input type="text" name="delivery_sun_openSecond" id="delivery_sun_openSecond" value="04" style="width:50px; " />
<input type="text" name="delivery_sun_openSecondMin" id="delivery_sun_openSecondMin" value="00" style="width:50px;" />

<select name="delivery_sun_openSecondMinAM" placeholder='AM OR PM' id="delivery_sun_openSecondMinAM" style="width:70px;">
<option value="AM">AM</option>
<option value="PM" selected="selected">PM</option>
</select>
</td>

<td><input type="text" name="delivery_sun_closeSecond" id="delivery_sun_closeSecond" value="11" style="width:50px; " />
<input type="text" name="delivery_sun_closeSecondMin" id="delivery_sun_closeSecondMin" value="30" style="width:50px;" />

<select name="delivery_sun_closeSecondMinPM" placeholder='AM OR PM' id="delivery_sun_closeSecondMinPM" style="width:70px;">
<option value="PM"  selected="selected" >PM</option>
<option value="AM" >AM</option>
</select>
</td>
</tr>
</table>
        <!-- <label  class="col-md-2 control-label text-left">Check In</label>
         <div class="col-md-2">
          <input class="form-control timepicker" type="text" placeholder="" name="checkin" value="02:00 AM" data-format="hh:mm A">
                </div>
                <label  class="col-md-2 control-label text-left">Check Out</label>
                <div class="col-md-2">
                  <input class="form-control timepicker" type="text" placeholder="" name="checkout"  value="02:00 PM" data-format="hh:mm A">
                </div> -->
              </div>
              <!-- <h4 class="text-danger">SEO</h4>
              <div class="row form-group">
                <label  class="col-md-2 control-label text-left">Meta Keywords</label>
                <div class="col-md-4">
                  <input class="form-control" type="text" placeholder="" name="keywords" value="">
                </div>
                <label  class="col-md-2 control-label text-left">Meta Description</label>
                <div class="col-md-4">
                  <input class="form-control" type="text" placeholder="" name="description"  value="">
                </div>
              </div> -->
            </div>
           
            <div class="tab-pane wow fadeIn animated in" id="HOTELS_TYPES">
             <div class="xcrud">
                <div class="xcrud-container">
                  <div class="xcrud-ajax">
                    <input type="hidden" class="xcrud-data" name="key" value="" />
                    <input type="hidden" class="xcrud-data" name="orderby" value="" />
                    <input type="hidden" class="xcrud-data" name="order" value="desc" />
                    <input type="hidden" class="xcrud-data" name="start" value="0" />
                    <input type="hidden" class="xcrud-data" name="limit" value="10" />
                    <input type="hidden" class="xcrud-data" name="instance" value="" />
                    <input type="hidden" class="xcrud-data" name="task" value="list" />
                    <div class="xcrud-top-actions">
                      <!--   <div class="btn-group pull-right"><a href="" class="btn btn-danger"><i class="glyphicon glyphicon-remove"></i> Delete Selected</a><a href="javascript:;" data-task="print" class="btn btn-default xcrud-in-new-window xcrud-action"><i class="glyphicon glyphicon-print"></i> Print</a><a href="javascript:;" data-task="csv" class="btn btn-default xcrud-in-new-window xcrud-action"><i class="glyphicon glyphicon-file"></i>Export into CSV</a></div> -->
                    <div class="clearfix"></div>
                      <label  class="control-label text-center"><h2><b>Facilities:</b></h2></label>
                    </div>

              <div class="xcrud-list-container">
              <div class="row form-group">
              <label  class="col-md-2 control-label text-left">Pets Allowed:</label>
                <div class="col-md-4">
                  <select class="form-control" name="pets" required>
                    <option value="" label="" >--SELECT--</option>
                    <option value="1" <?php if($data['pets'] == '1') {echo 'selected="selected"';}  ?>>Yes</option>
                    <option value="0" <?php if($data['pets'] == '0') {echo 'selected="selected"';}  ?>>No</option>
                  </select>
                </div>
              </div>

              <div class="clearfix"></div>
              <div class="row form-group">
              <label  class="col-md-2 control-label text-left">Elevator:</label>
                <div class="col-md-4">
                  <select class="form-control" name="elevator" required>
                    <option value="" label="" >--SELECT--</option>
                    <option value="1" <?php if($data['elevator'] == '1') {echo 'selected="selected"';}  ?> >Yes</option>
                    <option value="0" <?php if($data['elevator'] == '0') {echo 'selected="selected"';}  ?> >No</option>
                  </select>
                </div>
              </div>

              <div class="clearfix"></div>
              <div class="row form-group">
              <label  class="col-md-2 control-label text-left">Card Accepted:</label>
                <div class="col-md-4">
                  <select class="form-control" name="card" required>
                    <option value="" label="" >--SELECT--</option>
                    <option value="1" <?php if($data['card'] == '1') {echo 'selected="selected"';}  ?>>Yes</option>
                    <option value="0" <?php if($data['card'] == '0') {echo 'selected="selected"';}  ?>>No</option>
                  </select>
                </div>
              </div>

              <div class="clearfix"></div>
              <div class="row form-group">
              <label  class="col-md-2 control-label text-left">Air Conditioned:</label>
                <div class="col-md-4">
                  <select class="form-control" name="ac" required>
                    <option value="" label="" >--SELECT--</option>
                    <option value="1" <?php if($data['card'] == '1') {echo 'selected="selected"';}  ?>>Yes</option>
                    <option value="0" <?php if($data['card'] == '0') {echo 'selected="selected"';}  ?>>No</option>
                  </select>
                </div>
              </div>

              <div class="clearfix"></div>
              <div class="row form-group">
              <label  class="col-md-2 control-label text-left">Spa:</label>
                <div class="col-md-4">
                  <select class="form-control" name="spa" required>
                    <option value="" label="" >--SELECT--</option>
                    <option value="1" <?php if($data['spa'] == '1') {echo 'selected="selected"';}  ?>>Yes</option>
                  <option value="0" <?php if($data['spa'] == '0') {echo 'selected="selected"';}  ?>>No</option>
                  </select>
                </div>
              </div>

              <div class="clearfix"></div>
              <div class="row form-group">
              <label  class="col-md-2 control-label text-left">Fitness:</label>
                <div class="col-md-4">
                  <select class="form-control" name="fitness" required>
                    <option value="" label="" >--SELECT--</option>
                  <option value="1" <?php if($data['gym'] == '1') {echo 'selected="selected"';}  ?>>Yes</option>
                  <option value="0" <?php if($data['gym'] == '0') {echo 'selected="selected"';}  ?>>No</option>
                  </select>
                </div>
              </div>

                <div class="clearfix"></div>
                  <div class="row form-group">
                    <label  class="col-md-2 control-label text-left">Inside Parking:</label>
                      <div class="col-md-4">
                        <select class="form-control" name="insideparking" required>
                          <option value="" label="" >--SELECT--</option>
                          <option value="1" <?php if($data['inside_parking'] == '1') {echo 'selected="selected"';}  ?>>Yes</option>
                          <option value="0" <?php if($data['inside_parking'] == '0') {echo 'selected="selected"';}  ?>>No</option>
                        </select>
                        <?php } ?>
                      </div>
                    </div>

                  </div>
                </div>
                <div class="xcrud-overlay"></div>
              </div>
            </div>
          </div>
          
          <div class="tab-pane wow fadeIn animated in" id="PAYMENT_METHODS">
            <div class="add_button_modal" >
              <button type="button" data-toggle="modal" data-target="#ADD_PAYMENT_TYPES" class="btn btn-success"><i class="glyphicon glyphicon-plus-sign"></i>Add</button>
            </div>
            <div class="xcrud">
              <div class="xcrud-container">
                <div class="xcrud-ajax">
                  <input type="hidden" class="xcrud-data" name="key" value="" />
                  <input type="hidden" class="xcrud-data" name="orderby" value="" />
                  <input type="hidden" class="xcrud-data" name="order" value="desc" />
                  <input type="hidden" class="xcrud-data" name="start" value="0" />
                  <input type="hidden" class="xcrud-data" name="limit" value="10" />
                  <input type="hidden" class="xcrud-data" name="instance" value="" />
                  <input type="hidden" class="xcrud-data" name="task" value="list" />
                  <div class="xcrud-top-actions">
                    <div class="btn-group pull-right"><a href="" class="btn btn-danger"><i class="glyphicon glyphicon-remove"></i> Delete Selected</a><a href="javascript:;" data-task="print" class="btn btn-default xcrud-in-new-window xcrud-action"><i class="glyphicon glyphicon-print"></i> Print</a><a href="javascript:;" data-task="csv" class="btn btn-default xcrud-in-new-window xcrud-action"><i class="glyphicon glyphicon-file"></i> Export into CSV</a></div>
                    <div class="clearfix"></div>
                  </div>
                  <div class="xcrud-list-container">
                    <table class="xcrud-list table table-striped table-hover">
                      <thead>
                        <tr class="xcrud-th">
                          <th><input class=all type='checkbox' value='' id=select_all ></th>
                          <th class="xcrud-num">&#35;</th>
                          <th data-order="desc" data-orderby="pt_hotels_types_settings.sett_name" class="xcrud-column xcrud-action">Name</th>
                          <th data-order="desc" data-orderby="pt_hotels_types_settings.sett_selected" class="xcrud-column xcrud-action">Selected</th>
                          <th data-order="desc" data-orderby="pt_hotels_types_settings.sett_status" class="xcrud-column xcrud-action">Status</th>
                          <th class="xcrud-actions">&nbsp;</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr class="xcrud-row xcrud-row-0">
                          <td><input class="checkboxcls" type='checkbox' value=175></td>
                          <td class="xcrud-current xcrud-num">1</td>
                          <td>Pay on Arrival</td>
                          <td>Yes</td>
                          <td>Yes</td>
                          <td class="xcrud-current xcrud-actions xcrud-fix"><span class="btn-group"><a class="btn btn-default btn-xcrud btn btn-warning" href="#sett175" title="Edit" data-toggle="modal"><i class="fa fa-edit"></i></a><a class="btn btn-default btn-xcrud btn-danger" href="" title="DELETE" target="_self" id="175"><i class="fa fa-times"></i></a></span></td>
                        </tr>
                        
                        <tr class="xcrud-row xcrud-row-0">
                          <td><input class=checkboxcls type='checkbox' value=27></td>
                          <td class="xcrud-current xcrud-num">7</td>
                          <td>Skrill</td>
                          <td>No</td>
                          <td>Yes</td>
                          <td class="xcrud-current xcrud-actions xcrud-fix"><span class="btn-group"><a class="btn btn-default btn-xcrud btn btn-warning" href="#sett27" title="Edit" data-toggle="modal"><i class="fa fa-edit"></i></a><a class="btn btn-default btn-xcrud btn-danger" href="" title="DELETE" target="_self" id="27"><i class="fa fa-times"></i></a></span></td>
                        </tr>
                        </tbody>
                       <tfoot>
                      </tfoot>
                    </table>
                  </div>

                  <div class="xcrud-nav">
                    <div class="btn-group xcrud-limit-buttons" data-toggle="buttons-radio">
                      <button type="button" class="btn btn-default active xcrud-action" data-limit="10">10</button> 
                      <button type="button" class="btn btn-default xcrud-action" data-limit="25">25</button>
                      <button type="button" class="btn btn-default xcrud-action" data-limit="50">50</button>
                      <button type="button" class="btn btn-default xcrud-action" data-limit="100">100</button>
                      <button type="button" class="btn btn-default xcrud-action" data-limit="all">All</button>
                    </div>

                    <a class="xcrud-search-toggle btn btn-default" href="javascript:;">Search</a><span class="xcrud-search form-inline" style="display:none;">
                    
                    <input class="xcrud-searchdata xcrud-search-active input-small form-control" name="phrase" data-type="text" style="" data-fieldtype="default" type="text" value="" />
                    
                    <select class="xcrud-searchdata  input-small form-control" name="phrase" style="display:none" data-type="select" data-fieldtype="dropdown" data-fieldname="pt_hotels_types_settings.sett_selected">
                      <option value="Yes">Yes</option>
                      <option value="No">No</option>
                    </select>

                    <select class="xcrud-searchdata  input-small form-control" name="phrase" style="display:none" data-type="select" data-fieldtype="dropdown" data-fieldname="pt_hotels_types_settings.sett_status">
                      <option value="Yes">Yes</option>
                      <option value="No">No</option>
                    </select>

                    <select class="xcrud-data xcrud-columns-select input-small form-control" name="column">
                      <option value="">All fields</option>
                      <option value="pt_hotels_types_settings.sett_name" data-type="text">Name</option>
                      <option value="pt_hotels_types_settings.sett_selected" data-type="select">Selected
                      </option>
                      <option value="pt_hotels_types_settings.sett_status" data-type="select">Status</option>
                    </select>
                  </div>
                 </div>
                <div class="xcrud-overlay"></div>
              </div>
            </div>
          </div>
          <!--  <div class="tab-pane wow fadeIn animated in" id="HOTELS_AMENITIES">
            <div class="add_button_modal" >
              <button type="button" data-toggle="modal" data-target="#ADD_HOTEL_AMT" class="btn btn-success"><i class="glyphicon glyphicon-plus-sign"></i>Add</button>
            </div>
            <div class="xcrud">
              <div class="xcrud-container">
                <div class="xcrud-ajax">
                  <input type="hidden" class="xcrud-data" name="key" value="" />
                  <input type="hidden" class="xcrud-data" name="orderby" value="" />
                  <input type="hidden" class="xcrud-data" name="order" value="desc" />
                  <input type="hidden" class="xcrud-data" name="start" value="0" />
                  <input type="hidden" class="xcrud-data" name="limit" value="10" />
                  <input type="hidden" class="xcrud-data" name="instance" value="" />
                  <input type="hidden" class="xcrud-data" name="task" value="list" />
                  <div class="xcrud-top-actions">
                    <div class="btn-group pull-right"><a href="" class="btn btn-danger"><i class="glyphicon glyphicon-remove"></i> Delete Selected</a><a href="javascript:;" data-task="print" class="btn btn-default xcrud-in-new-window xcrud-action"><i class="glyphicon glyphicon-print"></i> Print</a><a href="javascript:;" data-task="csv" class="btn btn-default xcrud-in-new-window xcrud-action"><i class="glyphicon glyphicon-file"></i> Export into CSV</a></div>
                    <div class="clearfix"></div>
                  </div>
                  <div class="xcrud-list-container">
                    <table class="xcrud-list table table-striped table-hover">
                      <thead>
                        <tr class="xcrud-th">
                          <th><input class=all type='checkbox' value='' id=select_all ></th>
                          <th class="xcrud-num">&#35;</th>
                          <th data-order="desc" data-orderby="" class="xcrud-column xcrud-action"> Icon</th>
                          <th data-order="desc" data-orderby="" class="xcrud-column xcrud-action"> Name</th>
                          <th data-order="desc" data-orderby="" class="xcrud-column xcrud-action"> Selected</th>
                          <th data-order="desc" data-orderby="" class="xcrud-column xcrud-action"> Status</th>
                          <th class="xcrud-actions">&nbsp;</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr class="xcrud-row xcrud-row-0">
                          <td><input class=checkboxcls type='checkbox' value=196></td>
                          <td class="xcrud-current xcrud-num">1</td>
                          <td class="zoom_img"><img alt="" src="" style="max-height: 20px;" /></td>
                          <td>Pets Allowed</td>
                          <td>Yes</td>
                          <td>Yes</td>
                          <td class="xcrud-current xcrud-actions xcrud-fix"><span class="btn-group"><a class="btn btn-default btn-xcrud btn btn-warning" href="#sett196" title="Edit" data-toggle="modal"><i class="fa fa-edit"></i></a><a class="btn btn-default btn-xcrud btn-danger" href="" title="DELETE" target="_self" id="196"><i class="fa fa-times"></i></a></span></td>
                        </tr>
                        <tr class="xcrud-row xcrud-row-1">
                          <td><input class=checkboxcls type='checkbox' value=195></td>
                          <td class="xcrud-current xcrud-num">2</td>
                          <td class="zoom_img"><img alt="" src="" style="max-height: 20px;" /></td>
                          <td>Elevator</td>
                          <td>Yes</td>
                          <td>Yes</td>
                          <td class="xcrud-current xcrud-actions xcrud-fix"><span class="btn-group"><a class="btn btn-default btn-xcrud btn btn-warning" href="#sett195" title="Edit" data-toggle="modal"><i class="fa fa-edit"></i></a><a class="btn btn-default btn-xcrud btn-danger" href="" title="DELETE" target="_self" id="195"><i class="fa fa-times"></i></a></span></td>
                        </tr>
                        <tr class="xcrud-row xcrud-row-0">
                          <td><input class=checkboxcls type='checkbox' value=194></td>
                          <td class="xcrud-current xcrud-num">3</td>
                          <td class="zoom_img"><img alt="" src="" style="max-height: 20px;" /></td>
                          <td>Cards Accepted</td>
                          <td>Yes</td>
                          <td>Yes</td>
                          <td class="xcrud-current xcrud-actions xcrud-fix"><span class="btn-group"><a class="btn btn-default btn-xcrud btn btn-warning" href="#sett194" title="Edit" data-toggle="modal"><i class="fa fa-edit"></i></a><a class="btn btn-default btn-xcrud btn-danger" href="" title="DELETE" target="_self" id="194"><i class="fa fa-times"></i></a></span></td>
                        </tr>
                        <tr class="xcrud-row xcrud-row-1">
                          <td><input class=checkboxcls type='checkbox' value=193></td>
                          <td class="xcrud-current xcrud-num">4</td>
                          <td class="zoom_img"><img alt="" src="" style="max-height: 20px;" /></td>
                          <td>Banquet Hall</td>
                          <td>Yes</td>
                          <td>Yes</td>
                          <td class="xcrud-current xcrud-actions xcrud-fix"><span class="btn-group"><a class="btn btn-default btn-xcrud btn btn-warning" href="#sett193" title="Edit" data-toggle="modal"><i class="fa fa-edit"></i></a><a class="btn btn-default btn-xcrud btn-danger" href="" title="DELETE" target="_self" id="193"><i class="fa fa-times"></i></a></span></td>
                        </tr>
                        <tr class="xcrud-row xcrud-row-0">
                          <td><input class=checkboxcls type='checkbox' value=192></td>
                          <td class="xcrud-current xcrud-num">5</td>
                          <td class="zoom_img"><img alt="" src="" style="max-height: 20px;" /></td>
                          <td>Air Conditioner</td>
                          <td>Yes</td>
                          <td>Yes</td>
                          <td class="xcrud-current xcrud-actions xcrud-fix"><span class="btn-group"><a class="btn btn-default btn-xcrud btn btn-warning" href="#sett192" title="Edit" data-toggle="modal"><i class="fa fa-edit"></i></a><a class="btn btn-default btn-xcrud btn-danger" href="" title="DELETE" target="_self" id="192"><i class="fa fa-times"></i></a></span></td>
                        </tr>
                        <tr class="xcrud-row xcrud-row-1">
                          <td><input class=checkboxcls type='checkbox' value=190></td>
                          <td class="xcrud-current xcrud-num">6</td>
                          <td class="zoom_img"><img alt="" src="" style="max-height: 20px;" /></td>
                          <td>Children Activites</td>
                          <td>Yes</td>
                          <td>Yes</td>
                          <td class="xcrud-current xcrud-actions xcrud-fix"><span class="btn-group"><a class="btn btn-default btn-xcrud btn btn-warning" href="#sett190" title="Edit" data-toggle="modal"><i class="fa fa-edit"></i></a><a class="btn btn-default btn-xcrud btn-danger" href="" title="DELETE" target="_self" id="190"><i class="fa fa-times"></i></a></span></td>
                        </tr>
                        <tr class="xcrud-row xcrud-row-0">
                          <td><input class=checkboxcls type='checkbox' value=189></td>
                          <td class="xcrud-current xcrud-num">7</td>
                          <td class="zoom_img"><img alt="" src="" style="max-height: 20px;" /></td>
                          <td>SPA</td>
                          <td>Yes</td>
                          <td>Yes</td>
                          <td class="xcrud-current xcrud-actions xcrud-fix"><span class="btn-group"><a class="btn btn-default btn-xcrud btn btn-warning" href="#sett189" title="Edit" data-toggle="modal"><i class="fa fa-edit"></i></a><a class="btn btn-default btn-xcrud btn-danger" href="" title="DELETE" target="_self" id="189"><i class="fa fa-times"></i></a></span></td>
                        </tr>
                        <tr class="xcrud-row xcrud-row-1">
                          <td><input class=checkboxcls type='checkbox' value=188></td>
                          <td class="xcrud-current xcrud-num">8</td>
                          <td class="zoom_img"><img alt="" src="" style="max-height: 20px;" /></td>
                          <td>Fitness Center</td>
                          <td>Yes</td>
                          <td>Yes</td>
                          <td class="xcrud-current xcrud-actions xcrud-fix"><span class="btn-group"><a class="btn btn-default btn-xcrud btn btn-warning" href="#sett188" title="Edit" data-toggle="modal"><i class="fa fa-edit"></i></a><a class="btn btn-default btn-xcrud btn-danger" href="" title="DELETE" target="_self" id="188"><i class="fa fa-times"></i></a></span></td>
                        </tr>
                        <tr class="xcrud-row xcrud-row-0">
                          <td><input class=checkboxcls type='checkbox' value=187></td>
                          <td class="xcrud-current xcrud-num">9</td>
                          <td class="zoom_img"><img alt="" src="" style="max-height: 20px;" /></td>
                          <td>Shuttle Bus Service</td>
                          <td>Yes</td>
                          <td>Yes</td>
                          <td class="xcrud-current xcrud-actions xcrud-fix"><span class="btn-group"><a class="btn btn-default btn-xcrud btn btn-warning" href="#sett187" title="Edit" data-toggle="modal"><i class="fa fa-edit"></i></a><a class="btn btn-default btn-xcrud btn-danger" href="" title="DELETE" target="_self" id="187"><i class="fa fa-times"></i></a></span></td>
                        </tr>
                        <tr class="xcrud-row xcrud-row-1">
                          <td><input class=checkboxcls type='checkbox' value=186></td>
                          <td class="xcrud-current xcrud-num">10</td>
                          <td class="zoom_img"><img alt="" src="" style="max-height: 20px;" /></td>
                          <td>Inside Parking</td>
                          <td>Yes</td>
                          <td>Yes</td>
                          <td class="xcrud-current xcrud-actions xcrud-fix"><span class="btn-group"><a class="btn btn-default btn-xcrud btn btn-warning" href="#sett186" title="Edit" data-toggle="modal"><i class="fa fa-edit"></i></a><a class="btn btn-default btn-xcrud btn-danger" href="" title="DELETE" target="_self" id="186"><i class="fa fa-times"></i></a></span></td>
                        </tr>
                      </tbody>
                      <tfoot>
                      </tfoot>
                    </table>
                  </div>
                  <div class="xcrud-nav">
                    <div class="btn-group xcrud-limit-buttons" data-toggle="buttons-radio">
                      <button type="button" class="btn btn-default active xcrud-action" data-limit="10">10</button>
                      <button type="button" class="btn btn-default xcrud-action" data-limit="25">25</button>
                      <button type="button" class="btn btn-default xcrud-action" data-limit="50">50</button>
                      <button type="button" class="btn btn-default xcrud-action" data-limit="100">100</button>
                      <button type="button" class="btn btn-default xcrud-action" data-limit="all">All</button>
                    </div>
                    <ul class="pagination">
                      <li class="active"><span>1</span></li>
                      <li class=""><a href="javascript:;" class="xcrud-action" data-start="10">2</a></li>
                    </ul>
                    <a class="xcrud-search-toggle btn btn-default" href="javascript:;">Search</a><span class="xcrud-search form-inline" style="display:none;">
                    <input class="xcrud-searchdata xcrud-search-active input-small form-control" name="phrase" data-type="text" style="" data-fieldtype="default" type="text" value="" />
                    <select class="xcrud-searchdata  input-small form-control" name="phrase" style="display:none" data-type="select" data-fieldtype="dropdown" data-fieldname="pt_hotels_types_settings.sett_selected">
                      <option value="Yes">Yes</option>
                      <option value="No">No</option>
                    </select>
                    <select class="xcrud-searchdata  input-small form-control" name="phrase" style="display:none" data-type="select" data-fieldtype="dropdown" data-fieldname="pt_hotels_types_settings.sett_status">
                      <option value="Yes">Yes</option>
                      <option value="No">No</option>
                    </select>
                    <select class="xcrud-data xcrud-columns-select input-small form-control" name="column">
                      <option value="">All fields</option>
                      <option value="pt_hotels_types_settings.sett_name" data-type="text">Name</option>
                      <option value="pt_hotels_types_settings.sett_selected" data-type="select">Selected</option>
                      <option value="pt_hotels_types_settings.sett_status" data-type="select">Status</option>
                    </select>
                  </div>
                </div>
                <div class="xcrud-overlay"></div>
              </div>
            </div>
          </div> -->
          <div class="tab-pane wow fadeIn animated in" id="ROOMS_AMENITIES">
            <div class="add_button_modal" >
              <button type="button" data-toggle="modal" data-target="#ADD_ROOM_AMT" class="btn btn-success"><i class="glyphicon glyphicon-plus-sign"></i>Add Facility</button>
            </div>
            <div class="xcrud">
              <div class="xcrud-container">
                <div class="xcrud-ajax">
                  <input type="hidden" class="xcrud-data" name="key" value="" />
                  <input type="hidden" class="xcrud-data" name="orderby" value="" />
                  <input type="hidden" class="xcrud-data" name="order" value="desc" />
                  <input type="hidden" class="xcrud-data" name="start" value="0" />
                  <input type="hidden" class="xcrud-data" name="limit" value="10" />
                  <input type="hidden" class="xcrud-data" name="instance" value="" />
                  <input type="hidden" class="xcrud-data" name="task" value="list" />
                  <div class="xcrud-top-actions">
                    <div class="btn-group pull-right"><a href="" class="btn btn-danger"><i class="glyphicon glyphicon-remove"></i> Delete Selected</a><a href="javascript:;" data-task="print" class="btn btn-default xcrud-in-new-window xcrud-action"><i class="glyphicon glyphicon-print"></i> Print</a><a href="javascript:;" data-task="csv" class="btn btn-default xcrud-in-new-window xcrud-action"><i class="glyphicon glyphicon-file"></i> Export into CSV</a></div>
                    <div class="clearfix"></div>
                  </div>
                  <div class="xcrud-list-container">
                    <table class="xcrud-list table table-striped table-hover">
                      <thead>
                        <tr class="xcrud-th">
                          <th>
                          <input class="all" type='checkbox' value='' id="select_all" ></th>
                          <th class="xcrud-num">&#35;</th>
                          <th data-order="desc" data-orderby="pt_hotels_types_settings.sett_name" class="xcrud-column xcrud-action"> Name</th>
                          <th data-order="desc" data-orderby="pt_hotels_types_settings.sett_status" class="xcrud-column xcrud-action"> Status</th>
                          <th class="xcrud-actions">&nbsp;</th>
                        </tr>
                      </thead>

              <tbody>
                   <?php
                if(isset($_POST['provider'])){
                $a = $_POST['provider'];
                $sql   = "SELECT * FROM `tbl_facilities` WHERE provider_id = '$a'";
                $query = $this->db->query($sql);
                foreach ($query->result() as $row) { ?>
                        <tr class="xcrud-row xcrud-row-0">

                          <td><input class="checkboxcls" type="checkbox" name="facility[]" value="174"></td>
                          <td class="xcrud-current xcrud-num"><?php echo $row->id; ?></td>
                          <td><?php echo $row->facility_name; ?></td>
                          
                          <td><?php echo $row->status; ?></td>
                          <td class="xcrud-current xcrud-actions xcrud-fix"><span class="btn-group"><a class="btn btn-default btn-xcrud btn btn-warning" href="#facility<?php echo $row->id; ?>" title="Edit" data-toggle="modal"><i class="fa fa-edit"></i></a><a class="btn btn-default btn-xcrud btn-danger" href="<?php echo site_url('welcome/update_facility/'. $row->id.''); ?>" title="DELETE" target="_self" id="174"><i class="fa fa-times"></i></a></span></td>
                        </tr>
                        <?php } } ?>                        
                      </tbody>
                      <tfoot>
                      </tfoot>
                    </table>
                  </div>
                  <div class="xcrud-nav">
                    
                    <div class="btn-group xcrud-limit-buttons" data-toggle="buttons-radio">
                      <button type="button" class="btn btn-default active xcrud-action" data-limit="10">10
                      </button>
                      <button type="button" class="btn btn-default xcrud-action" data-limit="25">25</button>
                      <button type="button" class="btn btn-default xcrud-action" data-limit="50">50</button>
                      <button type="button" class="btn btn-default xcrud-action" data-limit="100">100</button>
                      <button type="button" class="btn btn-default xcrud-action" data-limit="all">All</button>
                    </div>
                    <!-- <ul class="pagination">
                      <li class="active"><span>1</span></li>
                      <li class=""><a href="javascript:;" class="xcrud-action" data-start="10">2</a></li>
                      <li class=""><a href="javascript:;" class="xcrud-action" data-start="20">3</a></li>
                      <li class=""><a href="javascript:;" class="xcrud-action" data-start="30">4</a></li>
                    </ul> -->
                    <a class="xcrud-search-toggle btn btn-default" href="javascript:;">Search</a><span class="xcrud-search form-inline" style="display:none;">
                    <input class="xcrud-searchdata xcrud-search-active input-small form-control" name="phrase" data-type="text" style="" data-fieldtype="default" type="text" value="" />
                    <select class="xcrud-searchdata  input-small form-control" name="phrase" style="display:none" data-type="select" data-fieldtype="dropdown" data-fieldname="pt_hotels_types_settings.sett_selected">
                      <option value="Yes">Yes</option>
                      <option value="No">No</option>
                    </select>
                    <select class="xcrud-searchdata  input-small form-control" name="phrase" style="display:none" data-type="select" data-fieldtype="dropdown" data-fieldname="pt_hotels_types_settings.sett_status">
                    <option value="Yes">Yes</option>
                    <option value="No">No</option>
                    </select>
                    
                    <select class="xcrud-data xcrud-columns-select input-small form-control" name="column">
                    <option value="">All fields</option>
                    <option value="pt_hotels_types_settings.sett_name" data-type="text">Name</option>
                    <option value="pt_hotels_types_settings.sett_selected" data-type="select">Selected</option>
                    <option value="pt_hotels_types_settings.sett_status" data-type="select">Status</option>
                    </select>
                  </div>
                </div>
                <div class="xcrud-overlay"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="panel-footer">
        <input type="hidden" name="updatesettings" value="1" />
        <input type="hidden" name="updatefor" value="hotels" />
        <button type="submit" class="btn btn-primary">Update</button>
      </div>
    </div>
  </form>
<!--Add hotel types Modal -->
<!-- edit modal -->

<script>
$(document).ready(function(){
if(window.location.hash != "") {
$('a[href="' + window.location.hash + '"]').click() } });
</script>
</div>
</div>
</div>
<?php include('footer.php'); ?>