<?php include('header.php'); ?>
			<header class="entry-header">
			<h1 class="entry-title">Become a <?php echo $member->membership_plan;?></h1>		</header><!-- .entry-header -->
				<div class="entry-content">
			    
    <ul class="wvn-payment-error">
            </ul>
    <div class="wcmp_regi_main">
        <form class="register" role="form" method="post" enctype="multipart/form-data">
            <input name="vendor_membership_plan_id" id="user_type_for_registration" value="28" type="hidden">
                        <div class="wcmp_regi_form_box">
                                         <h3 class="reg_header2">Account Details</h3>
                                        <div class="wcmp-regi-12">
                        <label for="reg_email">Email address <span class="required">*</span></label>
                        <input required="required" name="email" id="reg_email" value="" type="email">
                    </div>
                                            <div class="wcmp-regi-12">
                            <label for="reg_password">Password <span class="required">*</span></label>
                            <input required="required" name="password" id="reg_password" type="password">
                        </div>
                                        <div style="left: -999em; position: absolute;"><label for="trap">Anti-spam</label><input name="email_2" id="trap" tabindex="-1" type="text"></div>
                    <input id="woocommerce-register-nonce" name="woocommerce-register-nonce" value="eddb1ee85d" type="hidden"><input name="_wp_http_referer" value="/addon/vendor_membership/wcmp_vendor_registration/" type="hidden">
                                    <div class="wcmp-regi-12">
                    <label>Test Vendor <span class="required">*</span></label>
                    <input value="" name="wcmp_vendor_fields[0][value]" placeholder="TV" required="required" type="text">
                    <input name="wcmp_vendor_fields[0][label]" value="Test Vendor" type="hidden">
                    <input name="wcmp_vendor_fields[0][type]" value="textbox" type="hidden">
                </div>
                                                <div class="clearboth"></div>
            </div>
                            <div class="wcmp_regi_form_box">
                    <h3 class="reg_header2">Proceed to Pay</h3>
                    <div class="wcmp-regi-12">
                        <label><input name="wvm_payment_method" class="wvm_payment_method" value="paypal" checked="" type="radio"> Pay via PayPal; you can pay with your credit card if you don’t have a PayPal account.</label>
                    </div>
                    <div class="wcmp-regi-12">
                        <label><input name="wvm_payment_method" class="wvm_payment_method" value="card" type="radio"> Pay via card.</label>
                    </div>
                    <div class="clearboth"></div>
                </div>

                <div class="wcmp_regi_form_box wvm-card-details" style="display: none;">
                    <h3 class="reg_header2">Card Details</h3>
                    <div class="wcmp-regi-12">
                        <!--<label>Card holder name</label>-->
                        <input name="c_holder" class="checkout-input checkout-name" id="cc-card-holder" title="Card Holder Name" placeholder="Card Holder Name" type="text">
                    </div>
                    <div class="wcmp-regi-12">
                        <!--<label>Card number</label>-->
                        <input name="c_number" id="wcmp_cat_card_number" class="checkout-input checkout-card cc-number" placeholder="4635 8000 0097 1893" type="text">	
                    </div>
                    <div class="wcmp-regi-12">
                        <!--<label>Card type</label>-->
                        <select name="c_type" id="wcmp_cat_card_type" class="checkout-input checkout-exp" placeholder="Card Type">
                            <option value="">Select Your Card Type</option>
                            <option value="Visa">Visa</option>
                            <option value="MasterCard">MasterCard</option>
                            <option value="Discover">Discover</option>
                            <option value="Amex">Amex</option>
                            <option value="JCB">JCB</option>
                        </select>
                    </div>
                    <div class="wcmp-regi-12">
                        <!--<label>Exp month and date in (MM/YY) format</label>-->
                        <input name="c_month_year" id="wcmp_cat_exp_month_year" class="checkout-input checkout-exp cc-exp " placeholder="MM/YY" type="text">
                    </div>
                    <div class="wcmp-regi-12">
                        <!--<label>CVV</label>-->
                        <input name="c_cvv" id="wcmp_cat_card_cvc" class="checkout-input checkout-cvc cc-cvc" placeholder="CVV" type="text">
                    </div>
                    <div class="clearboth"></div>
                </div>
                        <div class="clearboth"></div>
            <p class="woocomerce-FormRow form-row">
                                <input name="create_vendor_membership_payment" type="hidden">
                <a target="_self" btn-btn href="https://www.sandbox.paypal.com/in/cgi-bin/webscr?cmd=_flow&SESSION=6dEi7tvwp2X3tmjLD2PBFc833nybOUoSTDjIifgU9xswhIv0hm74JNAc364&dispatch=50a222a57771920b6a3d7b606239e4d529b525e0b7e69bf0224adecfb0124e9b61f737ba21b08198b34ff9207f24af4547ba33b31fcec6cd" class="btn btn-primary style="background-color:#1e73be" class="wvm_foot wvm_foot_0 ">Register</a> </div>
				<!--<input class="woocommerce-Button button" id="paypal_submit_btn" name="register" value="Register" type="submit">-->
            </p><h2 class="validation_message"></h2>
                    </form>
    </div>

					</div><!-- .entry-content -->
<?php include('footer.php'); ?>