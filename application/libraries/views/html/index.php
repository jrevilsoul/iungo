<?php error_reporting(0); ?>
<!DOCTYPE html>
<html lang="en">
    <head>
	
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Home</title>
        <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
        <link href="<?php echo base_url('frontend_assets/'); ?>css/style.css" type="text/css" rel="stylesheet">
        <link href="<?php echo base_url('frontend_assets/'); ?>css/animate.css" type="text/css" rel="stylesheet">
        <link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_url('frontend_assets/'); ?>favicons/apple-touch-icon.png">
        <link rel="icon" type="image/png" href="<?php echo base_url('frontend_assets/'); ?>favicons/favicon-32x32.png" sizes="32x32">
        <link rel="icon" type="image/png" href="<?php echo base_url('frontend_assets/'); ?>favicons/favicon-16x16.png" sizes="16x16">
        <link rel="manifest" href="<?php echo base_url('frontend_assets/'); ?>favicons/manifest.json">
        <link rel="mask-icon" href="<?php echo base_url('frontend_assets/'); ?>favicons/safari-pinned-tab.svg" color="#5bbad5">
        <meta name="theme-color" content="#ffffff">
        </head>
      <script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyDW7q2v55_UFRY3m1OHJw0Fhb08aXqwzzg&amp;libraries=places"></script>
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	  <script  src="<?php echo base_url('frontend_assets/');?>js/logger.js" ></script>
	  <script   src="<?php echo base_url('frontend_assets/');?>js/yyy.js"></script>
	
<?php
$language[0]->weddingfinder;
?>



	
	
    <style type="text/css">


   
        #boxes-list2::-webkit-scrollbar-track
        {
            -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
            border-radius: 3px;
            background-color: #fff;
        }

        #boxes-list2::-webkit-scrollbar
        {
            width: 10px;
            background-color: #fff !important;
        }

        #boxes-list2::-webkit-scrollbar-thumb
        {
            border-radius: 3px;
            -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,.3)!important;
            background-color: #2aa09c !important;
        }
    </style>
    <body id="boxes-list2" style="max-height:450px; overflow-x:hidden;">
        <!-- Modal -->
        <!--Header starts-->
        <div  class=" slide vedio-slide" >
            <div class="carousel-inner" role="listbox">
                <div class="item active">
                    <div align="center">
                        <video autoplay loop controls muted>
                            <source src="<?php echo base_url('frontend_assets/'); ?>images/7295584.mp4"  width="100%" height="300" type="video/mp4"> 
                        </video>
                    </div>    
                </div>
            </div>
        </div>

        <header>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 header-top">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-7 col-sm-7 col-xs-12">
                            <ul class="header-menu">
                                <?php
                                $sitesetting = "SELECT * FROM `site_settings`";
                                $queryfour = $this->db->query($sitesetting);
                                $qfour = $queryfour->result();
                                ?>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-envelope" aria-hidden="true"></i>&nbsp;
                                        <?php foreach ($qfour as $hello) { ?>
                                            <?php echo $hello->email; ?>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="" class="select_1">
                                            <i class="fa fa-phone" aria-hidden="true" style=""></i><?php if($language2){ echo $language2[0]->phone;}else{?>&nbsp;phone<?php } ?><?php echo $hello->mobile; ?>
                                        </a>
                                    </li>
                                <?php } ?> 
                            </ul>
                        </div>
                        <div class="col-lg-5 col-sm-5 col-xs-12 text-right for-mobile" style="padding-top: 4px;">
                            <div class="dropdown pull-right"> <div class="dropbtn">
							<?php if($lang_vallue == 'en'){
							?>English<?php }elseif($lang_vallue == 'ger'){?>
							Germany <?php }else{?>Language<?php }?><span><img src="http://clonescriptsmart.com/Emirates/img/united.png" align="left"></span></div>

  <div class="dropdown-content">
    <a href="<?php echo site_url('welcome/index/').'en';?>">English</a>
    <a href="<?php echo site_url('welcome/index/').'ger';?>">Germany</a>
  </div>

          
          </div>
          <a href="" style="color:#fff;">Did you need help or want to work with us? </a>                           
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 header-bottom">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-3 col-sm-3 col-xs-10 logo"> <a href="<?php echo site_url('welcome/index');?>"><img src="<?php echo base_url('frontend_assets/'); ?>images/logo.png"></a>
                        </div>
                     
                        <div class="col-lg-9 col-sm-9 col-xs-12 text-center menu-none">
                            <div class="navbar navbar-default pull-right" style="background: none; box-shadow: none; border: none;">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar"> <i class="fa fa-bars" aria-hidden="true"></i>
            
          </button>
                                <div class="navbar-collapse collapse" id="navbar" style="padding: 0;">
                                    <ul class="nav navbar-nav logo-m">
									
   <li><a href="<?php echo site_url('website/searchby_city');?>">
   <?php if($language){ echo $language[0]->weddingfinder;}else{?>WEDDING FINDER<?php }?></a></li>
									
										
                                        <li><a href="<?php echo site_url('website/searchby_city');?>" target="_blank"><?php if($language){ echo $language[0]->marketplace;}else{?>MARKETPLACE<?php }?></a></li>
                                        
                                  <?php if (!empty($_SESSION['email'])) { ?>
                                         <?php $username = $_SESSION['fname']; ?>
                                            <li><a href="<?php echo site_url('website/user_account'); ?>" data-target="">My Accounts</a></li>					
										  <?php $username = $_SESSION['fname']; ?>
                                            <li><a href="<?php echo site_url('website/favourite'); ?>" data-target="">favourite</a></li>
											
                                             <li><a href="<?php echo site_url('website/user_account'); ?>" data-target=""><?php  echo $username;?></a></li>
                                            <li><a href="<?php echo site_url('website/user_logout'); ?>">LOGOUT</a></li>
                                          <?php
										  }elseif(!empty($_SESSION['emaill'])){
										  $vusername = $_SESSION['vfname']; 
										  ?>
                                           <li><a href="<?php echo site_url('website/user_account'); ?>" data-target="">My Accounts</a></li>
                                          <li><a href="" data-target=""><?php  echo $vusername;?></a></li>
                                           <li><a href="<?php echo site_url('website/user_logout'); ?>"><?php if($language){ echo $language[0]->logout;}else{?>LOGOUT<?php }?></a></li>
                                            
                                        <?php } else { ?>
                                            <li><a href="" data-toggle="modal" data-target="#login_form"><?php if($language){ echo $language[0]->login;}else{?>LOGIN<?php }?></a></li>
                                            <li><a href="" data-toggle="modal" data-target="#vendor_login"><?php if($language){ echo $language[0]->vendor;}else{?>VENDOR LOGIN<?php }?></a></li>
                                        <?php } ?>         
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <!--Header ends-->
        <div class="clearfix"></div>
        <div class="find-section col-md-12"><!-- Find search section-->
            <div class="mobile-hero-image show-for-small image-fill" style="">
                <img src="https://www.foreverly.de/addons/default/themes/merchant/img/homepage/mobile-hero-image.jpg" alt="homepage/mobile-hero-image.jpg" class="mobile-img" style="position: absolute; left: 0px;">
            </div>
            <div class="container">
                <div class="row">     
                    <div class="col-md-12 finder-block">
                        <div class="finder-caption">
                            <h1><?php if($language){ echo $language[0]->eventName;}else{?>Find your perfect Wedding<?php }?></h1>
                            <!--<p>Over <strong>1200+ Wedding </strong>for you special date &amp; Find the perfect venue &amp; save you date.</p>-->
							<p><?php if($language){ echo $language[0]->weddingdetail;}else{?>Over 1200+ Wedding for you special date &amp; Find the perfect venue &amp; save you date.<?php }?></p>
                        </div>
                        <div class="finderform">         
                            <form action="<?php echo site_url('Website/search_action'); ?>" method="post">
                                <div class="row">
                                    <div class="form-group col-md-3 col-sm-6 col-xs-12">
                                        <p class="text-left"><?php if($language){ echo $language[0]->where;}else{?>Find Where?<?php }?></p>
							<input type="text" id="geocomplete" name="first_search" class="form-control"  placeholder="<?php if($language){ echo $language[0]->zipcode;}else{?>Enter your zipcode,city<?php }?>" required="required">
 								
                                    </div>
									
                                    <div class="form-group col-md-3 col-sm-6 col-xs-12">
                                        <p class=" text-left"><?php if($language){ echo $language[0]->what;}else{?>What?<?php }?></p>
                                        <select class="form-control" name="second_search" required="required">                                         
                                        <option value=""><?php if($language){ echo $language[0]->serviceProviderType;}else{?>select service type<?php }?></option>
                                        <?php foreach ($view_data as $datas):?>
									
									   <option value="<?php echo $datas['id']; ?>" <?php if($selecttype == $datas['id']) 
										echo 'selected="selected"'; ?>><?php echo $datas['en_serviceTypeName']; ?></option>	
                                
                                        <?php endforeach; ?>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-3 col-sm-6 col-xs-12">
                                         <p class="display_none_p"></p>
                                        <br />
                                        <input type="text" name="third_search" class="form-control" placeholder="<?php if($language){ echo $language[0]->proivderName;}else{?>Provider Name (Optional)<?php }?>">
                                    </div>
                                    <div class="form-group col-md-3 col-sm-6 col-xs-12">
                                        <p class="display_none_p"></p>
                                        <br>
                                        <button type="submit" name="search" class="btn btn-primary btn-lg btn-block"><?php if($language){ echo $language[0]->searchprovider;}else{?>Search Provider<?php }?></button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <!--=====================================================
                       MID CONTAINER SECTION START HERE
        -=============================================================-->
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 wry-section">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <div class="col-lg-3 col-sm-3 col-xs-12"><img src="<?php echo base_url('frontend_assets/'); ?>images/why.png" class="why-img"></div>
                        <div class="col-lg-9 col-sm-9 col-xs-12"><a href="" data-toggle="modal"  data-target="#login_form" class="btn pink-button btn-lg solid icon-btn" style="min-width:180px;">
                                <img src="<?php echo base_url('frontend_assets/'); ?>images/newwed.png">
                                <?php if($language){ echo $language[0]->NewlyWeds;}else{?>Newlyweds<?php } ?>
                            </a>
                            <span class="wry-span"><?php if($language){ echo $language[0]->howru;}else{?>WHO ARE YOU?<?php } ?></span>
                            <a href="" data-toggle="modal" data-target="#vendor_login" class="btn orange-button btn-lg solid icon-btn" style="min-width:180px;">
                               <img src="<?php echo base_url('frontend_assets/'); ?>images/service.png">
                               <?php if($language){ echo $language[0]->serviceProvider;}else{?>Service provider<?php }?> 
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>

        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 about-back">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <h3 class="about-h3 blue-color animated bounceInUp" style="color:#0e0e0e;"><?php if($language1){ echo $language1[0]->free_wedding_planning;}else{?>FREE WEDDING PLANNING<?php }?> </h3>
                        <span class="figure-image animated bounceInRight"><img src="<?php echo base_url('frontend_assets/'); ?>images/testi-brdr.png"></span> </div>
                    <div class="col-md-4 col-sm-4 col-xs-12"><div class="list-item">
                            <div class="icon"> <img src="<?php echo base_url('frontend_assets/'); ?>images/h1.png" alt="image"> </div>
                            <div class="content">
                                <h4><?php if($language1){ echo $language1[0]->filed_request;}else{?>Fill in the form and send us your 
                                    request<?php } ?></h4>
                            </div>
                        </div>  </div>
                    <div class="col-md-4 col-sm-4 col-xs-12"> <div class="list-item">
                            <div class="icon"> <img src="<?php echo base_url('frontend_assets/'); ?>images/h2.png" alt="image"> </div>
                            <div class="content">
                                <h4><?php if($language1){ echo $language1[0]->best_service;}else{?>We are looking for the best service for you .<?php } ?></h4>
                            </div>
                        </div> 
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12"> <div class="list-item">
                            <div class="icon"> <img src="<?php echo base_url('frontend_assets/'); ?>images/h3.png" alt="image"> </div>
                            <div class="content">
                                <h4><?php if($language1){ echo $language1[0]->lhrdesireservice;}else{?>We are looking for the best service for you.<?php } ?></h4>
                            </div>
                        </div> </div>
                    <div class="content text-center col-md-12"><a href="#" class="read-more" style="box-shadow: 0px 2px 4px #ccc;"><?php if($language1){ echo $language1[0]->freeweddingplanner;}else{?>Use free wedding planner<?php } ?></a></div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
       <!--=================ABOUT US SECTION END HERE=================================-->
   <?php $sitesetting = "SELECT * FROM `site_ratings`";        
         $queryfour = $this->db->query($sitesetting);
         $q  = $queryfour->result();
		 foreach($q as $ratt){
			$ratings = $ratt->review;
		 }
 
 
 ?>
		 
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 menu-box">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <h3 class="about-h3 blue-color animated bounceInUp" style="color:#0e0e0e;"><?php if($language1){ echo $language1[0]->recommended_service;}else{?>WE RECOMMENDED SERVICE PROVIDER<?php } ?></h3>
                       <!-- <div class="col-md-12">
                            <ul class="nav nav-tabs">
                                <li class="active"><a data-toggle="tab" href="#home">Wedding Vendor</a></li>
                                <?php //foreach ($all_s_type as $key) { ?>
                                    <li><a data-toggle="tab" href="#menu1"><?php //echo $key->en_serviceTypeName; ?></a></li>
                                <?php //} ?>

                                <li><a data-toggle="tab" href="#menu4">More</a></li> 
                            </ul>
                        </div>-->
                        <div class="tab-content col-md-12 ">
                            <div id="home" class="tab-pane fade in active">
                                <div class="menu-boxes-wrap">
                                    <?php foreach ($view_data2 as $provider){  
									     
									  if($provider['provider_image'] == ''){
										  
									?>
										<div class="col-md-3 col-sm-6 col-xs-12 margin_b_19">
                                        <div class="box animated bounceInRight" style="margin-bottom:10px">
             										
<a href="<?php echo base_url('website/view_site_detail/').$provider['category_provider'];?>" class="figure-image"> <img src="<?php echo base_url('uploads/');?>empty.jpg" class="">
                                                    <div class="figure-overlay  animated flipInX">
                                                        <div class="figure-overlay-container">
                                                            <div class="favourite-bg"><i class="fa fa-heart pinkcolor"></i></div>
                                                        </div>
                                                    </div>
                                                </a>
                                                <h5 class="text-center pinkcolor hight_30"><?php echo $provider['provider_name']; ?></h5>
                                                <p class="location address_hight_event"><i class="fa fa-map-marker"></i>&nbsp;<?php echo $provider['address']; ?>
                                                <div class="clearfix"></div>
                                                </p>
                                                <p class="rating boder_bottom_none">
												<?php
												for($i=1; $i<=$ratings; $i++){
													?>
                                                 <i class="fa fa-star"></i> 
												<?php
												}
												?>
												</p>
                                            </div>
                                        </div>  
						
									  <?php
									  }else{
									 
									  ?>
										 
                                    
                                        <div class="col-md-3 col-sm-6 col-xs-12 margin_b_19">
                                        <div class="box animated bounceInRight" style="margin-bottom:10px">
              										
				<a href="<?php echo base_url('website/view_site_detail/').$provider['id'];?>" class="figure-image"> <img src="<?php echo base_url('uploads/') . $provider['provider_image']; ?>" class="">
                                                    <div class="figure-overlay  animated flipInX">
                                                        <div class="figure-overlay-container">
                                                            <div class="favourite-bg"><i class="fa fa-heart pinkcolor"></i></div>
                                                        </div>
                                                    </div>
                                                </a>
                                                <h5 class="text-center pinkcolor hight_30"><?php echo $provider['provider_name']; ?></h5>
                                                <p class="location address_hight_event"><i class="fa fa-map-marker"></i>&nbsp;<?php echo $provider['address']; ?>
                                                <div class="clearfix"></div>
                                                </p>
												<p class="rating boder_bottom_none">
												<?php
												for($i=1; $i<=$ratings; $i++){
													?>
                                                 <i class="fa fa-star"></i> 
												<?php
												}
												?>
												</p>
                                            </div>
                                        </div>
                                    <?php 
									  }
									} ?>
                                </div>
                                  

                                    <div class="clearfix"></div>
                                </div>
                              </div>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>    
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <!--===============================MENU SECTION END HERE=======================================-->
        <div class="carousel-reviews broun-block col-md-12">
            <div class="container">
                <div class="row">
                    <div id="carousel-reviews" class="carousel slide" data-ride="carousel">
                        <div class="col-md-12 text-center">
                            <h3 class="about-h3 animated bounceInUp" style="margin-top: 2px;"><?php if($language1){ echo $language1[0]->testimonials;}else{?>TESTIMONIALS<?php } ?></h3>
                            <span class="figure-image animated bounceInRight"><img src="<?php echo base_url('frontend_assets/'); ?>images/testi-brdr.png"></span> </div>
                        <div class="clearfix"></div>
                         <div class="carousel-inner testimonial-wrap">
                   			<div class="item active">
                                <?php
                            
							 $one = array_chunk($testimon, 3);
							 $size = sizeof($one);
							 foreach($one as $new_test){
							 //echo $new_test[0]->name;
							?>
                            <div class="item active">
                             <div class="col-md-4 col-sm-4  testi-wrap">
                             <div class="testimonial"><article class="text-box"><?php echo $new_test[0]->description;?></article><div class="author-block"><div class="photo-container" style="background-image: url('<?php echo base_url('banners/').$new_test[0]->image; ?>')"></div><strong class="name"><?php echo $new_test[0]->name;?></strong><small class="text-alt"><?php echo $new_test[0]->designation;?></small></div></div>
                            </div>
                            </div>
                            <?php
                            }
                            ?>
                            </div>
                          
                        <?php
						for($i=1; $i<$size; $i++){
						if($i < $size){
						?>
                        <div class="item">
                            <?php
                             $two = array_chunk($testimon, 3);
							 foreach($two as $new_test){
								
							?>
                             <div class="col-md-4 col-sm-4  testi-wrap">
                             <div class="testimonial"><article class="text-box"><?php echo $new_test[$i]->description;?></article><div class="author-block"><div class="photo-container" style="background-image: url('<?php echo base_url('banners/').$new_test[$i]->image; ?>')"></div><strong class="name"><?php echo $new_test[$i]->name;?></strong><small class="text-alt"><?php echo $new_test[$i]->designation;?></small></div></div>
                            </div>
                            <?php
                            }
                            ?>
                        </div>
                        <?php
						}
						}
						?>
                        
                    </div>
                        <a class="left carousel-control" href="#carousel-reviews" role="button" data-slide="prev" style="background-image: none;"> <i class="fa fa-chevron-left test-left-arrow"></i> </a> <a class="right carousel-control" href="#carousel-reviews" role="button" data-slide="next" style="background-image: none;"> <i class="fa fa-chevron-right test-right-arrow"></i> </a>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
        <!--===============================TESTIMONIAL SECTION END HERE=======================================-->


        <!--=====================================================
                      MID CONTAINER SECTION END HERE
        -=============================================================-->
        
        <?php include('footer.php');?>
        
    <!---bootstrap file --->     
   <!--<script src="jquery-3.1.1.min.js"></script>-->
<!--<script src="http://codeorigin.jquery.com/jquery-1.10.2.min.js">-->
</script>
<script src="<?php echo base_url('frontend_assets/'); ?>js/bootstrap.min.js">
</script> 

    <!--bootstrap file -->   

</body>
</html>
   <!-- For Mobile Menu-->
<div class="modal fade login_form" id="vendor_login" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content login_div">
            <div class="modal-header boder_none" >
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="z-index: 9999;
                        position: relative;
                        top: -29px;
                        left: 20px;
                        background: #000;
                        opacity: 1;
                        border-radius: 50%"><span class="glyphicon glyphicon-remove removed"aria-hidden="true" style="line-height:21px; font-size:16px !important; color: #f393bd;"></span></button>
            </div>
            <div class="modal-body">
                <div class="karara">
                    <!-- Logo -->

                    <!-- Form Base -->
                    <div class="row">
                        <div class="form-base">
                            <header>
                                <h1 class="pop-login">Vendor Login</h1>
                            </header>
                            <!-- Form -->
                            <section>
                                <form action="<?php echo site_url('login_vendors/index'); ?>" method="post" id="vendor_login">
                                    <!-- Socials - delete this section if you don't want social connects -->

                                    <!-- /.Socials -->
                                    <div class="input-group login-username">
                                        <div class="input-group-addon">Username</div>
                                        <input type="email" placeholder="User name" name="vendoremail" id="login-username">
                                        <span aria-hidden="true" id="status-username" class="status-icon"></span>
                                    </div>

                                    <div class="input-group login-password">
                                        <div class="input-group-addon">Password</div>
                                        <input type="password" placeholder="Password" name="password" id="login-password">
                                        <span aria-hidden="true" id="status-password" class="status-icon"></span></div>

                                        <div class="row section-action">
                                        <div class="col-xs-12 form-group">
                                        <a class="forgotten-password-trigger custom-color" data-toggle="modal" data-target="#Forrget_form">Forgotten password?</a></div>
                
                                        <!-- Submit -->
                                        <div class="col-xs-12">
                                            <button type="submit" class="btn primary pull-right custom-color-back">Login</button>
                                            <a href="<?php echo site_url('website/view_vendor_signup'); ?>">Sign Up</a>
                                        </div>
                                    </div>

                                </form>
                            </section>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade login_form" id="login_form" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content login_div">
            <div class="modal-header boder_none">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" style=""><span class="glyphicon glyphicon-remove removed"aria-hidden="true"></span></button>
            </div>
            <div class="modal-body pa_4_x">
                <div class=" karara">
                    <!-- Logo -->          
                    <!-- Form Base -->
                    <div class="row">
                        <div class="form-base">
                            <header>

                                <h1 class="pop-login">Usersfsf Login</h1>
                            </header>
                            <!-- Form -->
                            <section>
                                <form action="<?php echo site_url('login_users/index'); ?>" method="post" id="login-form">

                                    <div class="input-group login-username">
                                        <div class="input-group-addon">Email</div>
                                        <input type="text" placeholder="E-mail" name="email" 
                                               id="login-username">
                                        <span aria-hidden="true" id="status-username" class="status-icon"></span>
                                    </div>
                                    <div class="input-group login-password">
                                        <div class="input-group-addon">Password</div>
                                        <input type="password" placeholder="Password" name="password" id="login-password">
                                        <span aria-hidden="true" id="status-password" class="status-icon"></span>
                                    </div>
                                    <div class="row section-action">
                                        <!-- Forgotten Password Trigger -->
                                        <div class="col-xs-12 form-group"><a class="forgotten-password-trigger custom-color" data-toggle="modal" data-target="#Forrget_form">Forgotten password?</a></div>
                                        <!-- Submit -->
                                        <div class="col-xs-12">
                                            <button type="submit" class="btn primary pull-right custom-color-back">Login</button>
                                            <a href="<?php echo site_url('website/view_user_signup'); ?>">Sign Up</a>
                                            <a href="<?php echo site_url("auth/login"); ?>" target="_blank" style="margin-left: 82px;">Login with Facebook</a>
                                        </div>
                                    </div>
                                </form>
                            </section>             
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<script>
      $(function(){
        
        $("#geocomplete").geocomplete();
        
      });
	  
    </script>
	<script>
    $('#myCarousel').carousel({
        interval: 4000
    });
    var clickEvent = false;
    $('#myCarousel').on('click', '.nav a', function() {
        clickEvent = true;
        $('.nav li').removeClass('active');
        $(this).parent().addClass('active');
    }).on('slid.bs.carousel', function(e) {
        if (!clickEvent) {
            var count = $('.nav').children().length - 1;
            var current = $('.nav li.active');
            current.removeClass('active').next().addClass('active');
            var id = parseInt(current.data('slide-to'));
            if (count == id) {
                $('.nav li').first().addClass('active');
            }
        }
        clickEvent = false;
    });
</script>
