<?php include('header.php');?>

<div class="container">
    <div class="row">
        <div class="col-xs-12 col-md-3" style="margin-top:10px; margin-left: 120px;">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        Standard</h3>
                </div>
                <div class="panel-body">
                    <div class="the-price">
                        <h1>
                            $10<span class="subscript">/mo</span></h1>
                        <small>1 month FREE trial</small>
                    </div>
                    <table class="table">
                        <tr>
                            <td>
                                1 Account
                            </td>
                        </tr>
                        <tr class="active">
                            <td>
                                1 Project
                            </td>
                        </tr>
                        <tr>
                            <td>
                                100K API Access
                            </td>
                        </tr>
                        <tr class="active">
                            <td>
                                100MB Storage
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Custom Cloud Services
                            </td>
                        </tr>
                        <tr class="active">
                            <td>
                                Weekly Reports
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="panel-footer">
                    <a href="http://www.jquery2dotnet.com" class="btn btn-success" role="button">Sign Up</a>
                 </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-3" style="margin-top:10px;">
            <div class="panel panel-success">
                <div class="cnrflash">
                    <div class="cnrflash-inner">
                      
                    </div>
                </div>
                <div class="panel-heading">
                    <h3 class="panel-title">
                        Premium Level</h3>
                </div>
                <div class="panel-body">
                    <div class="the-price">
                        <h1>
                            $20<span class="subscript">/mo</span></h1>
                        <small>1 month FREE trial</small>
                    </div>
                    <table class="table">
                        <tr>
                            <td>
                                2 Account
                            </td>
                        </tr>
                        <tr class="active">
                            <td>
                                5 Project
                            </td>
                        </tr>
                        <tr>
                            <td>
                                100K API Access
                            </td>
                        </tr>
                        <tr class="active">
                            <td>
                                200MB Storage
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Custom Cloud Services
                            </td>
                        </tr>
                        <tr class="active">
                            <td>
                                Weekly Reports
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="panel-footer">
                    <a href="http://www.jquery2dotnet.com" class="btn btn-success" role="button">Sign Up</a>
                   </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-3" style="margin-top:10px;">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        Basic Level</h3>
                </div>
                <div class="panel-body">
                    <div class="the-price">
                        <h1>
                            $35<span class="subscript">/mo</span></h1>
                        <small>1 month FREE trial</small>
                    </div>
                    <table class="table">
                        <tr>
                            <td>
                                5 Account
                            </td>
                        </tr>
                        <tr class="active">
                            <td>
                                20 Project
                            </td>
                        </tr>
                        <tr>
                            <td>
                                300K API Access
                            </td>
                        </tr>
                        <tr class="active">
                            <td>
                                500MB Storage
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Custom Cloud Services
                            </td>
                        </tr>
                        <tr class="active">
                            <td>
                                Weekly Reports
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="panel-footer">
                    <a href="http://www.jquery2dotnet.com" class="btn btn-success" role="button">Sign Up</a></div>
            </div>
        </div>
    </div>
</div>

        You already have an account?<a href="<?php echo base_url('website/view_website');?>">Log in here</a>
      
<?php include('footer.php');?>