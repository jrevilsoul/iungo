<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Website extends CI_Controller
{          
          /* http://example.com/index.php/welcome/index         
           * Since this controller is set as the default controller in
           * config/routes.php, it's displayed at http://example.com/
           * @see https://codeigniter.com/user_guide/general/urls.html
           */

                public function __construct()
                  {
                      parent::__construct();
                      $this->load->helper(array('form', 'url'));
                      $this->load->model('Welcome_model','welcome');
                      /* LOADING MODEL * Welcome_model as welcome */
                      /*$this->load->library('session');
                       $is_logged = $this->session->userdata('username');
                        if (!$is_logged) {
                         redirect('login/index', 'refresh');
                         }*/
                  }

            public function view_website()
                {
                   $s_prov = "SELECT * FROM `service_provider`";
                   $query_two = $this->db->query($s_prov);
                   $this->data['all_s_prov'] = $query_two->result();

                   $s_type = "SELECT * FROM `tbl_serviceprovidertype` LIMIT 4";
                   $query_s_type = $this->db->query($s_type);
                   $this->data['all_s_type'] = $query_s_type->result();
                   $this->data['view_data'] = $this->welcome->view_sprovider();

                   $this->load->view('html/index', $this->data);
                }

            public function view_site_singerband()
                {
                 $this->load->view('html/singernbandpage');
                }
            public function view_site_aboutus()
                {
                  $this->load->view('html/Aboutus');
                }
                public function view_site_termsofuse()
                {
                  $this->load->view('html/termofuse');
                }
                public function view_site_contact()
                {
                  $this->load->view('html/contactus');
                }
                
            public function view_site_detail($p_id)
               {                    
               $one_provider = "SELECT * FROM `service_provider` WHERE id = '$p_id'";

                $one_row = $this->db->query($one_provider);
                $this->data['prov_id'] = $one_row->result();
                $this->load->view('html/detailpage', $this->data);
                }
           public function view_site_faq()
                {
                  $this->load->view('html/faq');
                }
                
           public function view_site_search()
                {
                  $this->load->view('html/search');
                }

              public function searchby_city()
                {
                $searchtermone    = $this->input->post('first_search');
                $searchtermtwo    = $this->input->post('second_search');
                $searchtermthree  = $this->input->post('third_search');
                $city = $this->uri->segment(3);                

                $red = "SELECT service_provider.id,
                service_provider.provider_type,
                service_provider.provider_image,
                service_provider.provider_name,
                tbl_serviceProviderType.en_serviceTypeName,
                service_provider.address,service_provider.email
                 FROM `service_provider` JOIN tbl_serviceProviderType ON
                service_provider.provider_type = tbl_serviceProviderType.id WHERE provider_name = '$searchtermthree' OR
                provider_type = '$searchtermtwo' OR service_provider.city='$city'";

                $query = $this->db->query($red);

                $redtwo = "SELECT * FROM `service_provider`";
                $querytwo = $this->db->query($redtwo);

                $redthree = "SELECT * FROM `tbl_serviceProviderType`";
                $querythree = $this->db->query($redthree);

                $this->data['q'] = $query->result();    // for query result one
                $this->data['qtwo'] = $querytwo->result();    // for query result two
                $this->data['qthree'] = $querythree->result();    // for query result three
                $this->data['view_data'] = $this->welcome->view_sprovider();

                $this->load->view('html/search', $this->data);
                }

                public function search_action()
                {
                $searchtermone    = $this->input->post('first_search');
                $searchtermtwo    = $this->input->post('second_search');
                $searchtermthree  = $this->input->post('third_search');
                $fromurl = $this->uri->segment(3);                

                $red = "SELECT service_provider.id,
                service_provider.provider_type,
                service_provider.provider_image,
                service_provider.provider_name,
                tbl_serviceProviderType.en_serviceTypeName,
                service_provider.address,service_provider.email
                 FROM `service_provider` JOIN tbl_serviceProviderType ON
                service_provider.provider_type = tbl_serviceProviderType.id WHERE provider_name = '$searchtermthree' OR
                provider_type = '$searchtermtwo' OR service_provider.zipcode='$searchtermone' OR tbl_serviceProviderType.id='$fromurl'";

                $query = $this->db->query($red);

                $redtwo = "SELECT * FROM `service_provider`";
                $querytwo = $this->db->query($redtwo);

                $redthree = "SELECT * FROM `tbl_serviceProviderType`";
                $querythree = $this->db->query($redthree);


                $this->data['q'] = $query->result();    // for query result one
                $this->data['qtwo'] = $querytwo->result();    // for query result two
                $this->data['qthree'] = $querythree->result();    // for query result three
                   // for query result three
                $this->data['view_data'] = $this->welcome->view_sprovider();

                $this->load->view('html/search', $this->data);
                
                }
           /************************** Website user module end   *******************/
           

         }
?>