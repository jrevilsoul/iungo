<?php  
include('header.php'); 
//print_r($_SESSION['id']);

?>
<link href="<?php echo base_url('frontend_assets/'); ?>css/slider_style.css" type="text/css" rel="stylesheet" />
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script>
$( function() {
  //alert("hello");
$( "#contact_request_company_date" ).datepicker();
	//alert('yes');
});
</script>

<script src="<?php echo base_url('frontend_assets/'); ?>js/jssor.slider-22.2.7.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        jssor_1_slider_init = function() {

            var jssor_1_SlideshowTransitions = [
              {$Duration:1200,x:0.3,$During:{$Left:[0.3,0.7]},$Easing:{$Left:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:1200,x:-0.3,$SlideOut:true,$Easing:{$Left:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:1200,x:-0.3,$During:{$Left:[0.3,0.7]},$Easing:{$Left:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:1200,x:0.3,$SlideOut:true,$Easing:{$Left:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:1200,y:0.3,$During:{$Top:[0.3,0.7]},$Easing:{$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:1200,y:-0.3,$SlideOut:true,$Easing:{$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:1200,y:-0.3,$During:{$Top:[0.3,0.7]},$Easing:{$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:1200,y:0.3,$SlideOut:true,$Easing:{$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:1200,x:0.3,$Cols:2,$During:{$Left:[0.3,0.7]},$ChessMode:{$Column:3},$Easing:{$Left:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:1200,x:0.3,$Cols:2,$SlideOut:true,$ChessMode:{$Column:3},$Easing:{$Left:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:1200,y:0.3,$Rows:2,$During:{$Top:[0.3,0.7]},$ChessMode:{$Row:12},$Easing:{$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:1200,y:0.3,$Rows:2,$SlideOut:true,$ChessMode:{$Row:12},$Easing:{$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:1200,y:0.3,$Cols:2,$During:{$Top:[0.3,0.7]},$ChessMode:{$Column:12},$Easing:{$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:1200,y:-0.3,$Cols:2,$SlideOut:true,$ChessMode:{$Column:12},$Easing:{$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:1200,x:0.3,$Rows:2,$During:{$Left:[0.3,0.7]},$ChessMode:{$Row:3},$Easing:{$Left:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:1200,x:-0.3,$Rows:2,$SlideOut:true,$ChessMode:{$Row:3},$Easing:{$Left:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:1200,x:0.3,y:0.3,$Cols:2,$Rows:2,$During:{$Left:[0.3,0.7],$Top:[0.3,0.7]},$ChessMode:{$Column:3,$Row:12},$Easing:{$Left:$Jease$.$InCubic,$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:1200,x:0.3,y:0.3,$Cols:2,$Rows:2,$During:{$Left:[0.3,0.7],$Top:[0.3,0.7]},$SlideOut:true,$ChessMode:{$Column:3,$Row:12},$Easing:{$Left:$Jease$.$InCubic,$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:1200,$Delay:20,$Clip:3,$Assembly:260,$Easing:{$Clip:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:1200,$Delay:20,$Clip:3,$SlideOut:true,$Assembly:260,$Easing:{$Clip:$Jease$.$OutCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:1200,$Delay:20,$Clip:12,$Assembly:260,$Easing:{$Clip:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:1200,$Delay:20,$Clip:12,$SlideOut:true,$Assembly:260,$Easing:{$Clip:$Jease$.$OutCubic,$Opacity:$Jease$.$Linear},$Opacity:2}
            ];

            var jssor_1_options = {
              $AutoPlay: true,
              $SlideshowOptions: {
                $Class: $JssorSlideshowRunner$,
                $Transitions: jssor_1_SlideshowTransitions,
                $TransitionsOrder: 1
              },
              $ArrowNavigatorOptions: {
                $Class: $JssorArrowNavigator$
              },
              $ThumbnailNavigatorOptions: {
                $Class: $JssorThumbnailNavigator$,
                $Cols: 10,
                $SpacingX: 8,
                $SpacingY: 8,
                $Align: 360
              }
            };

            var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

            /*responsive code begin*/
            /*you can remove responsive code if you don't want the slider scales while window resizing*/
            function ScaleSlider() {
                var refSize = jssor_1_slider.$Elmt.parentNode.clientWidth;
                if (refSize) {
                    refSize = Math.min(refSize, 800);
                    jssor_1_slider.$ScaleWidth(refSize);
                }
                else {
                    window.setTimeout(ScaleSlider, 30);
                }
            }
            ScaleSlider();
            $Jssor$.$AddEvent(window, "load", ScaleSlider);
            $Jssor$.$AddEvent(window, "resize", ScaleSlider);
            $Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);
            /*responsive code end*/
        };
    </script>

<!--=====================================================              
 MID CONTAINER SECTION START HERE-=============================================================-->

<style>
.unclickable {
	color:#939393;
}
.clickable {
	color:#fbd200;
}
.start-review a {
	color:#939393;
}
.start-review a:hover {
	color: #fbd200;
}
.margin_top_1{ margin:0px !important;}
.tab-content>.tab-pane{    border: solid 4px #db5677 !important;}
.nav-tabs > li.active > a{    background-color: #db5677 !important;}
</style>
<div class="col-md-12 col-sm-12 col-xs-12 search-section">
  <div class="container">
    <div class="row">
      <div class="bc">
	
        <ul class="breadcrumb">
          <li> <a href="<?php echo site_url('website/view_website'); ?>" rel="" title="Eventlocation">Home
            </a> </li>
           <li><?php echo $cityname[0]->cityName;?>
             <!--<a href=""  rel="" title="Eventlocation Duesseldorf"> 
              <?php //echo $this->breadcrumb->add('Tutorials', base_url().'tutorials');?>
            </a>-->
          </li>
          <li> <?php echo $service_provide[0]->provider_name;?>
            <!--<a href=""  rel="" title="Eventlocation Duesseldorf"> 
              <?php //echo $this->breadcrumb->add('Tutorials', base_url().'tutorials');?>
            </a> 
          </li>
          <!--<li class="active"> <span class="pinkcolor-new"> <?php //echo $qcat['0']->provider_name;?>
            <?php //echo $this->breadcrumb->add('Spring Tutorial', base_url().'tutorials/spring-tutorials'); ?>
            </span> </li>-->
        </ul>
      </div>
    </div>
    <div class="row">
      <div class="col-md-9 width_72">
        <div class="deail-disc">
        <?php $rating = $rattinstar[0]->star; ?>
          <h3 class="pinkcolor-new"> <?php echo $value->provider_name;?> 
		  <?php if(!empty($uid = $_SESSION['uid'])){
			    $id = $service_provide[0]->id; 
				$uid = $_SESSION['uid'];
			  ?>
		  <span class="pull-right"><a href="<?php echo site_url('website/add_favourite/').$id.'/'.$uid;?>" 
          class="pinkcolor-new" style="font-size: 19px;"> <i class="fa fa-heart" aria-hidden="true"> </i> </a> </span> <?php } ?></h3>
          <span class="start-review">
          <?php
			for($i=1; $i<=$rating; $i++){
			?>
          <a href=""> <i class="fa fa-star clickable" aria-hidden="true"> </i> </a>
          <?php		  
			}
			?>
          </span> <span class="deatails-add pinkcolor-new"> <?php echo $value->address;?> </span> </div>
        <div class="clearfix"></div>
        
        <ul class="nav nav-tabs">
          <li class="active"><a data-toggle="tab" href="#home">Gallery</a></li>
          <li><a data-toggle="tab" href="#menu1">Videos</a></li>
          <li><a data-toggle="tab" href="#menu2">Mp3</a></li>
        </ul> 

<div class="tab-content margin_top_1">
 <div id="home" class="tab-pane fade in active">
 <div id="jssor_1" class="width_100" style="position:relative;margin:0 auto;top:0px;left:0px;width:800px;height:456px;overflow:hidden;visibility:hidden;background-color:#24262e;">
        <!-- Loading Screen -->
        <div data-u="loading" style="position:absolute;top:0px;left:0px;background-color:rgba(0,0,0,0.7);">
            <div style="filter: alpha(opacity=70); opacity: 0.7; position: absolute; display: block; top: 0px; left: 0px; width: 100%; height: 100%;"></div>
            <div style="position:absolute;display:block;background:url('<?php echo base_url('frontend_assets/'); ?>images/slider_re/loading.gif') no-repeat center center;top:0px;left:0px;width:100%;height:100%;"></div>
        </div>
        <div data-u="slides" style="cursor:default;position:relative;top:0px;left:0px;width:800px;height:356px;overflow:hidden;">
            <?php foreach($gallery as $gall){?>
			
			<div>
                <img data-u="image" src="<?php echo base_url('uploads/files/').$gall->file_name; ?>" />
                <img data-u="thumb" src="<?php echo base_url('uploads/files/').$gall->file_name; ?>"  />
            </div>
			<?php
			}
			?>
        </div>
        <!-- Thumbnail Navigator -->
        <div data-u="thumbnavigator" class="jssort01" style="position:absolute;left:0px;bottom:0px;width:800px;height:100px;" data-autocenter="1">
            <!-- Thumbnail Item Skin Begin -->
            <div data-u="slides" style="cursor: default;">
                <div data-u="prototype" class="p">
                    <div class="w">
                        <div data-u="thumbnailtemplate" class="t"></div>
                    </div>
                    <div class="c"></div>
                </div>
            </div>
            <!-- Thumbnail Item Skin End -->
        </div>
        <!-- Arrow Navigator -->
        <span data-u="arrowleft" class="jssora05l" style="top:158px;left:15px;width:40px;height:40px;"></span>
        <span data-u="arrowright" class="jssora05r" style="top:158px;right:25px;width:40px;height:40px;"></span>
    </div>
    <script type="text/javascript">jssor_1_slider_init();</script>
  </div>
  <div id="menu1" class="tab-pane fade">
    <iframe width="100%" height="456px" src="<?php echo base_url('uploads/video/').$service_provide[0]->provider_video;?>" frameborder="0" allowfullscreen></iframe>
  </div>
  <div id="menu2" class="tab-pane fade">
     <?php 
	   
	 foreach ($gallery as $file) :?>

           <center>
            <audio controls>
                  <source src="<?php echo base_url();?>uploads/audio/<?php echo $file->addd;?>"  type="audio/mp3">
              </audio>
          </center>
        <?php endforeach;?>
  </div>
</div>
        
       
        <div class="content_item details" style="margin-bottom: 0;">

<div class="price_info">
<div class="col-sm-10" style="padding-right:0px;">
<span class="pm-vc-views">
                    <strong style="color:#db5677;">218</strong>
                    <small><?php if($language3){ echo $language3[0]->views;}else{?>Views<?php }?></small>
                    </span>
</div>
<div class="col-sm-2">
<div class="pull-right">
       <button class="btn btn-small border-radius0 btn-video" type="button" data-toggle="button" id="pm-vc-share"><i class="fa fa-share" aria-hidden="true"></i><?php if($language3){ echo $language3[0]->share;}else{?>Share<?php }?></button>
					</div>
</div>
<div class="clearfix"></div>
<div id="pm-vc-share-content" class="alert alert-well col-lg-12">
                <div class="row-fluid">
                    <div class="col-sm-4 panel-1">
                    <div class="input-prepend"><span class="add-on"><?php if($language3){ echo $language3[0]->urll;}else{?>URL<?php }?> </span><input name="video_link" id="video_link" type="text" value="http://afghanshow.com//rahat-fateh-ali-khan-zaroori-tha_d71e4566f.html" class="input-medium form-control" onClick="SelectAll('video_link');"></div>
                    </div>
                    <div class="col-sm-5 panel-2">
                    					<button class="btn border-radius0 btn-video" type="button" id="pm-vc-embed"><?php if($language3){ echo $language3[0]->embeded;}else{?>Embed<?php }?></button>
                                        <button class="btn border-radius0 btn-video" type="button" data-toggle="button" id="pm-vc-email"><i class="fa fa-envelope"></i><?php if($language3){ echo $language3[0]-> 	sendtofriend;}else{?>Send to a friend<?php }?> </button>
                    </div>
                    <div class="col-sm-3 panel-3">
                    <a href="https://www.facebook.com/sharer.php?u=http%3A%2F%2Fafghanshow.com%2F%2Frahat-fateh-ali-khan-zaroori-tha_d71e4566f.html&amp;t=Rahat+Fateh+Ali+Khan-+Zaroori+Tha" onClick="javascript:window.open(this.href,
      '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;" rel="tooltip" title="Share on FaceBook"><i class="pm-vc-sprite facebook-icon"></i></a>
                    <a href="https://twitter.com/home?status=Watching%20Rahat+Fateh+Ali+Khan-+Zaroori+Tha%20on%20http%3A%2F%2Fafghanshow.com%2F%2Frahat-fateh-ali-khan-zaroori-tha_d71e4566f.html" onClick="javascript:window.open(this.href,
      '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;" rel="tooltip" title="Share on Twitter"><i class="pm-vc-sprite twitter-icon"></i></a>
                    <a href="https://plus.google.com/share?url=http%3A%2F%2Fafghanshow.com%2F%2Frahat-fateh-ali-khan-zaroori-tha_d71e4566f.html" onClick="javascript:window.open(this.href,
      '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;" rel="tooltip" title="Share on Google+"><i class="pm-vc-sprite google-icon"></i></a>  
                    </div>
                </div>
    
                                <div id="pm-vc-embed-content" class="col-sm-12">
                 <hr>
                  <textarea name="pm-embed-code" id="pm-embed-code" rows="3" class="col-sm-12 form-control" onClick="SelectAll('pm-embed-code');"><iframe width="629" height="400" src="http://afghanshow.com//embed.php?vid=d71e4566f" frameborder="0" allowfullscreen seamless></iframe>
                  <p><a href="http://afghanshow.com//rahat-fateh-ali-khan-zaroori-tha_d71e4566f.html" target="_blank">Rahat Fateh Ali Khan- Zaroori Tha</a></p></textarea>
                </div>
                                <div id="pm-vc-email-content" class="col-sm-12">
                    <hr>
                    <div id="share-confirmation" class="hide well well-small"></div>
                    <form name="sharetofriend" action="" method="POST" class="form-inline">
                      <input type="text" id="name" name="name" class="input-small inp-small form-control" value="" placeholder="<?php if($language3){ echo $language3[0]->yourname;}else{?>Your name<?php }?>">
                      <input type="text" id="email" name="email" class="input-small inp-small form-control" placeholder="<?php if($language3){ echo $language3[0]->friendsemail;}else{?>Friend's Email<?php }?>">
                         
                          <input type="text" name="imagetext" class="input-small inp-small form-control" autocomplete="off" placeholder="<?php if($language3){ echo $language3[0]->confirm;}else{?>Confirm<?php }?>">
                          <button class="btn btn-small btn-link" onClick="document.getElementById('securimage-share').src = '' + Math.random(); return false;"><i class="fa fa-refresh"></i> </button>
                          <img src="<?php echo base_url('frontend_assets/'); ?>images/securimage_show.png" id="securimage-share" alt="">
                    
                      <button type="submit" name="Submit" class="btn btn-success"><?php if($language3){ echo $language3[0]-> 	send;}else{?>Send<?php }?></button>
                    </form>
                </div>
            </div>
</div>
<div class="clearfix"></div>
</div>

<div class="content_item details">
<div class="company_name">
<h1><font><font>
Good Hohenholz 
 </font></font><span><font><font>
<?php echo $value->provider_name;?>
</font></font></span>
</h1>
</div>
<div class="price_info">
<div class="headline2"><font><font>Price from</font></font></div>
<p><font><font>on request (€)</font></font></p>
</div>
<div class="clearfix"></div>
</div>
<div class="content_item_header col-sm-12" style="padding:0px;">
<div class="headline2">
<span class=""><?php if($language3){ echo $language3[0]->location_at_glance;}else{?>Location at a glance<?php }?></span>
</div>
<div class="col-sm-4" style="padding-left:0px;">
<div class="info_short_left">
<p><font class="goog-text-highlight"><strong>For:</strong><?php echo $service_provide[0]->detailevent;?></font></p>
<p><strong>Persons:</strong><?php echo $service_provide[0]->persondetail;?></p>
<!--<p>Kitchen on site</p>
<p>Bookable with staff</p>-->
<p><strong>Square:</strong><?php echo $service_provide[0]->square;?></p>
</div>
</div>
<div class="col-sm-4" style="padding-left:0px;">
<div class="info_short_left">
<p><font><font><strong>Location:</strong><?php echo $service_provide[0]->location;?></font></font></p>
<p><font><font><strong>Catering:</strong><?php echo $service_provide[0]->catering;?></font></font></p>
<p><font><font>Parking lots available</font></font></p>
<p><font><font><strong>Outside:</strong><?php echo $service_provide[0]->outside;?></font></font></p>
<p><font><font><strong>Times booking:</strong><?php echo $service_provide[0]->booking;?></font></font></p>
</div>
</div>
<div class="col-sm-4" style="padding-left:0px;">
<div class="info_short_left" style="border-right:none;">
<p><font class="goog-text-highlight"><strong>Our opinion:</strong><?php echo $service_provide[0]->opinion;?></font></p>
</div>
</div>
</div>
<div class="metadisc col-sm-12" style="padding:0px;">
<div class="headline2">
<span class=""><?php if($language3){ echo $language3[0]->discription;}else{?>Description<?php }?></span>
</div>
<p class=""><?php  echo $service_provide[0]->description; ?></p>
</div>
 <div class="col-sm-12 rm-deatils" style="padding:0px;">
<div class="headline2">
<span class=""><?php if($language3){ echo $language3[0]->room;}else{?>Rooms<?php }?></span>
</div>
 <div class="table-responsive">
  <table class="table table-striped table-bordered table-condensed">
    
	<thead>
      <tr>
        <th>Name</th>
        <th>L</th>
        <th>B</th>
        <th>H</th>
        <th>Sq.m.</th>
        <th>String</th>
         <th>Parliament</th>
        <th>U-Form</th>
        <th>Banquet</th>
        <th>Gala</th>
        <th>Standing</th>
      </tr>
    </thead>
    <tbody>
	<?php 
	
	foreach($new_roomdata as $roomvalue){?>
      <tr>
        <td><?php echo $roomvalue->name;?></td>
        <td><?php echo $roomvalue->luxury;?></td>
        <td><?php echo $roomvalue->basic;?></td>
        <td><?php echo $roomvalue->high;?></td>
        <td><?php echo $roomvalue->squrem;?></td>
        <td><?php echo $roomvalue->string;?></td>
        <td><?php echo $roomvalue->parliament;?></td>
        <td><?php echo $roomvalue->uform;?></td>
        <td><?php echo $roomvalue->banquet;?></td>
        <td><?php echo $roomvalue->gala;?></td>
        <td><?php echo $roomvalue->standing;?></td>
      </tr>
	<?php }?>
	
    </tbody>
  </table>
  </div>
</div>

<div class="col-sm-12 rm-deatils" style="padding:0px;">
<div class="headline2">
<span class=""><?php if($language3){ echo $language3[0]->latest_comments;}else{?>Latest comments and Rate<?php }?></span>
</div>
<?php 
 
foreach($results as $res){	?> 
 <div class="row">
 <div class="col-sm-3 mb20px text-center">
 <img src="<?php echo base_url('frontend_assets/'); ?>images/1.jpg" class="review-user">
 <p class=" " style="color: #979797;"><?php echo $res->username;?></p>
 </div>
 <div class="col-sm-9">
 <?php 
$start = $rattinstar[0]->star;
 for($i=0; $i<$start; $i++){
 ?>
<span class="start-review">	
      <a href=""><i class="fa fa-star clickable"  aria-hidden="true"></i></a>
 </span>
 <?php
 }
 ?>
      <p class="mb20px"><?php echo $res->comment;?></p>
      <p class="mb20px" style="color: #979797;"><?php echo $res->created_date;?></p>
 </div>
 </div>
 <?php
 }
 ?>

 <div class="row">
 <div class="col-md-10 col-sm-10 col-xs-12 pull-right nospace">
 <div class="col-sm-3 mb20px text-center">
 <img src="<?php echo base_url('frontend_assets/'); ?>images/1.jpg" class="review-user">
 <p class=" " style="color: #979797;">Arushi</p>
 </div>
 <div class="col-sm-9">
   <?php 
		   $id = $service_provide[0]->id; 
		   $uid = $_SESSION['uid'];
		   $fname = $_SESSION['fname'];
		  ?>
          <form action="<?php echo site_url('website/comment/'). $id; ?>" method="post" name="comment_form">
		   <input type="hidden" name="item_id" value="<?php echo $id; ?>">
            <input type="hidden" name="username" value="<?php echo $fname;?>" />
            <input type="hidden" name="user_id" value="<?php echo $_SESSION['uid'];?>" /> 
            <input type="hidden" name="stars" value="0" class="nostar" />
 <p class="mb20px"><span class="start-review">
      <a href=""><i class="fa fa-star" aria-hidden="true"></i></a>
      <a href=""><i class="fa fa-star" aria-hidden="true"></i></a>
      <a href=""><i class="fa fa-star" aria-hidden="true"></i></a>
      <a href=""><i class="fa fa-star" aria-hidden="true"></i></a>
      <a href=""><i class="fa fa-star" aria-hidden="true"></i></a>
      </span></p>
      <p class="mb20px">
	  
      <textarea class="form-control" rows="5" name="comment" cols="" style="width:100%;"></textarea>
      </p>
      <p class="mb20px">
     <input type="submit" name="submit" value="<?php if($language3){ echo $language3[0]->comment;}else{?>Comment<?php }?>" class=" primary pull-right custom-color-back full_width_hafe ma_top_bottom pull-right btn_new">
      </p>
   
 </div>
 </form>
 </div>
 <div class="clearfix"></div>
 </div>
 <div id="pagination">
      <ul class="tsc_pagination">
        <?php

?>

        <?php
echo $links; ?>
      </ul>
    </div>
</div>

<div class="col-sm-12 rm-deatils" style="padding:0px;">
<div class="headline2">
    <?php 
                     
   ?>
    <span class=""><?php if($language3){ echo $language3[0]->more_location;}else{?>More Locations<?php }?></span> </div>
 
 <div class="row">
    <?php
	
	
	foreach($citydata as $data){
	 ?>
    <a href="<?php echo base_url('website/view_site_detail/').$data->id;?>">
    <div class="col-sm-4">
      <div class="loc-img"> <img src="<?php echo base_url('uploads/').$data->provider_image; ?>"> </div>
      <div class=""> <?php echo $data->address; ?> </div>
      <span class="pinkcolor-new"> <?php //echo'===='. $data->cityName; ?> </span> </div>
    </a>
    <?php 
	}
	?>
  </div>
 
</div>
<div class="clearfix"></div>       
</div>
<div class=" col-xs-12 col-sm-3 col-md-3 pull-right">
  <div class="row site-loc">
    <div class="headline2">
      <h4 class=""> <a class="btn big-btn js-contact-fancybox js_track_contact_event" href="" name="button" rel="" title="" type="submit" data-toggle="modal" data-target="#date-popover-content"><?php if($language3){ echo $language3[0]->site_contact;}else{?>Show site contact <?php }?>&gt; </a> </h4>
    </div>
    <?php  $sitesetting = "SELECT * FROM `site_settings`";                
		       $queryfour = $this->db->query($sitesetting);                
		       $qfour = $queryfour->result();                
?>
    <div class="text-center f-20px">
      <?php foreach ($qfour as $hello) { 
echo $hello->phone; }?>
    </div>
    <div class="show_explain pinkcolor-new text-center">Warum über Aroos24 anfragen? </div>
  </div>
  <div class="row">
    <div class="side_routmap">
      <div class="headline2" style="margin-bottom:0px;">
        <h4 class="map-route"><?php if($language3){ echo $language3[0]->location;}else{?>Location<?php }?> </h4>
      </div>
      <?php
//$zip = $value->zipcode;    
$zip   = $service_provide[0]->zipcode;
$url = "http://maps.google.com/maps/api/geocode/json?address=$zip&sensor=false&region=England";
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
$response = curl_exec($ch);
curl_close($ch);
$response = json_decode($response);
$lat = $response->results[0]->geometry->location->lat;
$long = $response->results[0]->geometry->location->lng;  
?>
      <div id="googleMap" style="width:100%;height:200px;"> </div>
      <script src="https://code.jquery.com/jquery-1.9.1.min.js">
          </script> 
      <script>
            function myMap() {
              var myLatLng = {
                lat: <?php echo json_encode($lat);
                ?>, lng: <?php echo json_encode($long);
                ?>};
              var map = new google.maps.Map(document.getElementById('googleMap'), {
                zoom: 11,
				mapTypeId: google.maps.MapTypeId.ROADMAP,
                center: myLatLng
              }
                                           );
              var marker = new google.maps.Marker({
                position: myLatLng,
                map: map,
                title: 'Hello World!'
              }
                                                 );
            }
          </script> 
      <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDW7q2v55_UFRY3m1OHJw0Fhb08aXqwzzg&amp;libraries=places&callback=myMap">
          </script> 
      <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js">
          </script> 
      <script  src="<?php echo base_url('frontend_assets/');?>js/logger.js" >
          </script> 
      <script   src="<?php echo base_url('frontend_assets/');?>js/yyy.js">
          </script> 
    </div>
    <div class="side_routmap">
      <div class="headline2">
        <h4 class="map-route"><?php if($language3){ echo $language3[0]->contect_detail;}else{?>Contact Detail<?php }?> </h4>
      </div>
      <div class="c_details">
        <h3 class="headline3"> <span> <font>
          <?php  echo $service_provide[0]->contact_name; ?>
          </font> </span> </h3>
        <p> <span> <font>
          <?php  echo $service_provide[0]->address; ?>
          </font> </span> <br>
          <span> <font> <?php echo $service_provide[0]->zipcode; ?> </font> </span> <span> <font>Dusseldorf </font> </span> <br>
          <br>
          <span> <font> <?php echo $service_provide[0]->phone; ?> </font> </span> </p>
      </div>
    </div>
    <div class="side_routmap">
      <div class="headline2">
        <h4 class="map-route"><?php if($language3){ echo $language3[0]->social_media;}else{?>Social Media<?php }?></h4>
      </div>
      <div class="c_details">
        <ul class="social_media">
          <li> <a href="<?php echo $value->facebook; ?>"> <img src="<?php echo base_url('frontend_assets/'); ?>images/facebook.png"> </a> </li>
          <li> <a href="<?php echo $value->twitter; ?>"> <img src="<?php echo base_url('frontend_assets/'); ?>images/twit.png"> </a> </li>
          <li> <a href="<?php echo $value->googleplus; ?>"> <img src="<?php echo base_url('frontend_assets/'); ?>images/google-plus-icon.png"> </a> </li>
          <li> <a href="http://<?php echo $value->linkedin; ?>"> <img src="<?php echo base_url('frontend_assets/'); ?>images/youtube-icon.png"> </a> </li>
          <div class="clearfix"> </div>
        </ul>
        <div class="clearfix"> </div>
      </div>
    </div>
    <div class="side_routmap">
      <div class="headline2">
        <h4 class="map-route"><?php if($language3){ echo $language3[0]->request_free;}else{?>Request Free<?php }?></h4>
      </div>
      <div class="c_details" style="padding-bottom: 8px;"> 
        <!--<div id="date-popover" class="popover top a" style="cursor: pointer; display: block; margin-left: 33%; margin-top: -50px; width: 175px;">
              <div class="arrow">
              </div>
              <h3 class="popover-title" style="display: none;">
              </h3>
              <div id="date-popover-content" class="popover-content">
              </div>
            </div>-->
        <div id="my-calendar"> </div>
      </div>
    </div>
    <div class="side_routmap">
      <div class="headline2">
        <h4 class="map-route"><?php if($language3){ echo $language3[0]->facilites;}else{?>Facilities<?php }?></h4>
      </div>
      <div class="c_details">
        <ul class="tech_clari">
          <?php  foreach ($roomsonly as $facility) { 
?>
          <li>Wakeup calls<span class="pull-right">
            <?php  if($facility->wakeup_calls == 1){ echo "Yes";} else{ echo "No";} ?>
            </span> </li>
          <li>Bathronbs <span class="pull-right">
            <?php  if($facility->bathrobes == 1){ 
					echo "Yes";} else{ echo "No";} ?>
            </span> </li>
          <li>LCD TV <span class="pull-right">
            <?php  if($facility->lcd_tv == 1){
					echo "Yes";} else{ echo "No";} ?>
            </span> </li>
          <li>In Room Childcare <span class="pull-right">
            <?php  if($facility->inroom_childcare == 1){ 
					echo "Yes";} else{ echo "No";} ?>
            </span> </li>
          <li>House Keeping <span class="pull-right">
            <?php  if($facility->housekeeping == 1){ 
					echo "Yes";} else{ echo "No";} ?>
            </span> </li>
          <li>Pets <span class="pull-right">
            <?php  if($facility->pets == 1){
					echo "Yes";} else{ echo "No";} ?>
            </span> </li>
          <li>Elevator <span class="pull-right">
            <?php  if($facility->elevator == 1){
				 echo "Yes";} else{ echo "No";} ?>
            </span> </li>
          <li>Card <span class="pull-right">
            <?php  if($facility->card == 1){
					echo "Yes";} 
					else{ echo "No";
					} ?>
            </span> </li>
          <li>AC <span class="pull-right">
            <?php  if($facility->ac == 1){ 
					echo "Yes";} else{ echo "No";
					} ?>
            </span> </li>
          <li>Gym <span class="pull-right">
            <?php  if($facility->gym == 1){ 
					echo "Yes";
					} else{
					echo "No";
					} ?>
            </span> </li>
          <li>Spa <span class="pull-right">
            <?php  if($facility->spa == 1){
					echo "Yes";
					} else{
					echo "No";
					} ?>
            </span> </li>
          <li>Inside Parking <span class="pull-right">
            <?php if($facility->inside_parking == 1){
				echo "Yes";
				} else{ 
				echo "No";
				} ?>
            </span> </li>
          <?php  
				} 
				?>
        </ul>
      </div>
    </div>
    <div class="side_routmap">
      <div class="headline2">
        <h4 class="map-route"><?php if($language3){ echo $language3[0]->download_file;}else{?>Downloads Files PDF<?php }?></h4>
      </div>
      <div class="c_details">
        <ul class="tech_clari">
          <li>Plane (PDF) </li>
          <li>Information (PDF) </li>
        </ul>
      </div>
    </div>
  </div>
</div>
</div>
</div>
<div class="modal fade login_form" id="date-popover-content" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content login_div">
      <div class="modal-header boder_none" >
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="z-index: 9999;   position: relative; top: -29px;  left: 20px; background: #000;    opacity: 1;    border-radius: 50%"> <span class="glyphicon glyphicon-remove removed"aria-hidden="true" style="line-height:21px; font-size:16px !important; color: #f393bd;"> </span> </button>
      </div>
      <div class="modal-body">
        <div class=" karara">
          <div class="row">
            <div class="form-base">
              <header>
                <h1 class="pop-login"><?php if($language3){ echo $language3[0]->freequote;}else{?>FREE QUOTE <?php } ?></h1>
              </header>
              <form action="<?php echo base_url('website/submitquote');?>" method="post" id="free_quote">
                <div class="form-group">
                  <div class="col-md-6" style="padding-left:0px !important;">
                    <div class="required_field">
                      <label class="label_field required" for="contact_request_name"><?php if($language3){ echo $language3[0]->name;}else{?>Name<?php } ?></label>
                      <input class="text_field" id="contact_request_name" name="firstname" placeholder="<?php if($language3){ echo $language3[0]->name;}else{?>Firm<?php } ?>" required="required" type="text">
                    </div>
                  </div>
                  <div class="col-md-6" style="padding-right:0px !important;">
                    <div class="right">
                      <label class="label_field" for="contact_request_company"><?php if($language3){ echo $language3[0]->firm;}else{?>Firm<?php } ?></label>
                      <input class="text_field" id="contact_request_company" name="company" required="required" placeholder="<?php if($language3){ echo $language3[0]->firm;}else{?>Firm<?php } ?>" type="text">
                    </div>
                  </div>
                  <div class="clearfix"> </div>
                </div>
                <div class="form-group">
                  <div class="col-md-6" style="padding-left:0px !important;">
                    <div class="required_field">
                      <label class="label_field required" for="contact_request_name"></label><?php if($language){ echo $language[0]->email;}else{?>Email<?php } ?>
                      <input class="text_field" id="contact_request_name" name="email" required="required" placeholder="<?php if($language3){ echo $language[0]->email;}else{?>Email<?php } ?>" type="text">
                    </div>
                  </div>
                  <div class="col-md-6" style="padding-right:0px !important;">
                    <div class="right">
                      <label class="label_field required" for="contact_request_company"><?php if($language3){ echo $language3[0]->telephone;}else{?>Telephone <?php } ?></label>
                      <input class="text_field" id="contact_request_company" name="telephone" required="required" placeholder="0120234567" type="number">
                    </div>
                  </div>
                  <div class="clearfix"> </div>
                </div>
                <div class="form-group">
                  <div class="col-md-6" style="padding-left:0px !important;">
                    <div class="required_field">
                      <label class="label_field required" for="contact_request_name"><?php if($language3){ echo $language3[0]->occasion;}else{?>Occasion <?php } ?></label>
                      <select class="form-control" name="occassion" required="required">
                        <option value=""><?php if($language3){ echo $language3[0]->select;}else{?>Select<?php } ?></option>
                        <option value="Meeting_congress">Meeting & Congress </option>
                        <option value="Wedding">Wedding </option>
                        <option value="birthday">Birthday </option>
                      </select>
                    </div>
                  </div>
                  <div class="col-md-6" style="padding-right:0px !important;">
                    <div class="right">
                      <label class="label_field" for="contact_request_company"><?php if($language3){ echo $language3[0]->datetime;}else{?>Date/time<?php } ?></label>
                    
                      <input type="text"  class="text_field" placeholder="<?php if($language3){ echo $language3[0]->datetime;}else{?>Date/time<?php } ?>" id="contact_request_company_date" name="datentime" required="required">
                    </div>
                  </div>
                  <div class="clearfix"> </div>
                </div>
                <div class="form-group">
                  <div class="col-md-4 no-left-margin">
                    <div class="required_field">
                      <label class="label_field required" for="contact_request_number_of_persons"><?php if($language3){ echo $language3[0]->guests;}else{?>Guests<?php } ?></label>
                      <input class="text_field" id="contact_request_number_of_persons" name="numberguests" required="required"    placeholder="250" type="text">
                    </div>
                  </div>
                  <div class="col-md-4" style="padding:10px !important;">
                    <select name="sitstand" class="form-control text_field" style="margin-top: 15px;" required="required">
                      <option value=""><?php if($language3){ echo $language3[0]->select;}else{?>Select<?php } ?></option>
                      <option value="sitting">Sitting </option>
                      <option value="standing">Standing </option>
                    </select>
                  </div>
                  <div class="col-md-4 budget_field no-left-margin">
                    <div class="right">
                      <label class="label_field" for="contact_request_event_budget"><?php if($language3){ echo $language3[0]->budget;}else{?>Budget<?php } ?></label>
                      <input class="text_field" id="contact_request_event_budget" name="budget" required="required" placeholder="z.B. 5.000 €" type="text">
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-md-6  budget_col" style="padding: 0 !important;">
                    <input checked="checked" class="radio" id="contact_request_budget_type_event" required="required" name="event" type="radio" value="event" style="float: left;    margin-right: 10px;">
                    <label class="budget_type_event" for="contact_request_budget_type_event"> <span> </span><?php if($language3){ echo $language3[0]->eventgesamt;}else{?>Event Gesamt <?php } ?></label>
                  </div>
                  <div class="col-md-6">
                    <input class="radio" id="contact_request_budget_type_location" name="location" required="required" type="radio" value="location" style="float: left; margin-right: 10px;">
                    <label class="budget_type_location" for="contact_request_budget_type_location"> <span> </span><?php if($language3){ echo $language3[0]->location;}else{?>Location<?php } ?> </label>
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-md-12">
                    <label class="label_field" for="contact_request_message"><?php if($language3){ echo $language3[0]->detailspop;}else{?>Details<?php } ?></label>
                    <textarea class="textarea_field" id="contact_request_message" required="required" name="details" placeholder="Details">
                    </textarea>
                  </div>
                  <div class="clearfix"> </div>
                </div>
                <div class="">
                  <div class="col-md-9">
                    <div class="more_suggestions">
                      <input class="checkbox" id="contact_request_more_suggestions" name="suggestion" type="checkbox" value="1" style="float:left; margin-right:5px;">
                      <label for="contact_request_more_suggestions" id="c1"> <span> </span><?php if($language3){ echo $language3[0]->appropriate;}else{?>Beat me more appropriate provider before<?php } ?> </label>
                    </div>
                    <div class="newsletter">
                      <input checked="checked" class="checkbox" id="contact_request_newsletter" name="newsletter"  type="checkbox" value="1" style="float: left; margin-right: 5px;">
                      <label for="contact_request_newsletter" id="c2"> <span> </span><?php if($language3){ echo $language3[0]->news;}else{?>I want to receive exciting event news <?php } ?></label>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <?php 
					if(!empty($_SESSION)){
					?>
                    <input type="hidden" name="hidden" value="<?php  echo $_SESSION['email']; ?>" >
                    <?php 
                      } ?>
                    <button type="submit" class="btn primary pull-right custom-color-back"><?php if($language3){ echo $language3[0]->send;}else{?>Send<?php } ?> </button>
                  </div>
                  <div class="clearfix"> </div>
                </div>
                <div class="form-group">
                  <div class="trust_footer">
                    <div class="inner_trust">
                      <div class="col-md-4"> <span class="icon icon-ok"> </span> <span class="trust_text">Known from the manufacturer </span> </div>
                      <div class="col-md-4"> <span class="icon icon-ok"> </span> <span class="trust_text">100% free &amp; not binding </span> </div>
                      <div class="col-md-4"> <span class="icon icon-ok"> </span> <span class="trust_text">Personal attention from Event Experts </span> </div>
                    </div>
                  </div>
                  <div class="clearfix"> </div>
                </div>
              </form>
            </div>
          </div>
          <!-- Sign Up / Login Switch --> 
          <!--<div class="row">            <div class="account-switch text-center">              <p>Don't have an account? <a href="" class="custom-color"  data-toggle="modal" data-target="#Register_form">Sign up</a></p>            </div>          </div>--> 
        </div>
      </div>
    </div>
  </div>
</div>
<div class="modal fade login_form_detail" id="login_form_detail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content login_div">
      <div class="modal-header boder_none" >
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="z-index: 9999;
                        position: relative;
                        top: -29px;
                        left: 20px;
                        background: #000;
                        opacity: 1;
                        border-radius: 50%"><span class="glyphicon glyphicon-remove removed"aria-hidden="true" style="line-height:21px; font-size:16px !important; color: #f393bd;"></span></button>
      </div>
      <div class="modal-body">
        <div class=" karara">
          <div class="row">
            <div class="form-base">
              <header>
                <h1 class="pop-login">Users Login</h1>
              </header>
              <section>
                <form action="<?php echo site_url('website/detail/').$pid; ?>" method="post" id="login-form">
                  <div class="input-group login-username">
                    <div class="input-group-addon">Email</div>
                    <input type="text" placeholder="E-mail" name="email" required="required"
                                               id="login-username">
                    <span aria-hidden="true" id="status-username" class="status-icon"></span> </div>
                  <div class="input-group login-password">
                    <div class="input-group-addon">Password</div>
                    <input type="password" placeholder="Password" name="password" required="required" id="login-password">
                    <span aria-hidden="true" id="status-password" class="status-icon"></span> </div>
                  <div class="row section-action"> <a href="<?php echo site_url('website/forget_password'); ?>">Forget password?</a>
                    <div class="col-xs-12">
                      <button type="submit" class="btn primary pull-right custom-color-back">Login</button>
                      <a href="<?php echo site_url('website/view_user_signup'); ?>">Sign Up</a> </div>
                  </div>
                </form>
              </section>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
<?php include('footer.php');  ?>



<script src="http://codeorigin.jquery.com/jquery-1.10.2.min.js">
</script>
<script src="<?php echo base_url('frontend_assets/'); ?>js/bootstrap.min.js">
</script>
<script>
  $('#myCarousel').carousel({
    interval: 4000
  }
                           );
  var clickEvent = false;
  $('#myCarousel').on('click', '.nav a', function() {
    clickEvent = true;
    $('.nav li').removeClass('active');
    $(this).parent().addClass('active');
  }
                     ).on('slid.bs.carousel', function(e) {
    if (!clickEvent) {
      var count = $('.nav').children().length - 1;
      var current = $('.nav li.active');
      current.removeClass('active').next().addClass('active');
      var id = parseInt(current.data('slide-to'));
      if (count == id) {
        $('.nav li').first().addClass('active');
      }
    }
    clickEvent = false;
  }
                         );
</script>
<script src="<?php echo base_url('frontend_assets/'); ?>js/zabuto_calendar.min.js">
</script>
<script type="application/javascript">
  $(document).ready(function() {
    $("#date-popover").popover({
      html: true,
      trigger: "manual"
	  
    }
                              );
    $("#date-popover").hide();
    $("#date-popover").click(function(e) {
      $(this).hide();
    }
                              );
    $("#my-calendar").zabuto_calendar({

	
      action: function() {
        return myDateFunction(this.id, false);
      }
      ,
      action_nav: function() {
        return myNavFunction(this.id);
      }
      ,
      ajax: {
        url: "show_data.php?action=1",
        modal: true
      }
      ,
      legend: [{
        type: "text",
        label: "Special event",
        badge: "00"
      }
               , {
                 type: "block",
                 label: "Regular event"
               }
              ]
			  
    }
	
                                     );
  }
                   );
				   
					 var myVal = null; 
					  
  function myDateFunction(id, fromModal) {
    $("#date-popover").hide();
    if (fromModal) {
      $("#" + id + "_modal").modal("hide");
    }
    var date = $("#" + id).data("date");
    var hasEvent = $("#" + id).data("hasEvent");
    if (hasEvent && !fromModal) {
      return false;
    }
    //$("#date-popover-content").html('You clicked on date ' + date);
	 $('#date-popover-content').modal('show');
	
	myVal = $("#contact_request_company_date").val(date);
	
    $("#date-popover").show();
    return true;
  }
 
  function myNavFunction(id) {
    $("#date-popover").hide();
    var nav = $("#" + id).data("navigation");
    var to = $("#" + id).data("to");
    console.log('nav ' + nav + ' to: ' + to.month + '/' + to.year);
  }
</script>
<script src="<?php echo base_url('frontend_assets/'); ?>js/sharemelody.dev.js" type="text/javascript">
</script>
<script src="<?php echo base_url('frontend_assets/'); ?>js/share2melody.dev.js" type="text/javascript">
</script>
<script>
$("form[name='comment_form']").submit(function(e) {
var user_session ="<?php echo $_SESSION['uid']; ?>";
if(user_session==''){
  	e.preventDefault();
	$('#login_form_detail').modal('show');
  }
  
  });
  $('.start-review a').click(function(e){
  e.preventDefault();
  if($(this).children().hasClass("clickable")){
  	$(this).children().removeClass("clickable");
		$(this).children().addClass("unclickable");
  }else{
  $(this).children().removeClass("unclickable");
  $(this).children().addClass("clickable");
  }
 noOfstars = $('.clickable').length;
 $('input[name="stars"]').val(noOfstars);
  
})
</script>
<script>
$(document).ready(function(){
   $(".announce").click(function(){
     $("#cafeId").val($(this).data('id'));
     $('#createFormId').modal('show');
   });
});
</script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="/resources/demos/style.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
