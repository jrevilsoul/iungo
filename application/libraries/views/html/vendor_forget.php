
<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Vendor Registration Form</title>
	<link href="<?php echo base_url("css/bootstrap.min.css"); ?>" rel="stylesheet" type="text/css" />
</head>
<div class="container">
<div class="row">
	<div class="col-md-6 col-md-offset-3" style="margin-top:140px;">
		<?php echo $this->session->flashdata('verify_msg'); ?>
	</div>
</div>
<?php if($message){ ?>
<div class="alert alert-success"  align="center">
			<?php  echo $message; ?>
            </div>
<?php
}
?>
<div class="row">
	<div class="col-md-6 col-md-offset-3 " >
		<div class="panel panel-default" >
			<div class="panel-heading" align="center">
				<h4>forget password</h4>
			</div>
			<div class="panel-body">
				<?php //$attributes = array("name" => "registrationform");
				echo form_open("Website/sends_email", $attributes);?>
				<div class="form-group">
					<label for="name">Email Address</label>
					<input class="form-control" name="email" placeholder="Email Address" required="required" type="email" value="" />
					<span class="text-danger"><?php echo form_error('fname'); ?></span>
				</div>

				<div class="form-group">
					<button name="submit" type="submit" class="btn btn-success">Submit</button>
					<a class="btn btn-primary" href="<?php echo base_url('website/view_website');?>">Back</a>
					<!--<a href="<?php //echo site_url('website/view_website'); ?>">Already Registered! Login Here</a>-->
				</div>
				<?php echo form_close(); ?>
				<?php echo $this->session->flashdata('msg'); ?>
			</div>
		</div>
	</div>
</div>
</div>
</html>

<?php //include('footer.php');?>