<?php //include('header.php');?>

<!DOCTYPE html>

<html>

<head>

	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<title>Vendor Registration Form</title>

	<link href="<?php echo base_url("css/bootstrap.min.css"); ?>" rel="stylesheet" type="text/css" />


	</head>
<div class="container">
<div class="row">
	<div class="col-md-6 col-md-offset-3">
		<?php echo $this->session->flashdata('verify_msg'); ?>
	</div>
</div>

<div class="row">
	<div class="col-md-6 col-md-offset-3">
		<div class="panel panel-default" style="margin-top:55px;">
			<div class="panel-heading" align="center">
				<h4>User Registration</h4>
			</div>
			<div class="panel-body">
				<?php $attributes = array("name" => "registrationform");
				echo form_open("website/register", $attributes);?>
				<div class="form-group">
					<label for="name">First Name</label>
					<input class="form-control" name="fname" placeholder="Your First Name" type="text" value="" />
					<span class="text-danger"><?php echo form_error('fname'); ?></span>
				</div>

				<div class="form-group">
					<label for="name">Last Name</label>
					<input class="form-control" name="lname" placeholder="Last Name" type="text" value="" />
					<span class="text-danger"><?php echo form_error('lname'); ?></span>
				</div>
				
				<div class="form-group">
					<label for="email">Email ID</label>
					<input class="form-control" name="email" placeholder="Email-ID" type="text" value="" />
					<span class="text-danger"><?php  echo form_error('email'); ?></span>
				</div>
                
                <!--<div class="form-group">
					<label for="email">Phone Number</label>
					<input class="form-control" name="phone_number" placeholder="Phone" type="number" value="" />
					<span class="text-danger"><?php //echo form_error('phone'); ?></span>
				</div>-->

				<div class="form-group">
					<label for="subject">Password</label>
					<input class="form-control" name="password" placeholder="Password" type="password" />
					<span class="text-danger"><?php echo form_error('password'); ?></span>
				</div>

				<div class="form-group">
					<label for="subject">Confirm Password</label>
					<input class="form-control" name="cpassword" placeholder="Confirm Password" type="password" />
					<span class="text-danger"><?php echo form_error('cpassword'); ?></span>
				</div>

				<div class="form-group">
					<button name="submit" type="submit" class="btn btn-success">Signup</button>
					<button name="cancel" type="reset" class="btn btn-danger">Cancel</button>
					<a href="<?php echo site_url('website/view_website'); ?>">Already Registered! Login Here</a>
				</div>
				<?php echo form_close(); ?>
				<?php echo $this->session->flashdata('msg'); ?>
			</div>
		</div>
	</div>
</div>
</div>
</html>

<?php //include('footer.php');?>