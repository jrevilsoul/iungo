<?php //include('header.php');?>

<!DOCTYPE html>

<html>

<head>

	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<title>Change password</title>

	<link href="<?php echo base_url("css/bootstrap.min.css"); ?>" rel="stylesheet" type="text/css" />

</head>
<div class="container">
<div class="row">
	<div class="col-md-6 col-md-offset-3">
		<?php echo $this->session->flashdata('verify_msg'); ?>
	</div>
</div>

<div class="row">
	<div class="col-md-6 col-md-offset-3">
		<div class="panel panel-default" style="margin-top:55px;">
			<div class="panel-heading">
				<h4>Change password</h4>
			</div>
			<div class="panel-body">
				<?php $attributes = array("name" => "registrationform");
				echo form_open("website/vendor_change_password", $attributes);?>
				<div class="form-group">
					<label for="name">Current Password</label>
					<input class="form-control" name="oldpassword" placeholder="Current password" type="password" required="required" value="" />
					<span class="text-danger"><?php echo form_error('fname'); ?></span>
				</div>

           
				<div class="form-group">
					<label for="subject">New Password</label>
					<input class="form-control" name="newpassword" placeholder="New Password" type="password" required="required"/>
					<span class="text-danger"><?php echo form_error('password'); ?></span>
				</div>

				<div class="form-group">
					<label for="subject">Confirm Password</label>
					<input class="form-control" name="cpassword" placeholder="Confirm Password" type="password" required="required" />
					<span class="text-danger"><?php echo form_error('cpassword'); ?></span>
				</div>

				<div class="form-group">
					<button name="submit" type="submit" class="btn btn-default">Save</button>
					<button name="cancel" type="reset" class="btn btn-default">Cancel</button>
					<a href="<?php echo site_url('website/user_account'); ?>">Back</a>
				</div>
				<?php echo form_close(); ?>
				<?php echo $this->session->flashdata('msg'); ?>
			</div>
		</div>
	</div>
</div>
</div>
</html>

<?php //include('footer.php');?>