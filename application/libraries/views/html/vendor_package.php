<?php include('header.php'); 
?>

<style>
.review_heading{ color:#2aa09c; font-size:18px; font-weight:600; margin-top:10px; margin-bottom:10px; padding:15px;}
.star_rating_review i { color:#F90; font-size:16px;}
.delete a i { color:#060; font-size:22px;}
</style>
<?php //print_r($user);?>
<div class="col-md-12 col-sm-12 col-xs-12 search-section">
  <div class="container">
    <div class="row">
      <div class="bc">
<ul class="breadcrumb">
          <li> <a href="<?php echo site_url('website/view_website'); ?>" rel="" title="Eventlocation">Home
            <?php //echo $value->provider_name;?>
            </a> </li>
           <li><?php echo $user->vendor_fname.' '.$user->lname;?>
            <!--<a href=""  rel="" title="Eventlocation Duesseldorf"> 
              <?php //echo $this->breadcrumb->add('Tutorials', base_url().'tutorials');  ?>
            </a>--> 
          </li>
          <!--<li> <?php ///echo $qcat['0']->en_serviceTypeName;?> 
            <!--<a href=""  rel="" title="Eventlocation Duesseldorf"> 
              <?php //echo $this->breadcrumb->add('Tutorials', base_url().'tutorials');  ?>
            </a>
          </li>
          <li class="active"> <span class="pinkcolor-new"> <?php //echo $qcat['0']->provider_name;?>
            <?php //echo $this->breadcrumb->add('Spring Tutorial', base_url().'tutorials/spring-tutorials'); ?>
            </span> </li>--->
        </ul>
        </div>
        </div>
        </div>
        </div>
		
        <div class="search-section">
<div class="container">
    <div class="row">
        <div class="col-md-12">
        <h2 class="review_heading">Package</h2>
    <table class="table table-bordered table-condensed table-hover text-center">
    <tr class="table-condensed info">
    <td>S N.</td>
    <td>Plan Name</td>
    <td class="col-md-6" style="overflow:auto">product</td>
    <td>Description</td>
    <td>Duration</td>
	<td>price</td>
    <td>Action</td>
    </tr>
	<?php foreach($comment_data as $value_comment){?>
    <tr>
    <td><?php echo $value_comment->meb_id;?></td>
    <td><?php echo $value_comment->membership_plan;?></td>
    <td><?php echo $value_comment->product_name;?></td>
	<td><?php echo $value_comment->description;?></td>
	<td><?php echo $value_comment->duration;?></td>
	<td><?php echo $value_comment->price;?></td>
    <td><div class="delete"><a href="<?php //echo site_url('website/delvendorcomment/').$value_comment->id;?>"><i class="fa fa-trash" aria-hidden="true"></i> </a></div></td>
    </tr>
	<?php
	}
	?>
    </table> 
        <div class="clearfix"></div>
        </div>  
    </div>
</div>
  <div class="clearfix"></div>
</div>

<div class="modal fade login_form reply-sure" id="edit_profile" >
  <div class="modal-dialog" role="document">
    <div class="modal-content login_div">
      <div class="modal-header boder_none" >
       <button type="button"  data-dismiss="modal" class="close"><span class="glyphicon glyphicon-remove removed"aria-hidden="true"></span></button>
      </div>
      <div class="modal-body pa_4_x">
        <div class=" karara">
          <div class="row">
            <div class="form-base">
              <header>
                <h1 class="pop-login"><?php if($language){ echo $language[0]->editprofile;}else{?>Edit Profile<?php }?> </h1>
              </header>
              <section>
                <form id="" method="post" action="<?php echo site_url('website/editprofileuser/').$user->id;?>">
                
                <div class="form-group">
                  <input type="text" name="fname" id="fname" value="<?php echo $user->fname;?>">
                   </div>
                <div class="form-group">
                 <input type="text" name="lname" id="lname" value="<?php echo $user->lname;?>">
                 
                  </div>
                <div class="form-group">
                 <input type="text" name="email" id="email" value="<?php echo $user->email;?>">
                  </div>
                
               
                <div class="form-group">
                 <input type="submit" class="btn btn-primary" value="<?php if($language){ echo $language[0]->save;}else{?>Save<?php }?>">
                   </div>
                </form>
                 <?php echo $this->session->flashdata('msg'); ?>
                
                 </section>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="modal fade login_form reply-sure" id="Changepassword" >
  <div class="modal-dialog" role="document">
    <div class="modal-content login_div">
      <div class="modal-header boder_none" >
       <button type="button"  data-dismiss="modal" class="close"><span class="glyphicon glyphicon-remove removed"aria-hidden="true"></span></button>
      </div>
      <div class="modal-body pa_4_x">
        <div class=" karara">
          <div class="row">
            <div class="form-base">
              <header>
                <h1 class="pop-login"><?php if($language){ echo $language[0]->changepassword;}else{?>Change password<?php }?></h1>
              </header>
              <section>
                <?php $attributes = array("name" => "registrationform");
				echo form_open("website/change_password", $attributes);?>
				<div class="form-group">
					
					<input class="" name="oldpassword" placeholder="<?php if($language){ echo $language[0]->currentpassword;}else{?>Current password<?php }?>" type="password" required="required" value="" />
					<span class="text-danger"><?php echo form_error('fname'); ?></span>
				</div>

           
				<div class="form-group">
					
					<input class="" name="newpassword" placeholder="<?php if($language){ echo $language[0]->newpassword;}else{?>New password<?php }?>" type="password" required="required"/>
					<span class="text-danger"><?php echo form_error('password'); ?></span>
				</div>

				<div class="form-group">
					
					<input class="" name="cpassword" placeholder="<?php if($language){ echo $language[0]->confirmpassword;}else{?>Confirm password<?php }?>" type="password" required="required" />
					<span class="text-danger"><?php echo form_error('cpassword'); ?></span>
				</div>

				<div class="form-group">
					<button name="submit" type="submit" class="btn btn-primary"><?php if($language){ echo $language[0]->save;}else{?>Save<?php }?></button>
					<button name="cancel" type="reset" class="btn btn-primary"><?php if($language){ echo $language[0]->cancel;}else{?>Cancel<?php }?></button>
					
				</div>
				<?php echo form_close(); ?>
				<?php echo $this->session->flashdata('msg'); ?>
                
                 </section>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<?php include('footer.php'); ?>



<script src="http://codeorigin.jquery.com/jquery-1.10.2.min.js"></script>

<script src="<?php echo base_url('frontend_assets/'); ?>js/bootstrap.min.js"></script>

<script>

    $('#myCarousel').carousel({

        interval: 4000

    });



    var clickEvent = false;

    $('#myCarousel').on('click', '.nav a', function () {

        clickEvent = true;

        $('.nav li').removeClass('active');

        $(this).parent().addClass('active');

    }).on('slid.bs.carousel', function (e) {

        if (!clickEvent) {

            var count = $('.nav').children().length - 1;

            var current = $('.nav li.active');

            current.removeClass('active').next().addClass('active');

            var id = parseInt(current.data('slide-to'));

            if (count == id) {

                $('.nav li').first().addClass('active');

            }

        }

        clickEvent = false;

    });

</script>





<script src="<?php echo base_url('frontend_assets/'); ?>js/zabuto_calendar.min.js"></script>

<script type="application/javascript">

    $(document).ready(function () {

    $("#date-popover").popover({html: true, trigger: "manual"});

    $("#date-popover").hide();

    $("#date-popover").click(function (e) {

    $(this).hide();

    });



    $("#my-calendar").zabuto_calendar({

    action: function () {

    return myDateFunction(this.id, false);

    },

    action_nav: function () {

    return myNavFunction(this.id);

    },

    ajax: {

    url: "show_data.php?action=1",

    modal: true

    },

    legend: [

    {type: "text", label: "Special event", badge: "00"},

    {type: "block", label: "Regular event"}

    ]

    });

    });



    function myDateFunction(id, fromModal) {

    $("#date-popover").hide();

    if (fromModal) {

    $("#" + id + "_modal").modal("hide");

    }

    var date = $("#" + id).data("date");

    var hasEvent = $("#" + id).data("hasEvent");

    if (hasEvent && !fromModal) {

    return false;

    }

    $("#date-popover-content").html('You clicked on date ' + date);

    $("#date-popover").show();

    return true;

    }



    function myNavFunction(id) {

    $("#date-popover").hide();

    var nav = $("#" + id).data("navigation");

    var to = $("#" + id).data("to");

    console.log('nav ' + nav + ' to: ' + to.month + '/' + to.year);

    }

</script>





<script src="<?php echo base_url('frontend_assets/'); ?>js/sharemelody.dev.js" type="text/javascript"></script>

<script src="<?php echo base_url('frontend_assets/'); ?>js/share2melody.dev.js" type="text/javascript"></script>