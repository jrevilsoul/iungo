<?php  
include('header.php'); 
//print_r($_SESSION['id']);

?>
<!--=====================================================              
 MID CONTAINER SECTION START HERE-=============================================================-->
 <style>
 .unclickable{
 color:#939393;
 }
 .clickable{
 color:#fbd200;
 }
 .start-review a {color:#939393;}
 .start-review a:hover {color: #fbd200;}
 </style>
<div class="col-md-12 col-sm-12 col-xs-12 search-section">
  <div class="container">
    <div class="row">
      <div class="bc">
        <?php foreach($prov_id as $value){
		} 
		$new_id =  $prov_id['0']->id;
		$city   =  $prov_id['0']->city;
		$category = $prov_id['0']->category_provider;
		$room_provider = "SELECT * FROM `tbl_room` WHERE provider_id = '$new_id'";        
		$queryrooms = $this->db->query($room_provider);        
		$roomsonly = $queryrooms->result();  
		$rating = "SELECT * FROM `site_ratings` WHERE provider_id = 11";        
		$query = $this->db->query($rating);        
		$queryrating = $query->result();	
		foreach($queryrating as $rat){
		$ratings = $rat->review;
		}
	    $location  = "SELECT cityName,cityid,city FROM `tbl_city` INNER JOIN service_provider ON tbl_city.cityid = service_provider.id where tbl_city.cityid = $city";
		$query_location = $this->db->query($location);
		$que   = $query_location->result();
		 
	    $cat  = "SELECT tbl_serviceprovidertype.en_serviceTypeName,tbl_serviceprovidertype.id,
		service_provider.category_provider,service_provider.provider_name 
		FROM `tbl_serviceprovidertype` INNER JOIN service_provider 
		ON tbl_serviceprovidertype.id = service_provider.id 
		where service_provider.id = $category";
		$query_cat = $this->db->query($cat);
		$qcat   = $query_cat->result();
		
		
		
		?>
        <ul class="breadcrumb">
          <li>
            <a href="<?php echo site_url('website/view_website'); ?>" rel="" title="Eventlocation">Home
              <?php //echo $value->provider_name;?>
            </a>
          </li>
          <li>
            <?php echo $que['0']->cityName;?>
            <!--<a href=""  rel="" title="Eventlocation Duesseldorf"> 
              <?php //echo $this->breadcrumb->add('Tutorials', base_url().'tutorials');  ?>
            </a>-->
          </li>
          <li>
            <?php echo $qcat['0']->en_serviceTypeName;?>
            <!--<a href=""  rel="" title="Eventlocation Duesseldorf"> 
              <?php //echo $this->breadcrumb->add('Tutorials', base_url().'tutorials');  ?>
            </a>-->
          </li>
         
          <li class="active">
            <span class="pinkcolor-new">
            <?php echo $qcat['0']->provider_name;?>
              <?php //echo $this->breadcrumb->add('Spring Tutorial', base_url().'tutorials/spring-tutorials'); ?> 
            </span>
          </li>
        </ul>
      </div>
    </div>

    <div class="row">
      <div class="col-md-9">
        <div class="deail-disc">
          <h3 class="pinkcolor-new">
            <?php echo $value->provider_name;?>
            <span class="pull-right">
              <a href="" class="pinkcolor-new" style="font-size: 19px;">
                <i class="fa fa-heart" aria-hidden="true">
                </i>
              </a>
            </span>
          </h3>
          <span class="start-review">
            <?php
			for($i=1; $i<=$ratings; $i++){
			?>
            <a href="">
              <i class="fa fa-star" aria-hidden="true">
              </i>
            </a>     
            <?php		  
			}
			?>
          </span>      
          <span class="deatails-add pinkcolor-new">
            <?php echo $value->address;?>
          </span>      
        </div>
        <div class="content_item_gallery">
          <div id="myCarousel" class="carousel slide mid-gallery" data-ride="carousel">
            <!-- Wrapper for slides -->      
            <div class="carousel-inner">
              <div class="item active">
                <img src="<?php  echo base_url('uploads/') . $value->provider_image;?>">           
                <div class="carousel-caption">
                </div>
              </div>
              <!-- End Item -->
              <?php $files = "SELECT * FROM `files` WHERE provider_id = '$value->id'";               
				$query = $this->db->query($files);               
				$images = $query->result();               
				foreach($images as $data) { ?>         
              <div class="item">
                <img src="<?php echo base_url('uploads/files/') . $data->file_name; ?>">           
                <div class="carousel-caption">
                </div>
              </div>
              <!-- End Item -->    
              <?php } ?>        
              <!-- <div class="item">            
<img src="<?php //echo base_url('frontend_assets/'); ?>images/home_3.jpg">              <div class="carousel-caption">                 </div>           </div>    End Item                      <div class="item">             <img src="<?php //echo base_url('frontend_assets/'); ?>images/home_4.jpg">              <div class="carousel-caption">                      </div>           </div>  -->   
              <!-- End Item -->    
            </div>
            <!-- End Carousel Inner -->   
            <ul class="nav nav-pills nav-justified">
              <li data-target="#myCarousel" data-slide-to="0" class="active">
                <a href="#">
                  <img src="<?php echo base_url('uploads/').$value->provider_image;?>">
                </a>
              </li>
              <?php  $files = "SELECT * FROM `files` WHERE provider_id = '$value->id'";           
               $query = $this->db->query($files);           
			   $images = $query->result();               
               foreach($images as $data) { ?>          
              <li data-target="#myCarousel" data-slide-to="1">          
                <a href="#">
                  <img src="<?php echo base_url('uploads/files/') . $data->file_name; ?>">
                </a>
              </li>
              <?php } ?>        
            </ul>
          </div>
        </div>
        <div class="content_item details" style="margin-bottom: 0;">
          <div class="price_info">
            <div class="col-sm-10" style="padding-right:0px;">
              <span class="pm-vc-views">
                <strong style="color:#db5677;"><?php echo $increaseview;?>
                </strong>  
                <small>Views
                </small>
              </span>
            </div>
            <div class="col-sm-2">
              <div class="pull-right">
                <button class="btn btn-small border-radius0 btn-video" type="button" data-toggle="button" id="pm-vc-share">
                  <i class="fa fa-share" aria-hidden="true">
                  </i>Share
                </button>
              </div>
            </div>
            <div class="clearfix">
            </div>
            <div id="pm-vc-share-content" class="alert alert-well col-lg-12">
              <div class="row-fluid">
                <div class="col-sm-4 panel-1">
                  <div class="input-prepend">
                    <span class="add-on">URL
                    </span>
                    <input name="video_link" id="video_link" type="text" value="http://afghanshow.com//rahat-fateh-ali-khan-zaroori-tha_d71e4566f.html" class="input-medium form-control" onClick="SelectAll('video_link');">
                  </div>
                </div>
                <div class="col-sm-5 panel-2">
                  <button class="btn border-radius0 btn-video" type="button" id="pm-vc-embed">Embed
                  </button>                  
                  <button class="btn border-radius0 btn-video" type="button" data-toggle="button" id="pm-vc-email">
                    <i class="fa fa-envelope">
                    </i>Send to a friend
                  </button>                    
                </div>
                <div class="col-sm-3 panel-3">                    
                  <a href="https://www.facebook.com/sharer.php?u=http%3A%2F%2Fafghanshow.com%2F%2Frahat-fateh-ali-khan-zaroori-tha_d71e4566f.html&amp;t=Rahat+Fateh+Ali+Khan-+Zaroori+Tha" onClick="javascript:window.open(this.href,      '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;" rel="tooltip" title="Share on FaceBook">
                    <i class="pm-vc-sprite facebook-icon">
                    </i>
                  </a>                    
                  <a href="https://twitter.com/home?status=Watching%20Rahat+Fateh+Ali+Khan-+Zaroori+Tha%20on%20http%3A%2F%2Fafghanshow.com%2F%2Frahat-fateh-ali-khan-zaroori-tha_d71e4566f.html" onClick="javascript:window.open(this.href,      '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;" rel="tooltip" title="Share on Twitter">
                    <i class="pm-vc-sprite twitter-icon">
                    </i>
                  </a>                    
                  <a href="https://plus.google.com/share?url=http%3A%2F%2Fafghanshow.com%2F%2Frahat-fateh-ali-khan-zaroori-tha_d71e4566f.html" onClick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;" rel="tooltip" title="Share on Google+">
                    <i class="pm-vc-sprite google-icon">
                    </i>
                  </a>                      
                </div>
              </div>
              <div id="pm-vc-embed-content" class="col-sm-12">
                <hr>
                <textarea name="pm-embed-code" id="pm-embed-code" rows="3" class="col-sm-12 form-control" onClick="SelectAll('pm-embed-code');">
                  <iframe width="629" height="400" src="http://afghanshow.com//embed.php?vid=d71e4566f" frameborder="0" allowfullscreen seamless>
                  </iframe>  
                  <p>
                    <a href="http://afghanshow.com//rahat-fateh-ali-khan-zaroori-tha_d71e4566f.html" target="_blank">Rahat Fateh Ali Khan- Zaroori Tha
                    </a>
                  </p>
                </textarea>
              </div>
              <div id="pm-vc-email-content" class="col-sm-12">
                <hr>
                <div id="share-confirmation" class="hide well well-small">
                </div>
                <form name="sharetofriend" action="" method="POST" class="form-inline"
                      <input type="text" id="name" name="name" class="input-small inp-small form-control" value="" placeholder="Your name">                    
                <input type="text" id="email" name="email" class="input-small inp-small form-control" placeholder="Friend's Email">					  
                <input type="text" name="imagetext" class="input-small inp-small form-control" autocomplete="off" placeholder="Confirm">                   
                <button class="btn btn-small btn-link" onClick="document.getElementById('securimage-share').src = '' + Math.random(); return false;">
                  <i class="fa fa-refresh">
                  </i> 
                </button>                          
                <img src="<?php echo base_url('frontend_assets/'); ?>images/securimage_show.png"id="securimage-share" alt="">                         
                <button type="submit" name="Submit" class="btn btn-success">Send
                </button>
                </form>
            </div>
          </div>
        </div>
        <div class="clearfix">
        </div>
      </div>
      <div class="content_item details">
        <div class="company_name">
          <h1>
            <font>
              <font>Good Hohenholz  
              </font>
            </font>
            <span>
              <font>
                <font>
                  <?php echo $value->provider_name;?>
                </font>
              </font>
            </span>
          </h1>
        </div>
        <div class="price_info">
          <div class="headline2">
            <font>
              <font>Price from
              </font>
            </font>
          </div>
          <p>
            <font>
              <font>on request (€)
              </font>
            </font>
          </p>
        </div>
        <div class="clearfix">
        </div>
      </div>
      <div class="content_item_header col-sm-12" style="padding:0px;">
        <div class="headline2">
          <span class="">Location at a glance
          </span>
        </div>
        <div class="col-sm-4" style="padding-left:0px;">
          <div class="info_short_left">
            <p>
              <font class="goog-text-highlight">
                <?php echo $value->address;?>
              </font>
            </p>
          </div>
        </div>
      </div>
      <div class="metadisc col-sm-12" style="padding:0px;">
        <div class="headline2">
          <span class="">Description
          </span>
        </div>
        <p class="">
          <?php  echo $value->description; ?>
        </p>
      </div>
      <div class="col-sm-12 rm-deatils" style="padding:0px;">
        <div class="headline2">
          <span class="">Latest comments and Rate
          </span>
        </div>
        <div class="row">
          <div class="col-sm-3 mb20px text-center">
            <img src="<?php echo base_url('frontend_assets/'); ?>images/users.png" class="review-user"> 
            <p class=" " style="color: #979797;">User
            </p>
          </div>
          <?php 
		   $id = $value->id; 
		   $uid = $_SESSION['uid'];
		   $fname = $_SESSION['fname'];
		  ?>
          <form action="<?php echo site_url('website/comment/'). $id; ?>" method="post" name="comment_form">
            <input type="hidden" name="item_id" value="<?php echo $id; ?>">
            <input type="hidden" name="username" value="<?php echo $fname;?>" />
            <input type="hidden" name="user_id" value="<?php echo $_SESSION['uid'];?>" /> 
            <input type="hidden" name="stars" value="0" class="nostar" />
            <div class="col-sm-9" style="margin-top:8px">
              <textarea cols="90" rows="5" name="comment" class="from-control"  placeholder="Latest comments">
              </textarea>
              <div class="col-md-12">
               <span class="start-review">
              <a href=""><i class="fa fa-star" aria-hidden="true"></i></a>
              <a href=""><i class="fa fa-star" aria-hidden="true"></i></a>
              <a href=""><i class="fa fa-star" aria-hidden="true"></i></a>
              <a href=""><i class="fa fa-star" aria-hidden="true"></i></a>
              <a href=""><i class="fa fa-star" aria-hidden="true"></i></a>
 </span></div>
              <input type="submit"  name="submit" value="Comment" /> 
            </div>
          </form>
<div class="headline2">
</div>
 <?php 
 //print_r($results);    
foreach($results as $res){	?> 
 <div class="row">
 <div class="col-sm-3 mb20px text-center">
 <img src="<?php echo base_url('frontend_assets/'); ?>images/users.png" class="review-user">
 <p class=" " style="color: #979797;"><?php echo $res->username;?></p>
 </div>
 <div class="col-sm-9">
 <?php 
 $start = $res->star;
 for($i=0; $i<$start; $i++){
 ?>
<span class="start-review">	
      <a href=""><i class="fa fa-star clickable"  aria-hidden="true"></i></a>
 </span>
 <?php
 }
 ?>
      <p class="mb20px"><?php echo $res->comment;?></p>
      <p class="mb20px" style="color: #979797;"><?php echo $res->created_date;?></p>
 </div>
 </div>
 <?php
 }
 ?>
 

<div id="pagination">
<ul class="tsc_pagination">
<?php

?>
<!-- Show pagination links -->
<?php
echo $links; ?>
</ul>
</div>

</div>
</div>


      <div class="col-sm-12 rm-deatils" style="padding:0px;">
        <div class="headline2">
   <?php  
      $sitesetting = "SELECT * FROM `service_provider` JOIN tbl_city ON service_provider.city = tbl_city.cityid where tbl_city.cityid = $value->city ORDER BY RAND() LIMIT 3"; 
      $queryfour = $this->db->query($sitesetting);
      $qfour = $queryfour->result();                
   ?>
          <span class="">More Locations
          </span>
        </div>
        <div class="row">
          <?php foreach($qfour as $data){  ?>
          <div class="col-sm-4">
            <div class="loc-img">
              <img src="<?php echo base_url('uploads/') . $data->provider_image; ?>">
            </div>
            <div class="">
              <?php echo $data->address; ?>
            </div>
            <span class="pinkcolor-new">
              <?php echo $data->cityName; ?>
            </span>
          </div>
          <?php } ?>
        </div>
      </div>
    </div>
    <div class="col-md-3">
      <div class="row site-loc">
        <div class="headline2">
          <h4 class="">
            <a class="btn big-btn js-contact-fancybox js_track_contact_event" href="" name="button" rel="" title="" type="submit" data-toggle="modal" data-target="#free_quote">Show site contact &gt;
            </a>
          </h4>
        </div>
        <?php  $sitesetting = "SELECT * FROM `site_settings`";                
		       $queryfour = $this->db->query($sitesetting);                
		       $qfour = $queryfour->result();                
?>   
        <div class="text-center f-20px">
          <?php foreach ($qfour as $hello) { 
echo $hello->phone; }?>
        </div>
        <div class="show_explain pinkcolor-new text-center">Warum über Aroos24 anfragen?
        </div>
      </div>
      <div class="row">
        <div class="side_routmap">
          <div class="headline2" style="margin-bottom:0px;">
            <h4 class="map-route">Location
            </h4>
          </div>
          <?php
$zip = $value->zipcode;    
$url = "http://maps.google.com/maps/api/geocode/json?address=$zip&sensor=false&region=England";
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
$response = curl_exec($ch);
curl_close($ch);
$response = json_decode($response);
$lat = $response->results[0]->geometry->location->lat;
$long = $response->results[0]->geometry->location->lng;  
?>
          <div id="googleMap" style="width:100%;height:350px;">
          </div>
          <script src="https://code.jquery.com/jquery-1.9.1.min.js">
          </script>
          <script>
            function myMap() {
              var myLatLng = {
                lat: <?php echo json_encode($lat);
                ?>, lng: <?php echo json_encode($long);
                ?>};
              var map = new google.maps.Map(document.getElementById('googleMap'), {
                zoom: 11,
				mapTypeId: google.maps.MapTypeId.ROADMAP,
                center: myLatLng
              }
                                           );
              var marker = new google.maps.Marker({
                position: myLatLng,
                map: map,
                title: 'Hello World!'
              }
                                                 );
            }
          </script>
          <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDW7q2v55_UFRY3m1OHJw0Fhb08aXqwzzg&amp;libraries=places&callback=myMap">
          </script>
          <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js">
          </script>
          <script  src="<?php echo base_url('frontend_assets/');?>js/logger.js" >
          </script>
          <script   src="<?php echo base_url('frontend_assets/');?>js/yyy.js">
          </script>
        </div>
        <div class="side_routmap">
          <div class="headline2">
            <h4 class="map-route">Contact Details
            </h4>
          </div>
          <div class="c_details">
            <h3 class="headline3">
              <span>
                <font>
                  <?php  echo $value->contact_name; ?>
                </font>
              </span>
            </h3>
            <p>
              <span>
                <font>
                  <?php  echo $value->address; ?>
                </font>
              </span>
              <br>
              <span>
                <font>
                  <?php echo $value->zipcode; ?> 
                </font>
              </span>
              <span>
                <font>Dusseldorf
                </font>
              </span>
              <br>
              <br>
              <span>
                <font>
                  <?php echo $value->phone; ?>
                </font>
              </span>
            </p>
          </div>
        </div>
        <div class="side_routmap">
          <div class="headline2">
            <h4 class="map-route">Social Media
            </h4>
          </div>
          <div class="c_details">
            <ul class="social_media">
              <li>
                <a href="<?php echo $value->facebook; ?>">
                  <img src="<?php echo base_url('frontend_assets/'); ?>images/facebook.png">
                </a>
              </li>
              <li>
                <a href="<?php echo $value->twitter; ?>"> 
                  <img src="<?php echo base_url('frontend_assets/'); ?>images/twit.png">
                </a>
              </li>
              <li>
                <a href="<?php echo $value->googleplus; ?>">
                  <img src="<?php echo base_url('frontend_assets/'); ?>images/google-plus-icon.png">
                </a>
              </li>
              <li>
                <a href="http://<?php echo $value->linkedin; ?>">
                  <img src="<?php echo base_url('frontend_assets/'); ?>images/youtube-icon.png">
                </a>
              </li>
              <div class="clearfix">
              </div>
            </ul>
            <div class="clearfix">
            </div>
          </div>
        </div>
        <div class="side_routmap">
          <div class="headline2">
            <h4 class="map-route">Request Free
            </h4>
          </div>
          <div class="c_details" style="padding-bottom: 8px;">
            <div id="date-popover" class="popover top a" style="cursor: pointer; display: block; margin-left: 33%; margin-top: -50px; width: 175px;">
              <div class="arrow">
              </div>
              <h3 class="popover-title" style="display: none;">
              </h3>
              <div id="date-popover-content" class="popover-content">
              </div>
            </div>
            <div id="my-calendar">
            </div>
          </div>
        </div>
        <div class="side_routmap">
          <div class="headline2">
            <h4 class="map-route">Facilities
            </h4>
          </div>
          <div class="c_details">
            <ul class="tech_clari">
              <?php  foreach ($roomsonly as $facility) { 
?>
              <li>Wakeup calls
                <span class="pull-right">
                  <?php  if($facility->wakeup_calls == 1){ echo "Yes";} else{ echo "No";} ?>
                </span>
              </li>
              <li>Bathronbs
                <span class="pull-right">
                  <?php  if($facility->bathrobes == 1){ 
					echo "Yes";} else{ echo "No";} ?>
                </span>
              </li>
              <li>LCD TV
                <span class="pull-right">
                  <?php  if($facility->lcd_tv == 1){
					echo "Yes";} else{ echo "No";} ?>
                </span>
              </li>
              <li>In Room Childcare
                <span class="pull-right">
                  <?php  if($facility->inroom_childcare == 1){ 
					echo "Yes";} else{ echo "No";} ?>
                </span>
              </li>
              <li>House Keeping
                <span class="pull-right">
                  <?php  if($facility->housekeeping == 1){ 
					echo "Yes";} else{ echo "No";} ?>
                </span>
              </li>
              <li>Pets
                <span class="pull-right">
                  <?php  if($facility->pets == 1){
					echo "Yes";} else{ echo "No";} ?>
                </span>
              </li>
              <li>Elevator
                <span class="pull-right">
                  <?php  if($facility->elevator == 1){
				 echo "Yes";} else{ echo "No";} ?>
                </span>
              </li>
              <li>Card
                <span class="pull-right">
                  <?php  if($facility->card == 1){
					echo "Yes";} 
					else{ echo "No";
					} ?>
                </span>
              </li>
              <li>AC
                <span class="pull-right">
                  <?php  if($facility->ac == 1){ 
					echo "Yes";} else{ echo "No";
					} ?>
                </span>
              </li>
              <li>Gym
                <span class="pull-right">
                  <?php  if($facility->gym == 1){ 
					echo "Yes";
					} else{
					echo "No";
					} ?>
                </span>
              </li>
              <li>Spa
                <span class="pull-right">
                  <?php  if($facility->spa == 1){
					echo "Yes";
					} else{
					echo "No";
					} ?>
                </span>
              </li>
              <li>Inside Parking
                <span class="pull-right">
                  <?php if($facility->inside_parking == 1){
				echo "Yes";
				} else{ 
				echo "No";
				} ?>
                </span>
              </li>
              <?php  
				} 
				?>
            </ul>
          </div>
        </div>
        <div class="side_routmap">
          <div class="headline2">
            <h4 class="map-route"> Downloads Files PDF
            </h4>
          </div>
          <div class="c_details">
            <ul class="tech_clari">
              <li>Plane (PDF)
              </li>
              <li>Information (PDF)
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="modal fade login_form" id="free_quote" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content login_div">
      <div class="modal-header boder_none" >      
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="z-index: 9999;   position: relative; top: -29px;  left: 20px; background: #000;    opacity: 1;    border-radius: 50%">
          <span class="glyphicon glyphicon-remove removed"aria-hidden="true" style="line-height:21px; font-size:16px !important; color: #f393bd;">
          </span>
        </button>  
      </div>
      <div class="modal-body">
        <div class=" karara">
          <div class="row">
            <div class="form-base">
              <header>
                <h1 class="pop-login">FREE QUOTE
                </h1>
              </header>
              <form action="<?php echo base_url('website/submitquote');?>" method="post" id="free_quote">
                <div class="form-group">
                  <div class="col-md-6" style="padding-left:0px !important;">
                    <div class="required_field">
                      <label class="label_field required" for="contact_request_name">Name
                      </label>
                      <input class="text_field" id="contact_request_name" name="firstname" placeholder="Max Mustermann" type="text">
                    </div>
                  </div>
                  <div class="col-md-6" style="padding-right:0px !important;">
                    <div class="right">
                      <label class="label_field" for="contact_request_company">Firm
                      </label>
                      <input class="text_field" id="contact_request_company" name="company" placeholder="Mustermann GmbH" type="text">
                    </div>
                  </div>
                  <div class="clearfix">
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-md-6" style="padding-left:0px !important;">
                    <div class="required_field">
                      <label class="label_field required" for="contact_request_name">E-Mail
                      </label>
                      <input class="text_field" id="contact_request_name" name="email" placeholder="info@aroos.com" type="text">
                    </div>
                  </div>
                  <div class="col-md-6" style="padding-right:0px !important;">
                    <div class="right">
                      <label class="label_field required" for="contact_request_company">Telephone
                      </label>
                      <input class="text_field" id="contact_request_company" name="telephone" placeholder="0120234567" type="text">
                    </div>
                  </div>
                  <div class="clearfix">
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-md-6" style="padding-left:0px !important;">
                    <div class="required_field">
                      <label class="label_field required" for="contact_request_name">Occasion
                      </label>
                      <select class="form-control" name="occassion">
                        <option value="Meeting_congress">Meeting & Congress
                        </option>
                        <option value="Wedding">Wedding
                        </option>
                        <option value="birthday">Birthday
                        </option>
                      </select>
                    </div>
                  </div>
                  <div class="col-md-6" style="padding-right:0px !important;">
                    <div class="right">
                      <label class="label_field" for="contact_request_company">Date / time
                      </label>
                      <input class="text_field" id="contact_request_company_date" name="datentime" placeholder="0120234567" type="text">
                    </div>
                  </div>
                  <div class="clearfix">
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-md-4 no-left-margin">
                    <div class="required_field">
                      <label class="label_field required" for="contact_request_number_of_persons">Guests
                      </label>
                      <input class="text_field" id="contact_request_number_of_persons" name="numberguests" placeholder="250" type="text">
                    </div>
                  </div>
                  <div class="col-md-4" style="padding:10px !important;">
                    <select name="sitstand" class="form-control text_field" style="margin-top: 15px;">
                      <option value="sitting">Sitting
                      </option>
                      <option value="standing">Standing
                      </option>
                    </select>
                  </div>
                  <div class="col-md-4 budget_field no-left-margin">
                    <div class="right">
                      <label class="label_field" for="contact_request_event_budget">Budget
                      </label>
                      <input class="text_field" id="contact_request_event_budget" name="budget" placeholder="z.B. 5.000 €" type="text">
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-md-6  budget_col" style="padding: 0 !important;">
                    <input checked="checked" class="radio" id="contact_request_budget_type_event" name="event" type="radio" value="event" style="float: left;    margin-right: 10px;">
                    <label class="budget_type_event" for="contact_request_budget_type_event">
                      <span>
                      </span>Event Gesamt
                    </label>
                  </div>
                  <div class="col-md-6">
                    <input class="radio" id="contact_request_budget_type_location" name="location" type="radio" value="location" style="float: left; margin-right: 10px;">
                    <label class="budget_type_location" for="contact_request_budget_type_location">
                      <span>
                      </span>Location
                    </label>
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-md-12">
                    <label class="label_field" for="contact_request_message">Details
                    </label>
                    <textarea class="textarea_field" id="contact_request_message" name="details" placeholder="Details">
                    </textarea>
                  </div>
                  <div class="clearfix">
                  </div>
                </div>
                <div class="">
                  <div class="col-md-9">
                    <div class="more_suggestions">
                      <input class="checkbox" id="contact_request_more_suggestions" name="suggestion" type="checkbox" value="1" style="float:left; margin-right:5px;">
                      <label for="contact_request_more_suggestions" id="c1">
                        <span>
                        </span>Beat me more appropriate provider before
                      </label>
                    </div>
                    <div class="newsletter">
                      <input checked="checked" class="checkbox" id="contact_request_newsletter" name="newsletter" type="checkbox" value="1" style="float: left; margin-right: 5px;">
                      <label for="contact_request_newsletter" id="c2">
                        <span>
                        </span>I want to receive exciting event news
                      </label>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <?php 
					if(!empty($_SESSION)){
					?>
                    <input type="hidden" name="hidden" value="<?php  echo $_SESSION['email']; ?>" >
                    <?php 
                      } ?>
                    <button type="submit" class="btn primary pull-right custom-color-back">Send
                    </button>
                  </div>
                  <div class="clearfix">
                  </div>
                </div>
                <div class="form-group">
                  <div class="trust_footer">
                    <div class="inner_trust">
                      <div class="col-md-4">
                        <span class="icon icon-ok">
                        </span>
                        <span class="trust_text">Known from the manufacturer
                        </span>
                      </div>
                      <div class="col-md-4">
                        <span class="icon icon-ok">
                        </span>
                        <span class="trust_text">100% free &amp; not binding
                        </span>
                      </div>
                      <div class="col-md-4">
                        <span class="icon icon-ok">
                        </span> 
                        <span class="trust_text">Personal attention from Event Experts
                        </span>
                      </div>
                    </div>
                  </div>
                  <div class="clearfix">
                  </div>
                </div>
              </form>
            </div>
          </div>
          <!-- Sign Up / Login Switch -->          
          <!--<div class="row">            <div class="account-switch text-center">              <p>Don't have an account? <a href="" class="custom-color"  data-toggle="modal" data-target="#Register_form">Sign up</a></p>            </div>          </div>-->        
        </div>
      </div>
    </div>
  </div>
</div>
<div class="modal fade login_form_detail" id="login_form_detail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content login_div">
            <div class="modal-header boder_none" >
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="z-index: 9999;
                        position: relative;
                        top: -29px;
                        left: 20px;
                        background: #000;
                        opacity: 1;
                        border-radius: 50%"><span class="glyphicon glyphicon-remove removed"aria-hidden="true" style="line-height:21px; font-size:16px !important; color: #f393bd;"></span></button>
            </div>
            <div class="modal-body">
                <div class=" karara">
                    <div class="row">
                        <div class="form-base">
                            <header>

                            	<h1 class="pop-login">Users Login</h1>
                                </header>
                                <section>
                             
                                <form action="<?php echo site_url('login_users/detail/').$pid; ?>" method="post" id="login-form">
                                    <div class="input-group login-username">
                                        <div class="input-group-addon">Email</div>
                                        <input type="text" placeholder="E-mail" name="email" required="required"
                                               id="login-username">
                                        <span aria-hidden="true" id="status-username" class="status-icon"></span>
                                    </div>
                                    <div class="input-group login-password">
                                        <div class="input-group-addon">Password</div>
                                        <input type="password" placeholder="Password" name="password" required="required" id="login-password">
                                        <span aria-hidden="true" id="status-password" class="status-icon"></span>
                                    </div>
                                    <div class="row section-action">
                                    <a href="<?php echo site_url('website/forget_password'); ?>">Forgotten password?</a>
                                        <div class="col-xs-12">
                                            <button type="submit" class="btn primary pull-right custom-color-back">Login</button>
                                            <a href="<?php echo site_url('website/view_user_signup'); ?>">Sign Up</a>
                                        </div>
                                    </div>
                                </form>
                            </section>             
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<?php include('footer.php');  ?>
<script src="http://codeorigin.jquery.com/jquery-1.10.2.min.js">
</script>
<script src="<?php echo base_url('frontend_assets/'); ?>js/bootstrap.min.js">
</script>
<script>
  $('#myCarousel').carousel({
    interval: 4000
  }
                           );
  var clickEvent = false;
  $('#myCarousel').on('click', '.nav a', function() {
    clickEvent = true;
    $('.nav li').removeClass('active');
    $(this).parent().addClass('active');
  }
                     ).on('slid.bs.carousel', function(e) {
    if (!clickEvent) {
      var count = $('.nav').children().length - 1;
      var current = $('.nav li.active');
      current.removeClass('active').next().addClass('active');
      var id = parseInt(current.data('slide-to'));
      if (count == id) {
        $('.nav li').first().addClass('active');
      }
    }
    clickEvent = false;
  }
                         );
</script>
<script src="<?php echo base_url('frontend_assets/'); ?>js/zabuto_calendar.min.js">
</script>
<script type="application/javascript">
  $(document).ready(function() {
    $("#date-popover").popover({
      html: true,
      trigger: "manual"
    }
                              );
    $("#date-popover").hide();
    $("#date-popover").click(function(e) {
      $(this).hide();
    }
                            );
    $("#my-calendar").zabuto_calendar({
      action: function() {
        return myDateFunction(this.id, false);
      }
      ,
      action_nav: function() {
        return myNavFunction(this.id);
      }
      ,
      ajax: {
        url: "show_data.php?action=1",
        modal: true
      }
      ,
      legend: [{
        type: "text",
        label: "Special event",
        badge: "00"
      }
               , {
                 type: "block",
                 label: "Regular event"
               }
              ]
    }
                                     );
  }
                   );
  function myDateFunction(id, fromModal) {
    $("#date-popover").hide();
    if (fromModal) {
      $("#" + id + "_modal").modal("hide");
    }
    var date = $("#" + id).data("date");
    var hasEvent = $("#" + id).data("hasEvent");
    if (hasEvent && !fromModal) {
      return false;
    }
    $("#date-popover-content").html('You clicked on date ' + date);
	//debugging
	//$("#contact_request_company_date").attr("placeholder",date);
	$("#contact_request_company_date").val(date);
	
    $("#date-popover").show();
    return true;
  }
  function myNavFunction(id) {
    $("#date-popover").hide();
    var nav = $("#" + id).data("navigation");
    var to = $("#" + id).data("to");
    console.log('nav ' + nav + ' to: ' + to.month + '/' + to.year);
  }
</script>
<script src="<?php echo base_url('frontend_assets/'); ?>js/sharemelody.dev.js" type="text/javascript">
</script>
<script src="<?php echo base_url('frontend_assets/'); ?>js/share2melody.dev.js" type="text/javascript">
</script>

<script>
$("form[name='comment_form']").submit(function(e) {
var user_session ="<?php echo $_SESSION['uid']; ?>";
if(user_session==''){
  	e.preventDefault();
	$('#login_form_detail').modal('show');
	//data-toggle="modal" data-target="#login_form" target="data-toggle="modal"
  }//else{
  	//$(this).submit();
  //}
  });
  $('.start-review a').click(function(e){
  e.preventDefault();
  if($(this).children().hasClass("clickable")){
  	$(this).children().removeClass("clickable");
		$(this).children().addClass("unclickable");
  }else{
  $(this).children().removeClass("unclickable");
  $(this).children().addClass("clickable");
  }
 noOfstars = $('.clickable').length;
 $('input[name="stars"]').val(noOfstars);
  
})
</script>
<script>
$(document).ready(function(){
   $(".announce").click(function(){ // Click to only happen on announce links
     $("#cafeId").val($(this).data('id'));
     $('#createFormId').modal('show');
   });
});
</script>
