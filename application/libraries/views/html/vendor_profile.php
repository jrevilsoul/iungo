<?php include('header.php');?>

<div class="container">
      <div class="row">
      <div class="col-md-5  toppad  pull-right col-md-offset-3 " style="margin-top:10px;">
      </div>
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xs-offset-0 col-sm-offset-0 col-md-offset-3 col-lg-offset-3 toppad" >
   
          <div class="panel panel-info">
            <div class="panel-heading">
              <h3 class="panel-title">Edit Vendor</h3>
            </div>
            <div class="panel-body">
              <div class="row">
                <div class=" col-md-9 col-lg-9 "> 
                <form id="" method="post" action="<?php echo site_url('website/editvendor/').$vendor->id;?>">
                <table class="table table-user-information">
                    <tbody>
                    <tr>
                        <th>First Name:</th>
                        <td><input type="text" name="fname" id="fname" value="<?php echo $vendor->vendor_fname;?>"></td>
                      </tr>
                      <tr>
                        <th>Last Name:</th>
                        <td><input type="text" name="lname" id="lname" value="<?php echo $vendor->lname;?>"></td>
                      </tr>
                      <tr>
                        <th>Email Address:</th>
                        <td><input type="text" name="email" id="email" value="<?php echo $vendor->vendor_email;?>"></td>
                      </tr>
                      <tr>
                    </tbody>
                  </table>
				  
                </div>
              </div>
            </div>
              <div class="panel-footer">
             <a data-original-title="Broadcast Message" data-toggle="tooltip" type="button" class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-envelope"></i></a>
             <span class="pull-right">
             <input type="submit" class="btn btn-primary" value="Save">
               </span>
             </div>
             </form>
            <?php echo $this->session->flashdata('msg'); ?>
          </div>
        </div>
      </div>
    </div>

<?php include('footer.php');?>