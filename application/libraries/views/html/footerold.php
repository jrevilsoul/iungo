<script type="text/javascript">
function reset_verfication_code()
            {
                document.getElementById('verfification_code_img').src='<?php echo base_url(); ?>security_captcha/captcha.php?'+Math.random();
            }

</script>
<footer>
    <div id="footer" class="text-center col-md-12">
        <div class="container">
            <div class="row element-top-60 element-bottom-60 footer-columns-3">
                <div class="col-sm-2">
                    <h3 class="sidebar-header pinkcolor">Useful Links</h3>
                    <ul class="ft-menu">
                        <li><a href="<?php echo site_url('website/view_site_faq'); ?>">FAQs</a></li>
                        <li><a href="<?php echo site_url('website/view_site_aboutus'); ?>">About us</a></li>
                        <li><a href="<?php echo site_url('website/view_site_contact'); ?>">Contact us</a></li>
                        <li><a href="<?php echo site_url('website/view_privacy_policy'); ?>">Privacy Policy</a></li>
                        <li><a href="<?php echo site_url('website/view_site_termsofuse'); ?>">Terms & Conditions</a></li>
                        <li><a href="<?php echo site_url('website/view_disclaimer'); ?>">Disclaimer</a></li>  
                        <li><a href="<?php echo site_url('website/view_cancellation'); ?>">Cancellation Policy</a></li>
                        <li><a href="<?php echo site_url('website/view_career'); ?>">Career</a></li>            
                    </ul>
                </div>
                <div class="col-sm-3">
                    <h3 class="sidebar-header pinkcolor">Wedding Vendor</h3>
                    <ul class="ft-menu">
                        <?php
                        $onlyprovider = "SELECT * FROM `tbl_serviceProviderType` LIMIT 9";
                        $query = $this->db->query($onlyprovider);
                        $providers = $query->result();
                        foreach ($providers as $data):
                            ?>          
                            <li><a href="#"><?php echo $data->en_serviceTypeName; ?></a></li>
                        <?php endforeach; ?>
                    </ul>
                </div>
                <div class="col-sm-2">
                    <h3 class="sidebar-header pinkcolor">Cities</h3>
                    <ul class="ft-menu">
                        <?php
                        $onlycities = "SELECT * FROM `tbl_city` LIMIT 9";
                        $query = $this->db->query($onlycities);
                        $cities = $query->result();
                        foreach ($cities as $data):
                            ?> <li>
                                <a href="<?php echo site_url('website/searchby_city/') . $data->cityid; ?>"><?php echo $data->cityName; ?></a>
                            </li><?php endforeach; ?>
                    </ul>
                </div>
                <div class="col-sm-5">
                    <div class="contact-form">
                        <!--Header Title-->
                        <div class="header-text center">
                            <h3 class="sidebar-header pinkcolor">Contact Us</h3>
                        </div>
                        <!--Header Title-->
                        <form role="form" class="form-contact" action="website/php/Send_email.php" method="GET">
                            <div class="col-md-6 nospace">
                                <input type="text" class="form-control " placeholder="write your name" required="required" name="name">
                            </div>
                            <div class="col-md-6" style="padding-right:0px;">
                                <input type="email" class="form-control" placeholder="write your email" required="required" name="email">
                            </div>
                            <input type="text" class="form-control" placeholder="write your subject" required="required" name="name">
                            <textarea required="required" placeholder="your message" name="message"></textarea>
                            <div class="text-left col-md-12 nospace">
                               <?php /*?> <div class="col-md-6 nospace"> <span>Captcha </span><img src="<?php echo base_url('frontend_assets/'); ?>images/captcha.jpg" class="captcha-img"> </div><?php */?>
                               <div class="text-left col-md-12 nospace"> <span>Captcha </span>
                               <img src="<?php echo base_url(); ?>security_captcha/captcha.php" alt="verfification code" 
                               id="verfification_code_img" width="170" />
                               <a style="cursor:pointer;"><img src="<?php echo base_url(); ?>image/cref.gif" class="captcha-img" onclick="reset_verfication_code();" /></a>
                               &nbsp;&nbsp;<input type="text"  name="captcha" id="captcha" maxlength="8" /></div></div>
                               <br><br>

                                <div class="col-md-6" style="padding-right:0px;">
                                    <button type="submit" class="btn btn-primary btn-sm btn-block">Send Message</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="copyright-container" class="col-md-12">
        <!-- .container start -->
        <div class="container">
            <!-- .row start -->
            <div class="row">
                <!-- .col-md-6 start -->
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <p style="padding-top: 9px;">Copyright © 2016 <a href="" class="pinkcolor">Php Expert Technologies</a>. All rights reserved.</p>
                </div>
                <!-- .col-md-6 end -->
                <!-- .col-md-6 start -->
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div id="oxywidgetsocial-4" class="sidebar-widget  ">
                        <ul class="unstyled inline social-icons social-simple social-normal">

                            <?php
                            $queryone = "SELECT * FROM `site_socialmedia`";
                            $query = $this->db->query($queryone);
                            $site_socialmedia = $query->result();
                            foreach ($site_socialmedia as $value) {
                                ?>
                                <li><a  href="<?php echo $value->facebook; ?>"><i class="fa fa-facebook"></i></a></li>
                                <li><a  href="<?php echo $value->twitter; ?>"><i class="fa fa-twitter"></i></a></li>
                                <li><a  href="<?php echo $value->googleplus; ?>"><i class="fa fa-google-plus"></i></a></li>
                                <li><a  href="<?php echo $value->pinterest; ?>"><i class="fa fa-instagram"></i></a></li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
                <!-- .col-md-6 end -->
            </div>
            <!-- .row end -->
        </div>
        <!-- .container end -->
        <a href="#0" class="cd-top">Top</a></a> </div>
</footer>
<!--<script src="js/jquery.min.js" type="text/javascript"></script>-->
<!--<script src="js/bootstrap.min.js" type="text/javascript"></script>-->
<!--<script src="js/owl.carousel.js" type="text/javascript"></script>-->
<!--<script src="js/dev.min.js" type="text/javascript"></script>-->
</body>
</html>
<script src="<?php echo base_url('frontend_assets/'); ?>js/common_scripts_min.js">
</script>
<!-- For Mobile Menu-->
<!--<script src="http://www.jqueryscript.net/demo/Responsive-Accessible-Lightbox-Gallery-Plugin-For-jQuery-littlelightbox/src/jquery.littlelightbox.js"></script>
<script>
$('.lightbox').littleLightBox();
</script>-->
<div class="modal fade login_form" id="vendor_login" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content login_div">
            <div class="modal-header boder_none" >
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="z-index: 9999;
                        position: relative;
                        top: -29px;
                        left: 20px;
                        background: #000;
                        opacity: 1;
                        border-radius: 50%"><span class="glyphicon glyphicon-remove removed"aria-hidden="true" style="line-height:21px; font-size:16px !important; color: #f393bd;"></span></button>
            </div>
            <div class="modal-body">
                <div class=" karara">
                    <!-- Logo -->
                    <!-- Form Base -->
                    <div class="row">
                        <div class="form-base">
                            <header>
                                <h1 class="pop-login">Vendor Login</h1>
                            </header>
                            <!-- Form -->
                            <section>
                                <form action="<?php echo site_url('login_vendors/index'); ?>" method="post" id="vendor_login">
                                    <!-- Socials - delete this section if you don't want social connects -->
                                    <!-- /.Socials -->
                                    <div class="input-group login-username">
                                        <div class="input-group-addon">Vendor Email</div>
                                        <input type="text" placeholder="Vendor-Email" name="vendoremail" id="login-username">
                                        <span aria-hidden="true" id="status-username" class="status-icon"></span></div>
                                    <div class="input-group login-password">
                                        <div class="input-group-addon">Password</div>
                                        <input type="password" placeholder="Password" name="password" id="login-password">
                                        <span aria-hidden="true" id="status-password" class="status-icon"></span>
                                    </div>
                                    <div class="row section-action">
                                        <!-- Forgotten Password Trigger -->
                                        <div class="col-xs-12 form-group"><a class="forgotten-password-trigger custom-color"  data-toggle="modal" data-target="#Forrget_form">Forgotten password?</a></div>
                                        <!-- Submit -->
                                        <div class="col-xs-12">
                                            <button type="submit" class="btn primary pull-right custom-color-back">Login</button>
                                            <a href="<?php echo site_url('website/view_vendor_signup'); ?>">Sign Up</a>
                                        </div>
                                    </div>
                                </form>
                            </section>
                        </div>
                    </div>
                    <!-- Sign Up / Login Switch -->
                    <!--<div class="row">
                      <div class="account-switch text-center">
                        <p>Don't have an account? <a href="" class="custom-color"  data-toggle="modal" data-target="#Register_form">Sign up</a></p>
                      </div>
                    </div>-->
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade login_form" id="login_form" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content login_div">
            <div class="modal-header boder_none" >
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="z-index: 9999;
                        position: relative;
                        top: -29px;
                        left: 20px;
                        background: #000;
                        opacity: 1;
                        border-radius: 50%"><span class="glyphicon glyphicon-remove removed"aria-hidden="true" style="line-height:21px; font-size:16px !important; color: #f393bd;"></span></button>
            </div>
            <div class="modal-body">
                <div class=" karara">
                    <!-- Logo -->
                    <!-- Form Base -->
                    <div class="row">
                        <div class="form-base">
                            <header>

                                <h1 class="pop-login">User Login</h1>
                            </header>
                            <!-- Form -->
                            <section>
                                <form action="<?php echo site_url('login_users/index'); ?>" method="post" id="login-form">
                                    <!-- Socials - delete this section if you don't want social connects -->
                                    <!-- /.Socials -->
                                    <div class="input-group login-username">
                                        <div class="input-group-addon">Email</div>
                                        <input type="text" placeholder="E-mail" name="email" 
                                               id="login-username">
                                        <span aria-hidden="true" id="status-username" class="status-icon"></span>
                                    </div>
                                    <div class="input-group login-password">
                                        <div class="input-group-addon">Password</div>
                                        <input type="password" placeholder="Password" name="password" id="login-password">
                                        <span aria-hidden="true" id="status-password" class="status-icon"></span>
                                    </div>
                                    <div class="row section-action">
                                        <!-- Forgotten Password Trigger -->
                                        <div class="col-xs-12 form-group"><a class="forgotten-password-trigger custom-color" data-toggle="modal" data-target="#Forrget_form">Forgotten password?</a></div>
                                        <!-- Submit -->
                                        <div class="col-xs-12">
                                            <button type="submit" class="btn primary pull-right custom-color-back">Login</button>
                                            <a href="<?php echo site_url('website/view_user_signup'); ?>">Sign Up</a>
                                        </div>
                                    </div>
                                </form>
                            </section>             
                        </div>
                    </div>
                    <!-- Sign Up / Login Switch -->
                    <!--<div class="row">
                      <div class="account-switch text-center">
                        <p>Don't have an account? <a href="" class="custom-color"  data-toggle="modal" data-target="#Register_form">Sign up</a></p>
                      </div>
                    </div>-->
                </div>
            </div>
        </div>
    </div>
</div>
<script src="http://codeorigin.jquery.com/jquery-1.10.2.min.js"></script>
<script src="<?php echo base_url('frontend_assets/'); ?>js/bootstrap.min.js"></script>
<script>
    $('#myCarousel').carousel({
        interval: 4000
    });

    var clickEvent = false;
    $('#myCarousel').on('click', '.nav a', function () {
        clickEvent = true;
        $('.nav li').removeClass('active');
        $(this).parent().addClass('active');
    }).on('slid.bs.carousel', function (e) {
        if (!clickEvent) {
            var count = $('.nav').children().length - 1;
            var current = $('.nav li.active');
            current.removeClass('active').next().addClass('active');
            var id = parseInt(current.data('slide-to'));
            if (count == id) {
                $('.nav li').first().addClass('active');
            }
        }
        clickEvent = false;
    });
    $(document).ready(function () {
        $(".alert-success,.alert-danger").delay(3000).slideUp('slow');
    });
</script>
<script src="js/zabuto_calendar.min.js"></script>
<script type="application/javascript">
    $(document).ready(function () {
    $("#date-popover").popover({html: true, trigger: "manual"});
    $("#date-popover").hide();
    $("#date-popover").click(function (e) {
    $(this).hide();
    });

    $("#my-calendar").zabuto_calendar({
    action: function () {
    return myDateFunction(this.id, false);
    },
    action_nav: function () {
    return myNavFunction(this.id);
    },
    ajax: {
    url: "show_data.php?action=1",
    modal: true
    },
    legend: [
    {type: "text", label: "Special event", badge: "00"},
    {type: "block", label: "Regular event"}
    ]
    });
    });

    function myDateFunction(id, fromModal) {
    $("#date-popover").hide();
    if (fromModal) {
    $("#" + id + "_modal").modal("hide");
    }
    var date = $("#" + id).data("date");
    var hasEvent = $("#" + id).data("hasEvent");
    if (hasEvent && !fromModal) {
    return false;
    }
    $("#date-popover-content").html('You clicked on date ' + date);
    $("#date-popover").show();
    return true;
    }

    function myNavFunction(id) {
    $("#date-popover").hide();
    var nav = $("#" + id).data("navigation");
    var to = $("#" + id).data("to");
    console.log('nav ' + nav + ' to: ' + to.month + '/' + to.year);
    }
</script>
<script src="<?php echo base_url('frontend_assets/'); ?>js/sharemelody.dev.js" type="text/javascript"></script>
<script src="<?php echo base_url('frontend_assets/'); ?>js/share2melody.dev.js" type="text/javascript"></script>
<style type="text/css">
    .panel-heading .accordion-toggle:after {
        font-family: 'FontAwesome';
        content: "\f067";
        float: right;
        color: #f388a3;
        background: #db5677;
        padding:3px 8px;
    }
    .panel-heading .accordion-toggle.collapsed:after {
        /* symbol for "collapsed" panels */
        content: "\f068";    /* adjust as needed, taken from bootstrap.css */
    }

</style>
