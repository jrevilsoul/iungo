<?php  ?>
<script type="text/javascript">

function reset_verfication()
            {
                document.getElementById('verfification_code').src='<?php echo base_url(); ?>security_captcha/captcha.php?'+Math.random();
            }
</script>
<style type="text/css">
#up_1 {
    background:#279794;
    background-position: -2329px 0;
    display: inline-block;
    vertical-align: middle;
    position: fixed;
    right: 30px; bottom: 200px;
    width: 45px;
    height: 45px;
    cursor: pointer;
    opacity: 0;
    -webkit-transition: all 0.5s ease-in-out;
		-moz-transition: all 0.5s ease-in-out;
        -ms-transition: all 0.5s ease-in-out;
        -o-transition: all 0.5s ease-in-out;
		transition: all 0.5s ease-in-out; line-height:45px;    text-align: center; border-radius:5px; border-bottom:solid 5px #044c4a;
}
#up_1 i{ font-size:25px; font-weight:bold;  color:#fff;    line-height: 41px;}
#up_1.scrolled{
    opacity:1;
   bottom: 200px; z-index:9999;
}
</style>


<div class="clearfix"></div>

<footer>
  <div id="footer">
    <div class="container">
      <div class="row element-top-60 element-bottom-60 footer-columns-3">
        <div class="col-xs-12 col-sm-6 col-md-3">
          <h3 class="sidebar-header pinkcolor"><?php if($language2){ echo $language2[0]->usefullink;}else{?>Useful Links<?php } ?></h3>
          <ul class="ft-menu">
            <li><a href="<?php echo site_url('website/view_site_faq'); ?>" target=""><?php if($language2){ echo $language2[0]->faq;}else{?>FAQ<?php } ?></a></li>
            <li><a href="<?php echo site_url('website/view_site_aboutus'); ?>"target=""><?php if($language2){ echo $language2[0]->AboutUs;}else{?>About us<?php } ?></a></li>
            <li><a href="<?php echo site_url('website/view_site_contact'); ?>"target=""><?php if($language2){ echo $language2[0]->ContactUs;}else{?>Contact us<?php } ?></a></li>
            <li><a href="<?php echo site_url('website/view_privacy_policy'); ?>"target=""><?php if($language2){ echo $language2[0]->privacyPolicy;}else{?>Privacy Policy<?php } ?></a></li>
            <li><a href="<?php echo site_url('website/view_site_termsofuse'); ?>"target=""><?php if($language2){ echo $language2[0]->term_condition;}else{?>Terms & Conditions<?php } ?></a></li>
            <li><a href="<?php echo site_url('website/view_disclaimer'); ?>"target=""><?php if($language2){ echo $language2[0]->disclaimer;}else{?>Disclaimer<?php } ?></a></li>
            <li><a href="<?php echo site_url('website/view_cancellation'); ?>"target=""><?php if($language2){ echo $language2[0]->cancellation_policy;}else{?>Cancellation Policy<?php } ?></a></li>
            <li><a href="<?php echo site_url('website/view_career'); ?>"target=""><?php if($language2){ echo $language2[0]->career;}else{?>Career<?php } ?></a></li>
            <li><a href="<?php echo site_url('website/view_dataprotection'); ?>"target=""><?php if($language2){ echo $language2[0]->Dataprotection;}else{?>Dataprotection<?php } ?></a></li>
            <li><a href="<?php echo site_url('website/view_services'); ?>"target=""><?php if($language2){ echo $language2[0]->service;}else{?>Service<?php } ?></a></li>
          </ul>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-3">
          <h3 class="sidebar-header pinkcolor"><?php if($language2){ echo $language2[0]->providers;}else{?>Providers<?php } ?></h3>
          <ul class="ft-menu">
            <?php
                        $onlyprovider = "SELECT * FROM `tbl_serviceprovidertype` LIMIT 9";
                        $query = $this->db->query($onlyprovider);
                        $providers = $query->result();
                        foreach ($providers as $data):
                            ?>
            <li><a href="<?php echo site_url('website/searchby_city/').$data->id; ?>"target=""><?php echo $data->en_serviceTypeName; ?></a></li>
            <?php endforeach; ?>
          </ul>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-2">
          <h3 class="sidebar-header pinkcolor"><?php if($language2){ echo $language2[0]->cities;}else{?>Cities<?php } ?></h3>
          <ul class="ft-menu">
            <?php
                        $onlycities = "SELECT * FROM `tbl_city` LIMIT 9";
                        $query = $this->db->query($onlycities);
                        $cities = $query->result();
                        foreach ($cities as $data):
                            ?>
            <li> <a href="<?php echo site_url('website/searchby_city/') . $data->cityid ?>"target=""><?php echo $data->cityName; ?></a> </li>
            <?php endforeach; ?>
          </ul>
        </div>
        <div class="col-xs-12 col-sm-8 col-md-4">
          <div class="contact-form" > 
            <!--<div class="alert alert-msg">

                            <!--<span style="color:red;text-align:center;"><?php //echo $message; ?></span>

                         </div>--> 
            
            <!--Header Title-->
            <div class="header-text center">
              <h3 class="sidebar-header pinkcolor"><?php if($language2){ echo $language2[0]->ContactUs;}else{?>Contact Us<?php } ?></h3>
            </div>
            <!--Header Title-->
            <?php
	  if($this->session->flashdata('message')) {
		$message = $this->session->flashdata('message');
		?>
            <div class="alert alert-success"> <?php echo $this->session->flashdata('message'); ?> </div>
            <?php	
	  }
	  ?>
            <form action="<?php echo site_url('Email/send_mail'); ?>" method="post">
              <div class="col-md-6 nospace">
                <input type="text" class="form-control " placeholder="<?php if($language2){ echo $language2[0]->name;}else{?>write your name<?php } ?>" required="required" name="name">
              </div>
              <div class="col-md-6 no_pa" style="padding-right:0px;">
                <input type="email" class="form-control" placeholder="<?php if($language2){ echo $language2[0]->email;}else{?>write your email<?php } ?>" required="required" name="email">
              </div>
              <input type="text" class="form-control" placeholder="<?php if($language2){ echo $language2[0]->subject;}else{?>write your subject<?php } ?>" required="required" name="subject">
              <textarea required="required" placeholder="<?php if($language2){ echo $language2[0]->message;}else{?>write your your message<?php } ?>" name="message"></textarea>
              <div class="text-left col-md-12 nospace">
                <?php ?>
                <div class="col-md-6 nospace"> </div>
          
              
                <div class="text-left col-md-12 nospace"> <img src="<?php echo base_url(); ?>security_captcha/captcha.php" alt="verfification code" 

                               id="verfification_code" width="170" style="margin-bottom:10px !important;"/> 
                  
                 
                  
                  <a class="btn btn-small btn-link" onclick="reset_verfication();" style="   margin-top: -7px;

                                font-size: 24px !important;"><i class="fa fa-refresh"></i> </a>
                  <div class="clearfix"></div>
                </div>
                <input type="text" name="captcha" value="" size="40" class="form-control" aria-required="true" aria-invalid="false" placeholder="<?php if($language2){ echo $language2[0]->captcha;}else{?>captcha<?php } ?>" required="required">
                <div class="" style="padding-right:0px;">
                  <button type="submit" class="btn btn-primary btn-sm btn-block"><?php if($language2){ echo $language2[0]-> 	sendMessage;}else{?>Send Message<?php } ?></button>
                </div>
              </div>
            </form>
            <?php echo form_close(); ?>
            <div class="clearfix"></div>
          </div>
          <div class="clearfix"></div>
        </div>
      </div>
    </div>
  </div>
  
  <div id="copyright-container" class=""> 
    <!-- .container start -->
    <div class="container"> 
      <!-- .row start -->
      <div class="row"> 
        <!-- .col-md-6 start -->
        <div class="col-md-6 col-sm-8 col-xs-12">
          <p style="padding-top: 9px;"><?php if($language2){ echo $language2[0]->copyright;}else{?> Copyright © 2017 <?php } ?><a href="" class="pinkcolor"> Php Expert Technologies</a> <?php if($language2){ echo $language2[0]->all_right_reserved;}else{?> All rights reserved<?php } ?> .</p>
        </div>
        <!-- .col-md-6 end --> 
        <!-- .col-md-6 start -->
        <div class="col-md-6 col-sm-4 col-xs-12">
          <div id="oxywidgetsocial-4" class="sidebar-widget  ">
            <ul class="unstyled inline social-icons social-simple social-normal">
              <?php
                            $queryone = "SELECT * FROM `site_socialmedia`";
                            $query = $this->db->query($queryone);
                            $site_socialmedia = $query->result();
                            foreach ($site_socialmedia as $value) {
                                ?>
              <li><a  href="<?php echo $value->facebook; ?>"><i class="fa fa-facebook"></i></a></li>
              <li><a  href="<?php echo $value->twitter; ?>"><i class="fa fa-twitter"></i></a></li>
              <li><a  href="<?php echo $value->googleplus; ?>"><i class="fa fa-google-plus"></i></a></li>
              <li><a  href="<?php echo $value->pinterest; ?>"><i class="fa fa-instagram"></i></a></li>
              <?php } ?>
            </ul>
          </div>
        </div>
        <!-- .col-md-6 end --> 
      </div>
      <!-- .row end --> 
    </div>
    <!-- .container end --> 
    <a href="#0" class="cd-top">Top</a></a> </div>
</footer>
<div class="clearfix"></div>

<div class="modal fade login_form reply-sure" id="vendor_login">
  <div class="modal-dialog" role="document">
    <div class="modal-content login_div">
      <div class="modal-header boder_none" >
        <button type="button" data-dismiss="modal" class="close"><span class="glyphicon glyphicon-remove removed"aria-hidden="true"></span></button>
      </div>
      <div class="modal-body pa_4_x">
        <div class=" karara"> 
          <!-- Logo --> 
          <!-- Form Base -->
          <div class="row">
            <div class="form-base">
              <header>
                <h1 class="pop-login"><?php if($language){ echo $language[0]->vendorlogin;}else{?>Vendor Login<?php } ?></h1>
              </header>
              <!-- Form -->
              <section>
                <form action="<?php echo site_url('website/vendor'); ?>" method="post" id="vendor_login">
                  <!-- Socials - delete this section if you don't want social connects --> 
                  <!-- /.Socials -->
                  <div class="input-group login-username">
                    <div class="input-group-addon">Vendor Email</div>
                    <input type="text" placeholder="<?php if($language){ echo $language[0]->vendoremail;}else{?>Vendor-Email<?php } ?>" name="email" required="required" id="login-username">
                    <span aria-hidden="true" id="status-username" class="status-icon"></span></div>
                  <div class="input-group login-password">
                    <div class="input-group-addon"></div>
                    <input type="password" placeholder="<?php if($language){ echo $language[0]->password;}else{?>Password<?php } ?>" name="password" required="required" id="login-password">
                    <span aria-hidden="true" id="status-password" class="status-icon"></span> </div>
                  <div class="row section-action"> <a href="" data-toggle="modal" data-target="#Forget_password_vendor" ><?php if($language){ echo $language[0]->forgetpassword;}else{?>Forget password?<?php } ?></a>
                    <div class="col-xs-12">
                      <input type="submit" value="Login" class="btn primary pull-right custom-color-back full_width ma_top_bottom"><?php if($language){ echo $language[0]->login;}else{?><?php } ?></button>
                      <a  data-toggle="modal" data-target="#vendor_registaion" class="btn primary pull-right custom-color-green full_width"><?php if($language){ echo $language[0]->signup;}else{?>Sign Up<?php } ?></a> </div>
                  </div>
                </form>
              </section>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
function myFunction() {
    alert("The form was submitted");
}
</script>
<div class="modal fade login_form reply-sure" id="login_form" >
  <div class="modal-dialog" role="document">
    <div class="modal-content login_div">
      <div class="modal-header boder_none" >
        <button type="button" data-dismiss="modal" class="close"><span class="glyphicon glyphicon-remove removed"aria-hidden="true"></span></button>
      </div>
      <div class="modal-body pa_4_x">
        <div class=" karara">
          <div class="row">
            <div class="form-base">
              <header>
                <h1 class="pop-login"><?php if($language){ echo $language[0]->userlogin;}else{?>Users Login<?php } ?></h1>
              </header>
              <section>
                <form action="<?php echo site_url('website/userpage'); ?>" method="post" id="login-form">
                  <div class="input-group login-username">
                    <div class="input-group-addon"><?php if($language){ echo $language[0]->email;}else{?>Email<?php } ?></div>
                    <input type="text" placeholder="E-mail" name="email" required="required"
                                               id="login-username">
                    <span aria-hidden="true" id="status-username" class="status-icon"></span> </div>
                  <div class="input-group login-password">
                    <div class="input-group-addon"></div>
                    <input type="password" required="required" placeholder="<?php if($language){ echo $language[0]->password;}else{?>Password<?php } ?>" name="password" id="login-password">
                    <span aria-hidden="true" id="status-password" class="status-icon"></span> </div>
                  <div class="row section-action"><a data-toggle="modal" data-target="#Forget_password"><?php if($language){ echo $language[0]->forgetpassword;}else{?>Forget password?<?php } ?></a>
                    <div class="col-xs-12">
                      <input type="submit" value="Login" class="btn primary pull-right custom-color-back full_width ma_top_bottom"><?php if($language){ echo $language[0]->login;}else{?><?php } ?></button>
                      <a data-toggle="modal" data-target="#user_registaion" class="btn primary pull-right custom-color-green full_width"><?php if($language){ echo $language[0]->signup;}else{?>Sign Up<?php } ?></a> </div>
                  </div>
                </form>
              </section>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script>
$(function(){

$('#registrationform').submit(function(event){
	alert('hello');
event.preventDefault();
var custemail = $('#email_id').val();
var custname = $('#name').val();

$.ajax({
        type: 'POST',
        url: your controller path,
        data: {
         'name': custname,
         'email': custemail

        },
        dataType: 'html',
        success: function(results){
             if(something not as you expected){
              $('.error_msg').html('error msg');
              return false;
             }
        }
  });


});

});
</script>
<!------------signup Form user ------------------------------------------------>
<div class="modal fade login_form reply-sure" id="user_registaion" >
  <div class="modal-dialog" role="document">
    <div class="modal-content login_div">
      <div class="modal-header boder_none" >
       <button type="button"  data-dismiss="modal" class="close"><span class="glyphicon glyphicon-remove removed"aria-hidden="true"></span></button>
      </div>
      <div class="modal-body pa_4_x">
        <div class=" karara">
          <div class="row">
            <div class="form-base">
              <header>
                <h1 class="pop-login"><?php if($language){ echo $language[0]->userregistration;}else{?>User Registration<?php } ?></h1>
              </header>
			 
              <section>
                <?php $attributes = array("name" => "registrationform");
				echo form_open("website/register", $attributes);?>
                <div class="form-group">
                  <input class="" name="fname" required="required" placeholder="<?php if($language){ echo $language[0]->firstname;}else{?>Your First Name<?php } ?>" type="text" value="" re />
                  <span class="text-danger"><?php echo form_error('fname'); ?></span> </div>
                <div class="form-group">
                  <input class="" name="lname" required="required" placeholder="<?php if($language){ echo $language[0]->lastname;}else{?>Your Last Name<?php } ?>" type="text" value="" />
                  <span class="text-danger"><?php echo form_error('lname'); ?></span> </div>
                <div class="form-group">
                  <input class="" name="email"required="required" placeholder="<?php if($language){ echo $language[0]->email;}else{?>Email-ID<?php } ?>" type="text" value="" />
                  <span class="text-danger">
                  <?php  echo form_error('email'); ?>
                  </span> </div>
               
                <div class="form-group">
                  <input class="" name="password" required="required" placeholder="<?php if($language){ echo $language[0]->password;}else{?>Password<?php } ?>" type="password" />
                  <span class="text-danger"><?php echo form_error('password'); ?></span> </div>
                <div class="form-group">
                  <input class="" name="cpassword" required="required" placeholder="<?php if($language){ echo $language[0]->confirmpassword;}else{?>Confirm Password<?php } ?>" type="password" />
                  <span class="text-danger"><?php echo form_error('cpassword'); ?></span></div>
                <div class="form-group">
                  <input name="submit" type="submit" value="Signup" class="btn btn-success"><?php if($language){ echo $language[0]->signup;}else{?><?php } ?></button>
                  <button name="cancel" type="reset" class="btn btn-danger"><?php if($language){ echo $language[0]->cancel;}else{?>Cancel<?php } ?></button>
                  <a href="<?php echo site_url('website/view_website'); ?>">Already Registered! Login Here</a> </div>
                <?php echo form_close(); ?> <?php echo $this->session->flashdata('msg'); ?> </section>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-----------------------------------------------------End Form----------------------------------> 
<!------------------------------------vendor siup Form---------------------------------------------->
<div class="modal fade login_form reply-sure" id="vendor_registaion" >
  <div class="modal-dialog" role="document">
    <div class="modal-content login_div">
      <div class="modal-header boder_none" >
       <button type="button" data-dismiss="modal" class="close"><span class="glyphicon glyphicon-remove removed"aria-hidden="true"></span></button>
      </div>
      <div class="modal-body pa_4_x">
        <div class=" karara">
          <div class="row">
            <div class="form-base">
              <header>
                <h1 class="pop-login"><?php if($language){ echo $language[0]->vendorregistration;}else{?>Vendor Registration<?php } ?></h1>
              </header>
              <section>
                <?php $attributes = array("name" => "registrationform");

				echo form_open("website/vender_register", $attributes);?>
                <div class="form-group">
                  <input class="" name="fname" placeholder="<?php if($language){ echo $language[0]->firstname;}else{?>Your First Name<?php } ?>" type="text" required="required" value="<?php echo set_value('fname'); ?>" />
                  <span class="text-danger"><?php echo form_error('fname'); ?></span> </div>
                <div class="form-group">
                  <input class="" required="required" name="lname" placeholder="<?php if($language){ echo $language[0]->lastname;}else{?>Your Last Name<?php } ?>" type="text" value="<?php echo set_value('lname'); ?>" />
                  <span class="text-danger"><?php echo form_error('lname'); ?></span> </div>
                <div class="form-group">
                  <input class="" name="email" placeholder="<?php if($language){ echo $language[0]->email;}else{?>Email-ID<?php } ?>Email-ID" type="text" required="required" value="<?php echo set_value('email'); ?>" />
                  <span class="text-danger"><?php echo form_error('email'); ?></span> </div>
                <div class="form-group">
                  <input class="" name="password" required="required" placeholder="<?php if($language){ echo $language[0]->password;}else{?>Password<?php } ?>" type="password" />
                  <span class="text-danger"><?php echo form_error('password'); ?></span> </div>
			<!--<div class="container">
  
        <h1>Choose Membership<span class="pull-right label label-default">:)</span></h1>
   
    <div class="row">
    	<div class="col-md-6">
            <div class="panel with-nav-tabs panel-default">
                <div class="panel-heading">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#tab1default" data-toggle="tab">Standard</a></li>
                            <li><a href="#tab2default" data-toggle="tab">Basic</a></li>
                            <li><a href="#tab3default" data-toggle="tab">Middle</a></li>
                           
                        </ul>
                </div>
                <div class="panel-body">
                    <div class="tab-content">
                        <div class="tab-pane fade in active" id="tab1default">Price 5000</div>
						 <div class="tab-pane fade in active" id="tab1default"></div>
                        <div class="tab-pane fade" id="tab2default">Default 2</div>
                        <div class="tab-pane fade" id="tab3default">Default 3</div>
                    </div>
                </div>
            </div>
        </div>
		</div>
		</div>-->
                <div class="form-group">
                  <input class="" name="cpassword" required="required" placeholder="<?php if($language){ echo $language[0]->confirmpassword;}else{?>Confirm Password<?php } ?>" type="password" />
                  <span class="text-danger"><?php echo form_error('cpassword'); ?></span> </div>
                <div class="form-group">
                  <label for="subject">Select Category</label>
                  <?php  $files = "SELECT * FROM `tbl_serviceprovidertype`";

			               $query = $this->db->query($files);
			               $type_cat = $query->result(); ?>
                  <select name="provider_type_id" class="form-control" required="required" />
                  
                  <option value=""><?php if($language){ echo $language[0]->selectcategory;}else{?>SELECT CATEGORY<?php } ?></option>
                  <?php foreach($type_cat as $data){ ?>
                  <option value="<?php echo $data->id; ?>"><?php echo $data->en_serviceTypeName; ?></option>
                  <?php } ?>
                  </select>
                  <span class="text-danger"></span> </div>
                <div class="form-group">
                  <input name="submit" value="Signup" type="submit" class="btn btn-success"><?php if($language){ echo $language[0]->signup;}else{?><?php } ?></button>
                  <button name="cancel" type="reset" class="btn btn-danger"><?php if($language){ echo $language[0]->cancel;}else{?>Cancel<?php } ?></button>
                  <a href="<?php echo site_url('website/view_website'); ?>">Already Registered! Login Here</a> </div>
                <?php echo form_close(); ?> <?php echo $this->session->flashdata('msg'); ?> </section>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-------------End Password Form-------------------------------------------------------> 

<!------------Forget password Form ------------------------------------------------>
<div class="modal fade login_form reply-sure" id="Forget_password" >
  <div class="modal-dialog" role="document">
    <div class="modal-content login_div">
      <div class="modal-header boder_none" >
        <button type="button"  data-dismiss="modal" class="close"><span class="glyphicon glyphicon-remove removed"aria-hidden="true"></span></button>
      </div>
      <div class="modal-body pa_4_x">
        <div class=" karara">
          <div class="row">
            <div class="form-base">
              <header>
                <h1 class="pop-login"><?php if($language){ echo $language[0]->forgetpassword;}else{?>Forget password<?php } ?></h1>
              </header>
              <section>
              <div class="row">
	<div class="col-md-12">
		<?php echo $this->session->flashdata('message'); ?>
	</div>
</div>
<?php if($message){ ?>
<div class="alert alert-success"  align="center">
			<?php  echo $message; ?>
            </div>
<?php
}
?>
              
               <?php $attributes = array("name" => "registrationform");
				echo form_open("Website/send_email", $attributes);?>
                <div class="form-group">
					
					<input class="f" name="email" placeholder="Email Address" required="required" type="email" value="" />
					<span class="text-danger"><?php echo form_error('fname'); ?></span>
				</div>
				<div class="form-group">
					<button name="submit" type="submit" class="btn btn-success"><?php if($language){ echo $language[0]->submit;}else{?>Submit<?php } ?></button>
					<a class="btn btn-primary" href="<?php echo base_url('website/view_website');?>"><?php if($language){ echo $language[0]->back;}else{?>Back<?php } ?></a>
					</div>
				<?php echo form_close(); ?>
				
				<?php echo $this->session->flashdata('msg');?>
              </section>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-----------------------------------------------------End Form---------------------------------->

<!------------Forget_password_vendor Form ------------------------------------------------>
<div class="modal fade login_form reply-sure" id="Forget_password_vendor" >
  <div class="modal-dialog" role="document">
    <div class="modal-content login_div">
      <div class="modal-header boder_none" >
        <button type="button"  data-dismiss="modal" class="close"><span class="glyphicon glyphicon-remove removed" aria-hidden="true"></span></button>
      </div>
      <div class="modal-body pa_4_x">
        <div class=" karara">
          <div class="row">
            <div class="form-base">
              <header>
                <h1 class="pop-login"><?php if($language){ echo $language[0]->forgetpassword;}else{?>Forget password<?php } ?></h1>
              </header>
              <section>
              <div class="row">
	<div class="col-md-12">
		<?php echo $this->session->flashdata('verify_msg'); ?>
	</div>
</div>
<?php if($message){ ?>
<div class="alert alert-success"  align="center">
			<?php  echo $message; ?>
            </div>
<?php
}
?>

               <?php //$attributes = array("name" => "registrationform");
				echo form_open("Website/sends_email", $attributes);?>
				<div class="form-group">
					
					<input class="" name="email" placeholder="Email Address" required="required" type="email" value="" />
					<span class="text-danger"><?php echo form_error('fname'); ?></span>
				</div>

				<div class="form-group">
					<button name="submit" type="submit" class="btn btn-success"><?php if($language){ echo $language[0]->submit;}else{?>Submit<?php } ?></button>
					<a class="btn btn-primary" href="<?php echo base_url('website/view_website');?>"><?php if($language){ echo $language[0]->back;}else{?>Back<?php } ?></a>
					<!--<a href="<?php //echo site_url('website/view_website'); ?>">Already Registered! Login Here</a>-->
				</div>
				<?php echo form_close(); ?>
				<?php echo $this->session->flashdata('msg'); ?>
              </section>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div id="up_1"><i class="fa fa-arrow-up" aria-hidden="true"></i></div>
<script>
jQuery(window).scroll(function(){
    
    var fromTopPx = 400; // distance to trigger
    
    var scrolledFromtop = jQuery(window).scrollTop();
    if(scrolledFromtop > fromTopPx){
        jQuery('#up_1').addClass('scrolled');
    }else{
        jQuery('#up_1').removeClass('scrolled');
    }
});
jQuery('#up_1').on('click',function(){
    jQuery("html, body").animate({ scrollTop: 0 }, 600);
    return false;
});
</script>
<!-----------------------------------------------------End Form---------------------------------->


<script type="text/javascript">
$(document).ready(function() {
    $('button').on('click', function() {
        $('.modal').modal('hide');
    });
});
</script>


<!--<script src="js/jquery.min.js" type="text/javascript"></script>--> 
<!--<script src="js/bootstrap.min.js" type="text/javascript"></script>--> 
<!--<script src="js/owl.carousel.js" type="text/javascript"></script>--> 
<!--<script src="js/dev.min.js" type="text/javascript"></script>-->
</body></html><!--<script src="<?php echo base_url('frontend_assets/'); ?>js/common_scripts_min.js">-->
</script>
<!-- For Mobile Menu-->
<!--<script src="http://www.jqueryscript.net/demo/Responsive-Accessible-Lightbox-Gallery-Plugin-For-jQuery-littlelightbox/src/jquery.littlelightbox.js"></script>
<script>
$('.lightbox').littleLightBox();
</script>-->

<!--<script src="http://codeorigin.jquery.com/jquery-1.10.2.min.js"></script>-->
<!--<script src="<?php echo base_url('frontend_assets/'); ?>js/bootstrap.min.js"></script>-->
<!--<script>
    $('#myCarousel').carousel({
        interval: 4000
    });

    var clickEvent = false;
    $('#myCarousel').on('click', '.nav a', function () {
        clickEvent = true;
        $('.nav li').removeClass('active');
        $(this).parent().addClass('active');
    }).on('slid.bs.carousel', function (e) {
        if (!clickEvent) {
            var count = $('.nav').children().length - 1;
            var current = $('.nav li.active');
            current.removeClass('active').next().addClass('active');
            var id = parseInt(current.data('slide-to'));
            if (count == id) {
                $('.nav li').first().addClass('active');
            }
        }
        clickEvent = false;
    });
    $(document).ready(function () {
        $(".alert-success,.alert-danger").delay(3000).slideUp('slow');
    });
</script>
<!--<script src="js/zabuto_calendar.min.js"></script>-->
<!--<script type="application/javascript">
    $(document).ready(function () {
    $("#date-popover").popover({html: true, trigger: "manual"});
    $("#date-popover").hide();
    $("#date-popover").click(function (e) {
    $(this).hide();
    });

    $("#my-calendar").zabuto_calendar({
    action: function () {
    return myDateFunction(this.id, false);
    },
    action_nav: function () {
    return myNavFunction(this.id);
    },
    ajax: {
    url: "show_data.php?action=1",
    modal: true
    },
    legend: [
    {type: "text", label: "Special event", badge: "00"},
    {type: "block", label: "Regular event"}
    ]
    });
    });

    function myDateFunction(id, fromModal) {
    $("#date-popover").hide();
    if (fromModal) {
    $("#" + id + "_modal").modal("hide");
    }
    var date = $("#" + id).data("date");
    var hasEvent = $("#" + id).data("hasEvent");
    if (hasEvent && !fromModal) {
    return false;
    }
    $("#date-popover-content").html('You clicked on date ' + date);
    $("#date-popover").show();
    return true;
    }

    function myNavFunction(id) {
    $("#date-popover").hide();
    var nav = $("#" + id).data("navigation");
    var to = $("#" + id).data("to");
    console.log('nav ' + nav + ' to: ' + to.month + '/' + to.year);
    }
</script>-->
<!--<script src="<?php echo base_url('frontend_assets/'); ?>js/sharemelody.dev.js" type="text/javascript"></script>
<script src="<?php echo base_url('frontend_assets/'); ?>js/share2melody.dev.js" type="text/javascript"></script>-->
<style type="text/css">
.panel-heading .accordion-toggle:after {
	font-family: 'FontAwesome';
	content: "\f067";
	float: right;
	color: #f388a3;
	background: #db5677;
	padding:3px 8px;
}
.panel-heading .accordion-toggle.collapsed:after {
	/* symbol for "collapsed" panels */
        content: "\f068";    /* adjust as needed, taken from bootstrap.css */
}


</style>
