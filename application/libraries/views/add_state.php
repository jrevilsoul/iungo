<!DOCTYPE html><head>

<?php include('headerandsidebar.php'); ?>
<div class="row">
  <div class="container" id="content">
    <div class="panel panel-default">
      <div class="panel-heading">Add State</div>
	  <?php if($this->session->flashdata('insertmessage')){?>
				  <div class="alert alert-success" align="center">
				  <?php echo $this->session->flashdata('insertmessage'); ?>
					</div>
				  <?php
				  }elseif($this->session->flashdata('updatemessage')){
				  ?>
				  <div class="alert alert-success" align="center">
				  <?php echo $this->session->flashdata('updatemessage'); ?>
					</div>
				  <?php
				  }
				  ?>
      
      <form class="add_button" action="#" method="post">

        <!-- <button type="submit" class="btn btn-success">
        <i class="glyphicon glyphicon-plus-sign"></i>Booking Edit Delete </button> -->
      </form>

      <div class="panel-body">   
              <div class="xcrud">
                <div class="xcrud-container">
                <div class="xcrud-ajax">
              <div class=" pull-right" style="float:right;">
               <input type="hidden" class="xcrud-data" name="key" value="017bfe2df5d71e29f34946466c178b21e4491ace" />
                <input type="hidden" class="xcrud-data" name="orderby" value="pt_cars.car_id" />
                <input type="hidden" class="xcrud-data" name="order" value="desc" />
                <input type="hidden" class="xcrud-data" name="start" value="0" />
                <input type="hidden" class="xcrud-data" name="limit" value="50" />
              <input type="hidden" class="xcrud-data" name="instance" value="786870d5a374d329252a31dbbc49e5d866817eb0" />
                <input type="hidden" class="xcrud-data" name="task" value="list" />
                </div> 
                <div class="xcrud-list-container">
                 <div id="container" class="container">
                 <form method="post" action="<?php echo site_url('Welcome/submit_state'); ?>" name="data_register">
                 <div class="col-md-12 col-sm-12 col-xs-12 form_admin_page">
      <div class="row form-group">
          <label class="col-md-2 control-label text-left"  for="Country">Status</label>
          <div class="col-md-10">
          <select class="form-control" name="status" required />
                <option value="1">Enable</option>
                <option value="0">Disable</option>
                </select>
        </div>
        </div>
        
        <div class="row form-group">
          <label class="col-md-2 control-label text-left"  for="Country">Country</label>
          <div class="col-md-10">
           <select class="form-control" name="country_id" required />
               <?php foreach($fetch as $data){ ?>
                <option value="<?php echo $data['id']; ?>"><?php echo $data['country']; ?></option>
                     <?php   }  ?>
                </select> 
        </div>
        </div>
        
      <div class="row form-group">
          <label class="col-md-2 control-label text-left" for="Country">State</label>
          <div class="col-md-10">
           <input type="text" align="center" class="form-control" name="state" required />      
        </div>
        </div>
      
      <div class="panel-footer">
         
           <button type="submit" class="btn btn-primary pull-right">Submit</button>
           <div class="clearfix"></div>
        </div>
        </div>
        </form>
             
                  </div>
                </div>
            </div>
            
    <?php include('footer.php'); ?>
</body>
</html>