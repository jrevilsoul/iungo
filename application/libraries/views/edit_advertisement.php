<?php include('headerandsidebar.php'); ?>

  <h3 class="margin-top-0">Add Advertisement</h3>
  
  <div class="output"></div>
  <div class="panel panel-default">
    <ul class="nav nav-tabs nav-justified" role="tablist">
      <li class="active"><a href="#GENERAL" data-toggle="tab">General</a></li>
      <!--  <li class=""><a href="#AMENITIES" data-toggle="tab">Amenities</a></li> -->
      <li class=""><a href="#TRANSLATE" data-toggle="tab">Translate</a></li>
    </ul>
    <div class="panel-body">
    <?php //echo $this->session->flashdata('msg'); ?>
     <?php
	 foreach($view_data as $data){
	   	 }
	 ?>
     <br>
      <div class="tab-content form-horizontal">
        <div class="tab-pane wow fadeIn animated active in" id="GENERAL" >
         <form method="POST" action="<?php echo site_url('Welcome/update_advertisement/').$data['id']; ?>" enctype="multipart/form-data">
            <div class="clearfix"></div>
            <div class="row form-group">
              <label class="col-md-2 control-label text-left">Status</label>
              <div class="col-md-2">
                <select class="form-control" name="status" id="status">
        
				
               <option value="<?php echo $data['id'];?>" <?php if($data['status']=="Active") echo selected;?>>Active</option>
               <option value="<?php echo $data['id'];?>" <?php if($data['status']=="Inactive") echo selected;?>>Inactive</option>
                  
                </select>
              </div>
            </div>
            <div class="row form-group">
              <label class="col-md-2 control-label text-left">Url Link</label>
              <div class="col-md-4">
                <input type="text"  class="form-control" placeholder="url" name="url" required="" value="<?php echo $data['url'];?>">
              </div>
            </div>
            <div class="row form-group">
              <label class="col-md-2 control-label text-left">Image</label>
              <div class="col-md-4">
                <input type="file" name="pic" value="" /><img src="<?php  echo base_url('banners/' . $data['image']);?>" height="80" width="80">
              </div>
            </div>
          </div>
        </div>
        <div class="panel-footer">
          <input class="btn btn-primary" type="submit" value="Submit">
        </div>
      </div>
    </div>
  </div>
</form>
</div>
<?php include('footer.php'); ?>
</body>
</html>