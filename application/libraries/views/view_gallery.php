<?php include('headerandsidebar.php'); ?>
<div class="row">
  <div class="container" id="content">
    <div class="panel panel-default">
      <div class="panel-heading">View Gallery Pictures</div>
      
      <form class="add_button" action="#" method="post">

        <!-- <button type="submit" class="btn btn-success">
        <i class="glyphicon glyphicon-plus-sign"></i>Booking Edit Delete</button> -->
      </form>
      <div class="panel-body">        
        <div class="xcrud">
          <div class="xcrud-container">
        
            <div class="xcrud-ajax">
            <div class=" pull-right" style="float:right;">
              <input type="hidden" class="xcrud-data" name="key" value="017bfe2df5d71e29f34946466c178b21e4491ace" />
              <input type="hidden" class="xcrud-data" name="orderby" value="pt_cars.car_id" />
              <input type="hidden" class="xcrud-data" name="order" value="desc" />
            <input type="hidden" class="xcrud-data" name="start" value="0" />
            <input type="hidden" class="xcrud-data" name="limit" value="50" />
          <input type="hidden" class="xcrud-data" name="instance" value="786870d5a374d329252a31dbbc49e5d866817eb0" />
          <input type="hidden" class="xcrud-data" name="task" value="list" />
            </div>
              <div class="xcrud-top-actions margin-bottom-30">
                <div class="btn-group pull-right"><a href="javascript: multiDelfunc('http://demo/cars/carajaxcalls/multiDelCars', 'checkboxcls')" class="btn btn-danger"><i class="glyphicon glyphicon-remove"></i>Delete Selected</a><a href="javascript:;" data-task="print" class="btn btn-default xcrud-in-new-window xcrud-action"><i class="glyphicon glyphicon-print"></i>Print</a><a href="javascript:;" data-task="csv" class="btn btn-default xcrud-in-new-window xcrud-action"><i class="glyphicon glyphicon-file"></i>Export into CSV</a>
                </div>
                <div class="clearfix"></div>
              </div>
              <div class="xcrud-list-container">
              <form action="<?php echo site_url('welcome/delete_bulkgallery'); ?>" method="post">
                <table class="xcrud-list table table-striped table-hover" id="myTable">
                  <thead>
                    <tr class="xcrud-th">
                      <th><input class="all" type='checkbox' value='' id="select_all" ></th>
                    <th class="xcrud-num">&#35;</th>
                <!--   <th data-order="desc" data-orderby="pt_cars.car_is_featured" class="xcrud-column xcrud-action"></th> -->
                      <!-- <th data-order="desc" data-orderby="pt_cars.thumbnail_image" class="xcrud-column xcrud-action">Image</th> -->
                      <th data-order="desc" data-orderby="pt_cars.car_title" class="xcrud-column xcrud-action">Gallery Images
                      </th>
					  <th data-order="desc" data-orderby="pt_cars.car_title" class="xcrud-column xcrud-action">Provider Name
                      </th>
					  <th data-order="desc" data-orderby="pt_cars.car_title" class="xcrud-column xcrud-action">Uploade Gallery
                      </th>
                      <th data-order="desc" data-orderby="pt_cars.car_title" class="xcrud-column xcrud-action">Total Uploaded
                      </th>
                      <!--<th data-order="desc" data-orderby="pt_cars.car_status" class="xcrud-column xcrud-action">Status</th>-->	
                      <th data-order="desc" data-orderby="pt_cars.car_delete">Actions</th>
                    </tr>
                  </thead>

                  <tbody>                  
                  <?php foreach($view_gallery as $data) { ?>
                  <tr class="xcrud-row xcrud-row-0">
                  <td><input class="checkboxcls" type="checkbox" name="boxes[]" value="<?php echo $data['id']; ?>" > </td>
                      <td class="xcrud-current xcrud-num"><?php echo $data['provider_id']; ?></td>
                     <!--  <td>
                     <span class="star fa fa-star fstar" style="cursor: pointer;" data-featured="no" id="8"></span>
                     </td> -->
                      <!-- <td class="zoom_img"><img alt="" src="<?php echo base_url(); ?>img/car/3.png" style="max-height: 20px;" /></td> -->
                      <td class="zoom_img"><img src="<?php  echo base_url('uploads/files/' . $data['file_name']); ?>" width="300"></td>
					  <td><a href="#"><?php echo $data['provider_name']; ?></a></td>
                      <td><a href="#"><?php echo $data['created']; ?></a></td>
					  <td><a href="<?php echo base_url('Upload_files/index1/').$data['provider_id'];?>"><?php echo 'Upload'.$data['count(*)']; ?></a></td>
                      
                     <?php 
                        
                        //if($data['status']== 1){ 
                         ?>
                          <!--<td><img src="<?php //echo base_url('img/active.png'); ?>" width="15px;" height="15px;" /></td>-->
                        <?php
                       // }else{
                        ?> 
                        <!--<td><img src="<?php //echo base_url('img/in_active.png'); ?>" width="15px" height="15px" /></td>-->
                        <?php
						//}
						?>
                      <td class="xcrud-current xcrud-actions xcrud-fix">
                      <span class="btn-group">
                    <!---<a class="btn btn-default btn-xcrud btn btn-warning" href="<?php //echo site_url('Welcome/edit_state/'. $data['id'].''); ?>" data-toggle="modal" title="Edit" target="_self">
                                           <i class="fa fa-edit"></i></a>-->
					
                       <a class="" href="<?php echo site_url('Welcome/delete_gallery/'. $data['provider_id'].''); ?>" title="DELETE" target="_self">
                         <i class="fa fa-trash-o" aria-hidden="true"></i> </a>
                       </span>
                       </td>                     
                      </tr>
                    <?php  } ?>
                    <input type="submit" class="btn btn-danger" value="Delete selected">
                  </tbody>
                  <tfoot>                  
                  </tfoot>
                </table>
                <script type="text/javascript">
                $(document).ready(function(){
                  $('#myTable').DataTable();   
                    });
                </script>
                </form>
              </div>
              <div class="xcrud-nav">
                <div class="btn-group xcrud-limit-buttons" data-toggle="buttons-radio">
                  <button type="button" class="btn btn-default xcrud-action" data-limit="25">25</button>
                  <button type="button" class="btn btn-default active xcrud-action" data-limit="50">50</button>
                  <button type="button" class="btn btn-default xcrud-action" data-limit="100">100</button>
                  <button type="button" class="btn btn-default xcrud-action" data-limit="all">All</button>
                </div>                
              </div>
            </div>
            
 <div class="xcrud-overlay"></div>
          </div>
        </div>
       <div class="modal fade" id="extra1" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <form action="" method="POST">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span>

              </button>
              <h4 class="modal-title">Car rental Edit </h4>
            </div>
            <div class="modal-body form-horizontal">
                This is text inside Modal Popup
            </div>
            <div class="modal-footer">
              <input type="hidden" name="updateextra" value="1" />
              <input type="hidden" name="extrasid" value="7" />
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary">Update</button>
            </div>
          </form>
        </div>
      </div>
    </div>
        </div>
    </div>
  </div>
</div>
</div>

<!-- Bootstrap JS-->

<script src="<?php echo base_url(); ?>js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>js/bootstrap-hover-dropdown.min.js"></script>
<script src="<?php echo base_url(); ?>js/sidebar.js"></script>
<script src="<?php echo base_url(); ?>js/panels.js"></script>

<!-- icheck -->
<script src="<?php echo base_url(); ?>js/icheck.min.js"></script>

              <script>
                $("#checkAll").change(function () {
                        $("input:checkbox").prop('checked', $(this).prop("checked"));
                    });
              </script>

              <script>
                  $('#checkAll').change(function(){
                  if($(this).is(":checked"))
                  $('#autoUpdate').show();
                  else
                  $('#autoUpdate').hide();
                  }); 
              </script>

<link href="<?php echo base_url(); ?>css/grey.css" rel="stylesheet">

<script>
var cb, optionSet1;
$(function () {

    var checkAll = $('input.all');
    var checkboxes = $('input.checkboxcls');

    $('input').iCheck({
      checkboxClass: "icheckbox_square-grey",
    });

        checkAll.on('ifChecked ifUnchecked', function(event) 
        {
            if (event.type == 'ifChecked') {
                checkboxes.iCheck('check');
            } else {
                checkboxes.iCheck('uncheck');
            }
        });

    checkboxes.on('ifChanged', function(event){
        if(checkboxes.filter(':checked').length == checkboxes.length) {
            checkAll.prop('checked', 'checked');
        } else {
            checkAll.removeProp('checked');
        }
        checkAll.iCheck('update');
    });
});

$(".radio").iCheck({
checkboxClass: "icheckbox_square-grey",
radioClass: "iradio_square-grey"
});
</script>

<!-- datepicker -->
<script src="<?php echo base_url(); ?>js/datepicker.js"></script>
<link rel="stylesheet" href="<?php echo base_url(); ?>css/datepicker.css" />
<script>
var fmt = "dd/mm/yyyy";
if (top.location != location) { top.location.href = document.location.href ; }
$(function(){ window.prettyPrint && prettyPrint(); $('.dob').datepicker({format: fmt,autoclose: true}).on('changeDate', function (ev) {
$(this).datepicker('hide'); });
$('#dp1').datepicker();
$('#dp2').datepicker();

// disabling dates

var nowTemp = new Date();
var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
var checkin = $('.dpd1').datepicker({format: fmt, onRender: function(date) { return date.valueOf() < now.valueOf() ? 'disabled' : ''; } }).on('changeDate', function(ev) {
if (ev.date.valueOf() > checkout.date.valueOf()) {
var newDate = new Date(ev.date)
newDate.setDate(newDate.getDate() + 1); checkout.setValue(newDate); } checkin.hide();
$('.dpd2')[0].focus(); }).data('datepicker'); var checkout = $('.dpd2').datepicker({format: fmt,
onRender: function(date) { return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : ''; }
}).on('changeDate', function(ev) { checkout.hide(); }).data('datepicker'); });
</script>

<!-- timepicker -->
<script src="<?php echo base_url(); ?>js/timepicker.js"></script>
<link rel="stylesheet" href="<?php echo base_url(); ?>css/timepicker.css" />
<script>
$(function(){
$('.timepicker').clockface(); });
</script>

<!-- dronzone -->
<link href="<?php echo base_url(); ?>css/dropzone.css" type="text/css" rel="stylesheet" />
<script src="<?php echo base_url(); ?>js/dropzone.min.js"></script>

<!-- Custom functions file -->
<script src="<?php echo base_url(); ?>js/funcs.js"></script>
<!-- Custom functions file -->

<!-- pnotify -->
<script src="<?php echo base_url(); ?>js/pnotify.custom.min.js"></script>
<link href="<?php echo base_url(); ?>css/pnotify.custom.css" rel="stylesheet">

<!-- select2 -->
<link href="<?php echo base_url(); ?>css/select2.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>css/select2-default.css" rel="stylesheet">
<script src="<?php echo base_url(); ?>js/select2.min.js"></script>

<script>

$(function() {
$('.chosen-select').select2( { width:'100%', maximumSelectionSize: 1 } );
$(document).ready(function() {
$(".chosen-multi-select").select2( { width:'100%', } ); }); }); function slideout(){ setTimeout(function(){
$(".alert-success").fadeOut("slow", function () { });
$(".alert-danger").fadeOut("slow", function () { }); }, 4000);}

</script>

<!-- smothwhell starts-->
<script src="<?php echo base_url(); ?>js/smoothwheel.js"></script>
<!-- smothwhell ends-->

<!-- jQuery slimScroll-->
<script src="<?php echo base_url(); ?>js/jquery.slimscroll.min.js"></script>
<script>
window.jQuery.ui || document.write('<script src="<?php echo base_url(); ?>js/jquery.slimscroll.min.js"><\/script>')
</script>
<script src="<?php echo base_url(); ?>js/wow.min.js"></script>
<script>
/*<![CDATA[*/
$(function() {
$(".social-sidebar").socialSidebar();
$('.main').panels();
});
/*]]>*/
</script></body>
</html>