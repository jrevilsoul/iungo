<!DOCTYPE html><head>
  <meta charset="utf-8">
  <title>Admin Login Form</title> 
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">
  <meta name="author" content="">
  <link rel="shortcut icon" href="">
  <link href="<?php echo base_url(); ?>css/loading.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>css/bootstrap.min" rel="stylesheet">
  <link href="<?php echo base_url(); ?>css/style.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>css/admin.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>css/font-awesome.min.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>css/themes/facebook.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>css/animate.min.css" rel="stylesheet">
      <style type="text/css">
     .colbox {
          margin-left: 0px;
          margin-right: 0px;
     }
	 .login_form_main{ width:35%; margin:10% auto; padding:35px 20px 35px 20px; background:#fff;    border-bottom: solid 5px #ddd; display:block; clear:both;}
	 .login_form_main h1{ font-size:25px; font-weight:600; color:#db5677; border-bottom:solid 1px #db5677; padding:0px 0px 15px 0px; margin:0px 0px 15px 0px;}
	 .width_45{ width:45%;}
	 .hight_line{ margin-top:15px; clear:both;}
	 .ma_bo{ margin-bottom:0px !important;}
	 .ma_top{margin-top: 35px;margin-bottom:0px !important;}
     </style>
</head>
<body style="background:#2aa09c;">
<div class="container" >
     <div class="row">
          <div class="login_form_main">
          <?php 
          $attributes = array("class" => "form-horizontal", "id" => "loginform", "name" => "loginform");
          echo form_open("login/index", $attributes);?>
          <fieldset>
          <h1>Admin Login</h1>
              
               <div class="form-group ma_bo">
               <div class="row colbox ">
               <div class="col-lg-12 col-sm-12 hight_line">
                   <label for="txt_username" class="control-label">Username</label>
               </div>
               <div class="col-lg-12 col-sm-12 hight_line">
                    <input class="form-control" id="txt_username" name="txt_username" placeholder="Username" type="text" value="<?php // echo set_value('txt_username'); ?>" />
                    <span class="text-danger"><?php echo form_error('txt_username'); ?></span>
               </div>
               </div>
               </div>

               <div class="form-group" style="clear:both;">
               <div class="row colbox">
               <div class="col-lg-12 col-sm-12 hight_line">
               <label for="txt_password" class="control-label">Password</label>
               </div>
               <div class="col-lg-12 col-sm-12 hight_line">
                    <input class="form-control" id="txt_password" name="txt_password" placeholder="Password" type="password" value="<?php // echo set_value('txt_password'); ?>" />
                    <span class="text-danger"><?php echo form_error('txt_password'); ?></span>
               </div>
               </div>
               </div>
			   
               <div class="form-group ma_top">
               <div class="col-lg-12 col-sm-12 pull-right">
                    <input id="btn_login" name="btn_login" type="submit" class="btn btn-primary pull-left width_45" value="Login" />
                    <input id="btn_cancel" name="btn_cancel" type="reset" class="btn btn-primary pull-right width_45" value="Cancel" />
               </div>
               </div>
          </fieldset>
          <?php echo form_close(); ?>
          <?php echo $this->session->flashdata('msg'); ?>
          </div>
     </div>
</div>
     
<!--load jQuery library-->
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<!--load bootstrap.js-->
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
</body>
</html>