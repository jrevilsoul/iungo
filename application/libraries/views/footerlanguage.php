<?php include('headerandsidebar.php'); 
error_reporting(0);
?>
<script>
    $(function(){
        $("#select_design").change(function()
        {
            var product_code = $("#select_design").val();
          window.location.href = "<?php echo site_url('Welcome/footer_navigation/');?>"+product_code;
				 
        });
		 
    });
</script>
<?php
foreach ($footer_setting as $data){

}
?>
<div class="row">
  <div class="container" id="content">
    <style type="text/css">
    .modal .modal-body {
    max-height: 450px;
    overflow-y: auto;
    }
    </style>
    <h3 class="margin-top-0">footer Navigation</h3>
    <?php //echo '===='.$header_setting[0]['id'];?>
            <!--<form method="post" action="<?php  //echo site_url('Welcome/header_navigation');  ?>" name="data_register">-->
            <form method="post" action="<?php if(!empty($footer_setting)){ echo site_url('Welcome/update_footersettings/').$footer_setting[0]['id']; } else{ echo site_url('Welcome/insert_footer_navigation'); } ?>" name="data_register">
      <div class="panel panel-default">
        <ul class="nav nav-tabs nav-justified" role="tablist">
          <li class="active"><a href="#GENERAL" data-toggle="tab">General</a></li>
          <li class=""><a href="#HOTELS_TYPES" data-toggle="tab">Social Media Links</a></li>
        <!--   <li class=""><a href="#ROOMS_TYPES" data-toggle="tab">Rooms Types</a></li>
        <li class=""><a href="#ROOMS_AMENITIES" data-toggle="tab">Facilities</a></li> -->
          <!-- <li class=""><a href="#PAYMENT_METHODS" data-toggle="tab">Payment Methods</a></li> -->
          <!-- <li class=""><a href="#HOTELS_AMENITIES" data-toggle="tab">Hotels Amenities</a></li> -->
        </ul>
        <div class="panel-body"> <?php if($this->session->flashdata('insertmessage')){?>
				  <div class="alert alert-success" align="center">
				  <?php echo $this->session->flashdata('insertmessage'); ?>
					</div>
				  <?php
				  }elseif($this->session->flashdata('updatemessage')){
				  ?>
				  <div class="alert alert-success" align="center">
				  <?php echo $this->session->flashdata('updatemessage'); ?>
					</div>
				  <?php
				  }
				  ?> <br>
          <div class="tab-content form-horizontal">
            <div class="tab-pane wow fadeIn animated active in" id="GENERAL">
              <div class="clearfix"></div>
              <hr>
              <div class="row form-group">
                <label  class="col-md-4 control-label text-left">Language:</label>
                <div class="col-md-4">
                 <select name="select_design" id="select_design" class="form-control" onchange="select_design()" >
                 <option value="">Select Language</option>
                 <option value="en"<?php if ($data['language'] === 'en') echo ' selected="selected"'?>>English</option>
                 <option value="ger"<?php if ($data['language'] === 'ger') echo ' selected="selected"'?>>Germany</option>
                 </select>                 
                </div>
                
              </div>
              <div class="clearfix"></div>
              <div class="row form-group">

              <label  class="col-md-2 control-label text-left">Faq:</label>
                <div class="col-md-4">
                <input class="form-control" type="text" placeholder="" name="faq"  value="<?php foreach ($footer_setting as $data) { echo $data['faq'];}?>">
                </div>
                <label  class="col-md-2 control-label text-left">About us:</label>
                <div class="col-md-3">
                  <input class="form-control" type="text" placeholder="" name="about"  value="<?php foreach ($footer_setting as $data) { echo $data['AboutUs'];}?>">
                </div>                
              </div>

              <div class="clearfix"></div>
              <div class="row form-group">
                <label  class="col-md-2 control-label text-left">Contact us:</label>
                <div class="col-md-4">
                  <input class="form-control" type="text" placeholder="" name="contactus"  value="<?php foreach ($footer_setting as $data) { echo $data['contact_page'];}?>">
                </div>
                 <label  class="col-md-2 control-label text-left">privacy policy:</label>
                <div class="col-md-3">
                  <input class="form-control" type="text" placeholder="" name="privacy"  value="<?php foreach ($footer_setting as $data) { echo $data['privacyPolicy'];}?>">
                </div>                  
              </div>

              <div class="clearfix"></div>
              <div class="row form-group">
                <label class="col-md-2 control-label text-left">Terms & Conditions:</label>
                <div class="col-md-4">
                 <input class="form-control" type="text" name="term" value="<?php foreach ($footer_setting as $data) { echo $data['term_condition'];}?>" required>
                </div>
                <label class="col-md-2 control-label text-left">Disclaimer:<sup>*</sup></label>
                <div class="col-md-3"><input class="form-control" type="text" placeholder="" name="disclaimer"  value="<?php foreach ($footer_setting as $data){ echo $data['disclaimer'];}?>"></div>
              </div>
              
              <div class="row form-group">
                <label  class="col-md-2 control-label text-left">Cancellation Policy:<sup>*</sup></label>
                <div class="col-md-4">
                  <input class="form-control" type="text" placeholder="" name="cancellation"  value="<?php foreach ($footer_setting as $data) { echo $data['cancellation_policy'];}?>" required>
                </div>
                <label  class="col-md-2 control-label text-left">Career:</label>
                <div class="col-md-3">
                  <input class="form-control" type="text" placeholder="" name="career"  value="<?php foreach ($footer_setting as $data) { echo $data['career'];}?>">
                </div>
              </div>
              <div class="row form-group">
                <label  class="col-md-2 control-label text-left">Dataprotection:<sup>*</sup></label>
                <div class="col-md-4">
                  <input class="form-control" type="text" placeholder="" name="dataprotection"  value="<?php foreach ($footer_setting as $data) { echo $data['Dataprotection'];}?>" required>
                </div>
                <label  class="col-md-2 control-label text-left">Service:</label>
                <div class="col-md-3">
                  <input class="form-control" type="text" placeholder="" name="service"  value="<?php foreach ($footer_setting as $data) { echo $data['service'];}?>">
                </div>
              </div>
              <div class="row form-group">
                <label  class="col-md-2 control-label text-left">Contact Us:<sup>*</sup></label>
                <div class="col-md-4">
                  <input class="form-control" type="text" placeholder="" name="contact"  value="<?php foreach ($footer_setting as $data) { echo $data['ContactUs'];}?>" required>
                </div>
                <label  class="col-md-2 control-label text-left">Send Message:</label>
                <div class="col-md-3">
                  <input class="form-control" type="text" placeholder="" name="send_message"  value="<?php foreach ($footer_setting as $data) { echo $data['sendMessage'];}?>">
                </div>
              </div>
              <div class="row form-group">
                <label  class="col-md-2 control-label text-left">Name:<sup>*</sup></label>
                <div class="col-md-4">
                  <input class="form-control" type="text" placeholder="" name="name"  value="<?php foreach ($footer_setting as $data) { echo $data['name'];}?>" required>
                </div>
                <label  class="col-md-2 control-label text-left">Email:</label>
                <div class="col-md-3">
                  <input class="form-control" type="text" placeholder="" name="email"  value="<?php foreach ($footer_setting as $data) { echo $data['email'];}?>">
                </div>
              </div>
              <div class="row form-group">
                <label  class="col-md-2 control-label text-left">Subject:<sup>*</sup></label>
                <div class="col-md-4">
                  <input class="form-control" type="text" placeholder="" name="subject"  value="<?php foreach ($footer_setting as $data) { echo $data['subject'];}?>" required>
                </div>
                <label  class="col-md-2 control-label text-left">Message:</label>
                <div class="col-md-3">
                  <input class="form-control" type="text" placeholder="" name="message"  value="<?php foreach ($footer_setting as $data) { echo $data['message'];}?>">
                </div>
              </div>
              <div class="row form-group">
                <label  class="col-md-2 control-label text-left">Captcha:<sup>*</sup></label>
                <div class="col-md-4">
                  <input class="form-control" type="text" placeholder="" name="captcha"  value="<?php foreach ($footer_setting as $data) { echo $data['captcha'];}?>" required>
                </div>
                <label  class="col-md-2 control-label text-left">Useful Links:</label>
                <div class="col-md-3">
                  <input class="form-control" type="text" placeholder="" name="usefullink"  value="<?php foreach ($footer_setting as $data) { echo $data['usefullink'];}?>">
                </div>
              </div>
              <div class="row form-group">
                <label  class="col-md-2 control-label text-left">Providers:<sup>*</sup></label>
                <div class="col-md-4">
                  <input class="form-control" type="text" placeholder="" name="providers"  value="<?php foreach ($footer_setting as $data) { echo $data['providers'];}?>" required>
                </div>
                <label  class="col-md-2 control-label text-left">Cities:</label>
                <div class="col-md-3">
                  <input class="form-control" type="text" placeholder="" name="cities"  value="<?php foreach ($footer_setting as $data) { echo $data['cities'];}?>">
                </div>
              </div>
              <div class="row form-group">
                <label  class="col-md-2 control-label text-left">Frequenty Asked Question:<sup>*</sup></label>
                <div class="col-md-4">
                  <input class="form-control" type="text" placeholder="" name="frequently_asked_question"  value="<?php foreach ($footer_setting as $data) { echo $data['frequently_asked_question'];}?>" required>
                </div>
                <label  class="col-md-2 control-label text-left">How long can.I keep my website:</label>
                <div class="col-md-3">
                  <input class="form-control" type="text" placeholder="" name="I_keep_my_website"  value="<?php foreach ($footer_setting as $data) { echo $data['I_keep_my_website'];}?>">
                </div>
              </div>
              <div class="row form-group">
                <label  class="col-md-2 control-label text-left">Home:<sup>*</sup></label>
                <div class="col-md-4">
                  <input class="form-control" type="text" placeholder="" name="home"  value="<?php foreach ($footer_setting as $data) { echo $data['home'];}?>" required>
                </div>
                <label  class="col-md-2 control-label text-left">Drop us Line:</label>
                <div class="col-md-3">
                  <input class="form-control" type="text" placeholder="" name="drop_us_line"  value="<?php foreach ($footer_setting as $data) { echo $data['drop_us_line'];}?>">
                </div>
              </div>
              <div class="row form-group">
                <label  class="col-md-2 control-label text-left">Term of Use:<sup>*</sup></label>
                <div class="col-md-4">
                  <input class="form-control" type="text" placeholder="" name="term_of_use"  value="<?php foreach ($footer_setting as $data) { echo $data['term_of_use'];}?>" required>
                </div>
                <label  class="col-md-2 control-label text-left">Data protection policy:</label>
                <div class="col-md-3">
                  <input class="form-control" type="text" placeholder="" name="dataprotection_policy"  value="<?php foreach ($footer_setting as $data) { echo $data['dataprotection_policy'];}?>">
                </div>
              </div>
              <div class="row form-group">
                <label  class="col-md-2 control-label text-left">Copyright@2016:<sup>*</sup></label>
                <div class="col-md-4">
                  <input class="form-control" type="text" placeholder="" name="copyright"  value="<?php foreach ($footer_setting as $data) { echo $data['copyright'];}?>" required>
                </div>
                <label  class="col-md-2 control-label text-left">All Right Reseverd:</label>
                <div class="col-md-3">
                  <input class="form-control" type="text" placeholder="" name="all_right_reserved"  value="<?php foreach ($footer_setting as $data) { echo $data['all_right_reserved'];}?>">
                </div>
              </div>
              <div class="row form-group">
                <label  class="col-md-2 control-label text-left">Did you need or want to work with us:<sup>*</sup></label>
                <div class="col-md-4">
                  <input class="form-control" type="text" placeholder="" name="did_you_need"  value="<?php foreach ($footer_setting as $data) { echo $data['did_you_need'];}?>" required>
                </div>
                 <label  class="col-md-2 control-label text-left">why do i have to register:</label>
                <div class="col-md-3">
                  <input class="form-control" type="text" placeholder="" name="why_do_register"  value="<?php foreach ($footer_setting as $data) { echo $data['why_do_register'];}?>">
                </div>
              </div>
              <div class="row form-group">
                <label  class="col-md-2 control-label text-left">Address:<sup>*</sup></label>
                <div class="col-md-4">
                  <input class="form-control" type="text" placeholder="" name="address"  value="<?php foreach ($footer_setting as $data) { echo $data['address'];}?>" required>
                </div>
                 <label  class="col-md-2 control-label text-left">phone:</label>
                <div class="col-md-3">
                  <input class="form-control" type="text" placeholder="" name="phone"  value="<?php foreach ($footer_setting as $data) { echo $data['phone'];}?>">
                </div>
              </div>
              <div class="row form-group">
                <label  class="col-md-2 control-label text-left">City:<sup>*</sup></label>
                <div class="col-md-4">
                  <input class="form-control" type="text" placeholder="" name="city"  value="<?php foreach ($footer_setting as $data) { echo $data['city'];}?>" required>
                </div>
              </div>
            </div>
        </div>
      </div>
      <div class="panel-footer">
        <input type="hidden" name="updatesettings" value="1" />
        <input type="hidden" name="updatefor" value="hotels" />
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
    </div>
  </form>
          
       
     
    </div>
  </div>
</div>
<!----edit modal--->
<script>
$(document).ready(function(){
if(window.location.hash != "") {
$('a[href="' + window.location.hash + '"]').click() } });
</script>
</div>
</div>
</div>
<?php include('footer.php'); ?>