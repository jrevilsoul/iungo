<?php include('headerandsidebar.php'); ?>

<div class="output"></div>
<div class="panel panel-default">
 <div class="panel-heading">Edit Provider Type</div>
  <ul class="nav nav-tabs nav-justified" role="tablist">
    <li class="active"><a href="#GENERAL" data-toggle="tab">General</a></li>
    <!--  <li class=""><a href="#AMENITIES" data-toggle="tab">Amenities</a></li> -->
    <li class=""><a href="#TRANSLATE" data-toggle="tab">Translate</a></li>
  </ul>
  <div class="panel-body">
  
    <br>
    <div class="tab-content form-horizontal">
      <div class="tab-pane wow fadeIn animated active in" id="GENERAL">
        <?php
        if($edit_data){
        foreach ($edit_data as $data) {
          ?>
        <form method="POST" action="<?php echo site_url('Welcome/update_sprovider/'.$data['id'].''); ?>" enctype="multipart/form-data">
          <div class="clearfix"></div>
          <div class="row form-group">
            <label class="col-md-2 control-label text-left">Status</label>
            <div class="col-md-2">
              <select class="form-control" name="status" id="status">
                <option value="1">Enabled</option>
                <option value="0">Disabled</option>
              </select>
            </div>
          </div>
          <div class="row form-group">
            <label class="col-md-2 control-label text-left">Provider Type</label>
            <div class="col-md-4">
              <input type="text" value="<?php echo $data['en_serviceTypeName']; ?>" class="form-control" name="en_serviceTypeName">
            </div>
          </div>

          <div class="row form-group">
            <label class="col-md-2 control-label text-left">Provider Image</label>
            <div class="col-md-4">
              <?php // echo form_open_multipart('upload/do_upload');?>
              <input type="file" name="en_serviceTypeIcon" size="20" />
            </div>
            <img src="<?php echo base_url('uploads/') . $data['en_serviceTypeIcon']; ?>"
            width="170px" height="130px">
          </div>

          <div class="row form-group">
            <label class="col-md-2 control-label text-left">Provider Type Description</label>
            <div class="col-md-10">
              <textarea name="en_serviceTypeDesc" id="roomdesc" rows="8" cols="60">
              <?php echo $data['en_serviceTypeDesc']; ?></textarea>
              <script type="text/javascript">
              //<![CDATA[
              window.CKEDITOR_BASEPATH='l/application/libraries/ckeditor.php';
              //]]>
              </script>
              <script type="text/javascript" src="l/application/libraries/ckeditor.phpckeditor.js?t=B5GJ5GG"></script>
              <script type="text/javascript">//<![CDATA[
              CKEDITOR.replace('roomdesc', {"toolbar":[["Source","-","Bold","Italic","Underline","Strike","Format","Styles"],["NumberedList","BulletedList","Outdent","Indent","Blockquote"],["Image","Link","Unlink","Anchor","Table","HorizontalRule","SpecialChar","Maximize"],["Cut","Copy","Paste","PasteText","PasteFromWord","-","Undo","Redo","Find","Replace","-","SelectAll","-","SpellChecker","Scayt"]],"language":"en"});
              //]]>
              </script>
            </div>
          </div>
          <!--  <div class="row form-group">
            <label class="col-md-2 control-label text-left">Meta Title</label>
            <div class="col-md-4">
              <input type="text" value="<?php // echo $data['en_mtitle']; ?>" class="form-control" name="en_mtitle" required>
            </div>
          </div> -->
          <div class="row form-group">
            <label class="col-md-2 control-label text-left">Meta Title</label>
            <div class="col-md-10">
              <textarea name="en_mtitle" id="en_mtitle" rows="8" cols="60"><?php echo $data['en_mtitle']; ?></textarea>
              <script type="text/javascript">//<![CDATA[
              window.CKEDITOR_BASEPATH='l/application/libraries/ckeditor.php';
              //]]></script>
              <script type="text/javascript" src="l/application/libraries/ckeditor.phpckeditor.js?t=B5GJ5GG"></script>
              <script type="text/javascript">//<![CDATA[
              CKEDITOR.replace('en_mtitle', {"toolbar":[["Source","-","Bold","Italic","Underline","Strike","Format","Styles"],["NumberedList","BulletedList","Outdent","Indent","Blockquote"],["Image","Link","Unlink","Anchor","Table","HorizontalRule","SpecialChar","Maximize"],["Cut","Copy","Paste","PasteText","PasteFromWord","-","Undo","Redo","Find","Replace","-","SelectAll","-","SpellChecker","Scayt"]],"language":"en"});
              //]]>
              </script>
            </div>
          </div>
          <div class="row form-group">
            <label class="col-md-2 control-label text-left">Meta Key</label>
            <div class="col-md-10">
              <textarea name="en_mkey" id="en_mkey" rows="8" cols="60"><?php echo $data['en_mkey']; ?></textarea>
              <script type="text/javascript">//<![CDATA[
              window.CKEDITOR_BASEPATH='l/application/libraries/ckeditor.php';
              //]]></script>
              <script type="text/javascript" src="l/application/libraries/ckeditor.phpckeditor.js?t=B5GJ5GG"></script>
              <script type="text/javascript">//<![CDATA[
              CKEDITOR.replace('en_mkey', {"toolbar":[["Source","-","Bold","Italic","Underline","Strike","Format","Styles"],["NumberedList","BulletedList","Outdent","Indent","Blockquote"],["Image","Link","Unlink","Anchor","Table","HorizontalRule","SpecialChar","Maximize"],["Cut","Copy","Paste","PasteText","PasteFromWord","-","Undo","Redo","Find","Replace","-","SelectAll","-","SpellChecker","Scayt"]],"language":"en"});
              //]]>
              </script>
            </div>
          </div>
          <div class="row form-group">
            <label class="col-md-2 control-label text-left">Meta Description</label>
            <div class="col-md-10">
              <textarea name="en_mdesc" id="en_mdesc" rows="8" cols="60"><?php echo $data['en_mdesc']; ?></textarea>
              <script type="text/javascript">//<![CDATA[
              window.CKEDITOR_BASEPATH='l/application/libraries/ckeditor.php';
              //]]></script>
              <script type="text/javascript" src="l/application/libraries/ckeditor.phpckeditor.js?t=B5GJ5GG">
              </script>
              <script type="text/javascript">//<![CDATA[
              CKEDITOR.replace('en_mdesc', {"toolbar":[["Source","-","Bold","Italic","Underline","Strike","Format","Styles"],["NumberedList","BulletedList","Outdent","Indent","Blockquote"],["Image","Link","Unlink","Anchor","Table","HorizontalRule","SpecialChar","Maximize"],["Cut","Copy","Paste","PasteText","PasteFromWord","-","Undo","Redo","Find","Replace","-","SelectAll","-","SpellChecker","Scayt"]],"language":"en"});
              //]]>
              </script>
            </div>
          </div>
        </div>
        <!-- <div class="tab-pane wow fadeIn animated in" id="AMENITIES">
          <div class="row form-group">
            <div class="col-md-12">
              <div class="col-md-4">
                <label class="pointer"><input class="all" type="checkbox" name="" value="" id="select_all" > Select All</label>
              </div>
              <div class="clearfix"></div>
              <hr>
              <div class="clearfix"></div>
              <div class="col-md-4">
                <label class="pointer"><input class="minimal"  type="checkbox" name="roomamenities[]" value="139"  > Access via exterior corridors</label>
              </div>
              
              <div class="col-md-4">
                <label class="pointer"><input class="minimal"  type="checkbox" name="roomamenities[]" value="173"  > Bathrobes</label>
              </div>
              
              <div class="col-md-4">
                <label class="pointer"><input class="minimal"  type="checkbox" name="roomamenities[]" value="174"  > Wake-up calls</label>
              </div>
            </div>
          </div>
        </div> -->
        <div class="tab-pane wow fadeIn animated in" id="TRANSLATE">
          <div class="clearfix"></div>
          <p>
          &nbsp;&nbsp;&nbsp;&nbsp; German:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="<?php echo base_url('img/') . 'flag.png'; ?>" height="75px;" width="150px;"></p>
          <div class="row form-group">
            <label class="col-md-2 control-label text-left">Provider Type</label>
            <div class="col-md-4">
              <input type="text" value="<?php echo $data['gr_serviceTypeName']; ?>" class="form-control" name="gr_serviceTypeName" >
            </div>
          </div>
          <div class="row form-group">
            <label class="col-md-2 control-label text-left">Provider Type Description</label>
            <div class="col-md-10">
              <textarea name="gr_serviceTypeDesc" id="grprodesc" rows="8" cols="60">
              <?php echo $data['gr_serviceTypeDesc']; ?></textarea>
              <script type="text/javascript">//<![CDATA[
              window.CKEDITOR_BASEPATH='l/application/libraries/ckeditor.php';
              //]]></script>
              <script type="text/javascript" src="l/application/libraries/ckeditor.phpckeditor.js?t=B5GJ5GG"></script>
              <script type="text/javascript">//<![CDATA[
              CKEDITOR.replace('grprodesc', {"toolbar":[["Source","-","Bold","Italic","Underline","Strike","Format","Styles"],["NumberedList","BulletedList","Outdent","Indent","Blockquote"],["Image","Link","Unlink","Anchor","Table","HorizontalRule","SpecialChar","Maximize"],["Cut","Copy","Paste","PasteText","PasteFromWord","-","Undo","Redo","Find","Replace","-","SelectAll","-","SpellChecker","Scayt"]],"language":"en"});
              //]]>
              </script>
            </div>
          </div>
          <div class="row form-group">
            <label class="col-md-2 control-label text-left">Meta Title</label>
            <div class="col-md-10">
              <textarea name="gr_mtitle" id="gr_mtitle" rows="8" cols="60"><?php echo $data['gr_mtitle']; ?></textarea>
              <script type="text/javascript">//<![CDATA[
              window.CKEDITOR_BASEPATH='l/application/libraries/ckeditor.php';
              //]]></script>
              <script type="text/javascript" src="l/application/libraries/ckeditor.phpckeditor.js?t=B5GJ5GG"></script>
              <script type="text/javascript">//<![CDATA[
              CKEDITOR.replace('gr_mtitle', {"toolbar":[["Source","-","Bold","Italic","Underline","Strike","Format","Styles"],["NumberedList","BulletedList","Outdent","Indent","Blockquote"],["Image","Link","Unlink","Anchor","Table","HorizontalRule","SpecialChar","Maximize"],["Cut","Copy","Paste","PasteText","PasteFromWord","-","Undo","Redo","Find","Replace","-","SelectAll","-","SpellChecker","Scayt"]],"language":"en"});
              //]]>
              </script>
            </div>
          </div>
          <div class="row form-group">
            <label class="col-md-2 control-label text-left">Meta Key</label>
            <div class="col-md-10">
              <textarea name="gr_mkey" id="gr_mkey" rows="8" cols="60"><?php echo $data['gr_mkey']; ?></textarea>
              <script type="text/javascript">//<![CDATA[
              window.CKEDITOR_BASEPATH='l/application/libraries/ckeditor.php';
              //]]></script>
              <script type="text/javascript" src="l/application/libraries/ckeditor.phpckeditor.js?t=B5GJ5GG"></script>
              <script type="text/javascript">//<![CDATA[
              CKEDITOR.replace('gr_mkey', {"toolbar":[["Source","-","Bold","Italic","Underline","Strike","Format","Styles"],["NumberedList","BulletedList","Outdent","Indent","Blockquote"],["Image","Link","Unlink","Anchor","Table","HorizontalRule","SpecialChar","Maximize"],["Cut","Copy","Paste","PasteText","PasteFromWord","-","Undo","Redo","Find","Replace","-","SelectAll","-","SpellChecker","Scayt"]],"language":"en"});
              //]]>
              </script>
            </div>
          </div>
          <div class="row form-group">
            <label class="col-md-2 control-label text-left">Meta Description</label>
            <div class="col-md-10">
              <textarea name="gr_mdesc" id="gr_mdesc" rows="8" cols="60"><?php echo $data['gr_mdesc']; ?></textarea>
              <script type="text/javascript">//<![CDATA[
              window.CKEDITOR_BASEPATH='l/application/libraries/ckeditor.php';
              //]]></script>
              <script type="text/javascript" src="l/application/libraries/ckeditor.phpckeditor.js?t=B5GJ5GG"></script>
              <script type="text/javascript">//<![CDATA[
              CKEDITOR.replace('gr_mdesc', {"toolbar":[["Source","-","Bold","Italic","Underline","Strike","Format","Styles"],["NumberedList","BulletedList","Outdent","Indent","Blockquote"],["Image","Link","Unlink","Anchor","Table","HorizontalRule","SpecialChar","Maximize"],["Cut","Copy","Paste","PasteText","PasteFromWord","-","Undo","Redo","Find","Replace","-","SelectAll","-","SpellChecker","Scayt"]],"language":"en"});
              //]]>
              </script>
            </div>
          </div>
          
        </div>
      </div>
      <div class="panel-footer">
        <!--   <input type="hidden" id="roomid" name="roomid" value="" />
        <input type="hidden" name="oldquantity" value="" />
        <input type="hidden" name="submittype" value="add" /> -->
        <input class="btn btn-primary" type="submit" value="Update">
      </div>
    </div>
  </div>
</div>
</form>
<?php  } } ?>
</div>
<?php include('footer.php'); ?>
</body>
</html>