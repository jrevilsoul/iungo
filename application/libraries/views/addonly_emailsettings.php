<!DOCTYPE html><head>
    <?php include('headerandsidebar.php'); ?>
    <div class="row">
        <div class="container" id="content">
            <div class="panel panel-default">
                <div class="panel-heading">Email Settings: </div>
                <div class="xcrud-list-container">
				 <?php if($this->session->flashdata('insertmessage')){?>
				  <div class="alert alert-success" align="center">
				  <?php echo $this->session->flashdata('insertmessage'); ?>
					</div>
				  <?php
				  }elseif($this->session->flashdata('updatemessage')){
				  ?>
				  <div class="alert alert-success" align="center">
				  <?php echo $this->session->flashdata('updatemessage'); ?>
					</div>
				  <?php
				  }
				  ?>

               <form method="post" action="<?php if(!empty($email_setting)){ echo site_url('Welcome/update_emailsettings'); } else{ echo site_url('Welcome/insert_emailsettings'); } ?>" name="data_register">
                     <div class="col-md-12 col-sm-12 col-xs-12 form_admin_page">
                     
                     <div class="row form-group">
                <label class="col-md-2 control-label text-left">Info:</label>
                <div class="col-md-10">
                <input type="text" class="form-control" name="info" value="<?php foreach ($email_setting as $data) { echo $data['info'];} ?>">
                </div>             
              </div>
              <div class="row form-group">
                <label class="col-md-2 control-label text-left">Support:</label>
                <div class="col-md-10">
               <input type="text" class="form-control" name="support" value="<?php foreach ($email_setting as $data) { echo $data['support'];} ?>">
                </div>             
              </div>
              <div class="row form-group">
                <label class="col-md-2 control-label text-left">Query:</label>
                <div class="col-md-10">
               <input type="text" class="form-control" name="query" value="<?php foreach ($email_setting as $data) { echo $data['query']; }?>">
                </div>             
              </div>
              <div class="row form-group">
                <label class="col-md-2 control-label text-left">Contact:</label>
                <div class="col-md-10">
               <input type="text" class="form-control" name="contact" value="<?php foreach ($email_setting as $data) { echo $data['contact']; } ?>">
                </div>             
              </div>
             
                   <div class="panel-footer">
        <input type="hidden" name="updatesettings" value="1" />
        <input type="hidden" name="updatefor" value="hotels" />
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
      </div>
                    </form>
                    <?php // } ?>
                  </div>
                </div>
               <?php include('footer.php'); ?>
              </body>
             </html>