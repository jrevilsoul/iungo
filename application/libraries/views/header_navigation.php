<?php include('headerandsidebar.php'); 
error_reporting(0);
?>
<script>
    $(function(){
        $("#select_design").change(function()
        {
            var product_code = $("#select_design").val();
          window.location.href = "<?php echo site_url('Welcome/header_navigation/');?>"+product_code;
				 
        });
		 
    });
</script>
<?php
foreach ($header_setting as $data){

}
?>
<div class="row">
  <div class="container" id="content">
    <style type="text/css">
    .modal .modal-body {
    max-height: 450px;
    overflow-y: auto;
    }
    </style>

    <?php //echo '===='.$header_setting[0]['id'];?>
            <!--<form method="post" action="<?php  //echo site_url('Welcome/header_navigation');  ?>" name="data_register">-->
            <form method="post" action="<?php if(!empty($header_setting)){ echo site_url('Welcome/update_headersettings/').$header_setting[0]['id']; } else{ echo site_url('Welcome/insert_header_navigation'); } ?>" name="data_register">
      <div class="panel panel-default">
         <div class="panel-heading">Header Navigation</div>
        <div class="panel-body">
		 <?php if($this->session->flashdata('insertmessage')){?>
				  <div class="alert alert-success" align="center">
				  <?php echo $this->session->flashdata('insertmessage'); ?>
					</div>
				  <?php
				  }elseif($this->session->flashdata('updatemessage')){
				  ?>
				  <div class="alert alert-success" align="center">
				  <?php echo $this->session->flashdata('updatemessage'); ?>
					</div>
				  <?php
				  }
				  ?>  <br>
          <div class="tab-content form-horizontal">
            <div class="tab-pane wow fadeIn animated active in" id="GENERAL">
              <div class="clearfix"></div>
              <hr>
              <div class="row form-group">
                <label  class="col-md-4 control-label text-left">Language:</label>
                <div class="col-md-4">
                 <select name="select_design" id="select_design" class="form-control" onchange="select_design()" >
                 <option value="">Select Language</option>
                 <option value="en"<?php if ($data['language'] === 'en') echo ' selected="selected"'?>>English</option>
                 <option value="ger"<?php if ($data['language'] === 'ger') echo ' selected="selected"'?>>Germany</option>
                 </select>                 
                </div>
                
              </div>
              <div class="clearfix"></div>
              <div class="row form-group">

              <label  class="col-md-2 control-label text-left">Wedding Finder:</label>
                <div class="col-md-4">
                <input class="form-control" type="text" placeholder="" name="finder"  value="<?php foreach ($header_setting as $data) { echo $data['weddingfinder'];}?>">
                </div>
                <label  class="col-md-2 control-label text-left">Marketplace:</label>
                <div class="col-md-3">
                  <input class="form-control" type="text" placeholder="" name="market"  value="<?php foreach ($header_setting as $data) { echo $data['marketplace'];}?>">
                </div>                
              </div>

              <div class="clearfix"></div>
              <div class="row form-group">
                <label  class="col-md-2 control-label text-left">Login:</label>
                <div class="col-md-4">
                  <input class="form-control" type="text" placeholder="" name="login"  value="<?php foreach ($header_setting as $data) { echo $data['login'];}?>">
                </div>
                 <label  class="col-md-2 control-label text-left">VendorLogin:</label>
                <div class="col-md-3">
                  <input class="form-control" type="text" placeholder="" name="vendor"  value="<?php foreach ($header_setting as $data) { echo $data['vendor'];}?>">
                </div>                  
              </div>

              <div class="clearfix"></div>
              <div class="row form-group">
                <label class="col-md-2 control-label text-left">Eventplanner Name:</label>
                <div class="col-md-4">
                 <input class="form-control" type="text" name="event_name" value="<?php foreach ($header_setting as $data) { echo $data['eventName'];}?>" required>
                </div>
                <label class="col-md-2 control-label text-left">Enter your zipcode,city:<sup>*</sup></label>
                <div class="col-md-3"><input class="form-control" type="text" placeholder="" name="zipcode"  value="<?php foreach ($header_setting as $data){ echo $data['zipcode'];}?>"></div>
              </div>
              
              <div class="row form-group">
                <label  class="col-md-2 control-label text-left">Select service Type:<sup>*</sup></label>
                <div class="col-md-4">
                  <input class="form-control" type="text" placeholder="" name="serviceProviderType"  value="<?php foreach ($header_setting as $data) { echo $data['serviceProviderType'];}?>" required>
                </div>
                <label  class="col-md-2 control-label text-left">provider Name:</label>
                <div class="col-md-3">
                  <input class="form-control" type="text" placeholder="" name="proname"  value="<?php foreach ($header_setting as $data) { echo $data['proivderName'];}?>">
                </div>
              </div>
              <div class="row form-group">
                <label  class="col-md-2 control-label text-left">Newlyweds:<sup>*</sup></label>
                <div class="col-md-4">
                  <input class="form-control" type="text" placeholder="" name="newly_weds"  value="<?php foreach ($header_setting as $data) { echo $data['NewlyWeds'];}?>" required>
                </div>
                <label  class="col-md-2 control-label text-left">service provider:</label>
                <div class="col-md-3">
                  <input class="form-control" type="text" placeholder="" name="service_provider"  value="<?php foreach ($header_setting as $data) { echo $data['serviceProvider'];}?>">
                </div>
              </div>
			  <div class="row form-group">
                <label  class="col-md-2 control-label text-left">search provider:<sup>*</sup></label>
                <div class="col-md-4">
                  <input class="form-control" type="text" placeholder="" name="searchprovider"  value="<?php foreach ($header_setting as $data) { echo $data['searchprovider'];}?>" required>
                </div>
				 <label  class="col-md-2 control-label text-left">FILTER SEARCH:</label>
                <div class="col-md-3">
                  <input class="form-control" type="text" placeholder="" name="filtersearch"  value="<?php foreach ($header_setting as $data) { echo $data['filtersearch'];}?>">
                </div>
            </div>
			 <div class="row form-group">
                <label  class="col-md-2 control-label text-left">Where?:<sup>*</sup></label>
                <div class="col-md-4">
                  <input class="form-control" type="text" placeholder="" name="where"  value="<?php foreach ($header_setting as $data) { echo $data['where'];}?>" required>
                </div>
				 <label  class="col-md-2 control-label text-left">What?:</label>
                <div class="col-md-3">
                  <input class="form-control" type="text" placeholder="" name="what"  value="<?php foreach ($header_setting as $data) { echo $data['what'];}?>">
                </div>
            </div>
			 <div class="row form-group">
                <label  class="col-md-2 control-label text-left">WHO ARE YOU?:<sup>*</sup></label>
                <div class="col-md-4">
                  <input class="form-control" type="text" placeholder="" name="howru"  value="<?php foreach ($header_setting as $data) { echo $data['howru'];}?>" required>
                </div>
                 <label  class="col-md-2 control-label text-left">Over 1200+ Wedding for you special date & Find the perfect venue & save you date.:<sup>*</sup></label>
                <div class="col-md-3">
          <input class="form-control" type="text" placeholder="" name="weddingdetail"  value="<?php foreach ($header_setting as $data) { echo $data['weddingdetail'];}?>" required>
                </div>
            </div>
            <div class="row form-group">
                <label  class="col-md-2 control-label text-left">User Login?:<sup>*</sup></label>
                <div class="col-md-4">
                  <input class="form-control" type="text" placeholder="" name="userlogin"  value="<?php foreach ($header_setting as $data) { echo $data['userlogin'];}?>" required>
                </div>
                 <label  class="col-md-2 control-label text-left">E-mail:<sup>*</sup></label>
                <div class="col-md-3">
          <input class="form-control" type="text" placeholder="" name="email"  value="<?php foreach ($header_setting as $data) { echo $data['email'];}?>" required>
                </div>
            </div>
            <div class="row form-group">
                <label  class="col-md-2 control-label text-left">Forget password:<sup>*</sup></label>
                <div class="col-md-4">
                  <input class="form-control" type="text" placeholder="" name="forgetpassword"  value="<?php foreach ($header_setting as $data) { echo $data['forgetpassword'];}?>" required>
                </div>
                 <label  class="col-md-2 control-label text-left">SIGN UP:<sup>*</sup></label>
                <div class="col-md-3">
          <input class="form-control" type="text" placeholder="" name="signup"  value="<?php foreach ($header_setting as $data) { echo $data['signup'];}?>" required>
                </div>
            </div>
            <div class="row form-group">
                <label  class="col-md-2 control-label text-left">logout:<sup>*</sup></label>
                <div class="col-md-4">
                  <input class="form-control" type="text" placeholder="" name="logout"  value="<?php foreach ($header_setting as $data) { echo $data['logout'];}?>" required>
                </div>
                 <label  class="col-md-2 control-label text-left">Profile Edit:<sup>*</sup></label>
                <div class="col-md-3">
          <input class="form-control" type="text" placeholder="" name="profileedit"  value="<?php foreach ($header_setting as $data) { echo $data['profileedit'];}?>" required>
                </div>
            </div>
            <div class="row form-group">
                <label  class="col-md-2 control-label text-left">Change password:<sup>*</sup></label>
                <div class="col-md-4">
                  <input class="form-control" type="text" placeholder="" name="changepassword"  value="<?php foreach ($header_setting as $data) { echo $data['changepassword'];}?>" required>
                </div>
                 <label  class="col-md-2 control-label text-left">current password:<sup>*</sup></label>
                <div class="col-md-3">
          <input class="form-control" type="text" placeholder="" name="currentpassword"  value="<?php foreach ($header_setting as $data) { echo $data['currentpassword'];}?>" required>
                </div>
            </div>
            <div class="row form-group">
                <label  class="col-md-2 control-label text-left">New password:<sup>*</sup></label>
                <div class="col-md-4">
                  <input class="form-control" type="text" placeholder="" name="newpassword"  value="<?php foreach ($header_setting as $data) { echo $data['newpassword'];}?>" required>
                </div>
                 <label  class="col-md-2 control-label text-left">Confirm Password:<sup>*</sup></label>
                <div class="col-md-3">
          <input class="form-control" type="text" placeholder="" name="confirmpassword"  value="<?php foreach ($header_setting as $data) { echo $data['confirmpassword'];}?>" required>
                </div>
            </div>
            <div class="row form-group">
                <label  class="col-md-2 control-label text-left">SAVE:<sup>*</sup></label>
                <div class="col-md-4">
                  <input class="form-control" type="text" placeholder="" name="save"  value="<?php foreach ($header_setting as $data) { echo $data['save'];}?>" required>
                </div>
                 <label  class="col-md-2 control-label text-left">CANCEL:<sup>*</sup></label>
                <div class="col-md-3">
          <input class="form-control" type="text" placeholder="" name="cancel"  value="<?php foreach ($header_setting as $data) { echo $data['cancel'];}?>" required>
                </div>
            </div>
            <div class="row form-group">
                <label  class="col-md-2 control-label text-left">User Registration:<sup>*</sup></label>
                <div class="col-md-4">
                  <input class="form-control" type="text" placeholder="" name="userregistration"  value="<?php foreach ($header_setting as $data) { echo $data['userregistration'];}?>" required>
                </div>
                 <label  class="col-md-2 control-label text-left">Email-ID:<sup>*</sup></label>
                <div class="col-md-3">
          <input class="form-control" type="text" placeholder="" name="emailid"  value="<?php foreach ($header_setting as $data) { echo $data['email'];}?>" required>
                </div>
            </div>
            <div class="row form-group">
                <label  class="col-md-2 control-label text-left">Edit Profile:<sup>*</sup></label>
                <div class="col-md-4">
                  <input class="form-control" type="text" placeholder="" name="editprofile"  value="<?php foreach ($header_setting as $data) { echo $data['editprofile'];}?>" required>
                </div>
                 <label  class="col-md-2 control-label text-left">Vendor Registration:<sup>*</sup></label>
                <div class="col-md-3">
          <input class="form-control" type="text" placeholder="" name="vendorregistration"  value="<?php foreach ($header_setting as $data) { echo $data['vendorregistration'];}?>" required>
                </div>
            </div>
            <div class="row form-group">
                <label  class="col-md-2 control-label text-left">Vendor Login:<sup>*</sup></label>
                <div class="col-md-4">
                  <input class="form-control" type="text" placeholder="" name="vendorlogin"  value="<?php foreach ($header_setting as $data) { echo $data['vendorlogin'];}?>" required>
                </div>
                 <label  class="col-md-2 control-label text-left">Vendor-Email:<sup>*</sup></label>
                <div class="col-md-3">
          <input class="form-control" type="text" placeholder="" name="vendoremail"  value="<?php foreach ($header_setting as $data) { echo $data['vendoremail'];}?>" required>
                </div>
            </div>
            <div class="row form-group">
                <label  class="col-md-2 control-label text-left">Your First Name:<sup>*</sup></label>
                <div class="col-md-4">
                  <input class="form-control" type="text" placeholder="" name="firstname"  value="<?php foreach ($header_setting as $data) { echo $data['firstname'];}?>" required>
                </div>
                 <label  class="col-md-2 control-label text-left">Last Name:<sup>*</sup></label>
                <div class="col-md-3">
          <input class="form-control" type="text" placeholder="" name="lastname"  value="<?php foreach ($header_setting as $data) { echo $data['lastname'];}?>" required>
                </div>
            </div>
            <div class="row form-group">
                <label  class="col-md-2 control-label text-left">password:<sup>*</sup></label>
                <div class="col-md-4">
                  <input class="form-control" type="text" placeholder="" name="password"  value="<?php foreach ($header_setting as $data) { echo $data['password'];}?>" required>
                </div>
                 <label  class="col-md-2 control-label text-left">Submit:<sup>*</sup></label>
                <div class="col-md-3">
          <input class="form-control" type="text" placeholder="" name="submit"  value="<?php foreach ($header_setting as $data) { echo $data['submit'];}?>" required>
                </div>
            </div>
            <div class="row form-group">
                <label  class="col-md-2 control-label text-left">Back:<sup>*</sup></label>
                <div class="col-md-4">
                  <input class="form-control" type="text" placeholder="" name="back"  value="<?php foreach ($header_setting as $data) { echo $data['back'];}?>" required>
                </div>
                 <label  class="col-md-2 control-label text-left">Welcome to welcome:<sup>*</sup></label>
                <div class="col-md-3">
          <input class="form-control" type="text" placeholder="" name="welcome"  value="<?php foreach ($header_setting as $data) { echo $data['welcome'];}?>" required>
                </div>
            </div>
            <div class="row form-group">
                <label  class="col-md-2 control-label text-left">Name:<sup>*</sup></label>
                <div class="col-md-4">
                  <input class="form-control" type="text" placeholder="" name="Name"  value="<?php foreach ($header_setting as $data) { echo $data['Name'];}?>" required>
                </div>
                 <label  class="col-md-2 control-label text-left">Bluemen &amp; Deko:<sup>*</sup></label>
                <div class="col-md-3">
          <input class="form-control" type="text" placeholder="" name="blumen"  value="<?php foreach ($header_setting as $data) { echo $data['blumen'];}?>" required>
                </div>
            </div>
            <div class="row form-group">
                <label  class="col-md-2 control-label text-left">Mobile No:<sup>*</sup></label>
                <div class="col-md-4">
                  <input class="form-control" type="text" placeholder="" name="mobileno"  value="<?php foreach ($header_setting as $data) { echo $data['mobileno'];}?>" required>
                </div>
                 <label  class="col-md-2 control-label text-left">Your Booked package:<sup>*</sup></label>
                <div class="col-md-3">
          <input class="form-control" type="text" placeholder="" name="package"  value="<?php foreach ($header_setting as $data) { echo $data['package'];}?>" required>
                </div>
            </div>
            <div class="row form-group">
                <label  class="col-md-2 control-label text-left">Demo test:<sup>*</sup></label>
                <div class="col-md-4">
                  <input class="form-control" type="text" placeholder="" name="demotest"  value="<?php foreach ($header_setting as $data) { echo $data['demotest'];}?>" required>
                </div>
            </div>
        </div>
      </div>
      <div class="panel-footer">
        <input type="hidden" name="updatesettings" value="1" />
        <input type="hidden" name="updatefor" value="hotels" />
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
    </div>
  </form>
          
       
     
    </div>
  </div>
</div>
<!----edit modal--->
<script>
$(document).ready(function(){
if(window.location.hash != "") {
$('a[href="' + window.location.hash + '"]').click() } });
</script>
</div>
</div>
</div>
<?php include('footer.php'); ?>