<?php include('headerandsidebar.php'); ?>
<div class="row">
  <div class="container" id="content">
    <style type="text/css">
    .modal .modal-body {
    max-height: 450px;
    overflow-y: auto;
    } </style>
    
    <h3 class="margin-top-0">Voucher Settings</h3>
<?php $id = $this->uri->segment('3'); ?>

    <form method="post" action="<?php if($id != 0){ echo site_url('Welcome/update_voucher/'. $id .''); } else{ echo site_url('Welcome/insert_voucher'); } ?>">
      <div class="panel panel-default">
        <ul class="nav nav-tabs nav-justified" role="tablist">
          <li class="active"><a href="#GENERAL" data-toggle="tab">Setup New Vouchers</a></li>
          <li class=""><a href="#HOTELS_TYPES" data-toggle="tab">Current Vouchers</a></li>
        <!--   <li class=""><a href="#ROOMS_TYPES" data-toggle="tab">Rooms Types</a></li>
        <li class=""><a href="#ROOMS_AMENITIES" data-toggle="tab">Facilities</a></li> -->
          <!-- <li class=""><a href="#PAYMENT_METHODS" data-toggle="tab">Payment Methods</a></li> -->
          <!-- <li class=""><a href="#HOTELS_AMENITIES" data-toggle="tab">Hotels Amenities</a></li> -->
        </ul>
        <div class="panel-body"><?php if($this->session->flashdata('insertmessage')){?>
				  <div class="alert alert-success" align="center">
				  <?php echo $this->session->flashdata('insertmessage'); ?>
					</div>
				  <?php
				  }elseif($this->session->flashdata('updatemessage')){
				  ?>
				  <div class="alert alert-success" align="center">
				  <?php echo $this->session->flashdata('updatemessage'); ?>
					</div>
				  <?php
				  }
				  ?><br>
          <div class="tab-content form-horizontal">
            <div class="tab-pane wow fadeIn animated active in" id="GENERAL">
              <div class="clearfix"></div>          
              <hr>

              <div class="row form-group">
                <label  class="col-md-2 control-label text-left">Voucher Title:<sup>*</sup></label>
                <div class="col-md-4">
                <?php $sql = "SELECT * FROM `site_voucher` WHERE id='$id'";
                $query = $this->db->query($sql); ?>
                <input class="form-control" type="text" 
                name="voucher_name" value="<?php foreach ($query->result() as $row) { echo $row->voucher_title; }  ?>" required />
                </div>
              </div>

              <div class="row form-group">
                <label class="col-md-2 control-label text-left">Select Provider:<sup>*</sup></label>
                <div class="col-md-4">
                <select class="form-control" name="providerid" required />
                <option value="">--SELECT PROVIDER--</option>
                <?php foreach ($view_providerdetails as $data){ ?>
                <option value="<?php echo $data['id']; ?>" <?php foreach ($query->result() as $row){ if($data['id']== $row->provider_id) {echo 'selected="selected"';}} ?>><?php echo $data['provider_name']; ?></option>
                <?php } ?>
                </select>
                </div>
              </div>

              <div class="row form-group">
                <label class="col-md-2 control-label text-left">Voucher Apply:<sup>*</sup></label>
                <div class="col-md-4">
                <select class="form-control" name="voucher_apply" required />
                <option value="1" <?php foreach ($query->result() as $row){ if($row->voucher_type == 1){echo 'selected="selected"'; } }?>>Fixed</option>
                <option value="0" <?php foreach ($query->result() as $row){ if($row->voucher_type == 0){echo 'selected="selected"'; } } ?>>Percentage</option>
                </select>
                </div>                
              </div>

              <div class="row form-group">
                <label  class="col-md-2 control-label text-left">Voucher Code:<sup>*</sup></label>
                <div class="col-md-4">
                  <input class="form-control" type="text" name="voucher_code" id="code" value="<?php foreach ($query->result() as $row) { echo $row->voucher_code; }  ?>" required />
                </div> 
                <div class="col-md-2">
                <button type="button" id="button" onclick="randomString();" class="btn btn-primary">Generate Code</button>
                </div>               
              </div>
              <script type="text/javascript">

    function randomString() {
  var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
  var string_length = 5;
  var randomstring = '';
        for (var i=0; i<string_length; i++) {
          var rnum = Math.floor(Math.random() * chars.length);
          randomstring += chars.substring(rnum,rnum+1);
        }
        document.getElementById('code').value =randomstring;
      }

</script>
              <div class="row form-group">
                <label  class="col-md-2 control-label text-left">Discount Price:<sup>*</sup></label>
                <div class="col-md-4">
                  <input class="form-control" type="text" name="discount_price" value="<?php foreach ($query->result() as $row) { echo $row->discount_price; }  ?>" required />
                </div>                
              </div>

              <div class="row form-group">
                <label  class="col-md-2 control-label text-left">Usage Limit:<sup>*</sup></label>
                <div class="col-md-4">
                  <input class="form-control" type="text" name="usage_limit" value="<?php foreach ($query->result() as $row) { echo $row->usage_limit; }  ?>" required />
                </div>                
              </div>

              <div class="row form-group">
                <label  class="col-md-2 control-label text-left">Description:<sup>*</sup></label>
                <div class="col-md-9">
              <textarea name="description" id="one" rows="8" cols="20"><?php foreach ($query->result() as $row) { echo $row->description; } ?>          
              </textarea>
                <script type="text/javascript">//<![CDATA[
                window.CKEDITOR_BASEPATH='l/application/libraries/ckeditor.php';
                //]]></script>
                <script type="text/javascript" src="l/application/libraries/ckeditor.phpckeditor.js?t=B5GJ5GG"></script>
                <script type="text/javascript">//<![CDATA[
                CKEDITOR.replace('one', {"toolbar":[["Source","-","Bold","Italic","Underline","Strike","Format","Styles"],["NumberedList","BulletedList","Outdent","Indent","Blockquote"],["Image","Link","Unlink","Anchor","Table","HorizontalRule","SpecialChar","Maximize"],["Cut","Copy","Paste","PasteText","PasteFromWord","-","Undo","Redo","Find","Replace","-","SelectAll","-","SpellChecker","Scayt"]],"language":"en"});
                //]]>
                </script>
              </div>                        
              </div>
               <div class="row form-group">
                <label  class="col-md-2 control-label text-left">Status:<sup>*</sup></label>
                <div class="col-md-4">
                <select class="form-control" name="status" required />
                <option value="1" <?php foreach ($query->result() as $row){ if($row->status == 1){echo 'selected="selected"'; } }?>>Enable</option>
                <option value="0" <?php foreach ($query->result() as $row){ if($row->status == 0){echo 'selected="selected"'; } } ?>>Disable</option>
                </select>
                </div>
             </div>
            <button type="submit" class="btn btn-primary">Submit</button>

          <div class="clearfix"></div> 

            </div>
            <div class="tab-pane wow fadeIn animated in" id="HOTELS_TYPES">
             <div class="xcrud">
                <div class="xcrud-container">
                  <div class="xcrud-ajax">
                    <input type="hidden" class="xcrud-data" name="key" value="" />
                    <input type="hidden" class="xcrud-data" name="orderby" value="" />
                    <input type="hidden" class="xcrud-data" name="order" value="desc" />
                    <input type="hidden" class="xcrud-data" name="start" value="0" />
                    <input type="hidden" class="xcrud-data" name="limit" value="10" />
                    <input type="hidden" class="xcrud-data" name="instance" value="" />
                    <input type="hidden" class="xcrud-data" name="task" value="list" />
                    <div class="xcrud-top-actions">
                    <div class="clearfix"></div>
                    </div>
                    <div class="xcrud-list-container">
                      <table class="xcrud-list table table-striped table-hover">
                        <thead>
                        <th>Voucher Title</th>
                        <th>Voucher Type</th>
                        <th>Voucher Code</th>
                        <th>Discount Price</th>
                        <th>Usage Limit</th>
                        <th>Status</th>
                        <th>Actions</th>
                        </thead>
                        <tbody>
            <?php foreach ($voucher_details as $data) { ?>
            <tr class="xcrud-row xcrud-row-0">
                <td><?php echo $data['voucher_title']; ?></td>
                <td><?php if($data['voucher_type'] == 1){echo 'Fixed'; } else {echo 'Percentage';} ?></td>
                <td><?php echo $data['voucher_code']; ?></td>
                <td><?php echo $data['discount_price']; ?></td>
                <td><?php echo $data['usage_limit']; ?></td>
                <td><?php if($data['status'] == 1){echo 'Enabled'; } else {echo 'Disabled';} ?></td>
                   <td> <span class="btn-group">
                      <a class="btn btn-default btn-xcrud btn btn-warning" href="<?php echo site_url('Welcome/view_voucher/'. $data['id'].''); ?>" data-toggle="modal" title="Edit" target="_self">
                       <i class="fa fa-edit"></i></a>                       
                       <a class="btn btn-default btn-xcrud btn-danger" href="<?php echo site_url('Welcome/delete_voucher/'. $data['id'].''); ?>" title="DELETE" target="_self">
                       <i class="fa fa-times"></i></a>
                       </span></td>
                  </tr> 
                  <?php } ?>
                            </tbody>                  
                          <tfoot>
                        </tfoot>
                    </table>
                  </div>
                </div>
                <div class="xcrud-overlay"></div>
              </div>
            </div>
          </div>
          <div class="tab-pane wow fadeIn animated in" id="ROOMS_TYPES">
            <div class="add_button_modal" >
              <button type="button" data-toggle="modal" data-target="#ADD_ROOM_TYPES" class="btn btn-success"><i class="glyphicon glyphicon-plus-sign"></i>Add Room Types</button>
            </div>
            <div class="xcrud">
              <div class="xcrud-container">
                <div class="xcrud-ajax">
                  <input type="hidden" class="xcrud-data" name="key" value="" />
                  <input type="hidden" class="xcrud-data" name="orderby" value="" />
                  <input type="hidden" class="xcrud-data" name="order" value="desc" />
                  <input type="hidden" class="xcrud-data" name="start" value="0" />
                  <input type="hidden" class="xcrud-data" name="limit" value="10" />
                  <input type="hidden" class="xcrud-data" name="instance" value="" />
                  <input type="hidden" class="xcrud-data" name="task" value="list" />
                  <div class="xcrud-top-actions">
                    <div class="btn-group pull-right"><a href="" class="btn btn-danger"><i class="glyphicon glyphicon-remove"></i> Delete Selected</a><a href="javascript:;" data-task="print" class="btn btn-default xcrud-in-new-window xcrud-action"><i class="glyphicon glyphicon-print"></i> Print</a><a href="javascript:;" data-task="csv" class="btn btn-default xcrud-in-new-window xcrud-action"><i class="glyphicon glyphicon-file"></i>Export into CSV</a></div>
                    <div class="clearfix"> </div>
                  </div>
                  <div class="xcrud-list-container">
                    <table class="xcrud-list table table-striped table-hover">
                      <thead>
                        <tr class="xcrud-th">
                          <th><input class="all" type="checkbox" value="" name="rooms[]" id="select_all" >
                          </th>
                          <th class="xcrud-num">&#35;</th>
                          <th data-order="desc" data-orderby="pt_hotels_types_settings.sett_name" class="xcrud-column xcrud-action">Room Types</th>
                          <th data-order="desc" data-orderby="pt_hotels_types_settings.sett_status" class="xcrud-column xcrud-action">Status</th>
                          <th class="xcrud-actions">&nbsp;</th>
                        </tr>
                      </thead>
                      <tbody>
                       <?php
                /*if(isset($_POST['provider'])){
                $a = $_POST['provider'];
                $sql  = "SELECT * FROM `tbl_room` WHERE provider_id = '$a'";
                $query = $this->db->query($sql);
                foreach ($query->result() as $row) {*/ 
                  ?>
                <input type="hidden" name="" value="<?php // echo $a; ?>">
                <tr class="xcrud-row xcrud-row-0"> <td>
                  <input class="checkboxcls" type="checkbox" name="rooms[]" value="<?php // echo $row->id; ?>"></td>
                  <td class="xcrud-current xcrud-num"><?php // echo $row->id; ?></td>
                          <td><?php //echo $row->room_type; ?></td>
                          <td><?php //if($row->status == 1){echo "Active";} else {echo "Inactive";}; ?></td>
                          <td class="xcrud-current xcrud-actions xcrud-fix"><span class="btn-group"><a class="btn btn-default btn-xcrud btn btn-warning" href="#<?php //echo $row->id; ?>" title="Edit" data-toggle="modal"><i class="fa fa-edit"></i></a><a class="btn btn-default btn-xcrud btn-danger" href="<?php //echo site_url('welcome/delete_roomtype/'.$row->id.''); ?>" title="DELETE" target="_self" ><i class="fa fa-times"></i></a></span>
                          </td>
                </tr><?php // } }?>
                      </tbody>
                      <tfoot>
                      </tfoot>
                    </table>
                  </div>
                  <div class="xcrud-nav">
                    <div class="btn-group xcrud-limit-buttons" data-toggle="buttons-radio">
                      <button type="button" class="btn btn-default active xcrud-action" data-limit="10">10</button>
                      <button type="button" class="btn btn-default xcrud-action" data-limit="25">25</button>
                      <button type="button" class="btn btn-default xcrud-action" data-limit="50">50</button>
                      <button type="button" class="btn btn-default xcrud-action" data-limit="100">100</button>
                      <button type="button" class="btn btn-default xcrud-action" data-limit="all">All</button>
                    </div>
                    <!-- <ul class="pagination">
                      <li class="active"><span>1</span></li>
                      <li class=""><a href="javascript:;" class="xcrud-action" data-start="10">2</a></li>
                      <li class=""><a href="javascript:;" class="xcrud-action" data-start="20">3</a></li>
                    </ul> -->
                    <a class="xcrud-search-toggle btn btn-default" href="javascript:;">Search</a><span class="xcrud-search form-inline" style="display:none;">
                    <input class="xcrud-searchdata xcrud-search-active input-small form-control" name="phrase" data-type="text" style="" data-fieldtype="default" type="text" value="" />
                    <select class="xcrud-searchdata  input-small form-control" name="phrase" style="display:none" data-type="select" data-fieldtype="dropdown" data-fieldname="pt_hotels_types_settings.sett_selected">
                      <option value="Yes">Yes</option>
                      <option value="No">No</option>
                    </select>
                    <select class="xcrud-searchdata  input-small form-control" name="phrase" style="display:none" data-type="select" data-fieldtype="dropdown" data-fieldname="pt_hotels_types_settings.sett_status">
                      <option value="Yes">Yes</option>
                      <option value="No">No</option>
                    </select>
                    <select class="xcrud-data xcrud-columns-select input-small form-control" name="column">
                    <option value="">All fields</option>
                    <option value="pt_hotels_types_settings.sett_name" data-type="text">Name</option>
                    <option value="pt_hotels_types_settings.sett_selected" data-type="select">Selected</option>
                    <option value="pt_hotels_types_settings.sett_status" data-type="select">Status</option>
                    </select>
                  </div>
                </div>
                <div class="xcrud-overlay"></div>
              </div>
            </div>
          </div>
          <div class="tab-pane wow fadeIn animated in" id="PAYMENT_METHODS">
            <div class="add_button_modal" >
              <button type="button" data-toggle="modal" data-target="#ADD_PAYMENT_TYPES" class="btn btn-success"><i class="glyphicon glyphicon-plus-sign"></i>Add</button>
            </div>
            <div class="xcrud">
              <div class="xcrud-container">
                <div class="xcrud-ajax">
                  <input type="hidden" class="xcrud-data" name="key" value="" />
                  <input type="hidden" class="xcrud-data" name="orderby" value="" />
                  <input type="hidden" class="xcrud-data" name="order" value="desc" />
                  <input type="hidden" class="xcrud-data" name="start" value="0" />
                  <input type="hidden" class="xcrud-data" name="limit" value="10" />
                  <input type="hidden" class="xcrud-data" name="instance" value="" />
                  <input type="hidden" class="xcrud-data" name="task" value="list" />
                  <div class="xcrud-top-actions">
                    <div class="btn-group pull-right"><a href="" class="btn btn-danger"><i class="glyphicon glyphicon-remove"></i> Delete Selected</a><a href="javascript:;" data-task="print" class="btn btn-default xcrud-in-new-window xcrud-action"><i class="glyphicon glyphicon-print"></i> Print</a><a href="javascript:;" data-task="csv" class="btn btn-default xcrud-in-new-window xcrud-action"><i class="glyphicon glyphicon-file"></i> Export into CSV</a></div>
                    <div class="clearfix"></div>
                  </div>
                  <div class="xcrud-list-container">
                    <table class="xcrud-list table table-striped table-hover">
                      <thead>
                        <tr class="xcrud-th">
                          <th><input class=all type='checkbox' value='' id=select_all ></th>
                          <th class="xcrud-num">&#35;</th>
                          <th data-order="desc" data-orderby="pt_hotels_types_settings.sett_name" class="xcrud-column xcrud-action">Name</th>
                          <th data-order="desc" data-orderby="pt_hotels_types_settings.sett_selected" class="xcrud-column xcrud-action">Selected</th>
                          <th data-order="desc" data-orderby="pt_hotels_types_settings.sett_status" class="xcrud-column xcrud-action">Status</th>
                          <th class="xcrud-actions">&nbsp;</th>
                        </tr>
                      </thead>

                      <tbody>
                        <tr class="xcrud-row xcrud-row-0">
                          <td><input class="checkboxcls" type='checkbox' value=175></td>
                          <td class="xcrud-current xcrud-num">1</td>
                          <td>Pay on Arrival</td>
                          <td>Yes</td>
                          <td>Yes</td>
                          <td class="xcrud-current xcrud-actions xcrud-fix"><span class="btn-group"><a class="btn btn-default btn-xcrud btn btn-warning" href="#sett175" title="Edit" data-toggle="modal"><i class="fa fa-edit"></i></a><a class="btn btn-default btn-xcrud btn-danger" href="" title="DELETE" target="_self" id="175"><i class="fa fa-times"></i></a></span></td>
                        </tr>
                        <tr class="xcrud-row xcrud-row-1">
                          <td><input class="checkboxcls" type="checkbox" value="138"></td>
                          <td class="xcrud-current xcrud-num">2</td>
                          <td>Master/ Visa Card</td>
                          <td>Yes</td>
                          <td>Yes</td>
                          <td class="xcrud-current xcrud-actions xcrud-fix"><span class="btn-group"><a class="btn btn-default btn-xcrud btn btn-warning" href="#sett138" title="Edit" data-toggle="modal"><i class="fa fa-edit"></i></a><a class="btn btn-default btn-xcrud btn-danger" href="" title="DELETE" target="_self" id="138"><i class="fa fa-times"></i></a></span></td>
                        </tr>
                        
                        <tr class="xcrud-row xcrud-row-0">
                          <td><input class="checkboxcls" type='checkbox' value=27></td>
                          <td class="xcrud-current xcrud-num">7</td>
                          <td>Skrill</td>
                          <td>No</td>
                          <td>Yes</td>
                          <td class="xcrud-current xcrud-actions xcrud-fix"><span class="btn-group"><a class="btn btn-default btn-xcrud btn btn-warning" href="#sett27" title="Edit" data-toggle="modal"><i class="fa fa-edit"></i></a><a class="btn btn-default btn-xcrud btn-danger" href="" title="DELETE" target="_self" id="27"><i class="fa fa-times"></i></a></span></td>
                        </tr>                        
                      </tbody>

                      <tfoot>
                      </tfoot>

                    </table>
                  </div>
                  <div class="xcrud-nav">
                    <div class="btn-group xcrud-limit-buttons" data-toggle="buttons-radio">
                      <button type="button" class="btn btn-default active xcrud-action" data-limit="10">10</button>
                      <button type="button" class="btn btn-default xcrud-action" data-limit="25">25</button>
                      <button type="button" class="btn btn-default xcrud-action" data-limit="50">50</button>
                      <button type="button" class="btn btn-default xcrud-action" data-limit="100">100</button>
                      <button type="button" class="btn btn-default xcrud-action" data-limit="all">All</button>
                    </div>
                    <a class="xcrud-search-toggle btn btn-default" href="javascript:;">Search</a><span class="xcrud-search form-inline" style="display:none;">
                    <input class="xcrud-searchdata xcrud-search-active input-small form-control" name="phrase" data-type="text" style="" data-fieldtype="default" type="text" value="" />
                    <select class="xcrud-searchdata  input-small form-control" name="phrase" style="display:none" data-type="select" data-fieldtype="dropdown" data-fieldname="pt_hotels_types_settings.sett_selected">
                      <option value="Yes">Yes</option>
                      <option value="No">No</option>
                    </select>
                    <select class="xcrud-searchdata  input-small form-control" name="phrase" style="display:none" data-type="select" data-fieldtype="dropdown" data-fieldname="pt_hotels_types_settings.sett_status">
                      <option value="Yes">Yes</option>
                      <option value="No">No</option>
                    </select>
                    <select class="xcrud-data xcrud-columns-select input-small form-control" name="column">
                    <option value="">All fields</option>
                    <option value="pt_hotels_types_settings.sett_name" data-type="text">Name</option>
                    <option value="pt_hotels_types_settings.sett_selected" data-type="select">Selected</option>
                    <option value="pt_hotels_types_settings.sett_status" data-type="select">Status</option>
                    </select>
                  </div>
                </div>
                <div class="xcrud-overlay"></div>
              </div>
            </div>
          </div>
          
          <div class="tab-pane wow fadeIn animated in" id="ROOMS_AMENITIES">
            <div class="add_button_modal" >
              <button type="button" data-toggle="modal" data-target="#ADD_ROOM_AMT" class="btn btn-success"><i class="glyphicon glyphicon-plus-sign"></i>Add Facility</button>
            </div>
            <div class="xcrud">
              <div class="xcrud-container">
                <div class="xcrud-ajax">
                  <input type="hidden" class="xcrud-data" name="key" value="" />
                  <input type="hidden" class="xcrud-data" name="orderby" value="" />
                  <input type="hidden" class="xcrud-data" name="order" value="desc" />
                  <input type="hidden" class="xcrud-data" name="start" value="0" />
                  <input type="hidden" class="xcrud-data" name="limit" value="10" />
                  <input type="hidden" class="xcrud-data" name="instance" value="" />
                  <input type="hidden" class="xcrud-data" name="task" value="list" />
                  <div class="xcrud-top-actions">
                    <div class="btn-group pull-right"><a href="" class="btn btn-danger"><i class="glyphicon glyphicon-remove"></i> Delete Selected</a><a href="javascript:;" data-task="print" class="btn btn-default xcrud-in-new-window xcrud-action"><i class="glyphicon glyphicon-print"></i> Print</a><a href="javascript:;" data-task="csv" class="btn btn-default xcrud-in-new-window xcrud-action"><i class="glyphicon glyphicon-file"></i>Export into CSV</a></div>
                    <div class="clearfix"></div>
                  </div>
                  <div class="xcrud-list-container">
                    <table class="xcrud-list table table-striped table-hover">
                      <thead>
                        <tr class="xcrud-th">
                          <th>
                          <input class="all" type='checkbox' value='' id="select_all" ></th>
                          <th class="xcrud-num">&#35;</th>
                          <th data-order="desc" data-orderby="pt_hotels_types_settings.sett_name" class="xcrud-column xcrud-action"> Name</th>
                          <th data-order="desc" data-orderby="pt_hotels_types_settings.sett_status" class="xcrud-column xcrud-action"> Status</th>
                          <th class="xcrud-actions">&nbsp;</th>
                        </tr>
                      </thead>

                      <tbody>
                   <?php
                if(isset($_POST['provider'])){
                $a = $_POST['provider'];
                $sql   = "SELECT * FROM `tbl_facilities` WHERE provider_id = '$a'";
                $query = $this->db->query($sql);
                foreach ($query->result() as $row) { ?>
                        <tr class="xcrud-row xcrud-row-0">
                          <td><input class="checkboxcls" type="checkbox" name="facility[]" value="174"></td>
                          <td class="xcrud-current xcrud-num"><?php echo $row->id; ?></td>
                          <td><?php echo $row->facility_name; ?></td>
                          
                          <td><?php echo $row->status; ?></td>
                          <td class="xcrud-current xcrud-actions xcrud-fix"><span class="btn-group"><a class="btn btn-default btn-xcrud btn btn-warning" href="#facility<?php echo $row->id; ?>" title="Edit" data-toggle="modal"><i class="fa fa-edit"></i></a><a class="btn btn-default btn-xcrud btn-danger" href="<?php echo site_url('welcome/update_facility/'. $row->id.''); ?>" title="DELETE" target="_self" id="174"><i class="fa fa-times"></i></a></span></td>
                        </tr>
                        <?php } } ?>                        
                      </tbody>
                      <tfoot>
                      </tfoot>
                    </table>
                  </div>
                  <div class="xcrud-nav">
                    
                    <div class="btn-group xcrud-limit-buttons" data-toggle="buttons-radio">
                      <button type="button" class="btn btn-default active xcrud-action" data-limit="10">10
                      </button>
                      <button type="button" class="btn btn-default xcrud-action" data-limit="25">25</button>
                      <button type="button" class="btn btn-default xcrud-action" data-limit="50">50</button>
                      <button type="button" class="btn btn-default xcrud-action" data-limit="100">100</button>
                      <button type="button" class="btn btn-default xcrud-action" data-limit="all">All</button>
                    </div>
                    <!-- <ul class="pagination">
                      <li class="active"><span>1</span></li>
                      <li class=""><a href="javascript:;" class="xcrud-action" data-start="10">2</a></li>
                      <li class=""><a href="javascript:;" class="xcrud-action" data-start="20">3</a></li>
                      <li class=""><a href="javascript:;" class="xcrud-action" data-start="30">4</a></li>
                    </ul> -->
                    <a class="xcrud-search-toggle btn btn-default" href="javascript:;">Search</a>
                    <span class="xcrud-search form-inline" style="display:none;">
                    <input class="xcrud-searchdata xcrud-search-active input-small form-control" name="phrase" data-type="text" style="" data-fieldtype="default" type="text" value="" />
                    <select class="xcrud-searchdata  input-small form-control" name="phrase" style="display:none" data-type="select" data-fieldtype="dropdown" data-fieldname="pt_hotels_types_settings.sett_selected">
                      <option value="Yes">Yes</option>
                      <option value="No">No</option>
                    </select>
                    <select class="xcrud-searchdata  input-small form-control" name="phrase" style="display:none" data-type="select" data-fieldtype="dropdown" data-fieldname="pt_hotels_types_settings.sett_status">
                      <option value="Yes">Yes</option>
                      <option value="No">No</option>
                    </select>
                    <select class="xcrud-data xcrud-columns-select input-small form-control" name="column">
                      <option value="">All fields</option>
                      <option value="pt_hotels_types_settings.sett_name" data-type="text">Name</option>
                      <option value="pt_hotels_types_settings.sett_selected" data-type="select">Selected</option>
                      <option value="pt_hotels_types_settings.sett_status" data-type="select">Status</option>
                    </select>
                  </div>
                </div>
                <div class="xcrud-overlay"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="panel-footer">
        <input type="hidden" name="updatesettings" value="1" />
        <input type="hidden" name="updatefor" value="hotels" />
        
      </div>
    </div>
  </form>
    </div>
  </div>
</div>
<!----edit modal--->
<script>
$(document).ready(function(){
if(window.location.hash != "") {
$('a[href="' + window.location.hash + '"]').click() } });
</script>
</div>
</div>
</div>
<?php include('footer.php'); ?>