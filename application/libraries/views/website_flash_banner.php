<!DOCTYPE html><head>
    <?php include('headerandsidebar.php'); ?>
    <div class="row">
        <div class="container" id="content">
            <div class="panel panel-default">
                <div class="panel-heading">Site Flash Banner Settings: </div>
                <div class="xcrud-list-container">
				<?php if($this->session->flashdata('insertmessage')){?>
				  <div class="alert alert-success" align="center">
				  <?php echo $this->session->flashdata('insertmessage'); ?>
					</div>
				  <?php
				  }elseif($this->session->flashdata('updatemessage')){
				  ?>
				  <div class="alert alert-success" align="center">
				  <?php echo $this->session->flashdata('updatemessage'); ?>
					</div>
				  <?php
				  }
				  ?>

              <form method="post" action="<?php if(!empty($website_flash_banner)) { echo site_url('Welcome/update_website_flash_banner'); } else{ echo site_url('Welcome/insert_website_flash_banner'); } ?>" enctype="multipart/form-data">
                      <table class="xcrud-list table table-striped table-hover">
                        <thead>
                          <tr class="xcrud-th">
                            <!--  <th><input class=all type='checkbox' value='' id=select_all ></th> -->
                            <!-- <th class="xcrud-num">&#35;</th> -->
                            <th data-order="desc" data-orderby="pt_hotels_types_settings.sett_name" class="xcrud-column xcrud-action"></th>                            
                            <th class="xcrud-actions">&nbsp;</th>
                          </tr>
                        </thead>
                        <tbody>
                        <br />
                        <br />
              <div class="row form-group">
              <label class="col-md-2 control-label text-center">Flash Banner Type</label>
              <div class="col-md-2">
                <select class="form-control" name="status_banner" id="typeofbanner">
                  <option value="image">Image</option>
                  <option value="video">Video</option>
                </select>
              </div>
            </div>  <br /><br />
            <div class="row form-group banner" id="image">
              <label class="col-md-2 control-label text-center">Upload Image for Banner</label>
              <div class="col-md-4" >
                <input type="file" name="banner_image" />
              </div>
              <div class="col-md-6" >
              <?php foreach($website_flash_banner as $data) { ?>
                <img src="<?php echo base_url('banners/') . $data['image'];  ?>" width="390px;" height="250px;">
                <?php } ?>
              </div>
            </div>
            <div  id="video" class="row form-group banner" style="display:none">
              <label class="col-md-2 control-label text-center">Video Url</label>
                <div class="col-md-4">
                <input type="text" value="" class="form-control" placeholder="http:// (Please insert .mp4 url here)"
                name="banner_video">
                </div>
            </div>
            <script type="text/javascript">
               $(function() {
                      $('#typeofbanner').change(function(){
                          $('.banner').hide();
                          $('#' + $(this).val()).show();
                      });
                  });
            </script>
               </tbody>
                    </table>
                    <div class="panel-footer">
                   
                      <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                    </form>
                    <?php // } ?>
                  </div>
                </div>
               <?php include('footer.php'); ?>
              </body>
             </html>