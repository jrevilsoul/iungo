<?php include('headerandsidebar.php'); ?>
<div class="row">
  <div class="container" id="content">
    <div class="panel panel-default">
      <div class="panel-heading">Add City</div>
<?php if($this->session->flashdata('insertmessage')){?>
				  <div class="alert alert-success" align="center">
				  <?php echo $this->session->flashdata('insertmessage'); ?>
					</div>
				  <?php
				  }elseif($this->session->flashdata('updatemessage')){
				  ?>
				  <div class="alert alert-success" align="center">
				  <?php echo $this->session->flashdata('updatemessage'); ?>
					</div>
				  <?php
				  }
				  ?>
      <form class="add_button" action="#" method="post">
        <!-- <button type="submit" class="btn btn-success">
        <i class="glyphicon glyphicon-plus-sign"></i>Booking Edit Delete </button> -->
      </form>

      <div class="panel-body">   
              <div class="xcrud">
                <div class="xcrud-container">
                <div class="xcrud-ajax">
              <!-- <div class=" pull-right" style="float:right;">
              <input type="hidden" class="xcrud-data" name="key" value= "017bfe2df5d71e29f34946466c178b21e4491ace"  />
                <input type="hidden" class="xcrud-data" name="orderby" value="pt_cars.car_id" />
                <input type="hidden" class="xcrud-data" name="order" value="desc" />
                <input type="hidden" class="xcrud-data" name="start" value="0" />
                <input type="hidden" class="xcrud-data" name="limit" value="50" />
                <input type="hidden" class="xcrud-data" name="instance" value="786870d5a374d329252a31dbbc49e5d866817eb0" />
                <input type="hidden" class="xcrud-data" name="task" value="list" />
                </div> -->
                <div class="xcrud-list-container">
                 <div id="container" class="container">
                   <form method="post" action="<?php echo site_url('Welcome/submit_city'); ?>" name="data_register">
                   <div class="col-md-12 col-sm-12 col-xs-12 form_admin_page">
      <div class="row form-group">
          <label class="col-md-2 control-label text-left"  for="Country">Status</label>
          <div class="col-md-10">
          <select class="form-control" name="status" required/>
                  <option value="1">Enable</option>
                  <option value="0">Disable</option>                         
                  </select> 
        </div>
        </div>
        
        <div class="row form-group">
          <label class="col-md-2 control-label text-left"  for="Country">Select Country</label>
          <?php

            /* foreach($list->result() as $data){ 
                echo "<pre>";
                echo $data->country; 
                echo "</pre>";
                } */

          if($list->num_rows() > 0){    ?>   
          <div class="col-md-10">
          <select id="countrylist" name="country" class="form-control" onchange="selectState(this.options[this.selectedIndex].value)" required="required" />
                <option value="0">Select country</option>
                <?php
                foreach($list->result() as $listElement){
                ?>
              <option value="<?php echo $listElement->id ?>" >
              <?php echo $listElement->country; ?>
              </option>
                  <?php }  ?>
              </select>
        </div>
        </div>
        
      <div class="row form-group">
          <label class="col-md-2 control-label text-left" for="Country">Select State</label>
          <div class="col-md-10">
           <select name="state" class="form-control" id="state_dropdown" onchange="selectCity(this.options[this.selectedIndex].value)" required="required" />
                <option value="0">Select state</option>
              </select>
             <span id="state_loader"></span>    
        </div>
        </div>
      <div class="row form-group">
          <label class="col-md-2 control-label text-left" for="Country">City</label>
          <div class="col-md-10">
           <input type="text" class="form-control" value="" name="city">

    <?php
      }else{
        echo 'No Country Name Found';
      }
      ?>
        </div>
        </div>
      
      <div class="panel-footer">
         <button type="submit" class="btn btn-primary pull-right">Submit</button>
           <div class="clearfix"></div>
        </div>
        </div>
                   </form>
                 
              </div>
             </div>
            </div>

    <?php include('footer.php'); ?>
</body>
</html>