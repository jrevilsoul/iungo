<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller
{          
          /* http://example.com/index.php/welcome/index         
           * Since this controller is set as the default controller in
           * config/routes.php, it's displayed at http://example.com/
           * @see https://codeigniter.com/user_guide/general/urls.html
           */

            public function __construct()
              {
                  parent::__construct();
                  $this->load->helper(array('form', 'url'));
                  $this->load->model('Welcome_model','welcome'); /* LOADING MODEL * Welcome_model as welcome */
                 /* $this->load->library('session');
                   $is_logged = $this->session->userdata('username');
                    if (!$is_logged) {
                        redirect('login/index', 'refresh');
                                  }*/
              }

              public function index()
                   {
                    $this->load->view('dashboard'); 
                   }

              public function logout()
                 {
                   unset($_SESSION['username']);
                   $this->session->sess_destroy();
                   redirect('login/index');
                 }

          /****************************Start General Settings  **********************/

            public function add_general_setting()
                {
                    $this->load->view('add_general_settings');
                }


        /****************************End General Settings  **********************/

             
        /****************************Start Service Provider Type  ******************/

            public function view_sprovider()
                {                  
                  $this->data['view_data'] = $this->welcome->view_sprovider();
                  $this->load->view('view_sprovider', $this->data);                
                }

            public function add_sprovider()
                {
                    $this->load->view('add_sprovider');
                }

            public function submit_sprovider()
              {
                $config['upload_path']          = './uploads/';
                $config['allowed_types']        = 'gif|jpg|png';
                $config['max_size']             = '1024';

                 $this->load->library('upload', $config);
                 $this->upload->do_upload('en_serviceTypeIcon');
                 $data_upload_files = $this->upload->data();

                   $data = array(
                    'en_serviceTypeName' => $this->input->post('en_serviceTypeName'),
                    'en_serviceTypeIcon' => $data_upload_files['file_name'],                    
                    'en_serviceTypeDesc' => $this->input->post('en_serviceTypeDesc'),
                    'en_mtitle'          => $this->input->post('en_mtitle'),
                    'en_mkey'            => $this->input->post('en_mkey'),
                    'en_mdesc'           => $this->input->post('en_mdesc'),
                    'gr_serviceTypeName' => $this->input->post('gr_serviceTypeName'),
                    'gr_serviceTypeDesc' => $this->input->post('gr_serviceTypeDesc'),
                    'gr_mtitle'          => $this->input->post('gr_mtitle'),
                    'gr_mkey'            => $this->input->post('gr_mkey'),
                    'gr_mdesc'           => $this->input->post('gr_mdesc'),
                    'status'             => $this->input->post('status'),
                    'created_date'       => date("d/m/y h:i:s"));

                  $insert = $this->welcome->insert_sprovider($data);
                  // $this->session->set_flashdata('message','Your data inserted Successfully..');
                  /*$this->data['viewafter'] = $this->welcome->view_sprovider();
                  $this->load->view('view_sprovider', $this->data);*/
                  redirect('welcome/view_sprovider');
              }

              public function edit_sprovider($id)
                {
                $this->data['edit_data'] = $this->welcome->edit_sprovider($id);
                $this->load->view('edit_sprovider', $this->data, FALSE);
                }

              public function update_sprovider($id)
                  {
                $config['upload_path']          = './uploads/';
                $config['allowed_types']        = 'gif|jpg|png';
                $config['max_size']             = '2048';
         
                 $this->load->library('upload', $config);

                 $this->upload->do_upload('en_serviceTypeIcon');

                if(!empty($this->upload->do_upload('en_serviceTypeIcon'))){
                 $data_upload_files = $this->upload->data();
                      {
                      $data = array(
                        'en_serviceTypeName' => $this->input->post('en_serviceTypeName'),
                        'en_serviceTypeIcon' => $data_upload_files['file_name'],
                        'en_serviceTypeDesc' => $this->input->post('en_serviceTypeDesc'),
                        'en_mtitle'          => $this->input->post('en_mtitle'),
                        'en_mkey'            => $this->input->post('en_mkey'),
                        'en_mdesc'           => $this->input->post('en_mdesc'),
                        'gr_serviceTypeName' => $this->input->post('gr_serviceTypeName'),
                        'gr_serviceTypeDesc' => $this->input->post('gr_serviceTypeDesc'),
                        'gr_mtitle'          => $this->input->post('gr_mtitle'),
                        'gr_mkey'            => $this->input->post('gr_mkey'),
                        'gr_mdesc'           => $this->input->post('gr_mdesc'),
                        'status'             => $this->input->post('status'),
                        'created_date'       => date("d/m/y h:i:s"));

                        $this->db->where('id', $id);
                        $this->db->update('tbl_serviceProviderType', $data);
                       // $this->session->set_flashdata('message','Your data updated Successfully..');
                        redirect('welcome/view_sprovider');
                        }
                      }

                        else
                        {
                $data = array(
                        'en_serviceTypeName' => $this->input->post('en_serviceTypeName'),
                        'en_serviceTypeDesc' => $this->input->post('en_serviceTypeDesc'),
                        'en_mtitle'          => $this->input->post('en_mtitle'),
                        'en_mkey'            => $this->input->post('en_mkey'),
                        'en_mdesc'           => $this->input->post('en_mdesc'),
                        'gr_serviceTypeName' => $this->input->post('gr_serviceTypeName'),
                        'gr_serviceTypeDesc' => $this->input->post('gr_serviceTypeDesc'),
                        'gr_mtitle'          => $this->input->post('gr_mtitle'),
                        'gr_mkey'            => $this->input->post('gr_mkey'),
                        'gr_mdesc'           => $this->input->post('gr_mdesc'),
                        'status'             => $this->input->post('status'),
                        'created_date'       => date("d/m/y h:i:s"));

                        $this->db->where('id', $id);
                        $this->db->update('tbl_serviceProviderType', $data);
                      //  $this->session->set_flashdata('message','Your data updated Successfully..');
                        redirect('welcome/view_sprovider'); 
                        }
                      }

                 public function delete_sprovider($id)
                    {  
                    $this->db->where('id', $id);
                    $this->db->delete('tbl_serviceProviderType');
                    $this->session->set_flashdata('message', 'Provider deleted Successfully..');
                    redirect('welcome/view_sprovider');
                    }

                    public function delete_bulkprovider()
                    {
                      $this->input->post['hello'];
                      $data = $this->input->post('hello');
                    foreach($data as $dtd)
                    {                                  
                      $this->db->where('id', $dtd);
                      $this->db->delete('tbl_serviceProviderType');
                    }                            
                      // $this->session->set_flashdata('message', 'Country deleted Successfully..');
                      redirect('welcome/view_sprovider');
                    }

              /*   if (!$this->upload->do_upload('userfile'))
                {
                        $error = array('error' => $this->upload->display_errors());

                        $this->load->view('upload_form', $error);
                }
                else
                {
                        $data = array('upload_data' => $this->upload->data());

                        $this->load->view('upload_success', $data);
                }*/
                    
       /************************ End Service Provider Type *****************/

       /************************ Start ServiceProvider details *****************/

                  public function add_providerdetails()
                      {
                        $this->data['view_provider']    = $this->welcome->view_sprovider();
                        $this->data['view_catone']      = $this->welcome->view_catone();
                     // $this->data['view_roomtype']    = $this->welcome->view_roomtype();
                        $this->load->view('add_providerdetails',$this->data);
                      }

                  public function edit_providerdetails($id)
                    {
                    $this->data['view_provider']    = $this->welcome->view_sprovider();
                    $this->data['view_catone']      = $this->welcome->view_catone();
                    $this->data['view_providerdetails'] = $this->welcome->view_providerdetails();
                    $this->data['edit_data'] = $this->welcome->edit_providerdetails($id);
                    $this->load->view('edit_providerdetails', $this->data, FALSE);
                    }

                  public function view_providerdetails()
                    {
                      $this->data['view_providerdetails'] = $this->welcome->view_providerdetails();
                      $this->load->view('view_providerdetails',$this->data);
                    }

                  public function submit_providerdetails()
                      {
                        $config['upload_path']          = './uploads/';
                        $config['allowed_types']        = 'gif|jpg|png';
                        $config['max_size']             = '2048';
                 
                         $this->load->library('upload', $config);
                         $this->upload->do_upload('provider_image');   
                         $data_upload_files = $this->upload->data();                  

                      $data = array(
                      'provider_type'     => $this->input->post('providertype'),
                      'category_provider' => $this->input->post('catname'),
                      'status'            => $this->input->post('status'),
                      'provider_name'     => $this->input->post('provider_name'),
                      'provider_image'    => $data_upload_files['file_name'], 
                      'description'       => $this->input->post('desc'),
                      'city'              => $this->input->post('provider_city'),
                      'zipcode'           => $this->input->post('zipcode'),
                      'address'           => $this->input->post('address'),
                      'contact_name'      => $this->input->post('contact_name'),
                      'email'             => $this->input->post('email'),
                      'phone'             => $this->input->post('phone'),
                      'mobile'            => $this->input->post('mobile'),
                      'facebook'          => $this->input->post('fb'),
                      'twitter'           => $this->input->post('twitter'),
                      'googleplus'        => $this->input->post('googleplus'),
                      'linkedin'          => $this->input->post('linkedin'),
                      'instagram'         => $this->input->post('instagram'),
                      'tags'              => $this->input->post('tags'),
                      'policy'            => $this->input->post('policy'),
                      'tnc'               => $this->input->post('tnc'),
                      'created_date'      => date("d/m/y h:i:s"));
                       $insert = $this->welcome->insert_providerdetails($data);
                       redirect('welcome/add_providerdetails');
                       
                      }

                    public function update_providerdetails($id)
                   { 
                     $config['upload_path']     = './uploads/';
                     $config['allowed_types']   = 'gif|jpg|png';
                     $config['max_size']        = '2048';
                
                     $this->load->library('upload', $config);
                     $this->upload->do_upload('provider_image');

                   if(!empty($this->upload->do_upload('provider_image'))){
                      $data_upload_files = $this->upload->data();
                      {
                      $data = array(
                      'provider_type'     => $this->input->post('providertype'),
                      'category_provider' => $this->input->post('catname'),
                      'status'            => $this->input->post('status'),
                      'provider_name'     => $this->input->post('provider_name'),
                       'provider_image'   => $data_upload_files['file_name'],
                      'description'       => $this->input->post('desc'),
                      'city'              => $this->input->post('provider_city'),
                      'zipcode'           => $this->input->post('zipcode'),
                      'address'           => $this->input->post('address'),
                      'contact_name'      => $this->input->post('contact_name'),
                      'email'             => $this->input->post('email'),
                      'phone'             => $this->input->post('phone'),
                      'mobile'            => $this->input->post('mobile'),
                      'facebook'          => $this->input->post('fb'),
                      'twitter'           => $this->input->post('twitter'),
                      'googleplus'        => $this->input->post('googleplus'),
                      'linkedin'          => $this->input->post('linkedin'),
                      'instagram'         => $this->input->post('instagram'),
                      'tags'              => $this->input->post('tags'),
                      'policy'            => $this->input->post('policy'),
                      'tnc'               => $this->input->post('tnc'),
                      'created_date'      => date("d/m/y h:i:s"));

                       $this->db->where('id', $id);
                       $this->db->update('service_provider', $data);                      
                       redirect('welcome/view_providerdetails');
                         }

                       }
                        else
                        {
                          $data = array(
                      'provider_type'     => $this->input->post('providertype'),
                      'category_provider' => $this->input->post('catname'),
                      'status'            => $this->input->post('status'),
                      'provider_name'     => $this->input->post('provider_name'),
                      'description'       => $this->input->post('desc'),
                      'city'              => $this->input->post('provider_city'),
                      'zipcode'           => $this->input->post('zipcode'),
                      'address'           => $this->input->post('address'),
                      'contact_name'      => $this->input->post('contact_name'),
                      'email'             => $this->input->post('email'),
                      'phone'             => $this->input->post('phone'),
                      'mobile'            => $this->input->post('mobile'),
                      'facebook'          => $this->input->post('fb'),
                      'twitter'           => $this->input->post('twitter'),
                      'googleplus'        => $this->input->post('googleplus'),
                      'linkedin'          => $this->input->post('linkedin'),
                      'instagram'         => $this->input->post('instagram'),
                      'tags'              => $this->input->post('tags'),
                      'policy'            => $this->input->post('policy'),
                      'tnc'               => $this->input->post('tnc'),
                      'created_date'      => date("d/m/y h:i:s"));

                       $this->db->where('id', $id);
                       $this->db->update('service_provider', $data);                      
                       redirect('welcome/view_providerdetails');

                        }

                     }

                   public function delete_providerdetails($id)
                    {
                     $this->db->where('id', $id);
                     $this->db->delete('service_provider');                
                     redirect('welcome/view_providerdetails');          
                    }

                    public function delete_bulkproviderdetails()
                    {
                      $this->input->post['hello'];
                      $data = $this->input->post('hello');
                    foreach($data as $dtd)
                    {                                  
                      $this->db->where('id', $dtd);
                      $this->db->delete('service_provider');
                    }
                     // $this->session->set_flashdata('message', 'Country deleted Successfully..');
                      redirect('welcome/view_providerdetails');
                    }

                public function view_gallery()
                    {
                      $this->data['view_gallery'] = $this->welcome->view_gallery();
                      $this->load->view('view_gallery',$this->data);
                    }

                public function delete_gallery($id)
                    {
                     $this->db->where('id', $id);
                     $this->db->delete('files');                
                     redirect('welcome/view_gallery');          
                    }

               public function delete_bulkgallery()
                    { $this->input->post['gallery'];
                      $data = $this->input->post('gallery');
                    foreach($data as $dtd)
                    { $this->db->where('id', $dtd);
                      $this->db->delete('files');
                    }
                     
                      redirect('welcome/view_gallery');
                    }

                public function view_roomtype()
                    {
                      $this->data['view_providerdetails'] = $this->welcome->view_providerdetails();
                      $this->data['view_roomtype'] = $this->welcome->view_roomtype();
                      $this->load->view('view_roomtype',$this->data);
                    }

                public function add_roomtype()
                    {
                      $this->data['view_providerdetails'] = $this->welcome->view_providerdetails();
                      $this->data['view_catone']          = $this->welcome->view_catone();
                      $this->load->view('add_roomtype',$this->data);
                    }

                public function submit_roomtype()
                  {
                    $data = array(
                    'provider_id'                 => $this->input->post('providerid'),
                    'provider_category'           => $this->input->post('catname'),
                    'room_type'                   => $this->input->post('roomtype'),
                    'room_desc'                   => $this->input->post('desc'),
                    'wakeup_calls'                => $this->input->post('wakeupcalls'),
                    'bathrobes'                   => $this->input->post('bathrobes'),
                    'lcd_tv'                      => $this->input->post('lcd'),
                    'inroom_childcare'            => $this->input->post('inroomchildcare'),
                    'housekeeping'                => $this->input->post('housekeeping'),
                    'pets'                        => $this->input->post('pets'),
                    'elevator'                    => $this->input->post('elevator'),
                    'card'                        => $this->input->post('card'),
                    'ac'                          => $this->input->post('ac'),
                    'spa'                         => $this->input->post('spa'),
                    'gym'                         => $this->input->post('fitness'),
                    'inside_parking'              => $this->input->post('insideparking')
                    , 'delivery_mon_selected'     => $this->input->post('delivery_mon_selected')
                    , 'delivery_tue_selected'     => $this->input->post('delivery_tue_selected')
                    , 'delivery_wed_selected'     => $this->input->post('delivery_wed_selected')
                    , 'delivery_thu_selected'     => $this->input->post('delivery_thu_selected')
                    , 'delivery_fri_selected'     => $this->input->post('delivery_fri_selected')
                    , 'delivery_sat_selected'     => $this->input->post('delivery_sat_selected')
                    , 'delivery_sun_selected'     => $this->input->post('delivery_sun_selected')
                    , 'delivery_sun_openFirst'    => $this->input->post('delivery_sun_openFirst')
                    , 'delivery_sun_openFirstMin' => $this->input->post('delivery_sun_openFirstMin')
                    , 'delivery_sun_openFirstMinAM'=> $this->input->post('delivery_sun_openFirstMinAM')
                    , 'delivery_sun_closeFirst'   => $this->input->post('delivery_sun_closeFirst')
                    , 'delivery_sun_closeFirstdMin'=> $this->input->post('delivery_sun_closeFirstdMin')
                    , 'delivery_sun_closeFirstMinPM'=> $this->input->post('delivery_sun_closeFirstMinPM')
                    , 'delivery_sun_openSecond'   => $this->input->post('delivery_sun_openSecond')
                    , 'delivery_sun_openSecondMin'=> $this->input->post('delivery_sun_openSecondMin')
                    , 'delivery_sun_openSecondMinAM'=> $this->input->post('delivery_sun_openSecondMinAM')
                    , 'delivery_sun_closeSecond'   => $this->input->post('delivery_sun_closeSecond')
                    , 'delivery_sun_closeSecondMin'=> $this->input->post('delivery_sun_closeSecondMin')
                    , 'delivery_sun_closeSecondMinPM'=> $this->input->post('delivery_sun_closeSecondMinPM')
                    , 'delivery_sat_openFirst'     => $this->input->post('delivery_sat_openFirst')
                    , 'delivery_sat_openFirstMin'  => $this->input->post('delivery_sat_openFirstMin')
                    , 'delivery_sat_openFirstMinAM'=> $this->input->post('delivery_sat_openFirstMinAM')
                    , 'delivery_sat_closeFirst'    => $this->input->post('delivery_sat_closeFirst')
                    , 'delivery_sat_closeFirstdMin'=> $this->input->post('delivery_sat_closeFirstdMin')
                    , 'delivery_sat_closeFirstMinPM'=> $this->input->post('delivery_sat_closeFirstMinPM')
                    , 'delivery_sat_openSecond'    => $this->input->post('delivery_sat_openSecond')
                    , 'delivery_sat_openSecondMin' => $this->input->post('delivery_sat_openSecondMin')
                    , 'delivery_sat_openSecondMinAM'=> $this->input->post('delivery_sat_openSecondMinAM')
                    , 'delivery_sat_closeSecond'   => $this->input->post('delivery_sat_closeSecond')
                    , 'delivery_sat_closeSecondMin'=> $this->input->post('delivery_sat_closeSecondMin')
                    , 'delivery_sat_closeSecondMinPM'=> $this->input->post('delivery_sat_closeSecondMinPM')
                    , 'delivery_fri_openFirst'     => $this->input->post('delivery_fri_openFirst')
                    , 'delivery_fri_openFirstMin'  => $this->input->post('delivery_fri_openFirstMin')
                    , 'delivery_fri_openFirstMinAM'=> $this->input->post('delivery_fri_openFirstMinAM')
                    , 'delivery_fri_closeFirst'    => $this->input->post('delivery_fri_closeFirst')
                    , 'delivery_fri_closeFirstdMin'=> $this->input->post('delivery_fri_closeFirstdMin')
                    , 'delivery_fri_closeFirstMinPM'=> $this->input->post('delivery_fri_closeFirstMinPM')
                    , 'delivery_fri_openSecond'    => $this->input->post('delivery_fri_openSecond')
                    , 'delivery_fri_openSecondMin'=> $this->input->post('delivery_fri_openSecondMin')
                    , 'delivery_fri_openSecondMinAM'=> $this->input->post('delivery_fri_openSecondMinAM')
                    , 'delivery_fri_closeSecond'   => $this->input->post('delivery_fri_closeSecond')
                    , 'delivery_fri_closeSecondMin'=> $this->input->post('delivery_fri_closeSecondMin')
                    , 'delivery_fri_closeSecondMinPM'=> $this->input->post('delivery_fri_closeSecondMinPM')
                    , 'delivery_thu_openFirst'     => $this->input->post('delivery_thu_openFirst')
                    , 'delivery_thu_openFirstMin'=> $this->input->post('delivery_thu_openFirstMin')
                    , 'delivery_thu_openFirstMinAM'=> $this->input->post('delivery_thu_openFirstMinAM')
                    , 'delivery_thu_closeFirst'    => $this->input->post('delivery_thu_closeFirst')
                    , 'delivery_thu_closeFirstdMin'=> $this->input->post('delivery_thu_closeFirstdMin')
                    , 'delivery_thu_closeFirstMinPM'=> $this->input->post('delivery_thu_closeFirstMinPM')
                    , 'delivery_thu_openSecond'     => $this->input->post('delivery_thu_openSecond')
                    , 'delivery_thu_openSecondMin'  => $this->input->post('delivery_thu_openSecondMin')
                    , 'delivery_thu_openSecondMinAM'=> $this->input->post('delivery_thu_openSecondMinAM')
                    , 'delivery_thu_closeSecond'   => $this->input->post('delivery_thu_closeSecond')
                    , 'delivery_thu_closeSecondMin'=> $this->input->post('delivery_thu_closeSecondMin')
                    , 'delivery_thu_closeSecondMinPM'=> $this->input->post('delivery_thu_closeSecondMinPM')
                    , 'delivery_wed_openFirst'      => $this->input->post('delivery_wed_openFirst')
                    , 'delivery_wed_openFirstMin'   => $this->input->post('delivery_wed_openFirstMin')
                    , 'delivery_wed_openFirstMinAM' => $this->input->post('delivery_wed_openFirstMinAM')
                    , 'delivery_wed_closeFirst'     => $this->input->post('delivery_wed_closeFirst')
                    , 'delivery_wed_closeFirstdMin' => $this->input->post('delivery_wed_closeFirstdMin')
                    , 'delivery_wed_closeFirstMinPM'=> $this->input->post('delivery_wed_closeFirstMinPM')
                    , 'delivery_wed_openSecond'     => $this->input->post('delivery_wed_openSecond')
                    , 'delivery_wed_openSecondMin'  => $this->input->post('delivery_wed_openSecondMin')
                    , 'delivery_wed_openSecondMinAM'=> $this->input->post('delivery_wed_openSecondMinAM')
                    , 'delivery_wed_closeSecond'    => $this->input->post('delivery_wed_closeSecond')
                    , 'delivery_wed_closeSecondMin' => $this->input->post('delivery_wed_closeSecondMin')
                    , 'delivery_wed_closeSecondMinPM'=> $this->input->post('delivery_wed_closeSecondMinPM')
                    , 'delivery_tue_openFirst'      => $this->input->post('delivery_tue_openFirst')
                    , 'delivery_tue_openFirstMin'   => $this->input->post('delivery_tue_openFirstMin')
                    , 'delivery_tue_openFirstMinAM' => $this->input->post('delivery_tue_openFirstMinAM')
                    , 'delivery_tue_closeFirst'     => $this->input->post('delivery_tue_closeFirst')
                    , 'delivery_tue_closeFirstdMin' => $this->input->post('delivery_tue_closeFirstdMin')
                    , 'delivery_tue_closeFirstMinPM'=> $this->input->post('delivery_tue_closeFirstMinPM')
                    , 'delivery_tue_openSecond'     => $this->input->post('delivery_tue_openSecond')
                    , 'delivery_tue_openSecondMin'  => $this->input->post('delivery_tue_openSecondMin')
                    , 'delivery_tue_openSecondMinAM'=> $this->input->post('delivery_tue_openSecondMinAM')
                    , 'delivery_tue_closeSecond'    => $this->input->post('delivery_tue_closeSecond')
                    , 'delivery_tue_closeSecondMin' => $this->input->post('delivery_tue_closeSecondMin')
                    , 'delivery_tue_closeSecondMinPM'=> $this->input->post('delivery_tue_closeSecondMinPM')
                    , 'delivery_mon_openFirst'      => $this->input->post('delivery_mon_openFirst')
                    , 'delivery_mon_openFirstMin'   => $this->input->post('delivery_mon_openFirstMin')
                    , 'delivery_mon_openFirstMinAM' => $this->input->post('delivery_mon_openFirstMinAM')
                    , 'delivery_mon_closeFirst'     => $this->input->post('delivery_mon_closeFirst')
                    , 'delivery_mon_closeFirstdMin' => $this->input->post('delivery_mon_closeFirstdMin')
                    , 'delivery_mon_closeFirstMinPM'=> $this->input->post('delivery_mon_closeFirstMinPM')
                    , 'delivery_mon_openSecond'     => $this->input->post('delivery_mon_openSecond')
                    , 'delivery_mon_openSecondMin'  => $this->input->post('delivery_mon_openSecondMin')
                    , 'delivery_mon_openSecondMinAM'=> $this->input->post('delivery_mon_openSecondMinAM')
                    , 'delivery_mon_closeSecond'    => $this->input->post('delivery_mon_closeSecond')
                    , 'delivery_mon_closeSecondMin' => $this->input->post('delivery_mon_closeSecondMin')
                    , 'delivery_mon_closeSecondMinPM' => $this->input->post('delivery_mon_closeSecondMinPM'),
                      'status'                      => $this->input->post('status'),
                      'created_date'                => date("d/m/y h:i:s"));

                    $insert = $this->welcome->insert_roomtype($data);
                    redirect('welcome/add_roomtype');
                  }

                  public function edit_roomtype($id)
                    {                   
                    $this->data['view_catone']        = $this->welcome->view_catone();
                    $this->data['view_providerdetails'] = $this->welcome->view_providerdetails();
                    $this->data['edit_data'] = $this->welcome->edit_roomtype($id);
                    $this->load->view('edit_roomtype', $this->data, FALSE);
                    }

                  public function update_roomtype($id)
              {     $data = array(
                    'provider_id'                 => $this->input->post('providerid'),
                    'provider_category'           => $this->input->post('catname'),
                    'room_type'                   => $this->input->post('roomtype'),
                    'room_desc'                   => $this->input->post('desc'),
                    'wakeup_calls'                => $this->input->post('wakeupcalls'),
                    'bathrobes'                   => $this->input->post('bathrobes'),
                    'lcd_tv'                      => $this->input->post('lcd'),
                    'inroom_childcare'            => $this->input->post('inroomchildcare'),
                    'housekeeping'                => $this->input->post('housekeeping'),
                    'pets'                        => $this->input->post('pets'),
                    'elevator'                    => $this->input->post('elevator'),
                    'card'                        => $this->input->post('card'),
                    'ac'                          => $this->input->post('ac'),
                    'spa'                         => $this->input->post('spa'),
                    'gym'                         => $this->input->post('fitness'),
                    'inside_parking'              => $this->input->post('insideparking')
                    , 'delivery_mon_selected'     => $this->input->post('delivery_mon_selected')
                    , 'delivery_tue_selected'     => $this->input->post('delivery_tue_selected')
                    , 'delivery_wed_selected'     => $this->input->post('delivery_wed_selected')
                    , 'delivery_thu_selected'     => $this->input->post('delivery_thu_selected')
                    , 'delivery_fri_selected'     => $this->input->post('delivery_fri_selected')
                    , 'delivery_sat_selected'     => $this->input->post('delivery_sat_selected')
                    , 'delivery_sun_selected'     => $this->input->post('delivery_sun_selected')
                    , 'delivery_sun_openFirst'    => $this->input->post('delivery_sun_openFirst')
                    , 'delivery_sun_openFirstMin' => $this->input->post('delivery_sun_openFirstMin')
                    , 'delivery_sun_openFirstMinAM'=> $this->input->post('delivery_sun_openFirstMinAM')
                    , 'delivery_sun_closeFirst'   => $this->input->post('delivery_sun_closeFirst')
                    , 'delivery_sun_closeFirstdMin'=> $this->input->post('delivery_sun_closeFirstdMin')
                    , 'delivery_sun_closeFirstMinPM'=> $this->input->post('delivery_sun_closeFirstMinPM')
                    , 'delivery_sun_openSecond'   => $this->input->post('delivery_sun_openSecond')
                    , 'delivery_sun_openSecondMin'=> $this->input->post('delivery_sun_openSecondMin')
                    , 'delivery_sun_openSecondMinAM'=> $this->input->post('delivery_sun_openSecondMinAM')
                    , 'delivery_sun_closeSecond'   => $this->input->post('delivery_sun_closeSecond')
                    , 'delivery_sun_closeSecondMin'=> $this->input->post('delivery_sun_closeSecondMin')
                    , 'delivery_sun_closeSecondMinPM'=> $this->input->post('delivery_sun_closeSecondMinPM')
                    , 'delivery_sat_openFirst'     => $this->input->post('delivery_sat_openFirst')
                    , 'delivery_sat_openFirstMin'  => $this->input->post('delivery_sat_openFirstMin')
                    , 'delivery_sat_openFirstMinAM'=> $this->input->post('delivery_sat_openFirstMinAM')
                    , 'delivery_sat_closeFirst'    => $this->input->post('delivery_sat_closeFirst')
                    , 'delivery_sat_closeFirstdMin'=> $this->input->post('delivery_sat_closeFirstdMin')
                    , 'delivery_sat_closeFirstMinPM'=> $this->input->post('delivery_sat_closeFirstMinPM')
                    , 'delivery_sat_openSecond'    => $this->input->post('delivery_sat_openSecond')
                    , 'delivery_sat_openSecondMin' => $this->input->post('delivery_sat_openSecondMin')
                    , 'delivery_sat_openSecondMinAM'=> $this->input->post('delivery_sat_openSecondMinAM')
                    , 'delivery_sat_closeSecond'   => $this->input->post('delivery_sat_closeSecond')
                    , 'delivery_sat_closeSecondMin'=> $this->input->post('delivery_sat_closeSecondMin')
                    , 'delivery_sat_closeSecondMinPM'=> $this->input->post('delivery_sat_closeSecondMinPM')
                    , 'delivery_fri_openFirst'     => $this->input->post('delivery_fri_openFirst')
                    , 'delivery_fri_openFirstMin'  => $this->input->post('delivery_fri_openFirstMin')
                    , 'delivery_fri_openFirstMinAM'=> $this->input->post('delivery_fri_openFirstMinAM')
                    , 'delivery_fri_closeFirst'    => $this->input->post('delivery_fri_closeFirst')
                    , 'delivery_fri_closeFirstdMin'=> $this->input->post('delivery_fri_closeFirstdMin')
                    , 'delivery_fri_closeFirstMinPM'=> $this->input->post('delivery_fri_closeFirstMinPM')
                    , 'delivery_fri_openSecond'    => $this->input->post('delivery_fri_openSecond')
                    , 'delivery_fri_openSecondMin'=> $this->input->post('delivery_fri_openSecondMin')
                    , 'delivery_fri_openSecondMinAM'=> $this->input->post('delivery_fri_openSecondMinAM')
                    , 'delivery_fri_closeSecond'   => $this->input->post('delivery_fri_closeSecond')
                    , 'delivery_fri_closeSecondMin'=> $this->input->post('delivery_fri_closeSecondMin')
                    , 'delivery_fri_closeSecondMinPM'=> $this->input->post('delivery_fri_closeSecondMinPM')
                    , 'delivery_thu_openFirst'     => $this->input->post('delivery_thu_openFirst')
                    , 'delivery_thu_openFirstMin'=> $this->input->post('delivery_thu_openFirstMin')
                    , 'delivery_thu_openFirstMinAM'=> $this->input->post('delivery_thu_openFirstMinAM')
                    , 'delivery_thu_closeFirst'    => $this->input->post('delivery_thu_closeFirst')
                    , 'delivery_thu_closeFirstdMin'=> $this->input->post('delivery_thu_closeFirstdMin')
                    , 'delivery_thu_closeFirstMinPM'=> $this->input->post('delivery_thu_closeFirstMinPM')
                    , 'delivery_thu_openSecond'     => $this->input->post('delivery_thu_openSecond')
                    , 'delivery_thu_openSecondMin'  => $this->input->post('delivery_thu_openSecondMin')
                    , 'delivery_thu_openSecondMinAM'=> $this->input->post('delivery_thu_openSecondMinAM')
                    , 'delivery_thu_closeSecond'   => $this->input->post('delivery_thu_closeSecond')
                    , 'delivery_thu_closeSecondMin'=> $this->input->post('delivery_thu_closeSecondMin')
                    , 'delivery_thu_closeSecondMinPM'=> $this->input->post('delivery_thu_closeSecondMinPM')
                    , 'delivery_wed_openFirst'      => $this->input->post('delivery_wed_openFirst')
                    , 'delivery_wed_openFirstMin'   => $this->input->post('delivery_wed_openFirstMin')
                    , 'delivery_wed_openFirstMinAM' => $this->input->post('delivery_wed_openFirstMinAM')
                    , 'delivery_wed_closeFirst'     => $this->input->post('delivery_wed_closeFirst')
                    , 'delivery_wed_closeFirstdMin' => $this->input->post('delivery_wed_closeFirstdMin')
                    , 'delivery_wed_closeFirstMinPM'=> $this->input->post('delivery_wed_closeFirstMinPM')
                    , 'delivery_wed_openSecond'     => $this->input->post('delivery_wed_openSecond')
                    , 'delivery_wed_openSecondMin'  => $this->input->post('delivery_wed_openSecondMin')
                    , 'delivery_wed_openSecondMinAM'=> $this->input->post('delivery_wed_openSecondMinAM')
                    , 'delivery_wed_closeSecond'    => $this->input->post('delivery_wed_closeSecond')
                    , 'delivery_wed_closeSecondMin' => $this->input->post('delivery_wed_closeSecondMin')
                    , 'delivery_wed_closeSecondMinPM'=> $this->input->post('delivery_wed_closeSecondMinPM')
                    , 'delivery_tue_openFirst'      => $this->input->post('delivery_tue_openFirst')
                    , 'delivery_tue_openFirstMin'   => $this->input->post('delivery_tue_openFirstMin')
                    , 'delivery_tue_openFirstMinAM' => $this->input->post('delivery_tue_openFirstMinAM')
                    , 'delivery_tue_closeFirst'     => $this->input->post('delivery_tue_closeFirst')
                    , 'delivery_tue_closeFirstdMin' => $this->input->post('delivery_tue_closeFirstdMin')
                    , 'delivery_tue_closeFirstMinPM'=> $this->input->post('delivery_tue_closeFirstMinPM')
                    , 'delivery_tue_openSecond'     => $this->input->post('delivery_tue_openSecond')
                    , 'delivery_tue_openSecondMin'  => $this->input->post('delivery_tue_openSecondMin')
                    , 'delivery_tue_openSecondMinAM'=> $this->input->post('delivery_tue_openSecondMinAM')
                    , 'delivery_tue_closeSecond'    => $this->input->post('delivery_tue_closeSecond')
                    , 'delivery_tue_closeSecondMin' => $this->input->post('delivery_tue_closeSecondMin')
                    , 'delivery_tue_closeSecondMinPM'=> $this->input->post('delivery_tue_closeSecondMinPM')
                    , 'delivery_mon_openFirst'      => $this->input->post('delivery_mon_openFirst')
                    , 'delivery_mon_openFirstMin'   => $this->input->post('delivery_mon_openFirstMin')
                    , 'delivery_mon_openFirstMinAM' => $this->input->post('delivery_mon_openFirstMinAM')
                    , 'delivery_mon_closeFirst'     => $this->input->post('delivery_mon_closeFirst')
                    , 'delivery_mon_closeFirstdMin' => $this->input->post('delivery_mon_closeFirstdMin')
                    , 'delivery_mon_closeFirstMinPM'=> $this->input->post('delivery_mon_closeFirstMinPM')
                    , 'delivery_mon_openSecond'     => $this->input->post('delivery_mon_openSecond')
                    , 'delivery_mon_openSecondMin'  => $this->input->post('delivery_mon_openSecondMin')
                    , 'delivery_mon_openSecondMinAM'=> $this->input->post('delivery_mon_openSecondMinAM')
                    , 'delivery_mon_closeSecond'    => $this->input->post('delivery_mon_closeSecond')
                    , 'delivery_mon_closeSecondMin' => $this->input->post('delivery_mon_closeSecondMin')
                    , 'delivery_mon_closeSecondMinPM' => $this->input->post('delivery_mon_closeSecondMinPM'),
                      'status'                      => $this->input->post('status'),
                      'created_date'                => date("d/m/y h:i:s"));

                    $this->db->where('id', $id);
                    $this->db->update('tbl_room', $data);
                    redirect('welcome/view_roomtype');
                  }

                 public function delete_roomtype($id)
                  {
                   $this->db->where('id', $id);
                   $this->db->delete('tbl_room');
               // $this->session->set_flashdata('message', 'Provider deleted Successfully..');
                   redirect('welcome/view_roomtype');          
                  }

                  public function delete_bulkroomtype()
                    { $this->input->post['roomtype'];
                      $data = $this->input->post('roomtype');
                    foreach($data as $dtd)
                    { $this->db->where('id', $dtd);
                      $this->db->delete('tbl_room');
                    }
                     
                      redirect('welcome/view_roomtype');
                    }

           /************************ End Room type details *****************/

           /***********************  Start Country Module *****************/
              
              public function view_country()
                   {
                   $this->data['view_countries'] = $this->welcome->view_country();
                   $this->load->view('view_country',$this->data);
                   }
        
              public function add_country()
                  {
                   $this->load->view('add_country');
                  }

              public function submit_country()
                {
                  $data = array('country' => $this->input->post('country'),
                       'status'           => '1',
                       'created_date'     => date("y/m/d h:i:s"));

                  $insert = $this->welcome->insert_country($data);
                // $this->session->set_flashdata('message', 'Your data inserted Successfully..');
                  redirect('welcome/view_country');
                }

              public function edit_country($id)
                  {
                  $this->data['edit_data'] = $this->welcome->edit_country($id);
                  $this->load->view('edit_country', $this->data, FALSE);
                  }

             public function update_country($id)
              {
              $data = array('status'  => $this->input->post('status'),
                            'country' => $this->input->post('country'));
              $this->db->where('id', $id);
              $this->db->update('tbl_country', $data);
            // $this->session->set_flashdata('message', 'Your data updated Successfully..');
              redirect('welcome/view_country');
              }

            public function delete_country($id)
              {
              $this->db->where('id', $id);
              $this->db->delete('tbl_country');
             // $this->session->set_flashdata('message', 'Country deleted Successfully..');
              redirect('welcome/view_country');
              }

              public function delete_bulkcountry()
                    { $this->input->post['country'];
                      $data = $this->input->post('country');
                    foreach($data as $dtd)
                    { $this->db->where('id', $dtd);
                      $this->db->delete('tbl_country');
                    }
                     // $this->session->set_flashdata('message', 'Country deleted Successfully..');
                      redirect('welcome/view_country');
                    }

   /***************  End Country Module ******************/

   /**************  Start State Module ***********/

            public function view_state()
                   {
                   $this->data['view_state'] = $this->welcome->view_state();
                   $this->load->view('view_state', $this->data);
                   }
        
            public function add_state()
                {
                  $this->data['fetch'] = $this->welcome->view_country();
                  $this->load->view('add_state', $this->data);
                }

            public function submit_state()
               {
                  $data = array('countryID'           => $this->input->post('country_id'),
                                'stateName'           => $this->input->post('state'),
                                'status'              => $this->input->post('status'),
                                'created_date'        => date("y/m/d h:i:s"));
                  $insert = $this->welcome->insert_state($data);
          // $this->session->set_flashdata('message', 'Your data inserted Successfully..');
                  redirect('welcome/view_state');
              }

           public function edit_state($id)
              {
              $this->data['view_countries'] = $this->welcome->view_country();
              $this->data['edit_state'] = $this->welcome->edit_state($id);
              $this->load->view('edit_state', $this->data, FALSE);              
              }

          public function update_state($id)
              {
             $data = array('stateName'    => $this->input->post('editstatename'),
                          'countryID'     => $this->input->post('editcountryid'),
                          'status'        => $this->input->post('status'),
                          'created_date'  => date("m/d/y h:i:s"));
                         
              $this->db->where('id', $id);
              $this->db->update('tbl_state', $data);
          //    $this->session->set_flashdata('message', 'Your data updated Successfully..');
              redirect('welcome/view_state');
              }

        public function delete_state($id)
          {
          $this->db->where('id', $id);
          $this->db->delete('tbl_state');
          $this->session->set_flashdata('message', 'State deleted Successfully..');
          redirect('welcome/view_state');
          }

          public function delete_bulkstate()
                    { $this->input->post['states'];
                      $data = $this->input->post('states');
                    foreach($data as $dtd)
                    { $this->db->where('id', $dtd);
                      $this->db->delete('tbl_state');
                    }
                     // $this->session->set_flashdata('message', 'Country deleted Successfully..');
                      redirect('welcome/view_state');
                    }

      /***********************  End State Module  *****************/

      /***********************  Start City Module *****************/
            public function view_city()
             {
             $this->data['view_city'] = $this->welcome->view_city();
             $this->load->view('view_city', $this->data);
             }

       /***********************  Add City Function with JS Starts*****************/

              public function add_city()
                {
                $result['list'] = $this->welcome->getCountry();     
                $this->load->view('add_city',$result);         
                }

              public function loadData()
              {
                $loadType=$_POST['loadType'];
                $loadId=$_POST['loadId'];

               /* $this->load->welcome('model');*/
                $result = $this->welcome->getData($loadType,$loadId);
                $HTML="";

                if($result->num_rows() > 0){
                  foreach($result->result() as $list){
                    $HTML.="<option value='".$list->id."'>".$list->name."</option>";
                  }
                }
                echo $HTML;
              }

        /***********************  Add City Function with JS End*****************/

          public function submit_city()
              {
                $data = array(
                  'countryID'        => $this->input->post('country'),
                  'stateID'          => $this->input->post('state'),
                  'cityName'         => $this->input->post('city'),
                  'status'           => $this->input->post('status'),
                  'created_date'     => date("y/m/d h:i:s"));

              $insert = $this->welcome->insert_city($data);
              // $this->session->set_flashdata('message', 'Your data inserted Successfully..');
              redirect('welcome/add_city');
              }

           public function edit_city($cityid)
            {
            $this->data['listing'] =  array($this->welcome->getCountry());
            $this->data['view_countries'] = $this->welcome->view_country();
            $this->data['view_state'] = $this->welcome->view_state();
            $this->data['edit_city'] = $this->welcome->edit_city($cityid);
            $this->load->view('edit_city',$this->data,FALSE);

$onlystate = $this->db->query("SELECT stateID FROM `tbl_city` AS `c` LEFT JOIN `tbl_state` AS `b`
ON `c`.`stateID` = `b`.`id` LEFT JOIN `tbl_country` AS `a` ON `b`.`countryID` = `a`.`id` 
WHERE `c`.`cityid` ='$cityid'");

    if(!empty($_POST['country_id'])) {  
  $results = $this->db->query("SELECT * FROM `tbl_state` WHERE `countryID` = '" . $_POST['country_id'] . "'");
  ?>
<option value="">Select State</option>
  <?php foreach($results->result_array() as $b=>$c) 
{ ?>
 <option value="<?php echo $c['id']; ?>"><?php echo $c['stateName'];?>
 
 </option>
<?php 
 }}}

/* public function edit_citycountry()
{
if(!empty($_POST['country_id'])) {  
  $results = $this->db->query("SELECT * FROM `tbl_state` WHERE `countryID` = '" . $_POST['country_id'] . "'");
?>
<option value="">Select State</option>
<?php   foreach($results->result_array() as $b=>$c) { ?>
 <option value="<?php echo $c['id']; ?>"><?php echo $c['stateName']; ?></option>
<?php }}} */

         public function update_city($cityid)
            {
            $data = array(
               'countryID'        => $this->input->post('editcountryid'),
               'stateID'          => $this->input->post('editstateid'),
               'cityName'         => $this->input->post('cityname'),
               'status'           => $this->input->post('status'),
               'created_date'     => date("y/m/d h:i:s"));

               $this->db->where('cityid', $cityid);
               $this->db->update('tbl_city', $data);
              // $this->session->set_flashdata('message', 'Your data updated Successfully..');
               redirect('welcome/view_city');
            }

           public function delete_city($id)
            {
            $this->db->where('cityid', $id);
            $this->db->delete('tbl_city');
            $this->session->set_flashdata('message', 'City deleted Successfully..');
            redirect('welcome/view_city');
            }

              public function delete_bulkcity()
                    {
                      $this->input->post['allcity'];
                      $data = $this->input->post('allcity');
                    foreach($data as $dtd){                                  
                      $this->db->where('cityid', $dtd);
                      $this->db->delete('tbl_city');
                    }                            
                      // $this->session->set_flashdata('message', 'Country deleted Successfully..');
                      redirect('welcome/view_city');
                    }

   /****************************  End City Module ****************************/

   /****************************  Catone Module Starts ****************************/

                      public function add_catone()
                      {            
                      $this->data['view_catone']= $this->welcome->view_catone();       
                      $this->load->view('add_catone' , $this->data);         
                      }

                      public function view_catone()
                        {
                          $this->data['view_catone'] = $this->welcome->view_catone();
                          $this->load->view('view_catone', $this->data);
                        }

                    public function submit_catone()
                      {
                      $data = array('catonename'     => $this->input->post('fcatname'),
                                    'status'         => $this->input->post('status'),
                                    'created_date'   => date("m/d/y h:i:s"));
                      $insert = $this->welcome->insert_catone($data);
                  //  $this->session->set_flashdata('message', 'Your data inserted Successfully..');
                      redirect('welcome/add_catone');
                      }
      
                    public function edit_catone($id)
                    {
                      $this->data['edit_catone'] = $this->welcome->edit_catone($id);
                      $this->load->view('edit_catone', $this->data, FALSE);
                    }

                 public function update_catone($id)
                  {
                  $data = array('catonename'     => $this->input->post('editcategory'),
                                'status'         => $this->input->post('status'),
                                'created_date'   => date("m/d/y h:i:s"));
                
                  $this->db->where('id', $id);
                  $this->db->update('tbl_catone', $data);
              //  $this->session->set_flashdata('message', 'Your data updated Successfully..');
                  redirect('welcome/view_catone');
                  }

                  public function delete_catone($id)
                    {
                    $this->db->where('id', $id);
                    $this->db->delete('tbl_catone');
                    // $this->session->set_flashdata('message', 'Your data deleted Successfully..');
                    redirect('welcome/view_catone');
                    }

                public function delete_bulkcatone()
                    {
                      $this->input->post['catone'];
                      $data = $this->input->post('catone');
                    foreach($data as $dtd){                                  
                      $this->db->where('id', $dtd);
                      $this->db->delete('tbl_catone');
                    }                            
                      // $this->session->set_flashdata('message', 'Country deleted Successfully..');
                      redirect('welcome/view_catone');
                    }

        /****************************  Catone Module End ***************************/    

        /****************************  Cattwo Module Starts ****************************/
                  public function add_cattwo()
                    {
                    $this->data['view_catone'] = $this->welcome->view_catone();       
                    $this->load->view('add_cattwo', $this->data);
                    }

                public function view_cattwo()
                {
                 $this->data['view_cattwo'] = $this->welcome->view_cattwo();
                 $this->load->view('view_cattwo', $this->data);
                }

             public function submit_cattwo()
              {
              $data = array(
                'catone_Id'      => $this->input->post('firstcatid'),
                'cattwoname'     => $this->input->post('scatname'),
                'status'         => $this->input->post('status'),
                'created_date'   => date("m/d/y h:i:s"));
              $insert = $this->welcome->insert_cattwo($data);
             // $this->session->set_flashdata('message', 'Your data inserted Successfully..');
              redirect('welcome/add_cattwo');
              }
      
            public function edit_cattwo($id)
            {
              $this->data['view_catone'] = $this->welcome->view_catone();
              $this->data['edit_cattwo'] = $this->welcome->edit_cattwo($id);
              $this->load->view('edit_cattwo', $this->data, FALSE);
            }

           public function update_cattwo($id)
            {
              $data = array('catone_Id'     => $this->input->post('maincatid'),
                            'cattwoname'     => $this->input->post('subcatname'),
                            'status'         => $this->input->post('status'),
                            'created_date'   => date("m/d/y h:i:s"));
                         
              $this->db->where('cattwo_id', $id);
              $this->db->update('tbl_cattwo',$data);          
              redirect('welcome/view_cattwo');
            }

          public function delete_cattwo($id)
            {
            $this->db->where('cattwo_id', $id);
            $this->db->delete('tbl_cattwo');
           //  $this->session->set_flashdata('message', 'Your data deleted Successfully..');
            redirect('welcome/view_cattwo');
            }

        /****************************  Cattwo Module End ***************************/  

        /****************************  Catthree Module Starts ****************************/
            public function add_catthree()
              {                   
              $this->load->view('add_catthree');         
              }

            public function view_catthree()
              {
               $this->data['view_catthree']= $this->welcome->view_catthree();
                $this->load->view('view_catthree' , $this->data);
              }

            public function submit_catthree()
              {
              $data = array('catonename'     => $this->input->post('fcatname'),
                            'status'         => $this->input->post('status'),
                            'created_date'   => date("m/d/y h:i:s"));
              $insert = $this->welcome->insert_catone($data);
       //     $this->session->set_flashdata('message','Your data inserted Successfully..');
              redirect('welcome/add_catthree');
              }

            public function edit_catthree($id)
              {
                $this->data['edit_data'] = $this->welcome->edit_data($id);
                $this->load->view('edit', $this->data, FALSE);
              }

            public function update_catthree($id)
              {
              $data = array('catonename'        => $this->input->post('fcatname'),
                            'status'         => $this->input->post('status'),
                            'created_date'   => date("m/d/y h:i:s"));

            $this->db->where('id', $id);
            $this->db->update('tbl_catthree', $data);
            $this->session->set_flashdata('message', 'Your data updated Successfully..');
            redirect('welcome/index');
              }

          public function delete_catthree($id)
            {
            $this->db->where('id', $id);
            $this->db->delete('tbl_catthree');
           // $this->session->set_flashdata('message', 'Your data deleted Successfully..');
            redirect('welcome/index');
            }            
        /****************************  Catthree Module End ***************************/ 

      /**************************** CMS_aboutus start ***************************/

            public function cms_aboutus()
              {
              $this->data['aboutusview']= $this->welcome->view_aboutus();      
              $this->load->view('cms_aboutus', $this->data);         
              }

            public function insert_cms_aboutus()
              {
              $data = array(
                'content_description'     => $this->input->post('content_description'),
                'content_TitleMeta'       => $this->input->post('content_TitleMeta'),
                'content_KeywordMeta'     => $this->input->post('content_KeywordMeta'),
                'content_DescriptionMeta' => $this->input->post('content_DescriptionMeta'));

              $insertandupdate = $this->welcome->insert_aboutus($data);
              redirect('welcome/cms_aboutus');
              }

          public function update_cms_aboutus($id)
              {
                $data = array('content_description' => $this->input->post('content_description'),
                'content_TitleMeta' => $this->input->post('content_TitleMeta'),
                'content_KeywordMeta' => $this->input->post('content_KeywordMeta'),
                'content_DescriptionMeta' => $this->input->post('content_DescriptionMeta'));
                $this->db->where('id!=',$id);
                $this->db->update('cms_aboutus', $data); 
                redirect('welcome/cms_aboutus');
              }
        /**************************** CMS_aboutus End ***************************/ 

        /**************************** CMS_services start ***************************/

            public function cms_services()
              {
              $this->data['aboutusview']= $this->welcome->view_services();      
              $this->load->view('cms_services', $this->data);         
              }

            public function insert_cms_services()
              {
              $data = array(
                'content_description'     => $this->input->post('content_description'),
                'content_TitleMeta'       => $this->input->post('content_TitleMeta'),
                'content_KeywordMeta'     => $this->input->post('content_KeywordMeta'),
                'content_DescriptionMeta' => $this->input->post('content_DescriptionMeta'));

              $insertandupdate = $this->welcome->insert_services($data);
              redirect('welcome/cms_services');
              }

          public function update_cms_services($id)
              {
                  $data = array('content_description' => $this->input->post('content_description'),
                'content_TitleMeta' => $this->input->post('content_TitleMeta'),
                'content_KeywordMeta' => $this->input->post('content_KeywordMeta'),
                'content_DescriptionMeta' => $this->input->post('content_DescriptionMeta'));
                $this->db->where('id!=',$id);
                $this->db->update('cms_services', $data); 
                redirect('welcome/cms_services');
              }
        /**************************** CMS_services End ***************************/

        /**************************** CMS_contactus start ***************************/

            public function cms_contactus()
              {
              $this->data['aboutusview']= $this->welcome->view_contactus();      
              $this->load->view('cms_contactus', $this->data);         
              }

            public function insert_cms_contactus()
              {
              $data = array(
                'content_description'     => $this->input->post('content_description'),
                'content_TitleMeta'       => $this->input->post('content_TitleMeta'),
                'content_KeywordMeta'     => $this->input->post('content_KeywordMeta'),
                'content_DescriptionMeta' => $this->input->post('content_DescriptionMeta'));

              $insertandupdate = $this->welcome->insert_contactus($data);
              redirect('welcome/cms_contactus');
              }

          public function update_cms_contactus($id)
              {
                  $data = array('content_description' => $this->input->post('content_description'),
                'content_TitleMeta' => $this->input->post('content_TitleMeta'),
                'content_KeywordMeta' => $this->input->post('content_KeywordMeta'),
                'content_DescriptionMeta' => $this->input->post('content_DescriptionMeta'));
                $this->db->where('id!=',$id);
                $this->db->update('cms_contactus', $data); 
                redirect('welcome/cms_contactus');
              }
      /**************************** CMS_contactus End ***************************/ 

       /**************************** CMS_privacypolicy start ***************************/

            public function cms_privacypolicy()
              {
              $this->data['aboutusview']= $this->welcome->view_privacypolicy();      
              $this->load->view('cms_privacypolicy', $this->data);         
              }

            public function insert_cms_privacypolicy()
              {
              $data = array(
                'content_description'     => $this->input->post('content_description'),
                'content_TitleMeta'       => $this->input->post('content_TitleMeta'),
                'content_KeywordMeta'     => $this->input->post('content_KeywordMeta'),
                'content_DescriptionMeta' => $this->input->post('content_DescriptionMeta'));

              $insertandupdate = $this->welcome->insert_privacypolicy($data);
              redirect('welcome/cms_privacypolicy');
              }

             public function update_cms_privacypolicy($id)
              {
                $data = array('content_description' => $this->input->post('content_description'),
                'content_TitleMeta' => $this->input->post('content_TitleMeta'),
                'content_KeywordMeta' => $this->input->post('content_KeywordMeta'),
                'content_DescriptionMeta' => $this->input->post('content_DescriptionMeta'));
                $this->db->where('id!=',$id);
                $this->db->update('cms_privacypolicy', $data); 
                redirect('welcome/cms_privacypolicy');
              }
      /**************************** CMS_privacypolicy End ***************************/


      /**************************** CMS_tnc start ***************************/

            public function cms_tnc()
              {
              $this->data['aboutusview']= $this->welcome->view_tnc();      
              $this->load->view('cms_tnc', $this->data);         
              }

            public function insert_cms_tnc()
              {
              $data = array(
              'content_description'     => $this->input->post('content_description'),
              'content_TitleMeta'       => $this->input->post('content_TitleMeta'),
              'content_KeywordMeta'     => $this->input->post('content_KeywordMeta'),
              'content_DescriptionMeta' => $this->input->post('content_DescriptionMeta'));

              $insertandupdate = $this->welcome->insert_tnc($data);
              redirect('welcome/cms_tnc');
              }

            public function update_cms_tnc($id)
              {
                $data = array('content_description' => $this->input->post('content_description'),
                'content_TitleMeta' => $this->input->post('content_TitleMeta'),
                'content_KeywordMeta' => $this->input->post('content_KeywordMeta'),
                'content_DescriptionMeta' => $this->input->post('content_DescriptionMeta'));
                $this->db->where('id!=',$id);
                $this->db->update('cms_tnc', $data); 
                redirect('welcome/cms_tnc');
              }

      /**************************** CMS_tnc End ***************************/ 

      /********************** General Settings start ***************************/

            public function add_general_settings()
              {
              $this->data['setting']= $this->welcome->view_settings();      
              $this->load->view('add_general_settings', $this->data);         
              }

            public function insert_general_settings()
              {
                $config['upload_path']          = './uploads/';
                $config['allowed_types']        = 'gif|jpg|png';
                $config['max_size']             = '4096';

                 $this->load->library('upload', $config);
                 $this->upload->do_upload('favicon');
                 $this->upload->do_upload('logo');
                 $data_upload_files = $this->upload->data();         
          
              $data = array(
              'site_name'          => $this->input->post('sitename'),
              'site_contact'       => $this->input->post('sitecontact'),
              'toll_free_no'       => $this->input->post('tollfree'),
              'currency'           => $this->input->post('currency'),
              'favicon'            => $data_upload_files['favicon'],
              'logo'               => $data_upload_files['logo'],
              'city'               => $this->input->post('city'),
              'zipcode'            => $this->input->post('zipcode'),
              'address'            => $this->input->post('address'),              
              'email'              => $this->input->post('email'),
              'phone'              => $this->input->post('phone'),
              'mobile'             => $this->input->post('mobile'),
              'lat'                => $this->input->post('latitude'),
              'longitude'          => $this->input->post('longitude'),
              'copyrights'         => $this->input->post('copyrights'),
              'metatitle'          => $this->input->post('meta_title'),
              'metakeyword'        => $this->input->post('meta_keyword'),
              'metadesc'           => $this->input->post('meta_description'),
              'admin_name'         => $this->input->post('admin_name'),
              'admin_email'        => $this->input->post('admin_email'),
              'admin_phone'        => $this->input->post('admin_phone'),
              'admin_mobile'       => $this->input->post('admin_mobile'));

              $insertandupdate = $this->welcome->insert_settings($data);
              redirect('welcome/add_general_settings');
              }

            public function update_general_settings()
              {         
              /*  $config['upload_path']          = './uploads/';
                $config['allowed_types']        = 'gif|jpg|png';
                $config['max_size']             = '4096';

              $data_upload_files = $this->upload->data();   */     
              
              $data = array(
              'site_name'          => $this->input->post('sitename'),
              'site_contact'       => $this->input->post('sitecontact'),
              'toll_free_no'       => $this->input->post('tollfree'),
              'currency'           => $this->input->post('currency'),
                           
              'city'               => $this->input->post('city'),
              'zipcode'            => $this->input->post('zipcode'),
              'address'            => $this->input->post('address'),              
              'email'              => $this->input->post('email'),
              'phone'              => $this->input->post('phone'),
              'mobile'             => $this->input->post('mobile'),
              'lat'                => $this->input->post('latitude'),
              'longitude'          => $this->input->post('longitude'),
              'copyrights'         => $this->input->post('copyrights'),
              'metatitle'          => $this->input->post('meta_title'),
              'metakeyword'        => $this->input->post('meta_keyword'),
              'metadesc'           => $this->input->post('meta_description'),
              'admin_name'         => $this->input->post('admin_name'),
              'admin_email'        => $this->input->post('admin_email'),
              'admin_phone'        => $this->input->post('admin_phone'),
              'admin_mobile'       => $this->input->post('admin_mobile'));
                $this->db->where('id!=','');
                $this->db->update('site_settings', $data); 
                redirect('welcome/add_general_settings');
              }

      /**************************** General Settings End ***************************/
       /**************************** Email Settings Start ***************************/

              public function email_settings()
              {
              $this->data['email_setting'] = $this->welcome->view_emailsettings();      
              $this->load->view('addonly_emailsettings', $this->data);         
              }

              public function insert_emailsettings()
              {
              $data = array(
              'info'     => $this->input->post('info'),
              'support'  => $this->input->post('support'),
              'query'    => $this->input->post('query'),
              'contact'  => $this->input->post('contact'));

              $insertandupdate = $this->welcome->insert_emailsettings($data);
              redirect('welcome/email_settings');
              }

            public function update_emailsettings($id)
              {
                $data = array(
              'info'     => $this->input->post('info'),
              'support'  => $this->input->post('support'),
              'query'    => $this->input->post('query'),
              'contact'  => $this->input->post('contact'));
                $this->db->where('id!=',$id);
                $this->db->update('email_settings', $data); 
                redirect('welcome/email_settings');
              }

 /**************************** Email Settings End ***************************/

 /************************** Website Social media Start *******************/

              public function website_social_media()
              {
               $this->data['website_social_media'] = $this->welcome->view_website_social_media();
               $this->load->view('website_social_media', $this->data);         
              }

              public function insert_website_social_media()
              {
              $data = array(
              'facebook'     => $this->input->post('facebook'),
              'twitter'  => $this->input->post('twitter'),
              'googleplus'    => $this->input->post('googleplus'),
              'pinterest'  => $this->input->post('pinterest'));

              $insertandupdate = $this->welcome->insert_website_social_media($data);
              redirect('welcome/website_social_media');
              }

              public function update_website_social_media($id)
              {
                $data = array(
              'facebook'     => $this->input->post('facebook'),
              'twitter'      => $this->input->post('twitter'),
              'googleplus'   => $this->input->post('googleplus'),
              'pinterest'    => $this->input->post('pinterest'));
                $this->db->where('id!=',$id);
                $this->db->update('site_socialmedia', $data); 
                redirect('welcome/website_social_media');
              }

      /****************************  Website Social media End ***************************/

      /************************** Website Flash Banner Start *******************/

              public function website_flash_banner()
              {
               $this->data['website_flash_banner'] = $this->welcome->view_website_flash_banner();
               $this->load->view('website_flash_banner', $this->data);         
              }

              public function insert_website_flash_banner()
              {

                $config['upload_path']          = './banners/';
                $config['allowed_types']        = 'gif|jpg|png';
                $config['max_size']             = '2048';

                 $this->load->library('upload', $config);
                 $this->upload->do_upload('banner_image');
                 $data_upload_files = $this->upload->data();

              $data = array(
              'banner_type'  => $this->input->post('status_banner'),
              'image'        => $data_upload_files['file_name'],
              'video'        => $this->input->post('banner_video'));

              $insertandupdate = $this->welcome->insert_website_flash_banner($data);
              redirect('welcome/website_flash_banner');
              }

              public function update_website_flash_banner($id)
              {
                $config['upload_path']     = './banners/';
                $config['allowed_types']   = 'gif|jpg|png';
                $config['max_size']        = '2048';

                 $this->load->library('upload', $config);
                 $this->upload->do_upload('banner_image');
                 $data_upload_files = $this->upload->data();

                $data = array(
                'banner_type' => $this->input->post('status_banner'),
                'image'       => $data_upload_files['file_name'],
                'video'       => $this->input->post('banner_video'));
                $this->db->where('id!=',$id);
                $this->db->update('site_flash_banner', $data); 
                redirect('welcome/website_flash_banner');
              }

      /************************** Website Flash Banner End *******************/

      /************************** Website Payment Module End *******************/

         public function website_payment_setting()
            {
             $this->data['view_payment'] = $this->welcome->view_site_payments();
             $this->load->view('add_website_payment', $this->data);         
            }

            public function insert_website_payment_setting()
              {
               $data = array(
              'paypal_show'         => $this->input->post('paypal_show'),
              'paypal_url'          => $this->input->post('paypal_url'),             
              'paypal_bussiness'    => $this->input->post('paypal_bussiness'),
              'stripe_show'         => $this->input->post('stripe_show'),
              'stripe_publish_key'  => $this->input->post('stripe_publish_key'),
              'stripe_api_key'      => $this->input->post('stripe_api_key'));

              $insertandupdate = $this->welcome->insert_site_payments($data);
              redirect('welcome/website_payment_setting');
              }

              public function update_website_payment_setting()
              {
                $data = array(
              'paypal_show'         => $this->input->post('paypal_show'),
              'paypal_url'           => $this->input->post('paypal_url'),             
              'paypal_bussiness'     => $this->input->post('paypal_bussiness'),
              'stripe_show'          => $this->input->post('stripe_show'),
              'stripe_publish_key'   => $this->input->post('stripe_publish_key'),
              'stripe_api_key'       => $this->input->post('stripe_api_key'));

                $this->db->where('id!=','');
                $this->db->update('site_payments', $data); 
                redirect('welcome/website_payment_setting');
              }

      /************************** Website Payment Module End *******************/

      /************************** Website Comments Reviews module Start *******************/

              public function website_comments_reviews()
                {
                 $this->data['view_comments'] = $this->welcome->view_website_comments_reviews();
                 $this->load->view('edit_comments', $this->data);         
                }

                public function delete_comments($id)
                {
                $this->db->where('id', $id);
                $this->db->delete('site_ratings');            
                redirect('welcome/website_comments_reviews');
                }

         /**************** Website Comments Reviews module  End *******************/

        /************************** Website user module Start *******************/
              public function view_website_user_management()
                {
                 $this->data['user_details'] = $this->welcome->view_users();
                 $this->load->view('add_user', $this->data);
                }

             public function insert_users()
              {
               $this->load->library('encrypt');                
               $data = array(
              'name'        => $this->input->post('name'),
              'address_one' => $this->input->post('address_one'),             
              'address_two' => $this->input->post('address_two'),
              'city_name'   => $this->input->post('city_name'),             
              'zipcode'     => $this->input->post('zipcode'),
              'phone'       => $this->input->post('phone'),
              'email'       => $this->input->post('email'),
              'password'    => $this->encrypt->encode($this->input->post('password')),
              'status'      => $this->input->post('status'),
              'created_date'=> date("y/m/d h:i:s"));

              $insert = $this->welcome->insert_users($data);
              redirect('welcome/view_website_user_management');
              }

                public function update_users($id)
              {
                $this->load->library('encrypt');
                $data = array(
              'name'        => $this->input->post('name'),
              'address_one' => $this->input->post('address_one'),             
              'address_two' => $this->input->post('address_two'),
              'city_name'   => $this->input->post('city_name'),             
              'zipcode'     => $this->input->post('zipcode'),
              'phone'       => $this->input->post('phone'),
              'email'       => $this->input->post('email'),
              'password'    => $this->encrypt->encode($this->input->post('password')),
              'status'      => $this->input->post('status'),
              'created_date'=> date("y/m/d h:i:s"));
                $this->db->where('id=',$id);
                $this->db->update('users', $data); 
                redirect('welcome/view_website_user_management');
              }

              public function delete_users($id)
              {
              $this->db->where('id', $id);
              $this->db->delete('users');            
              redirect('welcome/view_website_user_management');
              }

      /************************** Website user module end   *******************/


       /************************** Website user module Start *******************/
              public function view_voucher()
                {
                 $this->data['voucher_details'] = $this->welcome->view_voucher();
                 $this->data['view_providerdetails'] = $this->welcome->view_providerdetails();
                 $this->load->view('add_voucher', $this->data);
                }

             public function insert_voucher()
              {
               $data = array(
              'voucher_title'       => $this->input->post('voucher_name'),
              'provider_id'         => $this->input->post('providerid'),
              'voucher_type'        => $this->input->post('voucher_apply'),             
              'voucher_code'        => $this->input->post('voucher_code'),
              'discount_price'      => $this->input->post('discount_price'),             
              'usage_limit'         => $this->input->post('usage_limit'),
              'description'         => $this->input->post('description'),                    
              'status'              => $this->input->post('status'),
              'created_date'        => date("y/m/d h:i:s"));
              $insert = $this->welcome->insert_voucher($data);
              redirect('welcome/view_voucher');
              }

                public function update_voucher($id)
              {
                $data = array(
              'voucher_title'       => $this->input->post('voucher_name'),
              'provider_id'         => $this->input->post('providerid'),
              'voucher_type'        => $this->input->post('voucher_apply'),             
              'voucher_code'        => $this->input->post('voucher_code'),
              'discount_price'      => $this->input->post('discount_price'),             
              'usage_limit'         => $this->input->post('usage_limit'),
              'description'         => $this->input->post('description'),                    
              'status'              => $this->input->post('status'),
              'created_date'        => date("y/m/d h:i:s"));
                $this->db->where('id=',$id);
                $this->db->update('site_voucher', $data); 
                redirect('welcome/view_voucher');
              }

              public function delete_voucher($id)
              {
              $this->db->where('id', $id);
              $this->db->delete('site_voucher');
              redirect('welcome/view_voucher');
              }

              public function view_sms()
                {                 
                 $this->load->view('website_sms_api');
                }

            public function insert_sms()
              {
               $data = array(
              'sms_status'       => $this->input->post('status_sms'),
              'sms_username'       => $this->input->post('sms_username'),
              'sms_password'       => $this->input->post('sms_password'),
              'sms_sender_code'       => $this->input->post('sms_code'));
              $insert = $this->welcome->insert_sms($data);
              redirect('welcome/view_sms');
              }

      /************************** Website user module end   *******************/

      /************************** Scrap coding below this comment *******************/

        /*public function ajax_city()
            {
            $this->db->where('id', $id);
            $this->db->delete('tbl_catthree');
            $this->session->set_flashdata('message', 'Your data deleted Successfully..');
            redirect('welcome/index');
            }*/

        /****************************  Default Module Starts ****************************/
                /*public function view_category()
                {
                  // $this->data['view_category']= $this->welcome->view_category();
                  $this->load->view('add');
                }

                public function submit_data()
                {
                $data = array('country'        => $this->input->post('country'),
                              'status'         => '1',
                              'created_date'   => date("m/d/y h:i:s"));
                $insert = $this->welcome->insert_data($data);
                $this->session->set_flashdata('message', 'Your data inserted Successfully..');
                redirect('welcome/index');
                }
            
                public function edit_data($id)
                {
                  $this->data['edit_data'] = $this->welcome->edit_data($id);
                  $this->load->view('edit', $this->data, FALSE);
                }

                public function update_data($id)
                    {
                      $data = array('username' => $this->input->post('username'),
                                  'email'        => $this->input->post('email'),
                                  'sex'          => $this->input->post('sex'),
                                  'address'      => $this->input->post('address'),
                                  'created_date' => date("m/d/y h:i:s"),
                                  'status'       => 'Y');
                    $this->db->where('id', $id);
                    $this->db->update('user_data', $data);
                    $this->session->set_flashdata('message', 'Your data updated Successfully..');
                    redirect('welcome/index');
                    }

                  public function delete_data($id)
                  {
                  $this->db->where('id', $id);
                  $this->db->delete('user_data');
                  $this->session->set_flashdata('message', 'Your data deleted Successfully..');
                  redirect('welcome/index');
                  }*/
                  
      /****************************  END Default Module ***************************/
}

?>