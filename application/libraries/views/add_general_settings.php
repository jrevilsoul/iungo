<?php include('headerandsidebar.php'); ?>
<div class="row">
  <div class="container" id="content">
    <style type="text/css">
    .modal .modal-body {
    max-height: 450px;
    overflow-y: auto;
    }
    </style>
   
    <form method="post" action="<?php if(!empty($setting)){ echo site_url('Welcome/update_general_settings'); } else{ echo site_url('Welcome/insert_general_settings'); } ?>" name="data_register">
      <div class="panel panel-default">
      <div class="panel-heading">Site Configuration</div>
        <ul class="nav nav-tabs nav-justified" role="tablist">
          <li class="active"><a href="#GENERAL" data-toggle="tab">Website General Settings</a></li>
          <li class=""><a href="#HOTELS_TYPES" data-toggle="tab">Admin Details</a></li>
       
        </ul>
        <div class="panel-body">
		 <?php if($this->session->flashdata('insertmessage')){?>
		  <div class="alert alert-success" align="center">
		  <?php echo $this->session->flashdata('insertmessage'); ?>
			</div>
		  <?php
		  }elseif($this->session->flashdata('updatemessage')){
		  ?>
		  <div class="alert alert-success" align="center">
		  <?php echo $this->session->flashdata('updatemessage'); ?>
			</div>
		  <?php
		  }
		  ?>
	    <br>
          <div class="tab-content form-horizontal">
            <div class="tab-pane wow fadeIn animated active in" id="GENERAL">
              <div class="clearfix"></div>
              
              <hr>
              <div class="row form-group">
                <label  class="col-md-2 control-label text-left">Site Name:</label>
                <div class="col-md-4">
                <input class="form-control" type="text" name="sitename" value="<?php foreach($setting as $data){ echo $data['site_name'];} ?>">
                </div>             
              </div>
              <hr>
              <div class="row form-group">
                <label class="col-md-2 control-label text-left">Site Contact:</label>
                <div class="col-md-4">
                  <input class="form-control" type="text" name="sitecontact" value="<?php foreach($setting as $data){ echo $data['site_contact']; } ?>" required>
                </div>                
              </div>
			  
              <div class="row form-group">
                <label  class="col-md-2 control-label text-left">Toll Free No:</label>
                <div class="col-md-4">
                  <input class="form-control" type="text" placeholder="" name="tollfree"  value="<?php foreach($setting as $data){ echo $data['toll_free_no']; } ?>" required>
                </div>                
              </div>

              <div class="row form-group">
                <label  class="col-md-2 control-label text-left">Website Favicon:</label>
                <div class="col-md-4">
                  <input type="file" name="favicon" placeholder="">
                <img src="<?php //echo base_url('uploads/') . $data['favicon']; ?>" width="170px" height="130px">
                </div>                
              </div>

              <div class="row form-group">
                <label  class="col-md-2 control-label text-left">Website Logo:</label>
                <div class="col-md-4">
                  <input type="file" name="logo" placeholder="">
                  <img src="<?php // echo base_url('uploads/') . $data['logo']; ?>"
            width="170px" height="130px">
                </div>                
              </div>

              <div class="clearfix"></div>
              <div class="row form-group">
              <label  class="col-md-2 control-label text-left">City:</label>
              <?php $sql = "SELECT * FROM `tbl_city`";
                $query = $this->db->query($sql); ?>

                <div class="col-md-4">
                  <select class="form-control" name="city" required>
                    <option value="-1" label="" >--SELECT CITY--</option>
               <?php foreach ($query->result() as $row) { ?>
                  <option value="<?php echo $row->cityName;?>" <?php foreach($setting as $data){ if($row->cityid == $data['city']){echo 'selected="selected"';} }?>> <?php echo $row->cityName; ?></option>
                <?php } ?>
                  </select>
                </div>
                <label class="col-md-2 control-label text-left">Zip:</label>
                <div class="col-md-3">
                  <input class="form-control" type="text" placeholder="" name="zipcode"  value="<?php foreach($setting as $data){ echo $data['zipcode']; } ?>">
                </div>                
              </div>

              <div class="clearfix"></div>
              <div class="row form-group">
                <label  class="col-md-2 control-label text-left">Address:</label>
                <div class="col-md-4">
                  <input class="form-control" type="text" placeholder="" name="address"  value="<?php foreach($setting as $data){ echo $data['address']; } ?>">
                </div>                
              </div>

              <div class="clearfix"></div>
            <div class="row form-group">
                <label class="col-md-2 control-label text-left">Currency:</label>
                <div class="col-md-4">
                 <input class="form-control" type="text" name="currency" value="<?php foreach($setting as $data){ echo $data['currency']; } ?>" required>
                </div>
            <label class="col-md-2 control-label text-left">Email:</label>
            <div class="col-md-3"><input class="form-control" type="text" placeholder="" name="email"  value="<?php foreach($setting as $data){ echo $data['email']; } ?>">
            </div>
			     </div>              
              <div class="row form-group">
                <label  class="col-md-2 control-label text-left">Phone:</label>
                <div class="col-md-4">
                  <input class="form-control" type="text" placeholder="" name="phone"  value="<?php foreach($setting as $data){ echo $data['phone']; } ?>" required>
                </div>
                <label  class="col-md-2 control-label text-left">Mobile:</label>
                <div class="col-md-3">
                  <input class="form-control" type="text" placeholder="" name="mobile"  value="<?php foreach($setting as $data){ echo $data['mobile']; } ?>">
                </div>
              </div>

              <div class="row form-group">
                <label  class="col-md-2 control-label text-left">Latitude:</label>
                <div class="col-md-4">
                  <input class="form-control" type="text" placeholder="" name="latitude"  value="<?php foreach($setting as $data){ echo $data['lat']; } ?>" required>
                </div>
                <label  class="col-md-2 control-label text-left">Longitude:</label>
                <div class="col-md-3">
                  <input class="form-control" type="text" placeholder="" name="longitude"  value="<?php foreach($setting as $data){echo $data['longitude'];} ?>">
                </div>
              </div>
              <hr>
              <div class="row" >
				<label  class="col-md-2 control-label text-left">Copyrights:</label>
              <div class="col-md-9">
              <textarea name="copyrights" id="one" rows="8" cols="20"><?php foreach($setting as $data){ echo $data['copyrights']; } ?>               
              </textarea>
                <script type="text/javascript">//<![CDATA[
                window.CKEDITOR_BASEPATH='l/application/libraries/ckeditor.php';
                //]]></script>
                <script type="text/javascript" src="l/application/libraries/ckeditor.phpckeditor.js?t=B5GJ5GG"></script>
                <script type="text/javascript">//<![CDATA[
                CKEDITOR.replace('one', {"toolbar":[["Source","-","Bold","Italic","Underline","Strike","Format","Styles"],["NumberedList","BulletedList","Outdent","Indent","Blockquote"],["Image","Link","Unlink","Anchor","Table","HorizontalRule","SpecialChar","Maximize"],["Cut","Copy","Paste","PasteText","PasteFromWord","-","Undo","Redo","Find","Replace","-","SelectAll","-","SpellChecker","Scayt"]],"language":"en"});
                //]]>
                </script>
              </div>
              </div>
              <hr>
			  <div class="row" >
              <label  class="col-md-2 control-label text-left">Meta Title</label>
              <div class="col-md-9">
              <textarea name="meta_title" id="two" rows="8" cols="20">
              <?php foreach($setting as $data){ echo $data['metatitle']; } ?>               
              </textarea>
               <script type="text/javascript">//<![CDATA[
                window.CKEDITOR_BASEPATH='l/application/libraries/ckeditor.php';
                //]]>
                </script>
                <script type="text/javascript"
				 src="l/application/libraries/ckeditor.phpckeditor.js?t=B5GJ5GG"></script>
                <script type="text/javascript">//<![CDATA[
                CKEDITOR.replace('two', {"toolbar":[["Source","-","Bold","Italic","Underline","Strike","Format","Styles"],["NumberedList","BulletedList","Outdent","Indent","Blockquote"],["Image","Link","Unlink","Anchor","Table","HorizontalRule","SpecialChar","Maximize"],["Cut","Copy","Paste","PasteText","PasteFromWord","-","Undo","Redo","Find","Replace","-","SelectAll","-","SpellChecker","Scayt"]],"language":"en"});
                //]]>
                </script>
              </div>
              </div>
              <hr>
              <div class="row" >
              <label  class="col-md-2 control-label text-left">Meta Keyword</label>
              <div class="col-md-9">
              <textarea name="meta_keyword" id="keywords" rows="8" cols="20">
              <?php foreach($setting as $data){ echo $data['metakeyword']; } ?>    
              </textarea>
                <script type="text/javascript">//<![CDATA[
                window.CKEDITOR_BASEPATH='l/application/libraries/ckeditor.php';
                //]]></script>
                <script type="text/javascript" src="l/application/libraries/ckeditor.phpckeditor.js?t=B5GJ5GG"></script>
                <script type="text/javascript">//<![CDATA[
                CKEDITOR.replace('keywords', {"toolbar":[["Source","-","Bold","Italic","Underline","Strike","Format","Styles"],["NumberedList","BulletedList","Outdent","Indent","Blockquote"],["Image","Link","Unlink","Anchor","Table","HorizontalRule","SpecialChar","Maximize"],["Cut","Copy","Paste","PasteText","PasteFromWord","-","Undo","Redo","Find","Replace","-","SelectAll","-","SpellChecker","Scayt"]],"language":"en"});
                //]]>
                </script>
              </div>
              </div>

              <div class="row" >
              <label  class="col-md-2 control-label text-left">Meta Description</label>
              <div class="col-md-9">
              <textarea name="meta_description" id="description" rows="8" cols="20">  <?php foreach($setting as $data){ echo $data['metadesc']; } ?>              
              </textarea>
                <script type="text/javascript">//<![CDATA[
                window.CKEDITOR_BASEPATH='l/application/libraries/ckeditor.php';
                //]]></script>
                <script type="text/javascript" src="l/application/libraries/ckeditor.phpckeditor.js?t=B5GJ5GG"></script>
                <script type="text/javascript">//<![CDATA[
                CKEDITOR.replace('description', {"toolbar":[["Source","-","Bold","Italic","Underline","Strike","Format","Styles"],["NumberedList","BulletedList","Outdent","Indent","Blockquote"],["Image","Link","Unlink","Anchor","Table","HorizontalRule","SpecialChar","Maximize"],["Cut","Copy","Paste","PasteText","PasteFromWord","-","Undo","Redo","Find","Replace","-","SelectAll","-","SpellChecker","Scayt"]],"language":"en"});
                //]]>
                </script>
              </div>
              </div>  
              <!-- <h4 class="text-danger">SEO</h4>
              <div class="row form-group">
                <label  class="col-md-2 control-label text-left">Meta Keywords</label>
                <div class="col-md-4">
                  <input class="form-control" type="text" placeholder="" name="keywords" value="">
                </div>
                <label  class="col-md-2 control-label text-left">Meta Description</label>
                <div class="col-md-4">
                  <input class="form-control" type="text" placeholder="" name="description"  value="">
                </div>
              </div> -->
            </div>           
            <div class="tab-pane wow fadeIn animated in" id="HOTELS_TYPES">
             <div class="xcrud">
                <div class="xcrud-container">
                  <div class="xcrud-ajax">
                    <input type="hidden" class="xcrud-data" name="key" value="" />
                    <input type="hidden" class="xcrud-data" name="orderby" value="" />
                    <input type="hidden" class="xcrud-data" name="order" value="desc" />
                    <input type="hidden" class="xcrud-data" name="start" value="0" />
                    <input type="hidden" class="xcrud-data" name="limit" value="10" />
                    <input type="hidden" class="xcrud-data" name="instance" value="" />
                    <input type="hidden" class="xcrud-data" name="task" value="list" />
                  
                    <div class="xcrud-list-container">
                      <table class="xcrud-list table table-striped table-hover">
                        <thead>
                          
                        </thead>
                        <tbody>
                          <tr class="xcrud-row xcrud-row-0">
                            <!-- <td><input class=checkboxcls type='checkbox' value=191></td> -->
                            <!-- <td class="xcrud-current xcrud-num">1</td> -->
            <td>
                <b> Admin Name: </b><br /><br /><br /><br />
								<b>Admin Email: </b><br /><br /><br /><br />
								<b>Admin Phone: </b><br /><br /><br /><br />
								<b>Admin Mobile:</b><br /><br /><br /><br />
                            </td>
                            <td>
                             <?php
               /* if(isset($_POST['provider'])){
                $a = $_POST['provider'];
                $sql   = "SELECT * FROM `tbl_socialmedia` WHERE provider_id = '$a'";
                $query = $this->db->query($sql);
                foreach ($query->result() as $row) {*/ ?>
                    <input type="hidden" name="providerid" value="<?php // echo $a; ?>">
                            <input type="text" class="form-control" name="admin_name" value="<?php foreach($setting as $data){ echo $data['admin_name'];} ?>"><br /><br />
                            <input type="text" class="form-control" name="admin_email" value="<?php foreach($setting as $data){ echo $data['admin_email']; } ?>"><br /><br />
                               <input type="text" class="form-control" name="admin_phone" value="<?php foreach($setting as $data){ echo $data['admin_phone']; } ?>"><br /><br />
                               <input type="text" class="form-control" name="admin_mobile" value="<?php foreach($setting as $data){ echo $data['admin_mobile']; } ?>"><br /><br />
							              </td>
                           <!--  <td class="xcrud-current xcrud-actions xcrud-fix"><span class="btn-group"><a class="btn btn-default btn-xcrud btn btn-warning" href="#sett191" title="Edit" data-toggle="modal"><i class="fa fa-edit"></i></a><a class="btn btn-default btn-xcrud btn-danger" href="<?php //echo site_url('welcome/delete_socialmedia/'.$row->id.''); ?>" title="DELETE" target="_self" id="191"><i class="fa fa-times"></i></a></span></td> -->
                        <?php // } } ?></tr>   
<?php // } ?>						
                      </tbody>                      
                      <tfoot>
                      </tfoot>
                    </table>
                  </div>
                </div>
                <div class="xcrud-overlay"></div>
              </div>
            </div>
          </div>
          
        </div>
      </div>
      <div class="panel-footer">
        <input type="hidden" name="updatesettings" value="1" />
        <input type="hidden" name="updatefor" value="hotels" />
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
    </div>
  </form>
    </div>
  </div>
</div>
<!----edit modal--->
<script>
$(document).ready(function(){
if(window.location.hash != "") {
$('a[href="' + window.location.hash + '"]').click() } });
</script>
</div>
</div>
</div>
<?php include('footer.php'); ?>