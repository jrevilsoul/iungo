<!DOCTYPE html><head>

<?php include('headerandsidebar.php'); ?>



<div class="row">

  <div class="container" id="content">

    <div class="panel panel-default">

      <div class="panel-heading">FAQs Content</div>

      <div class="panel-body">
<?php if($this->session->flashdata('insertmessage')){?>
				  <div class="alert alert-success" align="center">
				  <?php echo $this->session->flashdata('insertmessage'); ?>
					</div>
				  <?php
				  }elseif($this->session->flashdata('updatemessage')){
				  ?>
				  <div class="alert alert-success" align="center">
				  <?php echo $this->session->flashdata('updatemessage'); ?>
					</div>
				  <?php
				  }
				  ?>
        <div class="xcrud">

          <div class="xcrud-container">

            <div class="xcrud-ajax">

              <div class="xcrud-list-container">

              <div id="container" class="container">
           <?php echo $this->session->flashdata('msg'); ?>
          
              <div class="col-md-11">

              <form method="post" action="<?php  echo site_url('Welcome/insert_cms_faq'); ?>" name="data_register">

               <br />

               <div class="row form-group">

                <label class="col-md-2 control-label text-left">Questions</label>

                <div class="col-md-10">

                <textarea name="faq_question" id="one" rows="8" cols="124"> </textarea>

                </div>

                </div>          

                <div class="row form-group">

                <label class="col-md-2 control-label text-left">Answer</label>

                <div class="col-md-10">

                <textarea name="content_description" id="two" rows="8" cols="60"></textarea>

                <script type="text/javascript">//<![CDATA[

                window.CKEDITOR_BASEPATH='l/application/libraries/ckeditor.php';

                //]]></script>

                <script type="text/javascript" src="l/application/libraries/ckeditor.phpckeditor.js?t=B5GJ5GG"></script>

                <script type="text/javascript">//<![CDATA[

                CKEDITOR.replace('two', {"toolbar":[["Source","-","Bold","Italic","Underline","Strike","Format","Styles"],["NumberedList","BulletedList","Outdent","Indent","Blockquote"],["Image","Link","Unlink","Anchor","Table","HorizontalRule","SpecialChar","Maximize"],["Cut","Copy","Paste","PasteText","PasteFromWord","-","Undo","Redo","Find","Replace","-","SelectAll","-","SpellChecker","Scayt"]],"language":"en"});

                //]]>

                </script>

                </div>

                </div>



                <button type="submit" class="btn btn-primary pull-right">Submit</button>

                </form>

                </div>

              </div>

            </div>

           </div>

    <?php include('footer.php'); ?>

</body>

</html>