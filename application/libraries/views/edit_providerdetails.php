<?php include('headerandsidebar.php'); ?>
      <div class="row">
      <div class="container" id="content">
          <style type="text/css">
    .modal .modal-body {
    max-height: 450px;
    overflow-y: auto;
    } 
</style>
          <?php foreach ($edit_data as $data) { 
          }
		  ?>
          <form action="<?php echo site_url('welcome/update_providerdetails/'.$data['id'].''); ?>" method="POST" enctype="multipart/form-data">
          <div class="panel panel-default">
              <div class="panel-heading">Edit Service Provider Details</div>
              <ul class="nav nav-tabs nav-justified" role="tablist">
              <li class="active"><a href="#GENERAL" data-toggle="tab">General</a></li>
              <li class=""><a href="#HOTELS_TYPES" data-toggle="tab">Social Media Links</a></li>
            </ul>
              <div class="panel-body">
              <div class="tab-content form-horizontal">
                  <div class="tab-pane wow fadeIn animated active in" id="GENERAL">
                  <div class="clearfix"></div>
                  <div class="col-md-12 col-sm-12 col-xs-12 form_admin_page">
                      <div class="row form-group">
                      <label  class="col-md-2 control-label text-left">Select Provider Type:</label>
                      <div class="col-md-4">
                          <div></div>
                          <select  name="providertype" class="form-control" name="target" required>
                          <option value="">--SELECT PROVIDER TYPE--</option>
                          <?php foreach ($view_provider as $nameprovider) { ?>
                          <option value="<?php echo $nameprovider['id']; ?>" <?php if($nameprovider['id'] == $data['provider_type']) { echo 'selected="selected"'; } ?>><?php echo $nameprovider['en_serviceTypeName']; ?></option>
                          <?php } ?>
                          </select>
                        </div>
                      <label  class="col-md-2 control-label text-left">Select Category:</label>
                      <div class="col-md-3">
                          <select  name="catname" class="form-control"required>
                          <option value="">--SELECT CATGORY--</option>
                          <?php foreach ($view_catone as $nameprovider) { ?>
                          <option value="<?php echo $nameprovider['id']; ?>" <?php if($nameprovider['id'] == $data['category_provider']) { echo 'selected="selected"'; } ?> ><?php echo $nameprovider['catonename']; ?></option>
                          <?php } ?>
                        </select>
                        </div>
                    </div>
                      <div class="row form-group">
                      <label  class="col-md-2 control-label text-left">Status:</label>
                      <div class="col-md-4">
                          <select  name="status" class="form-control" required>
                          <option value="1">Enable</option>
                          <option value="0">Disable</option>
                        </select>
                        </div>
                    </div>
                      <hr>
                      <div class="row form-group">
                      <label  class="col-md-2 control-label text-left">Provider Title (Name):</label>
                      <div class="col-md-4">
                          <input class="form-control" type="text" placeholder="" name="provider_name" 
                  value="<?php echo $data['provider_name']; ?>">
                        </div>
                    </div>
                      <div class="row form-group">
                      <label  class="col-md-2 control-label text-left">Provider Image:</label>
                      <div class="col-md-4">
                          <input class="form-control" type="file" name="provider_image" />
                        </div>
                      <div class="col-md-4"><img src="<?php  echo base_url('uploads/' . $data['provider_image']); ?>" width="300px;" height="200px;"></div>
					   <label  class="col-md-2 control-label text-left">provider video:</label>
                        <div class="col-md-3">
                       <input class="form-control" type="file" placeholder="" name="video_name"  value="">
                </div>
                    </div>
                      <div class="row form-group">
                      <label  class="col-md-2 control-label text-left">Provider Description:</label>
                      <div class="col-md-4">
                          <input class="form-control" type="text" placeholder="" name="desc"  value="<?php echo $data['description']; ?>" required>
                        </div>
                    </div>
                      <div class="clearfix"></div>
                      <div class="row form-group">
                      <label  class="col-md-2 control-label text-left">City:</label>
                      <?php $sql   = "SELECT * FROM `tbl_city`";
                    $query = $this->db->query($sql);   ?>
                      <div class="col-md-4">
                          <select class="form-control" name="provider_city" required>
                          <option value="-1" label="" >--SELECT CITY--</option>
                          <?php foreach ($query->result() as $row) { ?>
                          <option value="<?php echo $row->cityid;  ?>" <?php if($row->cityid == $data['city']) { echo 'selected="selected"'; } ?>"><?php echo $row->cityName; ?> </option>
                            <?php } ?>
                        </select>
                        </div>
                    </div>
                      <div class="row form-group">
                      <label  class="col-md-2 control-label text-left">Zip:</label>
                      <div class="col-md-3">
                          <input class="form-control" type="text" placeholder="" name="zipcode"  value="<?php echo $data['zipcode']; ?>">
                        </div>
                    </div>
                      <div class="clearfix"></div>
                      <div class="row form-group">
                      <label  class="col-md-2 control-label text-left">Address:</label>
                      <div class="col-md-4">
                          <input class="form-control" type="text" placeholder="" name="address"  value="<?php echo $data['address']; ?>">
                        </div>
						<label  class="col-md-2 control-label text-left">outside:</label>
                <div class="col-md-3">
                  <input class="form-control" type="text" placeholder="" name="outside"  value="<?php echo $data['outside']; ?>">
                </div> 
                    </div>
					<div class="clearfix"></div>
              <div class="row form-group">
                <label  class="col-md-2 control-label text-left">person of detail:</label>
                <div class="col-md-4">
                  <input class="form-control" type="text" placeholder="" name="persondetail"  value="<?php echo $data['persondetail']; ?>">
                </div> 
                <label  class="col-md-2 control-label text-left">square of area:</label>
                <div class="col-md-3">
                  <input class="form-control" type="text" placeholder="" name="square"  value="<?php echo $data['square']; ?>">
                </div> 				
              </div>
			    <div class="clearfix"></div>
              <div class="row form-group">
                <label  class="col-md-2 control-label text-left">Location:</label>
                <div class="col-md-4">
                  <input class="form-control" type="text" placeholder="" name="location"  value="<?php echo $data['location']; ?>">
                </div> 
                <label  class="col-md-2 control-label text-left">catering:</label>
                <div class="col-md-3">
                  <input class="form-control" type="text" placeholder="" name="catering"  value="<?php echo $data['catering']; ?>">
                </div> 				
              </div>
			    <div class="clearfix"></div>
              <div class="row form-group">
                <label  class="col-md-2 control-label text-left">Times booking:</label>
                <div class="col-md-4">
                  <input class="form-control" type="text" placeholder="" name="booking"  value="<?php echo $data['booking']; ?>">
                </div> 
                <label  class="col-md-2 control-label text-left">Our opinion:</label>
                <div class="col-md-3">
                  <input class="form-control" type="text" placeholder="" name="opinion"  value="<?php echo $data['opinion']; ?>">
                </div> 				
              </div>
			    <div class="clearfix"></div>
              <div class="row form-group">
                <label  class="col-md-2 control-label text-left">Detail of Events:</label>
                <div class="col-md-4">
                 <textarea rows="3" cols="46" name="detailevent" value="<?php echo $data['detailevent']; ?>"></textarea>
                </div> 			
              </div>
					
                    </div>
                  <div class="clearfix"></div>
                  <div class="row form-group">
                      <label class="col-md-2 control-label text-left">Contact Name:</label>
                      <div class="col-md-4">
                      <input class="form-control" type="text" name="contact_name" value="<?php echo $data['contact_name']; ?>" required>
                    </div>
                      <label class="col-md-2 control-label text-left">Email:<sup>*</sup></label>
                      <div class="col-md-3">
                      <input class="form-control" type="text" placeholder="" name="email"  value="<?php echo $data['email']; ?>">
                    </div>
                    </div>
                  <div class="row form-group">
                      <label  class="col-md-2 control-label text-left">Phone:<sup>*</sup></label>
                      <div class="col-md-4">
                      <input class="form-control" type="text" placeholder="" name="phone"  value="<?php echo $data['phone']; ?>" required>
                    </div>
                      <label  class="col-md-2 control-label text-left">Mobile:</label>
                      <div class="col-md-3">
                      <input class="form-control" type="text" placeholder="" name="mobile"  value="<?php echo $data['mobile']; ?>">
                    </div>
                    </div>
                  <hr>
                  <div class="row" >
                      <label  class="col-md-2 control-label text-left">Policy:</label>
                      <div class="col-md-9">
                      <textarea name="policy" id="one" rows="8" cols="20"> <?php echo $data['policy']; ?>               
              </textarea>
                      <script type="text/javascript">//<![CDATA[
                window.CKEDITOR_BASEPATH='l/application/libraries/ckeditor.php';
                //]]></script>
                      <script type="text/javascript" src="l/application/libraries/ckeditor.phpckeditor.js?t=B5GJ5GG"></script>
                      <script type="text/javascript">//<![CDATA[
                CKEDITOR.replace('one', {"toolbar":[["Source","-","Bold","Italic","Underline","Strike","Format","Styles"],["NumberedList","BulletedList","Outdent","Indent","Blockquote"],["Image","Link","Unlink","Anchor","Table","HorizontalRule","SpecialChar","Maximize"],["Cut","Copy","Paste","PasteText","PasteFromWord","-","Undo","Redo","Find","Replace","-","SelectAll","-","SpellChecker","Scayt"]],"language":"en"});
                //]]>
                </script>
                    </div>
                    </div>
                  <hr>
                  <div class="row" >
                      <label class="col-md-2 control-label text-left">Terms & Conditions:</label>
                      <div class="col-md-9">
                      <textarea name="tnc" id="two" rows="8" cols="20">
			  <?php echo $data['tnc']; ?></textarea>
                      <script type="text/javascript">//<![CDATA[
                window.CKEDITOR_BASEPATH='l/application/libraries/ckeditor.php';
                //]]></script>
                      <script type="text/javascript" 
				src="l/application/libraries/ckeditor.phpckeditor.js?t=B5GJ5GG"></script>
                      <script type="text/javascript">//<![CDATA[
                CKEDITOR.replace('two', {"toolbar":[["Source","-","Bold","Italic","Underline","Strike","Format","Styles"],["NumberedList","BulletedList","Outdent","Indent","Blockquote"],["Image","Link","Unlink","Anchor","Table","HorizontalRule","SpecialChar","Maximize"],["Cut","Copy","Paste","PasteText","PasteFromWord","-","Undo","Redo","Find","Replace","-","SelectAll","-","SpellChecker","Scayt"]],"language":"en"});
                //]]>
                </script>
                    </div>
                    </div>
                  <hr>
                  <!-- <h4 class="text-danger">SEO</h4>
              <div class="row form-group">
                <label  class="col-md-2 control-label text-left">Meta Keywords</label>
                <div class="col-md-4">
                  <input class="form-control" type="text" placeholder="" name="keywords" value="">
                </div>
                <label  class="col-md-2 control-label text-left">Meta Description</label>
                <div class="col-md-4">
                  <input class="form-control" type="text" placeholder="" name="description"  value="">
                </div>
              </div> -->
                </div>
                  <div class="tab-pane wow fadeIn animated in" id="HOTELS_TYPES">
                  <div class="xcrud">
                      <div class="xcrud-container">
                      <div class="xcrud-ajax">
                          <input type="hidden" class="xcrud-data" name="key" value="" />
                          <input type="hidden" class="xcrud-data" name="orderby" value="" />
                          <input type="hidden" class="xcrud-data" name="order" value="desc" />
                          <input type="hidden" class="xcrud-data" name="start" value="0" />
                          <input type="hidden" class="xcrud-data" name="limit" value="10" />
                          <input type="hidden" class="xcrud-data" name="instance" value="" />
                          <input type="hidden" class="xcrud-data" name="task" value="list" />
                          <div class="xcrud-top-actions">
                          <!--   <div class="btn-group pull-right"><a href="" class="btn btn-danger"><i class="glyphicon glyphicon-remove"></i> Delete Selected</a><a href="javascript:;" data-task="print" class="btn btn-default xcrud-in-new-window xcrud-action"><i class="glyphicon glyphicon-print"></i> Print</a><a href="javascript:;" data-task="csv" class="btn btn-default xcrud-in-new-window xcrud-action"><i class="glyphicon glyphicon-file"></i>Export into CSV</a></div> -->
                          <div class="clearfix"></div>
                        </div>
                          <br />
                          <div class="xcrud-list-container">
                          <div class="col-md-12 col-sm-12 col-xs-12 form_admin_page">
                    <div class="row form-group">
          <label class="col-md-2 control-label text-left">Facebook Link</label>
          <div class="col-md-10">
          <input type="hidden" name="providerid" value="<?php // echo $a; ?>">
         <input type="text" class="form-control"  name="fb" placeholder="Please enter Facebook id" value="">
        </div>
        </div>
        <div class="row form-group">
          <label class="col-md-2 control-label text-left">Twitter Link</label>
          <div class="col-md-10">
       <input type="text" class="form-control" placeholder="Please enter Twitter id" name="twitter" value="">
        </div>
        </div>
        <div class="row form-group">
          <label class="col-md-2 control-label text-left">Google Plus Link</label>
          <div class="col-md-10">
        <input type="text" class="form-control"  placeholder="Please enter Googleplus id" name="googleplus" value="">
        </div>
        </div>
        <div class="row form-group">
          <label class="col-md-2 control-label text-left">Linkedin Link</label>
          <div class="col-md-10">
        <input type="text" class="form-control"  placeholder="Please enter Linkedin id" name="linkedin" value="">
        </div>
        </div>
        <div class="row form-group">
          <label class="col-md-2 control-label text-left">Instagram Link:</label>
          <div class="col-md-10">
         <input type="text" class="form-control" placeholder="Please enter Instagram id" name="instagram" value="">
        </div>
        </div>
        <div class="row form-group">
          <label class="col-md-2 control-label text-left">Tags:</label>
          <div class="col-md-10">
        <input type="text" class="form-control" placeholder="Please enter Tags" name="tags" value="">
        </div>
        </div>
                    
                    </div>
                        </div>
                        </div>
                      <div class="xcrud-overlay"></div>
                    </div>
                    </div>
                </div>
                  <div class="tab-pane wow fadeIn animated in" id="ROOMS_TYPES">
                  <div class="add_button_modal" >
                      <button type="button" data-toggle="modal" data-target="#ADD_ROOM_TYPES" class="btn btn-success"><i class="glyphicon glyphicon-plus-sign"></i>Add Room Types</button>
                    </div>
                  <div class="xcrud">
                      <div class="xcrud-container">
                      <div class="xcrud-ajax">
                          <input type="hidden" class="xcrud-data" name="key" value="" />
                          <input type="hidden" class="xcrud-data" name="orderby" value="" />
                          <input type="hidden" class="xcrud-data" name="order" value="desc" />
                          <input type="hidden" class="xcrud-data" name="start" value="0" />
                          <input type="hidden" class="xcrud-data" name="limit" value="10" />
                          <input type="hidden" class="xcrud-data" name="instance" value="" />
                          <input type="hidden" class="xcrud-data" name="task" value="list" />
                          <div class="xcrud-top-actions">
                          <div class="btn-group pull-right"><a href="" class="btn btn-danger"><i class="glyphicon glyphicon-remove"></i>Delete Selected</a><a href="javascript:;" data-task="print" class="btn btn-default xcrud-in-new-window xcrud-action"><i class="glyphicon glyphicon-print"></i>Print</a><a href="javascript:;" data-task="csv" class="btn btn-default xcrud-in-new-window xcrud-action"><i class="glyphicon glyphicon-file"></i>Export into CSV</a></div>
                          <div class="clearfix"></div>
                        </div>
                          <div class="xcrud-list-container">
                          <table class="xcrud-list table table-striped table-hover">
                              <thead>
                              <tr class="xcrud-th">
                                  <th><input class="all" type="checkbox" value="" name="rooms[]" id="select_all" ></th>
                                  <th class="xcrud-num">&#35;</th>
                                  <th data-order="desc" data-orderby="pt_hotels_types_settings.sett_name" class="xcrud-column xcrud-action">Room Types</th>
                                  <th data-order="desc" data-orderby="pt_hotels_types_settings.sett_status" class="xcrud-column xcrud-action">Status</th>
                                  <th class="xcrud-actions">&nbsp;</th>
                                </tr>
                            </thead>
                              <tbody>
                              <?php
                if(isset($_POST['provider'])){
                $a = $_POST['provider'];
                $sql  = "SELECT * FROM `tbl_room` WHERE provider_id = '$a'";
                $query = $this->db->query($sql);
                foreach ($query->result() as $row) { ?>
                            <input type="hidden" name="providerid" value="<?php echo $a; ?>">
                              <tr class="xcrud-row xcrud-row-0">
                              <td><input class="checkboxcls" type="checkbox" name="rooms[]" value="<?php echo $row->id; ?>"></td>
                              <td class="xcrud-current xcrud-num"><?php echo $row->id; ?></td>
                              <td><?php echo $row->room_type; ?></td>
                              <td><?php if($row->status == 1){echo "Active";} else {echo "Inactive";}; ?></td>
                              <td class="xcrud-current xcrud-actions xcrud-fix"><span class="btn-group"><a class="btn btn-default btn-xcrud btn btn-warning" href="#<?php echo $row->id; ?>" title="Edit" data-toggle="modal"><i class="fa fa-edit"></i></a><a class="btn btn-default btn-xcrud btn-danger" href="<?php echo site_url('welcome/delete_roomtype/'.$row->id.''); ?>" title="DELETE" target="_self" ><i class="fa fa-times"></i></a></span></td>
                            </tr>
                              <?php  } }?>
                                </tbody>
                              <tfoot>
                            </tfoot>
                            </table>
                        </div>
                          <div class="xcrud-nav">
                          <div class="btn-group xcrud-limit-buttons" data-toggle="buttons-radio">
                              <button type="button" class="btn btn-default active xcrud-action" data-limit="10">10</button>
                              <button type="button" class="btn btn-default xcrud-action" data-limit="25">25</button>
                              <button type="button" class="btn btn-default xcrud-action" data-limit="50">50</button>
                              <button type="button" class="btn btn-default xcrud-action" data-limit="100">100</button>
                              <button type="button" class="btn btn-default xcrud-action" data-limit="all">All</button>
                            </div>
                          <!-- <ul class="pagination">
                      <li class="active"><span>1</span></li>
                      <li class=""><a href="javascript:;" class="xcrud-action" data-start="10">2</a></li>
                      <li class=""><a href="javascript:;" class="xcrud-action" data-start="20">3</a></li>
                    </ul> -->
                          <a class="xcrud-search-toggle btn btn-default" href="javascript:;">Search</a><span class="xcrud-search form-inline" style="display:none;">
                          <input class="xcrud-searchdata xcrud-search-active input-small form-control" name="phrase" data-type="text" style="" data-fieldtype="default" type="text" value="" />
                          <select class="xcrud-searchdata  input-small form-control" name="phrase" style="display:none" data-type="select" data-fieldtype="dropdown" data-fieldname="pt_hotels_types_settings.sett_selected">
                              <option value="Yes">Yes</option>
                              <option value="No">No</option>
                            </select>
                          <select class="xcrud-searchdata  input-small form-control" name="phrase" style="display:none" data-type="select" data-fieldtype="dropdown" data-fieldname="pt_hotels_types_settings.sett_status">
                              <option value="Yes">Yes</option>
                              <option value="No">No</option>
                            </select>
                          <select class="xcrud-data xcrud-columns-select input-small form-control" name="column">
                              <option value="">All fields</option>
                              <option value="pt_hotels_types_settings.sett_name" data-type="text">Name</option>
                              <option value="pt_hotels_types_settings.sett_selected" data-type="select">Selected</option>
                              <option value="pt_hotels_types_settings.sett_status" data-type="select">Status</option>
                            </select>
                        </div>
                        </div>
                      <div class="xcrud-overlay"></div>
                    </div>
                    </div>
                </div>
                  <div class="tab-pane wow fadeIn animated in" id="PAYMENT_METHODS">
                  <div class="add_button_modal" >
                      <button type="button" data-toggle="modal" data-target="#ADD_PAYMENT_TYPES" class="btn btn-success"><i class="glyphicon glyphicon-plus-sign"></i>Add</button>
                    </div>
                  <div class="xcrud">
                      <div class="xcrud-container">
                      <div class="xcrud-ajax">
                          <input type="hidden" class="xcrud-data" name="key" value="" />
                          <input type="hidden" class="xcrud-data" name="orderby" value="" />
                          <input type="hidden" class="xcrud-data" name="order" value="desc" />
                          <input type="hidden" class="xcrud-data" name="start" value="0" />
                          <input type="hidden" class="xcrud-data" name="limit" value="10" />
                          <input type="hidden" class="xcrud-data" name="instance" value="" />
                          <input type="hidden" class="xcrud-data" name="task" value="list" />
                          <div class="xcrud-top-actions">
                          <div class="btn-group pull-right"><a href="" class="btn btn-danger"><i class="glyphicon glyphicon-remove"></i>Delete Selected</a><a href="javascript:;" data-task="print" class="btn btn-default xcrud-in-new-window xcrud-action"><i class="glyphicon glyphicon-print"></i>Print</a><a href="javascript:;" data-task="csv" class="btn btn-default xcrud-in-new-window xcrud-action"><i class="glyphicon glyphicon-file"></i>Export into CSV</a></div>
                          <div class="clearfix"></div>
                        </div>
                          <div class="xcrud-list-container">
                          <table class="xcrud-list table table-striped table-hover">
                              <thead>
                              <tr class="xcrud-th">
                                  <th><input class=all type='checkbox' value='' id=select_all ></th>
                                  <th class="xcrud-num">&#35;</th>
                                  <th data-order="desc" data-orderby="pt_hotels_types_settings.sett_name" class="xcrud-column xcrud-action">Name</th>
                                  <th data-order="desc" data-orderby="pt_hotels_types_settings.sett_selected" class="xcrud-column xcrud-action">Selected</th>
                                  <th data-order="desc" data-orderby="pt_hotels_types_settings.sett_status" class="xcrud-column xcrud-action">Status</th>
                                  <th class="xcrud-actions">&nbsp;</th>
                                </tr>
                            </thead>
                              <tbody>
                              <tr class="xcrud-row xcrud-row-0">
                                  <td><input class="checkboxcls" type='checkbox' value=175></td>
                                  <td class="xcrud-current xcrud-num">1</td>
                                  <td>Pay on Arrival</td>
                                  <td>Yes</td>
                                  <td>Yes</td>
                                  <td class="xcrud-current xcrud-actions xcrud-fix"><span class="btn-group"><a class="btn btn-default btn-xcrud btn btn-warning" href="#sett175" title="Edit" data-toggle="modal"><i class="fa fa-edit"></i></a><a class="btn btn-default btn-xcrud btn-danger" href="" title="DELETE" target="_self" id="175"><i class="fa fa-times"></i></a></span></td>
                                </tr>
                              <tr class="xcrud-row xcrud-row-1">
                                  <td><input class="checkboxcls" type="checkbox" value="138"></td>
                                  <td class="xcrud-current xcrud-num">2</td>
                                  <td>Master/ Visa Card</td>
                                  <td>Yes</td>
                                  <td>Yes</td>
                                  <td class="xcrud-current xcrud-actions xcrud-fix"><span class="btn-group"><a class="btn btn-default btn-xcrud btn btn-warning" href="#sett138" title="Edit" data-toggle="modal"><i class="fa fa-edit"></i></a><a class="btn btn-default btn-xcrud btn-danger" href="" title="DELETE" target="_self" id="138"><i class="fa fa-times"></i></a></span></td>
                                </tr>
                              <tr class="xcrud-row xcrud-row-0">
                                  <td><input class=checkboxcls type='checkbox' value=137></td>
                                  <td class="xcrud-current xcrud-num">3</td>
                                  <td>Paypal</td>
                                  <td>No</td>
                                  <td>Yes</td>
                                  <td class="xcrud-current xcrud-actions xcrud-fix"><span class="btn-group"><a class="btn btn-default btn-xcrud btn btn-warning" href="#sett137" title="Edit" data-toggle="modal"><i class="fa fa-edit"></i></a><a class="btn btn-default btn-xcrud btn-danger" href="" title="DELETE" target="_self" id="137"><i class="fa fa-times"></i></a></span></td>
                                </tr>
                              <tr class="xcrud-row xcrud-row-1">
                                  <td><input class=checkboxcls type='checkbox' value=136></td>
                                  <td class="xcrud-current xcrud-num">4</td>
                                  <td>American Express</td>
                                  <td>Yes</td>
                                  <td>Yes</td>
                                  <td class="xcrud-current xcrud-actions xcrud-fix"><span class="btn-group"><a class="btn btn-default btn-xcrud btn btn-warning" href="#sett136" title="Edit" data-toggle="modal"><i class="fa fa-edit"></i></a><a class="btn btn-default btn-xcrud btn-danger" href="" title="DELETE" target="_self" id="136"><i class="fa fa-times"></i></a></span></td>
                                </tr>
                              <tr class="xcrud-row xcrud-row-0">
                                  <td><input class=checkboxcls type='checkbox' value=29></td>
                                  <td class="xcrud-current xcrud-num">5</td>
                                  <td>Wire Transfer</td>
                                  <td>Yes</td>
                                  <td>Yes</td>
                                  <td class="xcrud-current xcrud-actions xcrud-fix"><span class="btn-group"><a class="btn btn-default btn-xcrud btn btn-warning" href="#sett29" title="Edit" data-toggle="modal"><i class="fa fa-edit"></i></a><a class="btn btn-default btn-xcrud btn-danger" href="" title="DELETE" target="_self" id="29"><i class="fa fa-times"></i></a></span></td>
                                </tr>
                              <tr class="xcrud-row xcrud-row-1">
                                  <td><input class=checkboxcls type='checkbox' value=28></td>
                                  <td class="xcrud-current xcrud-num">6</td>
                                  <td>Credit Card</td>
                                  <td>Yes</td>
                                  <td>Yes</td>
                                  <td class="xcrud-current xcrud-actions xcrud-fix"><span class="btn-group"><a class="btn btn-default btn-xcrud btn btn-warning" href="#sett28" title="Edit" data-toggle="modal"><i class="fa fa-edit"></i></a><a class="btn btn-default btn-xcrud btn-danger" href="" title="DELETE" target="_self" id="28"><i class="fa fa-times"></i></a></span></td>
                                </tr>
                              <tr class="xcrud-row xcrud-row-0">
                                  <td><input class=checkboxcls type='checkbox' value=27></td>
                                  <td class="xcrud-current xcrud-num">7</td>
                                  <td>Skrill</td>
                                  <td>No</td>
                                  <td>Yes</td>
                                  <td class="xcrud-current xcrud-actions xcrud-fix"><span class="btn-group"><a class="btn btn-default btn-xcrud btn btn-warning" href="#sett27" title="Edit" data-toggle="modal"><i class="fa fa-edit"></i></a><a class="btn btn-default btn-xcrud btn-danger" href="" title="DELETE" target="_self" id="27"><i class="fa fa-times"></i></a></span></td>
                                </tr>
                            </tbody>
                              <tfoot>
                            </tfoot>
                            </table>
                        </div>
                          <div class="xcrud-nav">
                          <div class="btn-group xcrud-limit-buttons" data-toggle="buttons-radio">
                              <button type="button" class="btn btn-default active xcrud-action" data-limit="10">10</button>
                              <button type="button" class="btn btn-default xcrud-action" data-limit="25">25</button>
                              <button type="button" class="btn btn-default xcrud-action" data-limit="50">50</button>
                              <button type="button" class="btn btn-default xcrud-action" data-limit="100">100</button>
                              <button type="button" class="btn btn-default xcrud-action" data-limit="all">All</button>
                            </div>
                          <a class="xcrud-search-toggle btn btn-default" href="javascript:;">Search</a><span class="xcrud-search form-inline" style="display:none;">
                          <input class="xcrud-searchdata xcrud-search-active input-small form-control" name="phrase" data-type="text" style="" data-fieldtype="default" type="text" value="" />
                          <select class="xcrud-searchdata  input-small form-control" name="phrase" style="display:none" data-type="select" data-fieldtype="dropdown" data-fieldname="pt_hotels_types_settings.sett_selected">
                              <option value="Yes">Yes</option>
                              <option value="No">No</option>
                            </select>
                          <select class="xcrud-searchdata  input-small form-control" name="phrase" style="display:none" data-type="select" data-fieldtype="dropdown" data-fieldname="pt_hotels_types_settings.sett_status">
                              <option value="Yes">Yes</option>
                              <option value="No">No</option>
                            </select>
                          <select class="xcrud-data xcrud-columns-select input-small form-control" name="column">
                              <option value="">All fields</option>
                              <option value="pt_hotels_types_settings.sett_name" data-type="text">Name</option>
                              <option value="pt_hotels_types_settings.sett_selected" data-type="select">Selected</option>
                              <option value="pt_hotels_types_settings.sett_status" data-type="select">Status</option>
                            </select>
                        </div>
                        </div>
                      <div class="xcrud-overlay"></div>
                    </div>
                    </div>
                </div>
                  <!--             <div class="tab-pane wow fadeIn animated in" id="HOTELS_AMENITIES">
            <div class="add_button_modal" >
              <button type="button" data-toggle="modal" data-target="#ADD_HOTEL_AMT" class="btn btn-success"><i class="glyphicon glyphicon-plus-sign"></i>Add</button>
            </div>
            <div class="xcrud">
              <div class="xcrud-container">
                <div class="xcrud-ajax">
                  <input type="hidden" class="xcrud-data" name="key" value="" />
                  <input type="hidden" class="xcrud-data" name="orderby" value="" />
                  <input type="hidden" class="xcrud-data" name="order" value="desc" />
                  <input type="hidden" class="xcrud-data" name="start" value="0" />
                  <input type="hidden" class="xcrud-data" name="limit" value="10" />
                  <input type="hidden" class="xcrud-data" name="instance" value="" />
                  <input type="hidden" class="xcrud-data" name="task" value="list" />
                  <div class="xcrud-top-actions">
                    <div class="btn-group pull-right"><a href="" class="btn btn-danger"><i class="glyphicon glyphicon-remove"></i> Delete Selected</a><a href="javascript:;" data-task="print" class="btn btn-default xcrud-in-new-window xcrud-action"><i class="glyphicon glyphicon-print"></i> Print</a><a href="javascript:;" data-task="csv" class="btn btn-default xcrud-in-new-window xcrud-action"><i class="glyphicon glyphicon-file"></i> Export into CSV</a></div>
                    <div class="clearfix"></div>
                  </div>
                  <div class="xcrud-list-container">
                    <table class="xcrud-list table table-striped table-hover">
                      <thead>
                        <tr class="xcrud-th">
                          <th><input class=all type='checkbox' value='' id=select_all ></th>
                          <th class="xcrud-num">&#35;</th>
                          <th data-order="desc" data-orderby="" class="xcrud-column xcrud-action"> Icon</th>
                          <th data-order="desc" data-orderby="" class="xcrud-column xcrud-action"> Name</th>
                          <th data-order="desc" data-orderby="" class="xcrud-column xcrud-action"> Selected</th>
                          <th data-order="desc" data-orderby="" class="xcrud-column xcrud-action"> Status</th>
                          <th class="xcrud-actions">&nbsp;</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr class="xcrud-row xcrud-row-0">
                          <td><input class=checkboxcls type='checkbox' value=196></td>
                          <td class="xcrud-current xcrud-num">1</td>
                          <td class="zoom_img"><img alt="" src="" style="max-height: 20px;" /></td>
                          <td>Pets Allowed</td>
                          <td>Yes</td>
                          <td>Yes</td>
                          <td class="xcrud-current xcrud-actions xcrud-fix"><span class="btn-group"><a class="btn btn-default btn-xcrud btn btn-warning" href="#sett196" title="Edit" data-toggle="modal"><i class="fa fa-edit"></i></a><a class="btn btn-default btn-xcrud btn-danger" href="" title="DELETE" target="_self" id="196"><i class="fa fa-times"></i></a></span></td>
                        </tr>
                        <tr class="xcrud-row xcrud-row-1">
                          <td><input class=checkboxcls type='checkbox' value=195></td>
                          <td class="xcrud-current xcrud-num">2</td>
                          <td class="zoom_img"><img alt="" src="" style="max-height: 20px;" /></td>
                          <td>Elevator</td>
                          <td>Yes</td>
                          <td>Yes</td>
                          <td class="xcrud-current xcrud-actions xcrud-fix"><span class="btn-group"><a class="btn btn-default btn-xcrud btn btn-warning" href="#sett195" title="Edit" data-toggle="modal"><i class="fa fa-edit"></i></a><a class="btn btn-default btn-xcrud btn-danger" href="" title="DELETE" target="_self" id="195"><i class="fa fa-times"></i></a></span></td>
                        </tr>
                        <tr class="xcrud-row xcrud-row-0">
                          <td><input class=checkboxcls type='checkbox' value=194></td>
                          <td class="xcrud-current xcrud-num">3</td>
                          <td class="zoom_img"><img alt="" src="" style="max-height: 20px;" /></td>
                          <td>Cards Accepted</td>
                          <td>Yes</td>
                          <td>Yes</td>
                          <td class="xcrud-current xcrud-actions xcrud-fix"><span class="btn-group"><a class="btn btn-default btn-xcrud btn btn-warning" href="#sett194" title="Edit" data-toggle="modal"><i class="fa fa-edit"></i></a><a class="btn btn-default btn-xcrud btn-danger" href="" title="DELETE" target="_self" id="194"><i class="fa fa-times"></i></a></span></td>
                        </tr>
                        <tr class="xcrud-row xcrud-row-1">
                          <td><input class=checkboxcls type='checkbox' value=193></td>
                          <td class="xcrud-current xcrud-num">4</td>
                          <td class="zoom_img"><img alt="" src="" style="max-height: 20px;" /></td>
                          <td>Banquet Hall</td>
                          <td>Yes</td>
                          <td>Yes</td>
                          <td class="xcrud-current xcrud-actions xcrud-fix"><span class="btn-group"><a class="btn btn-default btn-xcrud btn btn-warning" href="#sett193" title="Edit" data-toggle="modal"><i class="fa fa-edit"></i></a><a class="btn btn-default btn-xcrud btn-danger" href="" title="DELETE" target="_self" id="193"><i class="fa fa-times"></i></a></span></td>
                        </tr>
                        <tr class="xcrud-row xcrud-row-0">
                          <td><input class=checkboxcls type='checkbox' value=192></td>
                          <td class="xcrud-current xcrud-num">5</td>
                          <td class="zoom_img"><img alt="" src="" style="max-height: 20px;" /></td>
                          <td>Air Conditioner</td>
                          <td>Yes</td>
                          <td>Yes</td>
                          <td class="xcrud-current xcrud-actions xcrud-fix"><span class="btn-group"><a class="btn btn-default btn-xcrud btn btn-warning" href="#sett192" title="Edit" data-toggle="modal"><i class="fa fa-edit"></i></a><a class="btn btn-default btn-xcrud btn-danger" href="" title="DELETE" target="_self" id="192"><i class="fa fa-times"></i></a></span></td>
                        </tr>
                        <tr class="xcrud-row xcrud-row-1">
                          <td><input class=checkboxcls type='checkbox' value=190></td>
                          <td class="xcrud-current xcrud-num">6</td>
                          <td class="zoom_img"><img alt="" src="" style="max-height: 20px;" /></td>
                          <td>Children Activites</td>
                          <td>Yes</td>
                          <td>Yes</td>
                          <td class="xcrud-current xcrud-actions xcrud-fix"><span class="btn-group"><a class="btn btn-default btn-xcrud btn btn-warning" href="#sett190" title="Edit" data-toggle="modal"><i class="fa fa-edit"></i></a><a class="btn btn-default btn-xcrud btn-danger" href="" title="DELETE" target="_self" id="190"><i class="fa fa-times"></i></a></span></td>
                        </tr>
                        <tr class="xcrud-row xcrud-row-0">
                          <td><input class=checkboxcls type='checkbox' value=189></td>
                          <td class="xcrud-current xcrud-num">7</td>
                          <td class="zoom_img"><img alt="" src="" style="max-height: 20px;" /></td>
                          <td>SPA</td>
                          <td>Yes</td>
                          <td>Yes</td>
                          <td class="xcrud-current xcrud-actions xcrud-fix"><span class="btn-group"><a class="btn btn-default btn-xcrud btn btn-warning" href="#sett189" title="Edit" data-toggle="modal"><i class="fa fa-edit"></i></a><a class="btn btn-default btn-xcrud btn-danger" href="" title="DELETE" target="_self" id="189"><i class="fa fa-times"></i></a></span></td>
                        </tr>
                        <tr class="xcrud-row xcrud-row-1">
                          <td><input class=checkboxcls type='checkbox' value=188></td>
                          <td class="xcrud-current xcrud-num">8</td>
                          <td class="zoom_img"><img alt="" src="" style="max-height: 20px;" /></td>
                          <td>Fitness Center</td>
                          <td>Yes</td>
                          <td>Yes</td>
                          <td class="xcrud-current xcrud-actions xcrud-fix"><span class="btn-group"><a class="btn btn-default btn-xcrud btn btn-warning" href="#sett188" title="Edit" data-toggle="modal"><i class="fa fa-edit"></i></a><a class="btn btn-default btn-xcrud btn-danger" href="" title="DELETE" target="_self" id="188"><i class="fa fa-times"></i></a></span></td>
                        </tr>
                        <tr class="xcrud-row xcrud-row-0">
                          <td><input class=checkboxcls type='checkbox' value=187></td>
                          <td class="xcrud-current xcrud-num">9</td>
                          <td class="zoom_img"><img alt="" src="" style="max-height: 20px;" /></td>
                          <td>Shuttle Bus Service</td>
                          <td>Yes</td>
                          <td>Yes</td>
                          <td class="xcrud-current xcrud-actions xcrud-fix"><span class="btn-group"><a class="btn btn-default btn-xcrud btn btn-warning" href="#sett187" title="Edit" data-toggle="modal"><i class="fa fa-edit"></i></a><a class="btn btn-default btn-xcrud btn-danger" href="" title="DELETE" target="_self" id="187"><i class="fa fa-times"></i></a></span></td>
                        </tr>
                        <tr class="xcrud-row xcrud-row-1">
                          <td><input class=checkboxcls type='checkbox' value=186></td>
                          <td class="xcrud-current xcrud-num">10</td>
                          <td class="zoom_img"><img alt="" src="" style="max-height: 20px;" /></td>
                          <td>Inside Parking</td>
                          <td>Yes</td>
                          <td>Yes</td>
                          <td class="xcrud-current xcrud-actions xcrud-fix"><span class="btn-group"><a class="btn btn-default btn-xcrud btn btn-warning" href="#sett186" title="Edit" data-toggle="modal"><i class="fa fa-edit"></i></a><a class="btn btn-default btn-xcrud btn-danger" href="" title="DELETE" target="_self" id="186"><i class="fa fa-times"></i></a></span></td>
                        </tr>
                      </tbody>
                      <tfoot>
                      </tfoot>
                    </table>
                  </div>
                  <div class="xcrud-nav">
                    <div class="btn-group xcrud-limit-buttons" data-toggle="buttons-radio">
                      <button type="button" class="btn btn-default active xcrud-action" data-limit="10">10</button>
                      <button type="button" class="btn btn-default xcrud-action" data-limit="25">25</button>
                      <button type="button" class="btn btn-default xcrud-action" data-limit="50">50</button>
                      <button type="button" class="btn btn-default xcrud-action" data-limit="100">100</button>
                      <button type="button" class="btn btn-default xcrud-action" data-limit="all">All</button>
                    </div>
                    <ul class="pagination">
                      <li class="active"><span>1</span></li>
                      <li class=""><a href="javascript:;" class="xcrud-action" data-start="10">2</a></li>
                    </ul>
                    <a class="xcrud-search-toggle btn btn-default" href="javascript:;">Search</a><span class="xcrud-search form-inline" style="display:none;">
                    <input class="xcrud-searchdata xcrud-search-active input-small form-control" name="phrase" data-type="text" style="" data-fieldtype="default" type="text" value="" />
                    <select class="xcrud-searchdata  input-small form-control" name="phrase" style="display:none" data-type="select" data-fieldtype="dropdown" data-fieldname="pt_hotels_types_settings.sett_selected">
                      <option value="Yes">Yes</option>
                      <option value="No">No</option>
                    </select>
                    <select class="xcrud-searchdata  input-small form-control" name="phrase" style="display:none" data-type="select" data-fieldtype="dropdown" data-fieldname="pt_hotels_types_settings.sett_status">
                      <option value="Yes">Yes</option>
                      <option value="No">No</option>
                    </select>
                    <select class="xcrud-data xcrud-columns-select input-small form-control" name="column">
                      <option value="">All fields</option>
                      <option value="pt_hotels_types_settings.sett_name" data-type="text">Name</option>
                      <option value="pt_hotels_types_settings.sett_selected" data-type="select">Selected</option>
                      <option value="pt_hotels_types_settings.sett_status" data-type="select">Status</option>
                    </select>
                  </div>
                </div>
                <div class="xcrud-overlay"></div>
              </div>
            </div>
          </div> -->
                  <div class="tab-pane wow fadeIn animated in" id="ROOMS_AMENITIES">
                  <div class="add_button_modal" >
                      <button type="button" data-toggle="modal" data-target="#ADD_ROOM_AMT" class="btn btn-success"><i class="glyphicon glyphicon-plus-sign"></i>Add Facility</button>
                    </div>
                  <div class="xcrud">
                      <div class="xcrud-container">
                      <div class="xcrud-ajax">
                          <input type="hidden" class="xcrud-data" name="key" value="" />
                          <input type="hidden" class="xcrud-data" name="orderby" value="" />
                          <input type="hidden" class="xcrud-data" name="order" value="desc" />
                          <input type="hidden" class="xcrud-data" name="start" value="0" />
                          <input type="hidden" class="xcrud-data" name="limit" value="10" />
                          <input type="hidden" class="xcrud-data" name="instance" value="" />
                          <input type="hidden" class="xcrud-data" name="task" value="list" />
                          <div class="xcrud-top-actions">
                          <div class="btn-group pull-right"><a href="" class="btn btn-danger"><i class="glyphicon glyphicon-remove"></i>Delete Selected</a><a href="javascript:;" data-task="print" class="btn btn-default xcrud-in-new-window xcrud-action"><i class="glyphicon glyphicon-print"></i>Print</a><a href="javascript:;" data-task="csv" class="btn btn-default xcrud-in-new-window xcrud-action"><i class="glyphicon glyphicon-file"></i>Export into CSV</a></div>
                          <div class="clearfix"></div>
                        </div>
                          <div class="xcrud-list-container">
                          <table class="xcrud-list table table-striped table-hover">
                              <thead>
                              <tr class="xcrud-th">
                                  <th><input class="all" type='checkbox' value='' id="select_all" ></th>
                                  <th class="xcrud-num">&#35;</th>
                                  <th data-order="desc" data-orderby="pt_hotels_types_settings.sett_name" class="xcrud-column xcrud-action">Name</th>
                                  <th data-order="desc" data-orderby="pt_hotels_types_settings.sett_status" class="xcrud-column xcrud-action">Status</th>
                                  <th class="xcrud-actions">&nbsp;</th>
                                </tr>
                            </thead>
                              <tbody>
                              <?php
               /* if(isset($_POST['provider'])){
                $a = $_POST['provider'];
                $sql   = "SELECT * FROM `tbl_facilities` WHERE provider_id = '$a'";
                $query = $this->db->query($sql);
                foreach ($query->result() as $row) {*/ ?>
                              <tr class="xcrud-row xcrud-row-0">
                                  <td><input class="checkboxcls" type="checkbox" name="facility[]" value="174"></td>
                                  <td class="xcrud-current xcrud-num"><?php // echo $row->id; ?></td>
                                  <td><?php // echo $row->facility_name; ?></td>
                                  <td><?php // echo $row->status; ?></td>
                                  <td class="xcrud-current xcrud-actions xcrud-fix"><span class="btn-group"><a class="btn btn-default btn-xcrud btn btn-warning" href="#facility<?php // echo $row->id; ?>" title="Edit" data-toggle="modal"><i class="fa fa-edit"></i></a><a class="btn btn-default btn-xcrud btn-danger" href="<?php // echo site_url('welcome/update_facility/'. $row->id.''); ?>" title="DELETE" target="_self" id="174"><i class="fa fa-times"></i></a></span></td>
                                </tr>
                              <?php  //} } ?>
                            </tbody>
                              <tfoot>
                            </tfoot>
                            </table>
                        </div>
                          <div class="xcrud-nav">
                          <div class="btn-group xcrud-limit-buttons" data-toggle="buttons-radio">
                              <button type="button" class="btn btn-default active xcrud-action" data-limit="10">10</button>
                              <button type="button" class="btn btn-default xcrud-action" data-limit="25">25</button>
                              <button type="button" class="btn btn-default xcrud-action" data-limit="50">50</button>
                              <button type="button" class="btn btn-default xcrud-action" data-limit="100">100</button>
                              <button type="button" class="btn btn-default xcrud-action" data-limit="all">All</button>
                            </div>
                          <!-- <ul class="pagination">
                      <li class="active"><span>1</span></li>
                      <li class=""><a href="javascript:;" class="xcrud-action" data-start="10">2</a></li>
                      <li class=""><a href="javascript:;" class="xcrud-action" data-start="20">3</a></li>
                      <li class=""><a href="javascript:;" class="xcrud-action" data-start="30">4</a></li>
                    </ul> -->
                          <a class="xcrud-search-toggle btn btn-default" href="javascript:;">Search</a><span class="xcrud-search form-inline" style="display:none;">
                          <input class="xcrud-searchdata xcrud-search-active input-small form-control" name="phrase" data-type="text" style="" data-fieldtype="default" type="text" value="" />
                          <select class="xcrud-searchdata  input-small form-control" name="phrase" style="display:none" data-type="select" data-fieldtype="dropdown" data-fieldname="pt_hotels_types_settings.sett_selected">
                              <option value="Yes">Yes</option>
                              <option value="No">No</option>
                            </select>
                          <select class="xcrud-searchdata  input-small form-control" name="phrase" style="display:none" data-type="select" data-fieldtype="dropdown" data-fieldname="pt_hotels_types_settings.sett_status">
                              <option value="Yes">Yes</option>
                              <option value="No">No</option>
                            </select>
                          <select class="xcrud-data xcrud-columns-select input-small form-control" name="column">
                              <option value="">All fields</option>
                              <option value="pt_hotels_types_settings.sett_name" data-type="text">Name</option>
                              <option value="pt_hotels_types_settings.sett_selected" data-type="select">Selected</option>
                              <option value="pt_hotels_types_settings.sett_status" data-type="select">Status</option>
                            </select>
                        </div>
                        </div>
                      <div class="xcrud-overlay"></div>
                    </div>
                    </div>
                </div>
                </div>
            </div>
              <div class="panel-footer">
              <input type="hidden" name="updatesettings" value="1" />
              <input type="hidden" name="updatefor" value="hotels" />
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>
            </div>
        </form>
          
          <!--Add hotel types Modal -->
          <div class="modal fade" id="ADD_HOTELS_TYPES" tabindex="-1" role="dialog" aria-labelledby =
    "ADD_HOTELS_TYPES" aria-hidden="true">
          <div class="modal-dialog">
              <div class="modal-content">
              <!--   <form action="<?php // echo site_url('Welcome/submit_socialmedia'); ?>" method="POST"> -->
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title">Add Social Media Links</h4>
                </div>
              <div class="modal-body form-horizontal">
                  <div class="row form-group">
                  <label  class="col-md-3 control-label text-left">Status</label>
                  <div class="col-md-8">
                      <select name="socialstatus" class="form-control" id="" required />
                                        <option value="1">Enable</option>
                      <option value="0">Disable</option>
                      </select>
                    </div>
                </div>
                  <div class="row form-group">
                  <label  class="col-md-3 control-label text-left">Facebook Link:</label>
                  <div class="col-md-8">
                      <input type="text" name="fblink" class="form-control" placeholder="Facebook url" value="" required />
                    </div>
                </div>
                  <div class="row form-group">
                  <label  class="col-md-3 control-label text-left">Twitter Link:</label>
                  <div class="col-md-8">
                      <input type="text" name="twitterlink" class="form-control" placeholder="Twitter url" value="" required />
                    </div>
                </div>
                  <div class="row form-group">
                  <label  class="col-md-3 control-label text-left">Google Plus Link:</label>
                  <div class="col-md-8">
                      <input type="text" name="googlepluslink" class="form-control" placeholder="Google plus url" value="" required />
                    </div>
                </div>
                  <div class="row form-group">
                  <label  class="col-md-3 control-label text-left">Pinterest Link:</label>
                  <div class="col-md-8">
                      <input type="text" name="pinterestlink" class="form-control" placeholder="Pinterest url" value="" required />
                    </div>
                </div>
                </div>
              <div class="modal-footer">
                  <input type="hidden" name="add" value="htypes" />
                  <input type="hidden" name="typeopt" value="htypes" />
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  <button type="submit" class="btn btn-primary">Add</button>
                </div>
              <!-- </form> -->
            </div>
            </div>
        </div>
          <!-----end add hotel types modal------>
          <!--Add room types Modal -->
          <div class="modal fade" id="ADD_ROOM_TYPES" tabindex="-1" role="dialog" aria-labelledby="ADD_ROOM_TYPES" aria-hidden="true">
          <div class="modal-dialog">
              <div class="modal-content">
              <!-- <form action="<?php // echo site_url('welcome/submit_roomtype/'); ?>" method="POST"> -->
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title">Add Room Type</h4>
                </div>
              <div class="modal-body form-horizontal">
                  <?php
                if(isset($_POST['provider'])){
                $a = $_POST['provider']; { ?>
                  <input type="hidden" value="<?php echo $a; }} ?>" name="providerid">
                  <div class="row form-group">
                  <label  class="col-md-3 control-label text-left">Status</label>
                  <div class="col-md-8">
                      <select name="roomstatus" class="form-control" id="">
                      <option value="1">Enable</option>
                      <option value="0">Disable</option>
                    </select>
                    </div>
                </div>
                  <div class="row form-group">
                  <label  class="col-md-3 control-label text-left">Room Type</label>
                  <div class="col-md-8">
                      <input type="text" name="roomtype" class="form-control" placeholder="Enter Room Type" value="">
                    </div>
                </div>
                  <div class="row form-group">
                  <label  class="col-md-3 control-label text-left">Wake-up calls</label>
                  <div class="col-md-8">
                      <select name="wakeupcalls" class="form-control" id="">
                      <option value="1">Enable</option>
                      <option value="0">Disable</option>
                    </select>
                    </div>
                </div>
                  <div class="row form-group">
                  <label  class="col-md-3 control-label text-left">In-room childcare (surcharge)</label>
                  <div class="col-md-8">
                      <select name="inroomchildcare" class="form-control" id="">
                      <option value="1">Enable</option>
                      <option value="0">Disable</option>
                    </select>
                    </div>
                </div>
                  <div class="row form-group">
                  <label  class="col-md-3 control-label text-left">Free wired high-speed Internet</label>
                  <div class="col-md-8">
                      <select name="freehispeedinternet" class="form-control" id="">
                      <option value="1">Enable</option>
                      <option value="0">Disable</option>
                    </select>
                    </div>
                </div>
                  <div class="row form-group">
                  <label  class="col-md-3 control-label text-left">Direct-dial phone</label>
                  <div class="col-md-8">
                      <select name="directdialphone" class="form-control" id="">
                      <option value="1">Enable</option>
                      <option value="0">Disable</option>
                    </select>
                    </div>
                </div>
                  <div class="row form-group">
                  <label  class="col-md-3 control-label text-left">Daily housekeeping</label>
                  <div class="col-md-8">
                      <select name="dailyhousekeeping" class="form-control" id="">
                      <option value="1">Enable</option>
                      <option value="0">Disable</option>
                    </select>
                    </div>
                </div>
                </div>
              <div class="modal-footer">
                  <input type="hidden" name="add" value="1" />
                  <input type="hidden" name="typeopt" value="rtypes" />
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  <button type="submit" class="btn btn-primary">Add</button>
                </div>
              
              <!--  </form> -->
            </div>
            </div>
        </div>
          <!-----end add ROOM types modal------>
          <!--Add payment types Modal -->
          <!-- <div class="modal fade" id="ADD_PAYMENT_TYPES" tabindex="-1" role="dialog" aria-labelledby="ADD_PAYMENT_TYPES" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <form action="" method="POST">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Add Payment Type</h4>
          </div>
          <div class="modal-body form-horizontal">
            <div class="row form-group">
              <label  class="col-md-3 control-label text-left">Type Name</label>
              <div class="col-md-8">
                <input type="text" name="name" class="form-control" placeholder="Name" value="" >
              </div>
            </div>
            <div class="row form-group">
              <label  class="col-md-3 control-label text-left">Selected</label>
              <div class="col-md-8">
                <select name="setselect" class="form-control" id="">
                  <option value="Yes">Yes</option>
                  <option value="No">No</option>
                </select>
              </div>
            </div>
            <div class="row form-group">
              <label  class="col-md-3 control-label text-left">Status</label>
              <div class="col-md-8">
                <select name="statusopt" class="form-control" id="">
                  <option value="Yes">Enable</option>
                  <option value="No">Disable</option>
                </select>
              </div>
            </div>
            <div class="row form-group">
              <label  class="col-md-3 control-label text-left">Name in<img src="img/ar.png" height="20" alt="" />&nbsp;Arabic</label>
              <div class="col-md-8">
                <input type="text" name='translated[ar][name]' class="form-control" placeholder="Name" value="" >
              </div>
            </div>
            <div class="row form-group">
              <label  class="col-md-3 control-label text-left">Name in<img src="img/tl.png" height="20" alt="" />&nbsp;Filipino</label>
              <div class="col-md-8">
                <input type="text" name='translated[tl][name]' class="form-control" placeholder="Name" value="" >
              </div>
            </div>
            <div class="row form-group">
              <label  class="col-md-3 control-label text-left">Name in<img src="img/pk.png" height="20" alt="" />&nbsp;Urdu</label>
              <div class="col-md-8">
                <input type="text" name='translated[pk][name]' class="form-control" placeholder="Name" value="" >
              </div>
            </div>
            <div class="row form-group">
              <label  class="col-md-3 control-label text-left">Name in<img src="img/fr.png" height="20" alt="" />&nbsp;French</label>
              <div class="col-md-8">
                <input type="text" name='translated[fr][name]' class="form-control" placeholder="Name" value="" >
              </div>
            </div>
            <div class="row form-group">
              <label  class="col-md-3 control-label text-left">Name in<img src="img/ru.png" height="20" alt="" />&nbsp;Russian</label>
              <div class="col-md-8">
                <input type="text" name='translated[ru][name]' class="form-control" placeholder="Name" value="" >
              </div>
            </div>
            <div class="row form-group">
              <label  class="col-md-3 control-label text-left">Name in<img src="img/ms.png" height="20" alt="" />&nbsp;Malay</label>
              <div class="col-md-8">
                <input type="text" name='translated[ms][name]' class="form-control" placeholder="Name" value="" >
              </div>
            </div>
            <div class="row form-group">
              <label  class="col-md-3 control-label text-left">Name in<img src="img/es.png" height="20" alt="" />&nbsp;Spanish</label>
              <div class="col-md-8">
                <input type="text" name='translated[es][name]' class="form-control" placeholder="Name" value="" >
              </div>
            </div>
            <div class="row form-group">
              <label  class="col-md-3 control-label text-left">Name in<img src="img/pt.png" height="20" alt="" />&nbsp;Portuguese</label>
              <div class="col-md-8">
                <input type="text" name='translated[pt][name]' class="form-control" placeholder="Name" value="" >
              </div>
            </div>
            <div class="row form-group">
              <label  class="col-md-3 control-label text-left">Name in<img src="img/uk.png" height="20" alt="" />&nbsp;Ukrainian</label>
              <div class="col-md-8">
                <input type="text" name='translated[uk][name]' class="form-control" placeholder="Name" value="" >
              </div>
            </div>
            <div class="row form-group">
              <label  class="col-md-3 control-label text-left">Name in<img src="img/it.png" height="20" alt="" />&nbsp;Italian</label>
              <div class="col-md-8">
                <input type="text" name='translated[it][name]' class="form-control" placeholder="Name" value="" >
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <input type="hidden" name="add" value="1" />
            <input type="hidden" name="typeopt" value="hpayments" />
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Add</button>
          </div>
        </form>
      </div>
    </div>
  </div> -->
          <!-----end add payment types modal------>
          <!--Add hotel amenities types Modal -->
          
          <div class="modal fade" id="ADD_HOTEL_AMT" tabindex="-1" role="dialog" aria-labelledby="ADD_HOTEL_AMT" aria-hidden="true">
          <div class="modal-dialog">
              <div class="modal-content">
              <!--  <form action="" method="POST" enctype="multipart/form-data" > -->
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title">Add Hotel Amenity</h4>
                </div>
              <div class="modal-body form-horizontal">
                  <div class="row form-group">
                  <label  class="col-md-3 control-label text-left">Amenity Name</label>
                  <div class="col-md-8">
                      <input type="text" name="name" class="form-control" placeholder="Name" value="" >
                    </div>
                </div>
                  <div class="row form-group">
                  <label class="col-md-3 control-label text-left">Icon</label>
                  <div class="col-md-8">
                      <input type="file" name="amticon" id="amticon" />
                    </div>
                </div>
                  <div class="row form-group">
                  <label  class="col-md-3 control-label text-left">Selected</label>
                  <div class="col-md-8">
                      <select name="setselect" class="form-control" id="">
                      <option value="Yes">Yes</option>
                      <option value="No">No</option>
                    </select>
                    </div>
                </div>
                  <div class="row form-group">
                  <label  class="col-md-3 control-label text-left">Name in<img src="img/uk.png" height="20" alt="" />&nbsp;Ukrainian</label>
                  <div class="col-md-8">
                      <input type="text" name='translated[uk][name]' class="form-control" placeholder="Name" value="" >
                    </div>
                </div>
                  <div class="row form-group">
                  <label  class="col-md-3 control-label text-left">Name in<img src="img/it.png" height="20" alt="" />&nbsp;Italian</label>
                  <div class="col-md-8">
                      <input type="text" name='translated[it][name]' class="form-control" placeholder="Name" value="" >
                    </div>
                </div>
                </div>
              <div class="modal-footer">
                  <input type="hidden" name="add" value="hamenities" />
                  <input type="hidden" name="typeopt" value="hamenities" />
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  <button type="submit" class="btn btn-primary">Add</button>
                </div>
              <!-- </form> -->
            </div>
            </div>
        </div>
          <!-----end add hotel amenity modal------>
          <!--Add room amenities types Modal -->
          <div class="modal fade" id="ADD_ROOM_AMT" tabindex="-1" role="dialog" aria-labelledby="ADD_ROOM_AMT" aria-hidden="true">
          <div class="modal-dialog">
              <div class="modal-content">
              <!--   <form action="<?php // echo site_url('welcome/submit_facility'); ?>" method="POST"> -->
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title">Add Facility</h4>
                </div>
              <div class="modal-body form-horizontal">
                  <input type="hidden" name="providerid" value="<?php   ?>">
                  <div class="row form-group">
                  <label  class="col-md-3 control-label text-left">Status:</label>
                  <div class="col-md-8">
                      <select name="status" class="form-control" id="">
                      <option value="1">Enable</option>
                      <option value="0">Disable</option>
                    </select>
                    </div>
                </div>
                  <div class="row form-group">
                  <label  class="col-md-3 control-label text-left">Facility Name</label>
                  <div class="col-md-8">
                      <input type="text" name="facility" class="form-control" placeholder="Name" value="" >
                    </div>
                </div>
                </div>
              <div class="modal-footer">
                  <input type="hidden" name="add" value="1" />
                  <input type="hidden" name="typeopt" value="ramenities" />
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  <button type="submit" class="btn btn-primary">Add</button>
                </div>
              <!-- </form> -->
            </div>
            </div>
        </div>
          <!-- end add room amenity modal -->
          
          <!-- Edit Modal -->
          
          <div class="modal fade" id="sett191" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
          <div class="modal-dialog">
              <div class="modal-content">
              <?php
                if(isset($_POST['provider'])){
                $a = $_POST['provider'];
                $sql   = "SELECT * FROM `tbl_socialmedia` WHERE provider_id = '$a'";
                $query = $this->db->query($sql);
                foreach ($query->result() as $row) { ?>
              <form action="<?php echo site_url('welcome/update_socialmedia/'.$row->id.''); ?>" method="POST" enctype="multipart/form-data">
                  <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title">Update Social Media Links</h4>
                </div>
                  <div class="modal-body form-horizontal">
                  <input type="hidden" name="providerid" value="<?php echo $a; ?>">
                  <div class="row form-group">
                      <label  class="col-md-3 control-label text-left">Status</label>
                      <div class="col-md-8">
                      <select name="socialstatus" class="form-control" id="">
                          <option value="<?php if($row->status == 1){echo 'selected';} ?>">Enable</option>
                          <option value="<?php if($row->status == 0){echo 'selected';} ?>">Disable</option>
                        </select>
                    </div>
                    </div>
                  <div class="row form-group">
                      <label  class="col-md-3 control-label text-left">Facebook Link:</label>
                      <div class="col-md-8">
                      <input type="text" name="fblink" class="form-control" placeholder="Facebook url" value="<?php echo $row->facebook; ?>" >
                    </div>
                    </div>
                  <div class="row form-group">
                      <label  class="col-md-3 control-label text-left">Twitter Link:</label>
                      <div class="col-md-8">
                      <input type="text" name="twitterlink" class="form-control" placeholder="Twitter url" value="<?php echo $row->twitter; ?>" >
                    </div>
                    </div>
                  <div class="row form-group">
                      <label  class="col-md-3 control-label text-left">Google Plus Link:</label>
                      <div class="col-md-8">
                      <input type="text" name="googlepluslink" class="form-control" placeholder="Google plus url" value="<?php echo $row->googleplus; ?>" >
                    </div>
                    </div>
                  <div class="row form-group">
                      <label  class="col-md-3 control-label text-left">Pinterest Link:</label>
                      <div class="col-md-8">
                      <input type="text" name="pinterestlink" class="form-control" placeholder="Pinterest url" value="<?php echo $row->pinterest; ?>" >
                    </div>
                    </div>
                </div>
                  <div class="modal-footer">
                  <input type="hidden" name="updatetype" value="1" />
                  <input type="hidden" name="oldicon" value="" />
                  <input type="hidden" name="settid" value="191" />
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  <button type="submit" class="btn btn-primary">Update</button>
                </div>
                </form>
              <?php } } ?>
            </div>
            </div>
        </div>
          <?php // foreach($view_facilities as $facility) { ?>
          <div class="modal fade" id="facility<?php //echo $facility['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
          <div class="modal-dialog">
              <div class="modal-content">
              <form action="<?php // echo site_url('welcome/update_facility/'. $facility['id'] .''); ?>" method="POST" enctype="multipart/form-data">
                  <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title">Update Facilities</h4>
                </div>
                  <div class="modal-body form-horizontal">
                  <div class="row form-group">
                      <label  class="col-md-3 control-label text-left">Selected</label>
                      <div class="col-md-8">
                      <select name="setselect" class="form-control" id="">
                          <option value="1" <?php //if($facility['status'] == 1){ echo "selected"; } ?>>Enable</option>
                          <option value="0" <?php // if($facility['status'] == 0){ echo "selected"; } ?>>Disable</option>
                        </select>
                    </div>
                    </div>
                  <div class="row form-group">
                      <label  class="col-md-3 control-label text-left">Facility Name</label>
                      <div class="col-md-8">
                      <input type="text" name="facility" class="form-control" placeholder="Facility Name" value="<?php // echo $facility['facility_name']; ?>" >
                    </div>
                    </div>
                </div>
                  <div class="modal-footer">
                  <input type="hidden" name="updatetype" value="1" />
                  <input type="hidden" name="oldicon" value="" />
                  <input type="hidden" name="settid" value="174" />
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  <button type="submit" class="btn btn-primary">Update</button>
                </div>
                </form>
            </div>
            </div>
        </div>
          <?php // } ?>
          <div class="modal fade" id="sett164" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
          <div class="modal-dialog">
              <div class="modal-content">
              <form action="" method="POST" enctype="multipart/form-data">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title">Update City view</h4>
                </div>
              <div class="modal-body form-horizontal">
                  <div class="row form-group">
                  <label  class="col-md-3 control-label text-left">Type Name</label>
                  <div class="col-md-8">
                      <input type="text" name="name" class="form-control" placeholder="Name" value="City view" >
                    </div>
                </div>
                  <div class="row form-group">
                  <label  class="col-md-3 control-label text-left">Selected</label>
                  <div class="col-md-8">
                      <select name="setselect" class="form-control" id="">
                      <option value="Yes" selected >Yes</option>
                      <option value="No"   >No</option>
                    </select>
                    </div>
                </div>
                  <div class="row form-group">
                  <label  class="col-md-3 control-label text-left">Status</label>
                  <div class="col-md-8">
                      <select name="statusopt" class="form-control" id="">
                      <option value="Yes" selected >Enable</option>
                      <option value="No"   >Disable</option>
                    </select>
                    </div>
                </div>
                  <div class="row form-group">
                  <label  class="col-md-3 control-label text-left">Name in<img src="img/ar.png" height="20" alt="" />&nbsp;Arabic</label>
                  <div class="col-md-8">
                      <input type="text" name='translated[ar][name]' class="form-control" placeholder="Name" value="مطلة على المدينة" >
                    </div>
                </div>
                  <div class="row form-group">
                  <label  class="col-md-3 control-label text-left">Name in<img src="img/tl.png" height="20" alt="" />&nbsp;Filipino</label>
                  <div class="col-md-8">
                      <input type="text" name='translated[tl][name]' class="form-control" placeholder="Name" value="" >
                    </div>
                </div>
                  <div class="row form-group">
                  <label  class="col-md-3 control-label text-left">Name in<img src="img/pk.png" height="20" alt="" />&nbsp;Urdu</label>
                  <div class="col-md-8">
                      <input type="text" name='translated[pk][name]' class="form-control" placeholder="Name" value="" >
                    </div>
                </div>
                </div>
              <div class="row form-group">
                  <label  class="col-md-3 control-label text-left">Name in<img src="img/it.png" height="20" alt="" />&nbsp;Italian</label>
                  <div class="col-md-8">
                  <input type="text" name='translated[it][name]' class="form-control" placeholder="Name" value="vista città" >
                </div>
                </div>
              </div>
              <div class="modal-footer">
                  <input type="hidden" name="updatetype" value="1" />
                  <input type="hidden" name="oldicon" value="" />
                  <input type="hidden" name="settid" value="164" />
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  <button type="submit" class="btn btn-primary">Update</button>
                </div>
            </form>
            </div>
        </div>
        </div>
      <div class="modal fade" id="sett136" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
          <div class="modal-dialog">
          <div class="modal-content">
              <form action="" method="POST" enctype="multipart/form-data">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title">Update American Express</h4>
                </div>
              <div class="modal-body form-horizontal">
                  <div class="row form-group">
                  <label  class="col-md-3 control-label text-left">Type Name</label>
                  <div class="col-md-8">
                      <input type="text" name="name" class="form-control" placeholder="Name" value="American Express" >
                    </div>
                </div>
                  <div class="row form-group">
                  <label  class="col-md-3 control-label text-left">Selected</label>
                  <div class="col-md-8">
                      <select name="setselect" class="form-control" id="">
                      <option value="Yes" selected >Yes</option>
                      <option value="No">No</option>
                    </select>
                    </div>
                </div>
                  <div class="row form-group">
                  <label  class="col-md-3 control-label text-left">Status</label>
                  <div class="col-md-8">
                      <select name="statusopt" class="form-control" id="">
                      <option value="Yes" selected >Enable</option>
                      <option value="No">Disable</option>
                    </select>
                    </div>
                </div>
                  <div class="row form-group">
                  <label  class="col-md-3 control-label text-left">Name in<img src="img/it.png" height="20" alt="" />&nbsp;Italian</label>
                  <div class="col-md-8">
                      <input type="text" name='translated[it][name]' class="form-control" placeholder="Name" value="American Express" >
                    </div>
                </div>
                </div>
              <div class="modal-footer">
                  <input type="hidden" name="updatetype" value="" />
                  <input type="hidden" name="oldicon" value="" />
                  <input type="hidden" name="settid" value="" />
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  <button type="submit" class="btn btn-primary">Update</button>
                </div>
            </form>
            </div>
        </div>
        </div>
      <?php if(isset($_POST['provider'])){
                $a = $_POST['provider'];
                $sql   = "SELECT * FROM `tbl_room` WHERE provider_id = '$a'";
                $query = $this->db->query($sql);
                foreach ($query->result() as $row) { ?>
      <div class="modal fade" id="<?php echo $row->id; ?>" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
          <div class="modal-dialog">
          <div class="modal-content">
              <form action="<?php echo site_url('welcome/update_roomtype/'. $row->id .''); ?>" method="POST" enctype="multipart/form-data">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title">Update Room Types<?php echo $row->id; ?></h4>
                </div>
              <div class="modal-body form-horizontal">
                  <div class="row form-group">
                  <label  class="col-md-3 control-label text-left">Status</label>
                  <div class="col-md-8">
                      <input type="hidden" value="<?php echo $a; ?>" name="providerid">
                      <select name="roomstatus" class="form-control" id="">
                      <option value="1" <?php if($row->status == 1){ echo "selected"; } ?>>Enable</option>
                      <option value="0" <?php if($row->status == 0){ echo "selected"; } ?>>Disable</option>
                    </select>
                    </div>
                </div>
                  <div class="row form-group">
                  <label  class="col-md-3 control-label text-left">Room Type</label>
                  <div class="col-md-8">
                      <input type="text" name="roomtype" class="form-control"
              value="<?php echo $row->room_type; ?>">
                    </div>
                </div>
                  <div class="row form-group">
                  <label  class="col-md-3 control-label text-left">Wake-up calls</label>
                  <div class="col-md-8">
                      <select name="wakeupcalls" class="form-control" id="">
                      <option value="1" <?php if($row->wakeup_calls == 1){ echo "selected"; } ?>>Enable</option>
                      <option value="0" <?php if($row->wakeup_calls == 0){ echo "selected"; } ?>>Disable</option>
                    </select>
                    </div>
                </div>
                  <div class="row form-group">
                  <label  class="col-md-3 control-label text-left">In-room childcare (surcharge)</label>
                  <div class="col-md-8">
                      <select name="inroomchildcare" class="form-control" id="">
                      <option value="1" <?php if($row->inroom_childcare == 1){ echo "selected"; } ?>>Enable</option>
                      <option value="0" <?php if($row->inroom_childcare == 0){ echo "selected"; } ?>>Disable</option>
                    </select>
                    </div>
                </div>
                  <div class="row form-group">
                  <label  class="col-md-3 control-label text-left">Free wired high-speed Internet</label>
                  <div class="col-md-8">
                      <select name="freehispeedinternet" class="form-control" id="">
                      <option value="1" <?php if($row->hispeed_internet == 1){ echo "selected"; } ?>>Enable</option>
                      <option value="0" <?php if($row->hispeed_internet == 0){ echo "selected"; } ?>>Disable</option>
                    </select>
                    </div>
                </div>
                  <div class="row form-group">
                  <label  class="col-md-3 control-label text-left">Direct-dial phone</label>
                  <div class="col-md-8">
                      <select name="directdialphone" class="form-control" id="">
                      <option value="1" <?php if($row->dial_phonecall == 1){ echo "selected"; } ?>>Enable</option>
                      <option value="0" <?php if($row->dial_phonecall == 0){ echo "selected"; } ?>>Disable</option>
                    </select>
                    </div>
                </div>
                  <div class="row form-group">
                  <label  class="col-md-3 control-label text-left">Daily housekeeping</label>
                  <div class="col-md-8">
                      <select name="dailyhousekeeping" class="form-control" id="">
                      <option value="1" <?php if($row->daily_housekeeping == 1){ echo "selected"; } ?>>Enable</option>
                      <option value="0" <?php if($row->daily_housekeeping == 0){ echo "selected"; } ?>>Disable</option>
                    </select>
                    </div>
                </div>
                </div>
              <div class="modal-footer">
                  <input type="hidden" name="updatetype" value="" />
                  <input type="hidden" name="oldicon" value="" />
                  <input type="hidden" name="settid" value="" />
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  <button type="submit" class="btn btn-primary">Update</button>
                </div>
            </form>
            </div>
        </div>
        </div>
      <?php } } ?>
      <div class="modal fade" id="sett78" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
          <div class="modal-dialog">
          <div class="modal-content">
              <form action="" method="POST" enctype="multipart/form-data">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title">Update Restaurant</h4>
                </div>
              <div class="modal-body form-horizontal">
                  <div class="row form-group">
                  <label  class="col-md-3 control-label text-left">Type Name</label>
                  <div class="col-md-8">
                      <input type="text" name="name" class="form-control" placeholder="Name" value="Restaurant" >
                    </div>
                </div>
                  <div class="row form-group">
                  <label  class="col-md-3 control-label text-left">Name in<img src="img/it.png" height="20" alt="" />&nbsp;Italian</label>
                  <div class="col-md-8">
                      <input type="text" name='translated[it][name]' class="form-control" placeholder="Name" value="Ristorante" >
                    </div>
                </div>
                </div>
              <div class="modal-footer">
                  <input type="hidden" name="updatetype" value="1" />
                  <input type="hidden" name="oldicon" value="79773_breakfast.png" />
                  <input type="hidden" name="settid" value="78" />
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  <button type="submit" class="btn btn-primary">Update</button>
                </div>
            </form>
            </div>
        </div>
        </div>
      <div class="modal fade" id="sett71" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
          <div class="modal-dialog">
          <div class="modal-content">
              <form action="" method="POST" enctype="multipart/form-data">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title">Update Laundry Service</h4>
                </div>
              <div class="modal-body form-horizontal">
                  <div class="row form-group">
                  <label  class="col-md-3 control-label text-left">Type Name</label>
                  <div class="col-md-8">
                      <input type="text" name="name" class="form-control" placeholder="Name" value="Laundry Service" >
                    </div>
                </div>
                  <div class="row form-group"><img style="margin-top:-10px" src="http://phptravels.net/uploads/images/hotels/amenities/813018_laundry.png" alt="" />
                  <label class="col-md-3 control-label text-left">Icon</label>
                  <div class="col-md-7">
                      <input type="file" name="amticon" id="amticon" />
                    </div>
                </div>
                  <div class="row form-group">
                  <label  class="col-md-3 control-label text-left">Selected</label>
                  <div class="col-md-8">
                      <select name="setselect" class="form-control" id="">
                      <option value="Yes" selected >Yes</option>
                      <option value="No"   >No</option>
                    </select>
                    </div>
                </div>
                  <div class="row form-group">
                  <label  class="col-md-3 control-label text-left">Status</label>
                  <div class="col-md-8">
                      <select name="statusopt" class="form-control" id="">
                      <option value="Yes" selected >Enable</option>
                      <option value="No"   >Disable</option>
                    </select>
                    </div>
                </div>
                  <div class="row form-group">
                  <label  class="col-md-3 control-label text-left">Name in<img src="img/uk.png" height="20" alt="" />&nbsp;Ukrainian</label>
                  <div class="col-md-8">
                      <input type="text" name='translated[uk][name]' class="form-control" placeholder="Name" value="" >
                    </div>
                </div>
                  <div class="row form-group">
                  <label  class="col-md-3 control-label text-left">Name in<img src="img/it.png" height="20" alt="" />&nbsp;Italian</label>
                  <div class="col-md-8">
                      <input type="text" name='translated[it][name]' class="form-control" placeholder="Name" value="Servizio Di Lavanderia" >
                    </div>
                </div>
                </div>
              <div class="modal-footer">
                  <input type="hidden" name="updatetype" value="1" />
                  <input type="hidden" name="oldicon" value="813018_laundry.png" />
                  <input type="hidden" name="settid" value="71" />
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  <button type="submit" class="btn btn-primary">Update</button>
                </div>
            </form>
            </div>
        </div>
        </div>
      <div class="modal fade" id="sett62" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
          <div class="modal-dialog">
          <div class="modal-content">
              <form action="" method="POST" enctype="multipart/form-data">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title">Update Airport Transport</h4>
                </div>
              <div class="modal-body form-horizontal">
                  <div class="row form-group">
                  <label  class="col-md-3 control-label text-left">Type Name</label>
                  <div class="col-md-8">
                      <input type="text" name="name" class="form-control" placeholder="Name" value="Airport Transport" >
                    </div>
                </div>
                  <div class="row form-group"><img style="margin-top:-10px" src="http://phptravels.net/uploads/images/hotels/amenities/522827_airport.png" alt="" />
                  <label class="col-md-3 control-label text-left">Icon</label>
                  <div class="col-md-7">
                      <input type="file" name="amticon" id="amticon" />
                    </div>
                </div>
                  <div class="row form-group">
                  <label  class="col-md-3 control-label text-left">Name in<img src="img/it.png" height="20" alt="" />&nbsp;Italian</label>
                  <div class="col-md-8">
                      <input type="text" name='translated[it][name]' class="form-control" placeholder="Name" value="Trasporti Aeroporto" >
                    </div>
                </div>
                </div>
              <div class="modal-footer">
                  <input type="hidden" name="updatetype" value="1" />
                  <input type="hidden" name="oldicon" value="522827_airport.png" />
                  <input type="hidden" name="settid" value="62" />
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  <button type="submit" class="btn btn-primary">Update</button>
                </div>
            </form>
            </div>
        </div>
        </div>
      <div class="modal fade" id="sett29" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
          <div class="modal-dialog">
          <div class="modal-content">
              <form action="" method="POST" enctype="multipart/form-data">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title">Update Wire Transfer</h4>
                </div>
              <div class="modal-body form-horizontal">
                  <div class="row form-group">
                  <label  class="col-md-3 control-label text-left">Type Name</label>
                  <div class="col-md-8">
                      <input type="text" name="name" class="form-control" placeholder="Name" value="Wire Transfer" >
                    </div>
                </div>
                  <div class="row form-group">
                  <label  class="col-md-3 control-label text-left">Selected</label>
                  <div class="col-md-8">
                      <select name="setselect" class="form-control" id="">
                      <option value="Yes" selected >Yes</option>
                      <option value="No"   >No</option>
                    </select>
                    </div>
                </div>
                  <div class="row form-group">
                  <label  class="col-md-3 control-label text-left">Status</label>
                  <div class="col-md-8">
                      <select name="statusopt" class="form-control" id="">
                      <option value="Yes" selected >Enable</option>
                      <option value="No"   >Disable</option>
                    </select>
                    </div>
                </div>
                  <div class="row form-group">
                  <label  class="col-md-3 control-label text-left">Name in<img src="img/ar.png" height="20" alt="" />&nbsp;Arabic</label>
                  <div class="col-md-8">
                      <input type="text" name='translated[ar][name]' class="form-control" placeholder="Name" value="التحويل عن طريق البنك" >
                    </div>
                </div>
                  <div class="row form-group">
                  <label  class="col-md-3 control-label text-left">Name in<img src="img/it.png" height="20" alt="" />&nbsp;Italian</label>
                  <div class="col-md-8">
                      <input type="text" name='translated[it][name]' class="form-control" placeholder="Name" value="Trasferimento da Bank" >
                    </div>
                </div>
                </div>
              <div class="modal-footer">
                  <input type="hidden" name="updatetype" value="1" />
                  <input type="hidden" name="oldicon" value="" />
                  <input type="hidden" name="settid" value="29" />
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  <button type="submit" class="btn btn-primary">Update</button>
                </div>
            </form>
            </div>
        </div>
        </div>
      <div class="modal fade" id="sett27" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
          <div class="modal-dialog">
          <div class="modal-content">
              <form action="" method="POST" enctype="multipart/form-data">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title">Update Skrill</h4>
                </div>
              <div class="modal-body form-horizontal">
                  <div class="row form-group">
                  <label  class="col-md-3 control-label text-left">Type Name</label>
                  <div class="col-md-8">
                      <input type="text" name="name" class="form-control" placeholder="Name" value="Skrill" >
                    </div>
                </div>
                  <div class="row form-group">
                  <label  class="col-md-3 control-label text-left">Selected</label>
                  <div class="col-md-8">
                      <select name="setselect" class="form-control" id="">
                      <option value="Yes"  >Yes</option>
                      <option value="No" selected  >No</option>
                    </select>
                    </div>
                </div>
                  <div class="row form-group">
                  <label  class="col-md-3 control-label text-left">Status</label>
                  <div class="col-md-8">
                      <select name="statusopt" class="form-control" id="">
                      <option value="Yes" selected >Enable</option>
                      <option value="No"   >Disable</option>
                    </select>
                    </div>
                </div>
                </div>
              <div class="modal-footer">
                  <input type="hidden" name="updatetype" value="1" />
                  <input type="hidden" name="oldicon" value="" />
                  <input type="hidden" name="settid" value="27" />
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  <button type="submit" class="btn btn-primary">Update</button>
                </div>
            </form>
            </div>
        </div>
        </div>
      <!----edit modal--->
      <script>
$(document).ready(function(){
if(window.location.hash != "") {
$('a[href="' + window.location.hash + '"]').click() } });
</script>
    </div>
    </div>
</div>
  <?php include('footer.php'); ?>