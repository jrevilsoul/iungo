<!DOCTYPE html>
<style>
ul.list_menu_supplier {
	
	width:100%;
	display:block;
	margin:40px 0px 0px 0px;
	padding:10px;    padding-top: 10px;
}
ul.list_menu_supplier li {
	    width:48%;
    margin: 0px;
    padding: 10px 0px 10px 0px;
    background:#2aa09c;
    display: inline-block;
    margin-right: 31px;    margin-bottom: 55px; border-bottom:solid 5px #075653; color:#fff;
} 
ul.list_menu_supplier li div{ padding:0px 20px 0px 20px;}
.logo_moduls {
    float: left;
    width: 20%;
}
.logo_moduls i {
    font-size: 127px;
}
ul.list_menu_supplier li h2{ font-size:20px;}
ul.list_menu_supplier li:nth-child(2n+2){ margin-right:0px} 

.top_header_main{ width:100%; margin:0px; padding:5px 0px 5px 0px; display:block; background:#fff; margin-bottom:1px;}

</style>
<?php include('headerandsidebar.php'); ?>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
   
   <ul class="list_menu_supplier">
      <div class="container">
        <li class="color_1">
        <div class="supplier_dri">
        <div class="logo_moduls"> <i class="fa fa-users" aria-hidden="true"></i> </div>
        <div class="logo_moduls_con pull-right">
        <h3><?php echo $membership;?></h3>
        <h4>Membership to Stay</h4>
        </div>
        
        <div class="clearfix"></div>
        </div>
        <div class="dicribtion cl_1">
        <h2>View Membership <span class="pull-right"><i class="fa fa-arrow-right" aria-hidden="true"></i></span></h2>
        </div>
        </li>
        <li class="color_2">
        <div class="supplier_dri">
        <div class="logo_moduls"><i class="fa fa-tasks" aria-hidden="true"></i></div>
        <div class="logo_moduls_con pull-right">
        <h3><?php echo $cateone;?></h3>
        <h4>Categories  to Stay</h4>
        </div>
        <div class="clearfix"></div>
        </div>
        <div class="dicribtion cl_2">
        <h2>View Categories <span class="pull-right"><i class="fa fa-arrow-right" aria-hidden="true"></i></span></h2>
        </div>
        </li>
        <li class="color_3">
        <div class="supplier_dri">
        <div class="logo_moduls"><i class="fa fa-cog" aria-hidden="true"></i></div>
        <div class="logo_moduls_con pull-right">
        <h3><?php echo $service_provider;?> </h3>
        <h4>Service Provider</h4>
        </div>
        <div class="clearfix"></div>
        </div>
        <div class="dicribtion cl_3">
        <h2>View Service Provider  <span class="pull-right"><i class="fa fa-arrow-right" aria-hidden="true"></i></span></h2>
        </div>
        </li>
         <li class="color_3">
        <div class="supplier_dri">
        <div class="logo_moduls"> <i class="fa fa-bookmark" aria-hidden="true"></i> </div>
        <div class="logo_moduls_con pull-right">
        <h3><?php echo $booking;?></h3>
        <h4>Booking to Stay</h4>
        </div>
        <div class="clearfix"></div>
        </div>
        <div class="dicribtion cl_3">
        <h2>View Booking  <span class="pull-right"><i class="fa fa-arrow-right" aria-hidden="true"></i></span></h2>
        </div>
        </li>
         
        
      </div>
      <div class="clearfix"></div>
    </ul>
   </div>

     <?php include('footer.php'); ?>
