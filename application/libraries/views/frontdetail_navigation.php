<?php include('headerandsidebar.php'); 
error_reporting(0);
?>
<script>
    $(function(){
        $("#select_design").change(function()
        {
            var product_code = $("#select_design").val();
          window.location.href = "<?php echo site_url('Welcome/frontdetail_navigation/');?>"+product_code;
				 
        });
		 
    });
</script>
<?php
foreach ($footer_setting as $data){

}
?>
<div class="row">
  <div class="container" id="content">
    <style type="text/css">
    .modal .modal-body {
    max-height: 450px;
    overflow-y: auto;
    }
    </style>
    <h3 class="margin-top-0">Front Detail Navigation</h3>
    <?php //echo '===='.$header_setting[0]['id'];?>
            <!--<form method="post" action="<?php  //echo site_url('Welcome/header_navigation');  ?>" name="data_register">-->
            <form method="post" action="<?php if(!empty($footer_setting)){ echo site_url('Welcome/update_frontdetail/').$footer_setting[0]['id']; } else{ echo site_url('Welcome/insert_frontdetail_navigation'); } ?>" name="data_register">
      <div class="panel panel-default">
        <ul class="nav nav-tabs nav-justified" role="tablist">
          <li class="active"><a href="#GENERAL" data-toggle="tab">General</a></li>
          <li class=""><a href="#HOTELS_TYPES" data-toggle="tab">Social Media Links</a></li>
        <!--   <li class=""><a href="#ROOMS_TYPES" data-toggle="tab">Rooms Types</a></li>
        <li class=""><a href="#ROOMS_AMENITIES" data-toggle="tab">Facilities</a></li> -->
          <!-- <li class=""><a href="#PAYMENT_METHODS" data-toggle="tab">Payment Methods</a></li> -->
          <!-- <li class=""><a href="#HOTELS_AMENITIES" data-toggle="tab">Hotels Amenities</a></li> -->
        </ul>
        <div class="panel-body"> <?php if($this->session->flashdata('insertmessage')){?>
				  <div class="alert alert-success" align="center">
				  <?php echo $this->session->flashdata('insertmessage'); ?>
					</div>
				  <?php
				  }elseif($this->session->flashdata('updatemessage')){
				  ?>
				  <div class="alert alert-success" align="center">
				  <?php echo $this->session->flashdata('updatemessage'); ?>
					</div>
				  <?php
				  }
				  ?>  <br>
          <div class="tab-content form-horizontal">
            <div class="tab-pane wow fadeIn animated active in" id="GENERAL">
              <div class="clearfix"></div>
              <hr>
              <div class="row form-group">
                <label  class="col-md-4 control-label text-left">Language:</label>
                <div class="col-md-4">
                 <select name="select_design" id="select_design" class="form-control" onchange="select_design()" >
                 <option value="">Select Language</option>
                 <option value="en"<?php if ($data['language'] === 'en') echo ' selected="selected"'?>>English</option>
                 <option value="ger"<?php if ($data['language'] === 'ger') echo ' selected="selected"'?>>Germany</option>
                 </select>                 
                </div>
                
              </div>
              <div class="clearfix"></div>
              <div class="row form-group">

              <label  class="col-md-2 control-label text-left">SHOW SITE CONTACT:</label>
                <div class="col-md-4">
                <input class="form-control" type="text" placeholder="" name="site_contact"  value="<?php foreach ($footer_setting as $data) { echo $data['site_contact'];}?>">
                </div>
                <label  class="col-md-2 control-label text-left">Location:</label>
                <div class="col-md-3">
                  <input class="form-control" type="text" placeholder="" name="location"  value="<?php foreach ($footer_setting as $data) { echo $data['location'];}?>">
                </div>                
              </div>

              <div class="clearfix"></div>
              <div class="row form-group">
                <label  class="col-md-2 control-label text-left">Contact Detail:</label>
                <div class="col-md-4">
                  <input class="form-control" type="text" placeholder="" name="contect_detail"  value="<?php foreach ($footer_setting as $data) { echo $data['contect_detail'];}?>">
                </div>
                 <label  class="col-md-2 control-label text-left">Social Media:</label>
                <div class="col-md-3">
                  <input class="form-control" type="text" placeholder="" name="social_media"  value="<?php foreach ($footer_setting as $data) { echo $data['social_media'];}?>">
                </div>                  
              </div>

              <div class="clearfix"></div>
              <div class="row form-group">
                <label class="col-md-2 control-label text-left">Request Free:</label>
                <div class="col-md-4">
                 <input class="form-control" type="text" name="request_free" value="<?php foreach ($footer_setting as $data) { echo $data['request_free'];}?>" required>
                </div>
                <label class="col-md-2 control-label text-left">facilities:<sup>*</sup></label>
                <div class="col-md-3"><input class="form-control" type="text" placeholder="" name="facilities"  value="<?php foreach ($footer_setting as $data){ echo $data['facilities'];}?>"></div>
              </div>
              
              <div class="row form-group">
                <label  class="col-md-2 control-label text-left">Downloads files PDF:<sup>*</sup></label>
                <div class="col-md-4">
                  <input class="form-control" type="text" placeholder="" name="download_file"  value="<?php foreach ($footer_setting as $data) { echo $data['download_file'];}?>" required>
                </div>
                <label  class="col-md-2 control-label text-left">More Location:</label>
                <div class="col-md-3">
                  <input class="form-control" type="text" placeholder="" name="more_location"  value="<?php foreach ($footer_setting as $data) { echo $data['more_location'];}?>">
                </div>
              </div>
              <div class="row form-group">
                <label  class="col-md-2 control-label text-left">Latest comments and Rate:<sup>*</sup></label>
                <div class="col-md-4">
                  <input class="form-control" type="text" placeholder="" name="latest_comments"  value="<?php foreach ($footer_setting as $data) { echo $data['latest_comments'];}?>" required>
                </div>
                <label  class="col-md-2 control-label text-left">Description:</label>
                <div class="col-md-3">
                  <input class="form-control" type="text" placeholder="" name="discription"  value="<?php foreach ($footer_setting as $data) { echo $data['discription'];}?>">
                </div>
              </div>
			  <div class="row form-group">
                <label  class="col-md-2 control-label text-left">Location at a glance:<sup>*</sup></label>
                <div class="col-md-4">
                  <input class="form-control" type="text" placeholder="" name="location_at_glance"  value="<?php foreach ($footer_setting as $data) { echo $data['location_at_glance'];}?>" required>
                </div>
				 <label  class="col-md-2 control-label text-left">share:</label>
                <div class="col-md-3">
                  <input class="form-control" type="text" placeholder="" name="share"  value="<?php foreach ($footer_setting as $data) { echo $data['share'];}?>">
                </div>
            </div>
			<div class="row form-group">
                <label  class="col-md-2 control-label text-left">Views:<sup>*</sup></label>
                <div class="col-md-4">
                  <input class="form-control" type="text" placeholder="" name="views"  value="<?php foreach ($footer_setting as $data) { echo $data['views'];}?>" required>
                </div>
				 <label  class="col-md-2 control-label text-left">Price from on request:</label>
                <div class="col-md-3">
                  <input class="form-control" type="text" placeholder="" name="price_from_request"  value="<?php foreach ($footer_setting as $data) { echo $data['price_from_request'];}?>">
                </div>
            </div>
			<div class="row form-group">
                <label  class="col-md-2 control-label text-left">comment:<sup>*</sup></label>
                <div class="col-md-4">
                  <input class="form-control" type="text" placeholder="" name="comment"  value="<?php foreach ($footer_setting as $data) { echo $data['comment'];}?>" required>
                </div>
                 <label  class="col-md-2 control-label text-left">Free quote:</label>
                <div class="col-md-3">
                  <input class="form-control" type="text" placeholder="" name="freequote"  value="<?php foreach ($footer_setting as $data) { echo $data['freequote'];}?>">
                </div>
            </div>
            <div class="row form-group">
                <label  class="col-md-2 control-label text-left">firm:<sup>*</sup></label>
                <div class="col-md-4">
                  <input class="form-control" type="text" placeholder="" name="firm"  value="<?php foreach ($footer_setting as $data) { echo $data['firm'];}?>" required>
                </div>
                 <label  class="col-md-2 control-label text-left">telephone:</label>
                <div class="col-md-3">
                  <input class="form-control" type="text" placeholder="" name="telephone"  value="<?php foreach ($footer_setting as $data) { echo $data['telephone'];}?>">
                </div>
            </div>
            <div class="row form-group">
                <label  class="col-md-2 control-label text-left">Occasion:<sup>*</sup></label>
                <div class="col-md-4">
                  <input class="form-control" type="text" placeholder="" name="occasion"  value="<?php foreach ($footer_setting as $data) { echo $data['occasion'];}?>" required>
                </div>
                 <label  class="col-md-2 control-label text-left">Datet/time:</label>
                <div class="col-md-3">
                  <input class="form-control" type="text" placeholder="" name="datetime"  value="<?php foreach ($footer_setting as $data) { echo $data['datetime'];}?>">
                </div>
            </div>
            <div class="row form-group">
                <label  class="col-md-2 control-label text-left">Guests:<sup>*</sup></label>
                <div class="col-md-4">
                  <input class="form-control" type="text" placeholder="" name="guests"  value="<?php foreach ($footer_setting as $data) { echo $data['guests'];}?>" required>
                </div>
                 <label  class="col-md-2 control-label text-left">Budget:</label>
                <div class="col-md-3">
                  <input class="form-control" type="text" placeholder="" name="budget"  value="<?php foreach ($footer_setting as $data) { echo $data['budget'];}?>">
                </div>
            </div>
            <div class="row form-group">
                <label  class="col-md-2 control-label text-left">Event Gesamt:<sup>*</sup></label>
                <div class="col-md-4">
                  <input class="form-control" type="text" placeholder="" name="eventgesamt"  value="<?php foreach ($footer_setting as $data) { echo $data['eventgesamt'];}?>" required>
                </div>
                 <label  class="col-md-2 control-label text-left">detailspop:</label>
                <div class="col-md-3">
                  <input class="form-control" type="text" placeholder="" name="detailspop"  value="<?php foreach ($footer_setting as $data) { echo $data['detailspop'];}?>">
                </div>
            </div>
            <div class="row form-group">
                <label  class="col-md-2 control-label text-left">Beat me more appropriate provider Before:<sup>*</sup></label>
                <div class="col-md-4">
                  <input class="form-control" type="text" placeholder="" name="appropriate"  value="<?php foreach ($footer_setting as $data) { echo $data['appropriate'];}?>" required>
                </div>
                 <label  class="col-md-2 control-label text-left">I want to recive exciting news:</label>
                <div class="col-md-3">
                  <input class="form-control" type="text" placeholder="" name="news"  value="<?php foreach ($footer_setting as $data) { echo $data['news'];}?>">
                </div>
            </div>
            <div class="row form-group">
                <label  class="col-md-2 control-label text-left">Send:<sup>*</sup></label>
                <div class="col-md-4">
                  <input class="form-control" type="text" placeholder="" name="send"  value="<?php foreach ($footer_setting as $data) { echo $data['send'];}?>" required>
                </div>
                 <label  class="col-md-2 control-label text-left">Name:</label>
                <div class="col-md-3">
                  <input class="form-control" type="text" placeholder="" name="name"  value="<?php foreach ($footer_setting as $data) { echo $data['name'];}?>">
                </div>
            </div>
            <div class="row form-group">
                <label  class="col-md-2 control-label text-left">Email:<sup>*</sup></label>
                <div class="col-md-4">
                  <input class="form-control" type="text" placeholder="" name="email"  value="<?php foreach ($footer_setting as $data) { echo $data['email'];}?>" required>
                </div>
                <label  class="col-md-2 control-label text-left">Select:</label>
                <div class="col-md-3">
                  <input class="form-control" type="text" placeholder="" name="select1"  value="<?php foreach ($footer_setting as $data) { echo $data['select1'];}?>">
                </div>
            </div>
            <div class="row form-group">
                <label  class="col-md-2 control-label text-left">Your Name:<sup>*</sup></label>
                <div class="col-md-4">
                  <input class="form-control" type="text" placeholder="" name="yourname"  value="<?php foreach ($footer_setting as $data) { echo $data['yourname'];}?>" required>
                </div>
                <label  class="col-md-2 control-label text-left">Friend Email:</label>
                <div class="col-md-3">
                  <input class="form-control" type="text" placeholder="" name="friendsemail"  value="<?php foreach ($footer_setting as $data) { echo $data['friendsemail'];}?>">
                </div>
            </div>
            <div class="row form-group">
                <label  class="col-md-2 control-label text-left">Embed<sup>*</sup></label>
                <div class="col-md-4">
                  <input class="form-control" type="text" placeholder="" name="embeded"  value="<?php foreach ($footer_setting as $data) { echo $data['embeded'];}?>" required>
                </div>
                <label  class="col-md-2 control-label text-left">Confirm:</label>
                <div class="col-md-3">
                  <input class="form-control" type="text" placeholder="" name="confirm"  value="<?php foreach ($footer_setting as $data) { echo $data['confirm'];}?>">
                </div>
            </div>
            <div class="row form-group">
                <label  class="col-md-2 control-label text-left">Send To A Friend:<sup>*</sup></label>
                <div class="col-md-4">
                  <input class="form-control" type="text" placeholder="" name="sendtofriend"  value="<?php foreach ($footer_setting as $data) { echo $data['sendtofriend'];}?>" required>
                </div>
                <label  class="col-md-2 control-label text-left">url:</label>
                <div class="col-md-3">
                  <input class="form-control" type="text" placeholder="" name="urll"  value="<?php foreach ($footer_setting as $data) { echo $data['urll'];}?>">
                </div>
            </div>
			<div class="row form-group">
                <label  class="col-md-2 control-label text-left">Rooms:<sup>*</sup></label>
                <div class="col-md-4">
                  <input class="form-control" type="text" placeholder="" name="room"  value="<?php foreach ($footer_setting as $data) { echo $data['room'];}?>" required>
                </div>
                
            </div>
        </div>
      </div>
      <div class="panel-footer">
        <input type="hidden" name="updatesettings" value="1" />
        <input type="hidden" name="updatefor" value="hotels" />
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
    </div>
  </form>
          
       
     
    </div>
  </div>
</div>
<!----edit modal--->
<script>
$(document).ready(function(){
if(window.location.hash != "") {
$('a[href="' + window.location.hash + '"]').click() } });
</script>
</div>
</div>
</div>
<?php include('footer.php'); ?>