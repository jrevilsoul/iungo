<!DOCTYPE html><head>
    <?php include('headerandsidebar.php'); ?>
    <div class="row">
        <div class="container" id="content">
            <div class="panel panel-default">
                <div class="panel-heading">Social Media Urls :</div>
                <div class="xcrud-list-container">
				<?php if($this->session->flashdata('insertmessage')){?>
				  <div class="alert alert-success" align="center">
				  <?php echo $this->session->flashdata('insertmessage'); ?>
					</div>
				  <?php
				  }elseif($this->session->flashdata('updatemessage')){
				  ?>
				  <div class="alert alert-success" align="center">
				  <?php echo $this->session->flashdata('updatemessage'); ?>
					</div>
				  <?php
				  }
				  ?>

               <form method="post" action="<?php if(!empty($website_social_media)){ echo site_url('Welcome/update_website_social_media'); } else{ echo site_url('Welcome/insert_website_social_media'); } ?>" name="data_register">
               
               
               <div class="col-md-12 col-sm-12 col-xs-12 form_admin_page">
                     
                     <div class="row form-group">
                <label class="col-md-2 control-label text-left">Facebook:</label>
                <div class="col-md-10">
                <input type="text" class="form-control" name="facebook" value="<?php foreach ($website_social_media as $data) { echo $data['facebook'];} ?>">
                </div>             
              </div>
              <div class="row form-group">
                <label class="col-md-2 control-label text-left">Twitter:</label>
                <div class="col-md-10">
                <input type="text" class="form-control" name="twitter" value="<?php foreach ($website_social_media as $data) { echo $data['twitter'];} ?>">
                </div>             
              </div>
              <div class="row form-group">
                <label class="col-md-2 control-label text-left">Googleplus:</label>
                <div class="col-md-10">
                <input type="text" class="form-control" name="googleplus" value="<?php foreach ($website_social_media as $data) { echo $data['googleplus']; }?>">
                </div>             
              </div>
              <div class="row form-group">
                <label class="col-md-2 control-label text-left">Pinterest:</label>
                <div class="col-md-10">
              <input type="text" class="form-control" name="pinterest" value="<?php foreach ($website_social_media as $data) { echo $data['pinterest']; } ?>">
                </div>             
              </div>
             
                   <div class="panel-footer">
        <input type="hidden" name="updatesettings" value="1" />
        <input type="hidden" name="updatefor" value="hotels" />
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
                    </form>
                    <?php // } ?>
                  </div>
                </div>
               <?php include('footer.php'); ?>
              </body>
             </html>