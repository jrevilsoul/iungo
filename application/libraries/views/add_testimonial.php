<?php include('headerandsidebar.php'); ?>
  
  <div class="output"></div>
  <div class="panel panel-default">
   <div class="panel-heading">Add Testimonial </div>
  <div class="panel-body"> 
  <?php if($this->session->flashdata('message')){?>
  <div class="alert alert-success" align="center">
  <?php echo $this->session->flashdata('message'); ?>
	</div>
  <?php
  }
  ?>
	
	<br>
      <div class="tab-content form-horizontal">
      <div class="tab-pane wow fadeIn animated active in" id="GENERAL" >
      <form method="POST" action="<?php echo site_url('Welcome/insert_testimonial'); ?>" enctype="multipart/form-data">
      <div class="clearfix"></div>
       <div class="col-md-12 col-sm-12 col-xs-12 form_admin_page">
      <div class="row form-group">
          <label class="col-md-2 control-label text-left">Status</label>
          <div class="col-md-2">
          <select class="form-control" name="status" id="status">
              <option value="1">Enabled</option>
              <option value="0">Disabled</option>
            </select>
        </div>
        </div>
      <div class="row form-group">
          <label class="col-md-2 control-label text-left">Name</label>
          <div class="col-md-10">
          <input type="text"  class="form-control" placeholder="Name" name="name" required="">
        </div>
        </div>
      <div class="row form-group">
          <label class="col-md-2 control-label text-left">Description</label>
          <div class="col-md-10">
          <input type="text"  class="form-control" placeholder="Description" name="des" required="">
        </div>
        </div>
      <div class="row form-group">
          <label class="col-md-2 control-label text-left">Designation</label>
          <div class="col-md-10">
          <input type="text"  class="form-control" placeholder="Designation" name="designation" required="">
        </div>
        </div>
      <div class="row form-group">
          <label class="col-md-2 control-label text-left">Image</label>
          <div class="col-md-10">
          <input type="file" name="pic" />
        </div>
        </div>
      <div class="panel-footer">
         
          <input class="btn btn-primary" type="submit" value="Submit">
        </div>
        </div>
      </div>
      </div>
    </form>
    </div>
</div>
  <?php include('footer.php'); ?>
  </body>
  </html>
