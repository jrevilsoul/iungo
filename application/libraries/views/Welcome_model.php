<?php
class Welcome_model extends CI_Model
{
public function __construct()
{
parent::__construct();
}

function login($username, $password)
{
$this -> db -> select('id, username, password');
$this -> db -> from('users');
$this -> db -> where('username', $username);
$this -> db -> where('password', MD5($password));
$this -> db -> limit(1);
$query = $this -> db -> get();

if($query -> num_rows() == 1)
{
return $query->result();
}

else
{
return false;
}
}

/* Start Provider Model*/
public function insert_sprovider($data){
$this->db->insert('tbl_serviceProviderType', $data);
return TRUE;
}

public function insert_membership($data){
$this->db->insert('add_membership', $data);
return TRUE;
}
public function view_membership(){
$query=$this->db->query("SELECT * FROM add_membership");
return $query->result_array();
}

public function edit_membership($id){
$query=$this->db->query("SELECT * FROM add_membership WHERE meb_id = $id");
return $query->result_array();
}

public function insert_comment($data){
$this->db->insert('user_comment', $data);
return TRUE;
}
public function view_sprovider(){
$query=$this->db->query("SELECT * FROM tbl_serviceprovidertype");
return $query->result_array();
}
public function view_sprovider1(){
$query=$this->db->query("SELECT * FROM service_provider");
return $query->result_array();
}

public function view_sprovider2(){
$query=$this->db->query("SELECT * FROM service_provider order by id ASC limit 0,8");
return $query->result_array();
}



public function image(){
$query=$this->db->query("SELECT * FROM service_provider order by id desc limit 0,8");
return $query->result_array();
}

public function edit_sprovider($id){
$query=$this->db->query("SELECT * FROM tbl_serviceprovidertype WHERE id = $id");
return $query->result_array();
}
/* End Provider Model */

/* Start Country Model */
public function insert_country($data){
$this->db->insert('tbl_country', $data);
return TRUE;
}

public function view_country(){
$query=$this->db->query("SELECT * FROM tbl_country");
return $query->result_array();
}

public function edit_country($id){
$query=$this->db->query("SELECT * FROM tbl_country WHERE id = $id");
return $query->result_array();
}
/* End Country Model */

/* Start State Model */

public function insert_state($data){
$this->db->insert('tbl_state', $data);
return TRUE;
}

public function view_state(){
$query = $this->db->query("SELECT `tbl_state`.`stateName`,`tbl_state`.`status`,`tbl_state`.`id`, `tbl_country`.`country` FROM tbl_state INNER JOIN tbl_country ON `tbl_state`.`CountryID` = `tbl_country`.`id`");
return $query->result_array();
}

public function view_statewithcountry($country_id){
$query = $this->db->query("SELECT `stateName` FROM `tbl_state` where countryID='$country_id' ");
return $query->result_array();
}

public function edit_state($id){
$query=$this->db->query("SELECT `tbl_state`.`stateName` , `tbl_state`.`status` , `tbl_state`.`id` , `tbl_country`.`country` FROM tbl_state INNER JOIN tbl_country ON `tbl_state`.`CountryID` = `tbl_country`.`id` WHERE `tbl_state`.`id` = $id");
return $query->result_array();
}
/* End State Model */

/* Start City Model */
public function insert_city($data){
$this->db->insert('tbl_city', $data);
return TRUE;
}

public function view_city()
{
$query=$this->db->query("SELECT * FROM `tbl_city` AS `c` LEFT JOIN `tbl_state` AS `b` ON `c`.`stateID` = `b`.`id` LEFT JOIN `tbl_country` AS `a` ON `b`.`countryID` = `a`.`id`");
return $query->result_array();
}

public function edit_city($cityid){
$query=$this->db->query("SELECT * FROM `tbl_city` AS `c` LEFT JOIN `tbl_state` AS `b`
ON `c`.`stateID` = `b`.`id` LEFT JOIN `tbl_country` AS `a` ON `b`.`countryID` = `a`.`id` 
WHERE `c`.`cityid` ='$cityid'");
return $query->result_array();
}

/* End City Model */

function getCountry(){
$this->db->select('id,country');
$this->db->from('tbl_country');
$this->db->order_by('id', 'asc');
$query=$this->db->get();
return $query;
}

function getData($loadType,$loadId){
if($loadType=="state"){
$fieldList='id,stateName as name';
$table='tbl_state';
$fieldName='countryID';
$orderByField='id';
}

/* else{
$fieldList='id,cityName as name';
$table='tbl_city';
$fieldName='stateID';
$orderByField='cityName';
} */

$this->db->select($fieldList);
$this->db->from($table);
$this->db->where($fieldName, $loadId);
$this->db->order_by($orderByField, 'asc');
$query=$this->db->get();
return $query;
}

/* Start Catone Model */
public function insert_catone($data){
$this->db->insert('tbl_catone', $data);
return TRUE;
}

public function view_catone(){
$query=$this->db->query("SELECT * FROM tbl_catone");
return $query->result_array();
}

public function edit_catone($id){
$query=$this->db->query("SELECT * FROM tbl_catone WHERE id = $id");
return $query->result_array();
}
/* End Catone Model */

/* Start Cattwo Model */
public function insert_cattwo($data){
$this->db->insert('tbl_cattwo', $data);
return TRUE;
}

public function view_cattwo(){
$query = $this->db->query("SELECT * FROM `tbl_cattwo` INNER JOIN `tbl_catone` ON `tbl_cattwo`.`catone_ID` = `tbl_catone`.`id`");
return $query->result_array();
}

public function edit_cattwo($id){
$query=$this->db->query("SELECT `tbl_cattwo`.`cattwoname`,`tbl_cattwo`.`catone_Id` , `tbl_cattwo`.`status` , `tbl_cattwo`.`cattwo_id` , `tbl_catone`.`catonename` FROM `tbl_cattwo` INNER JOIN `tbl_catone` ON `tbl_cattwo`.`catone_Id` = `tbl_catone`.`id` WHERE `tbl_cattwo`.`cattwo_id` = $id");
return $query->result_array();
}

/* End Cattwo Model */

/* Start CMS_ABOUTUS Model */
public function view_aboutus(){
$query=$this->db->query("SELECT * FROM cms_aboutus where id!=''");
return $query->result_array();
}

public function insert_aboutus($data)
{
$this->db->insert('cms_aboutus', $data);
return TRUE;
}
/* End CMS_ABOUTUS Model */

/* Start CMS_Services Model */

			public function view_services(){
			$query=$this->db->query("SELECT * FROM cms_services where id!=''");
			return $query->result_array();
			}

			public function insert_services($data)
			{
			$this->db->insert('cms_services', $data);
			return TRUE;
			}
		/* End CMS_ABOUTUS Model */

		/* Start CMS_CONTACTUS Model */
		public function view_contactus(){
		$query=$this->db->query("SELECT * FROM cms_contactus where id!=''");
		return $query->result_array();
		}

		public function insert_contactus($data)
		{
		$this->db->insert('cms_contactus',$data);
		return TRUE;
		}
		/* End CMS_CONTACTUS Model */

		/* Start CMS_privacypolicy Model */
			public function view_privacypolicy(){
			$query=$this->db->query("SELECT * FROM cms_privacypolicy where id!=''");
			return $query->result_array();
			}

			public function insert_privacypolicy($data)
			{
			$this->db->insert('cms_privacypolicy', $data);
			return TRUE;
			}
		/* End CMS_privacypolicy Model */

		/* Start CMS_tnc Model */
			public function view_tnc(){
			$query=$this->db->query("SELECT * FROM cms_tnc where id!=''");
			return $query->result_array();
			}

			public function insert_tnc($data)
			{
			$this->db->insert('cms_tnc', $data);
			return TRUE;
			}
		/* End CMS_privacypolicy Model */
       /* Start CMS_Faq Model */
			public function view_faq(){
			$query=$this->db->query("SELECT * FROM cms_faq");
			return $query->result_array();
			}

			public function insert_cms_faq($data)
			{
			$this->db->insert('cms_faq', $data);
			return TRUE;
			}
			/* End CMS_Faq Model */

			/* Start CMS_Disclaimer Model */
 
             public function view_disclaimer(){
			$query=$this->db->query("SELECT * FROM cms_disclaimer where id!=''");
			return $query->result_array();
			}

			public function insert_disclaimer($data)
			{
			$this->db->insert('cms_disclaimer', $data);
			return TRUE;
			}
        /* End CMS_Disclaimer Model */

			/* Start CMS_Cancellation Model */
 
             public function view_cancellation(){
			$query=$this->db->query("SELECT * FROM cms_cancellation where id!=''");
			return $query->result_array();
			}

			public function insert_cancellation($data)
			{
			$this->db->insert('cms_cancellation', $data);
			return TRUE;
			}
        /* End CMS_Cancellation Model */
        /* Start CMS_Career Model */
 
             public function view_career(){
			$query=$this->db->query("SELECT * FROM cms_career where id!=''");
			return $query->result_array();
			}

			public function insert_career($data)
			{

			$this->db->insert('cms_career', $data);

			return TRUE;
			}
        /* End CMS_Career Model */

           /*     * ************************** CMS_dataprotection start ************************** */

    public function cms_dataprotection() {
        $this->data['dataprotection'] = $this->view_sprovider1->view_dataprotection();
        $this->load->view('cms_dataprotection', $this->data);
    }

    public function insert_cms_dataprotection() {
        $data = array(
            'content_description' => $this->input->post('content_description'),
            'content_TitleMeta' => $this->input->post('content_TitleMeta'),
            'content_KeywordMeta' => $this->input->post('content_KeywordMeta'),
            'content_DescriptionMeta' => $this->input->post('content_DescriptionMeta'));
        $insertand = $this->welcome->insert_dataprotection($data);
        redirect('welcome/cms_dataprotection');
    }

    public function update_cms_dataprotection($id) {
        $data = array('content_description' => $this->input->post('content_description'),
            'content_TitleMeta' => $this->input->post('content_TitleMeta'),
            'content_KeywordMeta' => $this->input->post('content_KeywordMeta'),
            'content_DescriptionMeta' => $this->input->post('content_DescriptionMeta'));
        $this->db->where('id!=', $id);
        $this->db->update('cms_dataprotection', $data);
        redirect('welcome/cms_dataprotection');
    }

    /*     * ******************** Dataprotection End ************************** */ 

             public function view_dataprotection(){
			$query=$this->db->query("SELECT * FROM cms_dataprotection where id!=''");
			return $query->result_array();
			}

			public function insert_dataprotection($data)
			{

			$this->db->insert('cms_dataprotection', $data);

			return TRUE;
			}
        /* End CMS_Career Model */

		/* Start General Setting  */
			public function view_settings(){
			$query=$this->db->query("SELECT * FROM site_settings where id!=''");
			return $query->result_array();
			}

			public function insert_settings($data)
			{
			$this->db->insert('site_settings', $data);
			return TRUE;
			}

		/* End General Setting */
		/* Start ProviderDetail Model */
		/* public function view_socialmedia(){
			$query=$this->db->query("SELECT * FROM tbl_socialmedia where id !=''");
			return $query->result_array();
			} */

		public function insert_socialmedia($data)
		{
		$this->db->insert('tbl_socialmedia',$data);
		return TRUE;
		}

		public function insert_roomtype($data)
		{
		$this->db->insert('tbl_room',$data);
		return TRUE;
		}

		public function view_facility(){
		$query=$this->db->query("SELECT * FROM tbl_facilities");
		return $query->result_array();
		}

		public function insert_facility($data)
		{
		$this->db->insert('tbl_facilities',$data);
		return TRUE;
		}
		
		public function view_providerdetails(){
		$query=$this->db->query("SELECT * FROM service_provider");
		return $query->result_array();
		}
		
		public function manage_users(){
		$query=$this->db->query("SELECT * FROM user");
		return $query->result_array();
		}
		
		public function manage_vender(){
		$query=$this->db->query("SELECT * FROM vendors");
		return $query->result_array();
		}

		public function edit_providerdetails($id){
		$query=$this->db->query("SELECT * FROM service_provider WHERE id=$id");
		return $query->result_array();
		}
		
		public function edit_users($id){
		$query=$this->db->query("SELECT * FROM user WHERE id = $id");
		return $query->result_array();
		}
		
		public function edit_vender($id){
		$query=$this->db->query("SELECT * FROM vendors WHERE id = $id");
		return $query->result_array();
		}

		public function insert_providerdetails($data)
		{
		$this->db->insert('service_provider',$data);
		return TRUE;
		}

	/* End ProviderDetail Model */

		public function view_gallery(){
		$query=$this->db->query("SELECT * FROM files");
		return $query->result_array();
		}

		public function view_roomtype(){
		$query=$this->db->query("SELECT * FROM tbl_room");
		return $query->result_array();
		}
		
		public function edit_roomtype($id){
		$query=$this->db->query("SELECT * FROM tbl_room WHERE id=$id");
		return $query->result_array();
		}

		public function view_emailsettings()
		{
		$query=$this->db->query("SELECT * FROM `email_settings` where id!=''");
		return $query->result_array();
		}

		public function insert_emailsettings($data)
		{
		$this->db->insert('email_settings',$data);
		return TRUE;
		}

		public function view_website_social_media()
		{
		$query=$this->db->query("SELECT * FROM `site_socialmedia` where id!=''");
		return $query->result_array();
		}

		public function insert_website_social_media($data)
		{
		$this->db->insert('site_socialmedia',$data);
		return TRUE;
		}

		public function view_website_flash_banner()
		{
		$query=$this->db->query("SELECT * FROM `site_flash_banner` where id!=''");
		return $query->result_array();
		}

		public function insert_website_flash_banner($data)
		{
		$this->db->insert('site_flash_banner', $data);
		return TRUE;
		}

		public function view_site_payments()
		{
		$query=$this->db->query("SELECT * FROM `site_payments` where id!=''");
		return $query->result_array();
		}

		public function view_users()
		{
		$query=$this->db->query("SELECT * FROM `users`");
		return $query->result_array();
		}

		public function insert_users($data)
		{
		$this->db->insert('users', $data);
		return TRUE;
		}

		public function view_website_comments_reviews()
		{
		$query=$this->db->query("SELECT * FROM `site_ratings`");
		return $query->result_array();
		}

		public function insert_website_comments_reviews()
		{
		$this->db->insert('site_ratings', $data);
		return TRUE;
		}

		public function view_voucher()
		{
		$query=$this->db->query("SELECT * FROM `site_voucher`");
		return $query->result_array();
		}
		
		public function insert_voucher($data)
		{
		$this->db->insert('site_voucher', $data);
		return TRUE;
		}

		public function insert_sms($data)
		{
		$this->db->insert('site_smsapi', $data);
		return TRUE;
		}
	
		public function insert_quote($data)
		{
		$this->db->insert('site_quote', $data);
		return TRUE;
		}
		
	    public function insert_advertisement($data)
		{
		 
		 $this->db->insert('add_advertisement', $data);
		return TRUE;
		}
		   
		public function edit_advertisment($id)
		{
		 $query=$this->db->query("SELECT * FROM add_advertisement where id ='$id'");
		 	return $query->result_array();
		
		}
		
		public function view_advertisement(){
		$query=$this->db->query("SELECT * FROM add_advertisement");
		return $query->result_array();
		}
		
		public function insert_philos($data)
		{
		$this->db->insert('philosophy', $data);
		return TRUE;
		}
		
		public function view_philos(){
		$query=$this->db->query("SELECT * FROM philosophy");
		return $query->result_array();
		}
		
		public function manage_review(){
		$query=$this->db->query("SELECT * FROM review");
		return $query->result_array();
		}
		
	    public function manage_enquiry(){
		$query=$this->db->query("SELECT * FROM enquiery");
		return $query->result_array();
		}
		
		public function insert_enquiry($data)
		{
		 $this->db->insert('enquiery', $data);
		return TRUE;
		}
		
		 public function manage_booking(){
		$query=$this->db->query("SELECT * FROM tbl_booking");
		return $query->result_array();
		}
		
	    public function manage_testimonial(){
		$query=$this->db->query("SELECT * FROM testimonial");
		return $query->result_array();
		}
		
		public function insert_testimonial($data)
		{
		 $this->db->insert('testimonial', $data);
		return TRUE;
		}
		
		public function edit_testimonial($id)
		{
		 $query = $this->db->query("SELECT * FROM testimonial where id ='$id'");
		 	return $query->result_array();
		}

		
		  /* public function search_results() {
				$query = $this->db->select('*');
                  		 $this->db->from('service_provider');
                  		 $this->db->join('tbl_serviceProviderType','service_provider.provider_type = tbl_serviceProviderType.$providertype','left');
                 		 $this->db->where('service_provider', $data);
							return $query->result_array();
					$query = $this->db->get();
                  	$data = $query->result_array();

						  $match = $this->input->post('third_search');
						  $match = $this->input->post('first_search');
						  $this->db->like('provider_name',$match);
						  $this->db->or_like('zipcode', $match);
						  $query = $this->db->get('service_provider');
						  return $query->result();
						}*/
			//}
}
?>