<!DOCTYPE html><head>
  <?php include('headerandsidebar.php');  ?>
  <div class="row">
    <div class="container" id="content">
      <div class="panel panel-default">
        <div class="panel-heading">Edit Country</div>
        <form class="add_button" action="#" method="post">
          <!-- <button type="submit" class="btn btn-success">
          <i class="glyphicon glyphicon-plus-sign"></i>Booking Edit Delete </button> -->
        </form>
        <div class="panel-body">
          <div class="xcrud">
            <div class="xcrud-container">
              <div class="xcrud-ajax">
                <div class=" pull-right" style="float:right;">
                  <input type="hidden" class="xcrud-data" name="key" value="017bfe2df5d71e29f34946466c178b21e4491ace" />
                  <input type="hidden" class="xcrud-data" name="orderby" value="pt_cars.car_id" />
                  <input type="hidden" class="xcrud-data" name="order" value="desc" />
                  <input type="hidden" class="xcrud-data" name="start" value="0" />
                  <input type="hidden" class="xcrud-data" name="limit" value="50" />
                  <input type="hidden" class="xcrud-data" name="instance" value="786870d5a374d329252a31dbbc49e5d866817eb0" />
                  <input type="hidden" class="xcrud-data" name="task" value="list" />
                </div>
                <div class="xcrud-list-container">
                  <div id="container" class="container">
                    <div class="row">
                      <div class="col-md-6 col-md-offset-2">
                        <!-- <h2 class="text-center"></h2> -->
                        <?php
                        if($edit_data){
                        foreach ($edit_data as $data) {
                        ?>
                        <form method="post" action="<?php echo site_url('Welcome/update_country/'.$data['id'].''); ?>">
                          <label for="Provider">Status:</label><br />
                          <select name="status" class="form-control">
                            <option value="1"  <?php if($data['status'] == '1') {echo 'selected="selected"';} ?>>Enable
                            </option>
                            <option value="0" <?php if($data['status'] == '0') {echo 'selected="selected"';} ?>>Disable
                            </option>
                          </select>
                          
                          <label for="Provider">Country:</label><br />
                          <input type="text" name="country" value="<?php echo $data['country']; ?>" class="form-control" name="updatecountry" required><br /><br />
                          <button type="submit" class="btn btn-primary pull-right">Update</button>
                          <br />
                        </form>
                        <?php } }
                        else{ echo "Incorrect Country Name"; }
                        ?>
                        <br />
                        <br />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <?php include('footer.php'); ?>
            </body>
          </html>