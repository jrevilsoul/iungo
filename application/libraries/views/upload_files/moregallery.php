<!DOCTYPE html><head>

<?php $this->load->view('headerandsidebar'); ?>


<style>
ul.gallery {
    clear: both;
    float: left;
    width: 100%;
    margin-bottom: -10px;
    padding-left: 3px;
}
ul.gallery li {
  
    padding: 10px;
list-style:none;
}

ul.gallery li img{width: 100%; height: 184px;}

ul.gallery li p {
    color: #6c6c6c;
    letter-spacing: 1px;
    text-align: center;
    position: relative;
    margin: 5px 0px 0px 0px;
}
</style>

<div class="row">
      <div class="container" id="content">
          <style type="text/css">
    .modal .modal-body {
    max-height: 450px;
    overflow-y: auto;
    } 
</style>

 <div class="panel panel-default">
              <div class="panel-heading">Add Pictures to Provider Gallery</div>
              
              <div class="panel-body">
              <div class="tab-content form-horizontal">
              <div class="col-md-12 col-sm-12 col-xs-12 form_admin_page">
               <p><?php //echo $this->session->flashdata('statusMsg'); ?></p>
              <form enctype="multipart/form-data" action="" method="post">
                    <div class="col-lg-4 col-md-4 col-sm-3 col-sm-12">
                    
                    <div class="form-group" >
                    <label>Provider Name</label>
                    <input type="text" name="provider"  value="<?php echo $provider[0]->provider_name;?>">
                    </div>
                   
                    <div class="form-group">
                        <input class="form-control" type="submit" name="fileSubmit1" value=" MORE UPLOAD FILES"/>
                    </div>
                      <div class="form-group">
                        <label>Choose Files</label>
                        <input type="file"  name="userFiles[]" multiple />
                    </div>
                   </div>
                  
                  <div class="clearfix"></div>
                 </form>
                 </div>
                   <div class="col-md-12 col-sm-12 col-xs-12 form_admin_page">
                   <ul class="gallery">
                    <?php if(!empty($files)): foreach($files as $file): ?>
                    <li class="col-md-4 col-sm-4 col-xs-12">
                        <img src="<?php echo base_url('uploads/files/'.$file->file_name); ?>" alt="" >
                        <!--<p>Uploaded On <?php //echo date("j M Y",strtotime($file->created)); ?></p>-->
						<a href="<?php echo base_url('Upload_files/delete/').$file->id; ?>">
<button>Delete</button></a>
                    </li>
                    <?php endforeach; else: ?>
                    <p>File(s) not found.....</p>
                    <?php endif; ?>
                </ul>
                   </div>
              </div>
              </div>
             
              </div>
</div>
</div>

        <?php $this->load->view('footer'); ?>
  