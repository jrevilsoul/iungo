<!DOCTYPE html><head>

<?php $this->load->view('headerandsidebar'); ?>


<style>
ul.gallery {
    clear: both;
    float: left;
    width: 100%;
    margin-bottom: -10px;
    padding-left: 3px;
}
ul.gallery li {
  
    padding: 10px;
list-style:none;
}

ul.gallery li img{width: 100%; height: 184px;}

ul.gallery li p {
    color: #6c6c6c;
    letter-spacing: 1px;
    text-align: center;
    position: relative;
    margin: 5px 0px 0px 0px;
}
</style>

<div class="row">
      <div class="container" id="content">
          <style type="text/css">
    .modal .modal-body {
    max-height: 450px;
    overflow-y: auto;
    } 
</style>

 <div class="panel panel-default">
              <div class="panel-heading">Add Pictures to Provider Gallery</div>
              
              <div class="panel-body">
			    <?php if($this->session->flashdata('insertmessage')){?>
				  <div class="alert alert-success" align="center">
				  <?php echo $this->session->flashdata('insertmessage'); ?>
					</div>
				  <?php
				  }elseif($this->session->flashdata('updatemessage')){
				  ?>
				  <div class="alert alert-success" align="center">
				  <?php echo $this->session->flashdata('updatemessage'); ?>
					</div>
				  <?php
				  }
				  ?>
              <div class="tab-content form-horizontal">
              <div class="col-md-12 col-sm-12 col-xs-12 form_admin_page">
               <p><?php echo $this->session->flashdata('statusMsg'); ?></p>
              <form enctype="multipart/form-data" action="" method="post">
                    <div class="col-lg-4 col-md-4 col-sm-3 col-sm-12">
          
                    <div class="form-group">
                    <label>Select Provider Name</label>
                    <select  name="providerid" class="form-control" required>
                      <option value="">--SELECT PROVIDER NAME--</option>
                      <?php
					  foreach ($view_providerdetails as $nameprovider) { ?>
                      <option value="<?php echo $nameprovider['id'].','.$nameprovider['provider_name']; ?>">
                      <?php echo $nameprovider['provider_name']; ?></option>
                      <?php } ?>
                    </select>
                    </div>
                    <div class="form-group">
                        <label>Choose Files</label>
                        <input type="file"  name="userFiles[]" multiple />
                    </div>
                    <p>(You can select multiple images at a time)</p>
                    <div class="form-group">
                        <input class="form-control" type="submit" name="fileSubmit" value="UPLOAD FILES"/>
                    </div>
                   </div>
                  <div class="clearfix"></div>
                 </form>
                 </div>
                   <div class="col-md-12 col-sm-12 col-xs-12 form_admin_page">
                   <ul class="gallery">
                    <?php if(!empty($files)): foreach($files as $file): ?>
                    <li class="col-md-4 col-sm-4 col-xs-12">
                        <img src="<?php echo base_url('uploads/files/'.$file['file_name']); ?>" alt="" >
                        <p>Uploaded On <?php echo date("j M Y",strtotime($file['created'])); ?></p>
                    </li>
                    <?php endforeach; else: ?>
                    <p>File(s) not found.....</p>
                    <?php endif; ?>
                </ul>
                   </div>
              </div>
              </div>
              
              
              </div>
</div>
</div>

        <?php $this->load->view('footer'); ?>
  