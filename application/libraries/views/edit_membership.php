<!DOCTYPE html><head>
<?php include('headerandsidebar.php'); ?>
<div class="row">
  <div class="container" id="content">
    <div class="panel panel-default">
      <div class="panel-heading">Edit Membership</div>      
      <form class="add_button" action="#" method="post">
        <!-- <button type="submit" class="btn btn-success">
        <i class="glyphicon glyphicon-plus-sign"></i>Booking Edit Delete </button> -->
      </form>

            <div class="panel-body">   
              <div class="xcrud">
                <div class="xcrud-container">
                <div class="xcrud-ajax">
              <div class=" pull-right" style="float:right;">
               <input type="hidden" class="xcrud-data" name="key" 
               value="017bfe2df5d71e29f34946466c178b21e4491ace" />
                <input type="hidden" class="xcrud-data" name="orderby" value="pt_cars.car_id" />
                <input type="hidden" class="xcrud-data" name="order" value="desc" />
                <input type="hidden" class="xcrud-data" name="start" value="0" />
                <input type="hidden" class="xcrud-data" name="limit" value="50" />
              <input type="hidden" class="xcrud-data" name="instance" value="786870d5a374d329252a31dbbc49e5d866817eb0" />
                <input type="hidden" class="xcrud-data" name="task" value="list" />
                </div> 
                <div class="xcrud-list-container">
                 <div id="container" class="container">
                 <div class="row">
                 <div class="col-md-6 col-md-offset-2">
                 <h2 class="text-center">Edit Membership</h2>
                <br />
                <?php 
			     if($edit_data){
				 foreach($edit_data as $data){
			    ?>
                <form method="post" action="<?php echo site_url('Welcome/update_membership/').$data['meb_id']; ?>" name="data_register">
                <label for="Country">Status</label>
                <select class="form-control" name="selectplan" required="required" />
               <option value="Standard"<?php if($data['membership_plan'] == 'Standard') {echo 'selected="selected"';} ?>>Standard </option>
               <option value="Middle" <?php if($data['membership_plan'] == 'Middle') {echo 'selected="selected"';} ?>>Middle</option>
               <option value="Basic" <?php if($data['membership_plan'] == 'Basic') {echo 'selected="selected"';} ?>>Basic</option>
                
                </select>
                <br />
                 <label for="Country">Status</label>
                <select class="form-control" name="status" required="required" />
                   <option value="1" <?php if($data['status'] == 'Active') {echo 'selected="selected"';} ?>>Enable</option>
               <option value="0" <?php if($data['status'] == 'Inactive') {echo 'selected="selected"';} ?>>Disable</option>
                </select>
                <br />
				<label for="Country">Decription</label>
                <input type="text" align="center" class="form-control" name="description" required="required" value="<?php echo $data['description'];?>" />          
                <br/>
				<label for="Country">Product</label>
                <input type="text" align="center" class="form-control" name="product_name" required="required" value="<?php echo $data['product_name'];?>" />          
                <br />
				<label for="Country">Duration</label>
                <input type="text" align="center" class="form-control" name="duration" required="required" value="<?php echo $data['duration'];?>"/>          
                <br />
                <label for="Country">Price</label>
                <input type="text" align="center" class="form-control" name="price" required="required" value="<?php echo $data['price'];?>" />          
                <br />
                  <button type="submit" class="btn btn-primary pull-right">Submit</button>
                  </form>
                  <?php
				  }
				  }
				  ?>
                  </div>
                  </div>
                  </div>
                </div>
            </div>
    <?php include('footer.php'); ?>
</body>
</html>