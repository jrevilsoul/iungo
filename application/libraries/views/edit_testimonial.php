<?php include('headerandsidebar.php'); ?>
      <div class="output"></div>
      <div class="panel panel-default">
      <div class="panel-heading">Edit Testimonial</div>
      <div class="panel-body">
      <?php //print_r($view_data);?>
      <?php foreach($view_data as $data){
	}
	 ?>
      <br>
      <div class="tab-content form-horizontal">
      <form method="POST" action="<?php echo site_url('Welcome/update_testimonial/').$data['id']; ?>" enctype="multipart/form-data">
          <div class="clearfix"></div>
          <div class="col-md-12 col-sm-12 col-xs-12 form_admin_page">
          <div class="row form-group">
              <label class="col-md-2 control-label text-left">Status</label>
              <div class="col-md-2">
              <select class="form-control" name="status" id="status">
                  <option value='1' <?php if($data['status'] == '1') { echo "selected='selected'" ; } ?> >Enabled</option>
                  <option value='0' <?php if($data['status'] == '0') { echo "selected='selected'" ; } ?>>Disabled</option>
                </select>
            </div>
            </div>
          <div class="row form-group">
              <label class="col-md-2 control-label text-left">Name</label>
              <div class="col-md-10">
              <input type="text"  class="form-control" placeholder="Name" name="name" required="" value="<?php echo $data['name'];?>">
            </div>
            </div>
          <div class="row form-group">
              <label class="col-md-2 control-label text-left">Description</label>
              <div class="col-md-10">
              <input type="text"  class="form-control" placeholder="Description" name="des" required="" value="<?php echo $data['description'];?>">
            </div>
            </div>
          <div class="row form-group">
              <label class="col-md-2 control-label text-left">Designation</label>
              <div class="col-md-10">
              <input type="text"  class="form-control" placeholder="Designation" name="designation" required="" value="<?php echo $data['designation'];?>">
            </div>
            </div>
          <div class="row form-group">
              <label class="col-md-2 control-label text-left">Image</label>
              <div class="col-md-10">
              <input type="file" name="pic" />
              <img src="<?php  echo base_url('banners/' . $data['image']);?>" height="80" width="80"></div>
            </div>
          <div class="panel-footer">
              <!--<input type="hidden" id="roomid" name="roomid" value="" />
          <input type="hidden" name="oldquantity" value="" />
          <input type="hidden" name="submittype" value="add" /> -->
              <input class="btn btn-primary" type="submit" value="Submit">
            </div>
        </div>
          
          </div>
        </form>
    </div>
    </div>
</div>
  <?php include('footer.php'); ?>
  </body>
  </html>