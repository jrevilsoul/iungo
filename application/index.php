﻿<?php 
session_start();
error_reporting(0); ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Home</title>
        <link rel="stylesheet" type="text/css" href="https://fonts.google.com/specimen/Open+Sans" />
        <link href="<?php echo base_url('frontend_assets/'); ?>css/style.css" type="text/css" rel="stylesheet">
        <link href="<?php echo base_url('frontend_assets/'); ?>css/animate.css" type="text/css" rel="stylesheet">
        <link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_url('frontend_assets/'); ?>favicons/apple-touch-icon.png">
        <link rel="icon" type="image/png" href="<?php echo base_url('frontend_assets/'); ?>favicons/favicon-32x32.png" sizes="32x32">
        <link rel="icon" type="image/png" href="<?php echo base_url('frontend_assets/'); ?>favicons/favicon-16x16.png" sizes="16x16">
        <link rel="manifest" href="<?php echo base_url('frontend_assets/'); ?>favicons/manifest.json">
        <link rel="mask-icon" href="<?php echo base_url('frontend_assets/'); ?>favicons/safari-pinned-tab.svg" color="#5bbad5">
        <meta name="theme-color" content="#ffffff">
        <!--[if lt IE 9]>
              <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
              <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <style type="text/css">
        #boxes-list2::-webkit-scrollbar-track
        {
            -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
            border-radius: 3px;
            background-color: #fff;
        }

        #boxes-list2::-webkit-scrollbar
        {
            width: 10px;
            background-color: #fff !important;
        }

        #boxes-list2::-webkit-scrollbar-thumb
        {
            border-radius: 3px;
            -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,.3)!important;
            background-color: #2aa09c !important;
        }
    </style>
    <body id="boxes-list2" style="max-height:450px; overflow-x:hidden;">
        <!-- Modal -->
        <!--Header starts-->
        <div  class=" slide vedio-slide" >
            <div class="carousel-inner" role="listbox">
                <div class="item active">
                    <div align="center">
                        <video autoplay loop controls muted>
                            <source src="<?php echo base_url('frontend_assets/'); ?>images/7295584.mp4"  width="100%" height="300" type="video/mp4"> 
                        </video>
                    </div>    
                </div>
            </div>
        </div>

        <header>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 header-top">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-7 col-sm-7 col-xs-12">
                            <ul class="header-menu">
                                <?php
                                $sitesetting = "SELECT * FROM `site_settings`";
                                $queryfour = $this->db->query($sitesetting);
                                $qfour = $queryfour->result();
                                ?>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-envelope" aria-hidden="true"></i>&nbsp;
                                        <?php foreach ($qfour as $hello) { ?>
                                            <?php echo $hello->email; ?>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="" class="select_1">
                                            <i class="fa fa-phone" aria-hidden="true" style=""></i>&nbsp;Phone <?php echo $hello->mobile; ?>
                                        </a>
                                    </li>
                                <?php } ?> 
                            </ul>
                        </div>
                        <div class="col-lg-5 col-sm-5 col-xs-12 text-right for-mobile" style="padding-top: 4px;">
                            <a href="" style="color:#fff;">Did you need help or want to work with us? </a>                           
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 header-bottom">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-3 col-sm-3 col-xs-6 logo"> <a href=""><img src="<?php echo base_url('frontend_assets/'); ?>images/logo.png"></a>
                        </div>
                        <div class="col-md-9 col-sm-8 col-xs-6">
                            <a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a>
                            <div class="main-menu">
                                <div id="header_menu">
                                    <img src="<?php echo base_url('frontend_assets/'); ?>img/logo.png" width="190" height="23" alt="" data-retina="true">
                                </div>
                                <a href="#" class="open_close" id="close_in">
                                    <i class="icon_close"></i></a>
                                <ul>
                                    <li class="submenu"><a href="javascript:void(0);" class="show-submenu">WEDDING FINDER</a></li>
                                    <li><a href="">MARKETPLACE</a></li>
                                    <li><a href="" data-toggle="modal" data-target="#login_form">LOGIN</a></li>
                                    <li><a href="" data-toggle="modal" data-target="#vendor_login">VENDOR LOGIN</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-9 col-sm-9 col-xs-6 text-center menu-none">
                            <div class="navbar navbar-default pull-right" style="background: none; box-shadow: none; border: none;">
                                <div class="navbar-collapse collapse" style="padding: 0;">
                                    <ul class="nav navbar-nav logo-m">
                                        <li><a href="">WEDDING FINDER </a></li>
                                        <li><a href="">MARKETPLACE</a></li>
                                        <?php if (!empty($_SESSION['email'])): ?>
                                            <li><?php echo $_SESSION['email']; ?></li>
                                            <li><a href="<?php echo site_url('website/user_account'); ?>" data-target="">My Accounts</a></li>
                                            <li><a href="<?php echo site_url('website/user_logout'); ?>" data-target="">LOGOUT</a></li>
                                        <?php else: ?>
                                            <li><a href="#" data-toggle="modal"  data-target="#login_form">LOGIN</a></li>
                                            <li><a href="#" data-toggle="modal" data-target="#vendor_login">VENDOR LOGIN</a></li>
                                        <?php endif; ?>                
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <!--Header ends-->
        <div class="clearfix"></div>
        <div class="find-section col-md-12"><!-- Find search section-->
            <div class="mobile-hero-image show-for-small image-fill" style="">
                <img src="https://www.foreverly.de/addons/default/themes/merchant/img/homepage/mobile-hero-image.jpg" alt="homepage/mobile-hero-image.jpg" class="mobile-img" style="position: absolute; left: 0px;">
            </div>
            <div class="container">
                <div class="row">     
                    <div class="col-md-12 finder-block">
                        <div class="finder-caption">
                            <h1>Find your perfect Wedding</h1>
                            <p>Over <strong>1200+ Wedding </strong>for you special date &amp; Find the perfect venue &amp; save you date.</p>
                        </div>
                        <div class="finderform">         
                            <form action="<?php echo site_url('website/search_action'); ?>" method="post">
                                <div class="row">
                                    <div class="form-group col-md-3">
                                        <p class="text-left">Where?</p>
                                        <input type="text" name="first_search" class="form-control" placeholder="ZIP code" required="required">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <p class=" text-left">What?</p>
                                        <select class="form-control" name="second_search"/>
                                        <option value="">-- Select Service Type --</option>
                                        <?php foreach ($view_data as $datas): ?>
                                            <option value="<?php echo $datas['id']; ?>"><?php echo $datas['en_serviceTypeName']; ?></option>
                                        <?php endforeach; ?>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <p></p>
                                        <br />
                                        <input type="text" name="third_search" class="form-control" placeholder="Provider Name (Optional )">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <p></p>
                                        <br>
                                        <button type="submit" name="search" class="btn btn-primary btn-lg btn-block">Search Provider</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <!--=====================================================
                       MID CONTAINER SECTION START HERE
        -=============================================================-->
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 wry-section">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <div class="col-lg-3 col-sm-3 col-xs-12"><img src="<?php echo base_url('frontend_assets/'); ?>images/why.png" class="why-img"></div>
                        <div class="col-lg-9 col-sm-9 col-xs-12"><a href="" data-toggle="modal"  data-target="#login_form" class="btn pink-button btn-lg solid icon-btn" style="min-width:180px;">
                                <img src="<?php echo base_url('frontend_assets/'); ?>images/newwed.png">
                                Newlyweds
                            </a>
                            <span class="wry-span">WHO ARE YOU?</span>
                            <a href="" data-toggle="modal" data-target="#vendor_login" class="btn orange-button btn-lg solid icon-btn" style="min-width:180px;">
                                <img src="<?php echo base_url('frontend_assets/'); ?>images/service.png">
                                Service provider
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="clearfix"></div>

        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 about-back">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <h3 class="about-h3 blue-color animated bounceInUp" style="color:#0e0e0e;">FREE WEDDING PLANNING</h3>
                        <span class="figure-image animated bounceInRight"><img src="<?php echo base_url('frontend_assets/'); ?>images/testi-brdr.png"></span> </div>
                    <div class="col-md-4"><div class="list-item">
                            <div class="icon"> <img src="<?php echo base_url('frontend_assets/'); ?>images/h1.png" alt="image"> </div>
                            <div class="content">
                                <h4>Fill in the form and send us your 
                                    request</h4>
                            </div>
                        </div>  </div>
                    <div class="col-md-4"> <div class="list-item">
                            <div class="icon"> <img src="<?php echo base_url('frontend_assets/'); ?>images/h2.png" alt="image"> </div>
                            <div class="content">
                                <h4>We are looking for the best service for you .</h4>
                            </div>
                        </div> 
                    </div>
                    <div class="col-md-4"> <div class="list-item">
                            <div class="icon"> <img src="<?php echo base_url('frontend_assets/'); ?>images/h3.png" alt="image"> </div>
                            <div class="content">
                                <h4>We make contact and get LHR offers 
                                    your desired service</h4>
                            </div>
                        </div> </div>
                    <div class="content text-center col-md-12"><a href="#" class="read-more" style="box-shadow: 0px 2px 4px #ccc;">Use free wedding planner</a></div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
        <!--=================ABOUT US SECTION END HERE=================================-->

        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 menu-box">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <h3 class="about-h3 blue-color animated bounceInUp" style="color:#0e0e0e;">WE RECOMMENDED SERVICE PROVIDER</h3>
                        <div class="col-md-12">
                            <ul class="nav nav-tabs">

                                <li class="active"><a data-toggle="tab" href="#home">Wedding Vendor</a></li>
                                <?php foreach ($all_s_type as $key) { ?>
                                    <li><a data-toggle="tab" href="#menu1"><?php echo $key->en_serviceTypeName; ?></a></li>
                                <?php } ?>

                                <li><a data-toggle="tab" href="#menu4">More</a></li> 
                            </ul>
                        </div>
                        <div class="tab-content col-md-12 nospace">
                            <div id="home" class="tab-pane fade in active">
                                <div class="menu-boxes-wrap">
                                    <?php foreach ($all_s_prov as $provider) {
                                        ?>
                                        <div class="col-md-3">
                                            <div class="box animated bounceInRight"> <a href="" class="figure-image"> <img src="<?php echo base_url('uploads/') . $provider->provider_image; ?>" class="">
                                                    <div class="figure-overlay  animated flipInX">
                                                        <div class="figure-overlay-container">
                                                            <div class="favourite-bg"><i class="fa fa-heart pinkcolor"></i></div>
                                                        </div>
                                                    </div>
                                                </a>
                                                <h5 class="text-center pinkcolor"><?php echo $provider->provider_name; ?></h5>
                                                <p class="location"><i class="fa fa-map-marker"></i>&nbsp;<?php echo $provider->address; ?>
                                                <div class="clearfix"></div>
                                                </p>
                                                <p class="rating"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <span class="rating-count">(3)</span></p>
                                            </div>
                                        </div>
                                    <?php } ?>
                                    <div class="col-md-3">
                                        <div class="box animated bounceInRight"> <a href="" class="figure-image"> <img src="<?php echo base_url('frontend_assets/'); ?>images/vendor-2.jpg" class="">
                                                <div class="figure-overlay  animated flipInX ">
                                                    <div class="figure-overlay-container">
                                                        <div class="favourite-bg"><i class="fa fa-heart pinkcolor"></i></div>
                                                    </div>
                                                </div>
                                            </a>
                                            <h5 class="text-center pinkcolor">Your online wedding planner</h5>
                                            <p class="location"><i class="fa fa-map-marker"></i>&nbsp;Brunnenstraße 164, 10119 Berlin, Germany
                                            <div class="clearfix"></div>
                                            </p>
                                            <p class="rating"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <span class="rating-count">(3)</span> </p>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="box animated bounceInLeft">
                                            <a href="" class="figure-image">
                                                <img src="<?php echo base_url('frontend_assets/'); ?>images/vendor-3.jpg" class="">
                                                <div class="figure-overlay  animated flipInX ">
                                                    <div class="figure-overlay-container">
                                                        <div class="favourite-bg"><i class="fa fa-heart pinkcolor"></i></div>
                                                    </div>
                                                </div>
                                            </a>
                                            <h5 class="text-center pinkcolor">Your online wedding planner</h5>
                                            <p class="location"><i class="fa fa-map-marker"></i>&nbsp;Brunnenstraße 164, 10119 Berlin, Germany
                                            <div class="clearfix"></div></p>

                                            <p class="rating"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <span class="rating-count">(3)</span> </p>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="box animated bounceInLeft"><a href="" class="figure-image"><img src="<?php echo base_url('frontend_assets/'); ?>images/vendor-4.jpg" class="">
                                                <div class="figure-overlay  animated flipInX ">
                                                    <div class="figure-overlay-container">
                                                        <div class="favourite-bg"><i class="fa fa-heart pinkcolor"></i></div>
                                                    </div>
                                                </div>
                                            </a>
                                            <h5 class="text-center pinkcolor">Your online wedding planner</h5>
                                            <p class="location"><i class="fa fa-map-marker"></i>&nbsp;Brunnenstraße 164, 10119 Berlin,
                                                Germany
                                            <div class="clearfix"></div>
                                            </p>
                                            <p class="rating"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <span class="rating-count">(3)</span> </p>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="menu-boxes-wrap-2">
                                    <div class="col-md-3">
                                        <div class="box animated bounceInRight">
                                            <a href="" class="figure-image"> <img src="<?php echo base_url('frontend_assets/'); ?>images/vendor-3.jpg" class="">
                                                <div class="figure-overlay  animated flipInX ">
                                                    <div class="figure-overlay-container">
                                                        <div class="favourite-bg"><i class="fa fa-heart pinkcolor"></i></div>
                                                    </div>
                                                </div>
                                            </a>
                                            <h5 class="text-center pinkcolor">Your online wedding planner</h5>
                                            <p class="location"><i class="fa fa-map-marker"></i>&nbsp;Brunnenstraße 164, 10119 Berlin,
                                                Germany
                                            <div class="clearfix"></div>
                                            </p>
                                            <p class="rating"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <span class="rating-count">(3)</span> </p>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="box animated bounceInRight"> <a href="" class="figure-image"> <img src="<?php echo base_url('frontend_assets/'); ?>images/vendor-1.jpg" class="">
                                                <div class="figure-overlay  animated flipInX ">
                                                    <div class="figure-overlay-container">
                                                        <div class="favourite-bg"><i class="fa fa-heart pinkcolor"></i></div>
                                                    </div>
                                                </div>
                                            </a>
                                            <h5 class="text-center pinkcolor">Your online wedding planner</h5>
                                            <p class="location"><i class="fa fa-map-marker"></i>&nbsp;Brunnenstraße 164, 10119 Berlin,
                                                Germany
                                            <div class="clearfix"></div>
                                            </p>
                                            <p class="rating"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <span class="rating-count">(3)</span> </p>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="box animated bounceInLeft"> <a href="" class="figure-image"> <img src="<?php echo base_url('frontend_assets/'); ?>images/vendor-4.jpg" class="">
                                                <div class="figure-overlay  animated flipInX ">
                                                    <div class="figure-overlay-container">
                                                        <div class="favourite-bg"><i class="fa fa-heart pinkcolor"></i></div>
                                                    </div>
                                                </div>
                                            </a>
                                            <h5 class="text-center pinkcolor">Your online wedding planner</h5>
                                            <p class="location"><i class="fa fa-map-marker"></i>&nbsp;Brunnenstraße 164, 10119 Berlin,
                                                Germany
                                            <div class="clearfix"></div>
                                            </p>
                                            <p class="rating"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <span class="rating-count">(3)</span> </p>
                                        </div>
                                    </div>


                                    <div class="col-md-3">
                                        <div class="box animated bounceInLeft"> <a href="" class="figure-image"> <img src="<?php echo base_url('frontend_assets/'); ?>images/vendor-2.jpg" class="">
                                                <div class="figure-overlay  animated flipInX ">
                                                    <div class="figure-overlay-container">
                                                        <div class="favourite-bg"><i class="fa fa-heart pinkcolor"></i></div>
                                                    </div>
                                                </div>
                                            </a>
                                            <h5 class="text-center pinkcolor">Your online wedding planner</h5>
                                            <p class="location"><i class="fa fa-map-marker"></i>&nbsp;Brunnenstraße 164, 10119 Berlin, Germany<div class="clearfix"></div>
                                            </p>
                                            <p class="rating"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <span class="rating-count">(3)</span> </p>
                                        </div>
                                    </div>

                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div id="menu1" class="tab-pane fade">
                                <div class="menu-boxes-wrap">
                                    <div class="col-md-3">
                                        <div class="box animated bounceInRight"> <a href="" class="figure-image"><img src="<?php echo base_url('frontend_assets/'); ?>images/vendor-1.jpg" class="">
                                                <div class="figure-overlay  animated flipInX ">
                                                    <div class="figure-overlay-container">
                                                        <div class="favourite-bg"><i class="fa fa-heart pinkcolor"></i></div>
                                                    </div>
                                                </div>
                                            </a>
                                            <h5 class="text-center pinkcolor">Your online wedding planner</h5>
                                            <p class="location"><i class="fa fa-map-marker"></i>&nbsp;Brunnenstraße 164, 10119 Berlin,
                                                Germany
                                            <div class="clearfix"></div>
                                            </p>
                                            <p class="rating"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <span class="rating-count">(3)</span> </p>
                                        </div>
                                    </div>


                                    <div class="col-md-3">
                                        <div class="box animated bounceInRight"> <a href="" class="figure-image"> <img src="<?php echo base_url('frontend_assets/'); ?>images/vendor-2.jpg" class="">
                                                <div class="figure-overlay  animated flipInX ">
                                                    <div class="figure-overlay-container">
                                                        <div class="favourite-bg"><i class="fa fa-heart pinkcolor"></i></div>
                                                    </div>
                                                </div>
                                            </a>
                                            <h5 class="text-center pinkcolor">Your online wedding planner</h5>
                                            <p class="location"><i class="fa fa-map-marker"></i>&nbsp;Brunnenstraße 164, 10119 Berlin,
                                                Germany
                                            <div class="clearfix"></div>
                                            </p>
                                            <p class="rating"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <span class="rating-count">(3)</span> </p>
                                        </div>
                                    </div>


                                    <div class="col-md-3">
                                        <div class="box animated bounceInLeft"> <a href="" class="figure-image"> <img src="<?php echo base_url('frontend_assets/'); ?>images/vendor-3.jpg" class="">
                                                <div class="figure-overlay  animated flipInX ">
                                                    <div class="figure-overlay-container">
                                                        <div class="favourite-bg"><i class="fa fa-heart pinkcolor"></i></div>
                                                    </div>
                                                </div>
                                            </a>
                                            <h5 class="text-center pinkcolor">Your online wedding planner</h5>
                                            <p class="location"><i class="fa fa-map-marker"></i>&nbsp;Brunnenstraße 164, 10119 Berlin,
                                                Germany
                                            <div class="clearfix"></div>
                                            </p>
                                            <p class="rating"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <span class="rating-count">(3)</span> </p>
                                        </div>
                                    </div>


                                    <div class="col-md-3">
                                        <div class="box animated bounceInLeft"> <a href="" class="figure-image"> <img src="<?php echo base_url('frontend_assets/'); ?>images/vendor-4.jpg" class="">
                                                <div class="figure-overlay  animated flipInX ">
                                                    <div class="figure-overlay-container">
                                                        <div class="favourite-bg"><i class="fa fa-heart pinkcolor"></i></div>
                                                    </div>
                                                </div>
                                            </a>
                                            <h5 class="text-center pinkcolor">Your online wedding planner</h5>
                                            <p class="location"><i class="fa fa-map-marker"></i>&nbsp;Brunnenstraße 164, 10119 Berlin,
                                                Germany
                                            <div class="clearfix"></div>
                                            </p>
                                            <p class="rating"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <span class="rating-count">(3)</span> </p>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div id="menu2" class="tab-pane fade">
                                <div class="menu-boxes-wrap">

                                    <div class="col-md-3">
                                        <div class="box animated bounceInRight">
                                            <a href="" class="figure-image"> 
                                                <img src="<?php echo base_url('frontend_assets/'); ?>images/vendor-1.jpg" class="">
                                                <div class="figure-overlay  animated flipInX ">
                                                    <div class="figure-overlay-container">
                                                        <div class="favourite-bg"><i class="fa fa-heart pinkcolor"></i></div>
                                                    </div>
                                                </div>
                                            </a>
                                            <h5 class="text-center pinkcolor">Your online wedding planner</h5>
                                            <p class="location"><i class="fa fa-map-marker"></i>&nbsp;Brunnenstraße 164, 10119 Berlin, Germany 
                                            <div class="clearfix"></div>
                                            </p>
                                            <p class="rating"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <span class="rating-count">(3)</span> </p>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="box animated bounceInRight"> <a href="" class="figure-image"> <img src="<?php echo base_url('frontend_assets/'); ?>images/vendor-2.jpg" class="">
                                                <div class="figure-overlay  animated flipInX ">
                                                    <div class="figure-overlay-container">
                                                        <div class="favourite-bg"><i class="fa fa-heart pinkcolor"></i></div>
                                                    </div>
                                                </div>
                                            </a>
                                            <h5 class="text-center pinkcolor">Your online wedding planner</h5>
                                            <p class="location"><i class="fa fa-map-marker"></i>&nbsp;Brunnenstraße 164, 10119 Berlin,
                                                Germany
                                            <div class="clearfix"></div>
                                            </p>
                                            <p class="rating"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <span class="rating-count">(3)</span> </p>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="box animated bounceInLeft"> <a href="" class="figure-image"> <img src="<?php echo base_url('frontend_assets/'); ?>images/vendor-3.jpg" class="">
                                                <div class="figure-overlay  animated flipInX ">
                                                    <div class="figure-overlay-container">
                                                        <div class="favourite-bg"><i class="fa fa-heart pinkcolor"></i></div>
                                                    </div>
                                                </div>
                                            </a>
                                            <h5 class="text-center pinkcolor">Your online wedding planner</h5>
                                            <p class="location"><i class="fa fa-map-marker"></i>&nbsp;Brunnenstraße 164, 10119 Berlin, Germany
                                            <div class="clearfix"></div>
                                            </p>
                                            <p class="rating"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <span class="rating-count">(3)</span> </p>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="box animated bounceInLeft"> <a href="" class="figure-image"> <img src="<?php echo base_url('frontend_assets/'); ?>images/vendor-4.jpg" class="">
                                                <div class="figure-overlay  animated flipInX ">
                                                    <div class="figure-overlay-container">
                                                        <div class="favourite-bg"><i class="fa fa-heart pinkcolor"></i></div>
                                                    </div>
                                                </div>
                                            </a>
                                            <h5 class="text-center pinkcolor">Your online wedding planner</h5>
                                            <p class="location"><i class="fa fa-map-marker"></i>&nbsp;Brunnenstraße 164, 10119 Berlin,
                                                Germany
                                            <div class="clearfix"></div>
                                            </p>
                                            <p class="rating"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <span class="rating-count">(3)</span> </p>
                                        </div>
                                    </div>

                                    <div class="clearfix"></div>
                                </div>
                            </div>

                            <div id="menu3" class="tab-pane fade">
                                <div class="menu-boxes-wrap">
                                    <div class="col-md-3">
                                        <div class="box animated bounceInRight"> <a href="" class="figure-image"> <img src="<?php echo base_url('frontend_assets/'); ?>images/vendor-1.jpg" class="">
                                                <div class="figure-overlay  animated flipInX ">
                                                    <div class="figure-overlay-container">
                                                        <div class="favourite-bg"><i class="fa fa-heart pinkcolor"></i></div>
                                                    </div>
                                                </div>
                                            </a>
                                            <h5 class="text-center pinkcolor">Your online wedding planner</h5>
                                            <p class="location"><i class="fa fa-map-marker"></i>&nbsp;Brunnenstraße 164, 10119 Berlin,
                                                Germany
                                            <div class="clearfix"></div>
                                            </p>
                                            <p class="rating"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <span class="rating-count">(3)</span> </p>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="box animated bounceInRight"> <a href="" class="figure-image"> <img src="<?php echo base_url('frontend_assets/'); ?>images/vendor-2.jpg" class="">
                                                <div class="figure-overlay  animated flipInX ">
                                                    <div class="figure-overlay-container">
                                                        <div class="favourite-bg"><i class="fa fa-heart pinkcolor"></i></div>
                                                    </div>
                                                </div>
                                            </a>
                                            <h5 class="text-center pinkcolor">Your online wedding planner</h5>
                                            <p class="location"><i class="fa fa-map-marker"></i>&nbsp;Brunnenstraße 164, 10119 Berlin,
                                                Germany
                                            <div class="clearfix"></div>
                                            </p>
                                            <p class="rating"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <span class="rating-count">(3)</span> </p>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="box animated bounceInLeft">
                                            <a href="" class="figure-image"><img src="<?php echo base_url('frontend_assets/'); ?>images/vendor-3.jpg" class="">
                                                <div class="figure-overlay  animated flipInX ">
                                                    <div class="figure-overlay-container">
                                                        <div class="favourite-bg"><i class="fa fa-heart pinkcolor"></i></div>
                                                    </div>
                                                </div>
                                            </a>
                                            <h5 class="text-center pinkcolor">Your online wedding planner</h5>
                                            <p class="location"><i class="fa fa-map-marker"></i>&nbsp;Brunnenstraße 164, 10119 Berlin, Germany
                                            <div class="clearfix"></div>
                                            </p>
                                            <p class="rating"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <span class="rating-count">(3)</span> </p>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="box animated bounceInLeft">
                                            <a href="" class="figure-image"> <img src="<?php echo base_url('frontend_assets/'); ?>images/vendor-4.jpg" class="">
                                                <div class="figure-overlay  animated flipInX ">
                                                    <div class="figure-overlay-container">
                                                        <div class="favourite-bg"><i class="fa fa-heart pinkcolor"></i></div>
                                                    </div>
                                                </div>
                                            </a>
                                            <h5 class="text-center pinkcolor">Your online wedding planner</h5>
                                            <p class="location"><i class="fa fa-map-marker"></i>&nbsp;Brunnenstraße 164, 10119 Berlin, Germany <div class="clearfix"></div>
                                            </p>
                                            <p class="rating"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <span class="rating-count">(3)</span> </p>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="menu-boxes-wrap-2">
                                    <div class="col-md-3">
                                        <div class="box animated bounceInRight"> <a href="" class="figure-image"> <img src="<?php echo base_url('frontend_assets/'); ?>images/vendor-3.jpg" class="">
                                                <div class="figure-overlay  animated flipInX ">
                                                    <div class="figure-overlay-container">
                                                        <div class="favourite-bg"><i class="fa fa-heart pinkcolor"></i></div>
                                                    </div>
                                                </div>
                                            </a>
                                            <h5 class="text-center pinkcolor">Your online wedding planner</h5>
                                            <p class="location"><i class="fa fa-map-marker"></i>&nbsp;Brunnenstraße 164, 10119 Berlin,
                                                Germany
                                            <div class="clearfix"></div>
                                            </p>
                                            <p class="rating"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <span class="rating-count">(3)</span> </p>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="box animated bounceInRight"> <a href="" class="figure-image"> <img src="<?php echo base_url('frontend_assets/'); ?>images/vendor-1.jpg" class="">
                                                <div class="figure-overlay  animated flipInX ">
                                                    <div class="figure-overlay-container">
                                                        <div class="favourite-bg"><i class="fa fa-heart pinkcolor"></i></div>
                                                    </div>
                                                </div>
                                            </a>
                                            <h5 class="text-center pinkcolor">Your online wedding planner</h5>
                                            <p class="location"><i class="fa fa-map-marker"></i>&nbsp;Brunnenstraße 164, 10119 Berlin,
                                                Germany
                                            <div class="clearfix"></div>
                                            </p>
                                            <p class="rating"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <span class="rating-count">(3)</span> </p>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="box animated bounceInLeft"> <a href="" class="figure-image"> <img src="<?php echo base_url('frontend_assets/'); ?>images/vendor-4.jpg" class="">
                                                <div class="figure-overlay  animated flipInX ">
                                                    <div class="figure-overlay-container">
                                                        <div class="favourite-bg"><i class="fa fa-heart pinkcolor"></i></div>
                                                    </div>
                                                </div>
                                            </a>
                                            <h5 class="text-center pinkcolor">Your online wedding planner</h5>
                                            <p class="location"><i class="fa fa-map-marker"></i>&nbsp;Brunnenstraße 164, 10119 Berlin,
                                                Germany
                                            <div class="clearfix"></div>
                                            </p>
                                            <p class="rating"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <span class="rating-count">(3)</span> </p>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="box animated bounceInLeft"> <a href="" class="figure-image"> <img src="<?php echo base_url('frontend_assets/'); ?>images/vendor-2.jpg" class="">
                                                <div class="figure-overlay  animated flipInX ">
                                                    <div class="figure-overlay-container">
                                                        <div class="favourite-bg"><i class="fa fa-heart pinkcolor"></i></div>
                                                    </div>
                                                </div>
                                            </a>
                                            <h5 class="text-center pinkcolor">Your online wedding planner</h5>
                                            <p class="location"><i class="fa fa-map-marker"></i>&nbsp;Brunnenstraße 164, 10119 Berlin,
                                                Germany
                                            <div class="clearfix"></div>
                                            </p>
                                            <p class="rating"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <span class="rating-count">(3)</span> </p>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>    
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <!--===============================MENU SECTION END HERE=======================================-->
        <div class="carousel-reviews broun-block col-md-12">
            <div class="container">
                <div class="row">
                    <div id="carousel-reviews" class="carousel slide" data-ride="carousel">
                        <div class="col-md-12 text-center">
                            <h3 class="about-h3 animated bounceInUp" style="margin-top: 2px;"> TESTIMONIALS</h3>
                            <span class="figure-image animated bounceInRight"><img src="<?php echo base_url('frontend_assets/'); ?>images/testi-brdr.png"></span> </div>
                        <div class="clearfix"></div>
                        <div class="carousel-inner testimonial-wrap">
                            <div class="item active">
                                <div class="col-md-4 col-sm-4  testi-wrap ">
                                    <div class="testimonial"><article class="text-box">Esse maxime tempore maiores laboriosam nobis, aut cum quidem est ab ipsam soluta voluptate totam quibusdam quo neque</article><div class="author-block"><div class="photo-container" style="background-image: url('<?php echo base_url('frontend_assets/'); ?>images/1.jpg')"></div><strong class="name">MARCI</strong><small class="text-alt">Designer</small></div></div>

                                </div>
                                <div class="col-md-4 col-sm-4  testi-wrap">
                                    <div class="testimonial"><article class="text-box">Esse maxime tempore maiores laboriosam nobis, aut cum quidem est ab ipsam soluta voluptate totam quibusdam quo neque</article><div class="author-block"><div class="photo-container" style="background-image: url('<?php echo base_url('frontend_assets/'); ?>images/1.jpg')"></div><strong class="name">MARCI</strong><small class="text-alt">Designer</small></div></div>

                                </div>
                                <div class="col-md-4 col-sm-4  testi-wrap">
                                    <div class="testimonial"><article class="text-box">Esse maxime tempore maiores laboriosam nobis, aut cum quidem est ab ipsam soluta voluptate totam quibusdam quo neque</article><div class="author-block"><div class="photo-container" style="background-image: url('<?php echo base_url('frontend_assets/'); ?>images/1.jpg')"></div><strong class="name">MARCI</strong><small class="text-alt">Designer</small></div></div>

                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="item">
                                <div class="col-md-4 col-sm-4  testi-wrap">
                                    <div class="testimonial"><article class="text-box">Esse maxime tempore maiores laboriosam nobis, aut cum quidem est ab ipsam soluta voluptate totam quibusdam quo neque</article><div class="author-block"><div class="photo-container" style="background-image: url('<?php echo base_url('frontend_assets/'); ?>images/1.jpg')"></div><strong class="name">MARCI</strong><small class="text-alt">Designer</small></div></div>
                                </div>
                                <div class="col-md-4 col-sm-4  testi-wrap">
                                    <div class="testimonial"><article class="text-box">Esse maxime tempore maiores laboriosam nobis, aut cum quidem est ab ipsam soluta voluptate totam quibusdam quo neque</article><div class="author-block"><div class="photo-container" style="background-image: url('<?php echo base_url('frontend_assets/'); ?>images/1.jpg')"></div><strong class="name">MARCI</strong><small class="text-alt">Designer</small></div></div>
                                </div>
                                <div class="col-md-4 col-sm-4  testi-wrap">
                                    <div class="testimonial"><article class="text-box">Esse maxime tempore maiores laboriosam nobis, aut cum quidem est ab ipsam soluta voluptate totam quibusdam quo neque</article><div class="author-block"><div class="photo-container" style="background-image: url('<?php echo base_url('frontend_assets/'); ?>images/1.jpg')"></div><strong class="name">MARCI</strong><small class="text-alt">Designer</small></div></div>
                                </div>
                            </div>
                        </div>
                        <a class="left carousel-control" href="#carousel-reviews" role="button" data-slide="prev" style="background-image: none;"> <i class="fa fa-chevron-left test-left-arrow"></i> </a> <a class="right carousel-control" href="#carousel-reviews" role="button" data-slide="next" style="background-image: none;"> <i class="fa fa-chevron-right test-right-arrow"></i> </a>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
        <!--===============================TESTIMONIAL SECTION END HERE=======================================-->


        <!--=====================================================
                      MID CONTAINER SECTION END HERE
        -=============================================================-->
        
        <?php include('footer.php'); ?>
        <script src="<?php echo base_url('frontend_assets/'); ?>js/backtotop.js" type="text/javascript"></script>
        <script src="<?php echo base_url('frontend_assets/'); ?>js/jquery.min.js" type="text/javascript"></script>
        <!--<script src="js/bootstrap.min.js" type="text/javascript"></script> -->
        <script src="<?php echo base_url('frontend_assets/'); ?>js/owl.carousel.js" type="text/javascript"></script>
        <script src="<?php echo base_url('frontend_assets/'); ?>js/dev.min.js" type="text/javascript"></script>
    </body>
</html>
<script src="<?php echo base_url('frontend_assets/'); ?>js/common_scripts_min.js"></script>   <!-- For Mobile Menu-->
<div class="modal fade login_form" id="vendor_login" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content login_div">
            <div class="modal-header boder_none" >
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="z-index: 9999;
                        position: relative;
                        top: -29px;
                        left: 20px;
                        background: #000;
                        opacity: 1;
                        border-radius: 50%"><span class="glyphicon glyphicon-remove removed"aria-hidden="true" style="line-height:21px; font-size:16px !important; color: #f393bd;"></span></button>
            </div>
            <div class="modal-body">
                <div class="karara">
                    <!-- Logo -->

                    <!-- Form Base -->
                    <div class="row">
                        <div class="form-base">
                            <header>
                                <h1 class="pop-login">Vendor Login</h1>
                            </header>
                            <!-- Form -->
                            <section>
                                <form action="<?php echo site_url('login_vendors/index'); ?>" method="post" id="vendor_login">
                                    <!-- Socials - delete this section if you don't want social connects -->

                                    <!-- /.Socials -->
                                    <div class="input-group login-username">
                                        <div class="input-group-addon">Username</div>
                                        <input type="email" placeholder="User name" name="vendoremail" id="login-username">
                                        <span aria-hidden="true" id="status-username" class="status-icon"></span>
                                    </div>

                                    <div class="input-group login-password">
                                        <div class="input-group-addon">Password</div>
                                        <input type="password" placeholder="Password" name="password" id="login-password">
                                        <span aria-hidden="true" id="status-password" class="status-icon"></span></div>

                                    <div class="row section-action">
                                        <!-- Forgotten Password Trigger -->
                                        <div class="col-xs-12 form-group"><a class="forgotten-password-trigger custom-color" data-toggle="modal" data-target="#Forrget_form">Forgotten password?</a></div>
                                        <!-- Submit -->
                                        <div class="col-xs-12">
                                            <button type="submit" class="btn primary pull-right custom-color-back">Login</button>
                                            <a href="<?php echo site_url('website/view_vendor_signup'); ?>">Sign Up</a>
                                        </div>
                                    </div>

                                </form>
                            </section>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade login_form" id="login_form" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content login_div">
            <div class="modal-header boder_none">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="z-index: 9999;
                        position: relative;
                        top: -29px;
                        left: 20px;
                        background: #000;
                        opacity: 1;
                        border-radius: 50%"><span class="glyphicon glyphicon-remove removed"aria-hidden="true" style="line-height:21px; font-size:16px !important; color: #f393bd;"></span></button>
            </div>
            <div class="modal-body">
                <div class=" karara">
                    <!-- Logo -->          
                    <!-- Form Base -->
                    <div class="row">
                        <div class="form-base">
                            <header>

                                <h1 class="pop-login">User Login</h1>
                            </header>
                            <!-- Form -->
                            <section>
                                <form action="<?php echo site_url('login_users/index'); ?>" method="post" id="login-form">

                                    <div class="input-group login-username">
                                        <div class="input-group-addon">Email</div>
                                        <input type="text" placeholder="E-mail" name="email" 
                                               id="login-username">
                                        <span aria-hidden="true" id="status-username" class="status-icon"></span>
                                    </div>
                                    <div class="input-group login-password">
                                        <div class="input-group-addon">Password</div>
                                        <input type="password" placeholder="Password" name="password" id="login-password">
                                        <span aria-hidden="true" id="status-password" class="status-icon"></span>
                                    </div>
                                    <div class="row section-action">
                                        <!-- Forgotten Password Trigger -->
                                        <div class="col-xs-12 form-group"><a class="forgotten-password-trigger custom-color" data-toggle="modal" data-target="#Forrget_form">Forgotten password?</a></div>
                                        <!-- Submit -->
                                        <div class="col-xs-12">
                                            <button type="submit" class="btn primary pull-right custom-color-back">Login</button>
                                            <a href="<?php echo site_url('website/view_user_signup'); ?>">Sign Up</a>
                                            <a href="<?php echo site_url("auth/login"); ?>" target="_blank" style="margin-left: 82px;">Login with Facebook</a>
                                        </div>
                                    </div>
                                </form>
                            </section>             
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
